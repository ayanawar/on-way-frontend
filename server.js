const express = require('express');
const path = require('path');
const app = express();
const io = ('socket.io-client');

const socket = io('http://localhost:3000');

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.listen(8120);