// sidebar nav links
export default {
   category1: [
      {
         "menu_title": "sidebar.mainDashboard",
         "menu_icon": "ti-home",
         "path": "/app/home-page",
         "new_item": false,
         "child_routes": null
      },
      // {
      //    "menu_title": "sidebar.incomingrequest",
      //    "menu_icon": "zmdi zmdi-assignment-o",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "path": "/app/accountingtree/addaccount",
      //          "new_item": false,
      //          "menu_title": "sidebar.addaccount"
      //       },
      //       {
      //          "path": "/app/accountingtree/editaccount",
      //          "new_item": false,
      //          "menu_title": "sidebar.editaccount"
      //       },
      //       {
      //          "path": "/app/accountingtree/dailybox",
      //          "new_item": false,
      //          "menu_title": "sidebar.dailybox"
      //       },
      //       // {
      //       //    "path": "/app/dealers-management/dealers-operations",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.appBar"
      //       // },
      //       // {
      //       //    "path": "/app/dealers-management/dealers-gift-points",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.dealersGift"
      //       // },
      //    ]
      // },
     
      // {
      //    "menu_title": "sidebar.charts",
      //    "menu_icon": "zmdi zmdi-assignment-o",
      //    "path": " /app/charts/re-charts",
      //    "new_item": false,
      //    "child_routes": null
      // },
      {
         "menu_title": "sidebar.incomingrequest",
         "menu_icon": "zmdi zmdi-assignment-o",
         "path": "/app/requests/incomingrequests",
         "new_item": false,
         "child_routes": null
      },
      
      {
         "menu_title": "sidebar.customerrequestinfo",
         "menu_icon": "zmdi zmdi-file-text",
         "path": "/app/requests/customersrequest",
         "new_item": false,
         "child_routes": null
      },
      {
         
         "menu_icon": "zmdi zmdi-accounts-alt",
         "path": "/app/worker-management/add-workers",
         "new_item": false,
         "menu_title": "sidebar.addWorker",
         "child_routes": null
      },
      {
         
         "menu_icon": "zmdi zmdi-accounts-alt",
         "path": "/app/worker-management/all-workers",
         "new_item": false,
         "menu_title": "sidebar.all-workers",
         "child_routes": null
      },

      // {
      //    "menu_title": "sidebar.manageworkers",
      //    "menu_icon": "zmdi zmdi-accounts-alt",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "path": "/app/worker-management/add-workers",
      //          "new_item": false,
      //          "menu_title": "sidebar.addWorker"
      //       },
            // {
            //    "path": "/app/worker-management/all-workers",
            //    "new_item": false,
            //    "menu_title": "sidebar.all-workers"
            // },
            // {
            //    "path": "/app/representatives-management/representatives-transactions",
            //    "new_item": false,
            //    "menu_title": "sidebar.sweetAlert"
            // },
      //    ]
      // },
   
      {
         "menu_title": "sidebar.addservice",
         "menu_icon": "zmdi zmdi-settings",
         "path": "/app/servicemanagement/addservice",
         "new_item": false,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.allservice",
         "menu_icon": "zmdi zmdi-settings",
         "path": "/app/servicemanagement/allservice",
         "new_item": false,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.changepassword",
         "menu_icon": "zmdi zmdi-accounts-alt",
         "path": "/app/requests/changepassword",
         "new_item": false,
         "child_routes": null
      },
      // {
      //    "menu_title": "sidebar.reportonesss",
      //    "menu_icon": "zmdi zmdi-assignment-check",
      //    "path": "/app/charts/re-charts",
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "menu_title": "sidebar.profitAccount",
      //          "path": "/app/accounts/profit-account-statement",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.dealerProfit",
      //          "path": "/app/accounts/dealer-profit-account-statement",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.alaimtidadAccount",
      //          "path": "/app/accounts/alaimtidad-account-statement",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.register",
      //          "path": "/app/accounts/dealers-account-statement",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.faq(s)",
      //          "new_item": false,
      //          "path": "/app/accounts/point-of-sale-account-statement"
      //       },
      //       {
      //          "menu_title": "sidebar.representativesAccount",
      //          "new_item": false,
      //          "path": "/app/accounts/representatives-account-statement"
      //       },
      //       {
      //          "menu_title": "sidebar.alaimtidadMovement",
      //          "path": "/app/accounts/alaimtidad-movement-of-balances",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.receivableReceipts",
      //          "new_item": false,
      //          "path": "/app/accounts/receivable-receipts"
      //       },
      //       {
      //          "menu_title": "sidebar.collectAnd",
      //          "new_item": false,
      //          "path": "/app/accounts/collect-and-issue-receivable-receipts"
      //       },
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.moneymang",
      //    "menu_icon": "zmdi zmdi-money",
      //    "path": "/app/charts/re-charts",
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "menu_title": "sidebar.sendandcashmoney",
      //          "menu_icon": "zmdi zmdi-money-off",
      //          "path": "/app/widgets",
      //          "type_multi": null,
      //          "new_item": false,
      //          "child_routes": [
                  // {
                  //    "path": "/app/representatives-management/add-representatives",
                  //    "new_item": false,
                  //    "menu_title": "sidebar.addRepresentatives"
                  // },
                  // {
                  //    "path": "/app/representatives-management/all-representatives",
                  //    "new_item": false,
                  //    "menu_title": "sidebar.allRepresentatives"
                  // },
                  // {
                  //    "path": "/app/representatives-management/representatives-transactions",
                  //    "new_item": false,
                  //    "menu_title": "sidebar.sweetAlert"
                  // },
            //    ]
            // },
            // {
            //    "menu_title": "sidebar.alaimtidadAvailabilityReport",
            //    "new_item": false,
            //    "path": "/app/reports/alaimtidad-availability-report"
            // },
            // {
            //    "menu_title": "sidebar.alaimtidadPrinted",
            //    "new_item": false,
            //    "path": "/app/reports/alaimtidad-printed-report"
            // },
            // {
            //    "menu_title": "sidebar.companiesAvailabilityReport",
            //    "path": "/app/reports/companies-availability-report",
            //    "new_item": false,
            // },
            // {
            //    "menu_title": "sidebar.companiesMinimum",
            //    "path": "/app/reports/companies-minimum-report",
            //    "new_item": false,
            // },
      //       {
      //          "menu_title": "sidebar.daily-money-report",
      //          "new_item": false,
      //          "path": "/app/reports/daily-money-report"
      //       },
      //       {
      //          "menu_title": "sidebar.weekly-money-report",
      //          "new_item": false,
      //          "path": "/app/reports/weekly-money-report"
      //       },
      //         {
      //          "menu_title": "sidebar.monthlymoneyreport",
      //          "new_item": false,
      //          "path": "/app/reports/monthly_money_report"
      //        },

      //        {
      //          "menu_title": "sidebar.annualmoneyreport",
      //          "new_item": false,
      //          "path": "/app/reports/annual_money_report"
      //        },
      //    ]
      // },
     
      // {
      //    "menu_title": "sidebar.alaimtidadBalances",
      //    "menu_icon": "ti-credit-card",
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "menu_title": "sidebar.createAlaimtidadBalances",
      //          "new_item": false,
      //          "path": "/app/alaimtidad-balances/create-alaimtidad-balances"
      //       },
      //       {
      //          "menu_title": "sidebar.presentAnd",
      //          "new_item": false,
      //          "path": "/app/alaimtidad-balances/present-and-print-alaimtidad-balances"
      //       },
      //       {
      //          "menu_title": "sidebar.balancesTransfer",
      //          "path": "/app/alaimtidad-balances/balances-transfer",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.balanceWithdrawal",
      //          "path": "/app/alaimtidad-balances/withdrawal-balances",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.withdrawalAccount",
      //          "path": "/app/alaimtidad-balances/withdrawal-account-statement",
      //          "new_item": false,
      //       },
      //       {
      //          "menu_title": "sidebar.printBalances",
      //          "path": "/app/alaimtidad-balances/print-balances",
      //          "new_item": false,
      //       },
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.salary",
      //    "menu_icon": "ti-money",
      //    "new_item": false,
      //    "child_routes": null,
      //    "path": "/app/salarysetting/salarysettingemployees"
        
      // },
      // {
      //    "menu_title": "sidebar.bankmoney",
      //    "menu_icon": "ti-credit-card",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
            // {
            //    "path": "/app/points-of-sale-management/display-points-of-sale",
            //    "menu_title": "sidebar.dispalyPOS"
            // },
            // {
            //    "path": "/app/points-of-sale-management/display-and-update-points-of-sale",
            //    "new_item": false,
            //    "menu_title": "sidebar.gallery"
            // },
            // {
            //    "path": "/app/points-of-sale-management/transactions-of-points-of-sale",
            //    "new_item": false,
            //    "menu_title": "sidebar.pricing"
            // },
            // {
            //    "path": "/app/points-of-sale-management/discount-and-extra-fees-of-points-of-sale",
            //    "menu_title": "PageTitleBar.discountAndExtraFess"
            // },
            // {
            //    "path": "/app/points-of-sale-management/regions-discount-and-extra-fees",
            //    "menu_title": "sidebar.regionsDiscount"
            // },
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.sendandcashmoney",
      //    "menu_icon": "zmdi zmdi-money-off",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       // {
      //       //    "path": "/app/representatives-management/add-representatives",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.addRepresentatives"
      //       // },
      //       // {
      //       //    "path": "/app/representatives-management/all-representatives",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.allRepresentatives"
      //       // },
      //       // {
      //       //    "path": "/app/representatives-management/representatives-transactions",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.sweetAlert"
      //       // },
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.restrication",
      //    "menu_icon": "zmdi zmdi-file-text",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
            // {
            //    "path": "/app/restrictions/openedrestriction",
            //    "new_item": false,
            //    "menu_title": "sidebar.openedrestriction"
            // },
            // {
            //    "path": "/app/invoices/addinvoice",
            //    "new_item": false,
            //    "menu_title": "sidebar.addinvoice"
            // },

            // {
            //    "path": "/app/invoices/dailyinvoice",
            //    "new_item": false,  
            //    "menu_title": "sidebar.dailyinvoice"
            // },

            // {
            //    "path": "/app/invoices/payinvoice",
            //    "new_item": false,
            //    "menu_title": "sidebar.payinvoice"
            // },

      //       {
      //          "path": "/app/invoices/allinvoices",
      //          "new_item": false,
      //          "menu_title": "sidebar.allinvoices"
      //       },
          
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.clientsmang",
      //    "menu_icon": "zmdi zmdi-accounts-alt",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [

      //       {
      //          "path": "/app/clients-managment/add-client",
      //          "new_item": false,
      //          "menu_title": "sidebar.add-client"
      //       },

            
      //       {
      //          "path": "/app/clients-managment/all-clients",
      //          "new_item": false,
      //          "menu_title": "sidebar.all-clients"
      //       },

      //       {
      //          "path": "/app/clients-managment/clientaccountstatment",
      //          "new_item": false,
      //          "menu_title": "sidebar.clientaccountstatment"
      //       },
         
      //       {
      //       "path": "/app/clients-managment/sales-invoices",
      //          "new_item": false,
      //          "menu_title": "sidebar.salesInvoices"
      //       },
      //       {
      //          "path": "/app/clients-managment/clientsaccount",
      //          "new_item": false,
      //          "menu_title": "sidebar.clientsaccount"
      //       },
            // {
            //    "path": "/app/representatives-management/add-representatives",
            //    "new_item": false,
            //    "menu_title": "sidebar.addRepresentatives"
            // },
            // {
            //    "path": "/app/representatives-management/all-representatives",
            //    "new_item": false,
            //    "menu_title": "sidebar.allRepresentatives"
            // },
            // {
            //    "path": "/app/representatives-management/representatives-transactions",
            //    "new_item": false,
            //    "menu_title": "sidebar.sweetAlert"
            // },
      //    ]
      // },

      // {
      //    "menu_title": "sidebar.restrication",
      //    "menu_icon": "zmdi zmdi-file-text",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [

      

            
          
      //    ]
      // },
      
    
      
      // {
      //    "menu_title": "sidebar.widgets",
      //    "menu_icon": "zmdi zmdi-accounts-alt",
      //    "path": "/app/widgets",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       // {
      //       //    "path": "/app/representatives-management/add-representatives",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.addRepresentatives"
      //       // },
      //       // {
      //       //    "path": "/app/representatives-management/all-representatives",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.allRepresentatives"
      //       // },
      //       // {
      //       //    "path": "/app/representatives-management/representatives-transactions",
      //       //    "new_item": false,
      //       //    "menu_title": "sidebar.sweetAlert"
      //       // },
      //    ]
      // },  
      // {
      //    "menu_title": "sidebar.suppliersManagement",
      //    "menu_icon": "zmdi zmdi-balance",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "path": "/app/suppliers-management/add-suppliers",
      //          "new_item": false,
      //          "menu_title": "sidebar.addSuppliers"
      //       },
      //       {
      //          "path": "/app/suppliers-management/all-suppliers",
      //          "new_item": false,
      //          "menu_title": "sidebar.allSuppliers"
      //       },
          
      //       {
      //       "path": "/app/suppliers-management/purchases-invoices",
      //          "new_item": false,
      //          "menu_title": "sidebar.purchasesInvoices"
      //       },
            // {
            //    "path": "/app/suppliers-management/sold-cards",
            //    "new_item": false,
            //    "menu_title": "sidebar.soldCards"
            // },
            // {
            //    "path": "/app/suppliers-management/available-cards",
            //    "new_item": false,
            //    "menu_title": "sidebar.availableCards"
            // },
            // {
            //    "path": "/app/suppliers-management/received-bundles",
            //    "new_item": false,
            //    "menu_title": "sidebar.receivedBundles"
            // }
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.employee-managment",
      //    "menu_icon": "zmdi zmdi-settings",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [

      //         {
      //          "path": "/app/employee-managment/addemployee",
      //          "new_item": false,
      //          "menu_title": "sidebar.addemployee"
      //       },

      //       {
      //          "path": "/app/employee-managment/allemployee",
      //          "new_item": false,
      //          "menu_title": "sidebar.allemployee"
      //       },

      //       {
      //          "path": "/app/employee-managment/employeetransaction",
      //          "new_item": false,
      //          "menu_title": "sidebar.employeetransaction"
      //       },
      //       {
      //          "menu_title": "sidebar.salary",
      //          "new_item": false,
      //          "path": "/app/salarysetting/salarysettingemployees"
              
      //       },
           
      
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.inventory",
      //    "menu_icon": "zmdi zmdi-settings",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [
      //       {
      //          "path": "/app/inventory/categories",
      //          "new_item": false,
      //          "menu_title": "sidebar.categories"
      //       },
      //       {
      //          "path": "/app/inventory/warehouses",
      //          "new_item": false,
      //          "menu_title": "sidebar.warehouses"
      //       },
      //       {
      //          "path": "/app/inventory/add-products",
      //          "new_item": false,
      //          "menu_title": "sidebar.addProducts"
      //       },
      //       {
      //          "path": "/app/inventory/display-all-products",
      //          "new_item": false,
      //          "menu_title": "sidebar.displayAll"
      //       },
      //       {
      //          "path": "/app/inventory/irredeemable",
      //          "new_item": false,
      //          "menu_title": "sidebar.irredeemable"
      //       },
      //       {
      //          "path": "/app/inventory/returns",
      //          "new_item": false,
      //          "menu_title": "sidebar.returns"
      //       },
      //       {
      //          "path": "/app/inventory/out-of-inventory",
      //          "new_item": false,
      //          "menu_title": "sidebar.outofinventory"
      //       },
            // {
            //    "menu_title": "sidebar.salary",
            //    "new_item": false,
            //    "path": "/app/salarysetting/salarysettingemployees"
              
            // },
           
      
      //    ]
      // },
      // {
      //    "menu_title": "sidebar.systemSettings",
      //    "menu_icon": "zmdi zmdi-settings",
      //    "type_multi": null,
      //    "new_item": false,
      //    "child_routes": [

            

      //       {
      //          "path": "/app/system-settings/system-user",
      //          "new_item": false,
      //          "menu_title": "sidebar.systemuser"
      //       },
      //       {
      //          "path": "/app/system-settings/system-user-update",
      //          "new_item": false,
      //          "menu_title": "sidebar.systemuserupdate"
      //       },
      //       {
      //          "path": "/app/system-settings/costcenter",
      //          "new_item": false,
      //          "menu_title": "sidebar.costcenter"
      //       },
      //       {
      //          "menu_title": "sidebar.currencymang",
      //          "menu_icon": "zmdi zmdi-money-box",
      //          "new_item": false,
      //          "child_routes": null,
      //          "path": "/app/currencymang"
      //       },
      //       {
      //          "path": "/app/clients-managment/reciepttypes",
      //          "new_item": false,
      //          "menu_title": "sidebar.reciepttypes"
      //       },
            // {
            //    "path": "/app/system-settings/extra-transfer-fees",
            //    "new_item": false,
            //    "menu_title": "sidebar.basic"
            // },
            // {
            //    "path": "/app/system-settings/display-regions-and-areas",
            //    "new_item": false,
            //    "menu_title": "sidebar.dataTable"
            // },
            // {
            //    "path": "/app/system-settings/determine-the-exchange-rate",
            //    "new_item": false,
            //    "menu_title": "sidebar.responsive"
            // },
            // {
            //    "path": "/app/system-settings/disable-system",
            //    "new_item": false,
            //    "menu_title": "sidebar.disableSystem" 
            // },
            // {
            //    "path": "/app/system-settings/reset-password",
            //    "new_item": false,
            //    "menu_title": "sidebar.resetPassword"
            // }
   //       ]
   //    }
    ],
   category2: [
   ],
   category3: [
      {
         "menu_title": "sidebar.DealerCellcards",
         "menu_icon": "zmdi zmdi-file-text",
         "path": "/app/Dealer/charts",
         "new_item": false,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.printBalances",
         "menu_icon": "zmdi zmdi-file-text",
         "path": "/app/DealerPrintBalances",
         "new_item": false,
         "child_routes": null
      },
      {
         "menu_title": "sidebar.DealerOperations",
         "menu_icon": "zmdi zmdi-chart-donut",
         "path": "/app/DealerRequestCredit/RequestfromEmtdad",
         "new_item": false,
         "child_routes": null,
         
       },
      {
         "menu_title": "sidebar.DealerAccountStatement",
         "menu_icon": "zmdi zmdi-grid",
         "path": "/app/DealerPrintCredit/Dealerprint-balances",
         "new_item": false,
         "child_routes": null
      },
   ],
         category21: [
      
            {
               "menu_title": "sidebar.savedcards",
               "menu_icon": "zmdi zmdi-view-web",
               "path": "/app/SupCards/savedcards",
               "new_item": false,
               "child_routes": null
            },
            {
               "menu_title": "sidebar.paidcards",
               "menu_icon": "zmdi zmdi-view-web",
               "path": "/app/SupCards/paidcards",
               "new_item": false,
               "child_routes": null
            },
       
            {
               "menu_title": "sup.totalAssesment",
               "menu_icon": "zmdi zmdi-view-web",
               "path": "/app/SupCards/totalAssesment",
               "new_item": false,
               "child_routes": null
            },
         ],
      
   category4: [

   ],
   category5: [
   ],
   category6: [
   ]
}
