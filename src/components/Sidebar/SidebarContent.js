/**
 * Sidebar Content
 */
import React, { Component } from 'react';
import List from '@material-ui/core/List';
// import ListSubheader from '@material-ui/core/ListSubheader';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import IntlMessages from 'Util/IntlMessages';

import NavMenuItem from './NavMenuItem';

// redux actions
import { onToggleMenu } from 'Actions';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';

class SidebarContent extends Component {

	toggleMenu(menu, stateCategory) {
		let data = {
			menu,
			stateCategory
		}
		this.props.onToggleMenu(data);
	};
	state={
		type:''
	}
componentDidMount ()
{
this.setState({type:localStorage.getItem("user_type_name")})
}

	render() {
		const { sidebarMenus } = this.props.sidebar;
		//  console.log("Type",this.state.type);
		 if (this.state.type=='الوكيل') {
			return (
				<div className="rct-sidebar-nav">
						<nav className="navigation">
				
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							// subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.component" /></ListSubheader>}
						>
							{sidebarMenus.category3.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category3')}
								/>
							))}
						</List>
					</nav>
				</div>
			);
		}
		else if(this.state.type == 'المورد'){
			return (
				<div className="rct-sidebar-nav">
						<nav className="navigation">
						<List
							className="rct-mainMenu p-0 m-0 list-unstyled"
							// subheader={
							// 	<ListSubheader className="side-title" component="li">
							// 		<IntlMessages id="sidebar.general" />
							// 	</ListSubheader>}
						>
							{sidebarMenus.category21.map((menu, key) => (
								<NavMenuItem
									menu={menu}
									key={key}
									onToggleMenu={() => this.toggleMenu(menu, 'category21')}
								/>
							))}
						</List>
					</nav>
				</div>
			);
		}
		else
		{//admin
		return (
		<React.Fragment>
    	{  PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") || PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ||  PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
			<div className="rct-sidebar-nav">
					<nav className="navigation">
					<List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						// subheader={
						// 	<ListSubheader className="side-title" component="li">
						// 		<IntlMessages id="sidebar.general" />
						// 	</ListSubheader>}
					>
						{sidebarMenus.category1.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category1')}
							/>
						))}
					</List>
					{/* <List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.modules" /></ListSubheader>}
					>
						{sidebarMenus.category2.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category2')}
							/>
						))}
					</List> */}
					{/* <List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.component" /></ListSubheader>}
					>
						{sidebarMenus.category3.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category3')}
							/>
						))}
					</List> */}
					<List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						// subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.features" /></ListSubheader>}
					>
						{sidebarMenus.category4.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category4')}
							/>
						))}
					</List>
					<List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						// subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.applications" /></ListSubheader>}
					>
						{sidebarMenus.category5.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category5')}
							/>
						))}
					</List>
					<List
						className="rct-mainMenu p-0 m-0 list-unstyled"
						// subheader={<ListSubheader className="side-title" component="li"><IntlMessages id="sidebar.extensions" /></ListSubheader>}
					>
						{sidebarMenus.category6.map((menu, key) => (
							<NavMenuItem
								menu={menu}
								key={key}
								onToggleMenu={() => this.toggleMenu(menu, 'category6')}
							/>
						))}
					</List>
				</nav>
			</div>
			: (
				this.props.history.push("/access-denied")
			   )
			} 
			</React.Fragment>
		);
						}
	}
}

// map state to props
const mapStateToProps = ({ sidebar, settings }) => {
	return { sidebar, settings };
};

export default withRouter(connect(mapStateToProps, {
    onToggleMenu
})(SidebarContent));
