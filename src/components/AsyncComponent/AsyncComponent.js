/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */


/**
 * الرئيسية
 * إنشاء أرصدة امتداد
 * تحميل كارتات الشركات
 * إدارة و حسابات الشحن الفورى
 * إدارة و حسابات الموردين
 * إدارة و حسابات المندوبين
 * إدارة و حسابات نقاط البيع
 * إدارة الوكلاء
 * تحديد الوكلاء للمندوب
 * سحب الرصيد
 * تحويل الرصيد
 * سعر البيع للفئات
 * تقارير و حسابات
 * إعدادات النظام
 */
import React from 'react';
import Loadable from 'react-loadable';

// rct page loader
import RctPageLoader from 'Components/RctPageLoader/RctPageLoader';

// ecommerce dashboard
// const AsyncEcommerceDashboardComponent = Loadable({
// 	loader: () => import("Routes/dashboard/ecommerce"),
// 	loading: () => <RctPageLoader />,
// });

// agency dashboard
// const AsyncSaasDashboardComponent = Loadable({
// 	loader: () => import("Routes/dashboard/saas"),
// 	loading: () => <RctPageLoader />,
// });

// agency dashboard
// const AsyncAgencyDashboardComponent = Loadable({
// 	loader: () => import("Routes/dashboard/agency"),
// 	loading: () => <RctPageLoader />,
// });

// boxed dashboard
// const AsyncNewsDashboardComponent = Loadable({
// 	loader: () => import("Routes/dashboard/news"),
// 	loading: () => <RctPageLoader />,
// });

const AsyncUserWidgetComponent = Loadable({
	loader: () => import("Routes/widgets/user-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncUserChartsComponent = Loadable({
	loader: () => import("Routes/widgets/charts-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncGeneralWidgetsComponent = Loadable({
	loader: () => import("Routes/widgets/general-widgets"),
	loading: () => <RctPageLoader />,
});

const AsyncPromoWidgetsComponent = Loadable({
	loader: () => import("Routes/widgets/promo-widgets"),
	loading: () => <RctPageLoader />,
});

// about us
const AsyncAboutUsComponent = Loadable({
	loader: () => import("Routes/about-us"),
	loading: () => <RctPageLoader />,
});

// chat app
const AsyncChatComponent = Loadable({
	loader: () => import("Routes/chat"),
	loading: () => <RctPageLoader />,
});

// mail app
const AsyncMailComponent = Loadable({
	loader: () => import("Routes/mail"),
	loading: () => <RctPageLoader />,
});

// todo app
const AsyncTodoComponent = Loadable({
	loader: () => import("Routes/todo"),
	loading: () => <RctPageLoader />,
});
const AsyncPurchaesinvoiceComponent = Loadable({
	loader: () => import("Routes/suppliers-management/purchases-invoices"),
	loading: () => <RctPageLoader />,
});
// gallery
// const AsyncGalleryComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/display-and-update-points-of-sale"),
// 	loading: () => <RctPageLoader />,
// });

// feedback
// const AsyncFeedbackComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/feedback"),
// 	loading: () => <RctPageLoader />,
// });

// report
// const AsyncReportComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/discount-and-extra-fees-of-points-of-sale"),
// 	loading: () => <RctPageLoader />,
// });

// const AsyncDiplayPointsOfSaleComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/display-points-of-sale"),
// 	loading: () => <RctPageLoader />,
// });

// regions-discount-and-extra-fees
// const AsyncRegionsDiscountAndExtraFeesComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/regions-discount-and-extra-fees"),
// 	loading: () => <RctPageLoader />,
// });

// faq
// const AsyncFaqComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/faq"),
// 	loading: () => <RctPageLoader />,
// });

// pricing
// const AsyncPricingComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/transactions-of-points-of-sale"),
// 	loading: () => <RctPageLoader />,
// });

// blank
// const AsyncBlankComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/blank"),
// 	loading: () => <RctPageLoader />,
// });

// google maps
// const AsyncGooleMapsComponent = Loadable({
// 	loader: () => import("Routes/maps/google-map"),
// 	loading: () => <RctPageLoader />,
// });

// google maps
// const AsyncLeafletMapComponent = Loadable({
// 	loader: () => import("Routes/maps/leaflet-map"),
// 	loading: () => <RctPageLoader />,
// });

//shop list
const AsyncShoplistComponent = Loadable({
	loader: () => import("Routes/suppliers-management/sold-cards"),
	loading: () => <RctPageLoader />,
});

//shop grid
const AsyncShopGridComponent = Loadable({
	loader: () => import("Routes/suppliers-management/available-cards"),
	loading: () => <RctPageLoader />,
});

//shop 
const AsyncShopComponent = Loadable({
	loader: () => import("Routes/suppliers-management/add-suppliers"),
	loading: () => <RctPageLoader />,
});

//cart 
const AsyncCartComponent = Loadable({
	loader: () => import("Routes/suppliers-management/all-suppliers"),
	loading: () => <RctPageLoader />,
});

//checkout 
const AsyncCheckoutComponent = Loadable({
	loader: () => import("Routes/suppliers-management/checkout"),
	loading: () => <RctPageLoader />,
});

//add inventory 
const AsyncAddInventoryComponent = Loadable({
	loader: () => import("Routes/inventory/add-products"),
	loading: () => <RctPageLoader />,
});

//update inventory 
const AsyncUpdateInventoryComponent = Loadable({
	loader: () => import("Routes/inventory/display-all-products"),
	loading: () => <RctPageLoader />,
});

//irredeemable  
const AsyncIrredeemableComponent = Loadable({
	loader: () => import("Routes/inventory/irredeemable"),
	loading: () => <RctPageLoader />,
});

//returns  
const AsyncReturnsComponent = Loadable({
	loader: () => import("Routes/inventory/returns"),
	loading: () => <RctPageLoader />,
});

//out of inventory  
const AsyncOutOfInventoryComponent = Loadable({
	loader: () => import("Routes/inventory/out-of-inventory"),
	loading: () => <RctPageLoader />,
});

//invoice
const AsyncInvoiceComponent = Loadable({
	loader: () => import("Routes/suppliers-management/received-bundles"),
	loading: () => <RctPageLoader />,
});

// react dragula
const AsyncReactDragulaComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/create-alaimtidad-balances"),
	loading: () => <RctPageLoader />,
});

const AsyncPresentAndPrintAlaimtidadBalancesComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/present-and-print-alaimtidad-balances"),
	loading: () => <RctPageLoader />,
});

// react dnd
const AsyncReactDndComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/balances-transfer"),
	loading: () => <RctPageLoader />,
});

// withdrawal-balances
const AsyncWithdrawalBalancesComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/withdrawal-balances"),
	loading: () => <RctPageLoader />,
})

// print-balances
const AsyncPrintBalancesComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/print-balances"),
	loading: () => <RctPageLoader />,
});

// themify icons
const AsyncThemifyIconsComponent = Loadable({
	loader: () => import("Routes/reports/alaimtidad-availability-report"),
	loading: () => <RctPageLoader />,
});

const AsyncAlaimtidadPrintedReportComponent = Loadable({
	loader: () => import("Routes/reports/alaimtidad-printed-report"),
	loading: () => <RctPageLoader />,
});

// Simple Line Icons
const AsyncSimpleLineIconsComponent = Loadable({
	loader: () => import("Routes/reports/companies-availability-report"),
	loading: () => <RctPageLoader />,
});

const AsyncCompaniesMinimumReportComponent = Loadable({
	loader: () => import("Routes/reports/companies-minimum-report"),
	loading: () => <RctPageLoader />,
});

// Material Icons
// const AsyncMaterialIconsComponent = Loadable({
// 	loader: () => import("Routes/reports/material-icons"),
// 	loading: () => <RctPageLoader />,
// });

// Basic Table


const Asynceditaccounts = Loadable({
	loader: () => import("Routes/accountingtree/editaccount"),
	loading: () => <RctPageLoader />,
});
const AsyncAccountingTree = Loadable({
	loader: () => import("Routes/accountingtree/addaccount"),
	loading: () => <RctPageLoader />,
});

const AsyncBasicTableComponent = Loadable({
	loader: () => import("Routes/system-settings/extra-transfer-fees"),
	loading: () => <RctPageLoader />,
});

// Basic Table
const AsyncDataTableComponent = Loadable({
	loader: () => import("Routes/system-settings/display-regions-and-areas"),
	loading: () => <RctPageLoader />,
});

// Responsive Table
const AsyncResponsiveTableComponent = Loadable({
	loader: () => import("Routes/system-settings/determine-the-exchange-rate"),
	loading: () => <RctPageLoader />,
});

const AsyncDisableSystemComponent = Loadable({
	loader: () => import("Routes/system-settings/disable-system"),
	loading: () => <RctPageLoader />,
});


const AsyncResetPasswordComponent = Loadable({
	loader: () => import("Routes/system-settings/reset-password"),
	loading: () => <RctPageLoader />,
});
const AsyncDailyMoneyReportComponent = Loadable({
	loader: () => import("Routes/reports/daily-money-report"),
	loading: () => <RctPageLoader />,
});
const AsyncWeeklyMoneyReportComponent = Loadable({
	loader: () => import("Routes/reports/weekly-money-report"),
	loading: () => <RctPageLoader />,
});
const Asyncdailybox = Loadable({
	loader: () => import("Routes/accountingtree/dailybox"),
	loading: () => <RctPageLoader />,
});
const Asyncaccountstatementclients = Loadable({
	loader: () => import("Routes/clients-managment/clientaccountstatment"),
	loading: () => <RctPageLoader />,
});
const Asyncreciepttypes = Loadable({
	loader: () => import("Routes/clients-managment/reciepttypes"),
	loading: () => <RctPageLoader />,
});
const Asyncclientsaccount = Loadable({
	loader: () => import("Routes/clients-managment/clientsaccount"),
	loading: () => <RctPageLoader />,
});
const AsyncAdvanceUIDateAndTimePickerComponent = Loadable({
	loader: () => import("Routes/representatives-management/add-representatives"),
	loading: () => <RctPageLoader />,
});

// worker
const AsyncAddWorkerComponent = Loadable({
	loader: () => import("Routes/worker-management/add-workers"),
	loading: () => <RctPageLoader />,
});
const AsyncShowUpdateWorkerComponent = Loadable({
	loader: () => import("Routes/worker-management/all-workers"),
	loading: () => <RctPageLoader />,
});

//advance components Tabs
const AsyncAdvanceUITabsComponent = Loadable({
	loader: () => import("Routes/representatives-management/all-representatives"),
	loading: () => <RctPageLoader />,
});
const Asyncallclients = Loadable({
	loader: () => import("Routes/clients-managment/all-clients"),
	loading: () => <RctPageLoader />,
});

//advance components Tabs
const Asyncaddclients = Loadable({
	loader: () => import("Routes/clients-managment/add-client"),
	loading: () => <RctPageLoader />,
});
const AsyncMonthlyMoneyReport = Loadable({
	loader: () => import("Routes/reports/monthly_money_report"),
	loading: () => <RctPageLoader />,
});

const AsyncAnnualMoneyReport = Loadable({
	loader: () => import("Routes/reports/annual_money_report"),
	loading: () => <RctPageLoader />,
});

const Asynccostcenter = Loadable({
	loader: () => import("Routes/system-settings/costcenter"),
	loading: () => <RctPageLoader />,
});

const AsyncCategories = Loadable({
	loader: () => import("Routes/inventory/categories"),
	loading: () => <RctPageLoader />,
});

const AsyncWarehouses = Loadable({
	loader: () => import("Routes/inventory/warehouses"),
	loading: () => <RctPageLoader />,
});
// Users List
// const AsyncUsersListComponent = Loadable({
// 	loader: () => import("Routes/users/user-list"),
// 	loading: () => <RctPageLoader />,
// });

// Users Profile
// const AsyncUserProfileComponent = Loadable({
// 	loader: () => import("Routes/users/user-profile"),
// 	loading: () => <RctPageLoader />,
// });

// Users Profile 1
// const AsyncUserProfile1Component = Loadable({
// 	loader: () => import("Routes/users/user-profile-1"),
// 	loading: () => <RctPageLoader />,
// });

// Users Management
// const AsyncUserManagementComponent = Loadable({
// 	loader: () => import("Routes/users/user-management"),
// 	loading: () => <RctPageLoader />,
// });

/*--------------- Charts ----------------*/

// Re charts
const AsyncRechartsComponent = Loadable({
	loader: () => import("Routes/charts/recharts"),
	loading: () => <RctPageLoader />,
});

// ReactChartsjs2
const AsyncReactChartsjs2Component = Loadable({
	loader: () => import("Routes/charts/react-chartjs2"),
	loading: () => <RctPageLoader />,
});

/*---------------------- Calendar -----------*/

// Basic Calendar
// const AsyncBasicCalendarComponent = Loadable({
// 	loader: () => import("Routes/calendar/BasicCalendar"),
// 	loading: () => <RctPageLoader />,
// });

// Cultures Calendar
// const AsyncCulturesComponent = Loadable({
// 	loader: () => import("Routes/calendar/Cultures"),
// 	loading: () => <RctPageLoader />,
// });

// Selectable Calendar
// const AsyncSelectableComponent = Loadable({
// 	loader: () => import("Routes/calendar/Selectable"),
// 	loading: () => <RctPageLoader />,
// });

// Custom Calendar
// const AsyncCustomComponent = Loadable({
// 	loader: () => import("Routes/calendar/Custom"),
// 	loading: () => <RctPageLoader />,
// });

/*---------------- Session ------------------*/

// Session Login
// const AsyncSessionLoginComponent = Loadable({
// 	loader: () => import("Routes/session/login"),
// 	loading: () => <RctPageLoader />,
// });

// Session Register
// const AsyncSessionRegisterComponent = Loadable({
// 	loader: () => import("Routes/session/register"),
// 	loading: () => <RctPageLoader />,
// });

// Session Lock Screen
// const AsyncSessionLockScreenComponent = Loadable({
// 	loader: () => import("Routes/session/lock-screen"),
// 	loading: () => <RctPageLoader />,
// });

// Session Forgot Password
// const AsyncSessionForgotPasswordComponent = Loadable({
// 	loader: () => import("Routes/session/forgot-password"),
// 	loading: () => <RctPageLoader />,
// });

// Session Page 404
// const AsyncSessionPage404Component = Loadable({
// 	loader: () => import("Routes/session/404"),
// 	loading: () => <RctPageLoader />,
// });

// Session Page 404
// const AsyncSessionPage500Component = Loadable({
// 	loader: () => import("Routes/session/500"),
// 	loading: () => <RctPageLoader />,
// });

// terms and condition
// const AsyncTermsConditionComponent = Loadable({
// 	loader: () => import("Routes/points-of-sale-management/terms-condition"),
// 	loading: () => <RctPageLoader />,
// });

/*---------------- Editor -------------------*/

// editor quill
// const AsyncQuillEditorComponent = Loadable({
// 	loader: () => import("Routes/editor/quill-editor"),
// 	loading: () => <RctPageLoader />,
// });

// editor Wysiwyg
// const AsyncWysiwygEditorComponent = Loadable({
// 	loader: () => import("Routes/editor/wysiwyg-editor"),
// 	loading: () => <RctPageLoader />,
// });

/*------------- Form Elemets -------------*/

// forms elements
// const AsyncFormElementsComponent = Loadable({
// 	loader: () => import("Routes/forms/form-elements"),
// 	loading: () => <RctPageLoader />,
// });

// forms TextField
// const AsyncTextFieldComponent = Loadable({
// 	loader: () => import("Routes/forms/material-text-field"),
// 	loading: () => <RctPageLoader />,
// });

// forms TextField
// const AsyncSelectListComponent = Loadable({
// 	loader: () => import("Routes/forms/select-list"),
// 	loading: () => <RctPageLoader />,
// });

/*------------------ UI Components ---------------*/

// components Alerts
// const AsyncUIAlertsComponent = Loadable({
// 	loader: () => import("Routes/dealers-management/add-dealers"),
// 	loading: () => <RctPageLoader />,
// });

// // components Appbar
// const AsyncUIAppbarComponent = Loadable({
// 	loader: () => import("Routes/dealers-management/dealers-operations"),
// 	loading: () => <RctPageLoader />,
// });

// 
// const AsyncDealersGiftPointsComponent = Loadable({
// 	loader: () => import("Routes/dealers-management/dealers-gift-points"),
// 	loading: () => <RctPageLoader />,
// });

// components BottomNavigation
// const AsyncUIBottomNavigationComponent = Loadable({
// 	loader: () => import("Routes/dealers-management/update-and-display-dealers"),
// 	loading: () => <RctPageLoader />,
// });

// components BottomNavigation
// const AsyncUIAvatarsComponent = Loadable({
// 	loader: () => import("Routes/components/avatar"),
// 	loading: () => <RctPageLoader />,
// });

// components Buttons
// const AsyncUIButtonsComponent = Loadable({
// 	loader: () => import("Routes/components/buttons"),
// 	loading: () => <RctPageLoader />,
// });

// components Badges
// const AsyncUIBadgesComponent = Loadable({
// 	loader: () => import("Routes/components/badges"),
// 	loading: () => <RctPageLoader />,
// });

// components CardMasonary
// const AsyncUICardMasonaryComponent = Loadable({
// 	loader: () => import("Routes/components/card-masonry"),
// 	loading: () => <RctPageLoader />,
// });

// components Cards
// const AsyncUICardsComponent = Loadable({
// 	loader: () => import("Routes/components/cards"),
// 	loading: () => <RctPageLoader />,
// });

// components Chips
// const AsyncUIChipsComponent = Loadable({
// 	loader: () => import("Routes/components/chip"),
// 	loading: () => <RctPageLoader />,
// });

// components Dialog
// const AsyncUIDialogComponent = Loadable({
// 	loader: () => import("Routes/components/dialog"),
// 	loading: () => <RctPageLoader />,
// });

// components Dividers
// const AsyncUIDividersComponent = Loadable({
// 	loader: () => import("Routes/components/dividers"),
// 	loading: () => <RctPageLoader />,
// });

// components Drawers
// const AsyncUIDrawersComponent = Loadable({
// 	loader: () => import("Routes/components/drawers"),
// 	loading: () => <RctPageLoader />,
// });

// components ExpansionPanel
// const AsyncUIExpansionPanelComponent = Loadable({
// 	loader: () => import("Routes/components/expansion-panel"),
// 	loading: () => <RctPageLoader />,
// });

// components Grid List
// const AsyncUIGridListComponent = Loadable({
// 	loader: () => import("Routes/components/grid-list"),
// 	loading: () => <RctPageLoader />,
// });

// components List
// const AsyncUIListComponent = Loadable({
// 	loader: () => import("Routes/components/list"),
// 	loading: () => <RctPageLoader />,
// });

// components Menu
// const AsyncUIMenuComponent = Loadable({
// 	loader: () => import("Routes/components/menu"),
// 	loading: () => <RctPageLoader />,
// });

// components Popover
// const AsyncUIPopoverComponent = Loadable({
// 	loader: () => import("Routes/components/popover"),
// 	loading: () => <RctPageLoader />,
// });

// components Progress
// const AsyncUIProgressComponent = Loadable({
// 	loader: () => import("Routes/components/progress"),
// 	loading: () => <RctPageLoader />,
// });

// components Snackbar
// const AsyncUISnackbarComponent = Loadable({
// 	loader: () => import("Routes/components/snackbar"),
// 	loading: () => <RctPageLoader />,
// });

// components SelectionControls
// const AsyncUISelectionControlsComponent = Loadable({
// 	loader: () => import("Routes/components/selection-controls"),
// 	loading: () => <RctPageLoader />,
// });

/*---------------- Advance UI Components -------------*/

// advance components DateAndTimePicker
// const AsyncAdvanceUIDateAndTimePickerComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/add-representatives"),
// 	loading: () => <RctPageLoader />,
// });

// advance components Tabs
// const AsyncAdvanceUITabsComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/all-representatives"),
// 	loading: () => <RctPageLoader />,
// });

// advance components Stepper
// const AsyncAdvanceUIStepperComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/stepper"),
// 	loading: () => <RctPageLoader />,
// });

// advance components NotificationComponent
// const AsyncAdvanceUINotificationComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/notification"),
// 	loading: () => <RctPageLoader />,
// });

// advance components SweetAlert
// const AsyncAdvanceUISweetAlertComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/representatives-transactions"),
// 	loading: () => <RctPageLoader />,
// });

// advance components autoComplete
// const AsyncAdvanceUIAutoCompleteComponent = Loadable({
// 	loader: () => import("Routes/representatives-management/autoComplete"),
// 	loading: () => <RctPageLoader />,
// });

const AsyncProfitAccountStatement = Loadable({
	loader: () => import("Routes/accounts/profit-account-statement"),
	loading: () => <RctPageLoader />,
});

const AsyncDealerProfitAccountStatement = Loadable({
	loader: () => import("Routes/accounts/dealer-profit-account-statement"),
	loading: () => <RctPageLoader />,
});

const AsyncAlaimtidadAccountStatement = Loadable({
	loader: () => import("Routes/accounts/alaimtidad-account-statement"),
	loading: () => <RctPageLoader />,
});

const AsyncRepresentativesAccountStatement = Loadable({
	loader: () => import("Routes/accounts/representatives-account-statement"),
	loading: () => <RctPageLoader />,
});

// const AsyncSuppliersAccountStatement = Loadable({
// 	loader: () => import("Routes/accounts/suppliers-account-statement"),
// 	loading: () => <RctPageLoader />,
// });

const AsyncDealersAccountStatement = Loadable({
	loader: () => import("Routes/accounts/dealers-account-statement"),
	loading: () => <RctPageLoader />,
});


const AsyncPointsOfSaleAccountStatement = Loadable({
	loader: () => import("Routes/accounts/point-of-sale-account-statement"),
	loading: () => <RctPageLoader />,
});

const AsyncWithdrawalAccountStatementComponent = Loadable({
	loader: () => import("Routes/alaimtidad-balances/withdrawal-account-statement"),
	loading: () => <RctPageLoader />,
});

// const AsyncArrearsComponent = Loadable({
// 	loader: () => import("Routes/accounts/arrears"),
// 	loading: () => <RctPageLoader />,
// });

// const AsyncPaymentsReceiptsComponent = Loadable({
// 	loader: () => import("Routes/accounts/payments-receipts"),
// 	loading: () => <RctPageLoader />,
// });

const AsyncAlaimtidadMovementOfBalancesComponent = Loadable({
	loader: () => import("Routes/accounts/alaimtidad-movement-of-balances"),
	loading: () => <RctPageLoader />,
});

const AsyncReceivableReceiptsComponent = Loadable({
	loader: () => import("Routes/accounts/receivable-receipts"),
	loading: () => <RctPageLoader />,
});

const AsyncCollectAndIssueReceivableReceiptsComponent = Loadable({
	loader: () => import("Routes/accounts/collect-and-issue-receivable-receipts"),
	loading: () => <RctPageLoader />,
});

// crm dashboard
// const AsyncCrmComponent = Loadable({
// 	loader: () => import("Routes/crm/dashboard"),
// 	loading: () => <RctPageLoader />,
// });

////////Supliers/////////////////////////////

const AsyncSupCardsComponent = Loadable({
	loader: () => import("Routes/SupCards/cards"),
	loading: () => <RctPageLoader />,
});
const AsyncsavedcardsComponents = Loadable({
	loader: () => import("Routes/SupCards/savedcards"),
	loading: () => <RctPageLoader />,
});

const AsyucpaidcardsComponents = Loadable({
	loader: () => import("Routes/SupCards/paidcards"),
	loading: () => <RctPageLoader />,
});

const AsyucfinishcardsComponents = Loadable({
	loader: () => import("Routes/SupCards/finishmoney"),
	loading: () => <RctPageLoader />,
});
 const AsyncioanmoneyComponents = Loadable({
	loader: () => import("Routes/SupCards/ioanmoney"),
	loading: () => <RctPageLoader />,
});

const AsyncUserChartsComponentDealer = Loadable({
    loader: () => import("Routes/Dealer/charts-widgets"),
    loading: () => <RctPageLoader />,
});

const AsyncDealerBalances = Loadable({
    loader: () => import("Routes/DealerPrintBalances"),
    loading: () => <RctPageLoader />,
});

const AsyncRequestfromEmtdadComponent = Loadable({
    loader: () => import("Routes/DealerRequestCredit/RequestfromEmtdad"),
    loading: () => <RctPageLoader />,
});
const AsyncRequestfromRepresentativeComponent = Loadable({
    loader: () => import("Routes/DealerRequestCredit/RequestfromRepresentative"),
    loading: () => <RctPageLoader />,
});
const AsyncDealerPrintBalancesComponent = Loadable({
	loader: () => import("Routes/DealerPrintCredit/Dealerprint-balances"),
	loading: () => <RctPageLoader />,
});
const AsynctotalAssesmentComponents = Loadable({
	loader: () => import("Routes/SupCards/totalAssesment"),
	loading: () => <RctPageLoader />,
});
const AsyncAddEmployeeComponent = Loadable({
	loader: () => import("Routes/employee-managment/addemployee"),
	loading: () => <RctPageLoader />,
});

const AsyncAllEmployeeComponent = Loadable({
	loader: () => import("Routes/employee-managment/allemployee"),
	loading: () => <RctPageLoader />,
});
const AsyncemployeetransactionComponent = Loadable({
loader: () => import("Routes/employee-managment/employeetransaction"),
loading: () => <RctPageLoader />,
});
const AsyncsalaryComponent = Loadable({
	loader: () => import("Routes/salarysetting/salarysettingemployees"),
	loading: () => <RctPageLoader />,
	});

	const AsyncSystemUser = Loadable({
		loader: () => import("Routes/system-settings/system-user"),
		loading: () => <RctPageLoader />,
	});
	const AsyncSystemUserUpdate = Loadable({
		loader: () => import("Routes/system-settings/system-user-update"),
		loading: () => <RctPageLoader />,
	});
	const AsyncOpenRestriction = Loadable({
		loader: () => import("Routes/restrictions/openedrestriction"),
		loading: () => <RctPageLoader />,
	});

	const Asyncdailyinvoice = Loadable({
		loader: () => import("Routes/invoices/dailyinvoice"),
		loading: () => <RctPageLoader />,
	});
	const Asyncpayinvoice = Loadable({
		loader: () => import("Routes/invoices/payinvoice"),
		loading: () => <RctPageLoader />,
	});
	const Asyncaddinvoice = Loadable({
		loader: () => import("Routes/invoices/addinvoice"),
		loading: () => <RctPageLoader />,
	});
	const Asyncallinvoice = Loadable({
		loader: () => import("Routes/invoices/allinvoices"),
		loading: () => <RctPageLoader />,
	});
	const Asyncreciepts = Loadable({
		loader: () => import("Routes/clients-managment/sales-invoices"),
		loading: () => <RctPageLoader />,
	});
// projects
const Asyncincomingrequests = Loadable({
	loader: () => import("Routes/requests/incomingrequests"),
	loading: () => <RctPageLoader />,
});
// // project detail
const Asyncemailconfirm = Loadable({
	loader: () => import("Routes/emailconfirmation/emailconfirm"),
	loading: () => <RctPageLoader />,
});

// // clients
const AsyncChangepassword = Loadable({
	loader: () => import("Routes/requests/changepassword"),
	loading: () => <RctPageLoader />,
});
// // reports
const AsyncCustomersrequest = Loadable({
	loader: () => import("Routes/requests/customersrequest"),
	loading: () => <RctPageLoader />,
});
const Asyncaddservices = Loadable({
	loader: () => import("Routes/servicemanagement/addservice"),
	loading: () => <RctPageLoader />,
});
// // reports
const Asyncallservices = Loadable({
	loader: () => import("Routes/servicemanagement/allservice"),
	loading: () => <RctPageLoader />,
});
export {
	AsyncUserWidgetComponent,
	AsyncUserChartsComponent,
	AsyncChangepassword,
	Asyncaddservices,
    Asyncallservices,
	AsyncGeneralWidgetsComponent,
	Asyncemailconfirm,
	AsyncPromoWidgetsComponent,
	AsyncAboutUsComponent,
	Asyncincomingrequests,
	AsyncCustomersrequest,
	AsyncChatComponent,
	AsyncMailComponent,
	AsyncTodoComponent,
	AsyncAllEmployeeComponent,
	AsyncemployeetransactionComponent,
	AsyncsalaryComponent,
	AsyncSystemUser,
	AsyncSystemUserUpdate,
	AsyncOpenRestriction,
	Asyncdailyinvoice,
    Asyncpayinvoice,
	Asyncaddinvoice,
	Asyncallinvoice,
	Asyncreciepts,
	// AsyncFeedbackComponent,
	// AsyncReportComponent,
	// AsyncDiplayPointsOfSaleComponent,
	// AsyncFaqComponent,
	// AsyncPricingComponent,
	// AsyncDealersGiftPointsComponent,
	// AsyncBlankComponent,
	AsyncProfitAccountStatement,
	// AsyncGooleMapsComponent,
	// AsyncLeafletMapComponent,
	AsyncAddEmployeeComponent,
	 AsyncShoplistComponent,
	 AsyncShopGridComponent,
	AsyncInvoiceComponent,
	AsyncReactDragulaComponent,
	AsyncPresentAndPrintAlaimtidadBalancesComponent,
	AsyncReactDndComponent,
	AsyncWithdrawalBalancesComponent,
	AsyncPrintBalancesComponent,
	AsyncThemifyIconsComponent,
	AsyncAlaimtidadPrintedReportComponent,
	AsyncSimpleLineIconsComponent,
	AsyncWarehouses,
	// AsyncMaterialIconsComponent,
	AsyncBasicTableComponent,
	AsyncAccountingTree,
	AsyncDataTableComponent,
	AsyncResponsiveTableComponent,
	Asynceditaccounts,
	AsyncPurchaesinvoiceComponent,
	AsyncAddWorkerComponent,
	AsyncShowUpdateWorkerComponent,
	// AsyncUsersListComponent,
	// AsyncUserProfileComponent,
	// AsyncUserProfile1Component,
	// AsyncUserManagementComponent,
	AsyncRechartsComponent,
	AsyncReactChartsjs2Component,
	AsyncAddInventoryComponent,
	AsyncUpdateInventoryComponent,
	AsyncIrredeemableComponent,
	AsyncReturnsComponent,
	AsyncOutOfInventoryComponent,
	// AsyncBasicCalendarComponent,
	// AsyncCulturesComponent,
	// AsyncSelectableComponent,
	// AsyncCustomComponent,
	// AsyncSessionLoginComponent,
	// AsyncSessionRegisterComponent,
	// AsyncSessionLockScreenComponent,
	// AsyncSessionForgotPasswordComponent,
	// AsyncSessionPage404Component,
	// AsyncSessionPage500Component,
	// AsyncTermsConditionComponent,
	// AsyncQuillEditorComponent,
	// AsyncWysiwygEditorComponent,
	// AsyncFormElementsComponent,
	// AsyncTextFieldComponent,
	// AsyncSelectListComponent,
	// AsyncUIAlertsComponent,
	// AsyncUIAppbarComponent,
	// AsyncUIBottomNavigationComponent,
	// AsyncUIAvatarsComponent,
	// AsyncUIButtonsComponent,
	// AsyncUIBadgesComponent,
	// AsyncUICardMasonaryComponent,
	// AsyncUICardsComponent,
	// AsyncUIChipsComponent,
	// AsyncUIDialogComponent,
	// AsyncUIDividersComponent,
	// AsyncUIDrawersComponent,
	// AsyncUIExpansionPanelComponent,
	// AsyncUIGridListComponent,
	// AsyncUIListComponent,
	// AsyncUIMenuComponent,
	// AsyncUIPopoverComponent,
	// AsyncUIProgressComponent,
	// AsyncUISnackbarComponent,
	// AsyncUISelectionControlsComponent,
	// AsyncAdvanceUIDateAndTimePickerComponent,
	AsyncResetPasswordComponent,
	AsyncMonthlyMoneyReport,
	AsyncAnnualMoneyReport,
	Asynccostcenter,
	AsyncWeeklyMoneyReportComponent,
	AsyncDailyMoneyReportComponent,
	Asyncallclients,
	Asyncaddclients,
	Asyncdailybox,
	Asyncaccountstatementclients,
	Asyncreciepttypes,
	Asyncclientsaccount,
	AsyncAdvanceUIDateAndTimePickerComponent,
	AsyncAdvanceUITabsComponent,
	AsyncCategories,
	// AsyncAdvanceUITabsComponent,
	// AsyncAdvanceUIStepperComponent,
	// AsyncAdvanceUINotificationComponent,
	// AsyncAdvanceUISweetAlertComponent,
	// AsyncAdvanceUIAutoCompleteComponent,
	 AsyncShopComponent,
	 AsyncCartComponent,
	 AsyncCheckoutComponent,
	// AsyncEcommerceDashboardComponent,
	// AsyncSaasDashboardComponent,
	// AsyncAgencyDashboardComponent,
	// AsyncNewsDashboardComponent,
	// AsyncRegionsDiscountAndExtraFeesComponent,
	AsyncRepresentativesAccountStatement,
	// AsyncSuppliersAccountStatement,
	AsyncDealersAccountStatement,
	AsyncPointsOfSaleAccountStatement,
	// AsyncArrearsComponent,
	// AsyncPaymentsReceiptsComponent,
    AsyncReceivableReceiptsComponent,
	AsyncCollectAndIssueReceivableReceiptsComponent,
	// AsyncPayableReceiptsComponent,
	AsyncDisableSystemComponent,
	AsyncAlaimtidadAccountStatement,
	AsyncAlaimtidadMovementOfBalancesComponent,
	AsyncCompaniesMinimumReportComponent,
	AsyncWithdrawalAccountStatementComponent,
	AsyncSupCardsComponent,
	AsyncsavedcardsComponents,
	AsyucpaidcardsComponents,
	AsyucfinishcardsComponents,
	AsyncioanmoneyComponents,
	AsyncUserChartsComponentDealer,
	AsyncDealerBalances,
    AsyncRequestfromEmtdadComponent,
	AsyncRequestfromRepresentativeComponent,
	AsyncDealerPrintBalancesComponent,
	AsynctotalAssesmentComponents,
	AsyncDealerProfitAccountStatement
	// AsyncCrmComponent,
	// AsyncProjectsComponent,
	// AsyncProjectDetailComponent,
	// AsyncClientsComponent,
	// AsyncReportsComponent
};
