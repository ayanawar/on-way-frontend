/**
 * Daily Sales Widget
 */
import React from 'react';

// chart
import SalesChart from 'Components/Charts/SalesChartXbox';

// constants
import ChartConfig from 'Constants/chart-config';

const DailySalesXbox = ({ label, chartdata, labels }) => (
	
	<div>
		<div className="p-20">
			<SalesChart
				label={label}
				chartdata={chartdata}
				labels={labels}
				borderColor={ChartConfig.color.info}
				pointBackgroundColor={ChartConfig.color.info}
				height={125}
				pointBorderColor={ChartConfig.color.white}
				borderWidth={4}
			/>
		</div>
	</div>
);

export default DailySalesXbox;
