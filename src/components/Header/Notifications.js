/**
 * Notification Component
 */
 import React, { useEffect, useState } from 'react';
 import { useForm } from 'react-hook-form';
import { Scrollbars } from 'react-custom-scrollbars';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
// import Button from '@material-ui/core/Button';
import { Badge } from 'reactstrap';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import io from 'socket.io-client';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import USER_TOKEN from '../../constants/Token';

export default function Notifications (){

   // const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
      notifications: null,
      count: ''

    });

    useEffect(() => {

	

		const socket = io('http://localhost:3000', {transports: ['websocket'], upgrade: false});
		socket.on('message', (msg)=>{
			console.log("called");
			console.log(msg);
         getNotifications(socket)
			// setTimeout(function () { location.reload()}, 500)
		})

		socket.on('removeRequest', (msg)=>{
			console.log("removeRequest");
			console.log(msg);

         setTimeout(function () { 

            let data = { user_id: localStorage.getItem("service_provider_location_id") }
            socket.emit('provider', data)
            console.log("prov location id", localStorage.getItem("service_provider_location_id"));
      
            axios.post('http://localhost:3000/getrequestedservices', { service_provider_id: data.user_id }, USER_TOKEN).then(response => {
      
               if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                  cookies.remove('UserToken', { path: '/' })
                  window.location.href = "/signin";
               }
               else {
                  //  console.log(response2.data.message);
                  //     let workers = [];
                  // for (let i = 0; i < response.data.length; i ++) {
                  //         console.log("eneterd to set workers");
                  //         let worker ={
                  //             request_id: response.data[i].request_id,
                  //             service_name: response.data[i].Request.Service.service_name,
                  //             request_status: response.data[i].Request.request_status,
                  //             customer_id: response.data[i].Request.customer_id,
                  //             created_at: response.data[i].created_at
                  //         }
                  //         console.log("done with worker");
                  //         workers.push(worker);
      
                  // }
                  console.log("requests", response.data.length);
      
                  //  console.log("workers", workers);
                  setState({
                     count: response.data.length
                     // representatives: workers,
                     // availableWorker:response2.data.message,
                     // hiddenStatusForm: true,
                     // hiddenDebitLimitForm: true,
                     // disabled: false,
                     // service_provider_location_id: data.user_id,
      
                  })
               }
            })
               .catch(error => {
                  console.log("erre", error);
                  if (error.response.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     });
                  }
               });
         
         }, 1000)
         // getNotifications(socket)
         


			// setTimeout(function () { location.reload()}, 500)
		})

      getNotifications(socket)
      let data = { user_id: localStorage.getItem("service_provider_location_id") }
      socket.emit('provider', data)
      console.log("prov location id", localStorage.getItem("service_provider_location_id"));

      axios.post('http://localhost:3000/getrequestedservices', { service_provider_id: data.user_id }, USER_TOKEN).then(response => {

         if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
            cookies.remove('UserToken', { path: '/' })
            window.location.href = "/signin";
         }
         else {
            //  console.log(response2.data.message);
            //     let workers = [];
            // for (let i = 0; i < response.data.length; i ++) {
            //         console.log("eneterd to set workers");
            //         let worker ={
            //             request_id: response.data[i].request_id,
            //             service_name: response.data[i].Request.Service.service_name,
            //             request_status: response.data[i].Request.request_status,
            //             customer_id: response.data[i].Request.customer_id,
            //             created_at: response.data[i].created_at
            //         }
            //         console.log("done with worker");
            //         workers.push(worker);

            // }
            console.log("requests", response.data.length);

            //  console.log("workers", workers);
            setState({
               count: response.data.length
               // representatives: workers,
               // availableWorker:response2.data.message,
               // hiddenStatusForm: true,
               // hiddenDebitLimitForm: true,
               // disabled: false,
               // service_provider_location_id: data.user_id,

            })
         }
      })
         .catch(error => {
            console.log("erre", error);
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
      // api.get('notifications.js')
      //    .then((response) => {
      //       this.setState({ notifications: response.data });
      //    })
      //    .catch(error => {
      //       // console.log(error);
      //    })

     
	}, []);


   // componentDidMount() {
	// 	const socket = io('http://localhost:3000', {transports: ['websocket'], upgrade: false});

   //    this.getNotifications();

   //    socket.on('message', (msg)=>{
	// 		console.log("called");
	// 		console.log(msg);
	// 		this.getNotifications();
	// 	})
   // }

   // get notifications
   const getNotifications = (socket) => {

      let data = { user_id: localStorage.getItem("service_provider_location_id") }
      socket.emit('provider', data)
      console.log("prov location id", localStorage.getItem("service_provider_location_id"));

      axios.post('http://localhost:3000/getrequestedservices', { service_provider_id: data.user_id }, USER_TOKEN).then(response => {

         if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
            cookies.remove('UserToken', { path: '/' })
            window.location.href = "/signin";
         }
         else {
            //  console.log(response2.data.message);
            //     let workers = [];
            // for (let i = 0; i < response.data.length; i ++) {
            //         console.log("eneterd to set workers");
            //         let worker ={
            //             request_id: response.data[i].request_id,
            //             service_name: response.data[i].Request.Service.service_name,
            //             request_status: response.data[i].Request.request_status,
            //             customer_id: response.data[i].Request.customer_id,
            //             created_at: response.data[i].created_at
            //         }
            //         console.log("done with worker");
            //         workers.push(worker);

            // }
            console.log("requests", response.data.length);

            //  console.log("workers", workers);
            setState({
               count: response.data.length
               // representatives: workers,
               // availableWorker:response2.data.message,
               // hiddenStatusForm: true,
               // hiddenDebitLimitForm: true,
               // disabled: false,
               // service_provider_location_id: data.user_id,

            })
         }
      })
         .catch(error => {
            console.log("erre", error);
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
		

   }

   // return(
      const { notifications } = state;
      return (
         <UncontrolledDropdown nav className="list-inline-item notification-dropdown">
            <DropdownToggle nav className="p-0">
               <Tooltip title="Notifications" placement="bottom">
                  <IconButton className="shake" aria-label="bell"  onClick={
							()=>{
								console.log("clicked");
								window.location.href = "/app/requests/incomingrequests";
							}
						}>
                     <i className="zmdi zmdi-notifications-active"></i>
                     <Badge color="danger" className="badge-xs badge-top-right rct-notify">{state.count == 0 ? '': state.count}</Badge>
                  </IconButton>
               </Tooltip>
            </DropdownToggle>
            <DropdownMenu right>
               {/* <div className="dropdown-content"> */}
               {/* <div className="dropdown-top d-flex justify-content-between rounded-top bg-primary"> */}
               {/* <span className="text-white font-weight-bold">
                        <IntlMessages id="widgets.recentNotifications" />
                     </span> */}
               {/* <Badge color="warning">1 NEW</Badge> */}
               {/* </div> */}
               {/* <Scrollbars className="rct-scroll" autoHeight autoHeightMin={100} autoHeightMax={280}>
                     <ul className="list-unstyled dropdown-list"> */}
               {/* {notifications && notifications.map((notification, key) => (
                           <li key={key}>
                              <div className="media">
                                 <div className="mr-10">
                                    <img src={notification.userAvatar} alt="user profile" className="media-object rounded-circle" width="50" height="50" />
                                 </div>
                                 <div className="media-body pt-5">
                                    <div className="d-flex justify-content-between">
                                       <h5 className="mb-5 text-primary">{notification.userName}</h5>
                                       <span className="text-muted fs-12">{notification.date}</span>
                                    </div>
                                    <span className="text-muted fs-12 d-block">{notification.notification}</span>
                                    <Button className="btn-xs mr-10">
                                       <i className="zmdi zmdi-mail-reply mr-2"></i> <IntlMessages id="button.reply" />
                                    </Button>
                                    <Button className="btn-xs">
                                       <i className="zmdi zmdi-thumb-up mr-2"></i> <IntlMessages id="button.like" />
                                    </Button>
                                 </div>
                              </div>
                           </li>
                        ))} */}
               {/* </ul>
                  </Scrollbars> */}
               {/* </div> */}
               {/* <div className="dropdown-foot p-2 bg-white rounded-bottom">
                  <Button
                     variant="contained"
                     color="primary"
                     className="mr-10 btn-xs bg-primary"
                  >
                     <IntlMessages id="button.viewAll" />
                  </Button>
               </div> */}
            </DropdownMenu>
         </UncontrolledDropdown>
      );
   // )
}

// export default Notifications;
