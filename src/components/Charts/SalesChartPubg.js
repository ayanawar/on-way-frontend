/**
 * Sales Chart Component
 */

import { Line } from 'react-chartjs-2';

// chart config file
import ChartConfig from 'Constants/chart-config';


import React, { useEffect, useState } from 'react';
import axios from 'axios';

import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
export default function HomePage(props) {
	const history = useHistory();
	const [state, setState] = useState({
		chartDataForPubg:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				    data:[],
                    fill: false,
                    label: "دينار عراقي",
                    lineTension: 0,
                    fillOpacity: 0.3,
                    borderColor: "#00D0BD",
                    borderWidth: 3,
                    pointBorderColor: "transparent",
                    pointBackgroundColor: "#00D0BD",
                    pointBorderWidth: 3,
                    pointRadius: 3,
                    pointHoverBackgroundColor: "red",
                    pointHoverBorderColor: "white",
                    pointHoverBorderWidth: 4,
                    pointHoverRadius: 7,
				}
			]
		},
	});
	
	useEffect(() => {
        axios.post('http://localhost:8000/getMonthlySalesGraphByCompanyID',{ "company_id": 5 },USER_TOKEN).then(res5 => {
        
            let chartPerMonthForPubg = {...state.chartDataForPubg};
            
            res5 = res5.data.monthlySalesGraphForCompany.map(i => {
                chartPerMonthForPubg.labels.push(i.day);
                chartPerMonthForPubg.datasets[0].data.push(i.company_cards_count);
            });

            setState({ 
                chartDataForPubg: { ...state.chartDataForPubg, chartPerMonthForPubg },  	 	
            });

        }).catch(function (error) {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        })	
	},[]);
    
        const { labels, label, borderColor, chartdata, pointBackgroundColor, height, pointBorderColor, borderWidth } = props;
        const data = (canvas) => {
            const ctx = canvas.getContext("2d");
            const _stroke = ctx.stroke;
            ctx.stroke = function () {
                ctx.save();
                ctx.shadowColor = ChartConfig.shadowColor;
                ctx.shadowBlur = 13;
                ctx.shadowOffsetX = 30;
                ctx.shadowOffsetY = 12;
                _stroke.apply(this, arguments);
                ctx.restore();
            };
            return {
                labels: labels,
                datasets: [
                    {
                        label: label,
                        fill: false,
                        lineTension: 0,
                        fillOpacity: 0.3,
                        borderColor: borderColor,
                        borderWidth: borderWidth,
                        pointBorderColor: pointBorderColor,
                        pointBackgroundColor: pointBackgroundColor,
                        pointBorderWidth: 3,
                        pointRadius: 6,
                        pointHoverBackgroundColor: pointBackgroundColor,
                        pointHoverBorderColor: pointBorderColor,
                        pointHoverBorderWidth: 4,
                        pointHoverRadius: 7,
                        data: chartdata,
                    }
                ]
            }
        }
        const options = {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    display: true,
                    ticks: {
                        min: 0
                    },
                    gridLines: {
                        display: true,
                        drawBorder: false
                    }
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 0,
                        beginAtZero: true
                    }
                }]
            },

        };
        return (
            <React.Fragment>
                <ToastContainer />
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
            <Line data={state.chartDataForPubg} options={options} height={height} />
            : (
				history.push("/access-denied")
               )
            } 
            </React.Fragment>
        );
}
