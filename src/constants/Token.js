import Cookies from 'universal-cookie';
const cookies = new Cookies();
const Token = cookies.get('UserToken');

// let PURE_TOKEN = Token.split(".");
// PURE_TOKEN.pop();
// PURE_TOKEN.join(".");
// console.log(Token);

const USER_TOKEN = {
    headers: { "x-access-token": `${Token}` }
};

// console.log("config t ", USER_TOKEN);
export default USER_TOKEN;