import image from '../assets/img/access_denied.jpg';
import React from 'react';
import { Helmet } from "react-helmet";
import { Link } from 'react-router-dom';
function index() {

    return (
        <React.Fragment>
			<section className="error-area">
				<Helmet>
					<title>غير مسموح بالدخول</title>
					<meta name="description" content="غير مسموح بالدخول" />
				</Helmet>
				<div className="container">
					<img src={image} className="access-denied-image" alt="access_denied" />
					
				</div>
				<br />
				<div className="container" >
					<Link to="/signin">
						<button style={{ backgroundColor: "#599A5F", borderRadius: "15px", 
										 fontSize:"17px", fontWeight: "bold", color: "white",
										 marginTop: "30px", padding: "10px" }}>
						Redirect for Login page
						</button>
					</Link>
				</div>
			</section>
		</React.Fragment>
    )
}

export default index;


