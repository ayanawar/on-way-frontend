/**
 * Signin Firebase
 */

import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import Logo from '../assets/img/Logo.png'
// import { Fab } from "@material-ui/core";
import io from 'socket.io-client';

// const socket = io('http://localhost:3000');
// components
import {
   SessionSlider
} from 'Components/Widgets';

// app config
import AppConfig from 'Constants/AppConfig';

// redux action
// import {
//    signinUserInFirebase,
//    signinUserWithFacebook,
//    signinUserWithGoogle,
//    signinUserWithGithub,
//    signinUserWithTwitter
// } from 'Actions';

//Auth File
import Auth from '../Auth/Auth';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import IntlMessages from 'Util/IntlMessages';
import { useForm } from 'react-hook-form';

const auth = new Auth();
const cookies = new Cookies();

export default function RctPickers(props) {
   const { register, handleSubmit, errors } = useForm();
   const [state, setState] = useState({
      user_email: '',
      user_password: '',
      device_identifier: '0',
      counter: 0,
   });

   // state = {

   // }

   /**
    * On User Login
    */
   const onUserLogin = () => {
      console.log("state", state.user_email, state.user_passwords);
      const socket = io('http://localhost:3000', { transports: ['websocket'], upgrade: false });

      if (state.user_email != '' && state.user_password != '') {
         // props.signinUserInFirebase(state, props.history);
         console.log("entered for login", state);
         axios.post('http://localhost:3000/login', state).then(res => {
            console.log("response", res);
            // cookies.set('UserToken', `${res.data.usertoken}.${uuidv4()}`, { path: '/', expires: new Date(Date.now() + 2592000) });


            // const cu = cookies.get('UserToken') 
            // let rr = cu.split(".");
            // console.log(rr);
            // console.log("tokennn ", USER_TOKEN);
            // console.log("response ",res);
            // console.log("token pop ",rr.pop());
            // console.log("token join ",rr.join("."));
            // window.location.reload(false);   

            // if(res.data.user_type_id == "2") {
            //    localStorage.setItem("user_type_name", "الوكيل");
            //    localStorage.setItem("user_id", "user-id");
            //    localStorage.setItem("userIdInUsers", res.data.userIdInUsers);
            //    localStorage.setItem("userId", res.data.userId);     
            //    localStorage.setItem("user_type_id", res.data.user_type_id);
            //    localStorage.setItem("userFirstName", res.data.userFirstName);
            //    localStorage.setItem("usermiddleName", res.data.usermiddleName);
            //    localStorage.setItem("userLastName", res.data.userLastName);
            //    localStorage.setItem("phoneNumber", res.data.phoneNumber);
            //    localStorage.setItem("area", res.data.area);
            //    localStorage.setItem("region", res.data.region);
            //    localStorage.setItem("regionId", res.data.regionId);
            //    localStorage.setItem("virtualMoneyBalance", res.data.virtualMoneyBalance);
            //    cookies.set('UserToken', `${res.data.usertoken}`, { path: '/', expires: new Date(Date.now() + (43200)), maxAge: (43200)  });
            //    toast.success('تم تسجيل الدخول بنجاح', {
            //       position: "top-center",
            //       autoClose: 4000,
            //       hideProgressBar: false,
            //       closeOnClick: true,
            //       pauseOnHover: true,
            //       draggable: true
            //    });
            //    window.location.href = "/app/Dealer/charts";
            //         } 
            // else if(res.data.user_type_id === "4") {
            //    localStorage.setItem("user_type_name", "المورد");
            //    localStorage.setItem("user_id", "user-id");
            //    localStorage.setItem("userIdInUsers", res.data.userIdInUsers);
            //    localStorage.setItem("userId", res.data.userId);     
            //    localStorage.setItem("user_type_id", res.data.user_type_id);
            //    localStorage.setItem("userFirstName", res.data.userFirstName);
            //    localStorage.setItem("usermiddleName", res.data.usermiddleName);
            //    localStorage.setItem("userLastName", res.data.userLastName);
            //    localStorage.setItem("phoneNumber", res.data.phoneNumber);
            //    localStorage.setItem("area", res.data.area);
            //    localStorage.setItem("region", res.data.region);
            //    localStorage.setItem("virtualMoneyBalance", res.data.virtualMoneyBalance);
            //    cookies.set('UserToken', `${res.data.usertoken}`, { path: '/', expires: new Date(Date.now() + (43200)), maxAge: (43200)  });
            //    toast.success('تم تسجيل الدخول بنجاح', {
            //       position: "top-center",
            //       autoClose: 4000,
            //       hideProgressBar: false,
            //       closeOnClick: true,
            //       pauseOnHover: true,
            //       draggable: true
            //    });
            //    window.location.href = "/app/SupCards/savedcards";
            // } 

            // else
            if (res.data.user_type_id == "2") {
               console.log("uuuuu");
               localStorage.setItem("user_type_name", "Service Provider");
               localStorage.setItem("user_id", res.data.user_id);
               //  localStorage.setItem("userIdInUsers", res.data.userIdInUsers);
               // localStorage.setItem("userId", res.data.userId);     
               localStorage.setItem("user_type_id", res.data.user_type_id);
               localStorage.setItem("user_first_name", res.data.user_first_name);
               localStorage.setItem("user_last_name", res.data.user_last_name);
               localStorage.setItem("user_email", res.data.user_email);
               localStorage.setItem("phone_number", res.data.phone_number);
               localStorage.setItem("service_provider_id", res.data.ServiceProvider.service_provider_id);
               localStorage.setItem("company_name", res.data.ServiceProvider.company_name);
               localStorage.setItem("company_type", res.data.ServiceProvider.company_type);
               localStorage.setItem("contact_role", res.data.ServiceProvider.contact_role);
               localStorage.setItem("service_provider_location_id", res.data.ServiceProvider.ServiceProviderLocation.service_provider_location_id);
               localStorage.setItem("city", res.data.ServiceProvider.ServiceProviderLocation.city);
               localStorage.setItem("area", res.data.ServiceProvider.ServiceProviderLocation.area);
               localStorage.setItem("street", res.data.ServiceProvider.ServiceProviderLocation.street);
               localStorage.setItem("status", res.data.ServiceProvider.ServiceProviderLocation.status);
               localStorage.setItem("token", res.data.token);
               // localStorage.setItem("virtualMoneyBalance", res.data.virtualMoneyBalance);

               let data = { user_id: res.data.ServiceProvider.ServiceProviderLocation.service_provider_location_id }
               socket.emit('provider', data)
               cookies.set('UserToken', `${res.data.token}`, { path: '/', expires: new Date(Date.now() + (43200)), maxAge: (43200) });
               toast.success('Logged In Successfully', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
               // window.location.href = "/app/worker-management/add-workers";
               window.location.href = " /app/home-page";
            } else if (res.data === "Token UnAuthorized") {
               toast.error('Wrong Username Or Password', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
            else {
               // window.location.href = "/signin";
               // console.log('response',res);
               toast.error('Error In Logging', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }


         }).catch(error => {
            console.log(error);
            if (error.response.data.message === "incorrect username or password") {
               setState({ ...state, counter: state.counter + 1 });
               if (state.counter >= 3) {
                  axios.post('http://localhost:8000/suspenduser', { "user_email": state.user_email }).then((res) => {
                     toast.error('تم إيقاف الحساب برجاء الرجوع للإدارة لإعادة تفعيلك من جديد', {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     });
                  }).catch((err) => {

                  })
               } else {
                  toast.error('Please check that password or email is correctly entered', {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  });
               }
            } else if (error.response.data.message === "user account not activated") {
               toast.error('الحساب موقوف برجاء التواصل مع الإدارة لإعادة تفعيل الحساب من جديد ', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
            else if (error.response.data === "Token UnAuthorized") {
               toast.error('النظام موقوف مؤقتاً من قبل الإدارة ', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
            else if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
            else {
               toast.error('Please Verify your Email', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
      }
   }

   /**
    * On User Sign Up
    */
   const onUserSignUp = () => {
      props.history.push('/');
   }

   const handleFields = ({ target }) => {
      setState({ ...state, [target.name]: target.value });
   };
   const MyFunction = () => {
      console.log("link clicked");
   }

   //Auth0 Login
   // loginAuth0() {
   //    auth.login();
   // }

   // render() {
   const { user_email, user_password } = state;
   const { loading } = props;
   // console.log(state);
   return (
      console.log("state", state),

      <QueueAnim type="bottom" duration={2000}>
         <div className="rct-session-wrapper">
            <ToastContainer />
            {loading &&
               <LinearProgress />
            }
            <AppBar position="static" className="session-header">
               <Toolbar>
                  <div className="container">
                     <div className="d-flex justify-content-between">
                        <div className="session-logo">
                           <Link to="/">
                              <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid" width="110" height="35" />
                           </Link>
                        </div>
                        <div>
                           <a className="mr-15" onClick={() => onUserSignUp()}>Create New account?</a>
                           <Button variant="contained" className="btn-light" onClick={() => onUserSignUp()}>Sign Up</Button>
                        </div>
                     </div>
                  </div>
               </Toolbar>
            </AppBar>
            {/* className="session-inner-wrapper" white background */}
            <div >

               <div className="container" style={{ marginLeft: '500px' }}>

                  <div className="row row-eq-height">

                     <div style= {{marginTop: "-80px"}} className="col-sm-5 col-md-5 col-lg-4">
                        <div className="session-body">
                           <div className="session-head mb-30" style={{ marginTop: '180px' }}>
                              {/* <h2 style={{color: "#599A5F", fontSize: "55px", textAlign: "center !important"}}><IntlMessages id="form.login" /></h2> */}
                              {/* <h2 className="font-weight-bold">Get started with {AppConfig.brandName}</h2> */}
                              {/* <p className="mb-0">Most powerful ReactJS admin panel</p> */}
                              <div style={{ alignContent: 'center' }}>
                                 <img style={{ width: '150px' }} src={Logo}></img>

                              </div>
                           </div>
                           <Form onSubmit={handleSubmit(onUserLogin)} style={{}}>
                              <div  >
                                 <label className="mt-5" style={{ fontSize: "14px", fontWeight: "400", lineHeight: "1.42857", textAlign: "center !important", color: '#000000' }}>
                                    Email
                                 </label>

                                 <FormGroup className="has-wrapper" style={{ marginBottom: "40px", textAlign: "center !important" }}>
                                    <div style={{
                                       borderWidth: "0px 2px 2px 0px",
                                       lineHeight: "0px",
                                       borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                    }}>
                                       <input
                                          type="email"
                                          // value={user_phonenumber}
                                          name="user_email"
                                          id="user-mail"
                                          className="form-control p-3"
                                          onChange={handleFields}
                                          style={{ fontSize: "17px", backgroundColor: 'white' }}
                                          ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}
                                       />
                                    </div>
                                    <span className="errors">
                                       {errors.user_email && errors.user_email.type === 'required' &&
                                          <IntlMessages id="form.requiredError" />}
                                       {errors.user_email && errors.user_email.type === 'pattern' &&
                                          <IntlMessages id="form.numbersOnlyErrorError" />}
                                       {/* {errors.user_email && errors.user_email.type === 'minLength' &&
                                          <IntlMessages id="form.minPhoneLengthError" />}
                                       {errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
                                          <IntlMessages id="form.minPhoneLengthError" />} */}
                                    </span>
                                    {/* <span className="has-icon"><i className="ti-mobile"></i></span> */}
                                 </FormGroup>
                              </div>
                              <label className="mt-5" style={{ color: '#000000', fontSize: "14px", fontWeight: "400", lineHeight: "1.42857" }}><IntlMessages id="form.password" /></label>
                              <FormGroup className="has-wrapper">
                                 <div style={{
                                    borderWidth: "0px 2px 2px 0px",
                                    lineHeight: "0px",
                                    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                 }}>
                                    <input
                                       // value={user_password}
                                       type="Password"
                                       name="user_password"
                                       id="pwd"
                                       className="form-control p-3"
                                       onChange={handleFields}
                                       style={{ fontSize: "17px", backgroundColor: 'white' }}
                                       ref={register({
                                          required: true
                                          , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/
                                       })}
                                    />
                                 </div>
                                 <span className="errors">
                                    {errors.user_password && errors.user_password.type === 'required' &&
                                       <IntlMessages id="form.requiredError" />}
                                    {errors.user_password && errors.user_password.type === 'pattern' &&
                                       <IntlMessages id="form.passwordMinLengthError" />}
                                 </span>
                                 {/* <span className="has-icon"><i className="ti-lock"></i></span> */}
                              </FormGroup>
                              <FormGroup className="mb-15">
                                 <a href="/forgotpassword" class="link-primary">Forgot Your Password?</a>
                                 {/* <Button
                                    type="submit"
                                    color="primary"
                                    className="btn-block w-50"
                                    variant="contained"
                                    size="large"
                                    onClick={() => onUserLogin()}
                                    style={{
                                       position: "relative", margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                       fontSize: "17px", fontWeight: "bold", height: '50px', marginTop: "30px"
                                    }}
                                 >
                                    <IntlMessages id="form.login" />
                                 </Button> */}
                               

                              </FormGroup>
                              <FormGroup className="mb-15">
                              <button type="submit" onClick={() => onUserLogin()} class="btn btn-primary btn-block">
                                 <IntlMessages id="form.login" />
                                 </button>
                              </FormGroup>
                              {/* <FormGroup className="mb-15">
                                    <Button
                                       variant="contained"
                                       className="btn-info btn-block text-white w-100"
                                       size="large"
                                       onClick={() => loginAuth0()}
                                    >
                                       Sign In With Auth0
                            			</Button>
                                 </FormGroup> */}
                           </Form>
                           {/* <p className="mb-20">or sign in with</p>
                              <Fab size="small" variant="round" className="btn-facebook mr-15 mb-20 text-white"
                                 onClick={() => props.signinUserWithFacebook(props.history)}
                              >
                                 <i className="zmdi zmdi-facebook"></i>
                              </Fab>
                              <Fab size="small" variant="round" className="btn-google mr-15 mb-20 text-white"
                                 onClick={() => props.signinUserWithGoogle(props.history)}
                              >
                                 <i className="zmdi zmdi-google"></i>
                              </Fab>
                              <Fab size="small" variant="round" className="btn-twitter mr-15 mb-20 text-white"
                                 onClick={() => props.signinUserWithTwitter(props.history)}
                              >
                                 <i className="zmdi zmdi-twitter"></i>
                              </Fab>
                              <Fab size="small" variant="round" className="btn-instagram mr-15 mb-20 text-white"
                                 onClick={() => props.signinUserWithGithub(props.history)}
                              >
                                 <i className="zmdi zmdi-github-alt"></i>
                              </Fab> */}
                           {/* <p className="text-muted">By signing up you agree to {AppConfig.brandName}</p>
                              <p className="mb-0"><a target="_blank" href="#/terms-condition" className="text-muted">Terms of Service</a></p> */}
                        </div>
                     </div>
                     {/* <div className="col-sm-5 col-md-5 col-lg-4">
                           <SessionSlider />
                        </div> */}
                  </div>
               </div>
            </div>
         </div>
      </QueueAnim>
   );
   // }
}

// // map state to props
// const mapStateToProps = ({ authUser }) => {
//    const { user, loading } = authUser;
//    return { user, loading }
// }

// export default connect(mapStateToProps, {
//    signinUserInFirebase,
//    signinUserWithFacebook,
//    signinUserWithGoogle,
//    signinUserWithGithub,
//    signinUserWithTwitter
// })(Signin);
