/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

// rct theme provider
import RctThemeProvider from './RctThemeProvider';

//Horizontal Layout
import HorizontalLayout from './HorizontalLayout';

//Agency Layout
import AgencyLayout from './AgencyLayout';

//Main App
import RctDefaultLayout from './DefaultLayout';

// boxed layout
import RctBoxedLayout from './RctBoxedLayout';

// CRM layout
// import CRMLayout from './CRMLayout';

// app signin
import AppSignIn from './SigninFirebase';
import EmailConfirmation from './EmailConfirmation';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './resetPassword';

import AccessDenied from './AccessDenied';
// import MayarTestComponent from './MayarTestComponent';

// async components
import {
   // AsyncSessionLoginComponent,
   // AsyncSessionRegisterComponent,
   // AsyncSessionLockScreenComponent,
   // AsyncSessionForgotPasswordComponent,
   // AsyncSessionPage404Component,
   // AsyncSessionPage500Component,
   // AsyncTermsConditionComponent
} from 'Components/AsyncComponent/AsyncComponent';

//Auth0
import Auth from '../Auth/Auth';

// callback component
import Callback from "Components/Callback/Callback";
import PURE_TOKEN_USER_TYPE from '../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../constants/TokenPhoneNum';

//Auth0 Handle Authentication
const auth = new Auth();

const handleAuthentication = ({ location }) => {
   if (/access_token|id_token|error/.test(location.hash)) {
      auth.handleAuthentication();
   }
}

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, authUser, ...rest }) =>
   <Route
      {...rest}
      render={props =>
         authUser
            ? <Component {...props} />
            : <Redirect
               to={{
                  pathname: '/access-denied',
                  state: { from: props.location }
               }}
            />}
   />;

class App extends Component {
   render() {
      const { location, match, user } = this.props;
      if (location.pathname === '/') {
         if (user === null) {
            return (<Redirect to={'/signin'} />);
            // return (<Redirect to={'/homepage'} />);
         } else {
            if(location.pathname === '/Dealer')
            {
               return (<Redirect to={'/app/widgets/charts'} />);
            }
            else
            {
            return (<Redirect to={'/app/home-page'} />);
            }
         }
      }
      return (
         <React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") || PURE_TOKEN_USER_TYPE === 4 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") || PURE_TOKEN_USER_TYPE === 2 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
         <RctThemeProvider>
            <NotificationContainer />
            <InitialPath
               path={`${match.url}app`}
               authUser={user}
               component={RctDefaultLayout}
            />
            <Route path="/access-denied" component={AccessDenied} />
            {/* <Route path="/homepage" component={MayarTestComponent} /> */}
            <Route path="/horizontal" component={HorizontalLayout} />
            <Route path="/agency" component={AgencyLayout} />
            <Route path="/boxed" component={RctBoxedLayout} />
            {/* <Route path="/dashboard" component={CRMLayout} /> */}
            <Route path="/signin" component={AppSignIn} />
            <Route path="/EmailConfirmation/" component={EmailConfirmation} />
            <Route path="/forgotpassword" component={ForgotPassword} />
            <Route path="/resetpassword" component={ResetPassword} />
            {/* <Route path="/session/register" component={AsyncSessionRegisterComponent} /> */}
            {/* <Route path="/session/lock-screen" component={AsyncSessionLockScreenComponent} /> */}
            {/* <Route
               path="/session/forgot-password"
               component={AsyncSessionForgotPasswordComponent}
            /> */}
            {/* <Route path="/session/404" component={AsyncSessionPage404Component} />
            <Route path="/session/500" component={AsyncSessionPage500Component} /> */}
            {/* <Route path="/terms-condition" component={AsyncTermsConditionComponent} /> */}
            <Route path="/callback" render={(props) => {
               handleAuthentication(props);
               return <Callback {...props} />
            }} />
         </RctThemeProvider>
         : (
            <RctThemeProvider>
            <InitialPath
               path={`${match.url}app`}
               // authUser={user}
               component={AccessDenied}    
            />
            <Route path="/access-denied" component={AccessDenied} />
            <Route path="/signin" component={AppSignIn} />
            <Route path="/EmailConfirmation" component={EmailConfirmation} />
            <Route path="/forgotpassword" component={ForgotPassword} />
            <Route path="/resetpassword" component={ResetPassword} />
            
            
            
         </RctThemeProvider>
            )
         } 
         </React.Fragment>
      );
   }
}

// map state to props
const mapStateToProps = ({ authUser }) => {
   const { user } = authUser;
   return { user };
};

export default connect(mapStateToProps)(App);
