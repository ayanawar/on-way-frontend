/**
 * Signin Firebase
 */

import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input } from 'reactstrap';
import LinearProgress from '@material-ui/core/LinearProgress';
import QueueAnim from 'rc-queue-anim';
import Logo from '../assets/img/Logo.png'
// import { Fab } from "@material-ui/core";

// components
import {
    SessionSlider
} from 'Components/Widgets';

// app config
import AppConfig from 'Constants/AppConfig';

// redux action
// import {
//    signinUserInFirebase,
//    signinUserWithFacebook,
//    signinUserWithGoogle,
//    signinUserWithGithub,
//    signinUserWithTwitter
// } from 'Actions';

//Auth File
import Auth from '../Auth/Auth';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import IntlMessages from 'Util/IntlMessages';
import { useForm } from 'react-hook-form';

const auth = new Auth();
const cookies = new Cookies();

export default function RctPickers(props) {
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        user_email: '',
        user_password: '',
        device_identifier: '0',
        counter: 0,
    });

    // state = {

    // }

    /**
     * On User Login
     */
    useEffect(() => {
        const authResult = new URLSearchParams(window.location.search);
        console.log("authResult", authResult);
        const tk = authResult.get('tk')
        const obj = { token: tk }
        console.log("tk", tk);
        axios.post(
            'http://localhost:3000/confirmemail',
            obj

        ).then(res => {
            console.log("ayyaaaa", res)
            if (res.mesage === "Token Expired")
                toast.error('Please Resend Your Mail', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            else if (res.mesage === "Token UnAuthorized") {
                toast.error('Sorry You didnt have access to enter', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }

            else {
                toast.success('The email has been verified', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });

            }

        }).catch(error => {
            console.log(error)
        });

        // settoken(tk)
        // console.log("token in state",token);
        // gdg=tk
        // console.log("gdg",gdg);

    }, []);


    const loginNavigate = () => {
        window.location.href = "/signin";
    }

    const onUserLogin = ()=>{
        console.log("state", state);

        axios.post(
            'http://localhost:3000/resetpasswordemail',
            {
                userEmail: state.user_email
            }

        ).then(res => {
            console.log("ayyaaaa", res)
            if (res.mesage === "Token Expired")
                toast.error('Please Resend Your Mail', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            else if (res.mesage === "Token UnAuthorized") {
                toast.error('Sorry You didnt have access to enter', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }

            else {
                toast.success('check your email for reset password', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });

            }

        }).catch(error => {
            console.log("err ", error)
        });
    }

    const resendEmail = () => {
        const authResult = new URLSearchParams(window.location.search);
        console.log("authResult", authResult);
        const tk = authResult.get('tk')
        const obj = { token: tk }
        console.log("tk", tk);
        axios.post(
            'http://localhost:3000/resendconfirmemail',
            obj

        ).then(res => {
            console.log("ayyaaaa", res)
            if (res.mesage === "Token Expired")
                toast.error('Please Resend Your Mail', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            else if (res.mesage === "Token UnAuthorized") {
                toast.error('Sorry You didnt have access to enter', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }

            else {
                toast.success('check your email for confirmation', {
                    position: "top-center",
                    autoClose: 8000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });

            }

        }).catch(error => {
            console.log("err ", error)
        });
    }
    /**
     * On User Sign Up
     */
    const onUserSignUp = () => {
        props.history.push('/');
    }

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    //Auth0 Login
    // loginAuth0() {
    //    auth.login();
    // }

    // render() {
    const { user_email, user_password } = state;
    const { loading } = props;
    // console.log(state);
    return (
        console.log("state", state),

        <QueueAnim type="bottom" duration={2000}>
            <div className="rct-session-wrapper">
                <ToastContainer />
                {loading &&
                    <LinearProgress />
                }
                <AppBar position="static" className="session-header">
                    <Toolbar>
                        <div className="container">
                            <div className="d-flex justify-content-between">
                                <div className="session-logo">
                                    <Link to="/">
                                        <img src={AppConfig.appLogo} alt="session-logo" className="img-fluid" width="110" height="35" />
                                    </Link>
                                </div>
                                <div>
                                    <a className="mr-15" onClick={() => onUserSignUp()}>Create New account?</a>
                                    <Button variant="contained" className="btn-light" onClick={() => onUserSignUp()}>Sign Up</Button>
                                </div>
                            </div>
                        </div>
                    </Toolbar>
                </AppBar>
                {/* className="session-inner-wrapper" white background */}
                <div >

                    <div className="container" style={{ marginLeft: '500px' }}>

                        <div className="row row-eq-height">

                            <div style= {{marginTop: "-74px"}} className="col-sm-5 col-md-5 col-lg-4">
                                <div className="session-body">
                                    <div className="session-head mb-30" style={{ marginTop: '180px' }}>
                                        {/* <h2 style={{color: "#599A5F", fontSize: "55px", textAlign: "center !important"}}><IntlMessages id="form.login" /></h2> */}
                                        {/* <h2 className="font-weight-bold">Get started with {AppConfig.brandName}</h2> */}
                                        {/* <p className="mb-0">Most powerful ReactJS admin panel</p> */}
                                        <div style={{ alignContent: 'center' }}>
                                            <h1>Forgot Password</h1>
                                            <br />
                                            <img style={{ width: '150px' }} src={Logo}></img>

                                        </div>
                                    </div>
                                    <div>
                                        <Form onSubmit={handleSubmit(onUserLogin)} style={{}}>
                                            <div  >
                                                <h4>
                                                When you fill in your registered email address, you will be sent instructions on how to reset your password.
                                                </h4>
                                                <label className="mt-5" style={{ fontSize: "14px", fontWeight: "400", lineHeight: "1.42857", textAlign: "center !important", color: '#000000' }}>
                                                    Email
                                                </label>

                                                <FormGroup className="has-wrapper" style={{ marginBottom: "40px", textAlign: "center !important" }}>
                                                    <div style={{
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                                        <input
                                                            type="email"
                                                            // value={user_phonenumber}
                                                            name="user_email"
                                                            id="user-mail"
                                                            className="form-control p-3"
                                                            onChange={handleFields}
                                                            style={{ fontSize: "17px", backgroundColor: 'white' }}
                                                            ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}
                                                        />
                                                    </div>
                                                    <span className="errors">
                                                        {errors.user_email && errors.user_email.type === 'required' &&
                                                            <IntlMessages id="form.requiredError" />}
                                                        {errors.user_email && errors.user_email.type === 'pattern' &&
                                                            <IntlMessages id="form.numbersOnlyErrorError" />}
                                                        {/* {errors.user_email && errors.user_email.type === 'minLength' &&
                                          <IntlMessages id="form.minPhoneLengthError" />}
                                       {errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
                                          <IntlMessages id="form.minPhoneLengthError" />} */}
                                                    </span>
                                                    {/* <span className="has-icon"><i className="ti-mobile"></i></span> */}
                                                </FormGroup>
                                                <h4>
                                                Already have an account? <Link to="/signin" className="mr-15 text-white">Login here</Link> 
                                                </h4>
                                            </div>
                                          
                                            
                                            <FormGroup className="mb-15">
                                                
                                                {/* <Button
                                                    type="submit"
                                                    color="primary"
                                                    className="btn-block w-50"
                                                    variant="contained"
                                                    size="large"
                                                    // onClick={() => onUserLogin()}
                                                >
                                                    Reset Password
                                                </Button> */}

                                                <button type="submit" onClick={() => onUserLogin()} class="btn btn-primary btn-block">Reset Password</button>

                                            </FormGroup>
                                            {/* <FormGroup className="mb-15">
                                    <Button
                                       variant="contained"
                                       className="btn-info btn-block text-white w-100"
                                       size="large"
                                       onClick={() => loginAuth0()}
                                    >
                                       Sign In With Auth0
                            			</Button>
                                 </FormGroup> */}
                                        </Form>
                                    </div>
                                    {/* <Form onSubmit={handleSubmit(onUserLogin)} style={{}}>
                                <div  >
                                   <label className="mt-5" style={{ fontSize: "14px", fontWeight: "400", lineHeight: "1.42857", textAlign: "center !important", color: '#000000' }}>
                                      Email
                                   </label>
  
                                   <FormGroup className="has-wrapper" style={{ marginBottom: "40px", textAlign: "center !important" }}>
                                      <div style={{
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                      }}>
                                         <input
                                            type="email"
                                            // value={user_phonenumber}
                                            name="user_email"
                                            id="user-mail"
                                            className="form-control p-3"
                                            onChange={handleFields}
                                            style={{ fontSize: "17px", backgroundColor: 'white' }}
                                            ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}
                                         />
                                      </div>
                                      <span className="errors">
                                         {errors.user_email && errors.user_email.type === 'required' &&
                                            <IntlMessages id="form.requiredError" />}
                                         {errors.user_email && errors.user_email.type === 'pattern' &&
                                            <IntlMessages id="form.numbersOnlyErrorError" />}
                                         {/* {errors.user_email && errors.user_email.type === 'minLength' &&
                                            <IntlMessages id="form.minPhoneLengthError" />}
                                         {errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
                                            <IntlMessages id="form.minPhoneLengthError" />} 
                                      </span>
                                      <span className="has-icon"><i className="ti-mobile"></i></span>
                                   </FormGroup>
                                </div>
                                <label className="mt-5" style={{ color: '#000000', fontSize: "14px", fontWeight: "400", lineHeight: "1.42857" }}><IntlMessages id="form.password" /></label>
                                <FormGroup className="has-wrapper">
                                   <div style={{
                                      borderWidth: "0px 2px 2px 0px",
                                      lineHeight: "0px",
                                      borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                   }}>
                                      <input
                                         // value={user_password}
                                         type="Password"
                                         name="user_password"
                                         id="pwd"
                                         className="form-control p-3"
                                         onChange={handleFields}
                                         style={{ fontSize: "17px", backgroundColor: 'white' }}
                                         ref={register({
                                            required: true
                                            , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/
                                         })}
                                      />
                                   </div>
                                   <span className="errors">
                                      {errors.user_password && errors.user_password.type === 'required' &&
                                         <IntlMessages id="form.requiredError" />}
                                      {errors.user_password && errors.user_password.type === 'pattern' &&
                                         <IntlMessages id="form.passwordMinLengthError" />}
                                   </span>
                                   {/* <span className="has-icon"><i className="ti-lock"></i></span> 
                                </FormGroup>
                                <FormGroup className="mb-15">
  
                                   <Button
                                      type="submit"
                                      color="primary"
                                      className="btn-block w-50"
                                      variant="contained"
                                      size="large"
                                      onClick={() => onUserLogin()}
                                      style={{
                                         position: "relative", margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                         fontSize: "17px", fontWeight: "bold", height: '50px', marginTop: "30px"
                                      }}
                                   >
                                      <IntlMessages id="form.login" />
                                   </Button>
  
                                </FormGroup>
                                <FormGroup className="mb-15">
                                      <Button
                                         variant="contained"
                                         className="btn-info btn-block text-white w-100"
                                         size="large"
                                         onClick={() => loginAuth0()}
                                      >
                                         Sign In With Auth0
                                          </Button>
                                   </FormGroup> 
                             </Form> */}
                                    {/* <p className="mb-20">or sign in with</p>
                                <Fab size="small" variant="round" className="btn-facebook mr-15 mb-20 text-white"
                                   onClick={() => props.signinUserWithFacebook(props.history)}
                                >
                                   <i className="zmdi zmdi-facebook"></i>
                                </Fab>
                                <Fab size="small" variant="round" className="btn-google mr-15 mb-20 text-white"
                                   onClick={() => props.signinUserWithGoogle(props.history)}
                                >
                                   <i className="zmdi zmdi-google"></i>
                                </Fab>
                                <Fab size="small" variant="round" className="btn-twitter mr-15 mb-20 text-white"
                                   onClick={() => props.signinUserWithTwitter(props.history)}
                                >
                                   <i className="zmdi zmdi-twitter"></i>
                                </Fab>
                                <Fab size="small" variant="round" className="btn-instagram mr-15 mb-20 text-white"
                                   onClick={() => props.signinUserWithGithub(props.history)}
                                >
                                   <i className="zmdi zmdi-github-alt"></i>
                                </Fab> */}
                                    {/* <p className="text-muted">By signing up you agree to {AppConfig.brandName}</p>
                                <p className="mb-0"><a target="_blank" href="#/terms-condition" className="text-muted">Terms of Service</a></p> */}
                                </div>
                            </div>
                            {/* <div className="col-sm-5 col-md-5 col-lg-4">
                             <SessionSlider />
                          </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </QueueAnim>
    );
    // }
}

  // // map state to props
  // const mapStateToProps = ({ authUser }) => {
  //    const { user, loading } = authUser;
  //    return { user, loading }
  // }

  // export default connect(mapStateToProps, {
  //    signinUserInFirebase,
  //    signinUserWithFacebook,
  //    signinUserWithGoogle,
  //    signinUserWithGithub,
  //    signinUserWithTwitter
  // })(Signin);
