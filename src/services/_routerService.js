// routes
import Widgets from 'Routes/widgets';
import PointsOfSaleManagement from 'Routes/points-of-sale-management';
import RepresentativesManagement from 'Routes/representatives-management';
import CalendarComponents from 'Routes/calendar';
import ChartsComponents from 'Routes/charts';
import FormElements from 'Routes/forms';
import Users from 'Routes/users';
import DealersManagement from 'Routes/dealers-management';
import SystemSettings from 'Routes/system-settings';
import Reports from 'Routes/reports';
import Maps from 'Routes/maps';
import AlaimtidadBalances from 'Routes/alaimtidad-balances';
import Dealer from 'Routes/Dealer';
import DealerPrintBalances from 'Routes/DealerPrintBalances';
import DealerRequestCredit from 'Routes/DealerRequestCredit';
import DealerPrintCredit from 'Routes/DealerPrintCredit';
import SupCards from 'Routes/SupCards';
import Editor from 'Routes/editor';
import SuppliersManagement from 'Routes/suppliers-management';
import employeemanagment from 'Routes/employee-managment';
import MoneyManagement from 'Routes/accountingtree';
import currency from 'Routes/currencymang';
import clients from 'Routes/clients-managment';
// import Crm from 'Routes/crm';
import ImageCropper from 'Routes/image-cropper';
import VideoPlayer from 'Routes/video-player';
import UploadCompaniesCards from 'Routes/upload-companies-cards';
import AccountActivationRequests from 'Routes/account-activation-requests';
import MovementOfCompaniesCards from 'Routes/movement-of-companies-cards';
import CardsWithdrawal from 'Routes/cards-withdrawal'
import DisplayCompanies from 'Routes/display-companies';
import HomePage from 'Routes/home-page';
import WorkerManagement from 'Routes/worker-management';
import ShowAndUpdateCompaniesCards from 'Routes/show-and-update-companies-cards';
import DisplayAndUpdateCategories from 'Routes/display-and-update-categories';
import Accounts from 'Routes/accounts';
import salary from 'Routes/salarysetting'
import restrictions from 'Routes/restrictions'
import inventory from 'Routes/inventory'
import invoices from 'Routes/invoices'
import confirm from 'Routes/emailconfirmation'
import request from 'Routes/requests'
import servicemanagement from 'Routes/servicemanagement'
// async component
import {
   AsyncAboutUsComponent,
   AsyncChatComponent,
   AsyncMailComponent,
   AsyncTodoComponent,
} from 'Components/AsyncComponent/AsyncComponent';

export default [
   {
      path: 'servicemanagement',
      component: servicemanagement
   },
   {
      path: 'emailconfirmation',
      component: confirm
   },
   {
      path: 'requests',
      component: request
   },
   {
      path: 'home-page',
      component: HomePage
   },
   {
      path: 'worker-management',
      component: WorkerManagement
   },
   {
      path: 'invoices',
      component: invoices
   },
   {
      path: 'accountingtree',
      component: MoneyManagement
   },
   {
      path: 'inventory',
      component: inventory
   },
   {
      path: 'widgets',
      component: Widgets
   },
   {
      path: 'employee-managment',
      component: employeemanagment
   },
   {
      path: 'restrictions',
      component: restrictions
   },
   {
      path: 'clients-managment',
      component: clients
   },
   {
      path: 'suppliers-management',
      component: SuppliersManagement
   },
   {
      path: 'reports',
      component: Reports
   },
   {
      path: 'about-us',
      component: AsyncAboutUsComponent
   },
   {
      path: 'points-of-sale-management',
      component: PointsOfSaleManagement
   },
   {
      path: 'chat',
      component: AsyncChatComponent
   },
   {
      path: 'mail',
      component: AsyncMailComponent
   },
   {
      path: 'todo',
      component: AsyncTodoComponent
   },
   {
      path: 'charts',
      component: ChartsComponents
   },
   {
      path: 'system-settings',
      component: SystemSettings
   },
   {
      path: 'salarysetting',
      component: salary
   },
   {
      path: 'maps',
      component: Maps
   },
   {
      path: 'users',
      component: Users
   },
   {
      path: 'dealers-management',
      component: DealersManagement
   },
   {
      path: 'representatives-management',
      component: RepresentativesManagement
   },
   {
      path: 'alaimtidad-balances',
      component: AlaimtidadBalances
   },
   {
      path: 'Dealer',
      component: Dealer
   },
   {
      path: 'DealerRequestCredit',
      component: DealerRequestCredit
   },
   {
      path: 'DealerPrintCredit',
      component: DealerPrintCredit
   },
   {
      path: 'DealerPrintBalances',
      component: DealerPrintBalances
   },
   
   {
      path: 'forms',
      component: FormElements
   },
   {
      path: 'editor',
      component: Editor
   },
   {
      path: 'calendar',
      component: CalendarComponents
   },
   {
      path: 'image-cropper',
      component: ImageCropper
   },
   {
      path: 'video-player',
      component: VideoPlayer
   },
   {
      path: 'upload-companies-cards',
      component: UploadCompaniesCards
   },
   {
      path: 'account-activation-requests',
      component: AccountActivationRequests
   },
   {
      path: 'currencymang',
      component: currency
   },
   {
      path: 'movement-of-companies-cards',
      component: MovementOfCompaniesCards
   },
   {
      path: 'show-and-update-companies-cards',
      component: ShowAndUpdateCompaniesCards
   },
   {
      path: 'cards-withdrawal',
      component: CardsWithdrawal
   },
   {
      path: 'display-companies',
      component: DisplayCompanies
   },
   {
      path: 'display-and-update-categories',
      component: DisplayAndUpdateCategories
   },
   {
      path: 'accounts',
      component: Accounts
   },
   {
      path: 'SupCards',
      component: SupCards
   },
]