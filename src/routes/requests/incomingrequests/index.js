
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import io from 'socket.io-client';

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

const options = {
   filter: true,
   filterType: 'dropdown',
   rowsPerPage: 5,
   rowsPerPageOptions: [5,10,25,50,100],
   responsive: 'vertical',
   enableNestedDataAccess: '.',
//   rowsSelected: '',
   selectableRows: "none",
   viewColumns: false,
   doawload: false,
   sort: false,
   fixedHeader: true,
   download: false,
   fixedSelectColumn: false,
   tableBodyHeight: "400px"
 };

export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        representatives: [],
        disabled: false,
		hiddenStatusForm: true,
		hiddenDebitLimitForm: true,
      	debit_limit: '',
		debitLimitTable: '',
		user_phonenumber: '',
		availableWorker:'',
		request_id: '',
		service_provider_location_id: '',
		worker_id : '',
		hiddenassign : true,

    });
	
	const nf = new Intl.NumberFormat();
   
	useEffect(() => {

	

		const socket = io('http://localhost:3000', {transports: ['websocket'], upgrade: false});
		socket.on('message', (msg)=>{
			console.log("called");
			console.log(msg);
			setTimeout(function () { location.reload()}, 500)
		})

		socket.on('removeRequest', (msg)=>{
			console.log("called");
			console.log(msg);
			setTimeout(function () { location.reload()}, 500)
		})
		let data = {user_id: localStorage.getItem("service_provider_location_id")}
		socket.emit('provider', data)
        console.log("prov location id", localStorage.getItem("service_provider_location_id"));
		
		console.log("prov location id", data.user_id);
		// all workers
		axios.post('http://localhost:3000/workers/getavailableworkers',{service_provider_location_id: data.user_id},USER_TOKEN).then(response2 => {
		axios.post('http://localhost:3000/getrequestedservices',{service_provider_id: data.user_id},USER_TOKEN).then(response => {
		
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
                console.log("workers", response2.data.message);
                let workers = [];
				for (let i = 0; i < response.data.length; i ++) {
                    console.log("eneterd to set workers");
                    let worker ={
                        request_id: response.data[i].request_id,
                        service_name: response.data[i].Request.Service.service_name,
                        request_status: response.data[i].Request.request_status,
                        customer_id: response.data[i].Request.customer_id,
                        created_at: response.data[i].created_at
                    }
                    console.log("done with worker");
                    workers.push(worker);
				
				}
				
                console.log("workers", workers);
				setState({
					...state,
					representatives: workers,
					availableWorker:response2.data.message,
					hiddenStatusForm: true,
					hiddenDebitLimitForm: true,
					disabled: false,
					service_provider_location_id: data.user_id,
				
				})
			}
		})
		.catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	})
	.catch(error => {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	});
	}, []);

	const onSubmit = e => {
		let data ={
			request_id:state.request_id,
			service_provider_location_id:parseInt(state.service_provider_location_id), 
			worker_id:parseInt(state.worker_id)
		}
		console.log("data",data);
		axios({
			method: 'post', url: 'http://localhost:3000/provider/assignworker', data:data,
			headers: { "x-access-token": `${cookies.get('UserToken')}`}
		}).then(res =>{
			toast.success("Worker has been successfully assigned", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload()}, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
   	}

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const onClearStatusClicked = () => {
		setState({...state,
			user_type_id: '',
			hiddenStatusForm: true,
			hiddenDebitLimitForm: true,
			debit_limit: '',
			debitLimitTable: '',
			user_phonenumber: '',
			representativeId: '',
			dealerId: '',
			representativeId: '',
			user_id: '',
			dealerFirstName: '',
			dealerMiddleName: '', 
			dealerLastName: '',
			repFirstName: '',
			repMiddleName: '',
			repLastName: '',
			status: ''
		})
	};

	const OnReject = (requestid) => {
		let data ={
			request_id:requestid,
			service_provider_location_id:parseInt(state.service_provider_location_id), 
		}
		console.log("data",data);
		axios({
			method: 'post', url: 'http://localhost:3000/provider/rejectrequest', data: data,
			headers: { "x-access-token": `${cookies.get('UserToken')}`}
			}).then(res =>{
				toast.success("Request has been rejected", {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
	} 

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

    const { match } = props;
	console.log("stateee",state);
    return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
        <div className="cart-wrapper">
            <PageTitleBar title="Incoming Requests" match={match} />
			<ToastContainer />
			<Helmet>
				<title>Incoming Requests</title>
				<meta name="description" content="Incoming Requests" />
  			</Helmet>

			
			{(state.hiddenassign === true)?
				<span>  </span>
			:
			<form onSubmit={handleSubmit(onSubmit)}>
			 <div className="form-group  dropdown col-md-6">
					<label>Choose Worker:</label>
					<select name="worker_id" className="form-control input-text" 
							onChange={handleFields} ref={register({ required: true })}>
							<option key="0" value="">Please Choose the worker to assign him to this task</option>
							{state.availableWorker.map(worker => {
							return (
								<option key={worker.user_id} value={worker.user_id}>
									{worker.User.user_first_name} {worker.User.user_last_name} " {worker.User.user_email} "
								</option>
							)
						})
						}
					</select>
					<span className="errors">
						{errors.worker_id && errors.worker_id.type === 'required' &&
							<IntlMessages id="form.requiredOptionError" /> }
					</span>
				</div>
	
				{(state.disabled === false) ? 
					<button type="submit" className="btn btn-primary btn-margin">
						<IntlMessages id="form.add" />
					</button> : 
					<button type="submit" className="btn btn-primary btn-margin" disabled={true}>
						<IntlMessages id="form.add" />
					</button>
			}
		</form>
			}
	

		
		
			<div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable 
									// title={<IntlMessages id="sidebar.allrepresentatives" />}
									data={state.representatives}
									columns={[
											{
												label: "id",
												name:  "request_id",
												options: {
													display: "none",
													filter: false,
													print: false,
												}
											},
											{
												label: "Requestor Id",
												name:  "customer_id",
											},
										
											{
												label: "Request status",
												name:  "request_status",
											},
											{
												label: "Request Date",
												name:  "created_at" 
											},
											{
												label: "Type of services needed",
												name:  "service_name" 
											},
											{
												label: "Assign",
												name: "",
												options: {
													filter: true,
													sort: false,
													empty: true,
													print: false,
												customBodyRender: (value, tableMeta, updateValue) => {
													return (
														<React.Fragment>
															<button type="button" className="btn btn-warning" 
															onClick={() => {
																setState({ 
																	...state, 
																	hiddenassign:false,
																	request_id: tableMeta.rowData[0],
															
																
																})
																}}>
															Assign to
															</button>
														</React.Fragment>
													);
												}
												}
						
											},
                                            {
												label: "Reject",
												name: "",
												options: {
													filter: true,
													sort: false,
													empty: true,
													print: false,
												customBodyRender: (value, tableMeta, updateValue) => {
													return (
														<React.Fragment>
															<button type="button" className="btn btn-danger" 
															onClick={() => 
													      OnReject(tableMeta.rowData[0])
															}>
															Reject
															</button>
														</React.Fragment>
													);
												}
												}
						
											},
										
											
											
										]}
									options={options} />
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
        </div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
   }


