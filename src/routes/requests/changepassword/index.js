import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import Modal from 'react-awesome-modal';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();

export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_id: '',
		user_type_id: '',
        new_password: '',
        old_password: '',
        person_user_id: '',
        name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
		commercialName: '',
		disabled: false,
		ModelToPrint: false,
	});

	const nf = new Intl.NumberFormat();
	
	const handleFields = ({ target }) => {
		// if(state.cardsnumber % 1000 != 0) {
			setState({ ...state, [target.name]: target.value,  });
			//  console.log(target);
		// }
		
	};
	const Token = cookies.get('UserToken');
	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:3000/changepassword', data: {
				"userTypeId": localStorage.getItem("user_type_id"),
				"user_password": state.new_password,

			},headers: { "x-access-token": `${Token}`  }
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				
				toast.success('Password changed Successfully', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true, ModelToPrint: false, })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				});
				setState({ ...state, ModelToPrint: false, }) 
			} else {
				toast.error('Your old password is incorrect please try agian', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, ModelToPrint: false, })
			}
		// console.log(error.message);
		});	
	// }
	}

	const { match } = props;
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
			<title>Change password</title>
 			<meta name="description" content="Change password" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.resetPassword" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.resetPassword" />}>
				    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-row">
                      
                         
                            <div className="form-group col-md-6">
                                <label>New password</label>
                                <input type="password" className="form-control" name="new_password"
                                        onChange={handleFields} ref={register({ required: true 
                                            , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/ })} />
                                <span className="errors">
                                    {errors.new_password && errors.new_password.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.new_password && errors.new_password.type === 'pattern' &&
                                        <IntlMessages id="form.passwordMinLengthError" /> }
                                </span>
                            </div>
					    </div>
					
                        {(state.disabled === false) ? 
                            // <button onClick={openPrintModel}> testtttt</button>
                            <button type="submit" className="btn btn-primary">
                            Change password
                            </button> : 
                            <button type="submit" className="btn btn-primary" disabled={true}>
                            Change password
                            </button>
                        }
				    </form>
				

			
			</RctCollapsibleCard>

		</div>
		{/* <div className="row">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							data={state.cards}
							columns={[
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_bundle_total_value" 
									},
									{
										label: <IntlMessages id="form.createdBy" />,
										name:  "admin_name",
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "virtual_mony_bundle_date_time" 
									},
									{
										label: <IntlMessages id="form.cardsCount" />,
										name:  "virtual_mony_bundle_count" 
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name: "category_value"
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div> */}
	</div>
	: (
			
		history.push("/access-denied")
	   )
	} 
	   </React.Fragment>
	)
	}