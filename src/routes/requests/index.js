/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    Asyncincomingrequests,
    AsyncChangepassword,
    AsyncCustomersrequest,
   
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>Requests</title>
             <meta name="description" content="incomingrequests" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/incomingrequests`} />
             <Route path={`${match.url}/incomingrequests`} component={Asyncincomingrequests} />
             <Route path={`${match.url}/changepassword`} component={AsyncChangepassword} />
             <Route path={`${match.url}/customersrequest`} component={AsyncCustomersrequest} />
         </Switch>
     </div>
 );
 
 export default Pages;
 