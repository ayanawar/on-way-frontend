
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

const options = {
   filter: true,
   filterType: 'dropdown',
   rowsPerPage: 5,
   rowsPerPageOptions: [5,10,25,50,100],
   responsive: 'vertical',
   enableNestedDataAccess: '.',
//   rowsSelected: '',
   selectableRows: "none",
   viewColumns: false,
   doawload: false,
   sort: false,
   fixedHeader: true,
   download: false,
   fixedSelectColumn: false,
   tableBodyHeight: "400px"
 };

export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        representatives: [],
        disabled: false,
		hiddenStatusForm: true,
		hiddenDebitLimitForm: true,
      	debit_limit: '',
		debitLimitTable: '',
		user_phonenumber: '',
    });
	
	const nf = new Intl.NumberFormat();
   
	useEffect(() => {
        console.log("prov location id", localStorage.getItem("service_provider_location_id"));
		// all workers
		axios.post('http://localhost:3000/provider/getequests',{ service_provider_location_id: localStorage.getItem("service_provider_location_id")},USER_TOKEN).then(response => {
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
                console.log(response);
                let workers = [];
				for (let i = 0; i < response.data.message.length; i ++) {
                    console.log("eneterd to set workers");
                    let worker ={
                        request_id: response.data.message[i].request_id,
                        customerName: response.data.message[i].customer_first_name + " " + response.data.message[i].customer_last_name,
                        customer_email: response.data.message[i].customer_email,
                        phoneNumber: response.data.message[i].customer_phone,
                        request_status: response.data.message[i].request_status,
                        rating: response.data.message[i].rating,
                        created_at: response.data.message[i].created_at,
						workerName: response.data.message[i].worker_first_name + " " + response.data.message[i].worker_last_name,
                        worker_email: response.data.message[i].worker_email,
                        worker_phone: response.data.message[i].worker_phone,
						total_price: response.data.message[i].total_price,
						servicename: response.data.message[i].service_name,
                    }
                    console.log("done with worker");
                    workers.push(worker);
					// response.data.representatives[i].representative_virtual_mony_balance = nf.format(response.data.representatives[i].representative_virtual_mony_balance);
				}
                console.log("workers", workers);
				setState({
					representatives: workers,
					hiddenStatusForm: true,
					hiddenDebitLimitForm: true,
					disabled: false,
				})
			}
		})
		.catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}, []);



	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

    const { match } = props;
    return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
        <div className="cart-wrapper">
            <PageTitleBar title="Customer Requests" match={match} />
			<ToastContainer />
			<Helmet>
				<title>Customer Requests</title>
				<meta name="description" content="Customer Requests" />
  			</Helmet>
            <div className="row">
              
		    </div>
			<div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable 
									// title={<IntlMessages id="sidebar.allrepresentatives" />}
									data={state.representatives}
									columns={[
											{
												label: "Request id",
												name:  "request_id",
											
											},
											{
												label: "Customer Name",
												name:  "customerName",
											},
										
											{
												label: "Customer Email",
												name:  "customer_email",
											},
											
											{
												label: "Customer phone number",
												name:  "phoneNumber",
											},
											{
												label: "Request Date",
												name:  "created_at" 
											},
											{
												label: "Assigned To",
												name:  "workerName"
											},
											{
												label: "worker phone number",
												name:  "worker_phone"
											},
											{
												label: "Rating",
												name:  "rating"
											},
											{
												label: "Request status",
												name:  "request_status"
											},
                                            {
												label: "Type of services needed",
												name:  "servicename" 
											},
										
											{
												label: "Total Price",
												name:  "total_price" 
											},
											
										]}
									options={options} />
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
        </div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
   }


