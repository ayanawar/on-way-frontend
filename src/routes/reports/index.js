/**
 * Icons Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async themify icons
import {
	AsyncThemifyIconsComponent,
	AsyncAlaimtidadPrintedReportComponent,
	AsyncSimpleLineIconsComponent,
	AsyncCompaniesMinimumReportComponent,
	AsyncMonthlyMoneyReport,
	AsyncAnnualMoneyReport,
	AsyncDailyMoneyReportComponent,
	AsyncWeeklyMoneyReportComponent,
} from 'Components/AsyncComponent/AsyncComponent';

const Icons = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>التقارير</title>
			<meta name="description" content="التقارير" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/alaimtidad-availability-report`} />
			<Route path={`${match.url}/alaimtidad-availability-report`} component={AsyncThemifyIconsComponent} />
			<Route path={`${match.url}/alaimtidad-printed-report`} component={AsyncAlaimtidadPrintedReportComponent} />
			<Route path={`${match.url}/companies-availability-report`} component={AsyncSimpleLineIconsComponent} />
			<Route path={`${match.url}/companies-minimum-report`} component={AsyncCompaniesMinimumReportComponent} />
			<Route path={`${match.url}/monthly_money_report`} component={AsyncMonthlyMoneyReport} />
			<Route path={`${match.url}/annual_money_report`} component={AsyncAnnualMoneyReport} />
			<Route path={`${match.url}/daily-money-report`} component={AsyncDailyMoneyReportComponent} />
			<Route path={`${match.url}/weekly-money-report`} component={AsyncWeeklyMoneyReportComponent} />
		</Switch>
	</div>
);

export default Icons;
