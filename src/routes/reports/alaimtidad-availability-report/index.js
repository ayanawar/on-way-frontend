/**
 * Alaimtidad Availability Report
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';

const cookies = new Cookies(); 

export default function BasicTable(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';
	const history = useHistory();
	const [state, setState] = useState({
		virtualMoney: [],
		sumOfBalances: [],
		sumRepresentativesBalance: 0,
		sumDealersBalance: 0,
		sumPOSBalance: 0,
		sumAdministratorsBalance: 0,
		hiddenExcelButton: true,
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
			axios.get('http://localhost:8000/alaimtidadavailabilityreport',USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
					// console.log(response.data.virtualMoney);
					for (let i = 0; i < response.data.virtualMoney.length; i++) {
						response.data.virtualMoney[i].administratorsBalance = nf.format((response.data.virtualMoney[i].administratorsBalance).toFixed(0));
						response.data.virtualMoney[i].representativesBalance = nf.format((response.data.virtualMoney[i].representativesBalance).toFixed(0));
						response.data.virtualMoney[i].dealersBalance = nf.format((response.data.virtualMoney[i].dealersBalance).toFixed(0));
						response.data.virtualMoney[i].POSBalance = nf.format((response.data.virtualMoney[i].POSBalance).toFixed(0));
					}

					setState({
						virtualMoney: response.data.virtualMoney,
						// sumOfBalances: response.data.sumOfBalances,
						hiddenExcelButton: false,
						sumRepresentativesBalance: nf.format((response.data.sumOfBalances.sumRepresentativesBalance).toFixed(0)),
						sumDealersBalance: nf.format((response.data.sumOfBalances.sumDealersBalance).toFixed(0)),
						sumPOSBalance: nf.format((response.data.sumOfBalances.sumPOSBalance).toFixed(0)),
						sumAdministratorsBalance: nf.format((response.data.sumOfBalances.sumAdministratorsBalance).toFixed(0)),
					})
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});		
	}, []); 

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})


	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		let newArr = []
		newArr = state.virtualMoney.map(vm => ({
			"Bundle Id": vm.virtual_mony_bundle_id,
			"Serial": vm.virtual_mony_serial_number,
			"PIN": vm.virtual_mony_pin_number,
			"Representatives Balances": parseLocaleNumber(vm.representativesBalance),
			"Dealers Balances": parseLocaleNumber(vm.dealersBalance),
			"Points of sale Balances": parseLocaleNumber(vm.POSBalance),
			"Administrators Balances": parseLocaleNumber(vm.administratorsBalance),
		}));

		const ws = XLSX.utils.json_to_sheet(newArr);

		const wb = { Sheets: { 'تقرير الوفرة أرصدة': ws }, SheetNames: ['تقرير الوفرة أرصدة'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, `تقرير الوفرة أرصدة ${new Date(Date.now())} ` + fileExtension);
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 8,
		rowsPerPageOptions: [5,8,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		download: false,
		sort: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "500px"
	};

	const { match } = props;
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
  			<title>تقرير الوفرة الامتداد</title>
  			<meta name="description" content="تقرير الوفرة الامتداد" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.alaimtidadAvailabilityReport" />} match={match} />
		<div className="form-group col-md-6">
			<br />
			<div className="container">
				{(state.hiddenExcelButton === false) ?
					<React.Fragment>
						<button className="btn btn-primary ml-5 mr-5" onClick={(e) => exportToCSV()}>
							<IntlMessages id="form.exportExcel" />
						</button>
					</React.Fragment>
					:
					<React.Fragment></React.Fragment>
				}
			</div>
		</div>
		<div className="row">
			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfRepresentatives" /></h3>
				<h4 className="reportsCardsNumberCenter">{state.sumRepresentativesBalance}</h4>
			</RctCollapsibleCard>

			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfDealers" /></h3>
				<h4 className="reportsCardsNumberCenter">{state.sumDealersBalance}</h4>
			</RctCollapsibleCard>

			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfPOS" /></h3>
				<h4 className="reportsCardsNumberCenter">{state.sumPOSBalance}</h4>	
			</RctCollapsibleCard>
			
			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfAdministrators" /></h3>
				<h4 className="reportsCardsNumberCenter">{state.sumAdministratorsBalance}</h4>
			</RctCollapsibleCard>
		</div>
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					// heading={<IntlMessages id="sidebar.alaimtidadAvailabilityReport" />}
					>
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							// title={<IntlMessages id="sidebar.basic" />}
							data={state.virtualMoney}
							columns={[
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id",
										options: {
											filter: true,
											setCellHeaderProps: () => ({
												style: {
												whiteSpace: "nowrap",
												position: "sticky",
												left: 0,
												zIndex: 101
												}
											})
										}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_serial_number",
									},
									{
										label: "PIN",
										name:  "virtual_mony_pin_number",
									},
									{
										label: <IntlMessages id="form.representativesBalance" />,
										name:  "representativesBalance" 
									},
									{
										label: <IntlMessages id="form.dealersBalance" />,
										name:  "dealersBalance",
									},
									{
										label: <IntlMessages id="form.POSBalance" />,
										name:  "POSBalance" 
									},
									{
										label: <IntlMessages id="form.administratorsBalance" />,
										name:  "administratorsBalance" 
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
	</div>
	
	: (
			
		history.push("/access-denied")
	   )
	} 
	</React.Fragment>
	)
	}