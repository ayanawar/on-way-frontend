

/**
 * Display And Update Categories
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        companies: [],
        categoriesByCompany: [],
        companyName: '',
        sellPriceTable: '',
        categoryId: '',  
        companyId: '',
        sellPrice: '',
        disabled: false,
        hidden: true,
    });
    
    const nf = new Intl.NumberFormat();

	useEffect(() => {
        axios.get(`http://localhost:8000/allcompanies`,USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else{
                response.data.companies.splice(0, 1);
                setState({
                    ...state,
                    companies: response.data.companies,
                    disabled: false,
                    hidden: true,
                })
            }
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
            MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
                    fontSize: "30px",
					backgroundColor: "#599A5F",
					fontFamily: "'Almarai', sans-serif"
				}
            },
            MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
    })
  
    const handleRowClick = (rowData, rowIndex) => {
        axios.post(`http://localhost:8000/cardsbelowlimit`,{"companyId":rowData[0], "userTypeId": localStorage.getItem("user_type_id")},USER_TOKEN).then(res => {
            for (let i = 0; i < res.data.message.length; i ++) {
                res.data.message[i].category_value = nf.format((res.data.message[i].category_value).toFixed(0));
                res.data.message[i].purchase_price = nf.format((res.data.message[i].purchase_price).toFixed(0));
                res.data.message[i].sell_price = nf.format((res.data.message[i].sell_price).toFixed(0));
            }
        setState({ 
            ...state,
            categoriesByCompany: res.data.message,
            // companyName: res.data.message[0].company_name_ar
            companyName: rowData[1]
        })
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        }); 
    };

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 50,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        download: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
    };
    
    const options2 = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        // onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        download: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
	};

	const { match } = props;
	// console.log(state);
	return (
        <React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>تقرير الحد الأدنى شركات</title>
 				<meta name="description" content="تقرير الحد الأدنى شركات" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.companiesMinimum" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.companiesMinimum" />}>
                    <div className="form-row mt-4">
                        <div className="form-group col-md-4 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.companies}
                                        columns={[
                                            {
                                                name:  "company_id",
                                                options: {
                                                display: "none",
                                                filter: false
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="form.companyName" />,
                                                // name:  "company_name",
                                                name:  "company_name_ar",
                                            },
                                            ]}
                                        options={options}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                        <div className="form-group col-md-8 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.categoriesByCompany}
                                        title={state.companyName}
                                        columns={[
                                            {
                                                name:  "cards_category_id",
                                                options: {
                                                    display: "none",
                                                    filter: false
                                                }
                                            },
                                            {
                                                name:  "company_id",
                                                options: {
                                                    display: "none",
                                                    filter: false
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="widgets.category" />,
                                                name:  "category_text",
                                            },
                                            {
                                                label: <IntlMessages id="form.purchasePrice" />,
                                                name:  "purchase_price"
                                            },
                                            {
                                                label: <IntlMessages id="form.sellPrice" />,
                                                name:  "sell_price"
                                            },
                                            {
                                                label: <IntlMessages id="form.currency" />,
                                                name:  "category_currency"
                                            },
                                            {
                                                label: <IntlMessages id="table.minimum" />,
                                                name: "cards_low_limit",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                <h1 className="btn btn-danger" style={{ pointerEvents: "none" }}>
                                                                    {value}
                                                                </h1>
                                                            </React.Fragment>
                                                        );
                                                    }
                                                }
                                            },
											{
												label: <IntlMessages id="form.currentBalance" />,
                                                name:  "cardsAvailable"
											},
                                        ]}
                                        options={options2}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                    </div>
			    </RctCollapsibleCard>
		    </div>
		</div>
        : (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}
