/**
 * add supplier
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import Cookies from 'universal-cookie';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

export default function Shop(props) {
	const history = useHistory();
	const [state, setState] = useState({
		companies: [],
		categoriesByCompany: [],
		companyName: '',
		availableCardsCount: 0,
		availableCardsValue: 0,
		printedCardsCount: 0,
		printedCardsValue: 0,
		suspendedCardsCount: 0,
		suspendedCardsValue: 0, 
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
    axios.get(`http://localhost:8000/getallcompanies/${localStorage.getItem("user_type_id")}`,USER_TOKEN).then(response => {
		if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
			cookies.remove('UserToken', {path:'/'})
			window.location.href = "/signin";	
		}
		else {
		// console.log(response.data.message);
		
			for (let i = 0; i < response.data.message.length; i++) {
				response.data.message[i].available_cards = nf.format((response.data.message[i].available_cards).toFixed(0));
				response.data.message[i].Printed_cards = nf.format((response.data.message[i].Printed_cards).toFixed(0));
				response.data.message[i].suspend_cards = nf.format((response.data.message[i].suspend_cards).toFixed(0));
			}
			setState({
				...state,
				companies: response.data.message,
				
				availableCardsValue: nf.format(response.data.total_values.total_value_available),
				printedCardsValue: nf.format(response.data.total_values.total_value_printed),
				suspendedCardsValue: nf.format(response.data.total_values.total_value_suspend),

				availableCardsCount: nf.format(response.data.totals.total_available_cards),
				printedCardsCount: nf.format(response.data.totals.total_printed_cards),
				suspendedCardsCount: nf.format(response.data.totals.total_suspended_cards), 
			})
		}
    }).catch(error => {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
    });
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontSize: "30px",
					backgroundColor: "#599A5F",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
  })
  
  const handleRowClick = (rowData, rowIndex) => {
    axios.post('http://localhost:8000/getcompanyavcards', {
		'userTypeId': localStorage.getItem("user_type_id") ,
		'companyId': rowData[0]
    },USER_TOKEN).then(res => {

		for (let i = 0; i < res.data.message.length; i ++) {
			res.data.message[i].number_of_cards = nf.format((res.data.message[i].number_of_cards).toFixed(0));
			res.data.message[i].number_of_printed_cards = nf.format((res.data.message[i].number_of_printed_cards).toFixed(0));
			res.data.message[i].number_of_suspend_cards = nf.format((res.data.message[i].number_of_suspend_cards).toFixed(0));
		}

		if (res.data.message.length == 0) {
			setState({
				...state,
				categoriesByCompany: res.data.message,
				companyName: ''
			})
		} else {
			setState({ 
				...state,
				categoriesByCompany: res.data.message,
				companyName: res.data.message[0].company_name_ar
				// companyName: res.data.message[0].company_name
			})
		}
		
    }).catch(error => {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
    }); 
  };


	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 50,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		onRowClick: handleRowClick,
		sort: false,
		viewColumns: false,
		download: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
	};

	const options2 = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		sort: false,
		viewColumns: false,
		download: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>تقرير الوفرة شركات</title>
				<meta name="description" content="تقرير الوفرة شركات" />
			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.companiesAvailabilityReport" />} match={match} />
			<div className="row">
			
				<RctCollapsibleCard
					customClasses="p-20 text-center"
					colClasses="col-sm-4 col-lg-4 col-xl-4"
				>
					<h3 className="mb-20">إجمالى الكارتات المتوفرة</h3>
					<h4 className="reportsCardsNumberContainer"> العدد   <span className="reportsCardsNumber ml-3">  {state.availableCardsCount}</span></h4>
					<br />
					<h4 className="reportsCardsNumberContainer"> القيمة   <span className="reportsCardsNumber ml-3">  {state.availableCardsValue}</span></h4>
				</RctCollapsibleCard>

				<RctCollapsibleCard
					customClasses="p-20 text-center"
					colClasses="col-sm-4 col-lg-4 col-xl-4"
				>
					<h3 className="mb-20">إجمالى الكارتات المطبوعة</h3>
					<h4 className="reportsCardsNumberContainer"> العدد   <span className="reportsCardsNumber ml-3">  {state.printedCardsCount}</span></h4>
					<br />
					<h4 className="reportsCardsNumberContainer"> القيمة   <span className="reportsCardsNumber ml-3">  {state.printedCardsValue}</span></h4>
				</RctCollapsibleCard>
			
				<RctCollapsibleCard
					customClasses="p-20 text-center"
					colClasses="col-sm-4 col-lg-4 col-xl-4"
				>
					<h3 className="mb-20">إجمالى الكارتات الموقوفة</h3>
					<h4 className="reportsCardsNumberContainer"> العدد   <span className="reportsCardsNumber ml-3">  {state.suspendedCardsCount}</span></h4>
					<br />
					<h4 className="reportsCardsNumberContainer"> القيمة   <span className="reportsCardsNumber ml-3">  {state.suspendedCardsValue}</span></h4>
				</RctCollapsibleCard>
				
			</div>

			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12">
					 {/* heading={<IntlMessages id="sidebar.companiesAvailabilityReport" />} */}

				<div className="form-row mt-4">
					<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
							data={state.companies}
							columns={[
								{
									name:  "company_id",
									options: {
									display: "none",
									filter: false
									}
								},
								{
									label: <IntlMessages id="form.companyName" />,
									// name:  "company_name",
									name:  "company_name_ar",
								},
								{
									label: "المتوفر",
									name:  "available_cards"
								},
								{
									label: "المطبوع",
									name:  "Printed_cards"
								},
								{
									label: "الموقوف",
									name:  "suspend_cards"
								},
							]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
          			<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable
									data={state.categoriesByCompany}
									title={state.companyName}
									columns={[
										{
											label: <IntlMessages id="widgets.category" />,
											name:  "category_text",
										},
										{
											label: "المتوفر",
											name:  "number_of_cards"
										},
										{
											label: "المطبوع",
											name:  "number_of_printed_cards"
										},
										{
											label: "الموقوف",
											name:  "number_of_suspend_cards"
										},
									]}
									options={options2}
								/>
							</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
				</div>
			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}