/**
 * add supplier
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../constants/Token';



export default function CreateEmCards(props) {
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '5',
		user_id: '1',
    	vmValue: '',
		companies: [],
		regions: [],
		areas: [],
		suppliers: [],
		isImagesChanged: false
	});


	useEffect(() => {
		axios.get('http://localhost:8000/allregions',USER_TOKEN).then(response => {
			axios.get('http://localhost:8000/allareas',USER_TOKEN).then(response2 => {
				axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response3 => {
					response.data.regions.splice(0, 1);
					response2.data.areas.splice(0, 1);
					response3.data.companies.splice(0, 1);
					setState({
						regions: response.data.regions, 
						areas: response2.data.areas,
						companies: response3.data.companies
					})
				}).catch(error => {
					// console.log('Error fetching and parsing data', error);
				});
			}).catch(error => {
				// console.log('Error fetching and parsing data', error);
			});
		}).catch(error => {
		   	// console.log('Error fetching and parsing data', error);
		});
	}, []); 

	const maxSelectFile = (e) => {
		let files = e.target.files 
			if (files.length > 1) { 
			  e.target.value = null 
			//   console.log("You can only upload one image");
			  toast.error("You can only upload one image", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			  });
			  return false;
		  }
		return true;
	  
	}
	  
	const checkMimeType= (e) => {
		let files = e.target.files 
		let err = ''
		const types = ['image/png', 'image/jpeg', 'image/gif', 'image/jpg']
		for(var x = 0; x<files.length; x++) {
				if (types.every(type => files[x].type !== type)) {   
				err += files[x].type+' is not a supported format, The allowed extensions are png,jpeg,gif and jpg';
			}
			};
		
		if (err !== '') {  
			e.target.value = null 
			// console.log(err);
			toast.error(err, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return false; 
		}
		return true;
	}
	  
	const checkFileSize = (e) => {
		let files = e.target.files
		let size = 500000 
		let err = ""; 
		for(var x = 0; x<files.length; x++) {
		if (files[x].size > size) {
		 	err += files[x].type+' is too large, please pick a smaller file, Files size allowed up to 500 KB';
	   	}
	  };
	  if (err !== '') {
		e.target.value = null
		// console.log(err);
		toast.error(err, {
			position: "top-center",
			autoClose: 4000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});
		return false
	  }
	  
	  return true;
	  
	}

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const handleImage = e => {
		if(maxSelectFile(e) && checkMimeType(e) && checkFileSize(e)){ 
			setState({ ...state, [e.target.name]: e.target.files[0]});
			// console.log(e.target.files[0].name);
	  	}
	}

	const onSubmit = e => {

		let data = new FormData();
		data.append('user_type_id', '4'),
		data.append('user_first_name', state.user_first_name),
		data.append('user_middle_name', state.user_middle_name),
		data.append('user_last_name', state.user_last_name),
		data.append('supplier_company_id', state.supplier_company_id),
		data.append('user_region_id', state.user_region_id),
		data.append('user_area_id', state.user_area_id),
		data.append('user_phonenumber', state.user_phonenumber),
		data.append('user_email', state.user_email),
		data.append('user_password', state.user_password),
		data.append('user_personal_image', state.user_personal_image),

		axios({
		method: 'post', url: 'http://localhost:8000/createaccount', data: data,
		headers: { 'Content-Type': 'multipart/form-data' }
		},USER_TOKEN).then(res =>{
			toast.success('تم إضافة المورد بنجاح', {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			location.reload();
		}).catch(error => {
		// console.log(error.message);
		});
	}

	const { match } = props;
	// console.log(state);
	return (
		<div className="shop-wrapper">
			<ToastContainer />
			<PageTitleBar title={<IntlMessages id="sidebar.shop" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.shop" />}>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="form-row">
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.firstName" /></label>
						<input type="text" className="form-control" name="user_first_name" 
								onChange={handleFields} 
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z]+$/i,})} />
						<span className="errors">
							{errors.user_first_name && errors.user_first_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_first_name && errors.user_first_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_first_name && errors.user_first_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.middleName" /></label>
						<input type="text" className="form-control" name="user_middle_name"
								onChange={handleFields}
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z]+$/i, })} />
						<span className="errors">
							{errors.user_middle_name && errors.user_middle_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.lastName" /></label>
						<input type="text" className="form-control" name="user_last_name"
								onChange={handleFields}
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z]+$/i, })} />
						<span className="errors">
							{errors.user_last_name && errors.user_last_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.companyName" /></label>
						<select className="form-control" name="supplier_company_id" 
								onChange={handleFields} ref={register({ required: true })}>  
								<option key="0" value="">برجاء الاختيار</option>    
						{state.companies.map((company)=>{
							return(
								<option key={company.company_id} value={company.company_id}>
									{company.company_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.supplier_company_id && errors.supplier_company_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.phoneNumber" /></label>
						<input type="tel" className="form-control" name="user_phonenumber"
								onChange={handleFields} 
								ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
						<span className="errors">
							{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
								<IntlMessages id="form.numbersOnlyErrorError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
								<IntlMessages id="form.minPhoneLengthError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
								<IntlMessages id="form.minPhoneLengthError" /> }
						</span>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group  dropdown col-md-6">
						<label><IntlMessages id="form.region" /></label>
						<select name="user_region_id" className="form-control input-text" 
								onChange={handleFields} ref={register({ required: true })}>
								<option key="0" value="">برجاء الاختيار</option>
						{state.regions.map(region => {
							return (
								<option key={region.region_id} value={region.region_id}>
									{region.region_arabic_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.user_region_id && errors.user_region_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
					<div className="form-group  dropdown col-md-6">
						<label><IntlMessages id="form.area" /></label>
						<select name="user_area_id" className="form-control input-text" 
								onChange={handleFields}  ref={register({ required: true })}>
								<option key="0" value="">برجاء الاختيار</option>
						{state.areas.map(area => {
							return (
								<option key={area.area_id} value={area.area_id}>
									{area.area_arabic_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.user_area_id && errors.user_area_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
				</div>

				<div className="form-row">
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.email" /></label>
						<input type="email" className="form-control" name="user_email"
								onChange={handleFields} 
								ref={register({
									required: true,
									pattern: /^[^@ ]+@[^@ ]+\.[^@ .]{2,}$/
								  })} />
						<span className="errors">
							{errors.user_email && errors.user_email.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_email && errors.user_email.type === 'pattern' &&
								<IntlMessages id="form.emailError" /> }
						</span>
					</div>
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.password" /></label>
						<input type="password" className="form-control" name="user_password"
								onChange={handleFields} ref={register({ required: true, minLength: 6})} />
						<span className="errors">
							{errors.user_password && errors.user_password.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_password && errors.user_password.type === 'minLength' &&
								<IntlMessages id="form.passwordMinLengthError" /> }
						</span>
					</div>
				</div>
				<div className="md-form md-outline">
					<label><IntlMessages id="form.image" /></label>
					<br />
					<input type="file" name="user_personal_image" onChange={handleImage}/>
				</div>
				<button type="submit" className="btn btn-primary btn-margin">
					<IntlMessages id="form.add" />
				</button>
			</form>


			</RctCollapsibleCard>
		</div>
		</div>
	)
	}