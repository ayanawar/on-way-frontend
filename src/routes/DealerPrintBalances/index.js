import React, { useEffect, useState, useRef } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import { useReactToPrint } from 'react-to-print';
import PrintPdf from '../alaimtidad-balances/print-balances/print-pdf';

const cookies = new Cookies();

export default function ExportCSV(props) {
	const history = useHistory();

	const componentRef = useRef();

	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
        onAfterPrint: () => { 
            setState({
                ...state, 
				disabled: true
            })
            location.reload()
        },
	});

	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
		commercialName: '',
		userTypeId: 0,
		userId: 0,
		virtualCards: [],
		cardsForPrintPdf: [],
		adminBalance: 0,
		disabled: false,
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		axios.post('http://localhost:8000/getuserdata',
			{ 
                'userTypeId': localStorage.getItem("user_type_id") ,
			    'userId': localStorage.getItem("userIdInUsers")
		},USER_TOKEN).then(response2 => {
			// console.log(response2);
			axios.post('http://localhost:8000/virtualcards', 
				{ 
                    'userTypeId': localStorage.getItem("user_type_id") ,
				    'userId': localStorage.getItem("userIdInUsers")
			},USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {

					for (let i = 0; i < response.data.virtualCards.length; i ++) {
						response.data.virtualCards[i].virtual_money_balance = nf.format(response.data.virtualCards[i].virtual_money_balance);
					}

					setState({
						...state,
						virtualCards: response.data.virtualCards, 
						userTypeId: localStorage.getItem("user_type_id") ,
						userId: localStorage.getItem("userIdInUsers"),
						disabled: false,
						adminBalance: nf.format(response2.data.message[0].dealer_virtual_money_balance)
					})
				}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						}); 
					}
				});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});	
	}, []);

	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

    const fileExtension = '.xlsx';

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
	
		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}
	
	let totalBalancesToBePrinted = 0;
	let selectedSerials = [];
	let selectedBalances = [];
	let selectedRowsWithData = [];
	let selectedPINsForPDF = [];
	let selectedSerialsForPDF = [];
	let objForPrintPdf = {};
	let objArr = [];
	const handlemultiSelect = (selectedRows,displayData) => {
		
		selectedSerials = [];
		selectedBalances = [];
		selectedRowsWithData = [];
		selectedPINsForPDF = [];
		selectedSerialsForPDF = [];
		objArr = [];
		objForPrintPdf = {};
		// console.log("selectedRowsArray",selectedRows);
		selectedRows.data.forEach(element => {
			selectedSerials.push(displayData[element.index].data[0]);
			selectedBalances.push(parseLocaleNumber(displayData[element.index].data[4]));
			selectedPINsForPDF.push(displayData[element.index].data[2])
			selectedSerialsForPDF.push(displayData[element.index].data[3])
			selectedRowsWithData.push({
				"Bundle Id": displayData[element.index].data[1],
				"PINs": displayData[element.index].data[2],
				"Serials": displayData[element.index].data[3],
				"Balances": parseLocaleNumber(displayData[element.index].data[4])
			})

			objForPrintPdf = {
				"pin": displayData[element.index].data[2],
				"serial": displayData[element.index].data[3],
				"balance": displayData[element.index].data[4],
				"date": displayData[element.index].data[5],
			};
			objArr.push(objForPrintPdf)
		});
		// console.log("selectedBalances: ",selectedBalances);
		// console.log("selectedPINsForPDF", selectedPINsForPDF);
		// console.log("selectedSerialsForPDF", selectedSerialsForPDF);
		// console.log("objForPrintPdf1", objForPrintPdf);
		// console.log("objArr",objArr);
		// console.log("selectedSerials: ",selectedSerials);
		
		totalBalancesToBePrinted = 0;
		for (let i = 0; i< selectedBalances.length; i++) {
			totalBalancesToBePrinted += selectedBalances[i];	
		}
		return(
			<React.Fragment>
				<div className="form-group col-md-12 mt-3 text-center">
					<h2 className="mb-3"><IntlMessages id="form.balancesToPrint" /></h2>
					<h2>{nf.format(totalBalancesToBePrinted)}</h2>
					<br />
					<div className="container">
						<div className="row">
							<div className="col-md-6">
								{(state.disabled === false) ? 
									<button className="btn btn-primary ml-4 mr-3" 
											onClick={(e) => downloadPdf()}>
										طباعة PDF
									</button>
									: 
									<button className="btn btn-primary ml-4 mr-3" 
											disabled={true}>
										طباعة PDF
									</button>
								}
							</div>
							<div className="col-md-6">
								{(state.disabled === false) ? 
									<button className="btn btn-primary" 
											onClick={(e) => exportToCSV()}>
										<IntlMessages id="form.exportExcel" />
									</button>
									:
									<button className="btn btn-primary" 
											disabled={true}>
										<IntlMessages id="form.exportExcel" />
									</button>
								}
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}

	const exportToCSV = () => {
		const ws = XLSX.utils.json_to_sheet(selectedRowsWithData);

		const wb = { Sheets: { 'Printed Serial Numbers': ws }, SheetNames: ['Printed Serial Numbers'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], {type: fileType});

		FileSaver.saveAs(data, 'Printed Serial Numbers' + fileExtension);

		axios.post('http://localhost:8000/printvirtualcards', 
			{ 
                'userTypeId': localStorage.getItem("user_type_id") ,
                'userId': localStorage.getItem("userIdInUsers"),
                'totalAmount': totalBalancesToBePrinted,
                'userPhoneNumber': localStorage.getItem("phoneNumber"),
                'virtualCardsIds': selectedSerials,
			},USER_TOKEN).then(response => {
				toast.success(<IntlMessages id="form.printSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 2000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});
    }


	const downloadPdf = () => {
		setState({ ...state, cardsForPrintPdf: objArr})
		axios.post('http://localhost:8000/printvirtualcards', 
			{ 
                'userTypeId': localStorage.getItem("user_type_id") ,
                'userId': localStorage.getItem("userIdInUsers"),
                'totalAmount': totalBalancesToBePrinted,
                'userPhoneNumber': localStorage.getItem("phoneNumber"),
                'virtualCardsIds': selectedSerials,
			},USER_TOKEN).then(response => {
				toast.success(<IntlMessages id="form.printSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				// console.log("objForPrintPdf3", objForPrintPdf);
				handlePrint()
				// setState({ ...state, disabled: true })
				// setTimeout(function () { location.reload()}, 2000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});
    }

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		customToolbarSelect: handlemultiSelect,
		selectableRowsHeader: true,
		download: false,
		print: false,
		viewColumns: false,
		sort: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_USER_TYPE === 2 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>طباعة أرصدة</title>
				<meta name="description" content="طباعة أرصدة" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.printBalances" />} match={match} />
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.printBalances" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.adminBalance}</h2>	
						</div>
					</div>
				<div style={{ display: "none" }}>
            		<PrintPdf ref={componentRef} {...state} />	
        		</div>
				<div className="row justify-content-center">
					<div className="form-row mt-5">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.balances" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.virtualCards}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_money_id",
										options: {
											display: "none",
											filter: false
										}
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id",
									},
									{
										label: "PIN",
										name:  "virtual_mony_pin_number",
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_serial_number",
									},
									{
										label: <IntlMessages id="form.virtualMoneyBalance" />,
										name:  "virtual_money_balance"
									},
									{
										label: "date",
										name:  "virtual_mony_date_time",
										options: {
											display: "none",
											filter: false
										}
									}
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</div>
			</RctCollapsibleCard>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}