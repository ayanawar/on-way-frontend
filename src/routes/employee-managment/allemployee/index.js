/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
  
    
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         employees: [],
         disabled: false,
         hiddenStatusForm: true,
         accountId:'',
         accNameAr:'',
         accNameEn:'',
         accDesc:'',
         statusId:'',
         securityLevelId:'',
         accType:'',
         accCurrencyId:'',
         userFirstName:'',
         userMiddleName:'',
         userLastName:'',
         jobTitle:'',
         basicSalary:'',
         employee_id:'',
         regionId:'',
         areaId:'',
         phoneNumber:'',
         regions: [],
         areas: [],
         levcels2:[],
         levcels3:[],
         types: [],
         levels: [],
         currency: [],
         accounts: [],
         MainAccounts:[],
         status:[],
     });
     useEffect(() => {
         // all suppliers
         axios.get('https://accbackend.alaimtidad-itland.com/all-employees',USER_TOKEN).then(response13 => {
            axios.get('https://accbackend.alaimtidad-itland.com/getallstatus', USER_TOKEN).then(response18 => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
                axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
                    axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
                        axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
                           axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                               axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
             
                     if (response13.data == "Token Expired" || response13.data == "Token UnAuthorized" ||response8.data == "Token Expired" || response8.data == "Token UnAuthorized" || response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized"|| response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response11.data == "Token Expired" || response11.data == "Token UnAuthorized" ) {
                 cookies.remove('UserToken', {path:'/'})
                 window.location.href = "/signin";	
             }
             else{
 
                 setState({
                    ...state,
                    regions: response8.data.regions,
                    levels: response.data.levels,
                    types: response3.data.message,
                    currency:response4.data.message,
                    accounts:response5.data.accounts,
                    MainAccounts:response11.data.mainAccounts,
                    employees: response13.data.employees,
                    status:response18.data.message,
                    disabled: false,
                    hiddenStatusForm: true,
                 })
             }


            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
         }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
         
           }).catch(error => {
                 if (error.response.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     }); 
                 }
             });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);
 
     const onClearStatusClicked = () => {
         setState({...state,
             user_type_id: '',
             hiddenStatusForm: true,
             hiddenDebitLimitForm: true,
             debit_limit: '',
             debitLimitTable: '',
             user_phonenumber: '',
             representativeId: '',
             dealerId: '',
             representativeId: '',
             user_id: '',
             dealerFirstName: '',
             dealerMiddleName: '', 
             dealerLastName: '',
             repFirstName: '',
             repMiddleName: '',
             repLastName: '',
             status: ''
         })
     };
 
     const onSubmit2 = e => {
         let data ={
            accountId:parseInt(state.accountId),
            accNameAr:state.accNameAr,
            accNameEn:state.accNameEn,
            employeeId:state.employee_id,
            accDesc:state.accDesc,
            securityLevelId:parseInt(state.securityLevelId),
            accType:parseInt(state.accType),
            statusId:parseInt(state.statusId),
            accCurrencyId:parseInt(state.accCurrencyId),
            userFirstName:state.userFirstName,
            userMiddleName:state.userMiddleName,
            userLastName:state.userLastName,
            jobTitle:state.jobTitle,
            basicSalary:state.basicSalary,
            regionId:parseInt(state.regionId),
            areaId:parseInt(state.areaId),
            phoneNumber:state.phoneNumber,
         }
         console.log("data",data);
         axios({
             method: 'post', url: 'https://accbackend.alaimtidad-itland.com/updateemployee', data: data,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
            if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            else{  
             toast.success(<IntlMessages id="updatedsuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
             setState({ ...state, disabled: true })
             setTimeout(function () { location.reload()}, 3000)
            }
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
     const handleTwoEvents3 = (e) => {
		handleFields(e);
		let regionId = e.target.value;

		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						regionId: regionId,
						areas: response.data.areas,
						user_area_id: '',
						
					})

				}else{
					setState({
						...state,
						regionId: regionId,
						areas: response.data.areas,
					
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}
 
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })
 
 //    render() {
       const { match } = props;
   
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>قائمة الموظفين</title>
                 <meta name="description" content="قائمة الموظفين" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.allemployee" />} match={match} />

             <div className="row">
                <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={"تعديل بيانات الموظفين"}>
                



                     {(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
                     : 
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                     <label> ستقوم بتعديل الحساب رقم {state.selected_user[1]}</label>
                      <div className="form-row">
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountname" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="accNameAr"
										  onChange={handleFields} defaultValue={state.accNameAr}
										  ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
											  </div>
									  <span className="errors">
										  {errors.accNameAr && errors.accNameAr.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.accNameAr && errors.accNameAr.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.accNameAr && errors.accNameAr.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountnameEng" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="accNameEn"
										  onChange={handleFields} defaultValue={state.accNameEn}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
									   </div>
									  <span className="errors">
										  {errors.accNameEn && errors.accNameEn.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.accNameEn && errors.accNameEn.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.accNameEn && errors.accNameEn.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.cointype" /></label>
									  <div >
                                      <label className="middletitle" >{state.currency_ar}</label>
					   </div>
				
								  </div>
							  </div>
						  
   

							  <div className="form-row">

                                  
                             <div className="form-group col-md-4">
                             <label><IntlMessages id="widgets.status" /></label>
                             <select className="form-control" name="statusId" 
                                     onChange={handleFields} ref={register({ required: true })}>  
                                     <option key="0" value="">اختر الحالة</option>
                                     {state.status.map(currency => {
												return (
													<option key={currency.status_id} value={currency.status_id}>
														{currency.status_ar}
													</option>
												)
											})
											}    
                             </select>
                             <span className="errors">
                                 {errors.statusId && errors.statusId.type === 'required' &&
                                     <IntlMessages id="form.requiredOptionError" /> }
                             </span>
                             </div>
							  <div className="form-group col-md-4">
									  <label><IntlMessages id="FORM.DEBT" /></label>
									  <div >
                                      <label className="middletitle">{state.audit_type_ar}</label>
					   </div>
					  
								  </div>

								 



							  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.description" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}> 
										<textarea  className="form-control" name="accDesc"   rows={3} cols={5}
										  onChange={handleFields} defaultValue={state.accDesc}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
										 </div>
									  <span className="errors">
										  {errors.accDesc && errors.accDesc.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.accDesc && errors.accDesc.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.accDesc && errors.accDesc.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>

								  </div>
							 
								  <div className="form-row">
								  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.secretdegree" /></label>
									  <div >
									  <select className="form-control input-text" name="securityLevelId"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار درجة السرية</option>
										  {state.levels.map(level => {
											 return (
												 <option key={level.security_level_id} value={level.security_level_id}>
													 {level.security_level_type_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.securityLevelId && errors.securityLevelId.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>

							  

								  </div>
								  <label className="middletitle">بيانات الموظف</label>

             
                                 <div className="form-row">
                                     <div className="form-group col-md-4" >
                                         <label><IntlMessages id="form.firstName" /></label>
                                         <input type="text" className="form-control" name="userFirstName"
                                             onChange={handleFields} defaultValue={state.userFirstName}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.userFirstName && errors.userFirstName.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.userFirstName && errors.userFirstName.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.userFirstName && errors.userFirstName.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.middleName" /></label>
                                         <input type="text" className="form-control" name="userMiddleName"
                                             onChange={handleFields} defaultValue={state.userMiddleName}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.userMiddleName && errors.userMiddleName.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.userMiddleName && errors.userMiddleName.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.userMiddleName && errors.userMiddleName.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.lastName" /></label>
                                         <input type="text" className="form-control" name="userLastName"
                                             onChange={handleFields} defaultValue={state.userLastName}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.userLastName && errors.userLastName.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.userLastName && errors.userLastName.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.userLastName && errors.userLastName.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                 </div>
                            
                                 <div className="form-row">

                                 <div className="form-group col-md-4">
										<label><IntlMessages id="form.phoneNumber" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="tel" className="form-control" name="phoneNumber"
											onChange={handleFields} defaultValue={state.phoneNumber}
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
									</div>
										<span className="errors">
											{errors.phoneNumber && errors.phoneNumber.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.phoneNumber && errors.phoneNumber.type === 'pattern' &&
												<IntlMessages id="form.numbersOnlyErrorError" />}
											{errors.phoneNumber && errors.phoneNumber.type === 'minLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
											{errors.phoneNumber && errors.phoneNumber.type === 'maxLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
										</span>
									</div>
                                     <div className="form-group  dropdown col-md-4">
                                         <label><IntlMessages id="form.region" /></label>
                                         <select name="regionId" className="form-control input-text"
                                             onChange={handleTwoEvents3} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المحافظة</option>
                                             {state.regions.map(region => {
                                                 return (
                                                     <option key={region.region_id} value={region.region_id}>
                                                         {region.region_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         <span className="errors">
                                             {errors.regionId && errors.regionId.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                     <div className="form-group  dropdown col-md-4">
                                         <label><IntlMessages id="form.area" /></label>
                                         <select name="areaId" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المنطقة</option>
                                             {state.areas.map(area => {
                                                 return (
                                                     <option key={area.area_id} value={area.area_id}>
                                                         {area.area_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         <span className="errors">
                                             {errors.areaId && errors.areaId.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                 </div>
 
                                 <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.jobtitle" /></label>
                                         <input type="text" className="form-control" name="jobTitle"
                                             onChange={handleFields} defaultValue={state.jobTitle}   ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })}/>
                                        <span className="errors">
                                             {errors.jobTitle && errors.jobTitle.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.jobTitle && errors.jobTitle.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.jobTitle && errors.jobTitle.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>

                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.salary" /></label>
                                         <input type="text" className="form-control" name="basicSalary"
                                             onChange={handleFields} defaultValue={state.basicSalary} ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 1, maxLength: 100, })} 
                                         />
                                          <span className="errors">
                                             {errors.basicSalary && errors.basicSalary.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.basicSalary && errors.basicSalary.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.basicSalary && errors.basicSalary.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     </div>
                                 
                             
                         {(state.disabled === false) ? 
                             <button type="submit" className="btn btn-warning">
                             <IntlMessages id="form.update" />
                             </button> : 
                             <button type="submit" className="btn btn-warning" disabled={true} >
                             <IntlMessages id="form.update" />
                             </button>
                             }
 
                         <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                             <IntlMessages id="form.clear" />
                         </button>
                             </form>
                 
                       }
                </RctCollapsibleCard>
             </div>
             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                     data={state.employees}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "employee_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                label: <IntlMessages id="form.accountnumber" />,
                                                name:  "acc_number",
                                            },
                                             {
                                                label: <IntlMessages id="form.accountname" />,
                                                name:  "acc_name_ar",
                                            },
                                            {
                                                label: <IntlMessages id="form.accountnameEng" />,
                                                name:  "acc_name_en",
                                            },
                                            {
                                                label: <IntlMessages id="form.secretdegree" />,
                                                name:  "security_level_type_ar",
                                            },
                                            {
                                                label: <IntlMessages id="widgets.status" />,
                                                name:  "status_ar",
                                            },
                                            {
                                                label: <IntlMessages id="form.cointype" />,
                                                name:  "currency_ar",
                                            },
                                            {
                                                label: <IntlMessages id="FORM.DEBT" />,
                                                name:  "audit_type_ar",
                                            },
                                            {
                                                label: <IntlMessages id="form.description" />,
                                                name:  "acc_desc",
                                            },
                                           
                                             {
                                                 label: <IntlMessages id="form.firstName" />,
                                                 name:  "emp_first_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.middleName" />,
                                                 name:  "emp_middle_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.lastName" />,
                                                 name:  "emp_last_name",
                                             },
                                          
                                             {
                                                label: <IntlMessages id="form.phoneNumber" />,
                                                name:  "employee_phone_number",
                                            },
                                             {
                                                 label: <IntlMessages id="form.region" />,
                                                 name:  "region_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.area" />,
                                                 name:  "area_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.jobtitle" />,
                                                 name:  "job_title" 
                                             },
                                             {
                                                label: <IntlMessages id="form.salary" />,
                                                name:  "basic_salary" 
                                            },
                                            {
                                                label: "id",
                                                name:  "acc_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                       
                                            {
                                                label: <IntlMessages id="form.update" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-warning" 
                                                            onClick={() => {
                                                               
                                                                  setState({
                                                                   ...state , 
                                                                   selected_user:tableMeta.rowData,
                                                                   employee_id:tableMeta.rowData[0],
                                                                  userFirstName:tableMeta.rowData[9],
                                                                  userMiddleName:tableMeta.rowData[10],
                                                                  userLastName:tableMeta.rowData[11],
                                                                  phoneNumber:tableMeta.rowData[12],
                                                                  basicSalary:tableMeta.rowData[16],
                                                                  jobTitle:tableMeta.rowData[15],
                                                                  accDesc:tableMeta.rowData[8],
                                                                  accNameEn:tableMeta.rowData[3],
                                                                  accNameAr:tableMeta.rowData[2],
                                                                  accountId:tableMeta.rowData[17],
                                                                  currency_ar: tableMeta.rowData[6],
                                                                  audit_type_ar: tableMeta.rowData[7],
                                                                  accType : tableMeta.rowData[21],
                                                                  accCurrencyId: tableMeta.rowData[20],
                                                                   hiddenStatusForm: false,
                                                                   hiddenDebitLimitForm: true
                                                                  });
                                                           }}>
                                                            <IntlMessages id="form.update" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },

                                             {
                                                label: <IntlMessages id="button.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger" 
                                                            onClick={() => {
                                                            axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,USER_TOKEN).then((res) => {
                                                                setState({
                                                                    ...state , 
                                                                    user_id:res.data.userresult[0].user_id,
                                                                    user_type_id: res.data.userresult[0].user_type_id,
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true,
                                                                    dealerFirstName: tableMeta.rowData[1],
                                                                    dealerMiddleName: tableMeta.rowData[2],
                                                                    dealerLastName: tableMeta.rowData[3],
                                                                    status: tableMeta.rowData[8],
                                                                    accType : tableMeta.rowData[8],
                                                                    accCurrencyId: tableMeta.rowData[8],

                                                                })
                                                            }).catch((error) => {
                                                                if (error.response.status === 429) {
                                                                    toast.error(error.response.data, {
                                                                       position: "top-center",
                                                                       autoClose: 4000,
                                                                       hideProgressBar: false,
                                                                       closeOnClick: true,
                                                                       pauseOnHover: true,
                                                                       draggable: true
                                                                    }); 
                                                                }
                                                            })
                                                            }}>
                                                            <IntlMessages id="button.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                            {
                                                label: "id",
                                                name:  "currency_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                          
                                            {
                                                label: "id",
                                                name:  "acc_type",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }