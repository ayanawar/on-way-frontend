/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
     AsyncAddEmployeeComponent,
     AsyncAllEmployeeComponent,
     AsyncemployeetransactionComponent
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>ادارة الموظفين</title>
             <meta name="description" content="ادارة الموظفين" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/addemployee`} />
  
             <Route path={`${match.url}/addemployee`} component={AsyncAddEmployeeComponent} />
             <Route path={`${match.url}/allemployee`} component={AsyncAllEmployeeComponent} />
             <Route path={`${match.url}/employeetransaction`} component={AsyncemployeetransactionComponent} />
             
         </Switch>
     </div>
 );
 
 export default Pages;
 