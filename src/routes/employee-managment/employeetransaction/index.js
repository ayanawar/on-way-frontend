/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
        transaction: [],
         accountNumber:'',
         disabled: false,
         hiddenStatusForm: true,
         
     });
   
 
    
 
     const onSubmit2 = e => {
         let data ={
            accountNumber:state.accountNumber
         }
         axios({
             method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getemployeetrasactions', data: data,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
             console.log("response",res.data[0]);
          
              setState({ ...state, disabled: true, transaction:res.data })
         
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
    
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })
 
 //    render() {
       const { match } = props;
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>الاستعلام عن حركة الموظفين</title>
                 <meta name="description" content="الاستعلام عن حركة الموظفين" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.employeetransaction" />} match={match} />

             <div className="row">
                <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={"الاستعلام عن حركة الموظفين"}>
                     {/*  */}
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                         <div className="form-row">
                         <div className="form-group col-md-4">
                             <label>
                                 <IntlMessages id="searchbyaccnum" />
                             </label>
                             </div>
                             <div className="form-group col-md-4">
                             <input type="text" className="form-control" name="accountNumber"
                                             onChange={handleFields}
                                             ref={register({ required: true, pattern: /^[0-9]+$/ })}
                                             />
                                         </div>
                                         <span className="errors">
                                             {errors.accountNumber && errors.accountNumber.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNumber && errors.accountNumber.type === 'pattern' &&
                                                 <IntlMessages id="form.numbersOnlyErrorError" />}
 
                                         </span>
                        
                         
                         {(state.disabled === false) ? 
                                     <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="widgets.search" />
                                     </button> : 
                                     <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="widgets.search" />
                                     </button>
                                 }
 
 </div>
                     </form>

                </RctCollapsibleCard>
             </div>
             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                     data={state.transaction}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "employee_transaction_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                label: <IntlMessages id="form.accountnumber" />,
                                                name:  "employee_account_number",
                                            },
                                             {
                                                 label: <IntlMessages id="form.firstName" />,
                                                 name:  "emp_first_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.middleName" />,
                                                 name:  "emp_middle_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.lastName" />,
                                                 name:  "emp_last_name",
                                             },
                                          
                                             
                                             {
                                                 label: <IntlMessages id="form.region" />,
                                                 name:  "region_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.area" />,
                                                 name:  "area_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.jobtitle" />,
                                                 name:  "emblyee_job_title" 
                                             },
                                             {
                                                label: <IntlMessages id="form.salary" />,
                                                name:  "employee_basic_salary" 
                                            },
                                            {
                                                label: <IntlMessages id="form.dataTime" />,
                                                name:  "action_date" 
                                            },
                                            {
                                                label: <IntlMessages id="form.actiontype" />,
                                                name:  "emplyee_updated_type" 
                                            },
                                            
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }