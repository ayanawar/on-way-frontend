import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 


export default function Shop(props) {
	const history = useHistory();
   	const Token = cookies.get('UserToken');
   	const { register, handleSubmit, errors } = useForm();
   	const [Data, setData] = useState([])
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,

        userIdInTable:'',
        userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: []
	});
   React.useEffect(() => {
    //   console.log("BeforAxios",localStorage.getItem('userIdInUsers'),);
      axios({url:'http://localhost:8000/dealeraccount',method:'post',
      data: {
		userId:parseInt(localStorage.getItem('userIdInUsers')),
		userTypeId:2,
		userPhonenumber:localStorage.getItem('phoneNumber')
	},
      headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` }
      })
      // axios.post('http://localhost:8000/creditmovement', 
      // { 'userId': localStorage.getItem('userIdInUsers'),
      // },USER_TOKEN,
      // )
      .then(res2 => {
                //  console.log("Response",res2.data.dealers);

             setData(res2.data.dealers)
         // for (let i = 0; i < res2.data.transactions.length; i ++) {
         //    res2.data.transactions[i].virtual_mony_transaction_value = nf.format(res2.data.transactions[i].virtual_mony_transaction_value);
         //    res2.data.transactions[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.transactions[i].virtual_money_transaction_transfer_amount);
         // }

         // console.log("uuu ",res2.data.userData.userName);
         // setState({
         //    ...state,
         //    name: res2.data.userData.userName,
         //    region: res.data.tableresult[0].region_arabic_name,
         //    area: res.data.tableresult[0].area_arabic_name,
         //    phoneNumber: res.data.tableresult[0].dealer_phone_number,
         //    balance: nf.format(res.data.tableresult[0].dealer_virtual_money_balance),
            
         //    userTypeIdInTable: res.data.userresult[0].user_type_id,
         //    userIdInTable: res.data.userresult[0].user_id,
         //    type: "وكيل",
         //    pos_company_cards_transaction: res2.data.pos_company_cards_transaction,
         //    pos_sim_card_transaction: res2.data.pos_sim_card_transaction,
         //    pos_vm_transaction: res2.data.transactions
         // })
      }).catch(error => {
        //  console.log('Error fetching and parsing data', error);
      });
      
         
      }, []);












	const nf = new Intl.NumberFormat();

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					zIndex: 1
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				}
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
		}
	})
	


	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_USER_TYPE === 2 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<PageTitleBar title={<IntlMessages id="sidebar.DealerAccountStatement" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.DealerAccountStatement" />}>
					{/* <div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<Form inline>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
								</Form>
							</div>
								<span className="errors">
									{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
										<IntlMessages id="form.requiredError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
										<IntlMessages id="form.numbersOnlyErrorError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
								</span>
							
							<br />
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<h1>{state.type}</h1>
									<br /> 
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
								</React.Fragment>
							}
						</div>
					</div> */}
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="sidebar.DealerAccountStatement" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={Data}
								columns={[
									{
										label: <IntlMessages id="dealer_virtual_money_balance" />,
										name:  "dealer_virtual_money_balance"
									},
									{
										label: <IntlMessages id="moneyTransferedTo" />,
										name:  "moneyTransferedTo"
									},
									{
										label: <IntlMessages id="moneyTransferedFrom" />,
										name:  "moneyTransferedFrom",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	);
}
