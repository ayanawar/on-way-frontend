/**
 * Drag and Drop Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async components
import {
	AsyncReactDragulaComponent,
	AsyncReactDndComponent,
	AsyncWithdrawalBalancesComponent,
	AsyncDealerPrintBalancesComponent
} from 'Components/AsyncComponent/AsyncComponent';

const DragAndDrop = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>Alaimtidad Balances</title>
			<meta name="description" content="Alaimtidad Balances" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/create-alaimtidad-balances`} />
			{/* <Route path={`${match.url}/create-alaimtidad-balances`} component={AsyncReactDragulaComponent} /> */}
			{/* <Route path={`${match.url}/balances-transfer`} component={AsyncReactDndComponent} /> */}
			{/* <Route path={`${match.url}/withdrawal-balances`} component={AsyncWithdrawalBalancesComponent} /> */}
			<Route path={`${match.url}/Dealerprint-balances`} component={AsyncDealerPrintBalancesComponent} />
		</Switch>
	</div>
);

export default DragAndDrop;
