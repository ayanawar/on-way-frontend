import React, { useState , useEffect, useRef } from 'react';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print';
import PrintPdf from '../../alaimtidad-balances/print-balances/print-pdf';

const cookies = new Cookies();


export default function Shop(props) {

	const history = useHistory();

	const componentRef = useRef();

	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
        onAfterPrint: () => { 
            setState({
                ...state, 
				disabled: true
            })
            location.reload()
        },
	});

	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		phoneNumber: '',
		phoneNumberField: '',
		user_type_id: '',
		user_phonenumber: '',
		receipts: [],
		cardsForPrintPdf: [],
		disabled: false,
		from_date: '',
		to_date: ''

	});

	useEffect(() => {
		var date = new Date();
		date = date.getUTCFullYear() + '-' +
			('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
			('00' + date.getUTCDate()).slice(-2);

		setState({
			...state,
			from_date: date,
			to_date: date,
		})

	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const showDetails = e => {
		axios.post('http://localhost:8000/printedcards',
			{
				'fromDate': state.from_date,
				'toDate': state.to_date,
				'userPhoneNumber': state.user_phonenumber,
                'isAll': false,
                'userTypeId': localStorage.getItem("user_type_id")
			}, USER_TOKEN).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.message === "No User With This Number") {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: [],
						})
					}
					else if (res2.data.DebitLimit.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: res2.data.DebitLimit,
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.DebitLimit,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const showDetails2 = e => {
		axios.post('http://localhost:8000/printedcards',
			{
				'fromDate': state.from_date,
				'toDate': state.to_date,
                'isAll': true,
                'userTypeId': localStorage.getItem("user_type_id")
			}, USER_TOKEN).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.DebitLimit.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: res2.data.DebitLimit,
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.DebitLimit,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const nf = new Intl.NumberFormat();

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
	
		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	let totalBalancesToBePrinted = 0;
	let selectedBalances = [];
	let objForPrintPdf = {};
	let objArr = [];
	const handlemultiSelect = (selectedRows,displayData) => {
		
		selectedBalances = [];
		objArr = [];
		objForPrintPdf = {};

		selectedRows.data.forEach(element => {
			selectedBalances.push(parseLocaleNumber(displayData[element.index].data[4]));

			objForPrintPdf = {
				"pin": displayData[element.index].data[2],
				"serial": displayData[element.index].data[1],
				"balance": displayData[element.index].data[4],
				"date": displayData[element.index].data[8],
			};
			objArr.push(objForPrintPdf)
		});
		
		totalBalancesToBePrinted = 0;
		for (let i = 0; i< selectedBalances.length; i++) {
			totalBalancesToBePrinted += selectedBalances[i];	
		}
		return(
			<React.Fragment>
				<div className="form-group col-md-12 mt-3 text-center">
					<h2 className="mb-3"><IntlMessages id="form.balancesToPrint" /></h2>
					<h2>{nf.format(totalBalancesToBePrinted)}</h2>
					<br />
					<div className="container">
						<div className="row justify-content-center">	
							{(state.disabled === false) ? 
								<button className="btn btn-primary ml-4 mr-3" 
										onClick={(e) => downloadPdf()}>
									طباعة PDF
								</button>
								: 
								<button className="btn btn-primary ml-4 mr-3" 
										disabled={true}>
									طباعة PDF
								</button>
							}
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}

	const downloadPdf = () => {
		setState({ ...state, cardsForPrintPdf: objArr })
		setTimeout(function () { handlePrint() }, 2000)
    }

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5, 10, 25, 50, 100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		customToolbarSelect: handlemultiSelect,
		selectableRowsHeader: true,
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "330px"
	};

	const { match } = props;
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="cart-wrapper">
					<ToastContainer />
					<Helmet>
						<title>يومية الصندوق</title> 
						<meta name="description" content="يومية الصندوق" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.dailybox" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.dailybox" />}>
							<div className="row">
								<div className="col-sm-6 col-lg-6 col-xl-6">
									<h3><IntlMessages id="form.dateFrom" /></h3>
									<input name="from_date" placeholder="date placeholder"
										defaultValue={state.from_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.from_date && errors.from_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								<div className="col-sm-6 col-lg-6 col-xl-6">
									<span><IntlMessages id="form.dateTo" /></span>
									<input name="to_date" placeholder="date placeholder"
										defaultValue={state.to_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.to_date && errors.to_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
							</div>


                            <div className="form-row">
                            <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.reversebox" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
									 <button type="submit" className="btn-margin"  style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
										<IntlMessages id="widgets.search" />
									</button>
                                     </div>
									 <div style={{borderColor:'#000',borderWidth:'100px',border:'ridge',padding: "10px",backgroundColor:'#D2D2D2'}}>
                                   <div className="form-row">
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Creditor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>-</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Debitor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} /> 
                                       
                                     </div>

                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>=</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="widgets.results" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     </div>
                                     </div>

							{/* <div className="form-row">
								<div className="form-group col-md-12">
									<div className="row">
										<div className="form-group col-md-4">
											<label><IntlMessages id="form.searchBy" /></label>
											<select className="form-control" name="search_by"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار وسيلة البحث</option>
												<option key="1" value="toUserId">الكل</option>
												<option key="2" value="phoneNumber">رقم الهاتف</option>
											</select>
											<span className="errors">
												{errors.search_by && errors.search_by.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
										{(state.search_by != "toUserId") ? <React.Fragment></React.Fragment> :
											<Form inline onSubmit={handleSubmit(showDetails2)}>
												<Button className="mb-10" color="primary" type="submit"><IntlMessages id="form.showDetails" /></Button>
											</Form>
										}
										{(state.search_by != "phoneNumber") ? <React.Fragment></React.Fragment> :
											<Form inline onSubmit={handleSubmit(showDetails)}>
												<FormGroup className="mr-10 mb-10">
													<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
													<input type="text" className="form-control mr-2 ml-2" name="user_phonenumber"
														onChange={handleFields}
														ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
												</FormGroup>

												<span className="errors m-2">
													{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
														<IntlMessages id="form.requiredError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
														<IntlMessages id="form.numbersOnlyErrorError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
														<IntlMessages id="form.minPhoneLengthError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
														<IntlMessages id="form.minPhoneLengthError" />}
												</span>
												<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
											</Form>
										}
									</div>
								</div>
							</div> */}

							<br />
							<br />
							<div style={{ display: "none" }}>
            					<PrintPdf ref={componentRef} {...state} />	
        					</div>
							<RctCard>
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											data={state.receipts}
											columns={[
                                              
												{
													label: <IntlMessages id="form.accountnumber" />,
													name: "virtual_mony_cards_category_value"

												},
												{
													label: <IntlMessages id="form.cointype" />,
													name: "virtual_money_balance"
												},
												{
													label: <IntlMessages id="form.Creditor" />,
													name: "userName"
												},
												{
													label: <IntlMessages id="form.Debitor" />,
													name: "user_phonenumber"

												},
                                                {
													label: <IntlMessages id="widgets.note" />,
													name: "user_phonenumber"

												},
										
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>
					</div>
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}