/**
 * Ecommerce Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    AsyncAccountingTree,
    Asynceditaccounts,
    Asyncdailybox,
     // AsyncShopGridComponent,
     // AsyncInvoiceComponent,
     // AsyncShopComponent,
     // AsyncCartComponent,
     // AsyncCheckoutComponent
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Ecommerce = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title> شجرة الحسابات</title>
             <meta name="description" content="شجرة الحسابات" />
         </Helmet>
         <Switch>
         <Route path={`${match.url}/addaccount`} component={AsyncAccountingTree} />
         <Route path={`${match.url}/editaccount`} component={Asynceditaccounts} />
         <Route path={`${match.url}/dailybox`} component={Asyncdailybox} />
             {/* <Redirect exact from={`${match.url}/`} to={`${match.url}/sold-cards`} />
             <Route path={`${match.url}/sold-cards`} component={AsyncShoplistComponent} />
             <Route path={`${match.url}/available-cards`} component={AsyncShopGridComponent} />
             <Route path={`${match.url}/received-bundles`} component={AsyncInvoiceComponent} />
             <Route path={`${match.url}/add-suppliers`} component={AsyncShopComponent} />
             <Route path={`${match.url}/all-suppliers`} component={AsyncCartComponent} />
             <Route path={`${match.url}/checkout`} component={AsyncCheckoutComponent} /> */}
         </Switch>
     </div>
 );
 
 export default Ecommerce;
 