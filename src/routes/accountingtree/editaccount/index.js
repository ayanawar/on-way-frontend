/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import { HashLink } from 'react-router-hash-link';
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     accountCurrencyIdupdate:'',
     accountTypeIdupdate: '',
     accountCurrencyIdupdateName:'',
     accountTypeIdupdateName: '',
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
     // state = {
     // 	suppliers: [],
     // };
    
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
        level1:0,
        level2:0,
        level3:0,
        accountNameAr: '',
         accountNameEn: '',
         acc_id:'',
         accountDescription: '',
         securityLevel: '',
         accountTypeId: '',
         audit_type_id:'',
         accountStatusId:'',
         accountCurrencyId:'',
         acc_type:'',
         disabled: false,
         hiddenlvl2:true,
         hiddenlvl3:true,
         levcels2:[],
         levcels3:[],
         currency_id:'',
         types: [],
         status:'',
         levels: [],
         currency: [],
         accounts: [],
         status:[],
         hiddenStatusForm: true,
         selected_user:'',
     });
     useEffect(() => {
         // all suppliers
         axios.get('https://accbackend.alaimtidad-itland.com/getallstatus', USER_TOKEN).then(response8 => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
            axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
               axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                   axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
                if (response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized"|| response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response8.data == "Token Expired" || response8.data == "Token UnAuthorized" ) {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else {
            
                    setState({
                      ...state,
                        status:response8.data.message,
                        levels: response.data.levels,
                        types: response3.data.message,
                        currency:response4.data.message,
                        accounts:response5.data.accounts,
                        disabled: false,
                        
                    })
                }
            }).catch(error => {
                if (error.response8.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
           }).catch(error => {
               if (error.response3.status === 429) {
                   toast.error(error.response.data, {
                      position: "top-center",
                      autoClose: 4000,
                      hideProgressBar: false,
                      closeOnClick: true,
                      pauseOnHover: true,
                      draggable: true
                   }); 
               }
           });
        }).catch(error => {
            if (error.response4.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });

       }).catch(error => {
           if (error.response5.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
           }
       });
     }, []);
 
     const onClearStatusClicked = () => {
         setState({...state,
                hiddenStatusForm: true,
                hiddenDebitLimitForm: true,
                accountNameAr: '',
                accountNameEn: '',
                accountDescription: '',
                securityLevel: '',
                accountTypeId: '',
                accountStatusId:'',
                accountCurrencyId:'',
         })
     };
 
     const onSubmit2 = e => {
let data={

    accountNumber:state.selected_user[2],
    accountNameAr:state.accountNameAr,
    accountNameEn:state.accountNameEn,
    accountDescription:state.accountDescription,
    securityLevel:state.securityLevel,
    accountTypeId:parseInt(state.accountTypeId),
    accountStatusId:state.accountStatusId,
    accountCurrencyId:parseInt(state.accountCurrencyId),
}


         axios({
             method: 'post', url: 'https://accbackend.alaimtidad-itland.com/updateaccount', data: data,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else{
             toast.success(<IntlMessages id="updatedsuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
             setState({ ...state, disabled: true })
              setTimeout(function () { location.reload()}, 3000)
            }
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
    
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })
 
 //    render() {
       const { match } = props;
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>قائمة الحسابات</title>
                 <meta name="description" content="قائمة الحسابات" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.editaccount" />} match={match} />
             <div className="row">
                <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={"تعديل بيانات الحسابات"}>
                     {/*  */}
                     {(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
                     : 
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                        <label  id="top"> ستقوم بتعديل الحساب رقم {state.selected_user[2]}</label>
                         <div className="form-row">
                             
                      
                             <div className="form-group col-md-4">
                             <label><IntlMessages id="widgets.status" /></label>
                 
                             <select className="form-control" name="accountStatusId" 
                                     onChange={handleFields} ref={register({ required: true })}>  
                                     <option key="0" value="">اختر الحالة</option>
                                     {state.status.map(currency => {
												return (
													<option key={currency.status_id} value={currency.status_id}>
														{currency.status_ar}
													</option>
												)
											})
											}    
                             </select>
                             <span className="errors">
                                 {errors.accountStatusId && errors.accountStatusId.type === 'required' &&
                                     <IntlMessages id="form.requiredOptionError" /> }
                             </span>
                             </div>
                         </div>
                         <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountname" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameAr" defaultValue={state.accountNameAr}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i,})}   />
                                                  </div>
                                         <span className="errors">
                                             {errors.accountNameAr && errors.accountNameAr.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountnameEng" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameEn"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} defaultValue={state.accountNameEn}/>
                                            </div>
                                         <span className="errors">
                                             {errors.accountNameEn && errors.accountNameEn.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.cointype" /></label>
                                         <div >
                                           <label className="middletitle">{state.currency_ar}</label>
                          </div>
                      
                                     </div>
                                 </div>
                             
      
 
                                 <div className="form-row">
                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="FORM.DEBT" /></label>
                                         <div >
                                         <label className="middletitle">{state.audit_type_ar}</label>
                          </div>
                    
                                     </div>

                                

                                 


                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.description" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <textarea  className="form-control" name="accountDescription"rows={3} cols={5}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} defaultValue={state.accountDescription} />
                                          </div>
                                         <span className="errors">
                                             {errors.accountDescription && errors.accountDescription.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>

                                     </div>
                                
                                     <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.secretdegree" /></label>
                                         <div >
                          <select className="form-control input-text" name="securityLevel"
                                             onChange={handleFields} ref={register({ required: true })} >
                                             <option key="0" value="">برجاء اختيار درجة السرية</option>
                                             {state.levels.map(level => {
												return (
													<option key={level.security_level_id} value={level.security_level_id}>
														{level.security_level_type_ar}
													</option>
												)
											})
											}
                          </select>
                          </div>
                          <span className="errors">
											{errors.securityLevel && errors.securityLevel.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
                                     </div>

                                 

                                     </div>

                         {(state.disabled === false) ? 
                             <button type="submit" className="btn btn-warning">
                             <IntlMessages id="form.update" />
                             </button> : 
                             <button type="submit" className="btn btn-warning" disabled={true} >
                             <IntlMessages id="form.update" />
                             </button>
                             }
 
                         <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                             <IntlMessages id="form.clear" />
                         </button>
                     </form>
                       }
                </RctCollapsibleCard>
             </div>
             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                     data={state.accounts}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "acc_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                            
                                             {
                                                label: <IntlMessages id="widgets.startDate" />,
                                                name:  "acc_creation_date_time",
                                            },
                                            {
                                              label: <IntlMessages id="form.accountnumber" />,
                                              name:  "acc_number",
                                            }, 
                                          
                                            {
                                                label: <IntlMessages id="form.mainaccountnumber" />,

                                                name:  "acc_parent_id",
                                                options: {
                                                  
                                                    filter: false,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                {(tableMeta.rowData[3] != 0 ) ? 
                                                                <span>{value}</span> : 
        
                                                                <span>---</span>
                                                                
                                                                }
                                                              
                                                            </React.Fragment>
                                                        );
                                                    }
                                                }
                                                
                                            },
                                             {
                                            label:<IntlMessages id="form.accounttype" /> ,
                                            name: "",
                                            options: {
                                                filter: true,
                                                sort: false,
                                                empty: true,
                                                print: false,
                                                customBodyRender: (value, tableMeta, updateValue) => {
                                                return (
                                                    <React.Fragment>
                                                        {(tableMeta.rowData[3] == 0 ) ? 
                                                        <span>رئيسى</span> : 

                                                        <span>فرعى</span>
                                                        
                                                        }
                                                      
                                                    </React.Fragment>
                                                );
                                            }
                                            }
                    
                                        },
                                            // 
                                            // {
                                                

                                            // },
                                             {
                                                 label: <IntlMessages id="form.accountname" />,
                                                 name:  "acc_name_ar",
                                             },
                                             {
                                                 label: <IntlMessages id="form.accountnameEng" />,
                                                 name:  "acc_name_en"
                                             },
                                             {
                                                 label: <IntlMessages id="form.cointype" />,
                                                 name:  "currency_ar",
                                             },
                                             {
                                                 label: <IntlMessages id="FORM.DEBT" />,
                                                 name:  "audit_type_ar"
                                             },
                                            
                                             {
                                                 label: <IntlMessages id="form.description" />,
                                                 name:  "acc_desc"
                                             },
                                             {
                                                 label: <IntlMessages id="form.secretdegree" />,
                                                 name:  "security_level_type_ar"
                                             },
                                             {
                                                 label: <IntlMessages id="form.currentStatus" />,
                                                 name:  "status_ar" 
                                             },
                                       
                                             {
                                                 label: <IntlMessages id="form.update" />,
                                                 name: "",
                                                 options: {
                                                     filter: true,
                                                     sort: false,
                                                     empty: true,
                                                     print: false,
                                                     customBodyRender: (value, tableMeta, updateValue) => {
                                                     return (
                                                         <React.Fragment>
                                                              {/* <HashLink to="#top"> */}
                                                             <button type="button" className="btn btn-warning" 
                                                             onClick={() => {
                                                               
                                                                   setState({
                                                                    ...state , 
                                                                    selected_user:tableMeta.rowData,
                                                                    accountNameAr:tableMeta.rowData[5],
                                                                    accountDescription:tableMeta.rowData[9],
                                                                    accountNameEn:tableMeta.rowData[6],
                                                                    currency_ar: tableMeta.rowData[7],
                                                                    audit_type_ar: tableMeta.rowData[8],
                                                                    accountTypeId: tableMeta.rowData[15],
                                                                    accountCurrencyId: tableMeta.rowData[14],
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true
                                                                   });
                                                                  
                                                     
                                                            }}>
                                                             <IntlMessages id="form.update" />
                                                             </button>
                                                             {/* </HashLink> */}
                                                         </React.Fragment>
                                                     );
                                                 }
                                                 }
                         
                                             },

                                             {
                                                label: <IntlMessages id="button.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className=" btn btn-danger" 
                                                            onClick={() => {
                                                            axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,USER_TOKEN).then((res) => {
                                                                setState({
                                                                    ...state , 
                                                                    user_id:res.data.userresult[0].user_id,
                                                                    user_type_id: res.data.userresult[0].user_type_id,
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true,
                                                                    dealerFirstName: tableMeta.rowData[1],
                                                                    dealerMiddleName: tableMeta.rowData[2],
                                                                    dealerLastName: tableMeta.rowData[3],
                                                                    currency_ar: tableMeta.rowData[2],
                                                                    audit_type_ar: tableMeta.rowData[3],
                                                                    status: tableMeta.rowData[8],
                                                                })
                                                            }).catch((error) => {
                                                                if (error.response.status === 429) {
                                                                    toast.error(error.response.data, {
                                                                       position: "top-center",
                                                                       autoClose: 4000,
                                                                       hideProgressBar: false,
                                                                       closeOnClick: true,
                                                                       pauseOnHover: true,
                                                                       draggable: true
                                                                    }); 
                                                                }
                                                            })
                                                            }}>
                                                            <IntlMessages id="button.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                         

                                            {
                                                label: "id",
                                                name:  "currency_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label: "id",
                                                name:  "acc_type",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label: "id",
                                                name:  "status",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label: "id",
                                                name:  "security_level_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                         

                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }