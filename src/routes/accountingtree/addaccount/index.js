/**
 * add account
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
import { MDBContainer, MDBInput } from "mdbreact";

 const cookies = new Cookies();
 const Token = cookies.get('UserToken');

 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
        level1:0,
        level2:0,
        level3:0,
        accountNameAr: '',
         accountNameEn: '',
         acc_id:'',
         accountDescription: '',
         securityLevel: '',
         accountTypeId: '',
         audit_type_id:'',
         accountStatusId:1,
         accountCurrencyId:'',
         accountCurrencyIdupdate:'',
         accountTypeIdupdate: '',
         accountCurrencyIdupdateName:'',
         accountTypeIdupdateName: '',
         disabled: false,
         hiddenlvl2:true,
         hiddenlvl3:true,
         levcels2:[],
         levcels3:[],
         types: [],
         levels: [],
         currency: [],
         accounts: [],
         MainAccounts:[],
        radio: '',
     });
 
    const onClick = nr => () => {
    setState({ ...state,radio: nr});
      }


     useEffect(() => {
        axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
         axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
             axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
                axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                    axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
                 if (response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized"|| response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response11.data == "Token Expired" || response11.data == "Token UnAuthorized" ) {
                     cookies.remove('UserToken', { path: '/' })
                     window.location.href = "/signin";
                 }
                 else {
            //   console.log(response11.data.mainAccounts);
                     setState({
                       ...state,
                         levels: response.data.levels,
                         types: response3.data.message,
                         currency:response4.data.message,
                         accounts:response5.data.accounts,
                        MainAccounts:response11.data.mainAccounts,
                         disabled: false,
                         
                     })
                 }
             }).catch(error => {
                 if (error.response11.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     }); 
                 }
             });
            }).catch(error => {
                if (error.response3.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response4.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
         }).catch(error => {
             if (error.response5.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });

        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
     }, []);
 

   

 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
     const handleTwoEvents2 = ({ target }) => {
     
        setState({ ...state, [target.name]: target.value });
       
         let parentid = target.value;
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
                
                 if(response.data.accounts.length == 0){
                  
                     setState({
                         ...state,
                         level2: parentid,
                         level3:0,
                         levcels3: [],
                     })
 
                 }
                 
                 else if(response.data.accounts.length>0){

                    setState({
                        ...state,
                        level2: parseInt(parentid),
                        accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: [],
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:true,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: response.data.accounts,
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
            
                 }
                 else{
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
     
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }
 
     const handleTwoEvents = ({ target }) => {
         


         for(let i=0; i<=state.MainAccounts.length;i++){
         
             
               if(parseInt(target.value) == parseInt(state.MainAccounts[i].acc_number)){

                setState({ ...state, [target.name]: target.value ,
                    accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
                    accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
                    accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
                    accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
                
                });
                let parentid = target.value;
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
                 
                 if(response.data.accounts.length == 0){
                     
                
                     setState({
                         ...state,
                         level1: parentid,
                         levcels2:  [],
                         levcels3:  [],
                         level2:0,
                         level3:0,
                         hiddenlvl2:true,
                         accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
                    accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
                    accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
                    accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
                     })
                 }
                 
                 else if(response.data.accounts.length>0){
                    setState({
                        ...state,
                        level1: parseInt(parentid),
                
                        accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level1: parentid,
                            levcels2:  [],
                            levcels3:  [],
                            level2:0,
                            level3:0,
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:false,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                         ...state,
                         level1: parentid,
                         levcels2: response.data.accounts,
                         levcels3:  [],
                         level3:0,
                         accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                         accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                         accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                         accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
                 }
                 else{
                   
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
             console.log("Catch");
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
                // console.log("entered yess");
                break;
               }
               else {
                //    console.log("n0oooo");
               }

         }
    
        
       
         
     }
 
     const onSubmit = e => {
 
        axios({url:'https://accbackend.alaimtidad-itland.com/addaccount', method:'post',data:{
            level1:parseInt(state.level1),
            level2:parseInt(state.level2),
            level3:parseInt(state.level3),
            accountNameAr:state.accountNameAr,
            accountNameEn:state.accountNameEn,
            accountDescription:state.accountDescription,
            securityLevel:state.securityLevel,
            accountTypeId:state.accountTypeIdupdate,
            accountStatusId:state.accountStatusId,
            accountCurrencyId:state.accountCurrencyIdupdate,

        },
        headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else{
             toast.success(<IntlMessages id="form.addAccountSuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
      
             setState({ ...state, disabled: true })
             setTimeout(function () { location.reload()}, 3000)
            }
         }).catch(error => {
             
  
                if (error.response.data.message) {
                    toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                } else if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
    }
     const onSubmitMain = e => {
    //    console.log("in submit");
     
       axios({url:'https://accbackend.alaimtidad-itland.com/addaccount',method:'post',data:{
                level1:state.level1,
                level2:state.level2,
                level3:state.level3,
                accountNameAr:state.accountNameAr,
                accountNameEn:state.accountNameEn,
                accountDescription:state.accountDescription,
                securityLevel:state.securityLevel,
                accountTypeId:state.accountTypeId,
                accountStatusId:state.accountStatusId,
                accountCurrencyId:state.accountCurrencyId,
            },
            headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else{
                 toast.success(<IntlMessages id="form.addAccountSuccess" />, {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                 });
      
                 setState({ ...state, disabled: true })
                 setTimeout(function () { location.reload()}, 3000)
                }
             }).catch(error => {
                 
                 if (error.response.data.message) {
                     toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
                         position: "top-center",
                         autoClose: 4000,
                         hideProgressBar: false,
                         closeOnClick: true,
                         pauseOnHover: true,
                         draggable: true
                     });
                 } else if (error.response.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     }); 
                 }
             });
     }
 
     const { match } = props;

     return (
        // console.log("levcels2",state),
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="container">
                     <ToastContainer />
                     <Helmet>
                         <title>إضافة حساب</title>
                         <meta name="description" content="إضافة حساب" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.addaccount" />} match={match} />
                     <div className="row">
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.addaccount" />}>
                             <form>
                                 <div className="form-row">
                              
                               
                             
                                 <MDBContainer  className="mt-5 row">
                                 <label style={{marginLeft:'60px'}}><IntlMessages id="form.chooseaccounttype" /></label>
                                 <label><IntlMessages id="form.main" /></label>
                                 <MDBInput onClick={onClick(1)} checked={state.radio===1 ? true : false}  type="radio" id="radio1" />
                                 <label><IntlMessages id="form.branch" /></label>
                                 <MDBInput onClick={onClick(2)} checked={state.radio===2 ? true : false}  type="radio" id="radio2" />
                                </MDBContainer>
                                     </div>

                                     {(state.radio === 1) ? 

                                      <>
                          <label  style={{marginBottom:'20px', marginTop:'20px'}} ><IntlMessages id="form.main" /></label>

                          <form onSubmit={handleSubmit(onSubmitMain)}>
                                 <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountname" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameAr"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
                                                  </div>
                                         <span className="errors">
                                             {errors.accountNameAr && errors.accountNameAr.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountnameEng" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameEn"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
                                            </div>
                                         <span className="errors">
                                             {errors.accountNameEn && errors.accountNameEn.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.cointype" /></label>
                                         <div >
                          <select className="form-control" name="accountCurrencyId"
                                             onChange={handleFields} ref={register({ required: true })} >
                                             <option key="0" value="">برجاء اختيار العملة</option>
                               
                                             {state.currency.map(currency => {
												return (
													<option key={currency.currency_id} value={currency.currency_id}>
														{currency.currency_ar}
													</option>
												)
											})
											}
                          </select>
                          </div>
                          <span className="errors">
											{errors.accountCurrencyId && errors.accountCurrencyId.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
                                     </div>
                                 </div>
                             
      
 
                                 <div className="form-row">
                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="FORM.DEBT" /></label>
                                         <div >
                          <select className="form-control" name="accountTypeId"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار  النوع</option>
                                             {state.types.map(type => {
												return (
													<option key={type.audit_type_id} value={type.audit_type_id}>
														{type.audit_type_ar}
													</option>
												)
											})
											}
                          </select>
                          </div>
                          <span className="errors">
											{errors.accountTypeId && errors.accountTypeId.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
                                     </div>

                                

                                 


                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.description" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <textarea  className="form-control" name="accountDescription"rows={3} cols={5}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                          </div>
                                         <span className="errors">
                                             {errors.accountDescription && errors.accountDescription.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>

                                     </div>
                                
                                     <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.secretdegree" /></label>
                                         <div >
                          <select className="form-control input-text" name="securityLevel"
                                             onChange={handleFields} ref={register({ required: true })} >
                                             <option key="0" value="">برجاء اختيار درجة السرية</option>
                                             {state.levels.map(level => {
												return (
													<option key={level.security_level_id} value={level.security_level_id}>
														{level.security_level_type_ar}
													</option>
												)
											})
											}
                          </select>
                          </div>
                          <span className="errors">
											{errors.securityLevel && errors.securityLevel.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
                                     </div>

                                 

                                     </div>


                                 {(state.disabled === false) ? 
                                     <button type="submit" className=" btn-margin"  style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
                             </form>
 
                        
                                      </>


                                     : (state.radio === 2) ?
                                  

                                     <>
  <label  style={{marginBottom:'20px', marginTop:'20px'}} ><IntlMessages id="form.branch" /></label>

                           
                          <form onSubmit={handleSubmit(onSubmit)}>
                          <div className="form-row">
                          <label><IntlMessages id="form.addparentaccount" /></label>
                          </div>
                          <div className="form-row">
                       


                          <div className="form-group col-md-4">
                       
                        
                       <div >
                       <select className="form-control" name="level1"
                                          onChange={handleTwoEvents} >
                                          <option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
                            
                                          {state.MainAccounts.map(account => {
                                             return (
                                                 <option key={account.acc_number} value={account.acc_number}>
                                                     {account.acc_name_ar}
                                                 </option>
                                             )
                                         })
                                         }
                       </select>
                       </div>
                       <span className="errors">
                                         {errors.level1 && errors.level1.type === 'required' &&
                                             <IntlMessages id="form.requiredOptionError" />}
                                     </span>
                       </div>
                          {(state.hiddenlvl2 == true)?
                          
                          <span></span>
                        :
                        
                        <div className="form-group col-md-4">
                       
                        
                        <div >
                        <select className="form-control" name="level2"
                                           onChange={handleTwoEvents2} >
                                           <option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
                             
                                           {state.levcels2.map(account => {
                                              return (
                                                  <option key={account.acc_number} value={account.acc_number}>
                                                      {account.acc_name_ar}
                                                  </option>
                                              )
                                          })
                                          }
                        </select>
                        </div>
                        {/* <span className="errors">
                                          {errors.level2 && errors.level2.type === 'required' &&
                                              <IntlMessages id="form.requiredOptionError" />}
                                      </span> */}
                        </div>
                        }
                      {(state.levcels3.length == 0)?
                          
                          <span></span>
                        :


                      <div className="form-group col-md-4">
                       
                        
                       <div >
                       <select className="form-control" name="level3"
                                          onChange={handleFields} >
                                          <option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
                            
                                          {state.levcels3.map(account => {
                                             return (
                                                 <option key={account.acc_number} value={account.acc_number}>
                                                     {account.acc_name_ar}
                                                 </option>
                                             )
                                         })
                                         }
                       </select>
                       </div>
                       {/* <span className="errors">
                                         {errors.level3 && errors.level3.type === 'required' &&
                                             <IntlMessages id="form.requiredOptionError" />}
                                     </span> */}
                       </div>}
                                     </div>
                                 <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountname" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameAr"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
                                                 </div>
                                         <span className="errors">
                                             {errors.accountNameAr && errors.accountNameAr.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameAr && errors.accountNameAr.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountnameEng" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" name="accountNameEn"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
                                          </div>
                                         <span className="errors">
                                             {errors.accountNameEn && errors.accountNameEn.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountNameEn && errors.accountNameEn.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.cointype" /></label>
                                         <div >
                                         <label>{state.accountCurrencyIdupdateName}</label>
                          {/* <select className="form-control" name="accountCurrencyId"
                                             onChange={handleFields} ref={register({ required: true })} >
                                             <option key="0" value="">برجاء اختيار العملة</option>
                      
                          {state.currency.map(currency => {
												return (
													<option key={currency.currency_id} value={currency.currency_id}>
														{currency.currency_ar}
													</option>
												)
											})
											}
                          </select> */}
                          </div>
                       
                                     </div>
                                 </div>
                             
      
 
                                 <div className="form-row">
                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="FORM.DEBT" /></label>
                                         <div >
                                         <label>{state.accountTypeIdupdateName}</label>
                          </div>
                       
                                     </div>

                                    



                                 <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.description" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}> 
                                           <textarea  className="form-control" name="accountDescription"   rows={3} cols={5}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                            </div>
                                         <span className="errors">
                                             {errors.accountDescription && errors.accountDescription.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.accountDescription && errors.accountDescription.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>

                                     </div>
                                
                                     <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.secretdegree" /></label>
                                         <div >
                                         <select className="form-control input-text" name="securityLevel"
                                             onChange={handleFields} ref={register({ required: true })} >
                                             <option key="0" value="">برجاء اختيار درجة السرية</option>
                                             {state.levels.map(level => {
												return (
													<option key={level.security_level_id} value={level.security_level_id}>
														{level.security_level_type_ar}
													</option>
												)
											})
											}
                          </select>
                          </div>
                          <span className="errors">
											{errors.securityLevel && errors.securityLevel.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
                                     </div>

                                 

                                     </div>
                               

                                 {(state.disabled === false) ? 
                                     <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className="btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
                             </form>
                                 </>

                          :

                        <>


                    
                        </>
                                }
                             </form>
 
 
                         </RctCollapsibleCard>
                     </div>
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }
         </React.Fragment>
     )
 }