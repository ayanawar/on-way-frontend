import React, { useState } from 'react';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const cookies = new Cookies();

const options = {
	filter: true,
	filterType: 'dropdown',
	rowsPerPage: 5,
	rowsPerPageOptions: [5, 10, 25, 50, 100],
	responsive: 'vertical',
	enableNestedDataAccess: '.',
	selectableRows: "none",
	viewColumns: false,
	sort: false,
	download: false,
	fixedHeader: true,
	fixedSelectColumn: false,
	tableBodyHeight: "330px"
};

export default function Shop(props) {

	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';

	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		phoneNumber: '',
		phoneNumberField: '',
		user_type_id: '',
		user_phonenumber: '',
		receipts: [],
		showExportExcel: false,
		from_date: '',
		to_date: ''

	});

	React.useEffect(() => {
		var date = new Date();
		date = date.getUTCFullYear() + '-' +
			('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
			('00' + date.getUTCDate()).slice(-2);
		// console.log(date);

		setState({
			...state,
			from_date: date,
			to_date: date
		})
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const showDetails = e => {
		axios.post('http://localhost:8000/admin/getrealmoneytransbytypeandphone',
			{
				'from_date': state.from_date,
				'to_date': state.to_date,
				'user_phonenumber': state.user_phonenumber
			}, USER_TOKEN).then(res2 => {
				// console.log("res ",res2.data);
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.message === "No User With This Number") {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: [],
							showExportExcel: false
						})
					}
					else if (res2.data.message.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: res2.data.message,
							showExportExcel: false
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.message,
							showExportExcel: true
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const showDetails2 = e => {
		axios.post('http://localhost:8000/admin/getrealmoneytransbytypeandphone',
			{
				'from_date': state.from_date,
				'to_date': state.to_date,
				'user_type_id': state.user_type_id
			}, USER_TOKEN).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.message.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: res2.data.message,
							showExportExcel: false
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.message,
							showExportExcel: true
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		let newArr = []
		newArr = state.receipts.map(p => ({
			"Receipt ID": p.rm_transaction_id,
			"Receipt To": p.TOTYPE,
			"Recipient's Name": p.TOName,
			"Recipient's Phone": p.To_Phone,
			"Collected From": p.FROMTYPE,
			"Sender's Name": p.FROMName,
			"Sender's Phone": p.From_Phone,
			"Collected Amount": parseLocaleNumber(p.paid_amount),
			"Collection Date": p.creation_date,
			"Bundle Id": p.virtual_mony_bundle_id,
			"Serial": p.virtual_mony_serial_number,
			"PIN": p.virtual_mony_pin_number
		}));

		const ws = XLSX.utils.json_to_sheet(newArr);

		const wb = { Sheets: { 'سندات القبض': ws }, SheetNames: ['سندات القبض'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, `سندات القبض - لل${state.receipts[0].TOName} - ${state.from_date} - ${state.to_date}` + fileExtension);
	}

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="cart-wrapper">
					<ToastContainer />
					<Helmet>
						<title>سندات القبض</title>
						<meta name="description" content="سندات القبض" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.receivableReceipts" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.receivableReceipts" />}>
							<div className="row">
								<div className="col-sm-6 col-lg-6 col-xl-6">
									<h3><IntlMessages id="form.dateFrom" /></h3>
									<input name="from_date" placeholder="date placeholder"
										defaultValue={state.from_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.from_date && errors.from_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								<div className="col-sm-6 col-lg-6 col-xl-6">
									<span><IntlMessages id="form.dateTo" /></span>
									<input name="to_date" placeholder="date placeholder"
										defaultValue={state.to_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.to_date && errors.to_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
							</div>

							<div className="form-row">
								<div className="form-group col-md-12">
									<div className="row">
										<div className="form-group col-md-4">
											<label><IntlMessages id="form.searchBy" /></label>
											<select className="form-control" name="search_by"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار وسيلة البحث</option>
												<option key="1" value="toUserId">المستلم</option>
												<option key="2" value="phoneNumber">رقم الهاتف</option>
											</select>
											<span className="errors">
												{errors.search_by && errors.search_by.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
										{(state.search_by != "toUserId") ? <React.Fragment></React.Fragment> :
											<Form inline onSubmit={handleSubmit(showDetails2)}>
												<FormGroup className="mr-10 mb-10">
													<Label className="form-control mr-2 ml-2"><IntlMessages id="form.receiver" /></Label>
													<select className="form-control" name="user_type_id"
														onChange={handleFields} ref={register({ required: true })}>
														<option key="0" value="">برجاء اختيار المستلم</option>
														<option key="1" value="5">الإدارة</option>
														<option key="2" value="1">المندوبين</option>
														<option key="3" value="2">الوكلاء</option>
														<option key="4" value="3">نقاط البيع</option>
													</select>
												</FormGroup>

												<span className="errors m-2">
													{errors.user_type_id && errors.user_type_id.type === 'required' &&
														<IntlMessages id="form.requiredOptionError" />}
												</span>
												<Button className="mb-10" color="primary" type="submit"><IntlMessages id="form.showDetails" /></Button>
											</Form>
										}
										{(state.search_by != "phoneNumber") ? <React.Fragment></React.Fragment> :
											<Form inline onSubmit={handleSubmit(showDetails)}>
												<FormGroup className="mr-10 mb-10">
													<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
													<input type="text" className="form-control mr-2 ml-2" name="user_phonenumber"
														onChange={handleFields}
														ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
												</FormGroup>

												<span className="errors m-2">
													{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
														<IntlMessages id="form.requiredError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
														<IntlMessages id="form.numbersOnlyErrorError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
														<IntlMessages id="form.minPhoneLengthError" />}
													{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
														<IntlMessages id="form.minPhoneLengthError" />}
												</span>
												<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
											</Form>
										}
									</div>

									{/* <br /> */}
								</div>
							</div>

							<div className="form-group col-md-6">
								<div className="container">
									{console.log("state", state),
										(state.showExportExcel == true) ?
											<React.Fragment>
												<button className="btn btn-primary ml-5 mr-5" onClick={(e) => exportToCSV()}>
													<IntlMessages id="form.exportExcel" />
												</button>
											</React.Fragment>
											:
											<React.Fragment></React.Fragment>
									}
								</div>
							</div>


							<br />
							<br />
							<RctCard>
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											// title={<IntlMessages id="sidebar.dataTable" />}
											data={state.receipts}
											columns={[
												{
													label: <IntlMessages id="table.receivableReceiptId" />,
													name: "rm_transaction_id",
												},
												{
													label: <IntlMessages id="table.receiptTo" />,
													name: "TOTYPE"

												},
												{
													label: <IntlMessages id="table.recipient'sName" />,
													name: "TOName"
												},
												{
													label: 'رقم هاتف المستلم',
													name: "To_Phone"
												},
												{
													label: <IntlMessages id="table.collectFrom" />,
													name: "FROMTYPE"

												},
												{
													label: <IntlMessages id="table.sender'sName" />,
													name: "FROMName",
												},
												{
													label: 'رقم هاتف المحصل منه',
													name: "From_Phone",
												},
												{
													label: <IntlMessages id="table.collectedAmount" />,
													name: "paid_amount"
												},
												// {
												// 	label: <IntlMessages id="table.remainingAmount" />,
												// 	name:  "remaining_amount",

												// },
												{
													label: <IntlMessages id="table.collectionDate" />,
													name: "creation_date",
												},
												{
													label: <IntlMessages id="form.bundleId" />,
													name: "virtual_mony_bundle_id"
												},
												{
													label: <IntlMessages id="form.serialNumber" />,
													name: "virtual_mony_serial_number"
												},
												{
													label: "PIN",
													name: "virtual_mony_pin_number",
												},
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>
					</div>
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}


