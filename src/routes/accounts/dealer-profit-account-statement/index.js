import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { Helmet } from "react-helmet";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { useForm } from 'react-hook-form';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';

export default function Shop(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';
	const { register, handleSubmit, errors } = useForm();
	const history = useHistory();

	const [state, setState] = useState({
		totalAdminProfit: 0,
		totalPOSProfit: 0,
		name: '',
		region: '',
		area: '',
		phoneNumber: '',
		balance: '',
		userIdInTable: '',
		userTypeIdInTable: '',
		type: '',
		hiddenExcelButton: true,
		profitAccountStatement: [],
		from_date: '',
		to_date: ''
	});

	React.useEffect(() => {
		var date = new Date();
		date = date.getUTCFullYear() + '-' +
			('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
			('00' + date.getUTCDate()).slice(-2);
		// console.log(date);

		setState({
			...state,
			from_date: date,
			to_date: date
		})
	}, []);

	const nf = new Intl.NumberFormat();

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`, USER_TOKEN).then(res => {
			if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {

				if (res.data.userresult[0].user_type_id == 2) {
					// console.log("res", res.data);
					setState({
						...state,
						name: res.data.userresult[0].userName,
						region: res.data.tableresult[0].region_arabic_name,
						area: res.data.tableresult[0].area_arabic_name,
						phoneNumber: res.data.tableresult[0].dealer_phone_number,
						balance: nf.format(res.data.tableresult[0].dealer_virtual_money_balance),
						user_id_in_table: res.data.tableresult[0].dealer_id,
						userTypeIdInTable: res.data.userresult[0].user_type_id,
						// userIdInTable: res.data.tableresult[0].dealer_id,
						type: "وكيل",

					})

				}
				else if (res.data.userresult[0].user_type_id != 2) {
					toast.error(<IntlMessages id="toast.NotDealerNumber" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						name: '',
						region: '',
						area: '',
						phoneNumber: '',
						balance: '',
						userIdInTable: '',
						userTypeIdInTable: '',
						type: '',
						pos_company_cards_transaction: [],
						pos_sim_card_transaction: [],
						pos_vm_transaction: []
					});
				}
			}
		}).catch(error => {
			// console.log("err", error);
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 32,
		rowsPerPageOptions: [5, 10, 32, 50, 100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		// onRowClick: handleRowClick,
		sort: false,
		download: false,
		viewColumns: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const handleSearch = () => {
		axios.post('http://localhost:8000/dealerposdiscount',
			{
				'from_date': state.from_date,
				'to_date': state.to_date,
				'dealer_id': state.user_id_in_table,
				'dealer_type_id': state.userTypeIdInTable

			}, USER_TOKEN).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					// console.log("res", res2);
					if (res2.data.message.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							profitAccountStatement: res2.data.message,
							totalPOSProfit: 0,
							hiddenExcelButton: true,
						})
					}
					else {
						setState({
							...state,
							profitAccountStatement: res2.data.message,
							totalPOSProfit: res2.data.totalDealerProfit,
							hiddenExcelButton: false,
						})
					}
				}
			}).catch(error => {
				// console.log(error);
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		let newArr = []
		newArr = state.profitAccountStatement.map(p => ({
			"Invoice Number": p.rm_cards_receipt,
			"Company Name": p.company_name_ar,
			"Category": p.category_text,
			"PIN": p.pin_number,
			"Serial": p.company_cards_serial,
			"Date And Time": p.creation_date,
			"POS Name": p.userName,
			"POS Phone": p.user_phonenumber,
			"Sell Price": parseLocaleNumber(p.admin_sell_price),
			"Purchase Price": parseLocaleNumber(p.purchase_price),
			"Alaimtdad Profit": parseLocaleNumber(p.adminProfit),
			"Dealer Profit": parseLocaleNumber(p.fr_pos_discount_amount)
		}));

		const ws = XLSX.utils.json_to_sheet(newArr);

		const wb = { Sheets: { 'حساب أرباح الوكيل': ws }, SheetNames: ['حساب أرباح الوكيل'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, `حساب أرباح الوكيل - ${state.name} - ${state.from_date} - ${state.to_date}` + fileExtension);
	}

	const { match } = props;
	// console.log("state", state);
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper">
					<ToastContainer />
					<Helmet>
						<title>حساب أرباح الوكيل</title>
						<meta name="description" content="حساب أرباح الوكيل" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.dealerProfit" />} match={props.match} />
					<div className="form-group col-md-12">
						<div className="row">
							<Form inline>
								<FormGroup className="mr-10 mb-10">
									<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
									<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
										onChange={handleFields}
										ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
								</FormGroup>

								<Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
							</Form>
						</div>
						<span className="errors">
							{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
								<IntlMessages id="form.requiredError" />}
							{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
								<IntlMessages id="form.numbersOnlyErrorError" />}
							{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
								<IntlMessages id="form.minPhoneLengthError" />}
							{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
								<IntlMessages id="form.minPhoneLengthError" />}
						</span>

						<br />
						{(state.name === '') ? <div></div> :
							<React.Fragment>
								<div className="container">
									<div className="row">
										<div className="col-md-6 reportsCardsNumberCenter">
											<h2>{state.type}</h2>
										</div>
										<div className="col-md-6 reportsCardsNumberCenter">
											<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
										</div>
									</div>
								</div>
								<br />
								<div className="container">
									<div className="row">
										<div className="col-md-6">
											<h2><IntlMessages id="form.name" />: {state.name}</h2>
											<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
										</div>
										<div className="col-md-6">
											<h2><IntlMessages id="form.region" />: {state.region}</h2>
											<h2><IntlMessages id="form.area" />: {state.area}</h2>
										</div>
									</div>
								</div>
							</React.Fragment>
						}
					</div>
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.dealerProfit" />}>
							<form onSubmit={handleSubmit(handleSearch)}>
								<div className="row">
									<div className="col-sm-4 col-lg-4 col-xl-4">
										<h3><IntlMessages id="form.dateFrom" /></h3>
										<input name="from_date" placeholder="date placeholder"
											defaultValue={state.from_date}
											type="date" className="form-control" onChange={handleFields}
											ref={register({ required: true })} />
										<span className="errors m-2">
											{errors.from_date && errors.from_date.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
									<div className="col-sm-4 col-lg-4 col-xl-4">
										<span><IntlMessages id="form.dateTo" /></span>
										<input name="to_date" placeholder="date placeholder"
											defaultValue={state.to_date}
											type="date" className="form-control" onChange={handleFields}
											ref={register({ required: true })} />
										<span className="errors m-2">
											{errors.to_date && errors.to_date.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
									<br />
									<div className="col-sm-4 col-lg-4 col-xl-4">
										<div className="container">
											<button color="primary" className="btn btn-primary" style={{ marginTop: "24px" }}>
												ابحث
									</button>
										</div>
									</div>
								</div>
							</form>
							<br />

							<div className="row">
								<div className="col-sm-4 col-lg-4 col-xl-4"
									style={{ display: "hidden" }}></div>
								<RctCollapsibleCard
									customClasses="p-80 text-center"
									colClasses="text-center col-sm-4 col-lg-4 col-xl-4">
									<h1 className="reportsCardsNumberCenter">إجمالى أرباح الوكيل</h1>
									<h1 className="reportsCardsNumberCenter">{state.totalPOSProfit}</h1>
								</RctCollapsibleCard>
								<div className="col-sm-4 col-lg-4 col-xl-4"
									style={{ display: "hidden" }}></div>
							</div>

							<div className="form-group col-md-6">
								<br />
								<div className="container">
									{(state.hiddenExcelButton === false) ?
										<React.Fragment>
											<button className="btn btn-primary ml-5 mr-5" onClick={(e) => exportToCSV()}>
												<IntlMessages id="form.exportExcel" />
											</button>
										</React.Fragment>
										:
										<React.Fragment></React.Fragment>
									}
								</div>
							</div>

							<div className="row">
								<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											data={state.profitAccountStatement}
											columns={[
												{
													label: <IntlMessages id="form.invoiceNumber" />,
													name: "rm_cards_receipt",
												},
												{
													label: <IntlMessages id="form.companyName" />,
													name: "company_name_ar",
												},
												{
													label: <IntlMessages id="widgets.category" />,
													name: "category_text",
												},
												{
													label: "PIN",
													name: "pin_number"
												},
												{
													label: "Serial",
													name: "company_cards_serial"
												},
												{
													label: "تاريخ و وقت البيع",
													name: "creation_date"
												},
												{
													label: "اسم نقطة البيع",
													name: "userName"
												},
												{
													label: "رقم الهاتف لنقطة البيع",
													name: "user_phonenumber"
												},
												{
													label: "سعر البيع امتداد",
													name: "admin_sell_price"
												},
												{
													label: "سعر الشراء امتداد",
													name: "purchase_price"
												},
												{
													label: "أرباح الإدارة",
													name: "adminProfit"
												},
												{
													label: "أرباح الوكيل",
													name: "fr_pos_discount_amount"
												},
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCollapsibleCard>
							</div>
						</RctCollapsibleCard>
					</div>
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}

