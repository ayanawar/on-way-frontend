import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Helmet } from "react-helmet";
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
const cookies = new Cookies(); 

export default function BasicTable(props) {
	const history = useHistory();
	const user = localStorage.getItem("user_type_id");
	const userPhone = localStorage.getItem("phoneNumber");
	const [state, setState] = useState({
		virtualMoney: [],
		sumOfBalances: [],
		reps:[],
		dealers:[],
		poss:[],
		sumRepresentativesBalance:0,
		sumDealersBalance: 0,
		sumPOSBalance: 0,
		sumAdministratorsBalance: 0,
		RepresentativesBalanceRM: 0,
		DealersBalanceRM: 0,
		POSBalanceRM: 0,
		AdministratorsBalanceRM: 0
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		const Token = cookies.get('UserToken');
		axios.get('http://localhost:8000/allrepresentatives',USER_TOKEN).then(response1 => {

			axios.get('http://localhost:8000/alldealers',USER_TOKEN).then(response2 => {

				axios({ url: `http://localhost:8000/admin/getallpos/${localStorage.getItem("user_type_id")}`, method: "post", headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response3 => {

					axios.get('http://localhost:8000/gettotaladmin',USER_TOKEN).then(response4 => {
						axios.get('http://localhost:8000/gettotalrep',USER_TOKEN).then(response5 => {
							axios.get('http://localhost:8000/gettotaldealer',USER_TOKEN).then(response6 => {
								axios.get('http://localhost:8000/gettotalpos',USER_TOKEN).then(response7 => {

									if(response1.data == "Token Expired" || response1.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized" || response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response6.data == "Token Expired" || response6.data == "Token UnAuthorized" || response7.data == "Token Expired" || response7.data == "Token UnAuthorized") {
										cookies.remove('UserToken', {path:'/'})
										window.location.href = "/signin";	
									}else{
										for (let i = 0; i < response3.data.message.length; i ++) {
											response3.data.message[i].rm_total_rm = nf.format(response3.data.message[i].rm_total_rm);
											// response3.data.message[i].purchase_price = nf.format(response3.data.message[i].purchase_price);
											// response3.data.message[i].sell_price = nf.format(response3.data.message[i].sell_price);
										}
										for (let i = 0; i < response2.data.dealers.length; i ++) {
											response2.data.dealers[i].dealer_virtual_money_balance = nf.format(response2.data.dealers[i].dealer_virtual_money_balance);
											response2.data.dealers[i].rm_total_rm = nf.format(response2.data.dealers[i].rm_total_rm);
											// response3.data.message[i].sell_price = nf.format(response3.data.message[i].sell_price);
										}
										for (let i = 0; i < response1.data.representatives.length; i ++) {
											response1.data.representatives[i].representative_virtual_mony_balance = nf.format(response1.data.representatives[i].representative_virtual_mony_balance);
											response1.data.representatives[i].rm_total_rm = nf.format(response1.data.representatives[i].rm_total_rm);
											// response3.data.message[i].sell_price = nf.format(response3.data.message[i].sell_price);
										}
										setState({
											...state,
											sumAdministratorsBalance:response4.data.TotalVM,
											AdministratorsBalanceRM:response4.data.TotalRM ,												

											sumRepresentativesBalance:response5.data.TotalVM,
											RepresentativesBalanceRM: response5.data.TotalRM,

											sumDealersBalance:response6.data.TotalVM,
											DealersBalanceRM: response6.data.TotalRM,

											sumPOSBalance:response7.data.TotalVM,																			
											POSBalanceRM: response7.data.TotalRM,
											
											poss:response3.data.message,
											dealers:response2.data.dealers,
											reps:response1.data.representatives
										});
									}
								}).catch(error => {
									if (error.response.status === 429) {
										toast.error(error.response.data, {
										   position: "top-center",
										   autoClose: 4000,
										   hideProgressBar: false,
										   closeOnClick: true,
										   pauseOnHover: true,
										   draggable: true
										}); 
									}
								});	
							}).catch(error => {
								if (error.response.status === 429) {
									toast.error(error.response.data, {
									   position: "top-center",
									   autoClose: 4000,
									   hideProgressBar: false,
									   closeOnClick: true,
									   pauseOnHover: true,
									   draggable: true
									}); 
								}
							});	
						}).catch(error => {
							if (error.response.status === 429) {
								toast.error(error.response.data, {
								   position: "top-center",
								   autoClose: 4000,
								   hideProgressBar: false,
								   closeOnClick: true,
								   pauseOnHover: true,
								   draggable: true
								}); 
							}
						});	
					}).catch(error => {
						if (error.response.status === 429) {
							toast.error(error.response.data, {
							   position: "top-center",
							   autoClose: 4000,
							   hideProgressBar: false,
							   closeOnClick: true,
							   pauseOnHover: true,
							   draggable: true
							}); 
						}
					});	
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}	
				});	
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});		 				
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
		});		
	}, []); 

	//  console.log(state)
	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
            },
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "350px"
	};

	const { match } = props;
	// console.log("ggff", PURE_TOKEN_USER_TYPE);
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
  			<title>كشف حساب الامتداد</title>
  			<meta name="description" content="كشف حساب الامتداد" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.alaimtidadAccount" />} match={match} />
		<div className="row sticky-top">
			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>		
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfRepresentatives" /></h3>
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="sidebar.alaimtidadBalances" />   <span className="reportsCardsNumber ml-3">  {state.sumRepresentativesBalance}</span></h4>
				<br />
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="form.RM" />   <span className="reportsCardsNumber ml-3">  {state.RepresentativesBalanceRM}</span></h4>
			</RctCollapsibleCard>

			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfDealers" /></h3>
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="sidebar.alaimtidadBalances" />   <span className="reportsCardsNumber ml-3">  {state.sumDealersBalance}</span></h4>
				<br />
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="form.RM" />   <span className="reportsCardsNumber ml-3">  {state.DealersBalanceRM}</span></h4>
			</RctCollapsibleCard>

			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfPOS" /></h3>
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="sidebar.alaimtidadBalances" />   <span className="reportsCardsNumber ml-3">  {state.sumPOSBalance}</span></h4>
				<br />
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="form.RM" />   <span className="reportsCardsNumber ml-3">  {state.POSBalanceRM}</span></h4>
			</RctCollapsibleCard>
			
			<RctCollapsibleCard
				customClasses="p-20 text-center"
				colClasses="col-sm-6 col-lg-3 col-xl-3"
			>
				<h3 className="mb-20"><IntlMessages id="form.totalBalancesOfAdministrators" /></h3>
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="sidebar.alaimtidadBalances" />   <span className="reportsCardsNumber ml-3">  {state.sumAdministratorsBalance}</span></h4>
				<br />
				<h4 className="reportsCardsNumberContainer"> <IntlMessages id="form.RM" />   <span className="reportsCardsNumber ml-3">  {state.AdministratorsBalanceRM}</span></h4>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="أرصدة المندوبين"
							data={state.reps}
							columns={[
									{
										label: <IntlMessages id="form.representativeName" />,
										name:  "userName" 
									},
									{
										label: <IntlMessages id="form.phoneNumber" />,
										name:  "user_phonenumber",
									},
									{
										label: <IntlMessages id="form.representativesBalance" />,
										name:  "representative_virtual_mony_balance" 
									},
									{
										label: <IntlMessages id="form.representativesBalanceRM" />,
										name:  "rm_total_rm" 
									},
									{
										label: <IntlMessages id="form.currentStatus" />,
										name:  "active_status_ar",
									},
									{
										label: <IntlMessages id="form.region" />,
										name:  "region_arabic_name",
									}
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="أرصدة الوكلاء"
							data={state.dealers}
							columns={[
									{
										label: <IntlMessages id="form.dealerName" />,
										name:  "DealerName" 
									},
									{
										label: <IntlMessages id="form.phoneNumber" />,
										name:  "user_phonenumber",
									},
									{
										label: <IntlMessages id="form.dealersBalance" />,
										name:  "dealer_virtual_money_balance" 
									},
									{
										label: <IntlMessages id="form.dealersBalanceRM" />,
										name:  "rm_total_rm" 
									},
									{
										label: <IntlMessages id="form.currentStatus" />,
										name:  "active_status_ar",
									},
									{
										label: <IntlMessages id="form.region" />,
										name:  "region_arabic_name",
									}
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="أرصدة نقاط البيع"
							data={state.poss}
							columns={[
									{
										label: <IntlMessages id="table.PosName" />,
										name:  "posFUllName" 
									},
									{
										label: <IntlMessages id="form.phoneNumber" />,
										name:  "pos_phone_number",
									},
									{
										label: <IntlMessages id="form.POSBalance" />,
										name:  "pos_vm_balance" 
									},
									{
										label: <IntlMessages id="form.POSBalanceRM" />,
										name:  "rm_total_rm" 
									},
									{
										label: <IntlMessages id="form.currentStatus" />,
										name:  "active_status_ar",
									},
									{
										label: <IntlMessages id="form.region" />,
										name:  "region_arabic_name",
									}
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
	</div>
	: (
		history.push("/access-denied")
	   )
	}
	   </React.Fragment>
	)
	}
