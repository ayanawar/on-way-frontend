import React, { useState, useEffect } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Helmet } from "react-helmet";
import { useHistory } from 'react-router-dom';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
// import AccessDenied from '../../../container/AccessDenied'
import Cookies from 'universal-cookie';

const cookies = new Cookies();


export default function Shop(props) {
	const history = useHistory();
	// const user = localStorage.getItem("user_type_id");
	// const userPhone = localStorage.getItem("phoneNumber");
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user : undefined,
		userPhone: undefined,
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
        commercialName: '',
        userIdInTable:'',
		userTypeIdInTable: '',
		bundle_id: '',
		serial_number: '',
		pin_number: '',
		userTypeId: 0,
		userId: 0,
		pos_company_cards_transaction: [],
		realmoney_transaction: [],
		vmcards: [],
		vm_transaction: []
	});

	useEffect(() => {
		setState({ 
			user: localStorage.getItem("user_type_id"), 
			userPhone: localStorage.getItem("phoneNumber")
		})
	}, [])


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	
	const showDetails = e => { 
		axios.post('http://localhost:8000/admin/getvmtransaction', 
		{ 	'user_type_id': localStorage.getItem("user_type_id"),
			'user_id': localStorage.getItem("userIdInUsers"),
			'serial_number': state.serial_number
		},USER_TOKEN).then(res2 => {
			// console.log(res2.data);
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin"; 
			}
			else {
				if( res2.data.realmoney_transaction.length == 0 && res2.data.vm_transaction.length == 0 && res2.data.vmcards.length == 0) {
					toast.error(<IntlMessages id="components.NoItemFound" /> , {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						realmoney_transaction: res2.data.realmoney_transaction,
						vm_transaction: res2.data.vm_transaction,
						vmcards: res2.data.vmcards
					})
				} else {
					setState({
						...state,
						realmoney_transaction: res2.data.realmoney_transaction,
						vm_transaction: res2.data.vm_transaction,
						vmcards: res2.data.vmcards
					})
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			}
		});	 
	}

	const showDetails2 = e => { 
		axios.post('http://localhost:8000/admin/getvmtransaction', 
		{ 	'user_type_id': localStorage.getItem("user_type_id"),
			'user_id': localStorage.getItem("userIdInUsers"),
			'bundle_id': state.bundle_id
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin"; 
			}
			else {
			// console.log(res2.data);
			if( res2.data.realmoney_transaction.length == 0 && res2.data.vm_transaction.length == 0 && res2.data.vmcards.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					realmoney_transaction: res2.data.realmoney_transaction,
					vm_transaction: res2.data.vm_transaction,
					vmcards: res2.data.vmcards
				})
			} else {
				setState({
					...state,
					realmoney_transaction: res2.data.realmoney_transaction,
					vm_transaction: res2.data.vm_transaction,
					vmcards: res2.data.vmcards
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			}
		});  
	}

	const showDetails3 = e => {
		axios.post('http://localhost:8000/admin/getvmtransaction', 
		{ 	'user_type_id': localStorage.getItem("user_type_id"),
			'user_id': localStorage.getItem("userIdInUsers"),
			'pin_number': state.pin_number
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin"; 
			}
			else {
			// console.log(res2.data);
			if( res2.data.realmoney_transaction.length == 0 && res2.data.vm_transaction.length == 0 && res2.data.vmcards.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					realmoney_transaction: res2.data.realmoney_transaction,
					vm_transaction: res2.data.vm_transaction,
					vmcards: res2.data.vmcards
				})
			} else {
				setState({
					...state,
					realmoney_transaction: res2.data.realmoney_transaction,
					vm_transaction: res2.data.vm_transaction,
					vmcards: res2.data.vmcards
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			}
		}); 	
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log(PURE_TOKEN_USER_TYPE);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
            <Helmet>
 				<title>حركة أرصدة الامتداد</title>
 				<meta name="description" content="حركة أرصدة الامتداد" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.alaimtidadMovement" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="form.inquireAboutTheMovementOfAlaimtidadBalances" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<div className="form-group col-md-4">
									<label><IntlMessages id="form.searchBy" /></label>
									<select className="form-control" name="search_by" 
											onChange={handleFields} ref={register({ required: true })}>  
											<option key="0" value="">برجاء اختيار وسيلة البحث</option>
											<option key="1" value="bundleId">رقم الكوتة</option> 
											<option key="2" value="serialNumber">الرقم التسلسلى</option>   
											<option key="3" value="PIN">PIN</option>      
									</select>
									<span className="errors">
										{errors.search_by && errors.search_by.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" /> }
									</span>
								</div>
								{(state.search_by != "bundleId") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails2)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.bundleId" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="bundle_id"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 1, })}/>
									</FormGroup>
									<Button className="mb-10" color="primary" type="submit"><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.bundle_id && errors.bundle_id.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.bundle_id && errors.bundle_id.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
								{(state.search_by != "serialNumber") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.serialNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="serial_number"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]*$/, minLength: 1,  })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.serial_number && errors.serial_number.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.serial_number && errors.serial_number.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
								{(state.search_by != "PIN") ? <React.Fragment></React.Fragment> :
								<Form inline>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2">PIN</Label>
										<input type="tel" className="form-control mr-2 ml-2" name="pin_number"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]*$/, minLength: 1,  })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary" onClick={showDetails3}><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.pin_number && errors.pin_number.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.pin_number && errors.pin_number.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
							</div>
							
							<br />
							{/* {(state.name === '') ? <div></div>:
								<React.Fragment>
									<h1>{state.type}</h1>
									<br /> 
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
									{(state.rank === '') ? <div></div>: 
										<React.Fragment>
											<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
											<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
										</React.Fragment>
									}
								</React.Fragment>
							} */}
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.balances" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.vmcards}
								columns={[
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id"
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_serial_number",
									},
									{
										label: "PIN",
										name:  "virtual_mony_pin_number",
									},
									{
										label: <IntlMessages id="form.companyName" />,
										name:  "company_name_ar",
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name:  "virtual_mony_cards_category_value"
									},
									{
										label: <IntlMessages id="form.createdBy" />,
										name:  "creator_userName"
									},
									// {
									// 	label: "الرصيد المتبقي",
									// 	name:  "virtual_money_balance"
									// },
									{
										label: <IntlMessages id="form.ownerUserName" />,
										name:  "current_owner_userName"
									},
									{
										label: <IntlMessages id="form.phoneNumber" />,
										name:  "user_phonenumber"
									},
									{
										label: <IntlMessages id="sidebar.basic" />,
										name:  "vm_transfer_fees"
									},
									{
										label: <IntlMessages id="widgets.status" />,
										name:  "virtual_money_status"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfRM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.realmoney_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "rm_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_serial_number",
									},
									{
										label: "PIN",
										name:  "virtual_mony_pin_number",
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id"
									},
									{
										label: <IntlMessages id="form.paidAmount" />,
										name:  "paid_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.vm_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "virtual_mony_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_transaction_serial_number",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_transaction_value"
									},
									{
										label: <IntlMessages id="form.transferredBalanceValue" />,
										name:  "virtual_money_transaction_transfer_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}
