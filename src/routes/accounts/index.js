/**
 * Drag and Drop Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async components
import {
    AsyncAlaimtidadAccountStatement,
	AsyncRepresentativesAccountStatement,
    // AsyncSuppliersAccountStatement,
    AsyncDealersAccountStatement,
    AsyncPointsOfSaleAccountStatement,
    // AsyncArrearsComponent,
    // AsyncPaymentsReceiptsComponent,
    AsyncReceivableReceiptsComponent,
	// AsyncPayableReceiptsComponent,
	AsyncAlaimtidadMovementOfBalancesComponent,
	AsyncProfitAccountStatement,
	AsyncDealerProfitAccountStatement,
	AsyncCollectAndIssueReceivableReceiptsComponent
	// AsyncWithdrawalAccountStatementComponent
} from 'Components/AsyncComponent/AsyncComponent';

const DragAndDrop = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>الحسابات</title>
			<meta name="description" content="Alaimtidad Balances" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/alaimtidad-account-statement`} />
			<Route path={`${match.url}/profit-account-statement`} component={AsyncProfitAccountStatement} />
			<Route path={`${match.url}/dealer-profit-account-statement`} component={AsyncDealerProfitAccountStatement} />
            <Route path={`${match.url}/alaimtidad-account-statement`} component={AsyncAlaimtidadAccountStatement} />
			<Route path={`${match.url}/representatives-account-statement`} component={AsyncRepresentativesAccountStatement} />
			{/* <Route path={`${match.url}/suppliers-account-statement`} component={AsyncSuppliersAccountStatement} /> */}
			<Route path={`${match.url}/point-of-sale-account-statement`} component={AsyncPointsOfSaleAccountStatement} />
			<Route path={`${match.url}/dealers-account-statement`} component={AsyncDealersAccountStatement} />
            {/* <Route path={`${match.url}/arrears`} component={AsyncArrearsComponent} /> */}
            <Route path={`${match.url}/alaimtidad-movement-of-balances`} component={AsyncAlaimtidadMovementOfBalancesComponent} />
			{/* <Route path={`${match.url}/payments-receipts`} component={AsyncPaymentsReceiptsComponent} /> */}
            <Route path={`${match.url}/receivable-receipts`} component={AsyncReceivableReceiptsComponent} />
            <Route path={`${match.url}/collect-and-issue-receivable-receipts`} component={AsyncCollectAndIssueReceivableReceiptsComponent} />
		</Switch>
	</div>
);

export default DragAndDrop;
