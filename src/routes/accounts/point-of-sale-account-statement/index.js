import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { Helmet } from "react-helmet";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { useForm } from 'react-hook-form';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';

export default function Shop(props) {
	const { register, handleSubmit, errors } = useForm();
	const history = useHistory();
	
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		disabled: false,
		hiddenExcelButton: true,
		rank: '',
        commercialName: '',
		checked: true,
		from_date: '',
		to_date: '',
        userIdInTable:'',
        userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		totalVM: 0,
		totalRM: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: [],
		VM: [],
		RM: [],
		companies: [],
		categoriesByCompany: [],
		companyName: '',
		user_id_in_table:'' 
	});
	
	const nf = new Intl.NumberFormat();

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}							
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiList: {
				root: {
					color: "#092346",
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}        
			},
			MuiPaper: {
				root: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif",
					fontSize: "20px"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
            },
		}
  	})

  	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
  
	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 32,
		rowsPerPageOptions: [5,10,32,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		sort: false,
		download: false,
		viewColumns: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				
			if (res.data.userresult[0].user_type_id == 3) {
				
				setState({
					...state,
					name: res.data.tableresult[0].userName,
					region: res.data.tableresult[0].region_arabic_name,
					area: res.data.tableresult[0].area_arabic_name,
					phoneNumber: res.data.tableresult[0].pos_phone_number,
					balance: res.data.tableresult[0].pos_vm_balance,
					rank: res.data.tableresult[0].pos_rank,
					commercialName: res.data.tableresult[0].pos_commercial_name,
					userTypeIdInTable: res.data.userresult[0].user_type_id,
					userIdInTable: res.data.userresult[0].user_id,
					user_id_in_table:res.data.tableresult[0].pos_id,
					type: "نقطة البيع",
					
				})
			}
			else if (res.data.userresult[0].user_type_id != 3)  {
				toast.error(<IntlMessages id="toast.NotPosNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '',
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});	
	}

	const handleSearch = () => {
		axios.post('http://localhost:8000/getaccountstatement', 
		{ 	
			'from_date': state.from_date,
			'to_date': state.to_date,
			'current_owner_type': state.userTypeIdInTable,
			'current_owner_id': state.userIdInTable,
			'user_id_in_table':state.user_id_in_table
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				if( res2.data.VM.length == 0 && res2.data.RM.length == 0 ) {
					// console.log("fff", res2.data.VM[0]);
					toast.error(<IntlMessages id="components.NoItemFound" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						VM: res2.data.VM,
						RM: res2.data.RM,
						totalVM: res2.data.VM[0].sum_of_vm,
						totalRM: res2.data.RM[0].sum_of_rm,
					})
				}
				else {
					setState({
						...state,
						VM: res2.data.VM,
						RM: res2.data.RM,
						totalVM: res2.data.TotalVM,
						totalRM: res2.data.TotalRM,
					})
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			}
		});		
	}

	const { match } = props;
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
  			<title>كشف حساب نقاط البيع</title>
  			 <meta name="description" content="كشف حساب نقاط البيع" />
  		</Helmet>
  		<PageTitleBar title={<IntlMessages id="sidebar.faq(s)" />} match={match} />
			 <div className="form-group col-md-12">
							<div className="row">
								<Form inline>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
								</Form>
							</div>
								<span className="errors">
									{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
										<IntlMessages id="form.requiredError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
										<IntlMessages id="form.numbersOnlyErrorError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
								</span>
							
							<br />
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<div className="container">
										<div className="row">
											<div className="col-md-6 reportsCardsNumberCenter">
												<h1>{state.type}</h1>
											</div>
											<div className="col-md-6 reportsCardsNumberCenter">
												<h1><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h1>
											</div>
										</div>
									</div>
									<br /> 
									<div className="container">
										<div className="row">
											<div className="col-md-5">
												<h2><IntlMessages id="form.name" />: {state.name}</h2>
												<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
											</div>
											<div className="col-md-3">
												<h2><IntlMessages id="form.region" />: {state.region}</h2>
												<h2><IntlMessages id="form.area" />: {state.area}</h2>	
											</div>
											<div className="col-md-4">
												{(state.rank === '') ? <div></div>: 
													<React.Fragment>
														<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
														<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
													</React.Fragment>
												}
											</div>
										</div>
									</div> 								
								</React.Fragment>
							}
						</div>
					{/* </div> */}
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.faq(s)" />}>
					<form onSubmit={handleSubmit(handleSearch)}>
						<div className="row">
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<h3><IntlMessages id="form.dateFrom" /></h3>
								<input name="from_date" placeholder="date placeholder"
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.from_date && errors.from_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<span><IntlMessages id="form.dateTo" /></span>
								<input name="to_date" placeholder="date placeholder"
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.to_date && errors.to_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<br />
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<div className="container">
									<button color="primary" className="btn btn-primary" style={{ marginTop: "24px"}}>
										ابحث
									</button>
								</div>
							</div>
						</div>
					</form>
					<br />			
				<div className="row">
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-6 col-lg-6 col-xl-6"
					>
						<h3 className="mb-20"><IntlMessages id="form.totalVM" /></h3>
						<h4 className="reportsCardsNumberCenter">{state.totalVM}</h4>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-6 col-lg-6 col-xl-6"
					>
						<h3 className="mb-20"><IntlMessages id="form.totalRM" /></h3>
						<h4 className="reportsCardsNumberCenter">{state.totalRM}</h4>	
					</RctCollapsibleCard>
				</div>
				<div className="form-row mt-4">
					<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
							data={state.VM}
							title={<IntlMessages id="sidebar.alaimtidadBalances" />}
							columns={[
								{
								   label: <IntlMessages id="form.serialNumber" />,
									name:  "virtual_money_id",
									options: {
										display: "none",
										filter: false
								  	}
								},
								{
									label: <IntlMessages id="form.bundleId" />,
									name:  "virtual_mony_bundle_id",
								},
								{
									label: <IntlMessages id="form.serialNumber" />,
									name:  "virtual_mony_serial_number",
								},
								{
									label: "PIN",
									name:  "virtual_mony_pin_number",
								},
								{
									label: <IntlMessages id="form.balanceValue" />,
									name:  "virtual_money_balance"
								},
								{
									label: <IntlMessages id="widgets.status" />,
									name:  "virtual_money_status"
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  "transfer_date"
								}, 
							]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
          		<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
              				data={state.RM}
							title={<IntlMessages id="form.totalCollectedBalances" />}
							columns={[
								{
								   label: <IntlMessages id="sidebar.receivableReceipt" />,
								   name:  "rm_transaction_id",
								},
								{
									label: <IntlMessages id="form.balanceValue" />,
									name:  "sum_of_rm"
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  "creation_date"
								}, 
								]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
				</div>
			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}




