import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { Helmet } from "react-helmet";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { useForm } from 'react-hook-form';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Modal from 'react-awesome-modal';
import Cookies from 'universal-cookie';

export default function Shop(props) {
	const { register, handleSubmit, errors } = useForm();
	const history = useHistory();
	const cookies = new Cookies();
	
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		dealerPoints: 0,
		dealerPointsFormatted: 0,
		type: '',
		checked: true,
		modal1: false,
		modal2: false,
		from_date: '',
		to_date: '',
        userIdInTable:'',
        userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		totalVM: 0,
		totalRM: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: [],
		VM: [],
		RM: [],
		companies: [],
		categoriesByCompany: [],
		companyName: '' ,
		user_id_in_table:'',
		sumGiftPoints: 0
	});
	
	const nf = new Intl.NumberFormat();

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}							
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiList: {
				root: {
					color: "#092346",
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}        
			},
			MuiPaper: {
				root: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif",
					fontSize: "20px"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
            },
		}
  	})

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
  
	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		// onRowClick: handleRowClick,
		sort: false,
		download: false,
		viewColumns: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				
			if (res.data.userresult[0].user_type_id == 2) {
				// console.log(res); 
				setState({
					...state,
					name: res.data.userresult[0].userName,
					region: res.data.tableresult[0].region_arabic_name,
					area: res.data.tableresult[0].area_arabic_name,
					phoneNumber: res.data.tableresult[0].dealer_phone_number,
					balance: nf.format(res.data.tableresult[0].dealer_virtual_money_balance),
					user_id_in_table:res.data.tableresult[0].dealer_id,
					dealerPointsFormatted: nf.format(res.data.tableresult[0].dealer_points),
					dealerPoints: res.data.tableresult[0].dealer_points,
					userTypeIdInTable: res.data.userresult[0].user_type_id,
					userIdInTable: res.data.userresult[0].user_id,
					type: "وكيل",
					modal1: false,	
					modal2: false,
				})		
			}
			else if (res.data.userresult[0].user_type_id != 2)  {
				toast.error(<IntlMessages id="toast.NotDealerNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '',
					modal1: false,
					modal2: false,
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            } else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
	});	
	}

	const handleSearch = () => {
		axios.post('http://localhost:8000/getaccountstatement', 
		{ 	
			'from_date': state.from_date,
			'to_date': state.to_date,
			'current_owner_type': state.userTypeIdInTable,
			'current_owner_id': state.userIdInTable,
			'user_id_in_table':state.user_id_in_table
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {

				setState({
					...state,
					sumGiftPoints: res2.data.sumDealerPoints
				})
				
				if( res2.data.VM.length == 0 && res2.data.RM.length == 0 ) {
					// console.log("fff", res2.data.VM[0]);
					toast.error(<IntlMessages id="components.NoItemFound" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						VM: res2.data.VM,
						RM: res2.data.RM,
						totalVM: res2.data.TotalVM[0].total_dealer_virtual_money_balance,
						totalRM: res2.data.TotalRM[0].sum_remaining_amount,
						sumGiftPoints: res2.data.sumDealerPoints
					})
				}
				else {
					
					setState({
						...state,
						VM: res2.data.VM,
						RM: res2.data.RM,
						totalVM: res2.data.TotalVM,
						totalRM: res2.data.TotalRM,
						sumGiftPoints: res2.data.sumDealerPoints
					})
					
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
			}
		});		
	}
	
	const { match } = props;

	const handleModalForGiftPointsByVM = () => {
		setState({
			...state,
		   modal1: true,
		})
	}

	const Token = cookies.get('UserToken');

	const handleGiftPointsByVM = () => {
		// console.log("hiiii");
		axios({
			method: 'post', url: 'http://localhost:8000/admin/paypoints', data: {
				"dealer_points": state.sumGiftPoints,
				"userTypeId": localStorage.getItem("user_type_id"),
				"ownerPhoneNumber": localStorage.getItem("phoneNumber"),
				"user_id": localStorage.getItem("userIdInUsers"),
				"recieverPhonenumber": state.phoneNumber,
				"dealer_id_in_table": state.user_id_in_table,
				"from_date": state.from_date,
				"to_date": state.to_date

			},headers: { "x-access-token": `${Token}`  }
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				toast.success('تمت تسوية نقاط الهدية بنجاح', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true, modal1: false, })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			} 
		});		
	}

	const handleModalForGiftPointsByPayingDebts = () => {
		setState({
			...state,
		   modal2: true,
		})
	}

	const handleGiftPointsByPayingDebts = () => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/paypointsbyrealmoney', data: {
				"dealer_points": state.sumGiftPoints,
				"user_type_id": localStorage.getItem("user_type_id"),
				"user_id": localStorage.getItem("userIdInUsers"),
				"dealer_type_id": state.userTypeIdInTable,
				"dealer_id": state.user_id_in_table,
				"dealer_id_in_user": state.userIdInTable,
				"from_date": state.from_date,
				"to_date": state.to_date
			}, headers: { "x-access-token": `${Token}`  }
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				toast.success('تمت تسوية نقاط الهدية بنجاح', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true, modal2: false, })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			} else if (error.response.status === 309) {
				setState({ ...state, disabled: true, modal2: false, })
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			} 
		});		
	}

	const closeModel1 = () => {
		setState({
			...state,
		   	modal1: false,
		});
	}

	const closeModel2 = () => {
		setState({
			...state,
		   	modal2: false,
		});
	}

	const resetState1 = () => {
		setState({
			...state,
			disabled: false,
			modal1: false,
			
		})
	}

	const resetState2 = () => {
		setState({
			...state,
			disabled: false,
			modal2: false,
		})
	}

	// console.log(state);

	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>كشف حساب الوكيل</title>
 				<meta name="description" content="كشف حساب الوكيل" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.register" />} match={props.match} />
			<div className="form-group col-md-12">
				<div className="row">
					<Form inline>
						<FormGroup className="mr-10 mb-10">
							<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
							<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
								onChange={handleFields} 
								ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
						</FormGroup>
				
						<Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
					</Form>
				</div>
					<span className="errors">
						{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
							<IntlMessages id="form.requiredError" /> }
						{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
							<IntlMessages id="form.numbersOnlyErrorError" /> }
						{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
							<IntlMessages id="form.minPhoneLengthError" /> }
						{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
							<IntlMessages id="form.minPhoneLengthError" /> }
					</span>
				
				<br />
				{(state.name === '') ? <div></div>:
					<React.Fragment>
						<div className="container">
							<div className="row">
								<div className="col-md-6 reportsCardsNumberCenter">
									<h1>{state.type}</h1>
								</div>
								<div className="col-md-6 reportsCardsNumberCenter">
									<h1><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h1>
								</div>
							</div>
						</div>
						<br /> 
						<div className="container">
							<div className="row">
								<div className="col-md-5">
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
								</div>
								<div className="col-md-3">
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>	
								</div>
							</div>
						</div> 								
					</React.Fragment>
				}
			</div>
					{/* </div> */}
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.register" />}>
					<form onSubmit={handleSubmit(handleSearch)}>
						<div className="row">
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<h3><IntlMessages id="form.dateFrom" /></h3>
								<input name="from_date" placeholder="date placeholder" 
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.from_date && errors.from_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<span><IntlMessages id="form.dateTo" /></span>
								<input name="to_date" placeholder="date placeholder" 
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.to_date && errors.to_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<br />
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<div className="container">
									<button color="primary" className="btn btn-primary" style={{ marginTop: "24px"}}>
										ابحث
									</button>
								</div>
							</div>
						</div>
					</form>
					<br />			
				<div className="row">
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-4 col-lg-4 col-xl-4"
					>
						<h3 className="mb-20"><IntlMessages id="form.totalVM" /></h3>
						<h4 className="reportsCardsNumberCenter">{state.totalVM}</h4>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-4 col-lg-4 col-xl-4"
					>
						<h3 className="mb-20"><IntlMessages id="form.totalRM" /></h3>
						<h4 className="reportsCardsNumberCenter">{state.totalRM}</h4>	
					</RctCollapsibleCard>
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-4 col-lg-4 col-xl-4"
					>
						<h3 className="mb-20">نقاط الهدية</h3>
						<h4 className="reportsCardsNumberCenter">{nf.format(state.sumGiftPoints)}</h4>
							<h3>تسوية نقاط الهدية </h3>
							{(state.sumGiftPoints < 50000) ? 
								<div className="row">
									<div className="col-sm-6 col-md-12 col-lg-12 col-xl-6">
										<button className="btn btn-success p-2 mb-2" 
												disabled={true}>
											بأرصدة الامتداد
										</button>
									</div>
									<div className="col-sm-6 col-md-12 col-lg-12 col-xl-6">
										<button className="btn btn-success p-2 mb-2" 
												disabled={true}>
											تسوية الديون
										</button>
									</div>
								</div>
								:
								<div className="row">
									<div className="col-sm-6 col-md-12 col-lg-12 col-xl-6">
										<button className="btn btn-success p-2 mb-2" 
											onClick={handleModalForGiftPointsByVM}>
											بأرصدة الامتداد
										</button>
									</div>
									<div className="col-sm-6 col-md-12 col-lg-12 col-xl-6">
										<button className="btn btn-success p-2 mb-2" 
												onClick={handleModalForGiftPointsByPayingDebts}>
											تسوية الديون
										</button>
									</div>
								</div>
							}	
					</RctCollapsibleCard>
				</div>

				{state.modal1 == true ?
					<Modal visible={state.modal1} width="550" effect="fadeInUp" onClickAway={()=>{closeModel1(); resetState1()}}>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">تأكيد تسوية نقاط الهدية</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
								<p>هل ترغب فى تسوية النقاط باستخدام أرصدة الامتداد؟</p>
							</div>

							<div className="modal-footer">
								<button type="submit" className="btn btn-primary" 
										onClick={handleGiftPointsByVM}>
									تأكيد
								</button>
								<button type="button" className="btn btn-secondary" 
										onClick={()=>{closeModel1(); resetState1()}}>
									إغلاق
								</button>	
							</div>
						</div>	
					</Modal>
                        :
                        null
                }

				{state.modal2 == true ?
					<Modal visible={state.modal2} width="550" effect="fadeInUp" onClickAway={()=>{closeModel2(); resetState2()}}>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">تأكيد تسوية نقاط الهدية</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
								<p>هل ترغب فى تسوية النقاط عن طريق تسديد ديون الوكيل؟</p>
							</div>

							<div className="modal-footer">
								<button type="submit" className="btn btn-primary" 
										onClick={handleGiftPointsByPayingDebts}>
									تأكيد
								</button>
								<button type="button" className="btn btn-secondary" 
										onClick={()=>{closeModel2(); resetState2()}}>
									إغلاق
								</button>	
							</div>
						</div>	
					</Modal>
                        :
                        null
                }
				<div className="form-row mt-4">
					<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
							title={<IntlMessages id="sidebar.alaimtidadBalances" />}
							data={state.VM}
							columns={[
								{
								   label: <IntlMessages id="form.serialNumber" />,
									name:  "virtual_money_id",
									options: {
										display: "none",
										filter: false
								  	}
								},
								{
									label: <IntlMessages id="form.bundleId" />,
									name:  "virtual_mony_bundle_id",
								},
								{
									label: <IntlMessages id="form.serialNumber" />,
									name:  "virtual_mony_serial_number",
								},
								{
									label: "PIN",
									name:  "virtual_mony_pin_number",
								},
								{
									label: <IntlMessages id="form.balanceValue" />,
									name:  "virtual_money_balance"
								},
								{
									label: <IntlMessages id="widgets.status" />,
									name:  "virtual_money_status"
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  "transfer_date"
								}, 
							]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
          		<div className="form-group col-md-6 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
              				data={state.RM}
							title={<IntlMessages id="form.totalCollectedBalances" />}
							columns={[
								{
								   label: <IntlMessages id="sidebar.receivableReceipt" />,
								   name:  "rm_transaction_id",
								},
								{
									label: <IntlMessages id="form.balanceValue" />,
									name:  "sum_of_rm"
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  "creation_date"
								}, 
							]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
				</div>
			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}

