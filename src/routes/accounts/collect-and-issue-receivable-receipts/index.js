import React, { useEffect, useState, useRef } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Card, Button, CardTitle, CardText, CardHeader, CardFooter } from 'reactstrap';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Helmet } from "react-helmet";
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import axios from 'axios';
import Modal from 'react-awesome-modal';
import PrintReceivableReceipt from './print-receivable-receipt';
import { useReactToPrint } from 'react-to-print';
const cookies = new Cookies(); 

export default function BasicTable(props) {
	const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
 
    const componentRef = useRef();

	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
        onAfterPrint: () => { 
            setState({
                modal: false,
                modalWithMoneyInputField: false,
                modalForConfirmation: false, 
            })
            location.reload()
        },
	});

	const [state, setState] = useState({
		virtualMoney: [],
		sumOfBalances: [],
		reps:[],
		dealers:[],
		poss:[],
        cards: [],
        realMoney: [],
        modal: false,
        modalWithMoneyInputField: false,
        modalForConfirmation: false,
        show: false,
        cardValue: '',
        valueDeterminedByAdmin: '',
        cardValueErorr: '',
        cardOwner: '',
        cardPin: '',
        cardSerial: '',
        cardID: '',
        cardRMCurrentOwnerID: '',
        cardVMCurrentOwnerType: '',
        cardVMBundleID: '',
        cardPhoneNum: '',
        rmtransid: ''
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		const Token = cookies.get('UserToken');
		axios.get('http://localhost:8000/allrepresentatives',USER_TOKEN).then(response1 => {
			axios.get('http://localhost:8000/alldealers',USER_TOKEN).then(response2 => {
				axios({ url: `http://localhost:8000/admin/getallpos/${localStorage.getItem("user_type_id")}`, method: "post", headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response3 => {

                    if (response1.data == "Token Expired" || response1.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
                        cookies.remove('UserToken', {path:'/'})
                        window.location.href = "/signin";	
                    } else {
                        for (let i = 0; i < response3.data.message.length; i ++) {
                            response3.data.message[i].total = nf.format(response3.data.message[i].total);
                        }

                        for (let i = 0; i < response2.data.dealers.length; i ++) {
                            response2.data.dealers[i].total = nf.format(response2.data.dealers[i].total);
                        }

                        for (let i = 0; i < response1.data.representatives.length; i ++) {
                            response1.data.representatives[i].total = nf.format(response1.data.representatives[i].total);
                        }

                        setState({
                            ...state,
                            poss:response3.data.message,
                            dealers:response2.data.dealers,
                            reps:response1.data.representatives
                        });
                    }
                }).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        }); 
                    }
                });	
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    }); 
                }
            });	
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            }
        });		
	}, []); 

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
            },
		}
	})

    const Token = cookies.get('UserToken');

    const closeModel = () => {
		setState({
			...state,
		   	modal: false,
            modalWithMoneyInputField: false,
            modalForConfirmation: false,
            cardValue: '',
            valueDeterminedByAdmin: '',
            cardValueErorr: '',
            cardOwner: '',
            cardPin: '',
            cardSerial: '',
            cardID: '',
            cardRMCurrentOwnerID: '',
            cardVMCurrentOwnerType: '',
            cardVMBundleID: '',
            cardPhoneNum: '',
            rmtransid: ''
		});
	}

    const closeModel2 = () => {
		setState({
			...state,
            modalWithMoneyInputField: false,
            modalForConfirmation: false,
            cardValue: '',
            valueDeterminedByAdmin: '',
            cardValueErorr: '',
            cardOwner: '',
            cardPin: '',
            cardSerial: '',
            cardID: '',
            cardRMCurrentOwnerID: '',
            cardVMCurrentOwnerType: '',
            cardVMBundleID: '',
            cardPhoneNum: '',
            rmtransid: ''
		});
	}

    const closeModel3 = () => {
		setState({
			...state,
            modalForConfirmation: false,
		});
	}

    const handleFields = ({ target }) => {
        if(target.name = "valueDeterminedByAdmin" && target.value > state.cardValue) {
            setState({
                ...state,
                valueDeterminedByAdmin: target.value, 
                cardValueErorr: 'عفواً لا يمكن تخطى المبلغ المتاح للتحصيل  برجاء إدخال رقم أقل أو مساو للمبلغ المتاح'
            });	
        } else {
            setState({ ...state, valueDeterminedByAdmin: target.value, cardValueErorr: '' });	
        }
	};

    const onSubmit = e => {
        setState({
            ...state,
            modalForConfirmation: true,
        })
	}

    const handleConfirmation = () => {
        // console.log("confirmed");
        let data = {};
        if(state.rmtransid != '') {
            if(state.valueDeterminedByAdmin == '') {
                data = {
                    "user_id": localStorage.getItem("userIdInUsers"),
                    "user_type_id": localStorage.getItem("user_type_id"),
                    "virtual_mony_serial_number": state.cardSerial,
                    "virtual_mony_pin_number": state.cardPin,
                    "rm_amount": state.cardValue,
                    "vmcardId": state.cardID,
                    "rm_current_owner_id": state.cardRMCurrentOwnerID,
                    "rm_current_owner_type_id": state.cardVMCurrentOwnerType,
                    "virtual_mony_bundle_id": state.cardVMBundleID,
                    "rmtransid": state.rmtransid
                }
            } else {
                data = {
                    "user_id": localStorage.getItem("userIdInUsers"),
                    "user_type_id": localStorage.getItem("user_type_id"),
                    "virtual_mony_serial_number": state.cardSerial,
                    "virtual_mony_pin_number": state.cardPin,
                    "rm_amount": state.valueDeterminedByAdmin,
                    "vmcardId": state.cardID,
                    "rm_current_owner_id": state.cardRMCurrentOwnerID,
                    "rm_current_owner_type_id": state.cardVMCurrentOwnerType,
                    "virtual_mony_bundle_id": state.cardVMBundleID,
                    "rmtransid": state.rmtransid
                }
            }

        } else {
            if(state.valueDeterminedByAdmin == '') {
                data = {
                    "user_id": localStorage.getItem("userIdInUsers"),
                    "user_type_id": localStorage.getItem("user_type_id"),
                    "virtual_mony_serial_number": state.cardSerial,
                    "virtual_mony_pin_number": state.cardPin,
                    "rm_amount": state.cardValue,
                    "vmcardId": state.cardID,
                    "rm_current_owner_id": state.cardRMCurrentOwnerID,
                    "rm_current_owner_type_id": state.cardVMCurrentOwnerType,
                    "virtual_mony_bundle_id": state.cardVMBundleID,
                }
            } else {
                data = {
                    "user_id": localStorage.getItem("userIdInUsers"),
                    "user_type_id": localStorage.getItem("user_type_id"),
                    "virtual_mony_serial_number": state.cardSerial,
                    "virtual_mony_pin_number": state.cardPin,
                    "rm_amount": state.valueDeterminedByAdmin,
                    "vmcardId": state.cardID,
                    "rm_current_owner_id": state.cardRMCurrentOwnerID,
                    "rm_current_owner_type_id": state.cardVMCurrentOwnerType,
                    "virtual_mony_bundle_id": state.cardVMBundleID,
                }
            }
        }

        axios({
			method: 'post', url: 'http://localhost:8000/payrealmoney', 
            data: data,
            headers: { "x-access-token": `${Token}` }
		}).then(res =>{
			// console.log(res.data.insertId);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {	
				toast.success('تم التحصيل بنجاح', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
                setState({ ...state, receiptID: res.data.insertId });
                setTimeout(function () { handlePrint() }, 2000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
    }

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "350px"
	};

	const { match } = props;
	// console.log("ggff", PURE_TOKEN_USER_TYPE);
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
  			<title>تحصيل و إصدار سندات القبض</title>
  			<meta name="description" content="تحصيل و إصدار سندات القبض" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.collectAnd" />} match={match} />

        {state.modal == true ?
            <Modal visible={state.modal} scrollable={true} width="550" height="600" effect="fadeInUp" onClickAway={closeModel}>
                <div className="modal-content" style={{ height: "600px", overflow: "auto" }}>
                    <div className="modal-header">
                        <h5 className="modal-title">التحصيلات</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {(state.cards.length == 0 && state.realMoney.length == 0) ? 
                        <p>لا يوجد مبالغ ليتم تحصيلها</p> 
                        :
                        <p>برجاء اختيار الكارت المراد تحصيله</p>  
                        }
                        <div>
                            {state.cards.map(card => {
                                return(
                                    <React.Fragment>
                                        <div key={card.transfer_date}>
                                            <Card body className="mb-2">
                                                <CardHeader>
                                                    <CardTitle>
                                                        <h4>اسم المحصل منه: <span>{card.userName}</span></h4> 
                                                        <br />
                                                        <h4><IntlMessages id="form.phoneNumber" />: <span>{card.user_phonenumber}</span></h4>
                                                    </CardTitle>
                                                </CardHeader>
                                                <CardText className="m-3">
                                                    <h4><IntlMessages id="form.serialNumber" />: <span>{card.virtual_mony_serial_number}</span></h4>
                                                    <br />
                                                    <h4>PIN: <span>{card.virtual_mony_pin_number}</span></h4>
                                                    <br />
                                                    <h4>التاريخ: <span>{card.transfer_date}</span></h4>
                                                </CardText>
                                                <CardFooter className="text-muted">
                                                    <div className="row">
                                                        <div className="col-8">
                                                            <h3>المبلغ المطلوب تحصيله:  <span>{nf.format(card.rm_amount)}</span></h3>     
                                                        </div>
                                                        <div className="col-4">
                                                            <div className="container">
                                                                <Button color="info" onClick={() => {
                                                                    setState({
                                                                        ...state,
                                                                        modalWithMoneyInputField: true,
                                                                        cardValue: card.rm_amount,
                                                                        cardOwner: card.userName,
                                                                        cardPin: card.virtual_mony_pin_number,
                                                                        cardSerial: card.virtual_mony_serial_number,
                                                                        cardID: card.virtual_money_id,
                                                                        cardRMCurrentOwnerID: card.virtual_mony_current_owner_id,
                                                                        cardVMCurrentOwnerType: card.virtual_mony_current_owner_type,
                                                                        cardVMBundleID: card.virtual_mony_bundle_id,
                                                                        cardPhoneNum: card.user_phonenumber
                                                                    })
                                                                }}>تحصيل</Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </CardFooter>
                                            </Card>
                                        </div>
                                    </React.Fragment>
                                )
                            })}

                            {state.realMoney.map(rm => {
                                return(
                                    <React.Fragment>
                                        <div key={rm.creation_date}>
                                            <Card body className="mb-2">
                                                <CardHeader>
                                                    <CardTitle>
                                                        <h4>اسم المحصل منه: <span>{rm.userName}</span></h4>
                                                            <br />
                                                        <h4><IntlMessages id="form.phoneNumber" />: <span>{rm.user_phonenumber}</span></h4>
                                                    </CardTitle>
                                                </CardHeader>
                                                <CardText className="m-3">
                                                    <h4><IntlMessages id="form.serialNumber" />: <span>{rm.virtual_mony_serial_number}</span></h4>
                                                    <br />
                                                    <h4>PIN: <span>{rm.virtual_mony_pin_number}</span></h4>
                                                    <br />
                                                    <h4>التاريخ: <span>{rm.creation_date}</span></h4>
                                                </CardText>
                                                <CardFooter className="text-muted">
                                                    <div className="row">
                                                        <div className="col-8">
                                                            <h3>المبلغ المطلوب تحصيله:  <span>{nf.format(rm.remaining_amount)}</span></h3>     
                                                        </div>
                                                        <div className="col-4">
                                                            <div className="container">
                                                                <Button color="info" onClick={() => {
                                                                    setState({
                                                                        ...state,
                                                                        modalWithMoneyInputField: true,
                                                                        cardValue: rm.remaining_amount,
                                                                        cardOwner: rm.userName,
                                                                        cardPin: rm.virtual_mony_pin_number,
                                                                        cardSerial: rm.virtual_mony_serial_number,
                                                                        cardID: rm.vmcardid,
                                                                        cardRMCurrentOwnerID: rm.to_user_id,
                                                                        cardVMCurrentOwnerType: rm.to_type_id,
                                                                        cardVMBundleID: rm.virtual_mony_bundle_id,
                                                                        cardPhoneNum: rm.user_phonenumber,
                                                                        rmtransid: rm.rm_transaction_id
                                                                    })
                                                                }}>تحصيل</Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </CardFooter>
                                            </Card>
                                        </div>
                                    </React.Fragment>
                                )
                            })}
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" 
                                onClick={closeModel}>
                            إغلاق
                        </button> 
                    </div>
                </div>	
            </Modal>
        :
            null
        }

        {state.modalWithMoneyInputField == true ?
            <Modal visible={state.modal} width="550" height="250" effect="fadeInUp" 
                   onClickAway={closeModel2}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">
                            <span className="ml-3"> هل تريد تحصيل هذا المبلغ ؟</span>
                            <span className="ml-3">المبلغ المتاح للتحصيل</span>
                            <span className="ml-3">{nf.format(state.cardValue)}</span>
                        </h4>
                    </div>
                    {/*  */}
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="modal-body">
                            <p>برجاء إدخال المبلغ المراد تحصيله 
                                <span className="ml-3">
                                    {(state.valueDeterminedByAdmin == '') ? 
                                    <React.Fragment>
                                        {nf.format(state.cardValue)}
                                    </React.Fragment>
                                    :
                                    <React.Fragment>
                                        {nf.format(state.valueDeterminedByAdmin)}
                                    </React.Fragment>
                                    } 
                                </span>
                            </p>
                            
                            <input
                                defaultValue={state.cardValue}
                                // value={user_password}
                                type="number"
                                name="valueDeterminedByAdmin"
                                className="form-control p-3"
                                onChange={handleFields}
                                style={{ borderRadius: "15px", fontSize:"17px" }}
                                ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, 
                                    minLength: 1 })}
                                />
                                <br />
                                <span className="errors">
                                    {errors.valueDeterminedByAdmin && errors.valueDeterminedByAdmin.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.valueDeterminedByAdmin && errors.valueDeterminedByAdmin.type === 'pattern' &&
                                        <IntlMessages id="form.numbersOnlyErrorError" /> }
                                    {state.cardValueErorr}
                                </span>
                        </div>
                        <div className="modal-footer">
                            {(state.cardValueErorr == '') ? 
                                <button type="submit" className="btn btn-primary">
                                    تأكيد
                                </button>
                                : 
                                <React.Fragment></React.Fragment>
                            }
                            <button type="button" className="btn btn-secondary" 
                                    onClick={closeModel2}>
                                إغلاق
                            </button>
                        </div>
                    </form>
                </div>	
            </Modal>
        : null
        }

        {state.modalForConfirmation == true ?
            <Modal visible={state.modal} width="550" height="250" effect="fadeInUp" 
                   onClickAway={closeModel3}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h3 className="modal-title"> 
                            <span className="ml-3"> هل أنت متأكد من تحصيل مبلغ</span>
                            
                            <span className="ml-3">
                                {(state.valueDeterminedByAdmin == '') ? 
                                <React.Fragment>
                                    {nf.format(state.cardValue)}
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    {nf.format(state.valueDeterminedByAdmin)}
                                </React.Fragment>
                                } 
                            </span>
                        </h3> 
                        <br />
                    </div>
                    <div className="modal-body">
                        <h4><IntlMessages id="form.serialNumber" />: <span>{state.cardSerial}</span></h4>
                        <br />
                        <h4>PIN: <span>{state.cardPin}</span></h4>
                        <br />
                        <h4>اسم المحصل منه: <span>{state.cardOwner}</span></h4>
                    </div>
                    {/*  */}
                    <div className="modal-footer">
                        <button type="submit" className="btn btn-primary"
                                onClick={handleConfirmation}>
                            تحصيل و إصدار سند القبض
                        </button>                             
                        
                        <button type="button" className="btn btn-secondary" 
                                onClick={closeModel3}>
                            إغلاق
                        </button>
                    </div>
                </div>	
            </Modal>
        : null
        }

        <div style={{ display: "none" }}>
            <PrintReceivableReceipt ref={componentRef} {...state} />				
        </div>
		
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="التحصيل من المندوبين"
							data={state.reps}
							columns={[
                                {
                                    label: '',
                                    name: "user_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },
                                {
                                    label: '',
                                    name: "user_type_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },	
                                {
                                    label: <IntlMessages id="form.representativeName" />,
                                    name:  "userName" 
                                },
                                {
                                    label: <IntlMessages id="form.phoneNumber" />,
                                    name:  "user_phonenumber",
                                },
                                {
                                    label: <IntlMessages id="form.currentStatus" />,
                                    name:  "active_status_ar",
                                },
                                {
                                    label: <IntlMessages id="form.region" />,
                                    name:  "region_arabic_name",
                                }, 
                                {
                                    label: <IntlMessages id="form.area" />,
                                    name:  "area_arabic_name",
                                },
                                {
                                    label: 'المبلغ المطلوب تحصيله',
                                    name:  "total",
                                },
                                {
                                    label: "تحصيل",
                                    name: "",
                                    options: {
                                    filter: true,
                                    sort: false,
                                    empty: true,
                                    customBodyRender: (value, tableMeta, updateValue) => {
                                        return (
                                            <React.Fragment>
                                                <button type="button" className="btn btn-info" 
                                                onClick={() => {
                                                    axios({url:`http://localhost:8000/getrealmoneycards`,method: 'post', 
                                                    data: { "user_id": tableMeta.rowData[0], "user_type_id": tableMeta.rowData[1] },
                                                    headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response => { 
                                                        // console.log(response);
                                                        
                                                        setState({
                                                            ...state,
                                                            modal: true,
                                                            hiddenUpdateForm: false,
                                                            cards: response.data.message,
                                                            realMoney: response.data.realmoney   
                                                        }); 
                                                    }).catch(error => {
                                                        if (error.response.status === 429) {
                                                            toast.error(error.response.data, {
                                                                position: "top-center",
                                                                autoClose: 4000,
                                                                hideProgressBar: false,
                                                                closeOnClick: true,
                                                                pauseOnHover: true,
                                                                draggable: true
                                                            }); 
                                                        }
                                                    }); 
                                                }}>
                                                تحصيل
                                                </button>
                                            </React.Fragment>
                                        );
                                    }
                                    }
                    
                                },
								]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="التحصيل من الوكلاء"
							data={state.dealers}
							columns={[
                                {
                                    label: '',
                                    name: "user_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },
                                {
                                    label: '',
                                    name: "user_type_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },	
                                {
                                    label: <IntlMessages id="form.dealerName" />,
                                    name:  "DealerName" 
                                },
                                {
                                    label: <IntlMessages id="form.phoneNumber" />,
                                    name:  "user_phonenumber",
                                },
                                {
                                    label: <IntlMessages id="form.currentStatus" />,
                                    name:  "active_status_ar",
                                },
                                {
                                    label: <IntlMessages id="form.region" />,
                                    name:  "region_arabic_name",
                                },
                                {
                                    label: <IntlMessages id="form.area" />,
                                    name:  "area_arabic_name",
                                },
                                {
                                    label: 'المبلغ المطلوب تحصيله',
                                    name:  "total",
                                },
                                {
                                    label: "تحصيل",
                                    name: "",
                                    options: {
                                    filter: true,
                                    sort: false,
                                    empty: true,
                                    customBodyRender: (value, tableMeta, updateValue) => {
                                        return (
                                            <React.Fragment>
                                                <button type="button" className="btn btn-info" 
                                                onClick={() => {
                                                    axios({url:`http://localhost:8000/getrealmoneycards`,method: 'post', 
                                                    data: { "user_id": tableMeta.rowData[0], "user_type_id": tableMeta.rowData[1] },
                                                    headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response => { 
                                                        // console.log(response);
                                                        
                                                        setState({
                                                            ...state,
                                                            modal: true,
                                                            hiddenUpdateForm: false,
                                                            cards: response.data.message,
                                                            realMoney: response.data.realmoney   
                                                        }); 
                                                    }).catch(error => {
                                                        if (error.response.status === 429) {
                                                            toast.error(error.response.data, {
                                                                position: "top-center",
                                                                autoClose: 4000,
                                                                hideProgressBar: false,
                                                                closeOnClick: true,
                                                                pauseOnHover: true,
                                                                draggable: true
                                                            }); 
                                                        }
                                                    }); 
                                                }}>
                                                تحصيل
                                                </button>
                                            </React.Fragment>
                                        );
                                    }
                                    }
                    
                                },
							]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							title="التحصيل من نقاط البيع"
							data={state.poss}
							columns={[
                                {
                                    label: '',
                                    name: "user_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },
                                {
                                    label: '',
                                    name: "user_type_id",
                                    options: {
                                        display: "none",
                                        filter: false,
                                        print: false,
                                    }
                                },	
                                {
                                    label: <IntlMessages id="table.PosName" />,
                                    name:  "posFUllName" 
                                },
                                {
                                    label: <IntlMessages id="form.phoneNumber" />,
                                    name:  "pos_phone_number",
                                },
                                {
                                    label: <IntlMessages id="form.currentStatus" />,
                                    name:  "active_status_ar",
                                },
                                {
                                    label: <IntlMessages id="form.region" />,
                                    name:  "region_arabic_name",
                                },
                                {
                                    label: <IntlMessages id="form.area" />,
                                    name:  "area_arabic_name",
                                },
                                {
                                    label: 'المبلغ المطلوب تحصيله',
                                    name:  "total",
                                },
                                {
                                    label: "تحصيل",
                                    name: "",
                                    options: {
                                    filter: true,
                                    sort: false,
                                    empty: true,
                                    customBodyRender: (value, tableMeta, updateValue) => {
                                        return (
                                            <React.Fragment>
                                                <button type="button" className="btn btn-info" 
                                                onClick={() => {
                                                    axios({url:`http://localhost:8000/getrealmoneycards`,method: 'post', 
                                                    data: { "user_id": tableMeta.rowData[0], "user_type_id": tableMeta.rowData[1] },
                                                    headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response => { 
                                                        // console.log(response);
                                                        
                                                        setState({
                                                            ...state,
                                                            modal: true,
                                                            cards: response.data.message,
                                                            realMoney: response.data.realmoney   
                                                        }); 
                                                    }).catch(error => {
                                                        if (error.response.status === 429) {
                                                            toast.error(error.response.data, {
                                                                position: "top-center",
                                                                autoClose: 4000,
                                                                hideProgressBar: false,
                                                                closeOnClick: true,
                                                                pauseOnHover: true,
                                                                draggable: true
                                                            }); 
                                                        }
                                                    }); 
                                                }}>
                                                تحصيل
                                                </button>
                                            </React.Fragment>
                                        );
                                    }
                                    }
                    
                                },
							]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
	</div>
	: (
		history.push("/access-denied")
	   )
	}
	   </React.Fragment>
	)
	}
