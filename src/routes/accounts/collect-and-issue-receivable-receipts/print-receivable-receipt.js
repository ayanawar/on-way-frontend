import moment from 'moment';
import React, { Component } from 'react';
import IntlMessages from 'Util/IntlMessages';
import logo from '../../../assets/img/icon10.jpg';

const nf = new Intl.NumberFormat();

class PrintReceivableReceipt extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { classes } = this.props;
        return (
            <div className="container">    
                <div className="col-md-6 text-center">
                    <img width={200} height={200} src={logo} className="img-fluid" style={{
                        marginLeft: "auto",
                        marginRight: "auto"
                    }} />
                </div>
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h1 className="text-primary" style={styles.headerFont}>سند قبض</h1>
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}>الاسم: <span className="ml-2">{localStorage.getItem("userFirstName")+" "+localStorage.getItem("usermiddleName")+" "+localStorage.getItem("userLastName")}</span></h2>
                        <br />
                        <h5 className="text-danger" style={styles.mainFont}>لقد تم تحصيل من</h5>
                        <h2 className="text-right mr-3" style={styles.font}>اسم المحصل منه: <span className="ml-2">{this.props.cardOwner}</span></h2>
                        <br />
                        <h2 className="text-danger" style={styles.mainFont}>PIN : <span className="ml-2">{this.props.cardPin}</span></h2>
                        <br />
                        <br />
                        <h2 style={styles.font} className="text-right mr-3">رقم الهاتف: <span className="ml-2">{this.props.cardPhoneNum}</span></h2>
                        <br />
                        <h2 style={styles.font} className="text-right mr-3" >   
                            <span className="ml-2 mr-2">
                                {(this.props.valueDeterminedByAdmin == '') ? 
                                <React.Fragment>
                                    {nf.format(this.props.cardValue)}
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    {nf.format(this.props.valueDeterminedByAdmin)}
                                </React.Fragment>
                                }
                            </span>
                            :المبلغ المدفوع
                        </h2>
                        <br />
                        <h2 style={styles.font} className="text-right mr-3"><span className="ml-2">{this.props.cardSerial}</span> :<IntlMessages id="form.serialNumber" /> </h2>
                        <br />
                        <h2 style={styles.font} className="text-right mr-3"><span className="ml-2">{moment().format('YYYY-MM-DD hh:mm:ss A')}</span> :التاريخ</h2>  
                        <br />
                        <h1 style={styles.receiptIdStyle}> رقم سند القبض: <span className="ml-2">{this.props.receiptID}</span></h1>
                    </div>
                </div>
            </div>
        );

    }
}

const styles = {
    headerFont: {
        fontSize: "45px",
    },
    mainFont: {
        fontSize: "28px",
    },
    font: {
        fontSize: "25px",
        marginRight: "100px !important"
    },
    fontStyle: {
        fontSize: "x-large"
    },
    receiptIdStyle: {
        color: "darkgreen",
        fontSize: "30px",
        border: "2px solid darkgreen",
        marginRight: "300px",
        marginLeft: "300px",
        padding: "10px"
    },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default PrintReceivableReceipt;
