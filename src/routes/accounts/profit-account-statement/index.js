import React, { useState, useEffect } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { Helmet } from "react-helmet";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { useForm } from 'react-hook-form';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';

export default function Shop(props) {
	const { register, handleSubmit, errors } = useForm();
	const history = useHistory();
	
	const [state, setState] = useState({
		totalAdminProfit: 0,
		totalRegionsProfit: 0,
		totalSuperDealerProfit: 0,
		totalDealersProfit: 0,
		delaerGiftPoints: 0,
		totalPOSProfit: 0,
		from_date: '',
		to_date: '',
		profitAccountStatement: [],
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		let todaysDate = new Date(); 
    	let year = todaysDate.getFullYear();
    	let month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
    	let day = ("0" + todaysDate.getDate()).slice(-2);
    	let currentDate = (year +"-"+ month +"-"+ day);
			setState({
				...state,
				from_date: currentDate,
				to_date: currentDate
			})	
	}, [])

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
  	})

    const handleFields = ({ target }) => {
	    setState({ ...state, [target.name]: target.value });
	};

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		// onRowClick: handleRowClick,
		sort: false,
		download: false,
		viewColumns: false,
		selectableRowsHeader: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const handleSearch = () => {
		axios.post('http://localhost:8000/getProfitAccountStatement', 
		{ 	
			'from_date': state.from_date,
			'to_date': state.to_date,
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				if( res2.data.message.length == 0 ) {
					toast.error(<IntlMessages id="components.NoItemFound" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						profitAccountStatement: res2.data.message,
						totalAdminProfit: 0,
						totalRegionsProfit: 0,
						totalSuperDealerProfit: 0,
						totalDealersProfit: 0,
						delaerGiftPoints: 0,
						totalPOSProfit: 0
					})
				}
				else {

					for ( let i = 0; i < res2.data.message.length; i++ ) {
						res2.data.message[i].super_dealer_pos_discount_amount = nf.format(res2.data.message[i].super_dealer_pos_discount_amount);
						res2.data.message[i].normal_dealer_pos_discount_amount = nf.format(res2.data.message[i].normal_dealer_pos_discount_amount);
						res2.data.message[i].region_discount_amount = nf.format(res2.data.message[i].region_discount_amount);
					}

					setState({
						...state,
						profitAccountStatement: res2.data.message,
						totalAdminProfit: res2.data.totals[0].total_admin_profit,
						totalRegionsProfit: nf.format(res2.data.totals[0].regiontotalprofit),
						totalSuperDealerProfit: nf.format(res2.data.totals[0].superdealertotalprofit),
						totalDealersProfit: nf.format(res2.data.totals[0].dealertotalprofit),
						delaerGiftPoints: nf.format(res2.data.totals[0].delaerGiftPoints),
						totalPOSProfit: res2.data.totals[0].total_pos_profit
					})
					
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
		});		
	}
	
	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>حساب الأرباح</title>
 				<meta name="description" content="حساب الأرباح" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.profitAccount" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.profitAccount" />}>
					<form onSubmit={handleSubmit(handleSearch)}>
						<div className="row">
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<h3><IntlMessages id="form.dateFrom" /></h3>
								<input name="from_date" placeholder="date placeholder" 
									   value={state.from_date} type="date" 
									   onChange={handleFields} className="form-control"
									   ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.from_date && errors.from_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<span><IntlMessages id="form.dateTo" /></span>
								<input name="to_date" placeholder="date placeholder" 
									   type="date" className="form-control" 
									   onChange={handleFields} value={state.to_date}
									   ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.to_date && errors.to_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<br />
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<div className="container">
									<button color="primary" className="btn btn-primary" style={{ marginTop: "24px"}}>
										ابحث
									</button>
								</div>
							</div>
						</div>
					</form>
					<br />			
				<div className="row">

					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">أرباح الإدارة</h3>
						<h4 className="reportsCardsNumberCenter">{state.totalAdminProfit}</h4>
					</RctCollapsibleCard>
					
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">أرباح زيادات المحافظات</h3>
						<h4 className="reportsCardsNumberCenter">{state.totalRegionsProfit}</h4>
					</RctCollapsibleCard>

					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">أرباح الوكيل السوبر</h3>
						<h4 className="reportsCardsNumberCenter">{state.totalSuperDealerProfit}</h4>
					</RctCollapsibleCard>

					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">أرباح الوكلاء</h3>
						<h4 className="reportsCardsNumberCenter">{state.totalDealersProfit}</h4>
					</RctCollapsibleCard>

					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">هدايا الوكلاء</h3>
						<h4 className="reportsCardsNumberCenter">{state.delaerGiftPoints}</h4>
					</RctCollapsibleCard>

					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-2 col-md-6 col-lg-2 col-xl-2"
					>
						<h3 className="mb-20">أرباح نقاط البيع</h3>
						<h4 className="reportsCardsNumberCenter">{state.totalPOSProfit}</h4>	
					</RctCollapsibleCard>
				</div>
			
                <div className="row">
                    <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                        <MuiThemeProvider theme={getMuiTheme()}>
                            <MUIDataTable 
                            data={state.profitAccountStatement}
                            columns={[
								{
                                    label: <IntlMessages id="form.invoiceNumber" />,
                                    name:  "rm_cards_receipt",
                                },
                                {
                                    label: <IntlMessages id="form.companyName" />,
                                    name:  "company_name_ar",
                                },
                                {
                                    label: <IntlMessages id="widgets.category" />,
                                    name:  "category_text",
                                },
                                {
                                    label: "PIN",
                                    name:  "pin_number"
                                },
                                {
                                    label: "Serial",
                                    name:  "company_cards_serial"
                                }, 
								{
									label: "تاريخ و وقت البيع",
									name: "creation_date"
								},
								{
                                    label: "رقم الهاتف لنقطة البيع",
                                    name:  "user_phonenumber"
                                },
								{
                                    label: "اسم مالك نقطة البيع",
                                    name:  "userName"
                                }, 
                                {
                                    label: "سعر البيع امتداد",
                                    name:  "admin_sell_price"
                                }, 
								{
									label: "سعر الشراء امتداد",
									name: "purchase_price"
								},
                                {
                                    label: "أرباح الإدارة",
                                    name:  "adminProfit",
                                },
                                {
                                    label: "أرباح زيادات المحافظات",
                                    name:  "fr_region_discount_amount",
                                },
                                {
                                    label: "أرباح الوكيل السوبر",
                                    name:  "super_dealer_pos_discount_amount",
                                },
                                {
                                    label: "أرباح الوكلاء",
                                    name:  "normal_dealer_pos_discount_amount"
                                },
								{
                                    label: "هدايا الوكلاء",
                                    name:  "dealer_gift_points_fr"
                                }, 
                                {
                                    label: "أرباح نقاط البيع",
                                    name:  "cards_pos_profit"
                                }
                            ]}
                            options={options}  
                            />
                        </MuiThemeProvider>
                    </RctCollapsibleCard>
                </div>
			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}

