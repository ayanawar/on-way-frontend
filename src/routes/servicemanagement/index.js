/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    Asyncaddservices,
    Asyncallservices,
   
   
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>Service Managment</title>
             <meta name="description" content="addservice" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/addservice`} />
             <Route path={`${match.url}/addservice`} component={Asyncaddservices} />
             <Route path={`${match.url}/allservice`} component={Asyncallservices} />
      
         </Switch>
     </div>
 );
 
 export default Pages;
 