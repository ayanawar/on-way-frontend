
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();
const Token = cookies.get('UserToken'); 

export default function RctPickers(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '1',
		user_first_name: '',
    	user_middle_name: '',
		user_last_name: '',
		representative_id: '',
		user_region_id: '',
   		user_area_id: '',
		user_email: '',
    	user_password: '',
		user_phonenumber: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		disabled: false,
		 FuelCategories: [],
		regions: [],
		areas: [],
		service_id:'',
		fuel_category_id:'',
		// suppliers: [],
		isImagesChanged: false,
        hiddenDropdown:true 
	});


	useEffect(() => {
		axios.post('http://localhost:3000/getservices',{
			user_id:'',
			user_type_id:'',},USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized" ) {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
				console.log("response", response.data.message);
					setState({
						...state,
						regions: response.data.message, 
						disabled: false,

					
					})
				}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}, []); 


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const handleTwoEvents = (e) => {
		handleFields(e);
        console.log("e",e.target.value - 1);
        let i = e.target.value - 1
        axios.post('http://localhost:3000/getservices',{
		user_id:'',
		user_type_id:'',},USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized" ) {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            if( response.data.message[i].FuelCategories.length == 0){
                setState({
                    ...state,
                    FuelCategories: response.data.message[i].FuelCategories, 
                    hiddenDropdown:true ,

                })
            }
            else {
            console.log("response", response.data.message[i].FuelCategories);
                setState({
                    ...state,
                    FuelCategories: response.data.message[i].FuelCategories, 
                    hiddenDropdown: false,

                
                })
            }
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}

	

	const onSubmit = e => {

		let data = {
			service_provider_location_id:parseInt(localStorage.getItem("service_provider_location_id")),
			service_id:parseInt(state.service_id),
			fuel_category_id:null,
		}
        console.log("data",data);

		axios({
		url: 'http://localhost:3000/addservice', method: 'post',  data: data,
		headers: {  'x-access-token': `${cookies.get('token')}` }
		},USER_TOKEN).then(res =>{
			toast.success("Add Service Successfully", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload()}, 3000)
		}).catch(error => {
			if(error.response.status === 409){
				toast.error(error.response.data.message, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});	
			}else 
			if(error.response.data.message){
				toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});	
			} else if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}

	const { match } = props;
	console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>Add Service</title>
				<meta name="description" content="Add Service" />
  			</Helmet>
			<PageTitleBar title="Add Service" match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12">
			<form onSubmit={handleSubmit(onSubmit)}>
			
				<div className="form-row">
					<div className="form-group  dropdown col-md-6">
						<label>Choose service:</label>
						<select name="service_id" className="form-control input-text" 
								onChange={handleFields} ref={register({ required: true })}>
								<option key="0" value="">Please Choose the service</option>
						{state.regions.map(region => {
							return (
								<option key={region.service_id} value={region.service_id}>
									The service is "{region.service_name}" and it's price is "{region.service_price}" L.E
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.service_id && errors.service_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
                    {(state.hiddenDropdown == true)?
                    <span></span>
                :
                 <div className="form-group  dropdown col-md-6">
						<label>Choose The Fuel Category:</label>
						<select name="fuel_category_id" className="form-control input-text" 
								onChange={handleTwoEvents} ref={register({ required: true })}>
								<option key="0" value="">Please Choose the fuel category</option>
						{state.FuelCategories.map(region => {
							return (
								<option key={region.fuel_category_id} value={region.fuel_category_id}>
									The service is "{region.fuel_category_name}" and it's price is "{region.fuel_category_price}" L.E
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.fuel_category_id && errors.fuel_category_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>


                }
				
				</div>

			
				
				{(state.disabled === false) ? 
						<button type="submit" className="btn btn-primary btn-margin">
							<IntlMessages id="form.add" />
						</button> : 
						<button type="submit" className="btn btn-primary btn-margin" disabled={true}>
							<IntlMessages id="form.add" />
						</button>
				}
			</form>


			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}
