import React, { Component } from 'react';

// import Link from 'next/link';
import axios from 'axios';
import Cookies from 'universal-cookie';
import USER_TOKEN from '../../../constants/Token';
import IntlMessages from 'Util/IntlMessages';

import PropTypes from "prop-types";
// import { useTranslation } from 'react-i18next';
// import { Trans } from "react-i18next";
import { Flag, Print } from '@material-ui/icons';
import logo from '../../../assets/img/icon10.jpg';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import { ToastContainer, toast } from 'react-toastify';
import Modal from 'react-awesome-modal';

const nf = new Intl.NumberFormat();

const cookies = new Cookies();

class PrintInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //defauilt value of the date time
            todayDate: '',
            categoryName: '',
            supplierFName: '',
            supplierMName: '',
            supplierLName: '',
            regionName: '',
            threeLastBundles: [],
            lastSellPrice: 0,
            lastPurchasePrice: 0,
            totalCardsCount: 0,
            sumCurrentBundle: 0,
            sumTotalBundles: 0,
            companyName: 0,
            isPrinted: false
        };


    }

    componentDidMount() {
        var date = new Date();
        date = date.getUTCFullYear() + 1 + '/' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '/' +
            ('00' + date.getUTCDate()).slice(-2);
        // console.log(date);

        this.setState({
            todayDate: date
        })
        // console.log("props",this.props);
        

    }

    renderTableData() {
        return this.props.tableArray.map((item, index) => {
        
            const { productName, quantity, sellPrice, discountAmount, discountPercentage, taxAmount, taxPercentage } = item //destructuring
            
            return (
                <tr key={productName}>
                    {/* <td style={styles.tableStyle} style={styles.fontStyle}></td> */}
                    <td style={styles.tableStyle} style={styles.fontStyle}>{productName}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{quantity}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{sellPrice}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{discountAmount}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{discountPercentage}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{taxAmount}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{taxPercentage}</td>
                </tr>
            )
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <div className="container" >
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body p-0">
                                    <div className="row p-5">
                                        <div className="col-md-3 text-right" style={{ textAlignLast: "right" }}>

                                        </div>
                                        <div className="col-md-6 text-center">
                                            <img width={150} height={150} src={logo} className="img-fluid" style={{
                                                marginLeft: "auto",
                                                marginRight: "auto"
                                            }} />
                                        </div>
                                        <div className="col-md-3">

                                        </div>


                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <h4 className="text-center" style={styles.font}>فاتورة رفع كوتة الشركات</h4>
                                            
                                            <p className="text-center" style={styles.font}>الفاتورة #
                                            {/* {this.props.bundleNumber} */}
                                            </p>
                                        </div>
                                    </div>
                                    


                                    <hr className="my-5" />



                                    <div className="row pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-6 text-left" style={{ textAlignLast: "right", direction: "rtl !important" }}>
                                            <p className="font-weight-bold mb-4" style={styles.font}>بيانات الفاتورة</p>
                                            <p className="mb-1 ml-2" style={styles.spaceSize}>تاريخ الفاتورة:  {this.props.invoiceDate}</p>
                                            <div className="row">
                                                <div className="col-6">
                                                    <p className="mb-1 ml-2" style={styles.font}>المسلسل:  {this.props.invoiceSerial}</p>
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>اسم المورد:  {this.props.selectedClientName}</p>
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>رقم حساب المورد:  {this.props.selectedClientAccountNumber}</p> 
                                                    <p className="mb-1 ml-2" style={styles.font}>دائن/مدين:  {this.props.selectedClientAuditType}</p>
                                                </div>
                                                <div className="col-6">
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>العملة:  {this.props.selectedClientCurrency}</p>
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>طريقة الدفع:  {this.props.paymentMethod}</p>
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>رقم الايصال:  {this.props.recieptNo}</p>
                                                    <p className="mb-1 ml-2" style={styles.spaceSize}>مركز التكلفة:  {this.props.costCetnerName}</p>
                                                    
                                                </div>
                                            </div>
                                            <p className="mb-1 ml-2" style={styles.spaceSize}>ملاحظات:  {this.props.notes}</p>
                                        </div>
                                    </div>
                                    <div style={{ direction: "rtl" }}>
                                        <div className="row p-5" style={{ textAlignLast: "right" }}>
                                            <div className="col-md-12">
                                                <table className="table" style={styles.tableStyle}>
                                                    <thead style={styles.tableStyle}>
                                                        <tr style={styles.tableStyle}>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="form.productName" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر البيع بالدينار العراقي</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>قيمة الخصم بالدينار العراقى</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>نسبة الخصم</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>قيمة الضريبة بالدينار العراقى</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>نسبة الضريبة</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style={styles.tableStyle}>
                                                        {this.renderTableData()}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        {/* <div className="row p-5">
                                            <div className="col-md-12">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th className="border-0 text-uppercase small font-weight-bold"></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر البيع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر الشراء</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الاجمالى</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style={styles.fontStyle}>لقد قمت برفع كوته</td>
                                                            <td style={styles.fontStyle}>{this.props.rightPinsCount}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.lastSellPrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.lastPurchasePrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.rightPinsCount * this.state.lastPurchasePrice)}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style={styles.fontStyle}>الاجمالى بعد الرفع</td>
                                                            <td style={styles.fontStyle}>{this.state.totalCardsCount}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.lastSellPrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.lastPurchasePrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.totalCardsCount * this.state.lastPurchasePrice)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> */}
                                    </div>

                                    <div className="row justify-content-between pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-4">
                                            {/* <Barcode value={this.props.bundleNumber} width="1" height="70" textAlign="center"
                                                textPosition="bottom" style={{ width: "18%", padding: "17px", marginLeft: "50px" }} displayValue={true} /> */}

                                        </div>

                                        <div className="col-md-4">
                                            {/* <QRCode size={75} value={this.props.bundleNumber} /> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div> */}

                </div>
            </div>
        );

    }
}

const styles = {
    tableStyle: {

        borderCollapse: "collapse",
        padding: "10px",
    },
    font: {
        fontSize: "large"
    },
    fontStyle: {
        fontSize: "x-large"
    },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default PrintInvoice;
