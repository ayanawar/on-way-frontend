/**
 * add supplier
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import { LocalGasStation } from '@material-ui/icons';

const cookies = new Cookies();
const Token = cookies.get('UserToken');



export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		
		user_first_name: '',
		user_middle_name: '',
		user_last_name: '',
		level1:0,
        level2:0,
        level3:0,
		accountCurrencyIdupdate:'',
		accountTypeIdupdate: '',
		accountCurrencyIdupdateName:'',
		accountTypeIdupdateName: '',
        acc_name_ar: '',
		acc_name_en: '',
        acc_id:'',
        acc_desc: '',
		security_level: '',
		acc_type: '',
		acc_currency_id:'',
        accountStatusId:1,
        supplier_company_name:'',
         disabled: false,
         hiddenlvl2:true,
         hiddenlvl3:true,
         levcels2:[],
         levcels3:[],
         types: [],
         levels: [],
         currency: [],
         accounts: [],
         MainAccounts:[],
		user_region_id: '',
		user_area_id: '',
		user_password: '',
		user_phonenumber: '',
		accountnumber:'',
		accounttype:'',
		disabled: false,
		companies: [],
		regions: [],
		areas: [],
	});


	useEffect(() => {
		axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
		axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
			axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
				axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
				   axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
					   axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
				// console.log("state", state);
				if (response8.data == "Token Expired" || response8.data == "Token UnAuthorized" || response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized"|| response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response11.data == "Token Expired" || response11.data == "Token UnAuthorized" ) {

					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					
					setState({
						...state,
						regions: response8.data.regions,
						levels: response.data.levels,
						types: response3.data.message,
						currency:response4.data.message,
						accounts:response5.data.accounts,
					   MainAccounts:response11.data.mainAccounts,
						disabled: false,
						
					})
				}
			}).catch(error => {
				if (error.response8.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
		}).catch(error => {
			if (error.response11.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}).catch(error => {
		if (error.response3.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	});
}).catch(error => {
	if (error.response4.status === 429) {
		toast.error(error.response.data, {
		   position: "top-center",
		   autoClose: 4000,
		   hideProgressBar: false,
		   closeOnClick: true,
		   pauseOnHover: true,
		   draggable: true
		}); 
	}
});
		}).catch(error => {
			if (error.response5.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}, []);



	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
	const handleTwoEvents2 = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
       
         let parentid = target.value;
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
               
                 if(response.data.accounts.length == 0){
              
                     setState({
                         ...state,
                         level2: parentid,
                         level3:0,
                         levcels3: [],
                     })
 
                 }
                 
                 else if(response.data.accounts.length>0){

                    setState({
                        ...state,
                        level2: parseInt(parentid),
						accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                       
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: [],
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:true,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: response.data.accounts,
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
            
                 }
                 else{
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
          
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }
 
     const handleTwoEvents = ({ target }) => {
        
		for(let i=0; i<=state.MainAccounts.length;i++){
         
             
            if(parseInt(target.value) == parseInt(state.MainAccounts[i].acc_number)){
        
        setState({ ...state, [target.name]: target.value,
            accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
            accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
            accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
            accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
        });
  
       
         let parentid = target.value;
         
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
                 
                 if(response.data.accounts.length == 0){
                     
                
                     setState({
                         ...state,
                         level1: parentid,
                         levcels2:  [],
                         levcels3:  [],
                         level2:0,
                         level3:0,
                         hiddenlvl2:true,
						 accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
                         accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
                         accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
                         accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
                     })
                 }
                 
                 else if(response.data.accounts.length>0){
                    setState({
                        ...state,
                        level1: parseInt(parentid),
                
                       
                        accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level1: parentid,
                            levcels2:  [],
                            levcels3:  [],
                            level2:0,
                            level3:0,
							
							accountCurrencyIdupdate:response.data.accounts[0].currency_id,
							accountTypeIdupdate:response.data.accounts[0].audit_type_id,
							accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
							accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:false,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                         ...state,
                         level1: parentid,
                         levcels2: response.data.accounts,
                         levcels3:  [],
                         level3:0,
						 
						 accountCurrencyIdupdate:response.data.accounts[0].currency_id,
						 accountTypeIdupdate:response.data.accounts[0].audit_type_id,
						 accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
						 accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
                 }
                 else{
                   
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
           //  console.log("Catch");
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
	
		 break;
        }
  
        else {
         //    console.log("n0oooo");
        }
	}
     }
 
	const handleTwoEvents3 = (e) => {
		handleFields(e);
		let regionId = e.target.value;
	
		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						user_area_id: '',
						
					})

				}else{
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}

	

	const onSubmit = e => {

//		console.log(Token);

          axios({
			  method:'post',
			url:'https://accbackend.alaimtidad-itland.com/addsupliers',
			headers: { "x-access-token": `${Token}` },
		    data:{level1:state.level1,
            level2:state.level2,
            level3:state.level3,
            acc_name_ar:state.acc_name_ar,
            acc_name_en:state.acc_name_en,
            acc_desc:state.acc_desc,
            security_level:state.security_level,
            acc_type:state.accountTypeIdupdate,
            acc_currency_id:state.accountCurrencyIdupdate,
            user_first_name:state.user_first_name,
            user_middle_name:state.user_middle_name,
            user_last_name:state.user_last_name,
            supplier_company_name:state.supplier_company_name,
            user_region_id:state.user_region_id,
            user_area_id:state.user_area_id,
            user_phonenumber:state.user_phonenumber},
	        }).then(res => {

				   if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else{
				toast.success(<IntlMessages id="form.addSupplierSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)
			}
			}).catch(error => {
				// console.log(error.response.data.message);
				if (error.response.data.message) {
					toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				} else if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
	}

	const { match } = props;
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper">
					<ToastContainer />
					<Helmet>
						<title>إضافة موردين</title>
						<meta name="description" content="إضافة موردين" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.addSuppliers" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.addSuppliers" />}>
							<form onSubmit={handleSubmit(onSubmit)}>

						
							<div className="form-group col-md-6">
							<label className="middletitle"> <IntlMessages id="accountdetails" /></label>
							</div>
					   <div className="form-group col-md-6">
							<label className="middletitlealert"> <IntlMessages id="alertforaccount" /> </label>
							</div>
							<div className="form-row">
                       
				

					   <div className="form-group col-md-4">
					
					 
					<div >
					<select className="form-control" name="level1"
									   onChange={handleTwoEvents} >
									   <option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
						 
									   {state.MainAccounts.map(account => {
										  return (
											  <option key={account.acc_number} value={account.acc_number}>
												  {account.acc_name_ar}
											  </option>
										  )
									  })
									  }
					</select>
					</div>
					<span className="errors">
									  {errors.level1 && errors.level1.type === 'required' &&
										  <IntlMessages id="form.requiredOptionError" />}
								  </span>
					</div>
					   {(state.hiddenlvl2 == true)?
					   
					   <span></span>
					 :
					 
					 <div className="form-group col-md-4">
					
					 
					 <div >
					 <select className="form-control" name="level2"
										onChange={handleTwoEvents2} >
										<option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
						  
										{state.levcels2.map(account => {
										   return (
											   <option key={account.acc_number} value={account.acc_number}>
												   {account.acc_name_ar}
											   </option>
										   )
									   })
									   }
					 </select>
					 </div>
					 {/* <span className="errors">
									   {errors.level2 && errors.level2.type === 'required' &&
										   <IntlMessages id="form.requiredOptionError" />}
								   </span> */}
					 </div>
					 }
				   {(state.levcels3.length == 0)?
					   
					   <span></span>
					 :


				   <div className="form-group col-md-4">
					
					 
					<div >
					<select className="form-control" name="level3"
									   onChange={handleFields} >
									   <option key="0" value="">برجاء اختيار الحساب الرئيسي</option>
						 
									   {state.levcels3.map(account => {
										  return (
											  <option key={account.acc_number} value={account.acc_number}>
												  {account.acc_name_ar}
											  </option>
										  )
									  })
									  }
					</select>
					</div>
					{/* <span className="errors">
									  {errors.level3 && errors.level3.type === 'required' &&
										  <IntlMessages id="form.requiredOptionError" />}
								  </span> */}
					</div>}
								  </div>
							  <div className="form-row">
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountname" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="acc_name_ar"
										  onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
											  </div>
									  <span className="errors">
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountnameEng" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="acc_name_en"
										  onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
									   </div>
									  <span className="errors">
										  {errors.acc_name_en && errors.acc_name_en.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_name_en && errors.acc_name_en.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_name_en && errors.acc_name_en.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.cointype" /></label>
									  <div >
									  <label>{state.accountCurrencyIdupdateName}</label>
					   </div>
					   <span className="errors">
										 {errors.acc_currency_id && errors.acc_currency_id.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>
							  </div>
						  
   

							  <div className="form-row">
							  <div className="form-group col-md-6">
									  <label><IntlMessages id="FORM.DEBT" /></label>
									  <div >
									  <label>{state.accountTypeIdupdateName}</label>
									
					   </div>
					   <span className="errors">
										 {errors.acc_type && errors.acc_type.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>

								 



							  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.description" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}> 
										<textarea  className="form-control" name="acc_desc"   rows={3} cols={5}
										  onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
										 </div>
									  <span className="errors">
										  {errors.acc_desc && errors.acc_desc.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_desc && errors.acc_desc.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_desc && errors.acc_desc.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>

								  </div>
							 
								  <div className="form-row">
								  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.secretdegree" /></label>
									  <div >
									  <select className="form-control input-text" name="security_level"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار درجة السرية</option>
										  {state.levels.map(level => {
											 return (
												 <option key={level.security_level_id} value={level.security_level_id}>
													 {level.security_level_type_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.security_level && errors.security_level.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>

							  

								  </div>
								  <label className="middletitle">بيانات المورد</label>

								<div className="form-row">
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.firstName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
																								<input type="text" className="form-control" name="user_first_name"
											onChange={handleFields}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
									</div>
										<span className="errors">
											{errors.user_first_name && errors.user_first_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_first_name && errors.user_first_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_first_name && errors.user_first_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.middleName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="user_middle_name"
											onChange={handleFields}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
									</div>
										<span className="errors">
											{errors.user_middle_name && errors.user_middle_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.lastName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="user_last_name"
											onChange={handleFields}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
										</div>
										<span className="errors">
											{errors.user_last_name && errors.user_last_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col-md-6">
										<label><IntlMessages id="form.companyName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="supplier_company_name"
											onChange={handleFields} ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })}/>
										
										</div>
										<span className="errors">
											{errors.user_last_name && errors.user_last_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-6">
										<label><IntlMessages id="form.phoneNumber" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="tel" className="form-control" name="user_phonenumber"
											onChange={handleFields}
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
									</div>
										<span className="errors">
											{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
												<IntlMessages id="form.numbersOnlyErrorError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
										</span>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group  dropdown col-md-6">
										<label><IntlMessages id="form.region" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<select name="user_region_id" className="form-control input-text"
											onChange={handleTwoEvents3} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار المحافظة</option>
											{state.regions.map(region => {
												return (
													<option key={region.region_id} value={region.region_id}>
														{region.region_arabic_name}
													</option>
												)
											})
											}
										</select>
										</div>
										<span className="errors">
											{errors.user_region_id && errors.user_region_id.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
									<div className="form-group  dropdown col-md-6">
										<label><IntlMessages id="form.area" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<select name="user_area_id" className="form-control input-text"
											onChange={handleFields} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار المنطقة</option>
											{state.areas.map(area => {
												return (
													<option key={area.area_id} value={area.area_id}>
														{area.area_arabic_name}
													</option>
												)
											})
											}
										</select>
										</div>
										<span className="errors">
											{errors.user_area_id && errors.user_area_id.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
								</div>





								{(state.disabled === false) ? 
									<button type="submit" className="btn-margin" style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
										<IntlMessages id="form.add" />
									</button> : 
									<button type="submit" className="btn-margin" disabled={true} style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
										<IntlMessages id="form.add" />
									</button>
								}
							</form>


						</RctCollapsibleCard>
					</div>
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}