/**
 * View Cart Page
 */
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';


// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const cookies = new Cookies(); 

const options = {
	filter: true,
	filterType: 'dropdown',
	rowsPerPage: 10,
	rowsPerPageOptions: [5,10,25,50,100],
	responsive: 'vertical',
	enableNestedDataAccess: '.',
	selectableRows: "none",
	viewColumns: false,
	sort: false,
	fixedHeader: true,
	download: false,
	fixedSelectColumn: false,
	tableBodyHeight: "600px"
 };

 export default function Shop(props) {
	// state = {
	// 	suppliers: [],
	// };
   
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        suppliers: [],
		areabyregion:[],
		areas2: [],
		hiddenStatusForm: true,
		hiddenDebitLimitForm: true,
		user_first_name: '',
		user_middle_name: '',
		selected_user:'',
		user_last_name: '',
		level1:0,
        level2:0,
        level3:0,
        acc_name_ar: '',
		acc_name_en: '',
        acc_id:'',
        acc_desc: '',
		security_level: '',
		acc_type: '',
		acc_currency_id:'',
		currency_id:'',
		status_id:'',
        accountStatusId:1,
        supplier_company_name:'',
         disabled: false,
         hiddenlvl2:true,
         hiddenlvl3:true,
         levcels2:[],
         levcels3:[],
         types: [],
         levels: [],
         currency: [],
		 status:[],
         accounts: [],
         MainAccounts:[],
		user_region_id: '',
		user_area_id: '',
		user_password: '',
		user_phonenumber: '',
		accountnumber:'',
		accounttype:'',
		filenumber:'',
		codenumber:'',
		codetype:'',
		tax_registration_number:'',
		user_personal_image: '',
		imagePreviewUrl: '',
		disabled: false,
		companies: [],
		regions: [],
		
    });
    useEffect(() => {
		// all suppliers
		axios.get('https://accbackend.alaimtidad-itland.com/allsupliers',USER_TOKEN).then(response2 => {
			axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
		axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
			axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
				axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
				   axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
					   axios.get('https://accbackend.alaimtidad-itland.com/getallstatus', USER_TOKEN).then(response5 => {


			if(response2.data == "Token Expired") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else{
console.log(response2.data.message);
				setState({
					...state,
					suppliers: response2.data.message,
					disabled: false,
					hiddenStatusForm: true,
					regions: response8.data.regions,
					levels: response.data.levels,
					types: response3.data.message,
					currency:response4.data.message,
					status:response5.data.message,
				   MainAccounts:response11.data.mainAccounts,
				})
			}
		// })		
		}).catch(error => {
			// console.log("error", error);
			if (error.response2.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		})
	}).catch(error => {
		if (error.response8.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	});
}).catch(error => {
	if (error.response11.status === 429) {
		toast.error(error.response.data, {
		   position: "top-center",
		   autoClose: 4000,
		   hideProgressBar: false,
		   closeOnClick: true,
		   pauseOnHover: true,
		   draggable: true
		}); 
	}
});
}).catch(error => {
	if (error.response.status === 429) {
		toast.error(error.response.data, {
		   position: "top-center",
		   autoClose: 4000,
		   hideProgressBar: false,
		   closeOnClick: true,
		   pauseOnHover: true,
		   draggable: true
		}); 
	}
});
}).catch(error => {
if (error.response3.status === 429) {
	toast.error(error.response.data, {
	   position: "top-center",
	   autoClose: 4000,
	   hideProgressBar: false,
	   closeOnClick: true,
	   pauseOnHover: true,
	   draggable: true
	}); 
}
});
}).catch(error => {
if (error.response4.status === 429) {
toast.error(error.response.data, {
   position: "top-center",
   autoClose: 4000,
   hideProgressBar: false,
   closeOnClick: true,
   pauseOnHover: true,
   draggable: true
}); 
}
});
}).catch(error => {
	if (error.response5.status === 429) {
		toast.error(error.response.data, {
		   position: "top-center",
		   autoClose: 4000,
		   hideProgressBar: false,
		   closeOnClick: true,
		   pauseOnHover: true,
		   draggable: true
		}); 
	}
});
    }, []);

	const onClearStatusClicked = () => {
		setState({...state,
			user_type_id: '',
			hiddenStatusForm: true,
			hiddenDebitLimitForm: true,
			debit_limit: '',
			debitLimitTable: '',
			user_phonenumber: '',
			representativeId: '',
			dealerId: '',
			representativeId: '',
			user_id: '',
			dealerFirstName: '',
			dealerMiddleName: '', 
			dealerLastName: '',
			repFirstName: '',
			repMiddleName: '',
			repLastName: '',
			status: '',
			// companies: [],
			// regions: [],
			// areas: [],
		})
	};

	
	
 
	const onSubmit2 = e => {
		let data={
			acc_name_ar: state.acc_name_ar,
            acc_name_en:  state.acc_name_en,
            acc_desc:  state.acc_desc,
            status_id:  state.status_id,
            security_level:  state.security_level,
            acc_type:  parseInt( state.acc_type,10),
            acc_currency_id:  parseInt(state.currency_id,10),
            user_first_name: state.user_first_name,
            user_middle_name:  state.user_middle_name,
            user_last_name:  state.user_last_name,
            supplier_company_name:  state.supplier_company_name,
            user_region_id: state.user_region_id,
            user_area_id:  state.user_area_id,
            user_phonenumber:  state.user_phonenumber,
            acc_id: state.selected_user[0],
		}
		console.log(data);
		
				 axios({
					 method: 'post', url: 'https://accbackend.alaimtidad-itland.com/updatesupplier', data: data,
					 headers: { "x-access-token": `${cookies.get('UserToken')}`}
				 }).then(res =>{
					if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else{
					 toast.success(<IntlMessages id="updatedsuccess" />, {
						 position: "top-center",
						 autoClose: 4000,
						 hideProgressBar: false,
						 closeOnClick: true,
						 pauseOnHover: true,
						 draggable: true
					 });
					 setState({ ...state, disabled: true })
					  setTimeout(function () { location.reload()}, 3000)
				}}).catch(error => {
				
					 if (error.response.status === 429) {
						 toast.error(error.response.data, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						 }); 
					 }
				 });
				}
	   const openUpdaeForm=(tableMeta)=>
	   {
		   //console.log("tableMeta",tableMeta);
		   setState({
			...state , 
			selected_user:tableMeta.rowData,
			hiddenStatusForm: false,
			hiddenDebitLimitForm: true
		   });
	   }

	const handleFields = ({ target }) => {
	//	console.log("target",target.value);
		setState({ ...state, [target.name]: target.value });
	};
	const handleTwoEvents3 = (e) => {
		handleFields(e);
		let regionId = e.target.value;
	// console.log("regionID", regionId);
		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
			//	console.log(response.data.areas);
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						user_region_id: regionId,
						areas2: response.data.areas,
						user_area_id: '',
					
					})

				}else{
				//	console.log("in else");
					setState({
						...state,
						user_region_id: regionId,
						areas2: response.data.areas,
						
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}
    const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

//    render() {
      const { match } = props;
	 // console.log("state",state);
      return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
         <div className="cart-wrapper">
			<ToastContainer />
			<Helmet>
				<title>قائمة الموردين</title>
				<meta name="description" content="قائمة الموردين" />
  			</Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.allSuppliers" />} match={match} />
            <div className="row">
               <RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={"تعديل بيانات الموردين"}>
					{/*  */}
					{(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
					: 
					<form onSubmit={handleSubmit(onSubmit2)}>	
				
					<div className="form-row">
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountname" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="acc_name_ar"
										defaultValue={state.acc_name_ar} 
										 onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, 
										})} 
										  />
											  
											  
											  </div>
									  <span className="errors">
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_name_ar && errors.acc_name_ar.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.accountnameEng" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}>
									  <input type="text" className="form-control" name="acc_name_en"
										  onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} 
										  defaultValue={state.acc_name_en}
										  />
									   </div>
									  <span className="errors">
										  {errors.acc_name_en && errors.acc_name_en.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_name_en && errors.acc_name_en.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_name_en && errors.acc_name_en.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>
								  <div className="form-group col-md-4">
									  <label><IntlMessages id="form.cointype" /></label>
									  <div >
									  <label className="middletitle"> {state.currency_ar}</label>
					   </div>
					
								  </div>
							  </div> 
						  
   

							  <div className="form-row">
							  <div className="form-group col-md-6">
									  <label><IntlMessages id="FORM.DEBT" /></label>
									  <div >
									  <label className="middletitle" >{state.audit_type_ar}</label>
					   </div>
					 
								  </div>

								 



							  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.description" /></label>
									  <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
 borderWidth: "0px 2px 2px 0px",
 lineHeight: "0px",
 borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
}}> 
										<textarea  className="form-control" name="acc_desc"   rows={3} cols={5}
										  onChange={handleFields}
										  ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} 
										  defaultValue={state.acc_desc}
										  
										  />
										 </div>
									  <span className="errors">
										  {errors.acc_desc && errors.acc_desc.type === 'required' &&
											  <IntlMessages id="form.requiredError" />}
										  {errors.acc_desc && errors.acc_desc.type === 'minLength' &&
											  <IntlMessages id="form.minLengthError" />}
										  {errors.acc_desc && errors.acc_desc.type === 'pattern' &&
											  <IntlMessages id="form.lettersOnlyError" />}
									  </span>
								  </div>

								  </div>
							 
								  <div className="form-row">
								  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.secretdegree" /></label>
									  <div >
									  <select className="form-control input-text" name="security_level"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار درجة السرية</option>
										  {state.levels.map(level => {
											 return (
												 <option key={level.security_level_id} value={level.security_level_id}>
													 {level.security_level_type_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.security_level && errors.security_level.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>
								  <div className="form-group col-md-6">
									  <label><IntlMessages id="form.currentStatus" /></label>
									  <div >
									  <select className="form-control input-text" name="status_id"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار الحالة </option>
										  {state.status.map(status => {
											 return (
												 <option key={status.status_id} value={status.status_id}>
													 {status.status_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.status_id && errors.status_id.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>
							  

								  </div>
								  <label className="middletitle">بيانات المورد</label>

								<div className="form-row">
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.firstName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
								  <input type="text" className="form-control" name="user_first_name"
											onChange={handleFields}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} 
											defaultValue={state.user_first_name}
											
											/>
									</div>
										<span className="errors">
											{errors.user_first_name && errors.user_first_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_first_name && errors.user_first_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_first_name && errors.user_first_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.middleName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="user_middle_name"
											onChange={handleFields}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })}
											defaultValue={state.user_middle_name}
											
											/>
									</div>
										<span className="errors">
											{errors.user_middle_name && errors.user_middle_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-4">
										<label><IntlMessages id="form.lastName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="user_last_name"
											onChange={handleFields}
											defaultValue={state.user_last_name}
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
										</div>
										<span className="errors">
											{errors.user_last_name && errors.user_last_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col-md-6">
										<label><IntlMessages id="form.companyName" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="text" className="form-control" name="supplier_company_name"
											defaultValue={state.supplier_company_name}
											onChange={handleFields} 
											ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i,
											})}/>
										
										</div>
										<span className="errors">
											{errors.user_last_name && errors.user_last_name.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
												<IntlMessages id="form.minLengthError" />}
											{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
												<IntlMessages id="form.lettersOnlyError" />}
										</span>
									</div>
									<div className="form-group col-md-6">
										<label><IntlMessages id="form.phoneNumber" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<input type="tel" className="form-control" name="user_phonenumber"
											defaultValue={state.user_phonenumber}
											onChange={handleFields}
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
									</div>
										<span className="errors">
											{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
												<IntlMessages id="form.requiredError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
												<IntlMessages id="form.numbersOnlyErrorError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
											{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
												<IntlMessages id="form.minPhoneLengthError" />}
										</span>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group  dropdown col-md-6">
										<label><IntlMessages id="form.region" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<select name="user_region_id" className="form-control input-text"
											onChange={handleTwoEvents3} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار المحافظة</option>
											{state.regions.map(region => {
												return (
													<option key={region.region_id} value={region.region_id}>
														{region.region_arabic_name}
													</option>
												)
											})
											}
										</select>
										</div>
										<span className="errors">
											{errors.user_region_id && errors.user_region_id.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
									<div className="form-group  dropdown col-md-6">
										<label><IntlMessages id="form.area" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<select name="user_area_id" className="form-control input-text"
											onChange={handleFields} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار المنطقة</option>
											{state.areas2.map(a=>
											{
												return (
													<option key={a.area_id} value={a.area_id}>
														{a.area_arabic_name}
													</option>
												)
											})
									
											}
										</select>
										</div>
										<span className="errors">
											{errors.user_area_id && errors.user_area_id.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
								</div>


							

						{(state.disabled === false) ? 
							<button type="submit" className="btn btn-warning">
							<IntlMessages id="form.update" />
							</button> : 
							<button type="submit"  className="btn btn-warning" disabled={true} >
							<IntlMessages id="form.update" />
							</button>
							}

						<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
							<IntlMessages id="form.clear" />
						</button>
					</form>
                  	}
               </RctCollapsibleCard>
		    </div>
			<div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable 
								// title={<IntlMessages id="sidebar.cart" />}
									data={state.suppliers}
									columns={[
											{
												label: "id",
												name:  "acc_id",
												options: {
													display: "none",
													filter: false,
													print: false,
												}
											},
											{
												label: <IntlMessages id="form.firstName" />,
												name:  "supplier_first_name",
											},
											{
												label: <IntlMessages id="form.middleName" />,
												name:  "supplier_middle_name"
											},
											{
												label: <IntlMessages id="form.lastName" />,
												name:  "supplier_last_name",
											},
											{
												label: <IntlMessages id="form.phoneNumber" />,
												name:  "supp_phonenumber"
											},
										
											
											{
												label: <IntlMessages id="form.region" />,
												name:  "region_arabic_name",
											},
											{
												label: <IntlMessages id="form.area" />,
												name:  "area_arabic_name"
											},
											{
												label: <IntlMessages id="form.companyName" />,
												name:  "company_name"
											},
											{
												label: <IntlMessages id="form.accountnumber" />,
												name:  "acc_number" 
											},
											{
												label: <IntlMessages id="form.accounttype" />,
												name:  "audit_type_ar" 
											},
											{
												label: <IntlMessages id="form.accountname" />,
												name:  "acc_name_ar" 
											},	
											{
												label: <IntlMessages id="form.accountnameEng" />,
												name:  "acc_name_en" 
											},
											{
												label: <IntlMessages id="form.description" />,
												name:  "acc_desc" 
											},
											{
												label: <IntlMessages id="security_level_type_ar" />,
												name:  "security_level_type_ar" 
											},
											{
												label: <IntlMessages id="form.cointype" />,
												name:  "currency_ar" 
											},
											{
												label: <IntlMessages id="form.currentStatus" />,
												name:  "status_ar"
											},
											{
												label: <IntlMessages id="form.update" />,
												name: "",
												options: {
													filter: true,
													sort: false,
													empty: true,
													print: false,
													customBodyRender: (value, tableMeta, updateValue) => {
													return (
														<React.Fragment>
															<button type="button" className="btn btn-warning" 
															onClick={() => {
															   //console.log("HIIIII",tableMeta.rowData[4]);
																  setState({
																   ...state , 
																   selected_user:tableMeta.rowData,
																   hiddenStatusForm: false,
																   acc_name_ar: tableMeta.rowData[10],
																   acc_name_en: tableMeta.rowData[11],
																   acc_desc:  tableMeta.rowData[12],
																   user_first_name: tableMeta.rowData[1],
																   user_middle_name:  tableMeta.rowData[2],
																   user_last_name:  tableMeta.rowData[3],
																   supplier_company_name:tableMeta.rowData[7],	
																   currency_ar: tableMeta.rowData[14],
																   audit_type_ar: tableMeta.rowData[9],
																   currency_id: tableMeta.rowData[18],
																   acc_type: tableMeta.rowData[19],
																   user_phonenumber: tableMeta.rowData[4],
																   hiddenDebitLimitForm: true
																  });
														   }}>
															<IntlMessages id="form.update" />
															</button>
														</React.Fragment>
													);
												}
												}
						
											},
											{
                                                label: <IntlMessages id="form.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger" 
                                                            onClick={() => {
                                                                // console.log("HIIIII",tableMeta.rowData[4]);
                                                                   setState({
                                                                    ...state , 
                                                                    selected_user:tableMeta.rowData,
																	acc_name_ar: tableMeta.rowData[10],
																	acc_name_en: tableMeta.rowData[11],
																	acc_desc:  tableMeta.rowData[12],
																	user_first_name: tableMeta.rowData[1],
																	user_middle_name:  tableMeta.rowData[2],
																	user_last_name:  tableMeta.rowData[3],
																	supplier_company_name:tableMeta.rowData[7],	
																	user_phonenumber: tableMeta.rowData[4],
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true
                                                                   });
                                                            }}>
                                                            <IntlMessages id="form.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
											{
                                                label: "id",
                                                name:  "currency_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                          
                                            {
                                                label: "id",
                                                name:  "acc_type",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
										]}
									options={options}
								/>
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
         </div>
		 : (
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
//    }
}