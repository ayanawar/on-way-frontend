/**
 * Ecommerce Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async components
import {
	AsyncShoplistComponent,
	AsyncShopGridComponent,
	AsyncInvoiceComponent,
	AsyncShopComponent,
	AsyncCartComponent,
	AsyncCheckoutComponent,
	AsyncPurchaesinvoiceComponent,
} from 'Components/AsyncComponent/AsyncComponent';

const Ecommerce = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>إدارة الموردين</title>
			<meta name="description" content="إدارة الموردين" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/sold-cards`} />
			<Route path={`${match.url}/sold-cards`} component={AsyncShoplistComponent} />
			<Route path={`${match.url}/available-cards`} component={AsyncShopGridComponent} />
			<Route path={`${match.url}/received-bundles`} component={AsyncInvoiceComponent} />
			<Route path={`${match.url}/add-suppliers`} component={AsyncShopComponent} />
			<Route path={`${match.url}/all-suppliers`} component={AsyncCartComponent} />
			<Route path={`${match.url}/checkout`} component={AsyncCheckoutComponent} />
			<Route path={`${match.url}/purchases-invoices`} component={AsyncPurchaesinvoiceComponent} />
		</Switch>
	</div>
);

export default Ecommerce;
