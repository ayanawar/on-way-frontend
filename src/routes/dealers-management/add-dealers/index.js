/**
 * add dealer
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 
const Token = cookies.get('UserToken'); 
export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '2',
		user_first_name: '',
    	user_middle_name: '',
		user_last_name: '',
		representative_id: '',
		user_region_id: '',
   		user_area_id: '',
		user_email: '',
    	user_password: '',
		user_phonenumber: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		disabled: false,
		representatives: [],
		regions: [],
		areas: [],
		suppliers: [],
		isImagesChanged: false
	});


	useEffect(() => {
		axios.get('http://localhost:8000/allregions',USER_TOKEN).then(response => {
				axios.get('http://localhost:8000/allrepresentatives',USER_TOKEN).then(response3 => {
					if(response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else {
						response.data.regions.splice(0, 1);
						// response2.data.areas.splice(0, 1);
						// response3.data.representatives.splice(0, 1);
						// console.log(response.data.regions);
						setState({
							...state,
							regions: response.data.regions, 
							// areas: response2.data.areas,
							representatives: response3.data.representatives,
							disabled: false,
							imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}	
				});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}, []); 

	const maxSelectFile = (e) => {
		let files = e.target.files 
			if (files.length > 1) { 
			  	e.target.value = null 
			//   console.log("You can only upload one image");
				toast.error("You can only upload one image", {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				return false;
		  	}
		return true;
	  
	}

	const handleTwoEvents = (e) => {
		handleFields(e);
		let regionId = e.target.value;
		// console.log("regionID", regionId);
		axios.post('http://localhost:8000/aresbyregionid', { "regionId": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				// response3.data.companies.splice(0, 1);
				if(response.data.areas.length == 0){
					toast.error('لا يوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						user_area_id: '',
						imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
					})

				}else{
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}
	  
	const checkMimeType= (e) => {
		let files = e.target.files 
		let err = ''
		const types = ['image/png', 'image/jpeg', 'image/gif', 'image/jpg']
		for(var x = 0; x<files.length; x++) {
				if (types.every(type => files[x].type !== type)) {   
					err += files[x].type+' is not a supported format, The allowed extensions are png,jpeg,gif and jpg';
				}
			};
		
		if (err !== '') {  
			e.target.value = null 
			// console.log(err);
			toast.error(err, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return false; 
		}
		return true;
	}
	  
	const checkFileSize = (e) => {
		let files = e.target.files
		let size = 500000 
		let err = ""; 
		for(var x = 0; x<files.length; x++) {
			if (files[x].size > size) {
				err += files[x].type+' is too large, please pick a smaller file, Files size allowed up to 500 KB';
			}
	  };
	  if (err !== '') {
		e.target.value = null
		// console.log(err);
		toast.error(err, {
			position: "top-center",
			autoClose: 4000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});
		return false
	  }
	  
	  return true;
	  
	}

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const imageHandler = e => {
		let reader = new FileReader();
    	let file = e.target.files[0];
		if(maxSelectFile(e) && checkMimeType(e) && checkFileSize(e)){ 
			reader.onloadend = () => {
			setState({ ...state,
				user_personal_image: file,
				imagePreviewUrl: reader.result
			});
			}
			reader.readAsDataURL(file)
		} else {
			setState({ ...state, user_personal_image: '', imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
			});
		}
	};

	const onSubmit = e => {

		let data = new FormData();
		data.append('user_type_id', '2'),
		data.append('user_first_name', state.user_first_name),
		data.append('user_middle_name', state.user_middle_name),
		data.append('user_last_name', state.user_last_name),
		data.append('representative_id', state.representative_id),
		data.append('user_region_id', state.user_region_id),
		data.append('user_area_id', state.user_area_id),
		data.append('user_phonenumber', state.user_phonenumber),
		data.append('user_email', state.user_email),
		data.append('user_password', state.user_password),
		data.append('user_personal_image', state.user_personal_image),

		axios({
		method: 'post', url: 'http://localhost:8000/createaccount', data: data,
		headers: { 'Content-Type': 'multipart/form-data', 'x-access-token': `${Token}` }
		},USER_TOKEN).then(res =>{
			toast.success(<IntlMessages id="form.addDealerSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload()}, 3000)
		}).catch(error => {
		// console.log(error.message);
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
				}); 
			} else if(error.response.data.message){
				toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});	
			}
		});
	}

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>إضافة وكلاء</title>
				<meta name="description" content="إضافة وكلاء" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.login" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.login" />}>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="form-row">
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.firstName" /></label>
						<input type="text" className="form-control" name="user_first_name" 
								onChange={handleFields} 
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i,})} />
						<span className="errors">
							{errors.user_first_name && errors.user_first_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_first_name && errors.user_first_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_first_name && errors.user_first_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.middleName" /></label>
						<input type="text" className="form-control" name="user_middle_name"
								onChange={handleFields}
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
						<span className="errors">
							{errors.user_middle_name && errors.user_middle_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
					<div className="form-group col-md-4">
						<label><IntlMessages id="form.lastName" /></label>
						<input type="text" className="form-control" name="user_last_name"
								onChange={handleFields}
								ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
						<span className="errors">
							{errors.user_last_name && errors.user_last_name.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_last_name && errors.user_last_name.type === 'minLength' &&
								<IntlMessages id="form.minLengthError" /> }
							{errors.user_last_name && errors.user_last_name.type === 'pattern' &&
								<IntlMessages id="form.lettersOnlyError" /> }
						</span>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.representativeName" /></label>
						<select className="form-control" name="representative_id" 
								onChange={handleFields} ref={register({ required: true })}>  
								<option key="0" value="">برجاء اختيار اسم المندوب</option>    
						{state.representatives.map((representative)=>{
							return(
								<option key={representative.representative_id} value={representative.representative_id}>
									{representative.representative_first_name} {representative.representative_middle_name} {representative.representative_last_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.representative_id && errors.representative_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.phoneNumber" /></label>
						<input type="tel" className="form-control" name="user_phonenumber"
								onChange={handleFields} 
								ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
						<span className="errors">
							{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
								<IntlMessages id="form.numbersOnlyErrorError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
								<IntlMessages id="form.minPhoneLengthError" /> }
							{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
								<IntlMessages id="form.minPhoneLengthError" /> }
						</span>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group  dropdown col-md-6">
						<label><IntlMessages id="form.region" /></label>
						<select name="user_region_id" className="form-control input-text" 
								onChange={handleTwoEvents} ref={register({ required: true })}>
								<option key="0" value="">برجاء اختيار المحافظة</option>
						{state.regions.map(region => {
							return (
								<option key={region.region_id} value={region.region_id}>
									{region.region_arabic_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.user_region_id && errors.user_region_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
					<div className="form-group  dropdown col-md-6">
						<label><IntlMessages id="form.area" /></label>
						<select name="user_area_id" className="form-control input-text" 
								onChange={handleFields}  ref={register({ required: true })}>
								<option key="0" value="">برجاء اختيار المنطقة</option>
						{state.areas.map(area => {
							return (
								<option key={area.area_id} value={area.area_id}>
									{area.area_arabic_name}
								</option>
							)
						})
						}
						</select>
						<span className="errors">
							{errors.user_area_id && errors.user_area_id.type === 'required' &&
								<IntlMessages id="form.requiredOptionError" /> }
						</span>
					</div>
				</div>

				<div className="form-row">
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.email" /></label>
						<input type="email" className="form-control" name="user_email"
								onChange={handleFields} 
								ref={register({
									// required: true,
									pattern: /^[^@ ]+@[^@ ]+\.[^@ .]{2,}$/
								  })} />
						<span className="errors">
							{/* {errors.user_email && errors.user_email.type === 'required' &&
								<IntlMessages id="form.requiredError" /> } */}
							{errors.user_email && errors.user_email.type === 'pattern' &&
								<IntlMessages id="form.emailError" /> }
						</span>
					</div>
					<div className="form-group col-md-6">
						<label><IntlMessages id="form.password" /></label>
						<input type="password" className="form-control" name="user_password"
								onChange={handleFields} ref={register({ required: true
									, pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/})} />
						<span className="errors">
							{errors.user_password && errors.user_password.type === 'required' &&
								<IntlMessages id="form.requiredError" /> }
							{errors.user_password && errors.user_password.type === 'pattern' &&
								<IntlMessages id="form.passwordMinLengthError" /> }
						</span>
					</div>
				</div>
				<div className="md-form md-outline">
					<label><IntlMessages id="form.image" /></label>
					<br />
					<div className="img-holder">
						<img src={state.imagePreviewUrl} alt="" id="img" className="img" />
					</div>
					<br />
					<input type="file" name="user_personal_image" onChange={imageHandler} />
				</div>
				{(state.disabled === false) ? 
					<button type="submit" className="btn btn-primary btn-margin">
						<IntlMessages id="form.add" />
					</button> : 
					<button type="submit" className="btn btn-primary btn-margin" disabled={true}>
						<IntlMessages id="form.add" />
					</button>
				}
			</form>


			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}