/**
* Report Page
*/
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';


import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

export default function PosDiscount (props) {
   const history = useHistory();
   const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
      categories:[],
		pos_discount_points: [],
      user_phonenumber:'',
      created_by:'',
      pos_id_in_user:'',
      cards_category_id:'',
      dealer_points: 0,
      companies:[],
      tableresult:[],
      userresult:[],
      type:'',
      pos_commercial_name: '',
      pos_first_name:'',
      pos_middle_name:'',
      pos_last_name:'',
      dealer_name:'',
      dealer_phone_number:'',
      dealer_region_arabic_name:'',
      dealer_area_arabic_name: '',
      dealer_virtual_money_balance: '',
      region_arabic_name: '',
      area_arabic_name:'',
      disabled: false,
      hiddenUpdateForm: true,
      name: '',
      region: '',
      area: '',
      phoneNumber: '',
      balance: '',
      userIdInTable: '',
      userTypeIdInTable: '',
      type: '', 
      pos_vm_balance: '',
      giftPointsID: '',
      company_name: '',
      category: '',
      giftPointsValue: '',
   });
   
   const [state2,setState2] = useState({
      companyId:'',
   })

   useEffect(() => {
		axios.post('http://localhost:8000/allgiftpoints', { "userTypeId": localStorage.getItem("user_type_id") },USER_TOKEN).then(response1 => {
			axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response2 => {
            if(response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response1.data == "Token Expired" || response1.data == "Token UnAuthorized") {
               cookies.remove('UserToken', {path:'/'})
               window.location.href = "/signin";	
            }
            else {
               response2.data.companies.splice(0, 1);
               setState({
                  ...state,
                  disabled: false,
                  hiddenUpdateForm: true,
                  pos_discount_points: response1.data.message,
                  companies: response2.data.companies,
                  created_by: localStorage.getItem("userIdInUsers")
               })
            }
			}).catch(error => {
				if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            }
			});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});	
	}, []); 

   const handleFields3 = ({ target }) => {
      if ( target.name = "dealer_points" && target.value < 0){
         setState({ ...state, dealer_points: 0 });
      } else {
         setState({ ...state, dealer_points: target.value });
      }
   };

   const handleFields = ({ target }) => {
      setState({ ...state, [target.name]: target.value });
   };

   const handleField2 = ({ target }) => {
		setState2({ ...state2, [target.name]: target.value });
	};

	const onClearClicked = () => {
		setState({
         ...state,
         dealer_points: '',
         cards_category_id: '',
         categories: [] ,
         hiddenUpdateForm: true,
         giftPointsID: '',
         company_name: '',
         category: '',
         giftPointsValue: '',
         dealer_name: ''
      })
   }
   
   const onClearClicked2 = () => {
      setState({
         ...state, 
         user_phonenumber: '',
         pos_first_name:'',
         pos_middle_name:'',
         pos_last_name:'',
         region_arabic_name: '',
         area_arabic_name:'',
         pos_vm_balance: '',
         dealer_name:'',
         dealer_phone_number:'',
         dealer_region_arabic_name:'',
         dealer_area_arabic_name: '',
         dealer_virtual_money_balance: '',
         pos_commercial_name: '',
         pos_id_in_user: '',
         name: '',
         region: '',
         area: '',
         phoneNumber: '',
         balance: '',
         userIdInTable: '',
         userTypeIdInTable: '',
         user_id_in_table: '',
         type: '', 
         hiddenUpdateForm: true,
      })
	}

   const onClearClicked3 = () => {
      setState({
         ...state, 
         user_phonenumber: '',
         pos_first_name:'',
         pos_middle_name:'',
         pos_last_name:'',
         region_arabic_name: '',
         area_arabic_name:'',
         pos_vm_balance: '',
         dealer_name:'',
         dealer_phone_number:'',
         dealer_region_arabic_name:'',
         dealer_area_arabic_name: '',
         dealer_virtual_money_balance: '',
         pos_commercial_name: '',
         pos_id_in_user: '',
         name: '',
         region: '',
         area: '',
         phoneNumber: '',
         balance: '',
         userIdInTable: '',
         userTypeIdInTable: '',
         user_id_in_table: '',
         type: '', 
         hiddenUpdateForm: true,
         dealer_points: '',
         cards_category_id: '',
         categories: [] ,
         giftPointsID: '',
         company_name: '',
         category: '',
         giftPointsValue: '',
         dealer_name: ''
      })
	}
   
   const onSubmit = e => {
			axios({
				method: 'post', url: 'http://localhost:8000/creategiftpoints', data:{
               "dealerId":state.user_id_in_table,
               "userTypeId": localStorage.getItem("user_type_id"),
               "categoryId":state.categoryId,
               "companyId":state2.companyId,
               "giftPoints":state.dealer_points,
            },headers: { "x-access-token": `${cookies.get('UserToken')}`}
				}).then(res =>{
					toast.success(<IntlMessages id="widgets.AddDiscount" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({ ...state, disabled: true })
					setTimeout(function () { location.reload()}, 3000)
				}).catch(error => {
               if (error.response.status === 429) {
                  toast.error(error.response.data, {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  }); 
               }else if (error.response.status === 303) {
                  toast.error("هذه الشركة والفئة موجودين من قبل من فضلك قم بالتعديل", {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  }); 
               }
				});
		// }
   }

   const onSubmit3 = e => {
      axios({
         method: 'post', url: 'http://localhost:8000/updategiftpoints', data:{
            "giftPointsId":state.giftPointsID,
            "userTypeId": localStorage.getItem("user_type_id"),
            "giftPoints":state.dealer_points,
         },headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
            toast.success("تم التعديل بنجاح", {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });
            setState({ ...state, disabled: true })
            setTimeout(function () { location.reload()}, 3000)
         }).catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            }else if (error.response.status === 303) {
               toast.error("هذه الشركة والفئة موجودين من قبل من فضلك قم بالتعديل", {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            }
         });
   // }
   }

   const nf = new Intl.NumberFormat();
   const getUserDataByphone = (e) => {
      // console.log("entered getUserDataByphone");
      axios.get(`http://localhost:8000/getuserbyphone/${state.user_phonenumber}`,USER_TOKEN)
      .then(response => {
         if(response.data.userresult[0].user_type_id != 2) {
            toast.error(<IntlMessages id="toast.NotDealerNumber" />, {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });
            setState({ 
               ...state,
               name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '', 
               hiddenUpdateForm: true,
            });
         } else {
            setState({
               ...state,
               name: response.data.userresult[0].userName,
					region: response.data.tableresult[0].region_arabic_name,
					area: response.data.tableresult[0].area_arabic_name,
					phoneNumber: response.data.tableresult[0].dealer_phone_number,
					balance: nf.format(response.data.tableresult[0].dealer_virtual_money_balance),
					user_id_in_table:response.data.tableresult[0].dealer_id,
					userTypeIdInTable: response.data.userresult[0].user_type_id,
					userIdInTable: response.data.userresult[0].user_id,
					type: "وكيل", 
               hiddenUpdateForm: true, 
            });
         }
      })
      .catch(error => {
         if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
      });
   }

   
   const getAllCategories = (e)=>{
      axios.get(`http://localhost:8000/allcategories/${e.target.value}`,USER_TOKEN)
      .then(response => {
         setState({ ...state, categories: response.data.message,});
      })
      .catch(error => {
         if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
      });
   }
   const handleTwochangeEvent = e => {
      handleField2(e);
      getAllCategories(e);
   }

   const getMuiTheme = () => createMuiTheme({
		overrides: {
         MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
         },
         MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
   })
   
   	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
      sort: false,
      download: false,
      fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "400px"
	};

	const { match } = props;
   // console.log("state", state);

   
      return (
         <React.Fragment>
    	   { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
         <div className="report-wrapper">
            <ToastContainer />
            <Helmet>
               <title>نقاط الهدية للوكلاء</title>
               <meta name="description" content="نقاط الهدية للوكلاء" />
  			   </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.dealersGift" />} match={match} />
            <div className="row">
                  <RctCollapsibleCard
                     colClasses="col-sm-6 col-md-6 col-lg-6"
                     heading={"بيانات الوكيل"}>
                        <form>
                           {/* <h3 className="text-center"><IntlMessages id="sidebar.basicDescription" /></h3> */}
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="form.phoneNumber" /></label>
                                 <input type="text" className="form-control" name="user_phonenumber" value={state.user_phonenumber}
                                       onChange={handleFields} 
                                       ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                 <span className="errors">
                                    {errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>
                           <div className="form-group col-md-12">
                              <h1>بيانات الوكيل</h1>
                              <br />
                              <h2><IntlMessages id="form.name" />: {state.name}</h2>
                              {/* <h2><IntlMessages id="form.phoneNumber" />: {state.user_phonenumber}</h2> */}
                              <h2><IntlMessages id="form.region" />: {state.region}</h2>
                              <h2><IntlMessages id="form.area" />: {state.area}</h2>
                              <h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
					            </div>
                        {(state.hiddenUpdateForm == true) ? 
                           <React.Fragment>
                              <button type="button" onClick={getUserDataByphone} className="btn btn-primary">
                                 <IntlMessages id="widgets.search" />
                              </button>
                              <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked2}>
                                 <IntlMessages id="form.clear" />
                              </button>
                           </React.Fragment>
                           : 
                           <React.Fragment></React.Fragment>
                        }
                     </form>
                  </RctCollapsibleCard>

               {(state.hiddenUpdateForm == true) ? 
                  <RctCollapsibleCard
                     colClasses="col-sm-6 col-md-6 col-lg-6"
                     heading={<IntlMessages id="sidebar.dealersGift" />}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="widgets.companyName" /></label>
                                 <select name="companyId" className="form-control" 
                                 onChange={handleTwochangeEvent} ref={register({ required: true })}>
                                       <option key="0" value="">برجاء اختيار الشركه</option>
                                       {state.companies.map(company => {
                                          return (
                                             <option key={company.company_id} value={company.company_id}>
                                                {company.company_name_ar}
                                             </option>
                                          )
                                       })
                                 }
                                 </select>
                                 <span className="errors">
									         {errors.companyId && errors.companyId.type === 'required' &&
										      <IntlMessages id="form.requiredOptionError" /> }
								         </span>
						            </div>    
                           </div>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="widgets.category" /></label>
                                 <select name="categoryId" className="form-control" 
                                         onChange={handleFields} ref={register({ required: true })}>
                                       <option key="0" value="">برجاء اختيار الفئة</option>
                                       {/* { console.log(state,state2)} */}
                                       {state.categories.map((category) => {
                                          return (
                                             <option key={category.cards_category_id} value={category.cards_category_id}>
                                                {category.category_text}
                                             </option>
                                          )
                                       })
                                       }
                                 </select>
                                 <span className="errors">
									         {errors.categoryId && errors.categoryId.type === 'required' &&
										      <IntlMessages id="form.requiredOptionError" /> }
								         </span>	
						            </div>
                           </div>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label>نقاط الهدية</label>
                                 <input type="number" className="form-control" name="dealer_points" placeholder="د.ع"
                                       onChange={handleFields3} value={state.dealer_points} min="0"
                                       ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, min: 0 })}/>
                                 <span className="errors">
                                    {errors.dealer_points && errors.dealer_points.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.dealer_points && errors.dealer_points.type === 'min' &&
                                       'يجب أن تكون نقاط الهدية بالموجب' }
                                    {errors.dealer_points && errors.dealer_points.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>

                           {(state.disabled === false) ? 
                              <button type="submit" className="btn btn-primary">
                                 <IntlMessages id="form.create" />
                              </button> : 
                              <button type="submit" className="btn btn-primary" disabled={true}>
                              <IntlMessages id="form.create" />
                              </button>
					            }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
                           <IntlMessages id="form.clear" />
                        </button>
                     </form>
                  </RctCollapsibleCard>
                  :
                  <RctCollapsibleCard
                     colClasses="col-sm-6 col-md-6 col-lg-6"
                     heading={<IntlMessages id="sidebar.dealersGift" />}>
                        <label className="mr-4">ستقوم بتعديل نقاط الهدية للوكيل: <span className="reportsCardsNumberCenter">{state.dealer_name}</span></label>
                        <br />
                        <label className="mr-4">الشركة: <span className="reportsCardsNumberCenter">{state.company_name}</span></label>
                        <label className="mr-4">الفئة: <span className="reportsCardsNumberCenter">{state.category}</span></label>
                        <br />
                        <br />
                        <form onSubmit={handleSubmit(onSubmit3)}>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label>نقاط الهدية</label>
                                 <input type="number" className="form-control" name="dealer_points" 
                                        placeholder="د.ع" value={state.dealer_points} onChange={handleFields3} min="0"
                                        ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, min: 0 })}/>
                                 <span className="errors">
                                    {errors.dealer_points && errors.dealer_points.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.dealer_points && errors.dealer_points.type === 'min' &&
                                       'يجب أن تكون نقاط الهدية بالموجب' }
                                    {errors.dealer_points && errors.dealer_points.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>

                           {(state.disabled === false) ? 
                              <button type="submit" className="btn btn-primary">
                                 <IntlMessages id="form.update" />
                              </button> : 
                              <button type="submit" className="btn btn-primary" disabled={true}>
                              <IntlMessages id="form.update" />
                              </button>
					            }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked3}>
                           <IntlMessages id="form.clear" />
                        </button>
                     </form>
                  </RctCollapsibleCard>
               }
            </div>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////// */}
            <div className="row mb-5">
               <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                  <RctCard className="mb-4">
                     <RctCardContent noPadding>
                        <MuiThemeProvider theme={getMuiTheme()}>
                           <MUIDataTable 
                           // title={<IntlMessages id="sidebar.basic" />}
                           data={state.pos_discount_points}
                           columns={[
                                 {
                                    label: <IntlMessages id="table.pos_discount_id" />,
                                    name:  "gift_points_id",
                                 },
                                 {
                                    label: <IntlMessages id="form.dealerName" />,
                                    name:  "userName",
                                 },
                                 {
                                    label: <IntlMessages id="form.companyArabicName"/>,
                                    name:  "company_name_ar" 
                                 },
                                 {
                                    label: <IntlMessages id="widgets.category" />,
                                    name:  "category_text",
                                 },
                                 {
                                    label: "نقاط الهدية",
                                    name:  "dealer_points_fr" 
                                 },                          
                                 {
                                    label: <IntlMessages id="form.dataTime"/>,
                                    name:  "creation_date" 
                                 },
                                 {
                                    label: "company_id",
                                    name:  "company_id",
                                    options: {
                                       display: "none",
                                       filter: false,
                                       print: false,
                                    }
                                 },
                                 {
                                    label: "cards_category_id",
                                    name:  "cards_category_id",
                                    options: {
                                       display: "none",
                                       filter: false,
                                       print: false,
                                    } 
                                 }, 
                                 {
                                    label: "dealer_phone_number",
                                    name:  "dealer_phone_number",
                                    options: {
                                       display: "none",
                                       filter: false,
                                       print: false,
                                    } 
                                 },
                                 {
                                    label: "dealer_points",
                                    name:  "dealer_points",
                                    options: {
                                       display: "none",
                                       filter: false,
                                       print: false,
                                    }  
                                 },
                                 {
                                    label: <IntlMessages id="form.update" />,
                                    name: "",
                                    options: {
                                       filter: true,
                                       sort: false,
                                       empty: true,
                                       customBodyRender: (value, tableMeta, updateValue) => {
                                          return (
                                             <React.Fragment>
                                                <button type="button" className="btn btn-primary" 
                                                onClick={() => {
                                                   // console.log(tableMeta.rowData[0])
                                                   axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[8]}`,USER_TOKEN).then(response => { 
                                                      setState({
                                                         ...state,
                                                         name: response.data.userresult[0].userName,
                                                         region: response.data.tableresult[0].region_arabic_name,
                                                         area: response.data.tableresult[0].area_arabic_name,
                                                         phoneNumber: response.data.tableresult[0].dealer_phone_number,
                                                         balance: nf.format(response.data.tableresult[0].dealer_virtual_money_balance),
                                                         user_id_in_table:response.data.tableresult[0].dealer_id,
                                                         userTypeIdInTable: response.data.userresult[0].user_type_id,
                                                         userIdInTable: response.data.userresult[0].user_id,
                                                         type: "وكيل", 
                                                         hiddenUpdateForm: false,
                                                         giftPointsID: tableMeta.rowData[0],
                                                         dealer_name: tableMeta.rowData[1],
                                                         company_name: tableMeta.rowData[2],
                                                         category: tableMeta.rowData[3],
                                                         dealer_points: tableMeta.rowData[9],
                                                         user_phonenumber: tableMeta.rowData[8],
                                                      });   
                                                   })
                                                   .catch(error => {
                                                      if (error.response.status === 429) {
                                                         toast.error(error.response.data, {
                                                            position: "top-center",
                                                            autoClose: 4000,
                                                            hideProgressBar: false,
                                                            closeOnClick: true,
                                                            pauseOnHover: true,
                                                            draggable: true
                                                         }); 
                                                      }
                                                   });
                                                   
                                                }}>
                                                   <IntlMessages id="form.update" />
                                                </button>
                                             </React.Fragment>
                                          );
                                       }
                                    }
            
                                 },
                                ]}
                           options={options}  
                           />
                        </MuiThemeProvider>
                     </RctCardContent>
                  </RctCard>
               </RctCollapsibleCard>
			   </div>
         </div>
         : (
			
            history.push("/access-denied")
            )
         } 
         </React.Fragment>
      );
   
}
