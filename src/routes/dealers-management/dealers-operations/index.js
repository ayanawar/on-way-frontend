import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';

const cookies = new Cookies(); 


export default function Shop(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
 
    const fileExtension = '.xlsx';
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		hiddenExcelButton: true,
		hiddenExcelButton2: true,
		from_date: '',
		to_date: '',

        userIdInTable:'',
        userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: []
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		let todaysDate = new Date();
    	let year = todaysDate.getFullYear();
    	let month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
    	let day = ("0" + todaysDate.getDate()).slice(-2);
    	let currentDate = (year +"-"+ month +"-"+ day);
	
		setState({
			...state,
			from_date: currentDate,
			to_date: currentDate,
		})

	}, []); 

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const handleSearch = (e) => {
		if( state.phoneNumberField == '' ) {
			toast.error('عليك إدخال رقم هاتف الوكيل', {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		} else {
			axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			if (res.data.userresult[0].user_type_id === 2) {
				axios.post('http://localhost:8000/vmtransactions', 
				{ 
					'userPhoneNumber': state.phoneNumberField,
					'userTypeId': 2,
					'fromDate': state.from_date,
					'toDate': state.to_date,
					'isAll': false
				} , USER_TOKEN ).then(res2 => {
					if (res2.data.transactions.length == 0) {
						toast.error(<IntlMessages id="components.NoItemFound" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});

						setState({
							...state,
							
							pos_company_cards_transaction: [],
							pos_sim_card_transaction: [],
							pos_vm_transaction: [],
							hiddenExcelButton: true,
							hiddenExcelButton2: true,
						})
					} else {

						for (let i = 0; i < res2.data.transactions.length; i ++) {
							res2.data.transactions[i].virtual_mony_transaction_value = nf.format(res2.data.transactions[i].virtual_mony_transaction_value);
							res2.data.transactions[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.transactions[i].virtual_money_transaction_transfer_amount);
						}

						setState({
							...state,
							name: res2.data.userData.userName,
							region: res.data.tableresult[0].region_arabic_name,
							area: res.data.tableresult[0].area_arabic_name,
							phoneNumber: res.data.tableresult[0].dealer_phone_number,
							balance: nf.format(res.data.tableresult[0].dealer_virtual_money_balance),
							
							userTypeIdInTable: res.data.userresult[0].user_type_id,
							userIdInTable: res.data.userresult[0].user_id,
							type: "وكيل",
							pos_company_cards_transaction: res2.data.pos_company_cards_transaction,
							pos_sim_card_transaction: res2.data.pos_sim_card_transaction,
							pos_vm_transaction: res2.data.transactions,
							hiddenExcelButton: false,
							hiddenExcelButton2: true,
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
						}); 
					}
				});
			}
			else if (res.data.userresult[0].user_type_id != 1) {
				toast.error(<IntlMessages id="toast.NotDealerNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '',
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
	});	
		}
	}

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	
	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			if (res.data.userresult[0].user_type_id === 2) {
				axios.post('http://localhost:8000/vmtransactions', 
				{ 
					'userTypeId': 2,
				  	'userPhoneNumber': state.phoneNumberField
				},USER_TOKEN).then(res2 => {

					for (let i = 0; i < res2.data.transactions.length; i ++) {
						res2.data.transactions[i].virtual_mony_transaction_value = nf.format(res2.data.transactions[i].virtual_mony_transaction_value);
						res2.data.transactions[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.transactions[i].virtual_money_transaction_transfer_amount);
					}

					// console.log("uuu ",res2.data.userData.userName);
					setState({
						...state,
						name: res2.data.userData.userName,
						region: res.data.tableresult[0].region_arabic_name,
						area: res.data.tableresult[0].area_arabic_name,
						phoneNumber: res.data.tableresult[0].dealer_phone_number,
						balance: nf.format(res.data.tableresult[0].dealer_virtual_money_balance),
						
						userTypeIdInTable: res.data.userresult[0].user_type_id,
						userIdInTable: res.data.userresult[0].user_id,
						type: "وكيل",
						pos_company_cards_transaction: res2.data.pos_company_cards_transaction,
						pos_sim_card_transaction: res2.data.pos_sim_card_transaction,
						pos_vm_transaction: res2.data.transactions,
						hiddenExcelButton: true,
						hiddenExcelButton2: false
					})
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
						}); 
					}
				});
			}
			else if (res.data.userresult[0].user_type_id != 1) {
				toast.error(<IntlMessages id="toast.NotDealerNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '',
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
				}); 
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
	});	
	}

	const exportToCSV1 = () => {
        let newArr = []
        newArr = state.pos_vm_transaction.map(p => ({ 
            "Serial Number": p.virtual_mony_transaction_serial_number,
            "Balance Value": parseLocaleNumber(p.virtual_mony_transaction_value),
            "Transferred Balance Value": parseLocaleNumber(p.virtual_money_transaction_transfer_amount),
            "Transfer From": p.from_user_type_name,
            "Transfer To": p.to_user_type_name,
            "Sender Name": p.from_userName,
            "Receiver Name": p.to_userName,
			"Data And Time": p.creation_date
        }));

        const ws = XLSX.utils.json_to_sheet(newArr);

        const wb = { Sheets: { 'معاملات الوكيل': ws }, SheetNames: ['معاملات الوكيل'] };

        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

        const data = new Blob([excelBuffer], {type: fileType});

        FileSaver.saveAs(data, `معاملات الوكيل - ${state.name} - ${state.from_date} - ${state.to_date}` + fileExtension);
    }

	const exportToCSV2 = () => {
        let newArr = []
        newArr = state.pos_vm_transaction.map(p => ({ 
            "Serial Number": p.virtual_mony_transaction_serial_number,
            "Balance Value": parseLocaleNumber(p.virtual_mony_transaction_value),
            "Transferred Balance Value": parseLocaleNumber(p.virtual_money_transaction_transfer_amount),
            "Transfer From": p.from_user_type_name,
            "Transfer To": p.to_user_type_name,
            "Sender Name": p.from_userName,
            "Receiver Name": p.to_userName,
			"Data And Time": p.creation_date
        }));

        const ws = XLSX.utils.json_to_sheet(newArr);

        const wb = { Sheets: { 'معاملات الوكيل': ws }, SheetNames: ['معاملات الوكيل'] };

        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

        const data = new Blob([excelBuffer], {type: fileType});

        FileSaver.saveAs(data, `معاملات الوكيل - ${state.name}` + fileExtension);
    }


	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;

	const parseLocaleNumber = (stringNumber, locale) => {
        var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
        var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
    
        return parseFloat(stringNumber
            .replace(new RegExp('\\' + thousandSeparator, 'g'), '')
            .replace(new RegExp('\\' + decimalSeparator), '.')
        );
    }
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>معاملات الوكلاء</title>
				<meta name="description" content="معاملات الوكلاء" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.appBar" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.appBar" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<Form inline onSubmit={handleSubmit(showDetails)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary">إظهار التفاصيل وعرض كل المعاملات</Button>
								</Form>
							</div>
								<span className="errors">
									{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
										<IntlMessages id="form.requiredError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
										<IntlMessages id="form.numbersOnlyErrorError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
								</span>
							
							<br />
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<React.Fragment>
										<div className="container">
											<div className="row">
												<div className="col-md-6 reportsCardsNumberCenter">
													<h1>{state.type}</h1>
												</div>
												<div className="col-md-6 reportsCardsNumberCenter">
													<h1><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h1>
												</div>
											</div>
										</div>
										<br />
										<div className="container">
											<div className="row">
												<div className="col-md-5">
													<h2><IntlMessages id="form.name" />: {state.name}</h2>
													<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
												</div>
												<div className="col-md-3">
													<h2><IntlMessages id="form.region" />: {state.region}</h2>
													<h2><IntlMessages id="form.area" />: {state.area}</h2>	
												</div>
											</div>
										</div> 								
									</React.Fragment>
									<div className="container">
									{(state.hiddenExcelButton2 === false) ? 
										<React.Fragment>
											<button type="button" className="btn btn-primary mr-4 ml-4" style={{ marginTop: "24px"}} onClick={(e) => exportToCSV2()}>
												<IntlMessages id="form.exportExcel" />
											</button>
										</React.Fragment>
										: 
										<React.Fragment></React.Fragment>
									} 
									</div>
								</React.Fragment>
							}
						</div>
					</div>

					<div className="row">
						<div className="col-sm-4 col-lg-4 col-xl-4">
							<h3><IntlMessages id="form.dateFrom" /></h3>
							<input name="from_date" placeholder="date placeholder" 
									value={state.from_date} type="date" 
									onChange={handleFields} className="form-control"
									ref={register({ required: true })} />
							<span className="errors m-2">
								{errors.from_date && errors.from_date.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
						</div>
						<div className="col-sm-4 col-lg-4 col-xl-4">
							<span><IntlMessages id="form.dateTo" /></span>
							<input name="to_date" placeholder="date placeholder" 
									type="date" className="form-control" 
									onChange={handleFields} value={state.to_date}
									ref={register({ required: true })} />
							<span className="errors m-2">
								{errors.to_date && errors.to_date.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
						</div>
						<br />
						<div className="col-sm-4 col-lg-4 col-xl-4">
							<div className="container">
								<button color="primary" className="btn btn-primary" 
										onClick={handleSearch} style={{ marginTop: "24px"}}>
									ابحث
								</button>
								{(state.hiddenExcelButton === false) ? 
                                <React.Fragment>
                                    <button type="button" className="btn btn-primary mr-4 ml-4" style={{ marginTop: "24px"}} onClick={(e) => exportToCSV1()}>
                                        <IntlMessages id="form.exportExcel" />
                                    </button>
                                </React.Fragment>
                                : 
                                <React.Fragment></React.Fragment>
                                } 
							</div>
						</div>
					</div>

					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfCompaniesCards" /></h1>
							<h2>عدد الكارتات المباعة: <span style={{ marginRight: "30px" }}>{state.posSoldCardsCount}</span></h2>
							<br />
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								// title={state.posSoldCardsCount}
								data={state.pos_company_cards_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "bundle_number" 
									},
									{
										label: <IntlMessages id="form.cardType" />,
										name:  "company_name_ar",
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name:  "category_text"
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "company_cards_serial",
									},
									{
										label: "PIN",
										name:  "pin_number",
									},
									{
										label: "سعر البيع المحدد من الوكيل",
										name:  "cards_pos_sell_price"
									},
									{
										label: <IntlMessages id="form.cardsSellPriceAdmin" />,
										name:  "admin_sell_price",
									},
									{
										label: <IntlMessages id="widgets.netProfit" />,
										name:  "cards_pos_profit"
									},
									{
										label: <IntlMessages id="form.invoiceNumber" />,
										name:  "rm_cards_receipt"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.discountAmountPOS" />,
									// 	name:  "pos_discount_amount"
									// },
									{
										label: <IntlMessages id="form.regionDiscountAmount" />,
										name:  "region_discount_amount"
									},
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.pos_vm_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "virtual_mony_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_transaction_serial_number",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_transaction_value"
									},
									{
										label: <IntlMessages id="form.transferredBalanceValue" />,
										name:  "virtual_money_transaction_transfer_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	);
}
