/**
 * Drag and Drop Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async components
import {
	AsyncReactDragulaComponent,
	AsyncReactDndComponent,
	AsyncWithdrawalBalancesComponent,
	AsyncPrintBalancesComponent,
	AsyncWithdrawalAccountStatementComponent,
	AsyncPresentAndPrintAlaimtidadBalancesComponent
} from 'Components/AsyncComponent/AsyncComponent';

const DragAndDrop = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>أرصدة الامتداد</title>
			<meta name="description" content="أرصدة الامتداد" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/create-alaimtidad-balances`} />
			<Route path={`${match.url}/create-alaimtidad-balances`} component={AsyncReactDragulaComponent} />
			<Route path={`${match.url}/present-and-print-alaimtidad-balances`} component={AsyncPresentAndPrintAlaimtidadBalancesComponent} />
			<Route path={`${match.url}/balances-transfer`} component={AsyncReactDndComponent} />
			<Route path={`${match.url}/withdrawal-balances`} component={AsyncWithdrawalBalancesComponent} />
			<Route path={`${match.url}/withdrawal-account-statement`} component={AsyncWithdrawalAccountStatementComponent} />
			<Route path={`${match.url}/print-balances`} component={AsyncPrintBalancesComponent} />
		</Switch>
	</div>
);

export default DragAndDrop;
