/**
 * add supplier
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';

import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();


export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
		commercialName: '',
		userTypeId: 0,
		userId: 0,
		disabled: false,
		virtualCards: [],
		adminBalance: 0
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		axios.post('http://localhost:8000/getuserdata', 
			{ 'userTypeId': localStorage.getItem("user_type_id") ,
			'userId': localStorage.getItem("userIdInUsers")
		},USER_TOKEN).then(response2 => {
			// console.log(response2);
			axios.post('http://localhost:8000/virtualcards', 
				{ 'userTypeId': localStorage.getItem("user_type_id") ,
				'userId': localStorage.getItem("userIdInUsers")
			},USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
					for (let i = 0; i < response.data.virtualCards.length; i ++) {
						response.data.virtualCards[i].virtual_money_balance = nf.format(response.data.virtualCards[i].virtual_money_balance);
					}

					setState({
						...state,
						virtualCards: response.data.virtualCards, 
						userTypeId: localStorage.getItem("user_type_id") ,
						userId: localStorage.getItem("userIdInUsers"),
						adminBalance: nf.format(response2.data.message[0].administrator_virtual_money_balance),
						disabled: false
					})
				}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
					}
				});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});	
	}, []);
	

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
	
		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}
	
	let totalTransferValue = 0;
	let selectedSerials = [];
	let selectedBalances = [];
	const handlemultiSelect = (selectedRows,displayData) => {
		
		selectedSerials = [];
		selectedBalances = [];
		// console.log("selectedRowsArray",selectedRows);
		selectedRows.data.forEach(element => {
			selectedSerials.push(displayData[element.index].data[0]);
			selectedBalances.push(parseLocaleNumber(displayData[element.index].data[2]));	
		});
		// console.log("selectedBalances: ",selectedBalances);
		// console.log("selectedSerials: ",selectedSerials);
		
		totalTransferValue = 0;
		for (let i = 0; i< selectedBalances.length; i++) {
			totalTransferValue += selectedBalances[i];	
		}
		return(
			<React.Fragment>
					<div className="form-group col-md-12 mt-3 text-center">
						<h2 className="mb-3"><IntlMessages id="form.totalTransferValue" /></h2>
						<h2>{nf.format(totalTransferValue)}</h2>
					</div>
			</React.Fragment>
		)
	}

	

	const showDetails = e => {
		if (state.phoneNumberField === localStorage.getItem("phoneNumber")) {
			toast.error(<IntlMessages id="form.extraFeesError" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({
				...state,
				name: '',
				region: '',
				area: '',
				phoneNumber: '',
				balance: '',
				rank: '',
				commercialName: '',
				type: ''
			});
		} else {
			axios.post(`http://localhost:8000/userdata/${state.phoneNumberField}`,
				{ 'userTypeId': 5 },USER_TOKEN).then(res => {
				if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
				// console.log(res.data.user[0]);
					if (res.data.user[0].userTypeId === 5) {
						setState({  
							...state,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].administrator_phone_number,
							balance: nf.format(res.data.user[0].administrator_virtual_money_balance),
							active_status: res.data.user[0].active_status,
							rank: '',
							commercialName: '',
							type: "الإدارة"
						});
					}
					else if (res.data.user[0].userTypeId === 3) {
						setState({
							...state,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].pos_phone_number,
							balance: nf.format(res.data.user[0].pos_vm_balance),
							rank: res.data.user[0].pos_rank,
							commercialName: res.data.user[0].pos_commercial_name,
							type: "نقطة البيع"
						});
					}
					else if (res.data.user[0].userTypeId === 2) {
						setState({
							...state,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].dealer_phone_number,
							balance: nf.format(res.data.user[0].dealer_virtual_money_balance),
							rank: '',
							commercialName: '',
							type: "الوكيل"
						});
					}
					else if (res.data.user[0].userTypeId === 1) {
						setState({
							...state,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].representative_phone_number,
							balance: nf.format(res.data.user[0].representative_virtual_mony_balance),
							rank: '',
							commercialName: '',
							type: "المندوب"
						});
					} 
					else if(res.data.user[0].user_type_id === 4) {
						// console.log("مورد");
						toast.error('غير مسموح بالتحويل للمورد', {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							name: '',
							region: '',
							area: '',
							phoneNumber: '',
							balance: '',
							rank: '',
							commercialName: '',
							type: 'المورد'
						});
					}
				}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						}); 
					} else {
						toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							name: '',
							region: '',
							area: '',
							phoneNumber: '',
							balance: '',
							rank: '',
							commercialName: '',
							type: ''
						});
					}
			});
		}
	}

	const onClearClicked = () => {
		setState({...state,
			selectedSerials: [],
			selectedBalances: [],
			totalTransferValue: 0,
			name: '',
			region: '',
			area: '',
			phoneNumber: '',
			active_status: '',
			user_personal_image: '',
			imagePreviewUrl: '',
			phoneNumberField: '',
			balance: 0,
			type: '',
			checked: true,
			rank: '',
			commercialName: '',
		})
	}

	const onSubmit = e => {
		// console.log(selectedSerials);
		if (state.phoneNumberField === localStorage.getItem("phoneNumber")) {
			toast.error(<IntlMessages id="form.extraFeesError" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({
				...state,
				name: '',
				region: '',
				area: '',
				phoneNumber: '',
				balance: '',
				rank: '',
				commercialName: '',
				type: ''
			});
		} else if (selectedSerials.length == 0) {
			toast.error('عليك اختيار الأرصدة المراد تحويلها', {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({
				...state,
			});
			selectedSerials= [];
		} else if(state.type === 'المورد') {
			// console.log("مورد");
			toast.error('غير مسموح بالتحويل للمورد', {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({
				...state,
				name: '',
				region: '',
				area: '',
				phoneNumber: '',
				balance: '',
				rank: '',
				commercialName: '',
				
			});
		}
		else {
		axios.post('http://localhost:8000/transfervirtualmoney', 
			{ 'userTypeId': localStorage.getItem("user_type_id") ,
			  'userId': localStorage.getItem("userIdInUsers"),
			  'recieverPhoneNumber': state.phoneNumberField,
			  'userPhoneNumber': localStorage.getItem("phoneNumber"),
			  'virtualMoneyIds': selectedSerials,
			  'totalAmount': totalTransferValue
			},USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
					toast.success(<IntlMessages id="form.transferSuccess" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({ ...state, disabled: true })
					setTimeout(function () { location.reload()}, 3000)
				}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});
		}
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		customToolbarSelect: handlemultiSelect,
		selectableRowsHeader: false,
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>تحويل أرصدة</title>
				<meta name="description" content="تحويل أرصدة" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.balancesTransfer" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.balancesTransfer" />}>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="form-row">
					<div className="form-group col-md-4">
						<h1><IntlMessages id="form.will" /> <IntlMessages id="form.transferFrom" /></h1>
						<br />
						<h1>{localStorage.getItem("user_type_name")}</h1>
						<br /> 
						<h2><IntlMessages id="form.name" />: {localStorage.getItem("userFirstName")+" "+localStorage.getItem("usermiddleName")+" "+localStorage.getItem("userLastName")}</h2>
                        <h2><IntlMessages id="form.phoneNumber" />: {localStorage.getItem("phoneNumber")}</h2>
						<h2><IntlMessages id="form.region" />: {localStorage.getItem("region")}</h2>
						<h2><IntlMessages id="form.area" />: {localStorage.getItem("area")}</h2>
						<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.adminBalance}</h2>
					</div>
					<div className="form-group col-md-8">
						<h1><IntlMessages id="form.to" /></h1>
						<div className="row">
							<Form inline onSubmit={handleSubmit(showDetails)}>
								<FormGroup className="mr-10 mb-10">
									<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
									<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
										onChange={handleFields} 
										ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
								</FormGroup>
						
								<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
							</Form>
						</div>
							<span className="errors">
								{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
								{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
									<IntlMessages id="form.minPhoneLengthError" /> }
								{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
									<IntlMessages id="form.minPhoneLengthError" /> }
							</span>
						
						<br />
						{(state.name === '') ? <div></div>:
							<React.Fragment>
								<h1>{state.type}</h1>
								<br /> 
								<h2><IntlMessages id="form.name" />: {state.name}</h2>
								<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
								<h2><IntlMessages id="form.region" />: {state.region}</h2>
								<h2><IntlMessages id="form.area" />: {state.area}</h2>
								<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
								{(state.rank === '') ? <div></div>: 
									<React.Fragment>
										<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
										<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
									</React.Fragment>
								}
							</React.Fragment>
						}
					</div>
				</div>
				<div className="form-row mt-4">
					<div className="form-group col-md-6 text-center">
						<h1 className="mb-3"><IntlMessages id="form.balances" /></h1>
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
							data={state.virtualCards}
							columns={[
								{
								   label: <IntlMessages id="form.serialNumber" />,
									name:  "virtual_money_id",
									options: {
										display: "none",
										filter: false
								  	}
								},
								{
								   label: <IntlMessages id="form.serialNumber" />,
								   name:  "virtual_mony_serial_number",
								},
								{
								   label: <IntlMessages id="form.virtualMoneyBalance" />,
								   name:  "virtual_money_balance"
								},
								]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
					<div className="form-group col-md-6">
						<br />
						{(state.type === 'الإدارة' || state.type === 'نقطة البيع' || state.type === 'الوكيل' || state.type === 'المندوب') ?
						<React.Fragment>
							<div className="container">
								{(state.disabled === false) ?
								<button type="submit" className="btn btn-primary">
									<IntlMessages id="form.transfer" />
								</button> :
								<button type="submit" className="btn btn-primary" disabled={true}>
									<IntlMessages id="form.transfer" />
								</button>
								}
							</div>
						</React.Fragment>
						: 
						<React.Fragment></React.Fragment> }
					</div>
				</div>
				{/* <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
						<IntlMessages id="form.clear" />
				</button> */}
			</form>


			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}