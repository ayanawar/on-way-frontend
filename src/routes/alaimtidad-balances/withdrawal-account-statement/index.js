import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();

const options = {
   filter: true,
   filterType: 'dropdown',
   rowsPerPage: 10,
   rowsPerPageOptions: [5,10,25,50,100],
   responsive: 'vertical',
   enableNestedDataAccess: '.',
   selectableRows: "none",
   viewColumns: false,
   sort: false,
   fixedHeader: true,
   download: false,
   fixedSelectColumn: false,
   tableBodyHeight: "600px"
};

class Carts extends Component {
	state = {
		withdraw: [],
	};
   
   	componentDidMount() {
		axios.get('http://localhost:8000/admin/withdrawtransactions',USER_TOKEN).then(response => {
			// console.log(response.data);
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				// response.data.areas.splice(0, 1);
				this.setState({
					withdraw: response.data.message
				})
			}
		})
		.catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});
   }
   
   getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

   render() {
    //   const { match } = this.props;
      return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
         <div className="cart-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>كشف المسحوبات</title>
 				<meta name="description" content="كشف المسحوبات" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.withdrawalAccount" />} match={this.props.match} />
            {/* <PageTitleBar title={<IntlMessages id="sidebar.dataTable" />} match={match} /> */}
            <div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={this.getMuiTheme()}>
								<MUIDataTable 
								// title={<IntlMessages id="sidebar.dataTable" />}
								data={this.state.withdraw}
								columns={[
											{
												label: <IntlMessages id="form.bundleId" />,
												name:  "virtual_money_transaction_bundle_id",
											},
											{
												label: <IntlMessages id="form.serialNumber" />,
												name:  "virtual_mony_transaction_serial_number"
											},
											// {
											// 	label: "PIN",
											// 	name:  "virtual_mony_transaction_pin_number",
											// },
											{
												label: "القيمة المسحوبة",
												name:  "virtual_mony_transaction_value"
											},
											{
												label: "مسحوبة من",
												name:  "from_user_type",
											},
											{
												label: "اسم المسحوب منه",
												name:  "from_userName",
											},
											{
												label: <IntlMessages id="form.theReasonForWithdrawingBalances" />,
												name:  "notes"
											},
											{
												label: "تاريخ السحب",
												name:  "creation_date"
											},
										]}
									options={options}
								/>
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
         </div>
		: (
			this.props.history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
   }
}


export default Carts;
