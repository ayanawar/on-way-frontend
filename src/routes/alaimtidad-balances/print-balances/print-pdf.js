import React, { Component } from 'react';
import moment from 'moment';
import USER_TOKEN from '../../../constants/Token';
import axios from 'axios';
import Barcode from 'react-barcode';

class PrintPdf extends Component {
    constructor(props) {
        super(props);
        this.state = {
            website_phonenumber: '',
            website_whatsapp: '',
            website_name: ''
        }
    }
    componentDidMount() {
        // axios.get('http://localhost:8000/alaimtidad',USER_TOKEN).then(response => {
        //     if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
        //         cookies.remove('UserToken', {path:'/'})
        //         window.location.href = "/signin";	
        //     }
        //     else {
        //         // console.log(response.data.message);
        //         this.setState({
        //             website_phonenumber: response.data.message[0].website_phonenumber,
        //             website_whatsapp: response.data.message[0].website_whatsapp,
        //             website_name: response.data.message[0].website_name
        //         })
        //     }
        // }).catch(error => {
        //     // console.log(error);
        //     if (error.response.status === 429) {
        //         toast.error(error.response.data, {
        //             position: "top-center",
        //             autoClose: 4000,
        //             hideProgressBar: false,
        //             closeOnClick: true,
        //             pauseOnHover: true,
        //             draggable: true
        //         }); 
        //     }
        // });
    }
    render() {
        const { classes } = this.props;
        return (
            <div className="container mb-2">    
                <div className="row">
                    {this.props.cardsForPrintPdf.map(obj => {
                        return(
                        <React.Fragment>
                            {/* <div className="col-1">
                                        
                            </div> */}
                            <div className="col-5 text-center" style={styles.mainContainer}>
                                <span><p style={styles.pinStyle}>{obj.pin}</p></span>
                                <div style={styles.mainDiv}>
                                    <span className="ml-4 mr-4" style={styles.balanceStyle}> د.ع  </span>      
                                    <span style={styles.balanceStyle}>  {obj.balance} </span>  
                                </div>
                                <h2 style={styles.mainFont}>للمساعدة الرجاء الاتصال فوراً على</h2>
                                <h2 style={styles.mainFont}>{this.state.website_phonenumber} - {this.state.website_whatsapp}</h2>
                                <div style={styles.mainDiv}>
                                    <Barcode 
                                        value={obj.serial} 
                                        width="2" 
                                        height="50" 
                                        textAlign="center"
                                        textPosition="bottom" 
                                        style={{ width: "18%", marginLeft: "50px" }} 
                                        displayValue={true} 
                                    />
                                </div>
                                <div className="row">
                                    {/* <div className="col-1">
                                        
                                    </div> */}
                                    <div className="col-6">
                                        <h4 style={styles.font}>{this.state.website_name}</h4>
                                    </div>
                                    <div className="col-6">
                                        <h4 style={styles.font}> صالحة لغاية: {moment(obj.date).add(1, 'y').format('DD-MM-YYYY')}</h4>
                                    </div> 
                                    {/* <div className="col-1">
                                        
                                    </div> */}
                                </div>
                            </div>
                        </React.Fragment>
                        )
                    })}
                </div>
            </div>
        );
    }
}

const styles = {
    mainContainer: {
        border: "2px solid black",
        borderRadius: "10px",
        marginTop: "9px",
        marginLeft: "40px",
        marginRight: "40px",
        marginBottom: "9px"
    },
    pinStyle: {
        fontSize: "24px",
        border: "2px solid black",
        marginRight: "115px",
        marginLeft: "115px",
        marginTop: "15px"
    },
    balanceStyle: {
        fontSize: "30px",
        color: "red",
        fontWeight: "bold"
    },
    mainDiv: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",       
    },
    mainFont: {
        fontSize: "20px",
    },
    font: {
        fontSize: "17px",
    },
    fontStyle: {
        fontSize: "x-large"
    },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default PrintPdf;
