import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';
 
const cookies = new Cookies();
 
export default function BasicTable(props) {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
 
    const fileExtension = '.xlsx';
 
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        user_id: '',
        user_type_id: '',
        cards_category_id: '',
        selectedCategory: '',
        virtual_mony_cards_category_value: '',
        virtual_mony_current_owner_id: '',
        virtual_mony_current_owner_type: '',
        cardsnumber: '',
        from_date: '',
        to_date: '',
        disabled: false,
        hiddenExcelButton: true,
        cards: [],
        categories: []
    });
 
    const nf = new Intl.NumberFormat();
    useEffect(() => {
        let todaysDate = new Date();
        let year = todaysDate.getFullYear();
        let month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
        let day = ("0" + todaysDate.getDate()).slice(-2);
        let currentDate = (year +"-"+ month +"-"+ day);

        axios.get('http://localhost:8000/allcategories/1',USER_TOKEN).then(response => {
            axios.post('http://localhost:8000/getecards', { "isAll": true },USER_TOKEN).then(response2 => {
                if(response.data == "Token Expired" || response.data == "Token UnAuthorized" || response2.data == "Token Expired" || response2.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', {path:'/'})
                    window.location.href = "/signin";	
                }
                else {
                    for (let i = 0; i < response2.data.message.length; i ++) {
                        response2.data.message[i].virtual_mony_bundle_total_value = nf.format((response2.data.message[i].virtual_mony_bundle_total_value).toFixed(0));
                        response2.data.message[i].virtual_mony_bundle_count = nf.format((response2.data.message[i].virtual_mony_bundle_count).toFixed(0));
                        response2.data.message[i].category_value = nf.format((response2.data.message[i].category_value).toFixed(0));
                    }
                    setState({
                        ...state,
                        from_date: currentDate,
                        to_date: currentDate,
                        categories: response.data.message,
                        cards: response2.data.message,
                        user_id: localStorage.getItem("userIdInUsers"),
                        user_type_id: localStorage.getItem("user_type_id"),
                        virtual_mony_current_owner_id: localStorage.getItem("userIdInUsers"),
                        virtual_mony_current_owner_type: localStorage.getItem("user_type_id"),
                        disabled: false,
                        hiddenExcelButton: false,
                    })
                }
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
                }); 
            }
        });	
    }, []); 
 
 
    const handleSearch = (e) => {
        e.preventDefault();
        axios.post('http://localhost:8000/getecards', 
        { 	
            'fromDate': state.from_date,
            'toDate': state.to_date,
            'isAll': false
        },USER_TOKEN).then(res2 => {
            if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            else {
                if( res2.data.message.length == 0 ) {
                    toast.error(<IntlMessages id="components.NoItemFound" />, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                    setState({
                        ...state,
                        cards: res2.data.message,
                        hiddenExcelButton: true,
                    })
                }
                else {

                    for (let i = 0; i < res2.data.message.length; i ++) {
                        res2.data.message[i].virtual_mony_bundle_total_value = nf.format((res2.data.message[i].virtual_mony_bundle_total_value).toFixed(0));
                        res2.data.message[i].virtual_mony_bundle_count = nf.format((res2.data.message[i].virtual_mony_bundle_count).toFixed(0));
                        res2.data.message[i].category_value = nf.format((res2.data.message[i].category_value).toFixed(0));
                    }

                    setState({
                        ...state,
                        cards: res2.data.message,
                        hiddenExcelButton: false,
                    })
                    
                }
            }
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
                }); 
            }
        });		
    }

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value,  });	
    };
    const Token = cookies.get('UserToken');

    const onSubmit = e => {
        let selectedCategory = state.selectedCategory.split(",");
        axios({
            method: 'post', url: 'http://localhost:8000/createEcards', data: {
                "cards_category_id": selectedCategory[0],
                "virtual_mony_cards_category_value": selectedCategory[1],
                "cardsnumber": state.cardsnumber,
                "user_id": state.user_id
            },headers: { "x-access-token": `${Token}`  }
        }).then(res =>{
            // console.log(res);
            if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            else {
                toast.success(<IntlMessages id="form.balanceSuccess" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            }
        });
    }

    const parseLocaleNumber = (stringNumber, locale) => {
        var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
        var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
    
        return parseFloat(stringNumber
            .replace(new RegExp('\\' + thousandSeparator, 'g'), '')
            .replace(new RegExp('\\' + decimalSeparator), '.')
        );
    }
 
    const exportToCSV = () => {
        let newArr = []
        newArr = state.cards.map(p => ({ 
            "Bundle Id": p.virtual_mony_bundle_id,
            "Balance Value": parseLocaleNumber(p.virtual_mony_bundle_total_value),
            "Created By": p.admin_name,
            "Phone Number": p.user_phonenumber,
            "Data And Time": p.virtual_mony_bundle_date_time,
            "Cards Count": parseLocaleNumber(p.virtual_mony_bundle_count),
            "Category": parseLocaleNumber(p.category_value),
        }));

        const ws = XLSX.utils.json_to_sheet(newArr);

        const wb = { Sheets: { 'أرصدة الإدارة': ws }, SheetNames: ['أرصدة الإدارة'] };

        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

        const data = new Blob([excelBuffer], {type: fileType});

        FileSaver.saveAs(data, `أرصدة الإدارة` + fileExtension);
    }

    const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                maxHeight: 'unset',
                overflowX: 'unset',
                overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })

    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 5,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        sort: false,
        viewColumns: false,
        download: false,
        fixedHeader: true,
        fixedSelectColumn: false,
        tableBodyHeight: "345px"
    };

    const { match } = props;
     // console.log(state);
    return (
    <React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
    <div className="shop-wrapper mb-4">
        <ToastContainer />
            <Helmet>
                <title>عرض و طباعة أرصدة</title>
                <meta name="description" content="عرض و طباعة أرصدة" />
            </Helmet>
        <PageTitleBar title={<IntlMessages id="sidebar.presentAnd" />} match={match} />
        <br />
         
        <div className="row">
            <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                    <div className="row">
                        <div className="col-sm-4 col-lg-4 col-xl-4">
                            <h3><IntlMessages id="form.dateFrom" /></h3>
                            <input name="from_date" placeholder="date placeholder" 
                                   value={state.from_date} type="date" 
                                   onChange={handleFields} className="form-control"
                                   ref={register({ required: true })} />
                            <span className="errors m-2">
                                {errors.from_date && errors.from_date.type === 'required' &&
                                    <IntlMessages id="form.requiredOptionError" /> }
                            </span>
                        </div>
                        <div className="col-sm-4 col-lg-4 col-xl-4">
                            <span><IntlMessages id="form.dateTo" /></span>
                            <input name="to_date" placeholder="date placeholder" 
                                   type="date" className="form-control" 
                                   onChange={handleFields} value={state.to_date}
                                   ref={register({ required: true })} />
                            <span className="errors m-2">
                                {errors.to_date && errors.to_date.type === 'required' &&
                                    <IntlMessages id="form.requiredOptionError" /> }
                            </span>
                        </div>
                        <br />
                        <div className="col-sm-4 col-lg-4 col-xl-4">
                            <div className="container">
                                <button color="primary" className="btn btn-primary" 
                                        onClick={handleSearch} style={{ marginTop: "24px"}}>
                                    ابحث
                                </button>
                                {(state.hiddenExcelButton === false) ? 
                                <React.Fragment>
                                    <button type="button" className="btn btn-primary mr-4 ml-4" style={{ marginTop: "24px"}} onClick={(e) => exportToCSV()}>
                                        <IntlMessages id="form.exportExcel" />
                                    </button>
                                </React.Fragment>
                                : 
                                <React.Fragment></React.Fragment>
                                } 
                            </div>
                        </div>
                    </div>
 
                <RctCard className="mb-4">
                    <RctCardContent noPadding>
                        <MuiThemeProvider theme={getMuiTheme()}>
                            <MUIDataTable 
                                data={state.cards}
                                columns={[
                                    {
                                        label: <IntlMessages id="form.bundleId" />,
                                        name:  "virtual_mony_bundle_id",
                                    },
                                    {
                                        label: <IntlMessages id="form.balanceValue" />,
                                        name:  "virtual_mony_bundle_total_value" 
                                    },
                                    {
                                        label: <IntlMessages id="form.createdBy" />,
                                        name:  "admin_name",
                                    },
                                    {
                                        label: <IntlMessages id="form.phoneNumber" />,
                                        name:  "user_phonenumber",
                                    },
                                    {
                                        label: <IntlMessages id="form.dataTime" />,
                                        name:  "virtual_mony_bundle_date_time" 
                                    },
                                    {
                                        label: <IntlMessages id="form.cardsCount" />,
                                        name:  "virtual_mony_bundle_count" 
                                    },
                                    {
                                        label: <IntlMessages id="widgets.category" />,
                                        name: "category_value"
                                    },
                                ]}
                            options={options}  
                            />
                        </MuiThemeProvider>
                    </RctCardContent>
                </RctCard>
            </RctCollapsibleCard>
        </div>
    </div>
    : (
            
        history.push("/access-denied")
    )
    } 
    </React.Fragment>
    )
    }