import React, { useEffect, useState, useRef } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from 'mui-datatables';
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

import Modal from 'react-awesome-modal';

// printing
// import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import { useReactToPrint } from 'react-to-print';
// printed part
import Invoice from './reprint-bundle-invoice.js';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { Helmet } from 'react-helmet';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import { Multiselect } from 'multiselect-react-dropdown';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '',
		hidden: true,
		hiddenSellPriceForm: true,
		hiddenStatusForm: true,
		hiddenRegionForm: true,
		currentRegion: '',
		transfer_fees_id: '',
		pos_dealear_id: '',
		pos_rep_id: '',
		pos_type: '',
		pos_rank: '',
		bundleId: '',
		sellPrice: '',
		sellPriceTable: '',
		companyTable: '',
		categoryTable: '',
		user_phonenumber: '',
		bundleNum: '',
		companyId: '',
		categoryId: '',
		user_id: '',
		POSComercialName: '',
		POSFirstName: '',
		POSMiddleName: '',
		POSLastName: '',
		status: '',
		disabled: false,
		allBundles: [],
		representatives: [],
		companies: [],
		categories: [],
		name: '',
		region: '',
		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
		commercialName: '',
		userTypeId: 0,
		userId: 0,
		disabled: false,
		virtualCards: [],
		regions: [],
		regionIds: [],
		adminBalance: 0,
		threeLastBundles: [],
		ModelToPrint: false,
		categories: [],
		bundleNumber: 0,
		companyName: '',
		categoryName: '',
		supplierName: '',
		regionName: '',
		Currency: '',
		notes: '',
		bundleStatus: '',
		expirtDate: '',
		creationDate: '',
		sellPrice: 0,
		purchasePrice: 0,
		oldCardsCount: 0
	});

	const componentRef = useRef();
	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
	});

	useEffect(() => {

		axios.get('http://localhost:8000/allbundles', USER_TOKEN).then((res) => {
			if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {

				setState({
					allBundles: res.data.bundles,
					userTypeId: localStorage.getItem('user_type_id'),
					hidden: true,
					hiddenStatusForm: true,
					hiddenSellPriceForm: true,
					hiddenRegionForm: true,
					disabled: false,
				})
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}, []);


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const onClearClicked = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenSellPriceForm: true,
			hiddenRegionForm: true,
			transfer_fees_id: '',
			pos_dealear_id: '',
			pos_rep_id: '',
			pos_type: '',
			pos_rank: '',
			bundleId: '',
			companyId: '',
			categoryId: '',
			user_id: '',
		})
	}

	const onClearStatusClicked = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenSellPriceForm: true,
			hiddenRegionForm: true,
			transfer_fees_id: '',
			pos_dealear_id: '',
			pos_rep_id: '',
			pos_type: '',
			pos_rank: '',
			bundleId: '',
			companyId: '',
			categoryId: '',
			user_id: '',
		})
	}
	// const Token = cookies.get('UserToken');
	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updatebundle', data: state,
			headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updatebundleSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const Token = cookies.get('UserToken');
	const nf = new Intl.NumberFormat();

	const onSubmit2 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updateupdateBundleStatus',
			data: { "status_name": state.status_name, "bundle_id": state.bundleId }, headers: { "x-access-token": `${Token}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const updateForOneUser = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updateupdateBundleStatus',
			data: { "status_name": state.status_name, "bundle_id": state.bundleId, "user_id": state.userId }, headers: { "x-access-token": `${Token}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}


	const onSubmit3 = e => {
		if (state.sellPriceTable === state.sellPrice) {
			toast.error(<IntlMessages id="form.updateSellPriceError" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		} else {
			axios.post(`http://localhost:8000/updatesellpricebundle`, {
				bundleId: state.bundleId,
				sellPrice: state.sellPrice
			}, USER_TOKEN
			).then(res => {
				toast.success(<IntlMessages id="form.updateSellPriceSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload() }, 2000)
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
		}
	}


	const updateBundleRegion = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updateBundleRegion',
			data: { "region_id": state.regionIds, "bundle_id": state.bundleId },
			headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updatebundleSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const showDetails = e => {
		if (state.phoneNumberField === localStorage.getItem("phoneNumber")) {
			toast.error(<IntlMessages id="form.extraFeesError" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({
				...state,
				name: '',
				region: '',
				area: '',
				phoneNumber: '',
				balance: '',
				rank: '',
				commercialName: '',
				type: ''
			});
		} else {
			axios.post(`http://localhost:8000/userdata/${state.user_phonenumber}`,
				{ 'userTypeId': 5 }, USER_TOKEN).then(res => {
					if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
						cookies.remove('UserToken', { path: '/' })
						window.location.href = "/signin";
					}
					else {
						if (res.data.user[0].userTypeId === 3) {
							setState({
								...state,
								name: res.data.user[0].userName,
								region: res.data.user[0].region_arabic_name,
								area: res.data.user[0].area_arabic_name,
								phoneNumber: res.data.user[0].pos_phone_number,
								balance: nf.format(res.data.user[0].pos_vm_balance),
								rank: res.data.user[0].pos_rank,
								commercialName: res.data.user[0].pos_commercial_name,
								userId: res.data.user[0].userId,
								type: "نقطة البيع"
							});
						}
						else if (res.data.user[0].userTypeId === 2) {
							setState({
								...state,
								name: res.data.user[0].userName,
								region: res.data.user[0].region_arabic_name,
								area: res.data.user[0].area_arabic_name,
								phoneNumber: res.data.user[0].dealer_phone_number,
								userId: res.data.user[0].userId,
								balance: nf.format(res.data.user[0].dealer_virtual_money_balance),
								rank: '',
								commercialName: '',
								type: "الوكيل"
							});
						}
						else {
							toast.error('الرقم ليس وكيل أو نقطة بيع', {
								position: "top-center",
								autoClose: 4000,
								hideProgressBar: false,
								closeOnClick: true,
								pauseOnHover: true,
								draggable: true
							});
							setState({
								...state,
								name: '',
								region: '',
								area: '',
								phoneNumber: '',
								balance: '',
								rank: '',
								commercialName: '',
								type: ''
							});
						}
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
					} else {
						toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							name: '',
							region: '',
							area: '',
							phoneNumber: '',
							balance: '',
							rank: '',
							commercialName: '',
							type: ''
						});
					}
				});
		}
	}

	const getCategories = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

		axios.get(`http://localhost:8000/allcategories/${target.value}`, USER_TOKEN).then(response => {
			setState({
				...state,
				companyId: target.value,
				categories: response.data.message,
			})
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5, 10, 25, 50, 100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		fixedHeader: true,
		download: false,
		fixedSelectColumn: false,
		tableBodyHeight: "500px"
	};


	const onSelect = (selectedList, selectedItem) => {

		var regionIds2 = [];
		selectedList.map((item) => {

			regionIds2.push(item.region_id)
		})
		setState({ ...state, regionIds: regionIds2 });
	}

	const closeModel = () => {
		setState({ ...state, ModelToPrint: false });

	}


	const onRemove = (selectedList, removedItem) => {

		var regionIds = this.state.regionIds;

		for (var i = 0; i < regionIds.length; i++) {

			if (regionIds[i] === removedItem.region_id) {

				regionIds.splice(i, 1);
			}

		}

		setState({ ...state, regionIds: regionIds });
	}

	const { match } = props;

	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper mb-4">
					<ToastContainer />
					<Helmet>
						<title>عرض و تعديل كارتات</title>
						<meta name="description" content="عرض و تعديل كارتات" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.showAnd" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="form.updateCompaniesCards" />}>
							{(state.hidden == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit)}>
									{/* <div className="container"> */}
									<div className="row">
										<div className="col-md-5">
											<h3><IntlMessages id="form.updateThisBundle" />: {state.bundleNum}</h3>
										</div>
										<div className="col-md-4">
											<h3><IntlMessages id="form.companyName" />:  {state.companyTable} </h3>
										</div>
										<div className="col-md-3">
											<h3><IntlMessages id="widgets.category" />:  {state.categoryTable}</h3>
										</div>
									</div>
									{/* </div> 	 */}
									<br />
									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.companyName" /></label>
											<select className="form-control" name="companyId"
												onChange={getCategories} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار اسم الشركة</option>
												{state.companies.map((company) => {
													return (
														<option key={company.company_id} value={company.company_id}>
															{company.company_name_ar}
														</option>
													)
												})
												}
											</select>
											<span className="errors">
												{errors.companyId && errors.companyId.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
										{(state.companyId == '') ? <React.Fragment></React.Fragment> :
											<div className="form-group  dropdown col-md-4">
												<label><IntlMessages id="widgets.category" /></label>
												<select name="categoryId" className="form-control input-text"
													onChange={handleFields} ref={register({ required: true })}>
													<option key="0" value="">برجاء اختيار الفئة</option>
													{state.categories.map(category => {
														return (
															<option key={category.cards_category_id} value={category.cards_category_id}>
																{category.category_text}
															</option>
														)
													})
													}
												</select>
												<span className="errors">
													{errors.categoryId && errors.categoryId.type === 'required' &&
														<IntlMessages id="form.requiredOptionError" />}
												</span>
											</div>
										}
									</div>
									{/*  */}
									{(state.disabled === false) ?
										<button type="submit" className="btn btn-primary">
											<IntlMessages id="form.update" />
										</button>
										:
										<button type="submit" className="btn btn-primary" disabled={true} >
											<IntlMessages id="form.update" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}

							{/*  */}
							{(state.hiddenRegionForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(updateBundleRegion)}>
									{/* <div className="container"> */}
									<div className="row">
										<div className="col-lg-3">
											<h3><IntlMessages id="form.updateThisBundle" />: {state.bundleNum}</h3>
										</div>
										<div className="col-lg-3">
											<h3><IntlMessages id="form.companyName" />:  {state.companyTable} </h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="widgets.category" />:  {state.categoryTable}</h3>
										</div>
										<div className="col-lg-4">
											<h3>المحافظة الحالية:  {state.currentRegion}</h3>
										</div>
									</div>
									{/* </div> 	 */}
									<br />
									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.region" /></label>
											<Multiselect
												options={state.regions}
												selectedValues={state.selectedValue}
												onSelect={onSelect}
												onRemove={onRemove}
												displayValue="region_arabic_name"
												placeholder="ابحث هنا"
												closeIcon="close"
											/>
											{/* <select className="form-control" name="region_id" 
								onChange={handleFields} ref={register({ required: true })}>  
								<option key="0" value="">برجاء اختيار المحافظة</option>    
						{state.regions.map((region)=>{
							return(
								<option key={region.region_id} value={region.region_id}>
									{region.region_arabic_name}
								</option>
							)
						})
						}
						</select> */}
											<span className="errors">
												{errors.region_id && errors.region_id.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>
									{/*  */}
									{(state.disabled === false) ?
										<button type="submit" className="btn btn-primary">
											<IntlMessages id="form.update" />
										</button>
										:
										<button type="submit" className="btn btn-primary" disabled={true} >
											<IntlMessages id="form.update" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}

							{/*  */}
							{(state.hiddenSellPriceForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit3)}>
									{/* <div className="container"> */}
									<div className="row">
										<div className="col-lg-6">
											<h3><IntlMessages id="form.updateThisBundle" />: {state.bundleNum}</h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="form.companyName" />:  {state.companyTable} </h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="widgets.category" />:  {state.categoryTable}</h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="form.sellPrice" />:  {state.sellPriceTable}</h3>
										</div>
									</div>
									{/* </div> 	 */}
									<br />
									<div className="form-row">
										<div className="form-group col-md-4">
											<label><IntlMessages id="form.sellPrice" /></label>
											<input type="tel" className="form-control" name="sellPrice"
												onChange={handleFields} value={state.sellPrice}
												ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })} />

											<span className="errors">
												{errors.sellPrice && errors.sellPrice.type === 'required' &&
													<IntlMessages id="form.requiredError" />}
												{errors.sellPrice && errors.sellPrice.type === 'pattern' &&
													<IntlMessages id="form.numbersOnlyErrorError" />}
											</span>
										</div>
									</div>

									{(state.disabled === false) ?
										<button type="submit" className="btn btn-primary">
											<IntlMessages id="form.update" />
										</button>
										:
										<button type="submit" className="btn btn-primary" disabled={true}>
											<IntlMessages id="form.update" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}

							{/*  */}
							{(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit2)}>
									{/* <div className="form-row"> */}
									<div className="row">
										<div className="col-lg-6">
											<h3><IntlMessages id="form.updateThisBundle" />: {state.bundleNum}</h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="form.companyName" />:  {state.companyTable} </h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="widgets.category" />:  {state.categoryTable}</h3>
										</div>
										<div className="col-lg-2">
											<h3><IntlMessages id="widgets.status" />:  {state.status}</h3>
										</div>
									</div>
									{/* </div> */}
									<br />
									<div className="row">
										<div className="form-group col-md-4">
											<label><IntlMessages id="widgets.status" /></label>
											<select className="form-control" name="status_name"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار الحالة</option>
												<option key="1" value="Active">مفعل</option>
												{/* <option key="2" value="Inactive">غير مفعل</option>     */}
												<option key="2" value="Suspend">موقوف</option>
												<option key="3" value="Restricted">مخصصة</option>
											</select>
											<span className="errors">
												{errors.status_name && errors.status_name.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>
									{/* </div> */}


									{(state.disabled === false && state.status_name != "Restricted") ?
										<React.Fragment>
											<button type="submit" className="btn btn-warning">
												<IntlMessages id="form.updateStatus" />
											</button>
											<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
												<IntlMessages id="form.clear" />
											</button>
										</React.Fragment>
										:
										<React.Fragment></React.Fragment>
										// <button type="submit" className="btn btn-warning" disabled={true} >
										//    <IntlMessages id="form.updateStatus" />
										// </button>
									}
								</form>
							}

							{(state.status_name != "Restricted") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="text" className="form-control mr-2 ml-2" name="user_phonenumber"
											onChange={handleFields}
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
									</FormGroup>

									<span className="errors m-2">
										{errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
											<IntlMessages id="form.requiredError" />}
										{errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" />}
										{errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
											<IntlMessages id="form.minPhoneLengthError" />}
										{errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
											<IntlMessages id="form.minPhoneLengthError" />}
									</span>
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
								</Form>
							}

							{(state.name === '' || state.status_name != "Restricted") ? <div></div> :
								<React.Fragment>
									<h1>{state.type}</h1>
									<br />
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
									{(state.rank === '') ? <div></div> :
										<React.Fragment>
											<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
											<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
										</React.Fragment>
									}
								</React.Fragment>
							}

							{(state.disabled === false && state.status_name == "Restricted" && (state.type === 'الوكيل' || state.type === 'نقطة البيع')) ?
								<React.Fragment>
									<button type="submit" className="btn btn-warning" onClick={updateForOneUser}>
										<IntlMessages id="form.updateStatus" />
									</button>
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</React.Fragment>
								:
								<React.Fragment></React.Fragment>
								// <button type="submit" className="btn btn-warning" disabled={true} >
								//    <IntlMessages id="form.updateStatus" />
								// </button>
							}

						</RctCollapsibleCard>
					</div>
					<div className="row mb-5">
						<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
							<RctCard className="mb-4">
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											// title={<IntlMessages id="sidebar.basic" />}
											data={state.allBundles}
											columns={[
												{
													label: "bundleID",
													name: "bundle_id",
													options: {
														display: "none",
														filter: false
													}
												},
												{
													label: "companyId",
													name: "company_id",
													options: {
														display: "none",
														filter: false
													}
												},
												{
													label: "cardCategoryID",
													name: "card_category_id",
													options: {
														display: "none",
														filter: false
													}
												},
												{
													label: <IntlMessages id="form.bundleId" />,
													name: "bundle_number",
												},
												{
													label: <IntlMessages id="form.region" />,
													name: "region_arabic_name",
												},
												{
													label: <IntlMessages id="form.companyArabicName" />,
													name: "company_name_ar"
												},
												{
													label: <IntlMessages id="form.companyName" />,
													name: "company_name",
												},
												{
													label: <IntlMessages id="widgets.category" />,
													name: "category_text"
												},
												{
													label: <IntlMessages id="form.supplier" />,
													name: "userName"
												},
												{
													label: <IntlMessages id="form.phoneNumber" />,
													name: "supplier_phone_number"
												},
												{
													label: <IntlMessages id="form.purchasePrice" />,
													name: "purchase"
												},
												{
													label: <IntlMessages id="form.sellPrice" />,
													name: "bundle_sell_price",
												},
												{
													label: <IntlMessages id="form.sellPrice" />,
													name: "bundle_sell_price_un",
													options: {
														display: "none",
														filter: false
													}
												},
												{
													label: <IntlMessages id="form.currency" />,
													name: "currency"
												},
												{
													label: <IntlMessages id="form.cardsCount" />,
													name: "bundle_count",
												},
												{
													label: "المتوفر",
													name: "availablecards"
												},
												{
													label: "المطبوع",
													name: "Printedcards"
												},
												{
													label: "الموقوف",
													name: "Suspendedcards"
												},
												{
													label: <IntlMessages id="widgets.note" />,
													name: "bundle_note"
												},
												{
													label: <IntlMessages id="form.source" />,
													name: "bundle_source"
												},
												{
													label: <IntlMessages id="widgets.endDate" />,
													name: "bundle_cards_expiry_date"
												},
												{
													label: <IntlMessages id="form.dataTime" />,
													name: "creation_date",
												},
												{
													label: <IntlMessages id="widgets.status" />,
													name: "status_ar",
												},
												{
													label: "bundle_region_id",
													name: "bundle_region_id",
													options: {
														display: "none",
														filter: false
													}
												},
												{
													label: <IntlMessages id="form.updateSellPrice" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	{tableMeta.rowData[22] == "تم البيع" ?
																		null
																		:
																		<button type="button" className="btn btn-primary"
																			onClick={() => {
																				setState({
																					...state,
																					bundleId: tableMeta.rowData[0],
																					sellPriceTable: tableMeta.rowData[11],
																					sellPrice: tableMeta.rowData[12],
																					companyTable: tableMeta.rowData[5],
																					categoryTable: tableMeta.rowData[7],
																					bundleNum: tableMeta.rowData[3],

																					hidden: true,
																					hiddenStatusForm: true,
																					hiddenSellPriceForm: false,
																					hiddenRegionForm: true,
																				})
																			}}>
																			<IntlMessages id="form.updateSellPrice" />
																		</button>
																	}

																</React.Fragment>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.updateCompanyTypeAndCategory" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	{
																		tableMeta.rowData[22] == "تم البيع" ?
																			null
																			:
																			<button type="button" className="btn btn-primary"
																				onClick={() => {
																					axios.get('http://localhost:8000/allcompanies', USER_TOKEN).then((res) => {
																						res.data.companies.splice(0, 1);
																						setState({
																							...state,
																							bundleId: tableMeta.rowData[0],
																							companyTable: tableMeta.rowData[5],
																							categoryTable: tableMeta.rowData[7],
																							bundleNum: tableMeta.rowData[3],
																							companies: res.data.companies,
																							categories: [],
																							hidden: false,
																							hiddenStatusForm: true,
																							hiddenSellPriceForm: true,
																							hiddenRegionForm: true,
																						})
																					}).catch((error) => {
																						if (error.response.status === 429) {
																							toast.error(error.response.data, {
																								position: "top-center",
																								autoClose: 4000,
																								hideProgressBar: false,
																								closeOnClick: true,
																								pauseOnHover: true,
																								draggable: true
																							});
																						}
																					})
																				}}>
																				<IntlMessages id="form.updateCompanyTypeAndCategory" />
																			</button>
																	}

																</React.Fragment>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.updateStatus" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	{
																		tableMeta.rowData[22] == "تم البيع" ?
																			null
																			:
																			<button type="button" className="btn btn-warning"
																				onClick={() => {
																					setState({
																						...state,
																						bundleId: tableMeta.rowData[0],
																						companyTable: tableMeta.rowData[5],
																						categoryTable: tableMeta.rowData[7],
																						bundleNum: tableMeta.rowData[3],
																						status: tableMeta.rowData[20],

																						hidden: true,
																						hiddenStatusForm: false,
																						hiddenSellPriceForm: true,
																						hiddenRegionForm: true,
																					})

																				}}
																			>
																				<IntlMessages id="form.updateStatus" />
																			</button>
																	}

																</React.Fragment>
															);
														}
													}

												},
												{
													label: "تعديل المحافظة",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	{
																		tableMeta.rowData[22] == "تم البيع" ?
																			null
																			:
																			<button type="button" className="btn btn-success"
																				onClick={() => {
																					axios.get('http://localhost:8000/allregions', USER_TOKEN).then((res) => {
																						setState({
																							...state,
																							bundleId: tableMeta.rowData[0],
																							companyTable: tableMeta.rowData[5],
																							categoryTable: tableMeta.rowData[7],
																							bundleNum: tableMeta.rowData[3],
																							currentRegion: tableMeta.rowData[4],
																							regions: res.data.regions,
																							hidden: true,
																							hiddenStatusForm: true,
																							hiddenSellPriceForm: true,
																							hiddenRegionForm: false,
																						})
																					}).catch((error) => {
																						if (error.response.status === 429) {
																							toast.error(error.response.data, {
																								position: "top-center",
																								autoClose: 4000,
																								hideProgressBar: false,
																								closeOnClick: true,
																								pauseOnHover: true,
																								draggable: true
																							});
																						}
																					})
																				}}
																			>
																				تعديل المحافظة
																			</button>
																	}

																</React.Fragment>
															);
														}
													}

												},
												{
													label: "اعادة طباعة",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-danger"
																		onClick={() => {
																			let companyId = tableMeta.rowData[1];
																			let categoryId = tableMeta.rowData[2];
																			let creationDate = tableMeta.rowData[19];
																			let bundleId = tableMeta.rowData[0];
																			axios.post('http://localhost:8000/threebundlesbefore', { "bundleId": bundleId, "userTypeId": 5, "companyId": companyId, "categoryId": categoryId, "creationDate": creationDate }, USER_TOKEN).then((res) => {
																				if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
																					cookies.remove('UserToken', { path: '/' })
																					window.location.href = "/signin";
																				} else {
																					var oldCardsCount = 0;
																					for (var i = 0; i < res.data.message.length; i++) {
																						oldCardsCount = oldCardsCount + res.data.message[i].bundle_count_un;
																					}
																					setState({
																						...state,
																						threeLastBundles: res.data.message,
																						ModelToPrint: true,
																						bundleNumber: tableMeta.rowData[3],
																						companyName: tableMeta.rowData[5],
																						categoryName: tableMeta.rowData[7],
																						supplierName: tableMeta.rowData[8],
																						regionName: tableMeta.rowData[4],
																						Currency: tableMeta.rowData[13],
																						notes: tableMeta.rowData[16],
																						bundleStatus: tableMeta.rowData[20],
																						expirtDate: tableMeta.rowData[18],
																						creationDate: tableMeta.rowData[19],
																						sellPrice: tableMeta.rowData[12],
																						source: tableMeta.rowData[17],
																						bundleCount: tableMeta.rowData[14],
																						purchasePrice: tableMeta.rowData[27],
																						oldCardsCount: oldCardsCount
																					})
																				}
																			}).catch((error) => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																						position: "top-center",
																						autoClose: 4000,
																						hideProgressBar: false,
																						closeOnClick: true,
																						pauseOnHover: true,
																						draggable: true
																					});
																				}
																			})
																		}}>
																		اعادة طباعة الفاتورة
												</button>
																</React.Fragment>
															);
														}
													}
												},
												{
													label: "سعر الشراء",
													name: "purchase_un",
													options: {
														display: "none",
														filter: false
													}
												}
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>
					</div>

					{

						state.ModelToPrint == true ?
							<Modal visible={state.ModelToPrint} width="550" effect="fadeInUp" onClickAway={() => { closeModel() }}>
								<div className="modal-content">
									<div className="modal-header">
										<h5 className="modal-title">طباعة نسخة من الفاتورة</h5>
										<button type="button" className="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<p>هل ترغب فى طباعة فاتورة؟</p>
									</div>
									<div className="modal-footer">
										<button type="button" className="btn btn-secondary" onClick={() => { closeModel() }}>اغلاق</button>
										<div style={{ display: "none" }}>
											<Invoice ref={componentRef}
												{...state} />

											{/* <Invoice id="print-invoice" {...state} onSelectCLose={closeModelFromChild} ref={el => (componentRef = el)} /> */}
										</div>

										<button className="btn btn-danger" onClick={handlePrint}><IntlMessages id="form.print this out!" /></button>
									</div>
								</div>
							</Modal>
							:
							null
					}
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}
