import React, { Component } from 'react';

// import Link from 'next/link';
import axios from 'axios';
import Cookies from 'universal-cookie';
import USER_TOKEN from '../../constants/Token';
import IntlMessages from 'Util/IntlMessages';

import PropTypes from "prop-types";
// import { useTranslation } from 'react-i18next';
// import { Trans } from "react-i18next";
import { Flag, Print } from '@material-ui/icons';
import logo from '../../assets/img/icon10.jpg';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import { ToastContainer, toast } from 'react-toastify';
import Modal from 'react-awesome-modal';

const nf = new Intl.NumberFormat();

const cookies = new Cookies();

class Invoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //defauilt value of the date time
            todayDate: '',
            categoryName: '',
            supplierFName: '',
            supplierMName: '',
            supplierLName: '',
            regionName: '',
            threeLastBundles: [],
            lastSellPrice: 0,
            lastPurchasePrice: 0,
            totalCardsCount: 0,
            sumCurrentBundle: 0,
            sumTotalBundles: 0,
            companyName: 0,
            isPrinted: false
        };


    }

    componentDidMount() {
        var date = new Date();
        date = date.getUTCFullYear() + 1 + '/' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '/' +
            ('00' + date.getUTCDate()).slice(-2);
        // console.log(date);

        this.setState({
            todayDate: date
        })

        // console.log("props", this.props);
        // for (var i = 0; i < this.props.categories.length; i++) {
        //     if (this.props.categories[i].cards_category_id == this.props.categoryId) {
        //         this.setState({
        //             categoryName: this.props.categories[i].category_text
        //         })
        //     }
        // }

        // for (var i = 0; i < this.props.suppliers.length; i++) {
        //     if (this.props.suppliers[i].supplier_id == this.props.supplierId) {
        //         this.setState({
        //             supplierFName: this.props.suppliers[i].supplier_first_name,
        //             supplierMName: this.props.suppliers[i].supplier_middle_name,
        //             supplierLName: this.props.suppliers[i].supplier_last_name
        //         })
        //     }
        // }

        // var names = '';
        // var regionsNames = '';
        // for (var i = 0; i < this.props.regions.length; i++) {

        //     for (var j = 0; j < this.props.regionIds.length; j++) {
        //         if (this.props.regions[i].region_id == this.props.regionIds[j]) {
        //             var name = this.props.regions[i].region_arabic_name;
        //             if (regionsNames == '') {
        //                 names = regionsNames.concat(name);
        //                 regionsNames = names;
        //             } else {
        //                 names = regionsNames.concat(', ' + name);
        //                 regionsNames = names;
        //             }
        //         }
        //     }
        //     this.setState({
        //         regionName: regionsNames
        //     })

        // }

        // for (var i = 0; i < this.props.companies.length; i++) {
        //     if (this.props.companies[i].company_id == this.props.companyId) {
        //         this.setState({
        //             companyName: this.props.companies[i].company_name_ar,
        //         })
        //     }
        // }

        // if (this.props.sellPriceChange == true) {
        //     this.setState({
        //         lastSellPrice: this.props.cardsSellPrice
        //     })
        // } else {
        //     this.setState({
        //         lastSellPrice: this.props.sellPrice
        //     })
        // }

        // if (this.props.purchasePriceChange == true) {
        //     this.setState({
        //         lastPurchasePrice: this.props.cardsBuyingPrice
        //     })
        // } else {
        //     this.setState({
        //         lastPurchasePrice: this.props.purchasePrice
        //     })
        // }

        var totalCardsCount = parseInt(this.props.oldCardsCount) + parseInt(this.props.bundleCount)
        // console.log("data to get sum total cards count", this.props.oldCardsCount, this.props.bundleCount);
        // console.log("totalcount", totalCardsCount);
        this.setState({
            totalCardsCount: totalCardsCount
        })

        // axios.post('http://localhost:8000/lastbundles', { "userTypeId": 5, "companyId": this.props.companyId, "categoryId": this.props.categoryId }, USER_TOKEN)
        //     .then(response => {
        //         if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
        //             cookies.remove('UserToken', { path: '/' })
        //             window.location.href = "/signin";
        //         } else {
        //             console.log("3 bundles", response.data.message);
        //             this.setState({
        //                 threeLastBundles: response.data.message
        //             })
        //         }
        //     })
        //     .catch(error => {
        //         // console.log('Error fetching and parsing data', error);
        //     });

    }

    closeModelFromParent = () => {
        this.props.onSelectCLose(false)
    }

    askForPrint = () => {
        this.setState({
            isPrinted: true
        })
    }

    closeModel = () => {
        this.setState({
            isPrinted: false
        })
    }

    // handleSubmit = (e) => {
    //     e.preventDefault();

    //     // let isValidate = this.validate()

    //     // send excel file data to server
    //     // if (isValidate == true) {

    //     axios.post(`http://localhost:8000/uploadcards`, this.props, USER_TOKEN)
    //         .then(response => {
    //             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
    //                 cookies.remove('UserToken', { path: '/' })
    //                 window.location.href = "/signin";
    //             } else {
    //                 // this.closeModelFromParent();
    //                 this.askForPrint();
    //                 // console.log(response.data.message); 
    //                 toast.success('تم تحميل كرتات الشركات بنجاح', {
    //                     position: "top-center",
    //                     autoClose: 4000,
    //                     hideProgressBar: false,
    //                     closeOnClick: true,
    //                     pauseOnHover: true,
    //                     draggable: true
    //                 });
    //                 // setTimeout(function () { location.reload() }, 3000)

    //             }
    //         })
    //         .catch(error => {
    //             // this.closeModelFromParent();
    //             this.askForPrint();
    //             // console.log('Error fetching and parsing data ', error);
    //             toast.error("لقد قمت بتحميل هذة الكوته بالقعل", {
    //                 position: "top-center",
    //                 autoClose: 4000,
    //                 hideProgressBar: false,
    //                 closeOnClick: true,
    //                 pauseOnHover: true,
    //                 draggable: true
    //             });


    //         });
    // }

    renderTableData() {


        // console.log("three bundles", this.props.threeLastBundles);
        return this.props.threeLastBundles.map((bundle, index) => {
            const { bundle_number, sell_price, purchase, bundle_count, creation_date, purchase_un, bundle_count_un } = bundle //destructuring
            var totalAmount = purchase_un * bundle_count_un;
            // console.log("bundleCount", parseInt(bundle_count));
            // console.log("purchase", parseInt(purchase));
            // console.log("total", totalAmount);
            return (
                <tr key={bundle_number}>
                    <td style={styles.tableStyle} style={styles.fontStyle}></td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{bundle_number}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{bundle_count}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{nf.format(sell_price)}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{nf.format(purchase)}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{nf.format(totalAmount)}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{creation_date}</td>
                </tr>
            )
        })
    }

    render() {
        // console.log("cookies", cookies);
        const { classes } = this.props;
        // console.log("props", this.props);
        return (
            <div>
                <div className="container" >
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body p-0">
                                    <div className="row p-5">
                                        <div className="col-md-3 text-right" style={{ textAlignLast: "right" }}>
                                        </div>
                                        <div className="col-md-6 text-center">
                                            <img width={150} height={150} src={logo} className="img-fluid" style={{
                                                marginLeft: "auto",
                                                marginRight: "auto"
                                            }} />
                                        </div>
                                        <div className="col-md-3">

                                        </div>


                                    </div>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <h4 className="text-center" style={styles.font}>فاتورة رفع كوتة الشركات</h4>
                                            {/* <br/> */}
                                            <p className="text-center" style={styles.font}> Copy - الفاتورة #{this.props.bundleNumber}</p>
                                        </div>
                                    </div>
                                    {/* <div class="row">
                                        <div class="col-md-4" style={{ direction: "rtl !important" }}>
                                            
                                        </div>
                                    </div> */}




                                    <hr className="my-5" />



                                    <div className="row pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-6 text-left" style={{ textAlignLast: "right", direction: "rtl !important" }}>

                                            <p className="font-weight-bold mb-4" style={styles.font}>بيانات الفاتورة</p>
                                            <p className="mb-1" style={styles.spaceSize}>التاريخ: {this.state.todayDate}</p>
                                            <p className="mb-1" style={styles.font}>اسم الشركة: {this.props.companyName}</p>
                                            <p className="mb-1" style={styles.spaceSize}>الفئة: {this.props.categoryName}</p>
                                            <p className="mb-1" style={{ direction: "rtl" }} style={styles.font}>اسم المورد: {this.props.supplierName}</p>
                                            <p className="mb-1" style={styles.spaceSize}>المصدر: {this.props.source}</p>
                                            <p className="mb-1" style={styles.font}>المحافظة: {this.props.regionName}</p>
                                            <p className="mb-1" style={styles.spaceSize}>العملة: {this.props.Currency}</p>
                                            <p className="mb-1" style={styles.spaceSize}>الملاحظة: {this.props.notes}</p>
                                        </div>
                                    </div>
                                    <div style={{ direction: "rtl" }}>
                                        <div className="row p-5" style={{ textAlignLast: "right" }}>
                                            <div className="col-md-12">
                                                <table className="table" style={styles.tableStyle}>
                                                    <thead style={styles.tableStyle}>
                                                        <tr style={styles.tableStyle}>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>المتوفر قبل الرفع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>رقم الكوتة</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر البيع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر الشراء</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الاجمالى</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>تاريخ تحميل الكوته</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style={styles.tableStyle}>
                                                        {this.renderTableData()}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div className="row p-5">
                                            <div className="col-md-12">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th className="border-0 text-uppercase small font-weight-bold"></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر البيع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>سعر الشراء</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}>الاجمالى</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style={styles.fontStyle}>لقد تم رفع كوته</td>
                                                            <td style={styles.fontStyle}>{this.props.bundleCount}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.sellPrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.purchasePrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.bundleCount * this.props.purchasePrice)}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style={styles.fontStyle}>الاجمالى بعد الرفع</td>
                                                            <td style={styles.fontStyle}>{this.state.totalCardsCount}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.sellPrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.props.purchasePrice)}</td>
                                                            <td style={styles.fontStyle}>{nf.format(this.state.totalCardsCount * this.props.purchasePrice)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row justify-content-between pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-4">
                                            <Barcode value={this.props.bundleNumber} width="1" height="70" textAlign="center"
                                                textPosition="bottom" style={{ width: "18%", padding: "17px", marginLeft: "50px" }} displayValue={true} />

                                        </div>

                                        <div className="col-md-4">
                                            <QRCode size={75} value={this.props.bundleNumber} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div> */}

                </div>
            </div>
        );

    }
}

const styles = {
    tableStyle: {

        borderCollapse: "collapse",
        padding: "10px",
    },
    font: {
        fontSize: "large"
    },
    fontStyle: {
        fontSize: "x-large"
    },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default Invoice;
