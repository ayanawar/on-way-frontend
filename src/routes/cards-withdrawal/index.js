import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();


export default function ExportCSV(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		companyId: '',
		categoryId: '',
		companyName: '',
		category: '',
		companies: [],
		categories: [],
		cards: [],
		disabled: false,
		// hiddenExcelButton: true,
	});

	const nf = new Intl.NumberFormat();

	useEffect(() => {
		axios.get('http://localhost:8000/allcompanies', USER_TOKEN).then(response3 => {
			if (response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				response3.data.companies.splice(0, 1);
				setState({
					...state,
					companies: response3.data.companies,
					disabled: false,
					// hiddenExcelButton: true,
				})
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}, []);

	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})



	let totalBalancesToBePrinted = 0;
	let cardsIds = [];
	let bundleIds = [];
	let bundleNumbers = [];
	let pins = [];
	let serialNumbers = [];
	let selectedBalances = [];
	let selectedBalances2 = []
	let selectedRowsWithData = [];
	let selectedCount = 0;
	const handlemultiSelect = (selectedRows, displayData) => {
		// console.log("hii");
		cardsIds = [];
		bundleIds = [];
		bundleNumbers = [];
		pins = [];
		serialNumbers = [];
		selectedBalances = [];
		selectedBalances2 = [];
		selectedRowsWithData = [];
		selectedCount = 0;

		// console.log("selectedRowsArray",selectedRows);
		selectedRows.data.forEach(element => {
			cardsIds.push(displayData[element.index].data[0]);
			bundleIds.push(displayData[element.index].data[1]);
			bundleNumbers.push(displayData[element.index].data[2]);
			pins.push(displayData[element.index].data[3]);
			serialNumbers.push(displayData[element.index].data[4])
			selectedBalances2.push(displayData[element.index].data[5]);
			selectedBalances.push(displayData[element.index].data[6]);
			selectedRowsWithData.push({
				"Bundle Numbers": displayData[element.index].data[2],
				"PINs": displayData[element.index].data[3],
				"Serials": displayData[element.index].data[4],
				"Balances": displayData[element.index].data[6]
			})
		});
		// console.log("selectedBalances: ",selectedBalances);
		// console.log("cardsIds: ",cardsIds);

		totalBalancesToBePrinted = 0;
		for (let i = 0; i < selectedBalances.length; i++) {
			totalBalancesToBePrinted += selectedBalances[i];
			selectedCount = selectedBalances.length
		}
		return (
			<React.Fragment>
				<div className="form-group col-md-12 mt-3 text-center" id="cardsToBePrintedDiv">
					<h2 className="mb-3"><IntlMessages id="form.cardsToPrint" />: <span>{nf.format(totalBalancesToBePrinted)}</span></h2>
					<h2><span>عدد الكارتات المسحوبة: </span><span>{selectedCount}</span></h2>
					<div className="row">
						<div className="col-6">
							<h2><span>الشركة:</span> <span>{state.companyName}</span></h2>
						</div>
						<div className="col-6">
							<h2><span>الفئة: <span>{state.category}</span></span></h2>
						</div>
					</div>

					{(state.companyId == '' || state.categoryId == '' || state.cards.length == 0) ? <React.Fragment></React.Fragment> :
						<div className="form-group col-md-12">
							<br />
							<div className="container">
								{(state.disabled === false) ?
									<div className="row justify-content-center">
										<button className="btn btn-primary" onClick={(e) => exportToCSV()}>
											<IntlMessages id="form.exportExcel" />
										</button>
									</div>
									:
									<div className="row justify-content-center">
										<button className="btn btn-primary" disabled={true}>
											<IntlMessages id="form.exportExcel" />
										</button>
									</div>
								}
							</div>
						</div>
					}
				</div>
			</React.Fragment>
		)
	}

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const exportToCSV = () => {
		const ws = XLSX.utils.json_to_sheet(selectedRowsWithData);

		const wb = { Sheets: { 'الكارتات المسحوبة': ws }, SheetNames: ['الكارتات المسحوبة'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, 'الكارتات المسحوبة' + " ( " + state.companyName + " ) " + " ( " + new Date(Date.now()) + " ) " + fileExtension);
		axios.post('http://localhost:8000/updatecardstoprinted',
			{
				'userTypeId': localStorage.getItem("user_type_id"),
				'userId': localStorage.getItem("userIdInUsers"),
				'bundleIds': bundleIds,
				'cardsIds': cardsIds,
				//   'isPrinted': true
			}, USER_TOKEN).then(response => {
				toast.success(<IntlMessages id="form.printSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				// setState({ ...state, cards: [] })
				// totalBalancesToBePrinted = 0;
				// cardsIds = [];
				// bundleIds = [];
				// bundleNumbers = [];
				// selectedBalances = [];
				// selectedRowsWithData = [];
				setState({ ...state, disabled: true })
				location.reload()
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
		// }

		// console.log(cardsIds);

		// }
		// console.log(totalBalancesToBePrinted);
		// totalAmount
	}

	const getCategories = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

		axios.get(`http://localhost:8000/allcategories/${target.value}`, USER_TOKEN).then(response => {
			setState({
				...state,
				companyId: target.value,
				categories: response.data.message,
				// hiddenExcelButton: true,

			})
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const showDetails = e => {
		axios.post('http://localhost:8000/getalllactivecards',
			{
				'companyId': state.companyId,
				'categoryId': state.categoryId,
			}, USER_TOKEN).then(res2 => {
				// console.log(res2.data);
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.cards.length == 0) {
						// console.log("hiii");
						toast.error(<IntlMessages id="form.printCardsError" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							cards: res2.data.cards,
							// hiddenExcelButton: false,
						})
					}
					else {
						setState({
							...state,
							cards: res2.data.cards,
							companyName: res2.data.cards[0].company_name_ar,
							category: res2.data.cards[0].category_text
							// hiddenExcelButton: false,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5, 10, 25, 50, 100, 250, 500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		customToolbarSelect: handlemultiSelect,
		selectableRowsHeader: true,
		download: false,
		print: false,
		viewColumns: false,
		sort: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper">
					<ToastContainer />
					<Helmet>
						<title>سحب كارتات</title>
						<meta name="description" content="سحب كارتات" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.cardsWithdrawal" />} match={match} />
					<RctCollapsibleCard
						colClasses="col-sm-12 col-md-12 col-lg-12"
						heading={<IntlMessages id="sidebar.cardsWithdrawal" />}>
						<form onSubmit={handleSubmit(showDetails)}>
							<div className="form-row">
								<div className="form-group col-md-6">
									<label><IntlMessages id="form.companyName" /></label>
									<select className="form-control" name="companyId"
										onChange={getCategories} ref={register({ required: true })}>
										<option key="0" value="">برجاء اختيار اسم الشركة</option>
										{state.companies.map((company) => {
											return (
												<option key={company.company_id} value={company.company_id}>
													{company.company_name_ar}
												</option>
											)
										})
										}
									</select>
									<span className="errors">
										{errors.companyId && errors.companyId.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								{(state.companyId == '') ? <React.Fragment></React.Fragment> :
									<div className="form-group  dropdown col-md-6">
										<label><IntlMessages id="widgets.category" /></label>
										<select name="categoryId" className="form-control input-text"
											onChange={handleFields} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار الفئة</option>
											{state.categories.map(category => {
												return (
													<option key={category.cards_category_id} value={category.cards_category_id}>
														{category.category_text}
													</option>
												)
											})
											}
										</select>
										<span className="errors">
											{errors.categoryId && errors.categoryId.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>
								}

								{/* <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
						<IntlMessages id="form.clear" />
					</button> */}


								{(state.companyId == '' || state.categoryId == '') ? <React.Fragment></React.Fragment> :
									<div className="form-group col-md-6">
										<br />
										<div className="container">
											{(state.disabled === false) ?
												<button type="submit" className="btn btn-primary">
													<IntlMessages id="form.showDetails" />
												</button>
												:
												<button type="submit" className="btn btn-primary" disabled={true}>
													<IntlMessages id="form.showDetails" />
												</button>
											}
										</div>
									</div>
								}
							</div>
						</form>
						<div className="row justify-content-center">
							<div className="form-row mt-5">
								<div className="form-group col-md-12 text-center">
									{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
									<RctCollapsibleCard fullBlock>
										<MuiThemeProvider theme={getMuiTheme()}>
											<MUIDataTable
												data={state.cards}
												columns={[
													{
														label: <IntlMessages id="form.serialNumber" />,
														name: "card_id",
														options: {
															display: "none",
															filter: false
														}
													},
													{
														label: <IntlMessages id="form.serialNumber" />,
														name: "bundle_id",
														options: {
															display: "none",
															filter: false
														}
													},
													{
														label: <IntlMessages id="form.bundleId" />,
														name: "bundle_number",
													},
													{
														label: "PIN",
														name: "pin",
														// options: {
														// 	display: "none",
														// 	filter: false
														// }
													},
													{
														label: <IntlMessages id="form.serialNumber" />,
														name: "serial",
													},
													{
														label: <IntlMessages id="form.virtualMoneyBalance" />,
														name: "category_text"
													},
													{
														label: <IntlMessages id="form.serialNumber" />,
														name: "category_value",
														options: {
															display: "none",
															filter: false
														}
													},
												]}
												options={options}
											/>
										</MuiThemeProvider>
									</RctCollapsibleCard>
								</div>
							</div>
						</div>
					</RctCollapsibleCard>
				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}
