/**
 * Display And Update Categories
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        companies: [],
        categoriesByCompany: [],
        companyName: '',
        sellPriceTable: '',
        categoryId: '',  
        companyId: '',
        sellPrice: '',
        newLimitValue: '',
        newLimitValueTable: '',
        categoryValue: '',
        disabled: false,
        hidden: true,
        hiddenUpdateCardsLowLimitForm: true
    });
    
    const nf = new Intl.NumberFormat();

	useEffect(() => {
        axios.get(`http://localhost:8000/allcompanies`,USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else{
                response.data.companies.splice(0, 1);
                setState({
                    ...state,
                    companies: response.data.companies,
                    disabled: false,
                    hidden: true,
                    hiddenUpdateCardsLowLimitForm: true
                })
            }
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
            MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
                    fontSize: "30px",
					backgroundColor: "#599A5F",
					fontFamily: "'Almarai', sans-serif"
				}
            },
            MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
    })

    const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
  
    const handleRowClick = (rowData, rowIndex) => {
        axios.get(`http://localhost:8000/allcategories/${rowData[0]}`,USER_TOKEN).then(res => {
            for (let i = 0; i < res.data.message.length; i ++) {
                res.data.message[i].category_value = nf.format(res.data.message[i].category_value);
                res.data.message[i].purchase_price = nf.format(res.data.message[i].purchase_price);
                res.data.message[i].sell_price = nf.format(res.data.message[i].sell_price);
            }
        setState({ 
            ...state,
            categoriesByCompany: res.data.message,
            // companyName: res.data.message[0].company_name_ar
            companyName: rowData[1]
        })
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        }); 
    };

    const onClearClicked = () => {
        setState({
            ...state, 
            hidden: true,
            hiddenUpdateCardsLowLimitForm: true,
            sellPriceTable: '',
            categoryId: '', 
            companyId: '', 
            newLimitValue: '',
            sellPrice: ''
        })
	}

    const onSubmit = e => {
		if (state.sellPriceTable === state.sellPrice) {
			toast.error(<IntlMessages id="form.updateSellPriceError" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		} else {
            axios.post(`http://localhost:8000/updatesellprice`, {
                companyId: state.companyId,
                categoryId: state.categoryId, 
                sellPrice: state.sellPrice },USER_TOKEN
				).then(res =>{
					toast.success(<IntlMessages id="form.updateSellPriceSuccess" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
                    });
                    setState({ ...state, disabled: true })
					setTimeout(function () { location.reload()}, 2000)
				}).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                           position: "top-center",
                           autoClose: 4000,
                           hideProgressBar: false,
                           closeOnClick: true,
                           pauseOnHover: true,
                           draggable: true
                        }); 
                    }
				});
		}
    }

    const onSubmit2 = e => {
		if (state.newLimitValue === state.newLimitValueTable) {
			toast.error("برجاء تغيير الحد الأدنى لإتمام عملية التعديل", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		} else {
            axios.post(`http://localhost:8000/updatecategorylimitvalue`, {
                userTypeId: localStorage.getItem("user_type_id"),
                cardsCategoryId: state.categoryId, 
                newLimitValue: state.newLimitValue },USER_TOKEN
				).then(res =>{
					toast.success("تم تعديل الحد الأدنى بنجاح", {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
                    });
                    setState({ ...state, disabled: true })
					setTimeout(function () { location.reload()}, 2000)
				}).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                           position: "top-center",
                           autoClose: 4000,
                           hideProgressBar: false,
                           closeOnClick: true,
                           pauseOnHover: true,
                           draggable: true
                        }); 
                    }
				});
		}
    }
    
    const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
	
		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 50,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
        download: false,
		tableBodyHeight: "500px"
    };
    
    const options2 = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 25,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        // onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        download: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "500px"
	};

	const { match } = props;
    // console.log(state);
	return (
        <React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
            <Helmet>
				<title>عرض و تعديل الفئات</title>
				<meta name="description" content="عرض و تعديل الفئات" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.displayAndUpdateCategories" />} match={match} />
            <div className="row">
                <RctCollapsibleCard
                        colClasses="col-sm-12 col-md-12 col-lg-12"
                        heading={<IntlMessages id="sidebar.displayAndUpdateCategories" />}>
                    {(state.hidden == true) ? <React.Fragment></React.Fragment>
				    : 
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-row">
                            <label className="mr-4">ستقوم بتعديل سعر البيع لشركة: <span className="reportsCardsNumberCenter">{state.companyName}</span></label>
                            
                            <label className="mr-4">الفئة: <span className="reportsCardsNumberCenter">{state.categoryValue}</span></label>
                            <br />
                            <div className="form-group col-md-4">
                                <label><IntlMessages id="form.sellPrice" /></label>
                                <input type="tel" className="form-control" name="sellPrice"
                                        onChange={handleFields} value={state.sellPrice}
                                        ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                
                                <span className="errors">
                                    {errors.sellPrice && errors.sellPrice.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.sellPrice && errors.sellPrice.type === 'pattern' &&
                                        <IntlMessages id="form.numbersOnlyErrorError" /> }
                                </span>
                            </div>
                        </div>

                        {(state.disabled === false) ? 
                            <button type="submit" className="btn btn-primary">
                                <IntlMessages id="form.update" />
                            </button>
                            : 
                            <button type="submit" className="btn btn-primary" disabled={true}>
                                <IntlMessages id="form.update" />
                            </button>
					    }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
                            <IntlMessages id="form.clear" />
                        </button>
                    </form>
                    }       

                    {(state.hiddenUpdateCardsLowLimitForm == true) ? <React.Fragment></React.Fragment>
				    : 
                    <form onSubmit={handleSubmit(onSubmit2)}>
                        <div className="form-row">
                        <label className="mr-4">ستقوم بتعديل الحد الأدنى لشركة: <span className="reportsCardsNumberCenter">{state.companyName}</span></label>
                      
                        <label className="mr-4">الفئة:  <span className="reportsCardsNumberCenter">{state.categoryValue}</span></label>
                        <br />
                            <div className="form-group col-md-4">
                                <label><IntlMessages id="table.minimum" /></label>
                                <input type="tel" className="form-control" name="newLimitValue"
                                        onChange={handleFields} value={state.newLimitValue}
                                        ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                
                                <span className="errors">
                                    {errors.newLimitValue && errors.newLimitValue.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.newLimitValue && errors.newLimitValue.type === 'pattern' &&
                                        <IntlMessages id="form.numbersOnlyErrorError" /> }
                                </span>
                            </div>
                        </div>

                        {(state.disabled === false) ? 
                            <button type="submit" className="btn btn-primary">
                                <IntlMessages id="form.update" />
                            </button>
                            : 
                            <button type="submit" className="btn btn-primary" disabled={true}>
                                <IntlMessages id="form.update" />
                            </button>
					    }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
                            <IntlMessages id="form.clear" />
                        </button>
                    </form>
                    }           
                </RctCollapsibleCard>
            </div>
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.displayAndUpdateCategories" />}>
                    <div className="form-row mt-4">
                        <div className="form-group col-md-4 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.companies}
                                        columns={[
                                            {
                                                name:  "company_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="form.companyName" />,
                                                // name:  "company_name",
                                                name:  "company_name_ar",
                                            },
                                            ]}
                                        options={options}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                        <div className="form-group col-md-8 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.categoriesByCompany}
                                        title={state.companyName}
                                        columns={[
                                            {
                                                name:  "cards_category_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                name:  "company_id",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="widgets.category" />,
                                                name:  "category_value",
                                            },
                                            {
                                                label: <IntlMessages id="form.purchasePrice" />,
                                                name:  "purchase_price"
                                            },
                                            {
                                                label: <IntlMessages id="form.sellPrice" />,
                                                name:  "sell_price"
                                            },
                                            {
                                                label: <IntlMessages id="form.currency" />,
                                                name:  "category_currency"
                                            },
                                            {
                                                label: <IntlMessages id="table.minimum" />,
                                                name:  "cards_low_limit"
                                            },
                                            {
                                                label: <IntlMessages id="table.minimum" />,
                                                name:  "cards_low_limit_un",
                                                options: {
                                                    display: "none",
                                                    filter: false
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="form.updateSellPrice" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                <button type="button" className="btn btn-primary" 
                                                                    onClick={() => {
                                                                    // console.log(tableMeta.rowData[4])
                                                                    setState({  ...state , 
                                                                                categoryId:tableMeta.rowData[0],
                                                                                companyId:tableMeta.rowData[1],
                                                                                sellPrice: parseLocaleNumber(tableMeta.rowData[4]),
                                                                                sellPriceTable: tableMeta.rowData[4],
                                                                                categoryValue: tableMeta.rowData[2],
                                                                                hidden: false,
                                                                                hiddenUpdateCardsLowLimitForm: true
                                                                            })
                                                                    }}>
                                                                    <IntlMessages id="form.update" />
                                                                </button>
                                                            </React.Fragment>
                                                        );
                                                    }
                                                }
                 
                                            },
                                            {
                                                label: "تعديل الحد الأدنى",
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                <button type="button" className="btn btn-primary" 
                                                                    onClick={() => {
                                                                    // console.log(tableMeta.rowData[4])
                                                                    setState({  
                                                                                ...state , 
                                                                                categoryId:tableMeta.rowData[0],
                                                                                companyId:tableMeta.rowData[1],
                                                                                newLimitValue: tableMeta.rowData[7],
                                                                                newLimitValueTable: tableMeta.rowData[7],
                                                                                categoryValue: tableMeta.rowData[2],
                                                                                hidden: true,
                                                                                hiddenUpdateCardsLowLimitForm: false
                                                                            })
                                                                    }}
                                                                    >
                                                                    تعديل الحد الأدنى
                                                                </button>
                                                            </React.Fragment>
                                                        );
                                                    }
                                                }
                 
                                            },
                                            ]}
                                        options={options2}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                    </div>
			    </RctCollapsibleCard>
		    </div>
		</div>
        : (
			
            history.push("/access-denied")
           )
        } 
        </React.Fragment>
	)
	}