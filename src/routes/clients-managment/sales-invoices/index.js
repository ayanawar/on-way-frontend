import React, { useEffect, useState, useRef } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RctCard, RctCardContent } from 'Components/RctCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import MUIDataTable from "mui-datatables";
const cookies = new Cookies();
const Token = cookies.get('UserToken');
import { useReactToPrint } from 'react-to-print';
import PrintInvoice from './print-invoice';
import Modal from 'react-awesome-modal';



export default function Shop(props) {
    const history = useHistory();

    const componentRef = useRef();

	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
        onAfterPrint: () => { 
            setState({
                ...state
            })
            location.reload()
        },
	});
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        currencyID: '',
        secretdegree:'',
        Statement:'',
        accountname:'',
        equality:'',
        creditor:'',
        debtor:'',
        invoiceSerial: '',
        Equivalent:'',
        invoiceDate: '',
        selectedClient: '',
        newSelectedClient: '',
        selectedClientID: '',
        selectedClientAccountNumber: '',
        selectedClientCurrency: '',
        selectedClientCurrencyID: '',
        selectedClientAuditTypeID: '',
        selectedClientAuditType: '',
        selectedClientName: '',
        paymentMethod: '',
        recieptNo: '',
        notes: '',
        dollarValue: '',
        disabled: false,
        isSelected: false,
        hiddenAddNewProductButton: true,
        showModalForConfirmation: false,
        costCetnerID: '',
        costCetnerName: '',
        tableID: 1,
        productID: '',
        productName: '',
        quantity: '',
        sellPrice: '',
        discountAmount: '',
        discountPercentage: '',
        taxAmount: '',
        taxPercentage: '',
        selected_user:[],
        tableArray:[],
        products: [],
        allStatus: [],
        securityLevels: [],
        accTypes: [],
        currencies: [],
        clients: [],
        areas: [],
        costCetners:[],
        hiddenStatusForm: true,
        hiddenDebitLimitForm: true
    });

    const nf = new Intl.NumberFormat();

    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "300px"
    };
    useEffect(() => {
        let todaysDate = new Date(); 
        let year = todaysDate.getFullYear();
        let month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
        let day = ("0" + todaysDate.getDate()).slice(-2);
        let currentDate = (year +"-"+ month +"-"+ day);
        axios.get("https://accbackend.alaimtidad-itland.com/products", USER_TOKEN).then((response) => {
            axios.get("https://accbackend.alaimtidad-itland.com/all-business-units", USER_TOKEN).then((response2) => {
                axios.get("https://accbackend.alaimtidad-itland.com/all-security-levels",USER_TOKEN).then((response3) => {
                    axios.get("https://accbackend.alaimtidad-itland.com/gettype", USER_TOKEN).then((response4) => {
                        axios.get("https://accbackend.alaimtidad-itland.com/currency",USER_TOKEN).then((response5) => {
                            axios.get("https://accbackend.alaimtidad-itland.com/all-clients",USER_TOKEN).then((response6) => {
                                if (response.data == "Token Expired" || response.data =="Token UnAuthorized" ||
                                    response2.data == "Token Expired" || response2.data =="Token UnAuthorized" ||
                                    response3.data == "Token Expired" || response3.data == "Token UnAuthorized" ||
                                    response4.data == "Token Expired" || response4.data == "Token UnAuthorized" ||
                                    response5.data == "Token Expired" || response5.data == "Token UnAuthorized" ||
                                    response6.data == "Token Expired" || response6.data =="Token UnAuthorized") {
                                        cookies.remove("UserToken",{ path: "/" });
                                        window.location.href = "/signin";
                                } else {
                                    // console.log(response5.data.message);
                                        	
                                    setState({
                                        ...state,
                                        products: response.data.products,
                                        costCetners: response2.data.businessUnits,
                                        securityLevels: response3.data.levels,
                                        accTypes: response4.data.message,
                                        currencies: response5.data.message,
                                        
                                        clients: response6.data.clients,
                                        invoiceDate: currentDate,

                                        disabled: false,
                                        hiddenStatusForm: true,
                                    });
                                }
                            }).catch(error => {
                                if (error.response.status === 429) {
                                    toast.error(error.response.data, {
                                    position: "top-center",
                                    autoClose: 4000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true
                                    }); 
                                }
                            });
                        }).catch(error => {
                            if (error.response.status === 429) {
                                toast.error(error.response.data, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                                }); 
                            }
                        });
                    }).catch(error => {
                        if (error.response.status === 429) {
                            toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                            }); 
                        }
                    });
                }).catch((error) => {
                    if (error.response3.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                        });
                    }
                });
            }).catch((error) => {
                if (error.response4.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                    });
                }
            });
        }).catch((error) => {
            if (error.response4.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
        });
    }, []);

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const handleProduct = ({ target }) => {
        let selectedProduct = target.value.split(",");

        setState({ ...state, 
            [target.name]: target.value,
            productID: selectedProduct[0],
            productName:  selectedProduct[1]
        });
    }

    const handlecCostCetner = ({ target }) => {
        let selectedCostCetner = target.value.split(",");

        setState({ ...state, 
            [target.name]: target.value,
            costCetnerID: selectedCostCetner[0],
            costCetnerName: selectedCostCetner[1],
        });
    }
    

    const testPrint = () => {
        setTimeout(function () { handlePrint() }, 2000)
    }

    const handleClient = ({ target }) => {
        // console.log(target);
        
        if(state.isSelected == true) {
            let finalSelected;
            finalSelected = target.value.split(",");
 
            toast.error("عفواَ يوجد أصناف ملحقة بالفاتورة الحالية لتغيير العميل فإنك ستقوم ببدأ فاتورة جديدة", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });
            setState({
                ...state,
                showModalForConfirmation: true,
                newSelectedClient: finalSelected
                // selectedClient: target.value
            })
        } else {
            let selectedClient;
            
            selectedClient = target.value.split(",");
            handleClientDataAfterSelect(selectedClient)
            
        }

    }
    

    const handleClientDataAfterSelect = (selectedClient) => {
        // console.log("hhi", selectedClient);
        if(state.currencies.length == 0 && selectedClient[3] == 1) {
            toast.error("عفواَ لا يوجد سعر صرف فى النظام من فضلك قم بإضافة سعر الصرف لتتمكن من المتابعة", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
            });

            setState({ 
                ...state,
                selectedClient: selectedClient,
                selectedClientID: selectedClient[0],
                selectedClientAccountNumber: selectedClient[1],
                selectedClientCurrency: selectedClient[2],
                selectedClientCurrencyID: selectedClient[3],
                selectedClientAuditTypeID: selectedClient[4],
                selectedClientAuditType: selectedClient[5],
                selectedClientName: `${selectedClient[6]} ${selectedClient[7]} ${selectedClient[8]}`,
                hiddenAddNewProductButton: true,
                showModalForConfirmation: false,
                dollarValue: '',
                tableArray: [],
                tableID:1,
                productName: '',
                quantity: '',
                sellPrice: '',
                discountAmount: '',
                discountPercentage: '',
                taxAmount: '',
                taxPercentage: '',
            });

        } else if(state.currencies.length > 0 && selectedClient[3] == 1) {
            setState({ 
                ...state,
                selectedClient: selectedClient,
                selectedClientID: selectedClient[0],
                selectedClientAccountNumber: selectedClient[1],
                selectedClientCurrency: selectedClient[2],
                selectedClientCurrencyID: selectedClient[3],
                selectedClientAuditTypeID: selectedClient[4],
                selectedClientAuditType: selectedClient[5],
                selectedClientName: `${selectedClient[6]} ${selectedClient[7]} ${selectedClient[8]}`,
                hiddenAddNewProductButton: false,
                showModalForConfirmation: false,
                dollarValue: state.currencies[0].dollar_value,
                tableArray: [],
                tableID:1,
                productName: '',
                quantity: '',
                sellPrice: '',
                discountAmount: '',
                discountPercentage: '',
                taxAmount: '',
                taxPercentage: '',
                // dollarValue: state.currencies[0].dollar_value
            });
            
        } else {
            setState({ 
                ...state,
                selectedClient: selectedClient,
                selectedClientID: selectedClient[0],
                selectedClientAccountNumber: selectedClient[1],
                selectedClientCurrency: selectedClient[2],
                selectedClientCurrencyID: selectedClient[3],
                selectedClientAuditTypeID: selectedClient[4],
                selectedClientAuditType: selectedClient[5],
                selectedClientName: `${selectedClient[6]} ${selectedClient[7]} ${selectedClient[8]}`,
                tableArray: [],
                tableID:1,
                dollarValue: '',
                productName: '',
                quantity: '',
                sellPrice: '',
                discountAmount: '',
                discountPercentage: '',
                taxAmount: '',
                taxPercentage: '',
                hiddenAddNewProductButton: false,
                showModalForConfirmation: false,
            });
        }   
    }


    

    const addProductToInvoiceSubTable = () => {
        if(state.tableID != '' && state.productName != '' && state.quantity != '' && state.invoiceSerial != '' & state.costCetnerID != '' && state.paymentMethod != '') {
            let obj= {
                tableID: state.tableID,
                productID: state.productID,
                productName: state.productName,
                quantity: state.quantity,
                discountAmount: state.discountAmount,
                discountPercentage: 0,
                taxAmount: state.taxAmount,
                taxPercentage: 0,
            }
    
            if(state.selectedClientCurrencyID == 1) {
                obj.sellPrice = (state.sellPrice * state.dollarValue)
                obj.discountPercentage = ((state.discountAmount / obj.sellPrice) * 100 +" %")
                obj.taxPercentage = ((state.taxAmount / obj.sellPrice) * 100 +" %")
            } else {
                obj.sellPrice = state.sellPrice
                obj.discountPercentage = ((state.discountAmount / obj.sellPrice) * 100 +" %")
                obj.taxPercentage = ((state.taxAmount / obj.sellPrice) * 100 +" %")
            }
          
            state.tableArray.push(obj)
            setState({
                ...state,
                isSelected: true, 
                hiddentextbox: false,
                hiddentable:false, 
                tableArray:state.tableArray,
                tableID:(state.tableID+1),
                productID: '',
                productName: '',
                quantity: '',
                sellPrice: '',
                discountAmount: '',
                discountPercentage: 0,
                taxAmount: '',
                taxPercentage: 0,
            })    
        }  
    }

    const handleConfirmation = () => {
        setState({
            ...state, 
            showModalForConfirmation: false,
            isSelected: false,
            selectedClient: state.newSelectedClient,
            newSelectedClient: state.newSelectedClient,
            tableArray: [],
            tableID:1,
            productName: '',
            quantity: '',
            sellPrice: '',
            discountAmount: '',
            discountPercentage: '',
            taxAmount: '',
            taxPercentage: '',
        })  
        
        handleClientDataAfterSelect(state.newSelectedClient)
    }
        


    const  removeRow = (r) => {  

        for( var i = 0; i < state.tableArray.length; i++){ 
            // console.log(state.tableArray[i].tableid == r);
            // console.log(state.tableArray[i].tableid);
            // console.log(r);
            if (state.tableArray[i].tableid == r) { 
                // console.log("state.tableArray[i]",state.tableArray[i]);
                state.tableArray.splice(i, 1); 
                break;
            }
        }
        //    console.log("event", state.tableArray);
           setState({...state, tableArray:state.tableArray})
           if (state.tableArray.length == 0){
               setState({...state, hiddentable: true})
           }
    }

    const closeModel3 = () => {
		setState({
			...state,
            showModalForConfirmation: false,
		});
	}

    const getMuiTheme = () => createMuiTheme({
    overrides: {
        MUIDataTable: {
            responsiveScroll: {
                maxHeight: 'unset',
                overflowX: 'unset',
                overflowY: 'unset',
            },
        },
        MuiTableCell: {
            head: {
                color: "#599A5F",
                fontWeight: "bold",
                fontSize: "15px",
                fontFamily: "'Almarai', sans-serif"
            }, 
            body: {
                color: "#092346",
                fontWeight: "bold",
                fontSize: "15px",
                fontFamily: "'Almarai', sans-serif",
            }   
        },
        MUIDataTableHeadCell: {
            data: {
                color: "#599A5F",
                fontWeight: "bold",
                fontFamily: "'Almarai', sans-serif"
            },
            fixedHeader: {
                position: "sticky !important",
                zIndex: '100',
            }
        },
        MUIDataTableSelectCell: {
            headerCell: {
                zIndex: 1
            },
            fixedLeft: {
                zIndex: 1
            }
        },
        MUIDataTableToolbarSelect: {
            root: {
                color: "#599A5F",
                fontWeight: "bold",
                zIndex: 1,
            }   
        },
        MuiPaper: {
            root: {
                color: "#092346",
                fontWeight: "bold",
                fontFamily: "'Almarai', sans-serif"
            }
        },
        MuiToolbar:{
            regular: {
                backgroundColor:"gray"
            },
            root: {
                top: 0,
                position: 'sticky',
                background: 'white',
                zIndex: '100',
            },
        },
        MUIDataTablePagination: {
            tableCellContainer: {
                backgroundColor:"gray"
            }
        },
        MUIDataTableBody: {
            emptyTitle: {
                display: "none",
            }
        }
    }
})

    const onSubmit = e => {

    //  let data = new FormData();
    //  data.append('user_type_id', '4'),
    //      data.append('user_first_name', state.user_first_name),
    //      data.append('user_middle_name', state.user_middle_name),
    //      data.append('user_last_name', state.user_last_name),
    //      data.append('supplier_company_id', state.supplier_company_id),
    //      data.append('user_region_id', state.user_region_id),
    //      data.append('user_area_id', state.user_area_id),
    //      data.append('user_phonenumber', state.user_phonenumber),
    //      data.append('user_userlevel', state.user_level),
    //      data.append('tax_registration_number)', state.tax_registration_number),
    //      // data.append('user_email', state.user_email),
    //      // data.append('user_password', state.user_password),
    //      // data.append('user_personal_image', state.user_personal_image),

    //      axios({
    //          method: 'post', url: 'http://localhost:8000/createaccount', USER_TOKEN, data: data,
    //          headers: { 'Content-Type': 'multipart/form-data', 'x-access-token': `${Token}` }
    //      }).then(res => {
    //          toast.success(<IntlMessages id="form.addSupplierSuccess" />, {
    //              position: "top-center",
    //              autoClose: 4000,
    //              hideProgressBar: false,
    //              closeOnClick: true,
    //              pauseOnHover: true,
    //              draggable: true
    //          });
    //          setState({ ...state, disabled: true })
    //          setTimeout(function () { location.reload()}, 3000)
    //      }).catch(error => {
    //          // console.log(error.response.data.message);
    //          if (error.response.data.message) {
    //              toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
    //                  position: "top-center",
    //                  autoClose: 4000,
    //                  hideProgressBar: false,
    //                  closeOnClick: true,
    //                  pauseOnHover: true,
    //                  draggable: true
    //              });
    //          } else if (error.response.status === 429) {
    //              toast.error(error.response.data, {
    //                 position: "top-center",
    //                 autoClose: 4000,
    //                 hideProgressBar: false,
    //                 closeOnClick: true,
    //                 pauseOnHover: true,
    //                 draggable: true
    //              }); 
    //          }
    //      });
    }

    const { match } = props;
    // console.log(state);
    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="shop-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>فواتير المبيعات</title>
                        <meta name="description" content="فواتير المبيعات" />
                    </Helmet>
                    <PageTitleBar title={"بيانات الفاتورة"} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.salesInvoices" />}>
                                    
                            <div className="form-row"> 
                                <div className="form-group col-md-4">
                                    <label><IntlMessages id="widgets.currentDate" /></label>
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {new Date().getMonth()+1}/{new Date().getDate()}/{new Date().getFullYear()}</span>
                                </div>
                                <div className="form-group col-md-4">
                                    <label><IntlMessages id="form.recieptsDate" /></label>
                                    <input name="invoiceDate" placeholder="date placeholder"
											defaultValue={state.invoiceDate}
											type="date" className="form-control" onChange={handleFields}
											ref={register({ required: true })} />
                                    <span className="errors m-2">
                                        {errors.invoiceDate && errors.invoiceDate.type === 'required' &&
                                            <IntlMessages id="form.requiredOptionError" />}
                                    </span>
                                </div>
                            </div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-row">
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="table.serial" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                            <input type="number" className="form-control" name="invoiceSerial"
                                                value={state.invoiceSerial}
                                                onChange={handleFields}
                                                ref={register({ required: true })} />                                                        
                                            
                                        </div>
                                            <span className="errors">
                                                {errors.invoiceSerial && errors.invoiceSerial.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                            
                                            </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="sidebar.client" /></label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                            lineHeight: "0px",
                                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                                }}>
                                                                                                
                                                    <select className="form-control input-text"
                                                            name="clientIDAndAccountNumber"
                                                            onChange={handleClient}
                                                            ref={register({required: true,})}>
                                                        <option key="0" value="">
                                                                برجاء اختيار العميل 
                                                        </option>
                                                            {state.clients.map(
                                                                (client) => {
                                                                    return (
                                                                        <option key={client.client_id}
                                                                                value={[client.client_id, client.client_account_number, client.currency_ar, client.acc_currency_id, client.audit_type_id, client.audit_type_ar, client.client_first_name, client.client_middle_name,client.client_last_name]}>
                                                                            {client.client_first_name} {client.client_middle_name} {client.client_last_name}
                                                                        </option>
                                                                    );
                                                                }
                                                            )}
                                                    </select>
                                            </div>
                                                <span className="errors">
                                                    {errors.clientIDAndAccountNumber && errors.clientIDAndAccountNumber.type === 'required' &&
                                                        <IntlMessages id="form.requiredOptionError" />}
                                                
                                                </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="sidebar.clientaccount" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                                                                                
                                            <span className="form-control">
                                            {state.selectedClientAccountNumber}
                                                    
                                            </span>
                                        </div>
                                        <span className="errors">
                                            
                                        </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="FORM.DEBT" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                            <span className="form-control">
                                                {state.selectedClientAuditType}
                                            </span>
                                                                                                
                                        </div>
                                        <span className="errors">
                                                                              
                                        </span>
                                    </div>
                                
                                
                                {/* </div> */}
                                {/* <div className="form-row"> */}
                                    
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.currency" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                                                                                
                                            <span className="form-control">
                                                {state.selectedClientCurrency}
                                            </span>
                                        </div>
                                        <span className="errors">
                                            
                                        </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.PayMethod" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                                                                                
                                                <input type="text" className="form-control" name="paymentMethod"
                                                    value={state.paymentMethod}
                                                    onChange={handleFields}
                                                    ref={register({ required: true })} />
                                        </div>
                                        <span className="errors">
                                            {errors.paymentMethod && errors.paymentMethod.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}
                                        
                                        </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.recieptNo" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                                                                                
                                        <input type="number" className="form-control" name="recieptNo"
                                        value={state.recieptNo}
                                            onChange={handleFields} />
                                    </div>
                                        <span className="errors">
                                                                              
                                        </span>
                                    </div>

                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.costcenterName" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>

                                            <select className="form-control input-text"
                                                name="selectedCostCetner"
                                                onChange={handlecCostCetner}
                                                ref={register({required: true,})}>
                                                <option key="0" value="">
                                                        برجاء اختيار مركز التكلفة 
                                                </option>
                                                    {state.costCetners.map(
                                                        (costCetner) => {
                                                            return (
                                                                <option key={costCetner.business_unit_id}
                                                                        value={[costCetner.business_unit_id,costCetner.business_unit_name]}>
                                                                    {costCetner.business_unit_name}
                                                                </option>
                                                            );
                                                        }
                                                    )}
                                            </select>                            
                                        </div>
                                        <span className="errors">
                                            {errors.selectedCostCetner && errors.selectedCostCetner.type === 'required' &&
                                                <IntlMessages id="form.requiredOptionError" />}                                    
                                        </span>
                                    </div>
                                
                                   
                                </div>
                                


                                    <div className="form-row">

                                    <div className="form-group col-md-12">
                                        <label><IntlMessages id="widgets.note" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                            }}>
                                                                                                    
                                            <input type="text" className="form-control" name="notes"
                                            value={state.notes}
                                                onChange={handleFields}
                                                 />
                                        </div>
                                        <span className="errors">
                                        
                                        </span>
                                    </div>
                                    
                                </div>
                            </form>
                        </RctCollapsibleCard>

                    <RctCollapsibleCard
                        colClasses="col-sm-12 col-md-12 col-lg-12"
                        heading={"إضافة صنف"}>
                        <form onSubmit={handleSubmit(addProductToInvoiceSubTable)}>
                            <div className="form-row">
                                <div className="form-group col-md-3">
                                    <label><IntlMessages id="form.productName" /></label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                         

                                            <select className="form-control input-text"
                                                    name="selectedProduct"
                                                    onChange={handleProduct}>
                                                <option key="0" value="">
                                                    برجاء اختيار الصنف 
                                                </option>
                                                    {state.products.map(
                                                        (product) => {
                                                            return (
                                                                <option key={product.product_id}
                                                                        value={[product.product_id, product.product_name]}>
                                                                    {product.product_name}
                                                                </option>
                                                            );
                                                        }
                                                    )}
                                            </select>                                                     
                                        
                                            </div>
                                        <span className="errors">
                                            {/* {errors.productName && errors.productName.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />} */}
                                        
                                        </span>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>الكمية</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                        <input type="number" className="form-control" name="quantity"
                                            value={state.quantity}
                                            id="quantity"
                                            onChange={handleFields}
                                            // ref={register({ required: true })}
                                             />                                                        
                                        
                                    </div>
                                        <span className="errors">
                                            {/* {errors.quantity && errors.quantity.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />} */}
                                        
                                        </span>
                                </div>
                                <div className="form-group col-md-3">
                                    <label><IntlMessages id="form.sellPrice" /></label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                        <input type="number" className="form-control" name="sellPrice"
                                        value={state.sellPrice}
                                        id="sellPrice"
                                            onChange={handleFields}
                                            // ref={register({ required: true })} 
                                            />                                                        
                                        
                                    </div>
                                        <span className="errors">
                                            {/* {errors.sellPrice && errors.sellPrice.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />} */}
                                        
                                        </span>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>العملة</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                        <span className="form-control">
                                            {state.selectedClientCurrency} 
                                        </span>                                                      
                                        
                                    </div>
                                    {(state.currencies.length > 0 && state.selectedClientCurrencyID == 1) ? 
                                        <React.Fragment>
                                            <label>سعر الصرف</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                            }}>
                                                <span className="form-control">
                                                    {state.dollarValue} 
                                                </span>                                                          
                                                
                                            </div>  
                                        </React.Fragment> 
                                    :
                                        <React.Fragment></React.Fragment>
                                    }
                                                 
                                </div>
                                <div className="form-group col-md-3">
                                    <label>قيمة الخصم بالدينار العراقى</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                    borderWidth: "0px 2px 2px 0px",
                                                    lineHeight: "0px",
                                                    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                        <input type="number" className="form-control" name="discountAmount"
                                            value={state.discountAmount}
                                            id="discountAmount"
                                            onChange={handleFields} />                                                        
                                        
                                    </div>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>نسبة الخصم</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                            {(state.discountAmount != '' && state.selectedClientCurrencyID == 1) ?  
                                                <span className="form-control">
                                                {((state.discountAmount / (state.sellPrice * state.dollarValue)) * 100 +" %")}
                                                </span>
                                            : 
                                                <span className="form-control">
                                                
                                                </span>
                                            }    

                                            {(state.discountAmount != '' && state.sellPrice != '' && state.selectedClientCurrencyID != 1) ?  
                                                <span className="form-control">
                                                {((state.discountAmount / state.sellPrice) * 100 +" %")}
                                                </span>
                                            : 
                                                <span className="form-control">
                                                
                                                </span>
                                            }                                                  
                                    </div>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>قيمة الضريبة بالدينار العراقى</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                        <input type="number" className="form-control" name="taxAmount"
                                            value={state.taxAmount}
                                            onChange={handleFields}
                                            id="taxAmount" />                                                        
                                        
                                    </div>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>نسبة الضريبة</label>
                                    <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                        borderWidth: "0px 2px 2px 0px",
                                        lineHeight: "0px",
                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                    }}>
                                        
                                            {(state.taxAmount != '' && state.selectedClientCurrencyID == 1) ?  
                                                <span className="form-control">
                                                {((state.taxAmount / (state.sellPrice * state.dollarValue)) * 100 +" %")}
                                                </span>
                                            : 
                                                <span className="form-control">
                                                
                                                </span>
                                            }    

                                            {(state.taxAmount != '' && state.sellPrice != '' && state.selectedClientCurrencyID != 1) ?  
                                                <span className="form-control">
                                                {((state.taxAmount / state.sellPrice) * 100 +" %")}
                                                </span>
                                            : 
                                                <span className="form-control">
                                                
                                                </span>
                                            }
                                       
                                    </div>
                                </div>        
                            </div>
                            {(state.hiddenAddNewProductButton == true) ?
                                <React.Fragment></React.Fragment> 
                                :
                                <button onClick={()=>{addProductToInvoiceSubTable()}}
                                    className=" btn-margin" style={{ margin:"30px 30px 20px 0" ,color:'#fff',backgroundColor: "#0063c1", 
                                    fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                    إضافة صنف
                                </button>
                            }
                            
                        </form>
                    </RctCollapsibleCard>
                </div>

        {state.showModalForConfirmation == true ?
            <Modal visible={state.showModalForConfirmation} width="550" height="350" effect="fadeInUp" 
                   onClickAway={closeModel3}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h3 className="modal-title"> 
                            <span className="ml-3"> عفواَ يوجد أصناف ملحقة بالفاتورة الحالية لتغيير العميل فإنك ستقوم ببدأ فاتورة جديدة</span>
                        </h3> 
                        <br />
                    </div>
                    
                    {/*  */}
                    <div className="modal-footer">
                        <button type="submit" className="btn btn-primary"
                                onClick={handleConfirmation}>
                            موافق
                        </button>                             
                        
                        <button type="button" className="btn btn-secondary" 
                                onClick={closeModel3}>
                            إغلاق
                        </button>
                    </div>
                </div>	
            </Modal>
        : 
            null
        }

                    <div style={{ display: "none" }}>
                        <PrintInvoice ref={componentRef} {...state} />				
                    </div>
                    
            <div className="row mb-5">
                <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                    <RctCard>
                    {(state.tableArray.length == 0) ? 
                        <React.Fragment></React.Fragment> :
                            <button style={{ margin:"30px 30px 20px 0" ,color:'#fff',backgroundColor: "#0063c1", 
                                    fontSize:"17px", fontWeight: "bold", height:'50px'}}
                                        onClick={()=> testPrint()}>تأكيد و طباعة الفاتورة</button>
                    }
                        <RctCardContent noPadding>
                            <MuiThemeProvider theme={getMuiTheme()}>
                                <MUIDataTable 
                                // title={<IntlMessages id="sidebar.cart" />}
                                    data={state.tableArray}
                                    columns={[
                                        {
                                            label: "id",
                                            name:  "tableID",
                                            options:{
                                                display:false,
                                                filter:false,
                                                print:false
                                            }
                                        },
                                        
                                        {
                                            label: <IntlMessages id="form.productName" />,
                                            name:  "productName",
                                        },
                                        {
                                            label: "الكمية",
                                            name:  "quantity"
                                        },
                                        {
                                            label: "سعر البيع بالدينار العراقي",
                                            name:  "sellPrice",
                                        },
                                        {
                                            label: "قيمة الخصم بالدينار العراقى",
                                            name:  "discountAmount",
                                        },
                                        {
                                            label: "نسبة الخصم",
                                            name:  "discountPercentage",
                                        },
                                        {
                                            label: "قيمة الضريبة بالدينار العراقى",
                                            name:  "taxAmount",
                                        },
                                        {
                                            label: "نسبة الضريبة",
                                            name:  "taxPercentage",
                                        },
                                        {
                                            label: <IntlMessages id="button.delete" />,
                                            name: "tableid",
                                            options: {
                                                filter: true,
                                                sort: false,
                                                empty: true,
                                                print: false,
                                                customBodyRender: (value, tableMeta, updateValue) => {
                                                return (
                                                    <React.Fragment>
                                                        <button type="button" className="btn btn-danger" 
                                                        onClick={() => {removeRow(value)}}>
                                                            <IntlMessages id="form.delete" />
                                                        </button>
                                                    </React.Fragment>
                                                );
                                            }
                                            }
                    
                                        },
                                        
                                    ]}
                                    options={options}
                                />
                            </MuiThemeProvider>
                        </RctCardContent>
                    </RctCard>
                </RctCollapsibleCard>
                
            </div>
            

                </div>
                : (

                    history.push("/access-denied")
                )
            }
        </React.Fragment>
    )
}