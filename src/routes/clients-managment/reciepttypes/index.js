/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
  
    
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
        invoices: [],
         disabled: false,
         hiddenStatusForm: true,
     });
     useEffect(() => {
         // all suppliers
         axios.get('https://accbackend.alaimtidad-itland.com/all-invoices-types',USER_TOKEN).then(response => {
             if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', {path:'/'})
                 window.location.href = "/signin";	
             }
             else{
                // console.log(response.data);
                 setState({
                     invoices: response.data.invoices,
                     disabled: false,
                     hiddenStatusForm: true,
                 })
             }
         })
         .catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);
 
     const onClearStatusClicked = () => {
         setState({...state,
             user_type_id: '',
             hiddenStatusForm: true,
             hiddenDebitLimitForm: true,
             debit_limit: '',
             debitLimitTable: '',
             user_phonenumber: '',
             representativeId: '',
             dealerId: '',
             representativeId: '',
             user_id: '',
             dealerFirstName: '',
             dealerMiddleName: '', 
             dealerLastName: '',
             repFirstName: '',
             repMiddleName: '',
             repLastName: '',
             status: ''
         })
     };
 
     const onSubmit2 = e => {
         axios({
             method: 'post', url: 'https://accbackend.alaimtidad-itland.com/all-invoices-types', data: state,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
             toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
             setState({ ...state, disabled: true })
             setTimeout(function () { location.reload()}, 3000)
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
    
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })
 
 //    render() {
       const { match } = props;
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>انواع الفواتير</title>
                 <meta name="description" content="انواع الفواتير" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.reciepttypes" />} match={match} />

             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                     data={state.invoices}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "invoice_type_id",
                                             
                                             },
                                             {
                                                 label: <IntlMessages id="form.reciepttype" />,
                                                 name:  "invoice_type",
                                             },
                                          
                                         
                                       
                                            
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }