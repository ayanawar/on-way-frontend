import React, { useEffect, useState } from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from "Components/RctCard";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

// intl messages
import IntlMessages from "Util/IntlMessages";
import RctCollapsibleCard from "Components/RctCollapsibleCard/RctCollapsibleCard";

// page title bar
import PageTitleBar from "Components/PageTitleBar/PageTitleBar";
import USER_TOKEN from "../../../constants/Token";
import PURE_TOKEN_USER_TYPE from "../../../constants/TokenUserType";
import PURE_TOKEN_PHONE_NUM from "../../../constants/TokenPhoneNum";
import Cookies from "universal-cookie";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const cookies = new Cookies();

const options = {
    filter: true,
    filterType: "dropdown",
    rowsPerPage: 10,
    rowsPerPageOptions: [5, 10, 25, 50, 100],
    responsive: "vertical",
    enableNestedDataAccess: ".",
    selectableRows: "none",
    viewColumns: false,
    sort: false,
    fixedHeader: true,
    download: false,
    fixedSelectColumn: false,
    tableBodyHeight: "600px",
};

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        clients: [],
        allStatus: [],
        securityLevels: [],
        accTypes: [],
        currencies: [],
        regions: [],
        areas: [],
        clientAccountNumber: "",
        clientFirstName: "",
        clientMiddleName: "",
        clientLastName: "",
        accNameAr: "",
        accNameEn: "",
        accDesc: "",
        clientRegionID: "",
        clientAreaID: "",
        clientPhoneNumber: "",
        securityLevel: "",
        accType: "",
        status: "",
        currencyID: "",
      
        disabled: false,
        hiddenStatusForm: true,
    });
    useEffect(() => {
        // all clients
        axios.get("https://accbackend.alaimtidad-itland.com/all-clients", USER_TOKEN).then((response) => {
            axios.get("https://accbackend.alaimtidad-itland.com/getallstatus", USER_TOKEN).then((response2) => {
                axios.get("https://accbackend.alaimtidad-itland.com/all-security-levels",USER_TOKEN).then((response3) => {
                    axios.get("https://accbackend.alaimtidad-itland.com/gettype", USER_TOKEN).then((response4) => {
                        axios.get("https://accbackend.alaimtidad-itland.com/getcurrency",USER_TOKEN).then((response5) => {
                            axios.get("https://accbackend.alaimtidad-itland.com/all-regions",USER_TOKEN).then((response6) => {
                                if (response.data == "Token Expired" || response.data =="Token UnAuthorized" ||
                                    response2.data == "Token Expired" || response2.data =="Token UnAuthorized" ||
                                    response3.data == "Token Expired" || response3.data == "Token UnAuthorized" ||
                                    response4.data == "Token Expired" || response4.data == "Token UnAuthorized" ||
                                    response5.data == "Token Expired" || response5.data == "Token UnAuthorized" ||
                                    response6.data == "Token Expired" || response6.data =="Token UnAuthorized") {
                                        cookies.remove("UserToken",{ path: "/" });
                                        window.location.href = "/signin";
                                } else {
                                    console.log(response.data.clients);
                                    setState({
                                        ...state,
                                        clients: response.data.clients,
                                        allStatus: response2.data.message,
                                        securityLevels: response3.data.levels,
                                        accTypes: response4.data.message,
                                        currencies: response5.data.message,
                                        regions: response6.data.regions,

                                        disabled: false,
                                        hiddenStatusForm: true,
                                        });
                                }
                            }).catch(error => {
                                if (error.response.status === 429) {
                                    toast.error(error.response.data, {
                                    position: "top-center",
                                    autoClose: 4000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true
                                    }); 
                                }
                            });
                        }).catch(error => {
                            if (error.response.status === 429) {
                                toast.error(error.response.data, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                                }); 
                            }
                        });
                    }).catch(error => {
                        if (error.response.status === 429) {
                            toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                            }); 
                        }
                    });
                }).catch((error) => {
                    if (error.response3.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                        });
                    }
                });
            }).catch((error) => {
                if (error.response4.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                    });
                }
            });
        }).catch((error) => {
            if (error.response4.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
        });
    }, []);

    const onSubmit2 = (e) => {
        axios({
            method: "post",
            url: "https://accbackend.alaimtidad-itland.com/update-client-data",
            data: state,
            headers: { "x-access-token": `${cookies.get("UserToken")}` },
        })
            .then((res) => {
                toast.success("تم تعديل بيانات العميل بنجاح", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
                setState({ ...state, disabled: true });
                setTimeout(function () {location.reload();}, 3000);
            })
            .catch((error) => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                    });
                }
            });
    };

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const handleAreasByRegionID = (e) => {
        handleFields(e);
        let regionId = e.target.value;
        // console.log("regionID", regionId);
        axios.post("https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID",
                { regionID: regionId },
                USER_TOKEN
            ).then((response) => {
                if (
                    response.data == "Token Expired" ||
                    response.data == "Token UnAuthorized"
                ) {
                    cookies.remove("UserToken", { path: "/" });
                    window.location.href = "/signin";
                } else {
                    // response3.data.companies.splice(0, 1);
                    if (response.data.areas.length == 0) {
                        toast.error(
                            "لايوجد حاليا مناطق متوفرة فى هذة المحافظة",
                            {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                            }
                        );

                        setState({
                            ...state,
                            clientRegionID: regionId,
                            areas: response.data.areas,
                            clientAreaID: "",
                        });
                    } else {
                        setState({
                            ...state,
                            clientRegionID: regionId,
                            areas: response.data.areas,
                        });
                    }
                }
            })
        .catch((error) => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
        });
    };

    const getMuiTheme = () =>
        createMuiTheme({
            overrides: {
                MUIDataTable: {
                    responsiveScroll: {
                        maxHeight: "unset",
                        overflowX: "unset",
                        overflowY: "unset",
                    },
                },
                MuiTableCell: {
                    head: {
                        color: "#599A5F",
                        fontWeight: "bold",
                        fontSize: "15px",
                        fontFamily: "'Almarai', sans-serif",
                    },
                    body: {
                        color: "#092346",
                        fontWeight: "bold",
                        fontSize: "15px",
                        fontFamily: "'Almarai', sans-serif",
                    },
                },
                MUIDataTableHeadCell: {
                    data: {
                        color: "#599A5F",
                        fontWeight: "bold",
                        fontFamily: "'Almarai', sans-serif",
                    },
                    fixedHeader: {
                        position: "sticky !important",
                        zIndex: "100",
                    },
                },
                MUIDataTableSelectCell: {
                    headerCell: {
                        zIndex: 1,
                    },
                    fixedLeft: {
                        zIndex: 1,
                    },
                },
                MUIDataTableToolbarSelect: {
                    root: {
                        color: "#599A5F",
                        fontWeight: "bold",
                        zIndex: 1,
                    },
                },
                MuiPaper: {
                    root: {
                        color: "#092346",
                        fontWeight: "bold",
                        fontFamily: "'Almarai', sans-serif",
                    },
                },
                MuiToolbar: {
                    regular: {
                        backgroundColor: "gray",
                    },
                    root: {
                        top: 0,
                        position: "sticky",
                        background: "white",
                        zIndex: "100",
                    },
                },
                MUIDataTablePagination: {
                    tableCellContainer: {
                        backgroundColor: "gray",
                    },
                },
                MUIDataTableBody: {
                    emptyTitle: {
                        display: "none",
                    },
                },
            },
        });
    
        const onClearClicked = () => {
            setState({
                ...state,
                hiddenStatusForm: true,
                clientAccountNumber: "",
                clientFirstName: "",
                clientMiddleName: "",
                clientLastName: "",
                accNameAr: "",
                accNameEn: "",
                accDesc: "",
                clientRegionID: "",
                clientAreaID: "",
                clientPhoneNumber: "",
                securityLevel: "",
                accType: "",
                status: "",
                currencyID: "",
            })
        };

    //    render() {
        
    const { match } = props;
    // console.log(state);
    return (
        <React.Fragment>
            {PURE_TOKEN_USER_TYPE === 5 &&
            PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ? (
                <div className="cart-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>قائمة العملاء</title>
                        <meta
                            name="description"
                            content="عرض و تعديل العملاء"
                        />
                    </Helmet>
                    <PageTitleBar
                        title={<IntlMessages id="sidebar.allClients" />}
                        match={match}
                    />

                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={"تعديل بيانات العملاء"}
                        >
                            {/*  */}
                            {/* <form onSubmit={handleSubmit(onSubmit2)}>	
                        <div className="form-row">
                        <div className="form-group col-md-4">
                            <label>
                                <IntlMessages id="searchbyaccnum" />
                            </label>
                            </div>
                            <div className="form-group col-md-4">
                            <input type="text" className="form-control" name="user_first_name"
                                            onChange={handleFields}
                            />
                        
                    
                        </div>
                        
                        {(state.disabled === false) ? 
                                    <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                    fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                        <IntlMessages id="widgets.search" />
                                    </button> : 
                                    <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                    fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                        <IntlMessages id="widgets.search" />
                                    </button>
                                }

</div>
                    </form> */}

                            {state.hiddenStatusForm == true ? (
                                <React.Fragment></React.Fragment>
                            ) : (
                                // <form onSubmit={handleSubmit(onSubmit2)}>

                                //             <div className="form-row">
                                //                 <div className="form-group col-md-4" >
                                //                     <label><IntlMessages id="form.firstName" /></label>
                                //                     <input type="text" className="form-control" name="user_first_name"
                                //                         onChange={handleFields}
                                //                         ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                //                     <span className="errors">
                                //                         {errors.user_first_name && errors.user_first_name.type === 'required' &&
                                //                             <IntlMessages id="form.requiredError" />}
                                //                         {errors.user_first_name && errors.user_first_name.type === 'minLength' &&
                                //                             <IntlMessages id="form.minLengthError" />}
                                //                         {errors.user_first_name && errors.user_first_name.type === 'pattern' &&
                                //                             <IntlMessages id="form.lettersOnlyError" />}
                                //                     </span>
                                //                 </div>
                                //                 <div className="form-group col-md-4">
                                //                     <label><IntlMessages id="form.middleName" /></label>
                                //                     <input type="text" className="form-control" name="user_middle_name"
                                //                         onChange={handleFields}
                                //                         ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                //                     <span className="errors">
                                //                         {errors.user_middle_name && errors.user_middle_name.type === 'required' &&
                                //                             <IntlMessages id="form.requiredError" />}
                                //                         {errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
                                //                             <IntlMessages id="form.minLengthError" />}
                                //                         {errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
                                //                             <IntlMessages id="form.lettersOnlyError" />}
                                //                     </span>
                                //                 </div>
                                //                 <div className="form-group col-md-4">
                                //                     <label><IntlMessages id="form.lastName" /></label>
                                //                     <input type="text" className="form-control" name="user_last_name"
                                //                         onChange={handleFields}
                                //                         ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                //                     <span className="errors">
                                //                         {errors.user_last_name && errors.user_last_name.type === 'required' &&
                                //                             <IntlMessages id="form.requiredError" />}
                                //                         {errors.user_last_name && errors.user_last_name.type === 'minLength' &&
                                //                             <IntlMessages id="form.minLengthError" />}
                                //                         {errors.user_last_name && errors.user_last_name.type === 'pattern' &&
                                //                             <IntlMessages id="form.lettersOnlyError" />}
                                //                     </span>
                                //                 </div>
                                //             </div>

                                //             <div className="form-row">
                                //                 <div className="form-group  dropdown col-md-6">
                                //                     <label><IntlMessages id="form.region" /></label>
                                //                     <select name="user_region_id" className="form-control input-text"
                                //                         onChange={handleAreasByRegionID} ref={register({ required: true })}>
                                //                         <option key="0" value="">برجاء اختيار المحافظة</option>
                                //                         {state.regions.map(region => {
                                //                             return (
                                //                                 <option key={region.region_id} value={region.region_id}>
                                //                                     {region.region_arabic_name}
                                //                                 </option>
                                //                             )
                                //                         })
                                //                         }
                                //                     </select>
                                //                     <span className="errors">
                                //                         {errors.user_region_id && errors.user_region_id.type === 'required' &&
                                //                             <IntlMessages id="form.requiredOptionError" />}
                                //                     </span>
                                //                 </div>
                                //                 <div className="form-group  dropdown col-md-6">
                                //                     <label><IntlMessages id="form.area" /></label>
                                //                     <select name="user_area_id" className="form-control input-text"
                                //                         onChange={handleFields} ref={register({ required: true })}>
                                //                         <option key="0" value="">برجاء اختيار المنطقة</option>
                                //                         {state.areas.map(area => {
                                //                             return (
                                //                                 <option key={area.area_id} value={area.area_id}>
                                //                                     {area.area_arabic_name}
                                //                                 </option>
                                //                             )
                                //                         })
                                //                         }
                                //                     </select>
                                //                     <span className="errors">
                                //                         {errors.user_area_id && errors.user_area_id.type === 'required' &&
                                //                             <IntlMessages id="form.requiredOptionError" />}
                                //                     </span>
                                //                 </div>
                                //             </div>

                                //             <div className="form-row">
                                //                 <div className="form-group col-md-6">
                                //                     <label><IntlMessages id="form.jobtitle" /></label>
                                //                     <input type="email" className="form-control" name="user_email"
                                //                         onChange={handleFields} />

                                //                 </div>

                                //                 <div className="form-group col-md-6">
                                //                     <label><IntlMessages id="form.salary" /></label>
                                //                     <input type="email" className="form-control" name="user_email"
                                //                         onChange={handleFields}
                                //                     />

                                //                 </div>
                                //                 </div>

                                //             {(state.disabled === false) ?
                                //                 <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1",
                                //                 fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                //                     <IntlMessages id="form.update" />
                                //                 </button> :
                                //                 <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1",
                                //                 fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                //                     <IntlMessages id="form.update" />
                                //                 </button>
                                //             }
                                //         </form>
                                <React.Fragment>
                                    <form onSubmit={handleSubmit(onSubmit2)}>
                                        {/* <div className="form-group col-md-6"> */}
                                        <label> ستقوم بتعديل الحساب رقم {state.clientAccountNumber}</label>
                                        <br />
                                            <label className="middletitle">
                                                {" "}
                                                <IntlMessages id="accountdetails" />
                                            </label>
                                        {/* </div> */}
                                        {/* <div className="form-group col-md-6">
                                            <label className="middletitlealert">
                                                {" "}
                                                <IntlMessages id="alertforaccountClient" />{" "}
                                            </label>
                                        </div> */}
                                        {/* clientAccountNumber: "",
         */}

                                           
                                        <div className="form-row">
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.accountname" />
                                                </label>
                                                <div
                                                    style={{
                                                        position: "relative",
                                                        margin: "0 30px",
                                                        borderStyle: "solid",
                                                        borderWidth:
                                                            "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor:
                                                            "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8",
                                                    }}
                                                >
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="accNameAr"
                                                        defaultValue={state.accNameAr}
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                            minLength: 3,
                                                            pattern: /^[ء-ي_ ]+$/i,
                                                        })}
                                                    />
                                                </div>
                                                <span className="errors">
                                                    {errors.accNameAr &&
                                                        errors.accNameAr
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.accNameAr &&
                                                        errors.accNameAr
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.accNameAr &&
                                                        errors.accNameAr
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.accountnameEng" />
                                                </label>
                                                <div
                                                    style={{
                                                        position: "relative",
                                                        margin: "0 30px",
                                                        borderStyle: "solid",
                                                        borderWidth:
                                                            "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor:
                                                            "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8",
                                                    }}
                                                >
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="accNameEn"
                                                        defaultValue={state.accNameEn}
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                            minLength: 3,
                                                            pattern: /^[A-Za-z_ ]+$/i,
                                                        })}
                                                    />
                                                </div>
                                                <span className="errors">
                                                    {errors.accNameEn &&
                                                        errors.accNameEn
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.accNameEn &&
                                                        errors.accNameEn
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.accNameEn &&
                                                        errors.accNameEn
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.cointype" />
                                                </label>
                                                <div>
                                                <label className="middletitle">{state.currency_ar}</label>
                                                </div>
                                         
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label>
                                                    <IntlMessages id="FORM.DEBT" />
                                                </label>
                                                <div>
                                                   
                                                </div>
                                                <label className="middletitle">{state.audit_type_ar}</label>
                                            </div>

                                            <div className="form-group col-md-6">
                                                <label>
                                                    <IntlMessages id="form.description" />
                                                </label>
                                                <div
                                                    style={{
                                                        position: "relative",
                                                        margin: "0 30px",
                                                        borderStyle: "solid",
                                                        borderWidth:
                                                            "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor:
                                                            "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8",
                                                    }}
                                                >
                                                    <textarea
                                                        className="form-control"
                                                        name="accDesc"
                                                        defaultValue={state.accDesc}
                                                        rows={3}
                                                        cols={5}
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                            minLength: 3,
                                                            pattern: /^[A-Za-zء-ي_ ]+$/i,
                                                        })}
                                                    />
                                                </div>
                                                <span className="errors">
                                                    {errors.accDesc &&
                                                        errors.accDesc.type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.accDesc &&
                                                        errors.accDesc.type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.accDesc &&
                                                        errors.accDesc.type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label>
                                                    <IntlMessages id="form.secretdegree" />
                                                </label>
                                                <div>
                                                    <select
                                                        className="form-control input-text"
                                                        name="securityLevel"
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                        })}
                                                    >
                                                        <option
                                                            key="0"
                                                            value=""
                                                        >
                                                            برجاء اختيار درجة
                                                            السرية
                                                        </option>
                                                        {state.securityLevels.map(
                                                            (level) => {
                                                                return (
                                                                    <option
                                                                        key={
                                                                            level.security_level_id
                                                                        }
                                                                        value={
                                                                            level.security_level_id
                                                                        }
                                                                    >
                                                                        {
                                                                            level.security_level_type_ar
                                                                        }
                                                                    </option>
                                                                );
                                                            }
                                                        )}
                                                    </select>
                                                </div>
                                                <span className="errors">
                                                    {errors.securityLevel &&
                                                        errors.securityLevel
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredOptionError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-6">
                                                <label>
                                                    <IntlMessages id="widgets.status" />
                                                </label>
                                                <div>
                                                    <select
                                                        className="form-control input-text"
                                                        name="status"
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                        })}
                                                    >
                                                        <option
                                                            key="0"
                                                            value=""
                                                        >
                                                            برجاء اختيار الحالة
                                                            
                                                        </option>
                                                        {state.allStatus.map(
                                                            (status) => {
                                                                return (
                                                                    <option
                                                                        key={
                                                                            status.status_id
                                                                        }
                                                                        value={
                                                                            status.status_id
                                                                        }
                                                                    >
                                                                        {
                                                                            status.status_ar
                                                                        }
                                                                    </option>
                                                                );
                                                            }
                                                        )}
                                                    </select>
                                                </div>
                                                <span className="errors">
                                                    {errors.status &&
                                                        errors.status
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredOptionError" />
                                                        )}
                                                </span>
                                            </div>
                                        </div>
                                        <label className="middletitle">
                                            بيانات العميل
                                        </label>
                        
                                        <div className="form-row">
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.firstName" />
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="clientFirstName"
                                                    defaultValue={state.clientFirstName}
                                                    onChange={handleFields}
                                                    ref={register({
                                                        required: true,
                                                        minLength: 3,
                                                        pattern: /^[A-Za-zء-ي_ ]+$/i,
                                                    })}
                                                />
                                                <span className="errors">
                                                    {errors.clientFirstName &&
                                                        errors.clientFirstName
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.clientFirstName &&
                                                        errors.clientFirstName
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.clientFirstName &&
                                                        errors.clientFirstName
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.middleName" />
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="clientMiddleName"
                                                    defaultValue={state.clientMiddleName}
                                                    onChange={handleFields}
                                                    ref={register({
                                                        required: true,
                                                        minLength: 3,
                                                        pattern: /^[A-Za-zء-ي_ ]+$/i,
                                                    })}
                                                />
                                                <span className="errors">
                                                    {errors.clientMiddleName &&
                                                        errors.clientMiddleName
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.clientMiddleName &&
                                                        errors.clientMiddleName
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.clientMiddleName &&
                                                        errors.clientMiddleName
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.lastName" />
                                                </label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="clientLastName"
                                                    defaultValue={state.clientLastName}
                                                    onChange={handleFields}
                                                    ref={register({
                                                        required: true,
                                                        minLength: 3,
                                                        pattern: /^[A-Za-zء-ي_ ]+$/i,
                                                    })}
                                                />
                                                <span className="errors">
                                                    {errors.clientLastName &&
                                                        errors.clientLastName
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.clientLastName &&
                                                        errors.clientLastName
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minLengthError" />
                                                        )}
                                                    {errors.clientLastName &&
                                                        errors.clientLastName
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.lettersOnlyError" />
                                                        )}
                                                </span>
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="form-group col-md-4">
                                                <label>
                                                    <IntlMessages id="form.phoneNumber" />
                                                </label>
                                                <div
                                                    style={{
                                                        position: "relative",
                                                        margin: "0 30px",
                                                        borderStyle: "solid",
                                                        borderWidth:
                                                            "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor:
                                                            "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8",
                                                    }}
                                                >
                                                    <input
                                                        type="tel"
                                                        className="form-control"
                                                        name="clientPhoneNumber"
                                                        defaultValue={state.clientPhoneNumber}
                                                        onChange={handleFields}
                                                        ref={register({
                                                            required: true,
                                                            pattern: /^[0-9]+$/,
                                                            minLength: 11,
                                                            maxLength: 11,
                                                        })}
                                                    />
                                                </div>
                                                <span className="errors">
                                                    {errors.clientPhoneNumber &&
                                                        errors.clientPhoneNumber
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredError" />
                                                        )}
                                                    {errors.clientPhoneNumber &&
                                                        errors.clientPhoneNumber
                                                            .type ===
                                                            "pattern" && (
                                                            <IntlMessages id="form.numbersOnlyErrorError" />
                                                        )}
                                                    {errors.clientPhoneNumber &&
                                                        errors.clientPhoneNumber
                                                            .type ===
                                                            "minLength" && (
                                                            <IntlMessages id="form.minPhoneLengthError" />
                                                        )}
                                                    {errors.clientPhoneNumber &&
                                                        errors.clientPhoneNumber
                                                            .type ===
                                                            "maxLength" && (
                                                            <IntlMessages id="form.minPhoneLengthError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group  dropdown col-md-4">
                                                <label>
                                                    <IntlMessages id="form.region" />
                                                </label>
                                                <select
                                                    name="clientRegionID"
                                                    className="form-control input-text"
                                                    onChange={handleAreasByRegionID}
                                                    ref={register({
                                                        required: true,
                                                    })}
                                                >
                                                    <option key="0" value="">
                                                        برجاء اختيار المحافظة
                                                    </option>
                                                    {state.regions.map(
                                                        (region) => {
                                                            return (
                                                                <option
                                                                    key={
                                                                        region.region_id
                                                                    }
                                                                    value={
                                                                        region.region_id
                                                                    }
                                                                >
                                                                    {
                                                                        region.region_arabic_name
                                                                    }
                                                                </option>
                                                            );
                                                        }
                                                    )}
                                                </select>
                                                <span className="errors">
                                                    {errors.clientRegionID &&
                                                        errors.clientRegionID
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredOptionError" />
                                                        )}
                                                </span>
                                            </div>
                                            <div className="form-group  dropdown col-md-4">
                                                <label>
                                                    <IntlMessages id="form.area" />
                                                </label>
                                                <select
                                                    name="clientAreaID"
                                                    className="form-control input-text"
                                                    onChange={handleFields}
                                                    ref={register({
                                                        required: true,
                                                    })}
                                                >
                                                    <option key="0" value="">
                                                        برجاء اختيار المنطقة
                                                    </option>
                                                    {state.areas.map((area) => {
                                                        return (
                                                            <option
                                                                key={
                                                                    area.area_id
                                                                }
                                                                value={
                                                                    area.area_id
                                                                }
                                                            >
                                                                {
                                                                    area.area_arabic_name
                                                                }
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                                <span className="errors">
                                                    {errors.clientAreaID &&
                                                        errors.clientAreaID
                                                            .type ===
                                                            "required" && (
                                                            <IntlMessages id="form.requiredOptionError" />
                                                        )}
                                                </span>
                                            </div>
                                        </div>

                                        {/* <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.jobtitle" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>

                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.salary" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields}
                                         />
                                         
                                     </div>
                                     </div> */}

                                        {state.disabled === false ? (
                                            <button
                                                type="submit"
                                                className="btn btn-warning"
                                            >
                                                <IntlMessages id="form.update" />
                                            </button>
                                        ) : (
                                            <button
                                                type="submit"
                                                className="btn btn-warning"
                                                disabled={true}
                                               
                                            >
                                                <IntlMessages id="form.update" />
                                            </button>
                                        )}
                                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
                                            <IntlMessages id="form.clear" />
                                        </button>
                                    </form>
                                </React.Fragment>
                            )}
                        </RctCollapsibleCard>
                    </div>
                    <div className="row mb-5">
                        <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                            <RctCard>
                                <RctCardContent noPadding>
                                    <MuiThemeProvider theme={getMuiTheme()}>
                                        <MUIDataTable
                                            // title={<IntlMessages id="sidebar.cart" />}
                                            data={state.clients}
                                            columns={[
                                                {
                                                    label: "id",
                                                    name: "client_id ",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    },
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.accountnumber" />
                                                    ),
                                                    name:
                                                        "client_account_number",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.firstName" />
                                                    ),
                                                    name: "client_first_name",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.middleName" />
                                                    ),
                                                    name: "client_middle_name",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.lastName" />
                                                    ),
                                                    name: "client_last_name",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.region" />
                                                    ),
                                                    name: "region_arabic_name",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.area" />
                                                    ),
                                                    name: "area_arabic_name",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.phoneNumber" />
                                                    ),
                                                    name: "client_phone_number",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="widgets.status" />
                                                    ),
                                                    name: "status_ar",
                                                },
                                                // ===========================
                                                {
                                                    label: (
                                                        <IntlMessages id="form.accountname" />
                                                    ),
                                                    name: "acc_name_ar",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.accountnameEng" />
                                                    ),
                                                    name: "acc_name_en",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.description" />
                                                    ),
                                                    name: "acc_desc",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.secretdegree" />
                                                    ),
                                                    name:
                                                        "security_level_type_ar",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.cointype" />
                                                    ),
                                                    name: "currency_ar",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="FORM.DEBT" />
                                                    ),
                                                    name: "audit_type_ar",
                                                },
                                                {
                                                    label: (
                                                        <IntlMessages id="form.update" />
                                                    ),
                                                    name: "",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        print: false,
                                                        customBodyRender: (
                                                            value,
                                                            tableMeta,
                                                            updateValue
                                                        ) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button
                                                                        type="button"
                                                                        className="btn btn-warning"
                                                                        onClick={() => {
                                                                            // axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,USER_TOKEN).then((res) => {
                                                                            setState(
                                                                                {
                                                                                    ...state,
                                                                                    clientAccountNumber:
                                                                                        tableMeta
                                                                                            .rowData[1],
                                                                                    clientFirstName:
                                                                                        tableMeta
                                                                                            .rowData[2],
                                                                                    clientMiddleName:
                                                                                        tableMeta
                                                                                            .rowData[3],
                                                                                    clientLastName:
                                                                                        tableMeta
                                                                                            .rowData[4],
                                                                                    clientPhoneNumber:
                                                                                        tableMeta
                                                                                            .rowData[7],
                                                                                    status:
                                                                                        tableMeta
                                                                                            .rowData[8],
                                                                                    accNameAr:
                                                                                        tableMeta
                                                                                            .rowData[9],
                                                                                    accNameEn:
                                                                                        tableMeta
                                                                                            .rowData[10],
                                                                                    accDesc:
                                                                                        tableMeta
                                                                                            .rowData[11],
                                                                                    currency_ar:
                                                                                        tableMeta
                                                                                            .rowData[13],
                                                                                    audit_type_ar:
                                                                                        tableMeta
                                                                                            .rowData[14],
                                                                                    currencyID:
                                                                                        tableMeta
                                                                                            .rowData[17],
                                                                                    accType:
                                                                                        tableMeta
                                                                                            .rowData[18],
                                                                                    // clientRegionID:
                                                                                    //     tableMeta
                                                                                    //         .rowData[2],
                                                                                    // clientAreaID:
                                                                                    //     tableMeta
                                                                                    //         .rowData[2],
                                                                                    // securityLevel:
                                                                                    //     tableMeta
                                                                                    //         .rowData[2],
                                                                                    // accType:
                                                                                    //     tableMeta
                                                                                    //         .rowData[2],

                                                                                    hiddenStatusForm: false,
                                                                                }
                                                                            );
                                                                            // }).catch((error) => {
                                                                            //     if (error.response.status === 429) {
                                                                            //         toast.error(error.response.data, {
                                                                            //         position: "top-center",
                                                                            //         autoClose: 4000,
                                                                            //         hideProgressBar: false,
                                                                            //         closeOnClick: true,
                                                                            //         pauseOnHover: true,
                                                                            //         draggable: true
                                                                            //         });
                                                                            //     }
                                                                            // })
                                                                        }}
                                                                    >
                                                                        <IntlMessages id="form.update" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        },
                                                    },
                                                },

                                                {
                                                    label: (
                                                        <IntlMessages id="button.delete" />
                                                    ),
                                                    name: "",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        print: false,
                                                        customBodyRender: (
                                                            value,
                                                            tableMeta,
                                                            updateValue
                                                        ) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button
                                                                        type="button"
                                                                        className="btn btn-danger"
                                                                        onClick={() => {
                                                                            axios
                                                                                .get(
                                                                                    `http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,
                                                                                    USER_TOKEN
                                                                                )
                                                                                .then(
                                                                                    (
                                                                                        res
                                                                                    ) => {
                                                                                        setState(
                                                                                            {
                                                                                                ...state,
                                                                                                user_id:
                                                                                                    res
                                                                                                        .data
                                                                                                        .userresult[0]
                                                                                                        .user_id,
                                                                                                user_type_id:
                                                                                                    res
                                                                                                        .data
                                                                                                        .userresult[0]
                                                                                                        .user_type_id,
                                                                                                hiddenStatusForm: false,
                                                                                                dealerFirstName:
                                                                                                    tableMeta
                                                                                                        .rowData[1],
                                                                                                dealerMiddleName:
                                                                                                    tableMeta
                                                                                                        .rowData[2],
                                                                                                dealerLastName:
                                                                                                    tableMeta
                                                                                                        .rowData[3],
                                                                                                status:
                                                                                                    tableMeta
                                                                                                        .rowData[8],
                                                                                            }
                                                                                        );
                                                                                    }
                                                                                )
                                                                                .catch(
                                                                                    (
                                                                                        error
                                                                                    ) => {
                                                                                        if (
                                                                                            error
                                                                                                .response
                                                                                                .status ===
                                                                                            429
                                                                                        ) {
                                                                                            toast.error(
                                                                                                error
                                                                                                    .response
                                                                                                    .data,
                                                                                                {
                                                                                                    position:
                                                                                                        "top-center",
                                                                                                    autoClose: 4000,
                                                                                                    hideProgressBar: false,
                                                                                                    closeOnClick: true,
                                                                                                    pauseOnHover: true,
                                                                                                    draggable: true,
                                                                                                }
                                                                                            );
                                                                                        }
                                                                                    }
                                                                                );
                                                                        }}
                                                                    >
                                                                        <IntlMessages id="button.delete" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        },
                                                    },
                                                },

                                                {
                                                    label: "id",
                                                    name:  "currency_id",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    }
                                                },
                                              
                                                {
                                                    label: "id",
                                                    name:  "acc_type",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    }
                                                },
                                            ]}
                                            options={options}
                                        />
                                    </MuiThemeProvider>
                                </RctCardContent>
                            </RctCard>
                        </RctCollapsibleCard>
                    </div>
                </div>
            ) : (
                history.push("/access-denied")
            )}
        </React.Fragment>
    );
    //    }
}
