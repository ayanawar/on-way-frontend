/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
     // state = {
     // 	suppliers: [],
     // };
    
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         suppliers: [],
         disabled: false,
         hiddenStatusForm: true,
     });
     useEffect(() => {
         // all suppliers
         axios.get('http://localhost:8000/allsuppliers',USER_TOKEN).then(response => {
             if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', {path:'/'})
                 window.location.href = "/signin";	
             }
             else{
                 setState({
                     suppliers: response.data.suppliers,
                     disabled: false,
                     hiddenStatusForm: true,
                 })
             }
         })
         .catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);
 
     const onClearStatusClicked = () => {
         setState({...state,
             user_type_id: '',
             hiddenStatusForm: true,
             hiddenDebitLimitForm: true,
             debit_limit: '',
             debitLimitTable: '',
             user_phonenumber: '',
             representativeId: '',
             dealerId: '',
             representativeId: '',
             user_id: '',
             dealerFirstName: '',
             dealerMiddleName: '', 
             dealerLastName: '',
             repFirstName: '',
             repMiddleName: '',
             repLastName: '',
             status: ''
         })
     };
 
     const onSubmit2 = e => {
         axios({
             method: 'post', url: 'http://localhost:8000/admin/update', data: state,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
             toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
             setState({ ...state, disabled: true })
             setTimeout(function () { location.reload()}, 3000)
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
    
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })
 
 //    render() {
       const { match } = props;
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>كشف حساب العملاء</title>
                 <meta name="description" content="كشف حساب العملاء" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.clientaccountstatment" />} match={match} />

             <div className="row">
                <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={"كشف حساب العملاء"}>
                     {/*  */}
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                         <div className="form-row">
                         <div className="form-group col-md-4">
                             <label>
                                 <IntlMessages id="searchbyaccnum" />
                             </label>
                             </div>
                             <div className="form-group col-md-4">
                             <input type="text" className="form-control" name="user_first_name"
                                             onChange={handleFields}
                             />
                            
                        
                            </div>
                         
                         {(state.disabled === false) ? 
                                     <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="widgets.search" />
                                     </button> : 
                                     <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="widgets.search" />
                                     </button>
                                 }
 
 </div>
                     </form>



                     {(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
                     : 
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                     
                                 <div className="form-row">
                                     <div className="form-group col-md-4" >
                                         <label><IntlMessages id="form.firstName" /></label>
                                         <input type="text" className="form-control" name="user_first_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.user_first_name && errors.user_first_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_first_name && errors.user_first_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.user_first_name && errors.user_first_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.middleName" /></label>
                                         <input type="text" className="form-control" name="user_middle_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.user_middle_name && errors.user_middle_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_middle_name && errors.user_middle_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.user_middle_name && errors.user_middle_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.lastName" /></label>
                                         <input type="text" className="form-control" name="user_last_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.user_last_name && errors.user_last_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_last_name && errors.user_last_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.user_last_name && errors.user_last_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                 </div>
                            
                                 <div className="form-row">
                                     <div className="form-group  dropdown col-md-6">
                                         <label><IntlMessages id="form.region" /></label>
                                         <select name="user_region_id" className="form-control input-text"
                                             onChange={handleTwoEvents} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المحافظة</option>
                                             {state.regions.map(region => {
                                                 return (
                                                     <option key={region.region_id} value={region.region_id}>
                                                         {region.region_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         <span className="errors">
                                             {errors.user_region_id && errors.user_region_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                     <div className="form-group  dropdown col-md-6">
                                         <label><IntlMessages id="form.area" /></label>
                                         <select name="user_area_id" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المنطقة</option>
                                             {state.areas.map(area => {
                                                 return (
                                                     <option key={area.area_id} value={area.area_id}>
                                                         {area.area_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         <span className="errors">
                                             {errors.user_area_id && errors.user_area_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                 </div>
 
                                 <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.jobtitle" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>

                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.salary" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields}
                                         />
                                         
                                     </div>
                                     </div>
                                 
                                 {(state.disabled === false) ? 
                                     <button type="submit" className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className=" btn-margin" disabled={true} style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
                             </form>
                 
                       }
                </RctCollapsibleCard>
             </div>
             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                     data={state.suppliers}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "supplier_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                 label: <IntlMessages id="form.firstName" />,
                                                 name:  "supplier_first_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.middleName" />,
                                                 name:  "supplier_middle_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.lastName" />,
                                                 name:  "supplier_last_name",
                                             },
                                          
                                             {
                                                 label: <IntlMessages id="form.accountnumber" />,
                                                 name:  "region_arabic_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.region" />,
                                                 name:  "area_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.area" />,
                                                 name:  "company_name_ar"
                                             },
                                             {
                                                label: <IntlMessages id="form.Creditor" />,
                                                name: "userName"
                                            },
                                            {
                                                label: <IntlMessages id="form.Debitor" />,
                                                name: "user_phonenumber"

                                            },
                                       
                                       
                                            
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }