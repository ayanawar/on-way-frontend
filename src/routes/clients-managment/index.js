/**
 * Advance UI Components Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async routes
 import {
    Asyncallclients,
	Asyncaddclients,
    Asyncaccountstatementclients,
    Asyncreciepttypes,
    Asyncreciepts,
    Asyncclientsaccount
     // AsyncAdvanceUIStepperComponent,
     // AsyncAdvanceUINotificationComponent,
     // AsyncAdvanceUISweetAlertComponent,
     // AsyncAdvanceUIAutoCompleteComponent
 } from '../../components/AsyncComponent/AsyncComponent';
 
 const AdvanceUIComponents = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>إدارة المندوبين</title>
             <meta name="description" content="إدارة المندوبين" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/add-client`} />
            
             <Route path={`${match.url}/add-client`} component={Asyncaddclients} />
             <Route path={`${match.url}/all-clients`} component={Asyncallclients} /> 
             <Route path={`${match.url}/clientaccountstatment`} component={Asyncaccountstatementclients} /> 
             <Route path={`${match.url}/reciepttypes`} component={Asyncreciepttypes} /> 
             <Route path={`${match.url}/clientsaccount`} component={Asyncclientsaccount} /> 
             <Route path={`${match.url}/sales-invoices`} component={Asyncreciepts} /> 
             {/* <Route path={`${match.url}/stepper`} component={AsyncAdvanceUIStepperComponent} /> */}
             {/* <Route path={`${match.url}/notification`} component={AsyncAdvanceUINotificationComponent} /> */}
             {/* <Route path={`${match.url}/representatives-transactions`} component={AsyncAdvanceUISweetAlertComponent} /> */}
             {/* <Route path={`${match.url}/auto-complete`} component={AsyncAdvanceUIAutoCompleteComponent} /> */}
         </Switch>
     </div>
 );
 
 export default AdvanceUIComponents;
 