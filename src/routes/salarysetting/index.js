/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    AsyncsalaryComponent,

 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>إعدادات النظام</title>
             <meta name="description" content="إعدادات النظام" />
         </Helmet>
         <Switch>
         <Redirect exact from={`${match.url}/`} to={`${match.url}/extra-transfer-fees`} />
             <Route path={`${match.url}/salarysettingemployees`} component={AsyncsalaryComponent} />
   
         </Switch>
     </div>
 );
 
 export default Pages;
 