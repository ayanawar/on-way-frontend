import moment from 'moment';
import React, { Component } from 'react';
import IntlMessages from 'Util/IntlMessages';
import logo from '../../../assets/img/icon10.jpg';

const nf = new Intl.NumberFormat();

class PrintReceivableReceipt extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { classes } = this.props;
        return (
            <div className="container">    
              <div className="row"  >
                <div className="col-md-6 ">
                    <img width={400} height={400} src={logo} className="img-fluid" style={{
                         marginLeft: "-50px",
                        // marginRight: "auto"
                    }} />
                </div>
                </div>
                <br />
                        <br />
                <div className="row"  >
                    <div className="col-md-12 text-center">
                
                        <h1 className="text-danger" style={styles.headerFont}>سند قبض</h1>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div style={styles.receiptIdStyle1}>
                        <br />
                        <br />
                        <h1 style={styles.font} className="text-right mr-3" > رقم سند القبض: <span className="ml-2 text-danger">{this.props.recieptid}</span></h1>
                       <br/>
                       <br />
                        <h2  style={styles.font} className="text-right mr-3" ><span className="ml-2">{moment().format('YYYY-MM-DD hh:mm:ss A')}</span> :التاريخ</h2>  
                     
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}> أستلمنا من السادة: <span className="ml-2">شركة الامتداد</span></h2>
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}> لقد استلمات من: <span className="ml-2">{localStorage.getItem("userFirstName")+" "+localStorage.getItem("usermiddleName")+" "+localStorage.getItem("userLastName")}</span></h2>
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}> <span className="text-danger ml-2">IQD </span> مبلغ و قدرة: <span className="text-danger ml-2">{this.props.total}  </span>  </h2>
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}> :وذلك عن <span className="ml-2"></span></h2>
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}> :طريقة الدفع <span className="ml-2"></span></h2>
                        <br />
                        <br />
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div className="row text-center">
                        <div  >
                        <h2 style={styles.font} className="text-right mr-3"> اسم المستلم: <span className="ml-2">{this.props.fname} {this.props.Mname} {this.props.Lname}</span></h2>
                        </div>
                       </div>
                       <br />
                        <br />
                        <div className="row text-center">
                        <div  >
                        <h1 style={styles.font} className="text-right mr-3" >توقيع المستلم</h1>
                       </div>
                       </div>
                       <br />
                       <div className="row text-center">
                       <div  >
                        <h1 style={styles.font} className="text-right mr-3" >----------------------</h1>
                       </div>
                       </div>
                        <div  >
                        {/* <h1 style={styles.font} className="text-right mr-3" > رقم سند القبض: <span className="ml-2">{this.props.recieptid}</span></h1> */}
                      </div>
                       
                    </div>
                </div>
            </div>
        );

    }
}

const styles = {
    headerFont: {
        fontSize: "92px",
    },
    headerFont1: {
        fontSize: "33px",
    },
    headerFont2: {
        padding: "20px",
        fontSize: "33px",
        marginLeft: "3px",
    },
    mainFont: {
        fontSize: "28px",
    },
    font: {
        fontSize: "59px",
        marginRight: "100px !important"
    },
    fontStyle: {
        fontSize: "x-large"
    },
    receiptIdStyle: {
        color: "darkgreen",
        fontSize: "30px",
        border: "2px solid darkgreen",
        //marginRight: "300px",
        marginLeft: "300px",
        padding: "10px",
        width:"600px"
    },
    receiptIdStyle1: {
      //  fontSize: "30px",
        border: "2px solid black",
        width:"1500px",
       // display:'flex',
        
        //marginRight: "300px",
      
      //  padding: "10px",
      //  width:"600px"
    },
    receiptIdStyle2: {
        //  fontSize: "30px",
          border: "2px solid black",
          //width:"500px",
         // display:'flex',
          
          //marginRight: "300px",
        // marginLeft: "600px",
        //  padding: "10px",
         width:"200px"
      },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default PrintReceivableReceipt;
