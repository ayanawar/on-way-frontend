/**
 * View Cart Page
 */
import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import PrintReceivableReceipt from './print-receivable-receipt';
import { useReactToPrint } from 'react-to-print';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const cookies = new Cookies();

const options = {
    rowIndex: 0,
    filter: true,
    filterType: 'dropdown',
    rowsPerPage: 10,
    rowsPerPageOptions: [5, 10, 25, 50, 100],
    responsive: 'vertical',
    enableNestedDataAccess: '.',
    selectableRows: "none",
    viewColumns: false,
    sort: false,
    fixedHeader: true,
    download: false,
    fixedSelectColumn: false,
    tableBodyHeight: "300px"
};

export default function Shop(props) {
    // state = {
    // 	suppliers: [],
    // };

    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();

    const componentRef = useRef();

    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
        onAfterPrint: () => {
            // setState({
            //     modal: false,
            //     modalWithMoneyInputField: false,
            //     modalForConfirmation: false, 
            // })
            location.reload()
        },
    });
    const [state, setState] = useState({
        suppliers: [],
        disabled: false,
        hiddenStatusForm: true,
        selected_aCCOUNT: [],
        fname: '',
        Mname: '',
        Lname: '',
        hiddenaccountdropdown: true,
        accountidlevels: '',
        recieptid: '',
        total: '',
        level1: 0,
        level2: 0,
        level3: 0,
        level4: 0,
        salary1: '',
        salary2: '',
        salary3: '',
        salary4: '',
        tableid: 1,
        employees: [],
        accountId: '',
        level1name: '',
        level2name: '',
        level3name: '',
        level4name: '',
        accountNameAr: '',
        accountNameEn: '',
        acc_id: '',
        accountDescription: '',
        securityLevel: '',
        accountTypeId: '',
        audit_type_id: '',
        accountStatusId: 1,
        accountCurrencyId: '',
        accountCurrencyIdupdate: '',
        accountTypeIdupdate: '',
        accountCurrencyIdupdateName: '',
        accountTypeIdupdateName: '',
        accountid2: 0,
        hiddenlvl2: true,
        hiddenlvl3: true,
        hiddenlvl4: true,
        rows: [],
        levcels2: [],
        levcels3: [],
        levcels4: [],
        types: [],
        levels: [],
        currency: [],
        accounts: [],
        MainAccounts: [],
        hiddentextbox: true,
        hiddenaddtotablebtn: true,
        hiddentable: true,
        tablearray: [],
        enteredvalue: 0,
        exchangerate: '',

    });
    const nf = new Intl.NumberFormat();
    useEffect(() => {
        console.log("new Date()", new Date());
        let data1 = {
            accountNumber: 3300,

        }
        let data2 = {
            accountNumber: 3400,

        }
        let data3 = {
            accountNumber: 3500,

        }
        let data4 = {
            accountNumber: 3600,

        }



        axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
                axios.get('https://accbackend.alaimtidad-itland.com/currency', USER_TOKEN).then(response12 => {
                    axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
                        axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                            axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
                                axios({
                                    method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data1,
                                    headers: { "x-access-token": `${cookies.get('UserToken')}` }
                                }).then(res1 => {
                                    axios({
                                        method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data2,
                                        headers: { "x-access-token": `${cookies.get('UserToken')}` }
                                    }).then(res2 => {
                                        axios({
                                            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data3,
                                            headers: { "x-access-token": `${cookies.get('UserToken')}` }
                                        }).then(res3 => {
                                            axios({
                                                method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data4,
                                                headers: { "x-access-token": `${cookies.get('UserToken')}` }
                                            }).then(res4 => {
                                                if (response12.data == "Token Expired" || response12.data == "Token UnAuthorized" || response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized" || response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response11.data == "Token Expired" || response11.data == "Token UnAuthorized" || res1.data.message.length === 0 || res2.data.message.length === 0 || res3.data.message.length === 0 || res4.data.message.length === 0) {
                                                    cookies.remove('UserToken', { path: '/' })
                                                    window.location.href = "/signin";
                                                }
                                                else {
                                                    if (response12.data.message.length == 0) {

                                                        toast.error("عفوا لا يوجد سعر صرف فى النظام من فضلك قم باضافة سعر الصرف لتتمكن من المتابعة", {
                                                            position: "top-center",
                                                            autoClose: 4000,
                                                            hideProgressBar: false,
                                                            closeOnClick: true,
                                                            pauseOnHover: true,
                                                            draggable: true
                                                        });
                                                    }
                                                    else {
                                                        console.log(response5.data);
                                                        setState({
                                                            ...state,
                                                            levels: response.data.levels,
                                                            types: response3.data.message,
                                                            currency: response4.data.message,
                                                            accounts: response5.data.accounts,
                                                            exchangerate: response12.data.message[0].dollar_value,
                                                            MainAccounts: response11.data.mainAccounts,
                                                            salary1: res1.data.message[0].acc_name_ar,
                                                            salary2: res2.data.message[0].acc_name_ar,
                                                            salary3: res3.data.message[0].acc_name_ar,
                                                            salary4: res4.data.message[0].acc_name_ar,
                                                            disabled: false,

                                                        })
                                                    }
                                                    // getsalary1();
                                                    // getsalary2();
                                                    // getsalary3();
                                                    // getsalary4();
                                                }
                                            }).catch(error => {
                                                if (error.response.status === 429) {
                                                    toast.error(error.response.data, {
                                                        position: "top-center",
                                                        autoClose: 4000,
                                                        hideProgressBar: false,
                                                        closeOnClick: true,
                                                        pauseOnHover: true,
                                                        draggable: true
                                                    });
                                                }
                                            });
                                        }).catch(error => {
                                            if (error.response.status === 429) {
                                                toast.error(error.response.data, {
                                                    position: "top-center",
                                                    autoClose: 4000,
                                                    hideProgressBar: false,
                                                    closeOnClick: true,
                                                    pauseOnHover: true,
                                                    draggable: true
                                                });
                                            }
                                        });
                                    }).catch(error => {
                                        if (error.response.status === 429) {
                                            toast.error(error.response.data, {
                                                position: "top-center",
                                                autoClose: 4000,
                                                hideProgressBar: false,
                                                closeOnClick: true,
                                                pauseOnHover: true,
                                                draggable: true
                                            });
                                        }
                                    });
                                }).catch(error => {
                                    if (error.response.status === 429) {
                                        toast.error(error.response.data, {
                                            position: "top-center",
                                            autoClose: 4000,
                                            hideProgressBar: false,
                                            closeOnClick: true,
                                            pauseOnHover: true,
                                            draggable: true
                                        });
                                    }
                                });
                            }).catch(error => {
                                if (error.response.status === 429) {
                                    toast.error(error.response.data, {
                                        position: "top-center",
                                        autoClose: 4000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                }
                            });
                        }).catch(error => {
                            if (error.response.status === 429) {
                                toast.error(error.response.data, {
                                    position: "top-center",
                                    autoClose: 4000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true
                                });
                            }
                        });
                    }).catch(error => {
                        if (error.response.status === 429) {
                            toast.error(error.response.data, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                            });
                        }
                    });
                }).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });



    }, []);



    const onClearStatusClicked = () => {
        setState({
            ...state,
            user_type_id: '',
            hiddenStatusForm: true,
            hiddenDebitLimitForm: true,
            debit_limit: '',
            debitLimitTable: '',
            user_phonenumber: '',
            representativeId: '',
            dealerId: '',
            representativeId: '',
            user_id: '',
            dealerFirstName: '',
            dealerMiddleName: '',
            dealerLastName: '',
            repFirstName: '',
            repMiddleName: '',
            repLastName: '',
            status: ''
        })
    };


    const handleTwoEvents2 = ({ target }) => {
        var e = document.getElementById('level2')
        // var value=e.options[e.selectedIndex].value;// get selected option value
        var text = e.options[e.selectedIndex].text;
        //console.log("text",text);
        console.log("event", text);
        setState({
            ...state, [target.name]: target.value,
            level2name: text
        });

        for (let i = 0; i <= state.accounts.length; i++) {
            if (parseInt(target.value) == parseInt(state.accounts[i].acc_number)) {
                console.log("entered handle");
                let account;

                console.log("account", state.accounts[i]);
                account = state.accounts[i];
                setState({
                    ...state, [target.name]: target.value,
                    accountCurrencyIdupdate: state.accounts[i].currency_id,
                    accountTypeIdupdate: state.accounts[i].audit_type_id,
                    accountCurrencyIdupdateName: state.accounts[i].currency_ar,
                    accountTypeIdupdateName: state.accounts[i].audit_type_ar,
                    accountidlevels: account.acc_id,
                    //  level2name:target.name,

                });
                console.log("account id", account.acc_id);

                let parentid = target.value;
                axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
                    if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                        cookies.remove('UserToken', { path: '/' })
                        window.location.href = "/signin";
                    }
                    else {

                        if (response.data.accounts.length == 0) {

                            setState({
                                ...state,
                                level2: parentid,
                                level3: 0,
                                levcels3: [],
                                levcels4: [],
                                accountidlevels: account.acc_id,
                            })

                        }

                        else if (response.data.accounts.length > 0) {

                            setState({
                                ...state,
                                level2: parseInt(parentid),
                                accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                accountidlevels: account.acc_id,
                            })

                            if (parseInt(target.value) != parseInt(parentid)) {

                                setState({
                                    ...state,
                                    level2: parentid,
                                    levcels3: [],
                                    levcels4: [],
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,
                                    hiddenlvl3: true,
                                })

                            }

                            else if (parseInt(target.value) == parseInt(parentid)) {

                                setState({
                                    ...state,
                                    level2: parentid,
                                    levcels3: response.data.accounts,
                                    levcels4: [],
                                    //   level3name:response.data.accounts[0].acc_name_ar,
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,
                                    hiddenlvl3: false,
                                })
                            }

                        }
                        else {
                            console.log("Errorrrrrrrr");
                        }
                    }
                }).catch(error => {
                    console.log("catch", error);
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
                break;
            }
            else {
                //    console.log("n0oooo");
            }

        }
    }

    const handleTwoEvents = ({ target }) => {

        console.log("target", target);
        var e = document.getElementById('level1')
        // var value=e.options[e.selectedIndex].value;// get selected option value
        var text = e.options[e.selectedIndex].text;
        //console.log("text",text);
        console.log("event", e.options[e.selectedIndex].text);
        setState({
            ...state, [target.name]: target.value,
            level1name: e.options[e.selectedIndex].text
        });
        for (let i = 0; i <= state.MainAccounts.length; i++) {
            console.log(state.MainAccounts);
            let account;
            if (parseInt(target.value) == parseInt(state.MainAccounts[i].acc_number)) {
                console.log("MainAccounts", state.MainAccounts[i]);
                account = state.MainAccounts[i];
                setState({
                    ...state, [target.name]: target.value,
                    accountCurrencyIdupdate: state.MainAccounts[i].currency_id,
                    accountTypeIdupdate: state.MainAccounts[i].audit_type_id,
                    accountCurrencyIdupdateName: state.MainAccounts[i].currency_ar,
                    accountTypeIdupdateName: state.MainAccounts[i].audit_type_ar,
                    accountidlevels: state.MainAccounts[i].acc_id,
                });
                console.log("accountttttttttttt", account);
                let parentid = target.value;
                axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
                    if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                        cookies.remove('UserToken', { path: '/' })
                        window.location.href = "/signin";
                    }
                    else {

                        if (response.data.accounts.length == 0) {


                            setState({
                                ...state,
                                level1: parentid,
                                levcels2: [],
                                levcels3: [],
                                levcels4: [],
                                level2: 0,
                                level3: 0,
                                level4: 0,
                                hiddenlvl2: true,
                                accountCurrencyIdupdate: state.MainAccounts[i].currency_id,
                                accountTypeIdupdate: state.MainAccounts[i].audit_type_id,
                                accountCurrencyIdupdateName: state.MainAccounts[i].currency_ar,
                                accountTypeIdupdateName: state.MainAccounts[i].audit_type_ar,
                                accountidlevels: account.acc_id,
                            })
                        }

                        else if (response.data.accounts.length > 0) {
                            setState({
                                ...state,
                                level1: parseInt(parentid),

                                accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                accountidlevels: account.acc_id,
                            })

                            if (parseInt(target.value) != parseInt(parentid)) {

                                setState({
                                    ...state,
                                    level1: parentid,
                                    levcels2: [],
                                    levcels3: [],
                                    levcels4: [],
                                    level2: 0,
                                    level3: 0,
                                    level4: 0,
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,
                                    hiddenlvl2: false,
                                })

                            }

                            else if (parseInt(target.value) == parseInt(parentid)) {

                                setState({
                                    ...state,
                                    level1: parentid,
                                    levcels2: response.data.accounts,
                                    //     level2name:response.data.accounts[0].acc_name_ar,
                                    levcels3: [],
                                    levcels4: [],
                                    level3: 0,
                                    level4: 0,
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,
                                    hiddenlvl2: false,
                                })
                            }
                        }
                        else {

                            console.log("Errorrrrrrrr");
                        }
                    }
                }).catch(error => {
                    console.log("Catch", error);
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
                // console.log("entered yess");
                break;
            }
            else {
                //    console.log("n0oooo");
            }

        }




    }
    const pushinarray = () => {
        console.log("level2222222222222", state.level2);
        console.log("level111111111111", state.level1);


        var e = document.getElementById('level1')
        var e2 = document.getElementById('level2')
        var e3 = document.getElementById('level3')
        var e4 = document.getElementById('level4')

        var text = e.options[e.selectedIndex].text;




        if (state.level2 == 0) {
            console.log("entered zero level 2");

            let obj = {
                tableid: state.tableid,
                level1: e?.options[e?.selectedIndex].text,
                level2: (e2?.options[e2?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e2?.options[e2?.selectedIndex].text,
                level3: (e3?.options[e3?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e3?.options[e3?.selectedIndex].text,
                level4: (e4?.options[e4?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e4?.options[e4?.selectedIndex].text,
                amount: state.enteredvalue,
                accountTypeIdupdateName: state.accountTypeIdupdateName,
                currencyId: state.accountCurrencyIdupdate,
                type: state.accountTypeIdupdate,
                accountCurrencyIdupdateName: state.accountCurrencyIdupdateName,
                accountId: state.accountidlevels,
            }

            //   obj.id += 1;

            state.tablearray.push(obj)
            setState({
                ...state, accountid2: state.accountidlevels, disabled: false, hiddentextbox: false, hiddentable: false, tablearray: state.tablearray, tableid: (state.tableid + 1)
            })




        }
        else if (state.level3 == 0) {

            let obj = {
                tableid: state.tableid,
                level1: e?.options[e?.selectedIndex].text,
                level2: (e2?.options[e2?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e2?.options[e2?.selectedIndex].text,
                level3: (e3?.options[e3?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e3?.options[e3?.selectedIndex].text,
                level4: (e4?.options[e4?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e4?.options[e4?.selectedIndex].text,
                amount: state.enteredvalue,
                accountTypeIdupdateName: state.accountTypeIdupdateName,
                currencyId: state.accountCurrencyIdupdate,
                type: state.accountTypeIdupdate,
                accountCurrencyIdupdateName: state.accountCurrencyIdupdateName,
                accountId: state.accountidlevels,
            }

            //   obj.id += 1;

            state.tablearray.push(obj)
            setState({
                ...state, accountid2: state.accountidlevels, disabled: false, hiddentextbox: false, hiddentable: false, tablearray: state.tablearray, tableid: (state.tableid + 1)
            })

        }
        else if (state.level4 == 0) {
            let obj = {
                tableid: state.tableid,
                level1: e?.options[e?.selectedIndex].text,
                level2: (e2?.options[e2?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e2?.options[e2?.selectedIndex].text,
                level3: (e3?.options[e3?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e3?.options[e3?.selectedIndex].text,
                level4: (e4?.options[e4?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e4?.options[e4?.selectedIndex].text,
                amount: state.enteredvalue,
                accountTypeIdupdateName: state.accountTypeIdupdateName,
                currencyId: state.accountCurrencyIdupdate,
                type: state.accountTypeIdupdate,
                accountCurrencyIdupdateName: state.accountCurrencyIdupdateName,
                accountId: state.accountidlevels,
            }

            //   obj.id += 1;

            state.tablearray.push(obj)
            setState({
                ...state, accountid2: state.accountidlevels, disabled: false, hiddentextbox: false, hiddentable: false, tablearray: state.tablearray, tableid: (state.tableid + 1)
            })

        }
        else {
            let obj = {
                tableid: state.tableid,
                level1: e?.options[e?.selectedIndex].text,
                level2: (e2?.options[e2?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e2?.options[e2?.selectedIndex].text,
                level3: (e3?.options[e3?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e3?.options[e3?.selectedIndex].text,
                level4: (e4?.options[e4?.selectedIndex].text === 'اختار الحساب الفرعي') ? '' : e4?.options[e4?.selectedIndex].text,
                amount: state.enteredvalue,
                accountTypeIdupdateName: state.accountTypeIdupdateName,
                currencyId: state.accountCurrencyIdupdate,
                type: state.accountTypeIdupdate,
                accountCurrencyIdupdateName: state.accountCurrencyIdupdateName,
                accountId: state.accountidlevels,
            }

            //   obj.id += 1;

            state.tablearray.push(obj)
            setState({
                ...state, accountid2: state.accountidlevels, disabled: false, hiddentextbox: false, hiddentable: false, tablearray: state.tablearray, tableid: (state.tableid + 1)
            })

        }



    }
    const handleTwoEvents3 = ({ target }) => {
        var e = document.getElementById('level3')

        var text = e.options[e.selectedIndex].text;

        console.log("event", text);
        setState({
            ...state, [target.name]: target.value,
            level3name: text
        });
        let account;

        for (let i = 0; i <= state.accounts.length; i++) {
            if (parseInt(target.value) == parseInt(state.accounts[i].acc_number)) {
                console.log("entered handle");
               
                console.log("account", state.accounts[i]);
                account = state.accounts[i];
                setState({
                    ...state, [target.name]: target.value,

                    accountidlevels: account.acc_id,
                    //  level2name:target.name,

                });
                console.log("account id", account.acc_id);

                let parentid = target.value;
                axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
                    if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                        cookies.remove('UserToken', { path: '/' })
                        window.location.href = "/signin";
                    }
                    else {

                        if (response.data.accounts.length == 0) {

                            setState({
                                ...state,

                                level3: parentid,
                                levcels4: [],
                                hiddenlvl4: true,
                                accountidlevels: account.acc_id
                            })

                        }

                        else if (response.data.accounts.length > 0) {

                            setState({
                                ...state,
                                level3: parseInt(parentid),
                                accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                accountidlevels: account.acc_id
                            })

                            if (parseInt(target.value) != parseInt(parentid)) {

                                setState({
                                    ...state,
                                    level3: parentid,
                                    levcels4: [],
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,

                                    hiddenlvl4: true,
                                })

                            }

                            else if (parseInt(target.value) == parseInt(parentid)) {
                                console.log(parseInt(target.value));
                                console.log(parseInt(parentid));
                                setState({
                                    ...state,
                                    level3: parentid,
                                    levcels4: response.data.accounts,
                                    //level4name:response.data.accounts[0].acc_name_ar,
                                    accountCurrencyIdupdate: response.data.accounts[0].currency_id,
                                    accountTypeIdupdate: response.data.accounts[0].audit_type_id,
                                    accountCurrencyIdupdateName: response.data.accounts[0].currency_ar,
                                    accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                                    accountidlevels: account.acc_id,

                                    hiddenlvl4: false,
                                })
                            }

                        }
                        else {
                            console.log("Errorrrrrrrr");
                        }
                    }
                }).catch(error => {

                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
                break;
            }
            else {
                //    console.log("n0oooo");
            }
        }
    }


    const handleTwoEvents4 = ({ target }) => {
        var e = document.getElementById('level4')

        var text = e.options[e.selectedIndex].text;

        console.log("event", text);
        setState({
            ...state, [target.name]: target.value,
            level4name: text
        });
        let account;

        for (let i = 0; i <= state.accounts.length; i++) {
            if (parseInt(target.value) == parseInt(state.accounts[i].acc_number)) {
                console.log("entered handle");
               
                console.log("account", state.accounts[i]);
                account = state.accounts[i];
                setState({
                    ...state, [target.name]: target.value,

                    accountidlevels: account.acc_id,
                    //  level2name:target.name,

                });
                console.log("account id", account.acc_id);

                let parentid = target.value;
                axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
                    if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                        cookies.remove('UserToken', { path: '/' })
                        window.location.href = "/signin";
                    }
                    else {

                        if (response.data.accounts.length == 0) {

                            setState({
                                ...state,

                                level4: parentid,
                                accountidlevels: account.acc_id
                            })

                        }

                       
                        else {
                            console.log("Errorrrrrrrr");
                        }
                    }
                }).catch(error => {

                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
                break;
            }
            else {
                //    console.log("n0oooo");
            }
        }
    }
    const getsalary1 = e => {

        let data1 = {
            accountNumber: 3300,

        }
        let data2 = {
            accountNumber: 3400,

        }
        let data3 = {
            accountNumber: 3500,

        }
        let data4 = {
            accountNumber: 3600,

        }

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data1,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res1 => {
            axios({
                method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data2,
                headers: { "x-access-token": `${cookies.get('UserToken')}` }
            }).then(res2 => {
                axios({
                    method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data3,
                    headers: { "x-access-token": `${cookies.get('UserToken')}` }
                }).then(res3 => {
                    axios({
                        method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data4,
                        headers: { "x-access-token": `${cookies.get('UserToken')}` }
                    }).then(res4 => {
                        if (res1.data == "Token Expired" || res1.data == "Token UnAuthorized" || res2.data == "Token Expired" || res2.data == "Token UnAuthorized" || res3.data == "Token Expired" || res3.data == "Token UnAuthorized" || res4.data == "Token Expired" || res4.data == "Token UnAuthorized") {
                            cookies.remove('UserToken', { path: '/' })
                            window.location.href = "/signin";
                        }
                        else if (res1.data.message.length === 0 || res2.data.message.length === 0 || res3.data.message.length === 0 || res4.data.message.length === 0) {

                            toast.error(<IntlMessages id="components.NoItemFound" />, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                            });
                        }
                        else {
                            console.log("res1", res1.data.message[0]);
                            setState({
                                ...state,
                                salary1: res1.data.message[0].acc_name_ar,
                                salary2: res2.data.message[0].acc_name_ar,
                                salary3: res3.data.message[0].acc_name_ar,
                                salary4: res4.data.message[0].acc_name_ar,
                            })

                        }
                    }).catch(error => {
                        if (error.response.status === 429) {
                            toast.error(error.response.data, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                            });
                        }
                    });
                }).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }


    const onSubmit = e => {

        let data = {
            employeeId: state.employees[0].employee_id,
            entryDate: `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()};${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`,
            exchangeRate: state.exchangerate,
            salaryBreakdown: state.tablearray
        }
        console.log("data", data);

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/addsalary', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res", res.data);
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            else {
                console.log("res", res.data);
                if (res.data.message === "salary with breakdowns added successfully")
                    toast.success("تمت العملية بنجاح", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true, recieptid: res.data.tranId, total: res.data.totalAmount })
              setTimeout(function () { handlePrint() }, 2000)
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }

    const onSubmit2 = e => {

        let data = {
            accountNumber: state.accountId,

        }

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res", res.data);
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            else {
                console.log("res", res.data);
                setState({ ...state, disabled: true, hiddenStatusForm: false, employees: res.data.message, fname: res.data.message[0].emp_first_name, Mname: res.data.message[0].emp_middle_name, Lname: res.data.message[0].emp_last_name })

            }
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }

    const handleFields = ({ target }) => {

        console.log("stateeeee", state);
        console.log("target", target.name);
        setState({ ...state, [target.name]: target.value });
        if (target.name == "enteredvalue") {
            console.log("enteredvalueValue", target.value);

            console.log("hiddentextbox");
            setState({

                ...state, hiddenaddtotablebtn: false, [target.name]: target.value
            })
        }
    };

    const handleFields4 = ({ target }) => {
        console.log("target", target.name);
        var e = document.getElementById('level4')
        var text = e.options[e.selectedIndex].text;
        console.log("event", text);
        setState({
            ...state, [target.name]: target.value,
            level4name: text
        });

    };

    const removeRow = (r) => {

        for (var i = 0; i < state.tablearray.length; i++) {
            console.log(state.tablearray[i].tableid == r);
            console.log(state.tablearray[i].tableid);
            console.log(r);
            if (state.tablearray[i].tableid == r) {
                console.log("state.tablearray[i]", state.tablearray[i]);
                state.tablearray.splice(i, 1);
                break;
            }

        }
        console.log("event", state.tablearray);
        setState({ ...state, tablearray: state.tablearray })
        if (state.tablearray.length == 0) {
            setState({ ...state, hiddentable: true })
        }


    }


    const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                    maxHeight: 'unset',
                    overflowX: 'unset',
                    overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                },
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar: {
                regular: {
                    backgroundColor: "gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor: "gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
    console.log(state);
    //    render() {
    const { match } = props;
    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="cart-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>اعدادات قيد المرتبات</title>
                        <meta name="description" content="اعدادات قيد المرتبات" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.salary" />} match={match} />

                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={"اعدادات قيد المرتبات"}>
                            {/*  */}
                            <form onSubmit={handleSubmit(onSubmit2)}>
                                <div className="form-group col-md-4">
                                    <div className="form-row">
                                        <label><IntlMessages id="table.date" /></label>
                                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {new Date().getHours()}:{new Date().getMinutes()}:{new Date().getSeconds()}&nbsp;&nbsp;&nbsp;{new Date().getDate()}/{new Date().getMonth() + 1}/{new Date().getFullYear()}</span>
                                    </div>
                                </div>
                                <div className="form-row">

                                    <div className="form-group col-md-4">
                                        <label>
                                            <IntlMessages id="searchbyaccnum" />
                                        </label>
                                    </div>
                                    <div className="form-group col-md-4">
                                        <div style={{
                                            position: "relative", margin: "0 30px", borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                        }}>
                                            <input type="number" className="form-control" name="accountId"
                                                onChange={handleFields}
                                                ref={register({ required: true, pattern: /^[0-9]+$/ })}
                                            />
                                        </div>
                                        <span className="errors">
                                            {errors.accountId && errors.accountId.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}
                                            {errors.accountId && errors.accountId.type === 'pattern' &&
                                                <IntlMessages id="form.numbersOnlyErrorError" />}

                                        </span>

                                    </div>

                                    {(state.disabled === false) ?
                                        <button type="submit" className=" btn-margin" style={{
                                            margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                            fontSize: "17px", fontWeight: "bold", height: '50px'
                                        }}>
                                            <IntlMessages id="widgets.search" />
                                        </button> :
                                        <button type="submit" className=" btn-margin" disabled={true} style={{
                                            margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                            fontSize: "17px", fontWeight: "bold", height: '50px'
                                        }}>
                                            <IntlMessages id="widgets.search" />
                                        </button>
                                    }

                                </div>
                            </form>

                        </RctCollapsibleCard>

                    </div>

                    {(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
                        :
                        <div className="row mb-5">
                            <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                                <RctCard>
                                    <RctCardContent noPadding>
                                        <React.Fragment>
                                            <div className="container">

                                            </div>
                                            <br />
                                            <div className="container">
                                                <div className="row">

                                                    <div className="col-md-6">
                                                        <h2><IntlMessages id="form.accountnumber" />: {state.employees[0].acc_number}</h2>
                                                        <h2><IntlMessages id="form.firstName" />: {state.employees[0].emp_first_name} {state.employees[0].emp_middle_name} {state.employees[0].emp_last_name} </h2>
                                                        <h2><IntlMessages id="form.salary" />: {state.employees[0].basic_salary}</h2>
                                                    </div>

                                                    <div className="col-md-6">
                                                        <h2><IntlMessages id="form.jobtitle" />: {state.employees[0].job_title}</h2>
                                                        <h2><IntlMessages id="form.region" />: {state.employees[0].region_arabic_name}</h2>
                                                        <h2><IntlMessages id="form.area" />: {state.employees[0].area_arabic_name}</h2>

                                                    </div>

                                                </div>
                                            </div>
                                            <div className="row">
                                                <button onClick={() => {
                                                    setState({
                                                        ...state, hiddenaccountdropdown: false
                                                    })
                                                }} className=" btn-margin" style={{
                                                    margin: "30px 30px 20px 0", color: '#fff', backgroundColor: "#0063c1",
                                                    fontSize: "17px", fontWeight: "bold", height: '50px'
                                                }}>
                                                    <IntlMessages id="addsaleries" />
                                                </button>

                                                {/* <button  
                                    className=" btn-margin" style={{ margin:"30px 30px 20px 0" ,color:'#fff',backgroundColor: "#0063c1", 
                                     fontSize:"17px", fontWeight: "bold", height:'50px'}}>
                                         <IntlMessages id="form.getreciept" />
                                     </button> */}
                                            </div>
                                        </React.Fragment>
                                    </RctCardContent>
                                </RctCard>
                            </RctCollapsibleCard>
                        </div>
                    }
                    {/* drop down for accounts */}

                    {(state.hiddenaccountdropdown == true) ? <React.Fragment></React.Fragment>
                        :
                        <div className="row mb-5">
                            <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">

                                <RctCardContent >
                                    <React.Fragment>

                                        <div className="row">
                                            <label><IntlMessages id="form.addparentaccount" /></label>
                                        </div>
                                        <div className="form-row">



                                            <div className="form-group col-md-3">


                                                <div >
                                                    <select className="form-control" name="level1" id="level1" onChange={handleTwoEvents} >
                                                        <option key="0" value=""> اختار الحساب الرئيسي</option>
                                                        <option key="3300" value="3300">{state.salary1}</option>
                                                        <option key="3400" value="3400">{state.salary2}</option>
                                                        <option key="3500" value="3500">{state.salary3}</option>
                                                        <option key="3600" value="3600">{state.salary4}</option>
                                                        {/* {state.MainAccounts.map(account => {
                                             return (
                                                 <option key={account.acc_number} value={account.acc_number}>
                                                     {account.acc_name_ar}
                                                 </option>
                                             )
                                         })
                                         } */}
                                                    </select>
                                                </div>
                                                <span className="errors">
                                                    {errors.level1 && errors.level1.type === 'required' &&
                                                        <IntlMessages id="form.requiredOptionError" />}
                                                </span>
                                            </div>
                                            <div className="form-group col-md-3">
                                                {(state.hiddenlvl2 == true) ?

                                                    <span></span>
                                                    :




                                                    <div className="form-row">
                                                        <select className="form-control" name="level2" id="level2"
                                                            onChange={handleTwoEvents2} >
                                                            <option key="0" value="">اختار الحساب الفرعي</option>

                                                            {state.levcels2.map(account => {
                                                                return (
                                                                    <option key={account.acc_number} value={account.acc_number}>
                                                                        {account.acc_name_ar}
                                                                    </option>
                                                                )
                                                            })
                                                            }
                                                        </select>
                                                    </div>




                                                }

                                                <br />
                                                <br />

                                                {(state.level2 == "33001") ?
                                                    <div className="form-row">
                                                        <br />
                                                        <br />
                                                        <h2 ><IntlMessages id="form.salary" />: {state.employees[0].basic_salary}</h2>


                                                    </div>
                                                    :
                                                    <span></span>
                                                }

                                            </div>
                                            {(state.levcels3.length == 0) ?

                                                <span></span>
                                                :


                                                <div className="form-group col-md-3">


                                                    <div >
                                                        <select className="form-control" name="level3" id="level3"
                                                            onChange={handleTwoEvents3} >
                                                            <option key="0" value="">اختار الحساب الفرعي</option>

                                                            {state.levcels3.map(account => {
                                                                return (
                                                                    <option key={account.acc_number} value={account.acc_number}>
                                                                        {account.acc_name_ar}
                                                                    </option>
                                                                )
                                                            })
                                                            }
                                                        </select>
                                                    </div>

                                                </div>}
                                            {(state.levcels4.length == 0) ?

                                                <span></span>
                                                :
                                                (state.hiddenlvl4 == true) ?
                                                    <span></span>

                                                    :
                                                    <div className="form-group col-md-3">


                                                        <div >
                                                            <select className="form-control" name="level4" id="level4"
                                                                onChange={handleTwoEvents4} >
                                                                <option key="0" value="">اختار الحساب الفرعي</option>

                                                                {state.levcels4.map(account => {
                                                                    return (
                                                                        <option key={account.acc_number} value={account.acc_number}>
                                                                            {account.acc_name_ar}
                                                                        </option>
                                                                    )
                                                                })
                                                                }
                                                            </select>
                                                        </div>

                                                    </div>}
                                        </div>

                                        <div className="row">

                                            <div className="form-group col-md-4">
                                                <label><IntlMessages id="form.cointype" /></label>
                                                <div >

                                                    {(state.accountCurrencyIdupdateName == "دولار امريكى") ?
                                                        <>
                                                            <div className="form-row">
                                                                <label>{state.accountCurrencyIdupdateName}</label>
                                                            </div>
                                                            <div className="form-row">
                                                                <label><IntlMessages id="form.exchangeRate" />: {state.exchangerate}</label>
                                                            </div>
                                                        </>
                                                        :
                                                        <label>{state.accountCurrencyIdupdateName}</label>
                                                    }


                                                </div>

                                            </div>
                                            <div className="form-group col-md-4">
                                                <label><IntlMessages id="FORM.DEBT" /></label>
                                                <div >
                                                    <label>{state.accountTypeIdupdateName}</label>
                                                </div>

                                            </div>
                                        </div>
                                        {(state.hiddentextbox == true) ? <React.Fragment></React.Fragment>

                                            :
                                            <div className="form-group col-md-6">
                                                <input type="number" className="form-control" name="enteredvalue"
                                                    onChange={handleFields}
                                                    ref={register({ required: true, minLength: 1 })} />
                                            </div>
                                        }
                                        <br />
                                        {(state.level1 == 0) ? <React.Fragment></React.Fragment>

                                            :
                                            (state.hiddenaddtotablebtn == true) ?
                                                <button onClick={() => {
                                                    setState({
                                                        ...state, hiddentextbox: false
                                                    })
                                                }}
                                                    className=" btn-margin" style={{
                                                        margin: "30px 30px 20px 0", color: '#fff', backgroundColor: "#0063c1",
                                                        fontSize: "17px", fontWeight: "bold", height: '50px'
                                                    }}>
                                                    <IntlMessages id="enterthevalue" />
                                                </button>
                                                :
                                                <button onClick={() => { pushinarray() }}
                                                    className=" btn-margin" style={{
                                                        margin: "30px 30px 20px 0", color: '#fff', backgroundColor: "#0063c1",
                                                        fontSize: "17px", fontWeight: "bold", height: '50px'
                                                    }}>
                                                    <IntlMessages id="form.add" />
                                                </button>
                                        }

                                    </React.Fragment>
                                </RctCardContent>

                            </RctCollapsibleCard>
                        </div>

                    }
                    <div style={{ display: "none" }}>
                        <PrintReceivableReceipt ref={componentRef} {...state} />
                    </div>
                    {(state.hiddentable == true) ? <React.Fragment></React.Fragment>
                        :

                        <div className="row mb-5">
                            <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                                <RctCard>
                                    <RctCardContent noPadding>
                                        <MuiThemeProvider theme={getMuiTheme()}>
                                            <MUIDataTable
                                                id="myTable"

                                                data={state.tablearray}
                                                columns={[
                                                    {
                                                        label: <IntlMessages id="form.main" />,
                                                        name: "tableid",
                                                        options: {
                                                            display: false,
                                                            filter: false,
                                                            print: false
                                                        }
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.main" />,
                                                        name: "level1",
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.branch" />,
                                                        name: "level2"
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.branch" />,
                                                        name: "level3",
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.branch" />,
                                                        name: "level4",
                                                    },

                                                    {
                                                        label: <IntlMessages id="form.cointype" />,
                                                        name: "accountCurrencyIdupdateName",
                                                        options: {
                                                            display: false,
                                                            filter: false,
                                                            print: false
                                                        }
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.main" />,
                                                        name: "accountidlevels",
                                                        options: {
                                                            display: false,
                                                            filter: false,
                                                            print: false
                                                        }
                                                    },

                                                    {
                                                        label: <IntlMessages id="FORM.DEBT" />,
                                                        name: "accountTypeIdupdateName",
                                                        options: {
                                                            display: false,
                                                            filter: false,
                                                            print: false
                                                        }
                                                    },

                                                    {
                                                        label: <IntlMessages id="ENTEREDvalue" />,
                                                        name: "amount",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                                return (
                                                                    <React.Fragment>
                                                                        {(tableMeta.rowData[5] == "دولار امريكى") ?
                                                                            <label>{value * state.exchangerate}</label>

                                                                            :
                                                                            <label>{value}</label>


                                                                        }
                                                                    </React.Fragment>
                                                                );
                                                            }
                                                        }

                                                    },
                                                    {
                                                        label: <IntlMessages id="button.delete" />,
                                                        name: "tableid",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                                return (
                                                                    <React.Fragment>
                                                                        <button type="button" className="btn btn-danger"
                                                                            onClick={
                                                                                () => {
                                                                                    removeRow(value)


                                                                                }}>
                                                                            <IntlMessages id="form.delete" />
                                                                        </button>
                                                                    </React.Fragment>
                                                                );
                                                            }
                                                        }

                                                    },

                                                ]}
                                                options={options}


                                            />

                                        </MuiThemeProvider>
                                    </RctCardContent>
                                </RctCard>
                            </RctCollapsibleCard>
                        </div>

                    }
                    <div style={{ marginTop: "15px", display: 'flex', alignSelf: 'flex-end', marginBottom: "20px" }}>
                        {(state.hiddentable == true) ? <React.Fragment></React.Fragment>
                            :
                            (state.disabled === false) ?
                                <button onClick={() => { onSubmit() }}
                                    className=" btn-margin" style={{
                                        margin: "30px 30px 20px 0", color: '#fff', backgroundColor: "#0063c1",
                                        fontSize: "17px", fontWeight: "bold", height: '50px'
                                    }}>
                                    <IntlMessages id="form.getreciept" />
                                </button>
                                // <button  onClick={() => onSubmit()} className=" btn-margin" style={{ margin:"0 30px" ,color:'#fff',backgroundColor: "#0063c1", 
                                // fontSize:"17px", fontWeight: "bold", height:'50px'}} >
                                //     <IntlMessages id="form.getreciept" />
                                // </button>
                                :
                                <button className=" btn-margin" disabled={true} style={{
                                    margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                    fontSize: "17px", fontWeight: "bold", height: '50px'
                                }}>
                                    <IntlMessages id="form.getreciept" />
                                </button>
                        }
                    </div>

                </div>
                : (
                    history.push("/access-denied")
                )
            }
        </React.Fragment>
    )
    //    }
}