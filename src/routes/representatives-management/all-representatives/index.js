
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

const options = {
   filter: true,
   filterType: 'dropdown',
   rowsPerPage: 5,
   rowsPerPageOptions: [5,10,25,50,100],
   responsive: 'vertical',
   enableNestedDataAccess: '.',
//   rowsSelected: '',
   selectableRows: "none",
   viewColumns: false,
   doawload: false,
   sort: false,
   fixedHeader: true,
   download: false,
   fixedSelectColumn: false,
   tableBodyHeight: "400px"
 };

export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        representatives: [],
        disabled: false,
		hiddenStatusForm: true,
		hiddenDebitLimitForm: true,
      	debit_limit: '',
		debitLimitTable: '',
		user_phonenumber: '',
    });
	
	const nf = new Intl.NumberFormat();
   
	useEffect(() => {
		// all representatives
		axios.get('http://localhost:8000/allrepresentatives',USER_TOKEN).then(response => {
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				for (let i = 0; i < response.data.representatives.length; i ++) {
					response.data.representatives[i].representative_virtual_mony_balance = nf.format(response.data.representatives[i].representative_virtual_mony_balance);
				}
				setState({
					representatives: response.data.representatives,
					hiddenStatusForm: true,
					hiddenDebitLimitForm: true,
					disabled: false,
				})
			}
		})
		.catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}, []);

	const onSubmit2 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/update', data: state,
			headers: { "x-access-token": `${cookies.get('UserToken')}`}
		}).then(res =>{
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload()}, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
   	}

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const onClearStatusClicked = () => {
		setState({...state,
			user_type_id: '',
			hiddenStatusForm: true,
			hiddenDebitLimitForm: true,
			debit_limit: '',
			debitLimitTable: '',
			user_phonenumber: '',
			representativeId: '',
			dealerId: '',
			representativeId: '',
			user_id: '',
			dealerFirstName: '',
			dealerMiddleName: '', 
			dealerLastName: '',
			repFirstName: '',
			repMiddleName: '',
			repLastName: '',
			status: ''
		})
	};

	const onSubmit3 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/updateuserdebitlimit', data: state,
			headers: { "x-access-token": `${cookies.get('UserToken')}`}
			}).then(res =>{
				toast.success("تم تعديل حد الدين المسموح به", {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
	} 

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

    const { match } = props;
    return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
        <div className="cart-wrapper">
            <PageTitleBar title={<IntlMessages id="sidebar.allRepresentatives" />} match={match} />
			<ToastContainer />
			<Helmet>
				<title>قائمة المندوبين</title>
				<meta name="description" content="قائمة المندوبين" />
  			</Helmet>
            <div className="row">
               <RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="form.updateRepData" />}>
					{/*  */}
					{(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
					: 
					<form onSubmit={handleSubmit(onSubmit2)}>	
						<div className="form-row">
							<div className="form-group col-md-4">
							<label>
								<IntlMessages id="form.representativeName" />:  {state.dealerFirstName} {state.dealerMiddleName} {state.dealerLastName}
							</label>
							<label>
								<IntlMessages id="form.currentStatus" />: {state.status} 
							</label>
							</div>
							<div className="form-group col-md-4">
							<label><IntlMessages id="widgets.status" /></label>
							<select className="form-control" name="status_name" 
									onChange={handleFields} ref={register({ required: true })}>  
									<option key="0" value="">برجاء اختيار الحالة</option>
									<option key="1" value="Active">مفعل</option> 
									<option key="2" value="Inactive">غير مفعل</option>    
									<option key="3" value="Suspend">موقوف</option>       
							</select>
							<span className="errors">
								{errors.status_name && errors.status_name.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
							</div>
						</div>

						{(state.disabled === false) ? 
							<button type="submit" className="btn btn-warning">
							<IntlMessages id="form.updateStatus" />
							</button> : 
							<button type="submit" className="btn btn-warning" disabled={true} >
							<IntlMessages id="form.updateStatus" />
							</button>
							}

						<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
							<IntlMessages id="form.clear" />
						</button>
					</form>
                  	}
					{/*  */}
				{(state.hiddenDebitLimitForm == true) ? <React.Fragment></React.Fragment>
				: 
				<form onSubmit={handleSubmit(onSubmit3)}>	
					<div className="form-row">
						<div className="form-group col-md-4">
							{/* <label>
								<IntlMessages id="form.commercialName" />: {state.POSComercialName}
							</label> */}
							{/* <br /> */}
							<label>
							<IntlMessages id="form.representativeName" />:  {state.dealerFirstName} {state.dealerMiddleName} {state.dealerLastName}
							</label> 
                           <label>
						    <IntlMessages id="table.allowedDebtLimit" />: {state.debitLimitTable} 
                           </label>
                        </div>
						<div className="form-group col-md-4">
							<label><IntlMessages id="table.allowedDebtLimit" /></label>
							<input type="tel" className="form-control" name="debit_limit"
								   onChange={handleFields} value={state.debit_limit}
								   ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                
							<span className="errors">
								{errors.debit_limit && errors.debit_limit.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.debit_limit && errors.debit_limit.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
							</span>
						</div>
					</div>

					{(state.disabled === false) ? 
                        <button type="submit" className="btn btn-dark">
                           <IntlMessages id="table.updateAllowedDebtLimit" />
                        </button> : 
                        <button type="submit" className="btn btn-dark" disabled={true} >
                           <IntlMessages id="table.updateAllowedDebtLimit" />
                        </button>
					}
					<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
						<IntlMessages id="form.clear" />
					</button>
				</form>
				}
               </RctCollapsibleCard>
		    </div>
			<div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable 
									// title={<IntlMessages id="sidebar.allrepresentatives" />}
									data={state.representatives}
									columns={[
											{
												label: "id",
												name:  "representative_id",
												options: {
													display: "none",
													filter: false,
													print: false,
												}
											},
											{
												label: <IntlMessages id="form.firstName" />,
												name:  "representative_first_name",
											},
											{
												label: <IntlMessages id="form.middleName" />,
												name:  "representative_middle_name" 
											},
											{
												label: <IntlMessages id="form.lastName" />,
												name:  "representative_last_name",
											},
											{
												label: <IntlMessages id="form.phoneNumber" />,
												name:  "representative_phone_number" 
											},
											{
												label: <IntlMessages id="form.region" />,
												name:  "region_arabic_name",
											},
											{
												label: <IntlMessages id="form.area" />,
												name:  "area_arabic_name"
											},
											{
												label: <IntlMessages id="form.virtualMoneyBalance" />,
												name:  "representative_virtual_mony_balance"
											},
											{
												label: <IntlMessages id="form.image" />,
												name:  "representative_personal_image" ,
												options: {
													filter: true,
													sort: false,
													empty: true,
													customBodyRender: (value, tableMeta, updateValue) => {  
														return (
															<div>
															<img src={"http://localhost:8000/"+value} alt="لا يوجد صورة" width="90" height="100"/>
															</div>
														);
													}
														
												}
											},
											{
												label: <IntlMessages id="widgets.status" />,
												name: "active_status_ar"
											},
											{
												label: <IntlMessages id="table.allowedDebtLimit" />,
												name: "dept_limit"
											},
											{
												label: <IntlMessages id="table.allowedDebtLimit" />,
												name: "dept_limit_un",
												options: {
													display: "none",
													filter: false,
													print: false,
												}
											},
											{
												label: <IntlMessages id="form.updateStatus" />,
												name: "",
												options: {
													filter: true,
													sort: false,
													empty: true,
													print: false,
												customBodyRender: (value, tableMeta, updateValue) => {
													return (
														<React.Fragment>
															<button type="button" className="btn btn-warning" disabled={tableMeta.rowData[0] == 1 ? true : false}
															onClick={() => {
															axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,USER_TOKEN).then((res) => {
																setState({
																	...state , 
																	user_id:res.data.userresult[0].user_id,
																	user_type_id: res.data.userresult[0].user_type_id,
																	hiddenStatusForm: false,
																	hiddenDebitLimitForm: true,
																	dealerFirstName: tableMeta.rowData[1],
																	dealerMiddleName: tableMeta.rowData[2],
																	dealerLastName: tableMeta.rowData[3],
																	status: tableMeta.rowData[9],
																})
															}).catch((error) => {
																if (error.response.status === 429) {
																	toast.error(error.response.data, {
																		position: "top-center",
																		autoClose: 4000,
																		hideProgressBar: false,
																		closeOnClick: true,
																		pauseOnHover: true,
																		draggable: true
																	}); 
																}
															})
															}}>
															<IntlMessages id="form.updateStatus" />
															</button>
														</React.Fragment>
													);
												}
												}
						
											},
											{
												label: <IntlMessages id="table.updateAllowedDebtLimit" />,
												name: "",
												options: {
												filter: true,
												sort: false,
												empty: true,
												print: false,
												customBodyRender: (value, tableMeta, updateValue) => {
													return (
														<React.Fragment>
															<button type="button" className="btn btn-dark" disabled={tableMeta.rowData[0] == 1 ? true : false}
																	onClick={() => {
																	axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[4]}`,USER_TOKEN).then((res) => {
																		setState({
																				...state ,
																				user_id:res.data.userresult[0].user_id,
																				user_type_id: res.data.userresult[0].user_type_id,
																				hiddenStatusForm: true,
																				hiddenDebitLimitForm: false,
																				dealerFirstName: tableMeta.rowData[1],
																				dealerMiddleName: tableMeta.rowData[2],
																				dealerLastName: tableMeta.rowData[3],
																				user_phonenumber: tableMeta.rowData[4], 
																				status: tableMeta.rowData[9],
																				debit_limit: tableMeta.rowData[11],
																				debitLimitTable: tableMeta.rowData[10],
																			})
																	}).catch((error) => {
																		if (error.response.status === 429) {
																			toast.error(error.response.data, {
																			   position: "top-center",
																			   autoClose: 4000,
																			   hideProgressBar: false,
																			   closeOnClick: true,
																			   pauseOnHover: true,
																			   draggable: true
																			}); 
																		}			
																	})
															}}>
															<IntlMessages id="table.updateAllowedDebtLimit" />
															</button>
														</React.Fragment>
													);
												}
												}
						
											},
										]}
									options={options} />
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
        </div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
   }


