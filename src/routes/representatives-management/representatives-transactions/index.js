import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 


export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,

        userIdInTable:'',
        userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: []
	});

	const nf = new Intl.NumberFormat();

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			if (res.data.userresult[0].user_type_id === 1) {
				axios.post('http://localhost:8000/vmtransactions',
				{ 'userTypeId': 1,
				  'userPhoneNumber': state.phoneNumberField 
				},USER_TOKEN).then(res2 => {

					for (let i = 0; i < res2.data.transactions.length; i ++) {
						res2.data.transactions[i].virtual_mony_transaction_value = nf.format(res2.data.transactions[i].virtual_mony_transaction_value);
						res2.data.transactions[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.transactions[i].virtual_money_transaction_transfer_amount);
					}

					// console.log("uuu ",res2.data.userData.userName);
					setState({
						...state,
						name: res2.data.userData.userName,
						region: res.data.tableresult[0].region_arabic_name,
						area: res.data.tableresult[0].area_arabic_name,
						phoneNumber: res.data.tableresult[0].representative_phone_number,
						balance: nf.format(res.data.tableresult[0].representative_virtual_mony_balance),
						userTypeIdInTable: res.data.userresult[0].user_type_id,
						userIdInTable: res.data.userresult[0].user_id,
						type: "مندوب",
						pos_company_cards_transaction: res2.data.pos_company_cards_transaction,
						pos_sim_card_transaction: res2.data.pos_sim_card_transaction,
						pos_vm_transaction: res2.data.transactions
					})
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}	
				});
			}
			else if (res.data.userresult[0].user_type_id != 1) {
				toast.error(<IntlMessages id="toast.NotRepNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					type: '',
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
	});
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>معاملات المندوبين</title>
				<meta name="description" content="معاملات المندوبين" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.sweetAlert" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.sweetAlert" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<Form inline onSubmit={handleSubmit(showDetails)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
								</Form>
							</div>
								<span className="errors">
									{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
										<IntlMessages id="form.requiredError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
										<IntlMessages id="form.numbersOnlyErrorError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
								</span>
							
							<br />
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<h1>{state.type}</h1>
									<br /> 
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
								</React.Fragment>
							}
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.pos_vm_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "virtual_mony_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_transaction_serial_number",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_transaction_value"
									},
									{
										label: <IntlMessages id="form.transferredBalanceValue" />,
										name:  "virtual_money_transaction_transfer_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
}
