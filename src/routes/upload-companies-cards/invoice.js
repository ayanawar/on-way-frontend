import React, { Component } from 'react';

// import Link from 'next/link';
import axios from 'axios';
import Cookies from 'universal-cookie';
import USER_TOKEN from '../../constants/Token';
import IntlMessages from 'Util/IntlMessages';


import PropTypes from "prop-types";
// import { useTranslation } from 'react-i18next';
// import { Trans } from "react-i18next";
import { Flag, Print } from '@material-ui/icons';
import logo from '../../assets/img/icon10.jpg';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import QRCode from 'react-qr-code';
import Barcode from 'react-barcode';
import { ToastContainer, toast } from 'react-toastify';
import Modal from 'react-awesome-modal';
import style from 'react-awesome-modal/lib/style';

const nf = new Intl.NumberFormat();

const cookies = new Cookies();

class PrintInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //defauilt value of the date time
            todayDate: '',
            categoryName: '',
            supplierFName: '',
            supplierMName: '',
            supplierLName: '',
            regionName: '',
            threeLastBundles: [],
            lastSellPrice: 0,
            lastPurchasePrice: 0,
            totalCardsCount: 0,
            sumCurrentBundle: 0,
            sumTotalBundles: 0,
            companyName: 0,
            isPrinted: false
        };


    }

    componentDidMount() {
        var date = new Date();
        date = date.getUTCFullYear() + 1 + '/' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '/' +
            ('00' + date.getUTCDate()).slice(-2);
        // console.log(date);

        this.setState({
            todayDate: date
        })

        // console.log("props", this.props);
        for (var i = 0; i < this.props.categories.length; i++) {
            if (this.props.categories[i].cards_category_id == this.props.categoryId) {
                this.setState({
                    categoryName: this.props.categories[i].category_text
                })
            }
        }

        for (var i = 0; i < this.props.suppliers.length; i++) {
            if (this.props.suppliers[i].supplier_id == this.props.supplierId) {
                this.setState({
                    supplierFName: this.props.suppliers[i].supplier_first_name,
                    supplierMName: this.props.suppliers[i].supplier_middle_name,
                    supplierLName: this.props.suppliers[i].supplier_last_name
                })
            }
        }

        var names = '';
        var regionsNames = '';
        for (var i = 0; i < this.props.regions.length; i++) {
            
            for (var j = 0; j < this.props.regionIds.length; j++) {
                if (this.props.regions[i].region_id == this.props.regionIds[j]) {
                    var name = this.props.regions[i].region_arabic_name;
                    if (regionsNames == '') {
                        names = regionsNames.concat(name);
                        regionsNames = names;
                    } else {
                        names = regionsNames.concat(', ' + name);
                        regionsNames = names;

                    }
                }
                
            }
            this.setState({
                regionName: regionsNames
            })

        }

        for (var i = 0; i < this.props.companies.length; i++) {
            if (this.props.companies[i].company_id == this.props.companyId) {
                this.setState({
                    companyName: this.props.companies[i].company_name_ar,
                })
            }
        }

        if (this.props.sellPriceChange == true) {
            this.setState({
                lastSellPrice: this.props.cardsSellPrice
            })
        } else {
            this.setState({
                lastSellPrice: this.props.sellPrice
            })
        }

        if (this.props.purchasePriceChange == true) {
            this.setState({
                lastPurchasePrice: this.props.cardsBuyingPrice
            })
        } else {
            this.setState({
                lastPurchasePrice: this.props.purchasePrice
            })
        }

        var totalCardsCount = this.props.oldCardsCount + this.props.rightPinsCount
        // console.log("data to get sum total cards count", this.props.oldCardsCount, this.props.bundleCount);
        this.setState({
            totalCardsCount: totalCardsCount
        })

        // axios.post('http://localhost:8000/lastbundles', { "userTypeId": 5, "companyId": this.props.companyId, "categoryId": this.props.categoryId }, USER_TOKEN)
        //     .then(response => {
        //         if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
        //             cookies.remove('UserToken', { path: '/' })
        //             window.location.href = "/signin";
        //         } else {
        //             console.log("3 bundles", response.data.message);
        //             this.setState({
        //                 threeLastBundles: response.data.message
        //             })
        //         }
        //     })
        //     .catch(error => {
        //         // console.log('Error fetching and parsing data', error);
        //     });

    }

    closeModelFromParent = () => {

        this.props.onSelectCLose(false);
    }

    askForPrint = () => {
        this.setState({
            isPrinted: true
        })
    }

    closeModel = () => {
        this.setState({
            isPrinted: false
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();

        // let isValidate = this.validate()

        // send excel file data to server
        // if (isValidate == true) {
        if (this.props.fileType == "txt") {
            if (this.props.rightPinsCount == 0) {
                toast.error("لا يوجد كروت للتحميل, اختر ملف اخر سليم للتحميل", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            } else {
                axios.post(`http://localhost:8000/uploadcompanyfilteredcards`, this.props, USER_TOKEN)
                    .then(response => {
                        if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                            cookies.remove('UserToken', { path: '/' })
                            window.location.href = "/signin";
                        } else {
                            // this.props.onSelectLoad(true)
                            // this.closeModelFromParent();
                            // console.log("plaaaaaaaaaaaaaaa");
                            // console.log(response.data);
                            // console.log(response.status);
                            // console.log(response);

                            // this.setState({
                            //     // loader: false,
                            //     RepeatedPinsInDb: response.data.RepeatedArrayInDb,
                            //     RepeatedPinsInFile: response.data.RepeatedArrayInFile,
                            //     rightPins: response.data.notRepeatedPins,
                            //     unAppropriateArrayEOrU: response.data.unAppropriateArrayEOrU
                            // })
                            if (response.status == 200) {
                                toast.success('تم تحميل كرتات الشركات بنجاح', {
                                    position: "top-center",
                                    autoClose: 4000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true
                                });
                            }

                            // setTimeout(function () { location.reload() }, 3000)
                            // console.log("done with then");

                        }
                    })
                    .catch(error => {
                        this.closeModelFromParent();
                        // console.log("error occuered");
                        // console.log("error", error);
                        // console.log(error.response);
                        // console.log(error.response.status);
                        if (error.response.status == 422) {
                            toast.error("خطأ فى بيانات الملف, برجاء المراجعة والمحاولة مرة اخرى", {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                            });
                        } else if (error.response.status === 429) {
                            toast.error(error.response.data, {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                            });
                        }
                        else {
                            toast.error("لقد قمت بتحميل هذة الكوته بالقعل", {
                                position: "top-center",
                                autoClose: 4000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true
                            });
                        }
                        // console.log('Error fetching and parsing data ', error);


                    });
            }
        } else {
            toast.error("text document نوع الملف ليس", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }

    }
    handleTwoEvents = (e) => {
        this.handleSubmit(e);
        this.props.onSelectLoad();
    }

    renderTableData() {


        // console.log("three bundles", this.props.threeLastBundles);
        return this.props.threeLastBundles.map((bundle, index) => {
            const { bundle_number, bundle_sell_price, purchase, bundle_count, creation_date, purchase_un, bundle_count_un } = bundle //destructuring
            var totalAmount = purchase_un * bundle_count_un;
            // console.log("bundleCount", parseInt(bundle_count));
            // console.log("purchase", parseInt(purchase));
            // console.log("total", totalAmount);
            return (
                <tr key={bundle_number}>
                    <td style={styles.tableStyle}></td>
                    <td style={styles.tableStyle}>{bundle_number}</td>
                    <td style={styles.tableStyle}>{bundle_count}</td>
                    <td style={styles.tableStyle}>{bundle_sell_price}</td>
                    <td style={styles.tableStyle}>{purchase}</td>
                    <td style={styles.tableStyle}>{nf.format(totalAmount)}</td>
                    <td style={styles.tableStyle}>{creation_date}</td>
                </tr>
            )
        })
    }

    render() {
        // console.log("state", this.state);
        const { classes } = this.props;
        // console.log("bundle number", this.props.bundleNumber);
        return (
            <div>
                <ToastContainer />
                <div className="container" >
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body p-0">
                                    <div className="row p-5">
                                        <div className="col-md-3 text-right" style={{ textAlignLast: "right" }}>
                                        
                                        </div>
                                        <div className="col-md-6 text-center">
                                            <img width={150} height={150} src={logo} className="img-fluid" style={{
                                                marginLeft: "auto",
                                                marginRight: "auto"
                                            }} />
                                        </div>
                                        <div className="col-md-3">

                                        </div>


                                    </div>
                                    <div>
                                        <h4 className="text-center">فاتورة رفع كوتة الشركات</h4>
                                    </div>

                                    <hr className="my-5" />

                                    <div className="row justify-content-between pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-6 text-right">
                                            <Barcode value={this.props.bundleNumber} width="1" height="70" textAlign="center"
                                                textPosition="bottom" style={{ width: "18%", padding: "17px", marginLeft: "50px" }} displayValue={true} />

                                        </div>

                                        <div className="col-md-6" style={{
                                            // paddingRight: "406px",
                                            direction: "ltr",
                                            marginTop: "12px"
                                        }}>
                                            <QRCode size={75} value={this.props.bundleNumber} />
                                        </div>
                                    </div>

                                    <div className="row pb-5 p-5" style={{ margin: "auto" }}>
                                        <div className="col-md-12 text-left" style={{ textAlignLast: "right", direction: "rtl !important" }}>

                                            <p className="font-weight-bold mb-4">بيانات الفاتورة</p>
                                            <p className="mb-1">التاريخ: {this.state.todayDate}</p>
                                            <p className="mb-1">اسم الشركة: {this.state.companyName}</p>
                                            <p className="mb-1">الفئة: {this.state.categoryName}</p>
                                            <p className="mb-1">اسم المورد: {this.state.supplierFName} {this.state.supplierMName} {this.state.supplierLName}</p>
                                            <p className="mb-1">المصدر: {this.props.source}</p>
                                            <p className="mb-1">المحافظة: {this.state.regionName}</p>
                                            <p className="mb-1">العملة: {this.props.Currency}</p>
                                            <p className="mb-1">الملاحظة: {this.props.notes}</p>
                                        </div>
                                    </div>
                                    <div style={{ direction: "rtl" }}>
                                        <div className="row p-5" style={{ textAlignLast: "right" }}>
                                            <div className="col-md-12">
                                                <table className="table" style={styles.tableStyle}>
                                                    <thead style={styles.tableStyle}>
                                                        <tr style={styles.tableStyle}>
                                                            <th className="border-0 text-uppercase small font-weight-bold">المتوفر قبل الرفع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">رقم الكوتة</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">سعر البيع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">سعر الشراء</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">الاجمالى</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">تاريخ تحميل الكوته</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style={styles.tableStyle}>
                                                        {this.renderTableData()}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div className="row p-5">
                                            <div className="col-md-12">
                                                <table className="table">
                                                    <thead>
                                                        <tr>
                                                            <th className="border-0 text-uppercase small font-weight-bold"></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">الكمية</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">سعر البيع</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">سعر الشراء</th>
                                                            <th className="border-0 text-uppercase small font-weight-bold">الاجمالى</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>الجارى رفعة</td>
                                                            <td>{this.props.rightPinsCount}</td>
                                                            <td>{nf.format(this.state.lastSellPrice)}</td>
                                                            <td>{nf.format(this.state.lastPurchasePrice)}</td>
                                                            <td>{nf.format(this.props.rightPinsCount * this.state.lastPurchasePrice)}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>الاجمالى بعد الرفع</td>
                                                            <td>{this.state.totalCardsCount}</td>
                                                            <td>{nf.format(this.state.lastSellPrice)}</td>
                                                            <td>{nf.format(this.state.lastPurchasePrice)}</td>
                                                            <td>{nf.format(this.state.totalCardsCount * this.state.lastPurchasePrice)}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>



                                    <div className="row d-flex bg-dark text-white p-3">
                                        <div className="col-md-6 py-3 px-5" style={{ textAlignLast: "right" }}>
                                            <div className="mb-2">اجمالى عدد الكرتات</div>
                                            <div className="h2 font-weight-light"> {this.state.totalCardsCount} كارت</div>

                                            <div className="py-3 px-5 text-right">
                                                <div className="mb-2">هل انت منأكد وترغب فى الرفع؟</div>
                                            </div>
                                        </div>

                                        <div className="col-md-6">
                                            <div className="container">
                                                <div className="row">
                                                    <div className="col-12 p-2">
                                                        <div className="col-md-6">

                                                        </div>
                                                        <div className="col-md-6" style={{ display: "contents" }}>
                                                            <div className="container">

                                                                <button type="button" className="btn btn-primary" style={{ margin: "39px" }} onClick={this.handleTwoEvents}>نعم</button>
                                                                <button type="button" className="btn btn-secondary" onClick={this.closeModelFromParent}>اغلاق</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div> */}

                </div>


            </div>
        );

    }
}

const styles = {
    tableStyle: {

        borderCollapse: "collapse",
        padding: "10px",
    },
}


export default PrintInvoice;
