//upload comapny cards

import React, { Component } from 'react';
import USER_TOKEN from '../../constants/Token';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import Modal from 'react-awesome-modal';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// import UploadCardModel from './uploadCardsPopUp';

// pop up
import PrintInvoice from './invoice';

// printed part
import Invoice from './printInvoice';

// check file data model
import DataChcekModel from './uploaded-data-check-model';
import { Spinner } from 'reactstrap';
import { Multiselect } from 'multiselect-react-dropdown';

import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import { ToastContainer, toast } from 'react-toastify';
import * as XLSX from 'xlsx';
import axios from 'axios';
import Cookies from 'universal-cookie';
import image from '../../assets/img/upload-cards-1.png';
import image1 from '../../assets/img/upload-cards-2.png';
import { Helmet } from "react-helmet";


const nf = new Intl.NumberFormat();

const cookies = new Cookies();

class Dropzone extends Component {

   state = {
      isUploaded: false,
      cardsNumber: 0,
      excelFileName: '',
      sheetName: '',
      fileSize: 0,
      regions: [],
      companies: [],
      suppliers: [],
      categories: [],
      todayDate: '',
      companyId: '',
      categoryId: 0,
      supplierId: 0,
      source: '',
      regionId: 0,
      bundleNumber: '',
      cardsBuyingPrice: 0,
      cardsSellPrice: 0,
      expiryDate: '',
      currency: '',
      notes: '',
      fileData: [],
      bundleCount: 0,
      companyName: '',
      date: 0,
      sellPrice: '',
      purchasePrice: '',
      companyNameError: '',
      categoryNameError: '',
      supplierNameError: '',
      sourceError: '',
      regionError: '',
      expiryDateError: '',
      bundleNumberError: '',
      currencyError: '',
      uploadFileError: '',
      categoryCurrency: '',
      oldCardsCount: 0,
      loadModel: false,
      sellPriceChange: false,
      purchasePriceChange: false,
      ModelToPrint: false,
      fileType: '',
      checkPrice: '',
      loadCheckModel: false,
      threeLastBundles: [],
      RepeatedPinsInDb: [],
      RepeatedPinsInFile: [],
      rightPins: [],
      unAppropriateArrayEOrU: [],
      RepeatedPinsInDbCount: 0,
      RepeatedPinsInFileCount: 0,
      rightPinsCount: 0,
      regionIds: [],
      unAppropriateArrayEOrUCount: 0,
      formateSellPrice: '',
      formatePurchasePrice: '',
      userId: localStorage.getItem("userIdInUsers"),
      userTypeId: localStorage.getItem("user_type_id")
   };

   validate = () => {
      let companyNameError = '';
      let categoryNameError = '';
      let supplierNameError = '';
      let sourceError = '';
      let regionError = '';
      let expiryDateError = '';
      let bundleNumberError = '';
      let currencyError = '';
      let uploadFileError = '';
      let checkPrice = '';
      if (this.state.companyId == '') {
         companyNameError = 'برجاء اختيار الشركة'
      }
      if (this.state.categoryId == 0) {
         categoryNameError = 'برجاء اختيار الفئة'
      }
      if (this.state.supplierId == 0) {
         supplierNameError = 'برجاء اختيار الصادر من'
      }
      if (this.state.source == '') {
         sourceError = 'برجاء اختيار المصدر'
      }
      if (this.state.regionIds == []) {
         regionError = 'برجاء اختيار المحافظة'
      }
      if (this.state.expiryDate == '') {
         expiryDateError = 'برجاء ادخال تاريخ الانتهاء'
      }
      if (this.state.fileData == '') {
         uploadFileError = 'برجاء اخنبار ملف للتحميل'
      }
      var price = true;
      let cardsBuyingPrice = parseInt(this.state.cardsBuyingPrice);
      let cardsSellPrice = parseInt(this.state.cardsSellPrice);
      let purchasePrice = parseInt(this.state.purchasePrice);
      let sellPrice = parseInt(this.state.sellPrice);


      if (this.state.sellPriceChange == false && this.state.purchasePriceChange == false) {
         if (purchasePrice > sellPrice) {
            price = false;
            checkPrice = ' سعر البيع اقل من سعر الشراء برجاء التعديل';
         }
      } else if (this.state.sellPriceChange == true && this.state.purchasePriceChange == false) {
         if (purchasePrice > cardsSellPrice) {
            price = false;
            checkPrice = ' سعر البيع اقل من سعر الشراء برجاء التعديل';
         }
      } else if (this.state.sellPriceChange == false && this.state.purchasePriceChange == true) {
         if (cardsBuyingPrice > sellPrice) {
            price = false;
            checkPrice = ' سعر البيع اقل من سعر الشراء برجاء التعديل';
         }
      } else if (this.state.sellPriceChange == true && this.state.purchasePriceChange == true) {
         if (cardsBuyingPrice > cardsSellPrice) {
            price = false;
            checkPrice = ' سعر البيع اقل من سعر الشراء برجاء التعديل';
         }
      }
      if (companyNameError || categoryNameError || supplierNameError || sourceError || regionError || expiryDateError || bundleNumberError || currencyError || uploadFileError || price == false) {
         this.setState({
            companyNameError, categoryNameError, supplierNameError, sourceError, regionError, expiryDateError, bundleNumberError, currencyError, uploadFileError, checkPrice
         })

         return false;
      }


      return true;
   }

   setTodayDate = () => {
      var date = new Date();
      date = date.getUTCFullYear() + 1 + '-' +
         ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
         ('00' + date.getUTCDate()).slice(-2) + ' ';
      // console.log(date);

      this.setState({
         expiryDate: date.trim()
      })
      return date.trim();
   }

   componentDidMount() {
      this.setTodayDate()
      // console.log("cookies", cookies);

      // all regions
      axios.get('http://localhost:8000/allregions', USER_TOKEN)
         .then(response => {
            // console.log("response", response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            }
            else {
               // response.data.regions.splice(0, 1);
               this.setState({
                  regions: response.data.regions
               })
            }


         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });

      // all companies
      axios.get('http://localhost:8000/allcompanies', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               response.data.companies.splice(0, 1);
               this.setState({
                  companies: response.data.companies
               })
            }


         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });

      // all suppliers
      axios.get('http://localhost:8000/allsuppliers', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  suppliers: response.data.suppliers
               })
            }


         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });

      // next bundle number
      axios.get('http://localhost:8000/bundleid', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  bundleNumber: response.data.bundleId
               })
            }
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
   }
   constructor(props) {
      super(props);

      // // For a full list of possible configurations,
      // // please consult http://www.dropzonejs.com/#configuration
      // this.djsConfig = {
      //    addRemoveLinks: true,
      //    acceptedFiles: "file/xlsx"
      // };

      // this.componentConfig = {
      //    iconFiletypes: ['.xlsx'],
      //    showFiletypeIcon: true,
      //    postUrl: '/'
      // };

      // // If you want to attach multiple callbacks, simply
      // // create an array filled with all your callbacks.
      // this.callbackArray = [() => console.log('Hi!'), () => console.log('Ho!')];

      // // // Simple callbacks work too, of course
      // this.callback = (file) => { };

      // //this.success = file => console.log('uploaded', file);

      // this.removedfile = file => console.log('removing...', file);

      // this.dropzone = null;
      // this.handleChange = this.handleChange.bind(this);
   }
   handleTwochangeEvent = e => {
      this.handleChange(e);
      this.getAllCategoriesByCompanyID(e);
      // console.log("plaaaaaaaaa");
      this.setCompanyNameSelected(e);
      // this.getOldCardsCount(e);
   }

   getOldCardsCount = (event) => {
      // console.log("entered for cards number", event.target.value);
      let catId = event.target.value;
      let companyId = this.state.companyId;
      // console.log("companyId", companyId);
      axios.post(`http://localhost:8000/cardscount`, { "companyId": companyId, "categoryId": catId }, USER_TOKEN)
         .then(response => {
            // console.log(response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  oldCardsCount: response.data.cardsCount
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
   }

   setCompanyNameSelected = (event) => {
      for (var i = 0; i < this.state.companies.length; i++) {
         // console.log(event.target.value);
         if (this.state.companies[i].company_id == event.target.value) {
            // console.log(this.state.companies[i].company_name);
            this.setState({
               companyName: this.state.companies[i].company_name
            })
         }
      };
   }

   loadModel = () => {
      let compId = parseInt(this.state.companyId);
      let cattId = parseInt(this.state.categoryId);

      axios.post(`http://localhost:8000/cardscount`, { "companyId": compId, "categoryId": cattId }, USER_TOKEN)
         .then(response => {
            // console.log(response.data.cardsCount);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               // console.log("oldCardsCount", response.data);
               this.setState({
                  oldCardsCount: response.data.cardsCount
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });

      let catId = parseInt(this.state.categoryId);

      axios.post('http://localhost:8000/lastbundles', { "userTypeId": 5, "companyId": compId, "categoryId": catId }, USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               // console.log("3 bundles", response.data.message);
               this.setState({
                  threeLastBundles: response.data.message
               })
            }
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });

      let isValidate = this.validate()
      // console.log("isValide", isValidate);
      if (isValidate == true) {

         if (this.state.fileType == "txt") {
            axios.post(`http://localhost:8000/uploadcards`, this.state, USER_TOKEN)
               .then(response => {
                  if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                     cookies.remove('UserToken', { path: '/' })
                     window.location.href = "/signin";
                  } else {
                     // this.props.onSelectLoad(true)
                     // this.closeModelFromParent();
                     // console.log("plaaaaaaaaaaaaaaa");
                     // console.log(response.data);
                     this.setState({
                        // loader: false,
                        RepeatedPinsInDb: response.data.RepeatedArrayInDb,
                        RepeatedPinsInFile: response.data.RepeatedArrayInFile,
                        rightPins: response.data.notRepeatedPins,
                        unAppropriateArrayEOrU: response.data.unAppropriateArrayEOrU
                     })
                     // toast.success('تم تحميل كرتات الشركات بنجاح', {
                     //     position: "top-center",
                     //     autoClose: 4000,
                     //     hideProgressBar: false,
                     //     closeOnClick: true,
                     //     pauseOnHover: true,
                     //     draggable: true
                     // });
                     // setTimeout(function () { location.reload() }, 3000)
                     // console.log("done with then");
                     this.countNumberOfCards()

                  }
               })
               .catch(error => {
                  this.closeModelFromParent();
                  // console.log("error occuered");
                  // console.log("error", error);
                  // console.log(error.response);
                  // console.log(error.response.status);
                  if (error.response.status == 422) {
                     toast.error("خطأ فى فورمات الملف", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     });
                  } else if (error.response.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     });
                  }
                  else {
                     toast.error("لقد قمت بتحميل هذة الكوته بالقعل", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     });
                  }
                  // console.log('Error fetching and parsing data ', error);


               });
            // if (this.state.RepeatedPinsInDb.length != 0 || this.state.RepeatedPinsInFile.length != 0 || this.state.rightPins.length != 0 || this.state.unAppropriateArrayEOrU.length != 0) {
            //    console.log("plaaaaaaaaaaaa");
            //    this.countNumberOfCards()
            // }
         } else {
            toast.error("text document نوع الملف ليس", {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });
         }




         this.setState({
            loadCheckModel: true
         })
      }

   }

   countNumberOfCards = () => {
      // console.log("entered to set count");
      let RepeatedPinsInDbCount = this.state.RepeatedPinsInDb.length;
      let RepeatedPinsInFileCount = this.state.RepeatedPinsInFile.length;
      let rightPinsCount = this.state.rightPins.length;
      let unAppropriateArrayEOrUCount = this.state.unAppropriateArrayEOrU.length;
      // console.log("RepeatedPinsInDbCount", RepeatedPinsInDbCount);
      // console.log("RepeatedPinsInFileCount", RepeatedPinsInFileCount);
      // console.log("rightPinsCount", rightPinsCount);
      // console.log("unAppropriateArrayEOrUCount", unAppropriateArrayEOrUCount);

      this.setState({
         RepeatedPinsInDbCount: RepeatedPinsInDbCount,
         RepeatedPinsInFileCount: RepeatedPinsInFileCount,
         rightPinsCount: rightPinsCount,
         unAppropriateArrayEOrUCount: unAppropriateArrayEOrUCount
      })
   }

   getAllCategoriesByCompanyID = (event) => {
      // console.log("loggggggg");
      axios.get(`http://localhost:8000/allcategories/${event.target.value}`, USER_TOKEN)
         .then(response => {
            // console.log(response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  categories: response.data.message
               })
            }
            // console.log(this.state.categories);
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
   }

   getpurchaseAndSellPriceByCategoryId = (event) => {
      // console.log("get prices");
      let company_id = this.state.companyId
      let cards_category_id = event.target.value

      axios.post(`http://localhost:8000/allcategoriesbyid`, { company_id, cards_category_id }, USER_TOKEN)
         .then(response => {
            // console.log(response.data.message[0]);
            // console.log("plaaa");
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  sellPrice: response.data.message[0].sell_price,
                  purchasePrice: response.data.message[0].purchase_price,
                  Currency: response.data.message[0].category_currency,
                  formatePurchasePrice: response.data.message[0].purchase_price,
                  formateSellPrice: response.data.message[0].sell_price,
                  purchasePriceChange: false,
                  sellPriceChange: false
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
      // console.log(this.state.sellPrice);
   }

   readTextFile = (e) => {
      // console.log(e.target.files[0]);
      var file = e.target.files[0]
      // console.log(e.target.files[0]);
      var size = file.size;
      var name = file.name;
      let files = e.target.files
      let reader = new FileReader();
      reader.readAsText(files[0]);
      reader.onload = (e) => {
         // console.log(e.target.result);
         var cells = e.target.result.split('\n').map(function (el) { return el.split(/([^,]+)/g); });
         // let serialnumber, pinNumber, date;
         // let headings = [serialnumber, pinNumber, date]
         // console.log("cells", cells);
         // console.log("cells", cells[0][0]);

         // console.log("serialNumber", serialnumber);
         // var obj = cells.map(function (el) {
         //    var obj = {};
         //    for (var i = 0, l = el.length; i < l; i++) {
         //       obj[headings[i]] = isNaN(Number(el[i])) ? el[i] : +el[i];
         //    }
         //    return obj;
         // });

         // console.log("file", obj);
         var count = cells.length;
         var json = cells;

         this.setState({
            isUploaded: true,
            bundleCount: count,
            excelFileName: name,
            fileSize: size,
            fileData: json,
            fileType: "txt"
         });
      }

   }

   checkFileFormat = (fileData) => {
      // console.log("file format");
      // console.log(fileData);
   }

   closeDataCheckModel = () => {
      this.setState({
         loadCheckModel: false
      })
   }

   readExcel = (file) => {
      const promise = new Promise((resolve, reject) => {
         const fileReader = new FileReader();
         fileReader.readAsArrayBuffer(file);

         fileReader.onload = (e) => {
            const bufferArray = e.target.result;
            // console.log(file.type);
            const wb = XLSX.read(bufferArray, { type: 'buffer' });
            const wsname = wb.SheetNames[0];
            //console.log(wb.SheetNames);
            //console.log(wsname);
            const ws = wb.Sheets[wsname]
            const data = XLSX.utils.sheet_to_json(ws);
            data.push(wsname)
            resolve(data)
         };

         fileReader.onerror = ((error) => {
            reject(error);
         })
      })



      promise.then((d) => {

         // console.log("bundleCount", d.length)
         var count = d.length
         if (this.state.companyId == "6") {
            count = count - 1;
         }
         // console.log(d)
         let size = file.size / 1000;
         this.setState({
            isUploaded: true,
            bundleCount: count,
            excelFileName: file.name,
            sheetName: d[d.length - 1],
            fileSize: size,
            fileData: d,
            fileType: "xml"
         });
         this.checkFileFormat(d);
      })


   }

   handleChange = ({ target }) => {
      this.setState({ ...this.state, [target.name]: target.value });
      // console.log(this.state)
   };

   handleNewPrices = (event) => {
      // console.log("plaaaaa", event.target.name);
      this.setState({ ...this.state, [event.target.name]: event.target.value });
      if (event.target.name == 'cardsBuyingPrice') {
         this.setState({
            purchasePriceChange: true,
            formatePurchasePrice: event.target.value,
            purchasePrice: event.target.value
         })
      } else if (event.target.name == 'cardsSellPrice') {
         this.setState({
            sellPriceChange: true,
            formateSellPrice: event.target.value,
            sellPrice:  event.target.value
         })
      }

   }

   handleTwoEvents = (e) => {
      // console.log("entered for getting prices", e.target);
      this.handleChange(e);
      this.getpurchaseAndSellPriceByCategoryId(e);
      this.getOldCardsCount(e)
   }

   closeModel = () => {
      this.setState({
         ModelToPrint: false
      });
   }

   closeModelFromChild = (val) => {
      this.setState({
         loadModel: val
      })
   }

   closeCurrentModel = (val) => {
      this.setState({
         loadCheckModel: false,
         loadModel: true
      })
   }

   openPrintModel = (val) => {
      this.setState({
         ModelToPrint: true,
         loadModel: false
      })
   }

   resetState = () => {
      this.setState({
         isUploaded: false,
         cardsNumber: 0,
         excelFileName: '',
         sheetName: '',
         fileSize: 0,
         todayDate: '',
         companyId: '',
         categoryId: 0,
         supplierId: 0,
         regionId: 0,
         cardsBuyingPrice: 0,
         cardsSellPrice: 0,
         currency: '',
         notes: '',
         fileData: [],
         bundleCount: 0,
         companyName: '',
         date: 0,
         sellPrice: '',
         purchasePrice: '',
         companyNameError: '',
         categoryNameError: '',
         supplierNameError: '',
         bundleNumber: '',
         sourceError: '',
         regionError: '',
         expiryDateError: '',
         bundleNumberError: '',
         currencyError: '',
         uploadFileError: '',
         categoryCurrency: '',
         oldCardsCount: 0,
         loadModel: false,
         sellPriceChange: false,
         purchasePriceChange: false,
         ModelToPrint: false,
         fileType: '',
         checkPrice: '',
         regionIds: [],
         threeLastBundles: [],
      })
      location.reload();
   }

   FileNotFormatted = (file) => {
      this.setState({
         fileType: ''
      })
      toast.error("text document نوع الملف ليس", {
         position: "top-center",
         autoClose: 4000,
         hideProgressBar: false,
         closeOnClick: true,
         pauseOnHover: true,
         draggable: true
      });
   }
   // handleSubmit = (e) => {
   //    e.preventDefault();

   //    // let isValidate = this.validate()

   //    // send excel file data to server
   //    // if (isValidate == true) {

   //    axios.post(`http://localhost:8000/uploadcards`, this.state, USER_TOKEN)
   //       .then(response => {
   //          if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
   //             cookies.remove('UserToken', { path: '/' })
   //             window.location.href = "/signin";
   //          } else {
   //             this.closeModel();
   //             console.log(response.data.message);
   //             // if(response.data.message == "file formate not acceabtable"){
   //             //    toast.success('خطأ فى فومات الملف', {
   //             //       position: "top-center",
   //             //       autoClose: 4000,
   //             //       hideProgressBar: false,
   //             //       closeOnClick: true,
   //             //       pauseOnHover: true,
   //             //       draggable: true
   //             //    });
   //             //    setTimeout(function () { location.reload() }, 3000)
   //             // }else{
   //             //    toast.success('تم تحميل كرتات الشركات بنجاح', {
   //             //       position: "top-center",
   //             //       autoClose: 4000,
   //             //       hideProgressBar: false,
   //             //       closeOnClick: true,
   //             //       pauseOnHover: true,
   //             //       draggable: true
   //             //    });
   //             //    setTimeout(function () { location.reload() }, 3000)
   //             // }


   //          }
   //       })
   //       .catch(error => {
   //          this.closeModel();
   //          // console.log('Error fetching and parsing data ', error);
   //          console.log("error", error);
   //          console.log(error.response);
   //          console.log(error.response.status);
   //          toast.error("لقد قمت بتحميل هذة الكوته بالقعل", {
   //             position: "top-center",
   //             autoClose: 4000000,
   //             hideProgressBar: false,
   //             closeOnClick: true,
   //             pauseOnHover: true,
   //             draggable: true
   //          });


   //       });
   // }

   onSelect = (selectedList, selectedItem) => {
      // console.log("selectedList", selectedList);
      // console.log("selectedItem", selectedItem);
      var regionIds2 = [];
      selectedList.map((item) => {
         // console.log(item.region_id);
         regionIds2.push(item.region_id)
      })
      // console.log("regions", regionIds2);
      this.setState({ ...this.state, regionIds: regionIds2 });
   }



   onRemove = (selectedList, removedItem) => {
      // console.log("selectedList", selectedList);
      // console.log("removedItem", removedItem.region_id);
      var regionIds = this.state.regionIds;
      // console.log("regionsIds from state", regionIds);
      for (var i = 0; i < regionIds.length; i++) {

         if (regionIds[i] === removedItem.region_id) {

            regionIds.splice(i, 1);
         }

      }
      // console.log("new region array", regionIds);
      this.setState({ ...this.state, regionIds: regionIds });
   }
   render() {

      // console.log(this.state);
      //console.log(this.state.todayDate)
      const config = this.componentConfig;
      const djsConfig = this.djsConfig;

      // For a list of all possible events (there are many), see README.md!
      const eventHandlers = {
         init: dz => this.dropzone = dz,
         drop: this.callbackArray,
         addedfile: this.callback,
         success: this.success,
         removedfile: this.removedfile
      }


      return (

         <div className="dropzone-wrapper">
            <ToastContainer />
            <Helmet>
               <title>تحميل كارتات الشركات</title>
               <meta name="description" content="تحميل كارتات الشركات" />
            </Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.dropzone" />} match={this.props.match} />
            <div className="row">


               <RctCollapsibleCard
                  colClasses="col-sm-12 col-md-12 col-lg-12"
                  heading={<IntlMessages id="sidebar.dropzone" />}
               >
                  <form>
                     <div className="form-row">
                        <div className="form-group col-md-6">
                           <label><IntlMessages id="form.companyName" /></label>
                           <select id="inputComapny" className="form-control" name="companyId" value={this.state.companyId} onChange={this.handleTwochangeEvent}>
                              <option key="0" value="" > </option>
                              {
                                 this.state.companies.map((company) => {
                                    return (
                                       <option key={company.company_id} value={company.company_id}>{company.company_name}</option>
                                    )
                                 })
                              }
                           </select>
                           {this.state.companyNameError ? <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.companyNameError}
                           </div> : null}
                        </div>
                        <div className="form-group col-md-6">
                           <label><IntlMessages id="widgets.category" /></label>
                           <select id="selectCategory" className="form-control" name="categoryId" value={this.state.categoryId} onChange={this.handleTwoEvents}>
                              <option key="0" value="" ></option>
                              {
                                 this.state.categories.map((category) => {
                                    return (
                                       <option key={category.cards_category_id} value={category.cards_category_id} >{category.category_text}</option>
                                    )
                                 })
                              }
                           </select>
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.categoryNameError}
                           </div>
                        </div>
                     </div>
                     <div className="form-row">
                        <div className="form-group col-md-6">
                           <label><IntlMessages id="form.supplier" /></label>
                           <select id="selectSupplier" className="form-control" name="supplierId" value={this.state.supplierId} onChange={this.handleChange}>
                              <option key="0" value="" ></option>
                              {
                                 this.state.suppliers.map((supplier) => {
                                    return (
                                       <option key={supplier.supplier_id} value={supplier.supplier_id}>{supplier.supplier_first_name} {supplier.supplier_middle_name} {supplier.supplier_last_name}</option>
                                    )
                                 })
                              }
                           </select>
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.supplierNameError}
                           </div>
                        </div>
                        <div className="form-group col-md-6">
                           <label><IntlMessages id="form.source" /></label>
                           <input type="text" className="form-control" name="source" onChange={this.handleChange} />
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.sourceError}
                           </div>
                        </div>
                     </div>
                     {/* <div class="form-row">
                        <div class="form-group col-md-6">
                         
                           <select id="inputState" class="form-control">
                              
                              <option>Select Region</option>
                              {
                                 this.state.regions.map((region)=>{
                                    console.log(region.region_arabic_name);
                                    <option key={region.region_id} value={region.region_id}>{region.region_english_name}</option>
                                 })
                              }
                           <option selected>اسم المحافظة</option>
                              <option>...</option> 
                           </select>
                        </div>

                     </div> */}

                     <div className="form-row">
                        <div className=" form-group col-md-4">
                           <label><IntlMessages id="form.region" /></label>
                           {/* <select name="regionId" id="selectRegion" className="form-control input-text"
                              value={this.state.regionId}
                              onChange={this.handleChange}
                           >
                              <option key="0" value="" ></option>
                              {this.state.regions.map(region => {
                                 return (
                                    <option key={region.region_id} value={region.region_id}> {region.region_arabic_name}   </option>
                                 )
                              })
                              }
                           </select> */}
                           <Multiselect
                              options={this.state.regions}
                              selectedValues={this.state.selectedValue}
                              onSelect={this.onSelect}
                              onRemove={this.onRemove}
                              displayValue="region_arabic_name"
                              placeholder="ابحث هنا"
                              closeIcon="close"
                           />
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.regionError}
                           </div>
                        </div>
                        <div className="form-group col-md-4">
                           <label><IntlMessages id="widgets.endDate" /></label>
                           <input className="form-control" type="date" defaultValue={this.state.expiryDate} id="example-date-input" name="expiryDate" onChange={this.handleChange} />
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.expiryDateError}
                           </div>
                        </div>
                        <div className="form-group col-md-2">
                           <label><IntlMessages id="form.bundleNumber" /></label>
                           {/* <input type="text" className="form-control" name="bundleNumber" onChange={this.handleChange} /> */}
                           <span>{this.state.bundleNumber}</span>
                        </div>
                     </div>

                     <div className="form-row">
                        <div className="form-group col-md-4">

                           <label><IntlMessages id="form.cardsByingPrice" />: {this.state.formatePurchasePrice == 0 ? '' : nf.format(this.state.formatePurchasePrice)}</label>
                           <input type="number" className="form-control" id="inputBuyingCards" value={this.state.purchasePrice} name="cardsBuyingPrice" onChange={this.handleNewPrices} />
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.cardsByingPriceError}
                           </div>
                        </div>
                        <div className="form-group col-md-4">
                           <label><IntlMessages id="form.cardsSellPrice" />: {this.state.formateSellPrice == 0 ? '' : nf.format(this.state.formateSellPrice)}</label>

                           <input type="number" className="form-control" id="inputSellCards" name="cardsSellPrice" value={this.state.sellPrice} onChange={this.handleNewPrices} />
                           {/* {console.log(this.state)} */}
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.cardsSellPriceError}
                           </div>
                        </div>
                        <div className="form-group col-md-2">
                           <label><IntlMessages id="form.currency" /></label>
                           <span>{this.state.Currency}</span>
                           {/* <input type="text" class="form-control controls readonly" id="inputZip" placeholder="دينار العراقى" readonly="readOnly" /> */}
                           {/* <small class="text-muted">تصنيف العملة</small> */}
                        </div>
                     </div>
                     <div style={{ color: 'red', fontSize: 12 }}>
                        {this.state.checkPrice}
                     </div>
                     <div className="md-form md-outline">
                        <label><IntlMessages id="widgets.note" /></label>
                        <textarea id="form75" className="md-textarea form-control" rows="3" name="notes" onChange={this.handleChange}></textarea>
                     </div>

                     {/* <div className="form-row container" style= {{marginTop: "18px"}}>
                        <div className="form-group col-md-6">
                           <div class="form-check">
                              <input class="form-check-input" type="radio" name="oneColumn" value= "one" id="flexRadioDefault1" />
                              <label class="form-check-label" for="flexRadioDefault1">
                                 يحتوى الملف على عمود واحد فقط Serial ثم , ثم PIN
                        </label>
                           </div>
                           <div class="form-check">
                              <input class="form-check-input" type="radio" name="twoColumn" value="two" id="flexRadioDefault2" checked />
                              <label class="form-check-label" for="flexRadioDefault2">
                                 يحتوى الملف على عمودين الاول Serial و الثانى PIN
                        </label>
                           </div>
                        </div>
                        <div className="form-group col-md-6">
                        <div style={{ color: 'red', fontSize: 14 }}>
                              شكل الملف اكسيل او txt الرقم التسلسلى ثم , ثم ال PIN بدون اى مسافات وبدون عناويين. اى بيانات اخرى فى الملف لن يتم قراءتها وليس من الضرورى ازالتها.
                              </div>
                        </div>
                     </div> */}


                     <div className="form-group col-md-6">

                     </div>
                     <form className="form-row">
                        <div className="input-group col-md-4">
                           <div className="custom-file">
                              <input type="file" className="custom-file-input" style={{ minWidth: "max-content" }} id="inputGroupFile01"
                                 aria-describedby="inputGroupFileAddon01" onChange={(e) => {
                                    const file = e.target.files[0];
                                    file.type == "text/plain" ? this.readTextFile(e) : this.FileNotFormatted(file)
                                 }} />
                              <label className="custom-file-label label-width"><IntlMessages id="form.uploadFile" /></label>
                           </div>
                           <div>
                              {
                                 this.state.isUploaded ?
                                    <div className="col-md-2">
                                       <label className="label-width">ملف تحميل كارتات الشركات.txt</label>
                                       <label className="label-width">{this.state.fileSize} KB</label>
                                    </div>
                                    :
                                    <div></div>
                              }
                              <div style={{ color: 'red', fontSize: 12 }}>
                                 {this.state.uploadFileError}
                              </div>
                           </div>
                        </div>
                        <div className="form-group col-md-6">
                           {/* <div style={{ color: 'red', fontSize: 14 }}>
                              شكل الملف اكسيل او txt الرقم التسلسلى ثم , ثم ال PIN بدون اى مسافات وبدون عناويين. اى بيانات اخرى فى الملف لن يتم قراءتها وليس من الضرورى ازالتها.
                              </div> */}
                        </div>
                        <div className="form-group col-md-6">
                           {
                              this.state.isUploaded ?
                                 <div>
                                    <label className="label-width" ><IntlMessages id="form.cardsCount" />: {this.state.bundleCount}</label>
                                    {
                                       this.state.sheetName == '' ? '' : <label className="label-width"><IntlMessages id="form.fileName" />: {this.state.sheetName}</label>
                                    }

                                 </div>
                                 :
                                 <div></div>
                           }
                        </div>


                     </form>

                     <div className="row mb-4">
                        <div className="form-group col-md-6">
                           <div className="container">
                              <button type="button" className="btn btn-primary btn-margin" onClick={this.loadModel}><IntlMessages id="form.upload" /></button>
                           </div>

                        </div>
                        <div className="form-group col-md-4">
                           <div className="container">
                              <img width={700} height={700} src={image1} className="img-fluid" alt="..." />
                           </div>

                        </div>
                        {/* <div className="form-group col-md-4">
                           <div className="container">
                              <img width={250} height={250} src={image} className="img-fluid" alt="..." />
                           </div>

                        </div> */}

                     </div>

                  </form>

                  {/* {
                     console.log("state", this.state.loadCheckModel),
                     this.state.loadCheckModel == true ?
                        <Modal visible={this.state.loadCheckModel} style={{ padding: "100px" }} effect="fadeInUp" onClick={() => {
                            console.log("loaderdtate", this.state.loader);
                        }}>

                        <div className="container">
                           <Spinner color="secondary" />
                        </div>


                         </Modal>
                        :
                        null
                  } */}

                  {
                     this.state.loadCheckModel == true ?
                        <Modal visible={this.state.loadCheckModel} effect="fadeInUp" onClickAway={this.closeModel}>
                           <div className="row">
                              <div className="col-md-12">
                                 <DataChcekModel {...this.state} openNextModel={this.closeCurrentModel} onSelectCLose={this.closeDataCheckModel} />
                              </div>
                              <div className="col-md-4">
                              </div>
                           </div>
                        </Modal>
                        :
                        null
                  }

                  {
                     this.state.loadModel == true ?
                        <Modal visible={this.state.loadModel} effect="fadeInUp" onClickAway={this.closeModel}>
                           <div className="row">
                              <div className="col-md-12">
                                 <PrintInvoice {...this.state} onSelectCLose={this.closeModelFromChild} onSelectLoad={this.openPrintModel} ref={el => (this.componentRef = el)} />
                              </div>
                              <div className="col-md-4">
                              </div>
                           </div>
                        </Modal>

                        // <Modal visible={this.state.loadModel} width="550" height="340" effect="fadeInUp" onClickAway={this.closeModel}>
                        //    <div className="modalstyleInvoice" Style="padding-bottom: 233px;">
                        //       <div class="modal-header">
                        //          <h5 class="modal-title" id="exampleModalCenterTitle">فاتورة تحميل الكارتات</h5>

                        //          <a type="button" class="close" aria-label="Close" onClick={this.closeModel}>
                        //             <span aria-hidden="true">&times;</span>
                        //          </a>
                        //       </div>
                        //       <div class="modal-body">
                        //          <h3 class="modal-title"> رقم الفاتورة {this.state.bundleNumber}</h3>
                        //          <blockquote class="blockquote">
                        //             <span class="mb-0"> لقد قمت بتحميل عدد كارتات {this.state.bundleCount} لشركة {this.state.companyName}</span>
                        //             <br></br>
                        //             <span> يوجد بالسيستم {this.state.oldCardsCount}, </span>
                        //             <span> ليصبح العدد الكلى للكارتات {this.state.bundleCount + this.state.oldCardsCount}</span>
                        //             <br></br>
                        //             <span> سعر البيع للكارت {
                        //                this.state.sellPriceChange == true ?
                        //                   this.state.cardsSellPrice :
                        //                   this.state.sellPrice
                        //             }, </span>
                        //             <span> سعر الشراء للكارت {
                        //                this.state.purchasePriceChange == true ?
                        //                   this.state.cardsBuyingPrice :
                        //                   this.state.purchasePrice
                        //             }</span>
                        //             <br></br>
                        //             <span> لتصبح القيمة الكلية للفاتورة {nf.format(this.state.purchasePrice * this.state.bundleCount)}</span>
                        //          </blockquote>
                        //       </div>
                        //       <div class="modal-footer">
                        //          <button type="button" class="btn btn-secondary" onClick={this.closeModel}>اغلاق</button>
                        //          <button type="button" class="btn btn-primary" onClick={this.handleSubmit}>تحميل</button>
                        //       </div>
                        //    </div>
                        // </Modal>
                        :
                        null
                  }


                  {
                     // console.log("check on printed", this.state),
                     this.state.ModelToPrint == true ?
                        <Modal visible={this.state.ModelToPrint} width="550" effect="fadeInUp" onClickAway={() => { this.closeModel(); this.resetState() }}>
                           <div className="modal-content">
                              <div className="modal-header">
                                 <h5 className="modal-title">لقد قمت بتحميل الكارتات بنجاح</h5>
                                 <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                              </div>
                              <div className="modal-body">
                                 <p>هل ترغب فى طباعة فاتورة؟</p>
                              </div>
                              <div className="modal-footer">
                                 <button type="button" className="btn btn-secondary" onClick={() => { this.closeModel(); this.resetState() }}>اغلاق</button>
                                 <div style={{ display: "none" }}>
                                    <Invoice id="print-invoice" {...this.state} onSelectCLose={this.closeModelFromChild} ref={el => (this.componentRef = el)} />
                                 </div>
                                 <ReactToPrint content={() => this.componentRef}>

                                    <PrintContextConsumer>
                                       {({ handlePrint }) => (
                                          // <div className="container" style={{
                                          //     marginRight: "494px",
                                          //     maxWidth: "max-content", position: "fixed",
                                          //     marginTop: "47px"
                                          // }}>
                                          <button className="btn btn-danger" onClick={handlePrint}><IntlMessages id="form.print this out!" /></button>

                                          // </div>
                                       )}
                                    </PrintContextConsumer>
                                 </ReactToPrint>
                              </div>
                           </div>
                           {/* <div className="modalstyleInvoice" Style="padding-bottom: 233px;">
                              <div class="modal-header">
                                 <h5 class="modal-title" id="exampleModalCenterTitle"> تحميل الكارتات</h5>

                                 <a type="button" class="close" aria-label="Close" onClick={this.closeModel}>
                                    <span aria-hidden="true">&times;</span>
                                 </a>
                              </div>
                              <div class="modal-body">
                                 <h3 class="modal-title"> رقم الفاتورة {this.props.bundleNumber}</h3>
                                 <blockquote class="blockquote">
                                    <span class="mb-0"> لقد قمت بتحميل عدد كارتات {this.state.bundleCount} لشركة {this.state.companyName}</span>
                                    <br></br>
                                    <span> يوجد بالسيستم {this.state.oldCardsCount}, </span>
                                    <span> ليصبح العدد الكلى للكارتات {this.state.bundleCount + this.state.oldCardsCount}</span>
                                    <br></br>
                                    <span> سعر البيع للكارت {
                                       this.state.sellPriceChange == true ?
                                          this.state.cardsSellPrice :
                                          this.state.sellPrice
                                    }, </span>
                                    <span> سعر الشراء للكارت {
                                       this.state.purchasePriceChange == true ?
                                          this.state.cardsBuyingPrice :
                                          this.state.purchasePrice
                                    }</span>
                                    <br></br>
                                    <span> لتصبح القيمة الكلية للفاتورة {nf.format(this.state.purchasePrice * this.state.bundleCount)}</span>
                                 </blockquote>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" onClick={this.closeModel}>اغلاق</button>
                                 <button type="button" class="btn btn-primary" onClick={this.handleSubmit}>تحميل</button>
                              </div>
                           </div> */}
                        </Modal>
                        :
                        null
                  }
               </RctCollapsibleCard>

               {/* <ReactToPrint content={() => this.componentRef}>
                  <PrintContextConsumer>
                     {({ handlePrint }) => (
                        <React.Fragment>
                           <button onClick={handlePrint} style={{ textAlign: "center", padding: "20px", margin: "20px", borderRadius: "15px", fontSize: "17px", backgroundColor: "#599A5F", color: "white" }}>
                              <i class='fa fa-print fa-1x' style={{ color: "white !important", marginRight: "20px !important" }} /><IntlMessages id="form.print this out!" /> </button>
                           <hr />
                        </React.Fragment>
                     )}
                  </PrintContextConsumer>
               </ReactToPrint> */}




               {/* <div className="container" style={{ textAlign: "center" }} ref={el => (this.componentRef = el)}>
                  <img src='' alt="lenkit-logo" style={{ width: "20%", padding: "17px", marginTop: "20px", marginRight: "10px" }} />
                  <span style={{ top: "50px", padding: "50px" }}>
                     <Barcode value={1} width="1" height="100" textAlign="center"
                        textPosition="bottom" style={{ width: "18%", padding: "17px", marginLeft: "50px" }} displayValue={true} />
                  </span>
                  <QRCode size={126} responsive={true} value={1} />
                  <PrintInvoice
                     shipment_id={1}
                     barcode={12}
                     invoice_number={12}
                  />
               </div> */}
            </div>

         </div>

      )
   }
}
export default Dropzone;
