/**
 * add supplier
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 import { RctCard, RctCardContent } from 'Components/RctCard';
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import MUIDataTable from "mui-datatables";
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 
 
 
 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         cost_center_name: '',
         costcetners:[],
         disabled: false,
         hiddenStatusForm: true,
         hiddenDebitLimitForm: true,
         businessUnitNameAr:'',
         businessUnitNameEn:'',
         business_unit_id:'',
         updateflag:true
     });
 
  
     const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
     };
    
     useEffect(() => {
        // all suppliers
        axios.get('https://accbackend.alaimtidad-itland.com/all-business-units',USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            else{
      
                setState({
                    ...state,
                   costcetners: response.data.businessUnits,
                   
                })
            }
        })
        .catch(error => {
   
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
    }, []);
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
 
     const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                  maxHeight: 'unset',
                  overflowX: 'unset',
                  overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }       
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
      const onSubmitUPDATE = e => {
 

    axios({url:'https://accbackend.alaimtidad-itland.com/update-business-unit',method:'post',data:{

        businessUnitNameAr:state.businessUnitNameAr,
        businessUnitNameEn:state.businessUnitNameEn,
        businessUnitID:state.business_unit_id,

      
    },
    headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
        if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
            cookies.remove('UserToken', { path: '/' })
            window.location.href = "/signin";
        }
  

        else{
  
          toast.success(<IntlMessages id="updatedsuccess" />, {
              position: "top-center",
              autoClose: 4000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true
          });
          setState({ ...state, disabled: true })
          setTimeout(function () { location.reload()}, 3000)
        }

     
      }).catch(error => {

          
         if (error.response.status === 409) {
              toast.error("هذا الاسم مسجل من قبل", {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
              }); 
          }
         else if (error.response.status === 429) {
            toast.error(error.response.data, {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            }); 
        }
      });
 
 }

 const onClearStatusClicked = () => {
    setState({
        ...state,
        updateflag:true,
        cost_center_name: '',
        disabled: false,
        hiddenStatusForm: true,
        hiddenDebitLimitForm: true,
        businessUnitNameAr:'',
        businessUnitNameEn:'',
        business_unit_id:'',
        
    })
};
     const onSubmit = e => {
 

        axios({url:'https://accbackend.alaimtidad-itland.com/create-business-unit',method:'post',data:{

            businessUnitNameAr:state.businessUnitNameAr,
            businessUnitNameEn:state.businessUnitNameEn,
        

          
        },
        headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
      

            else{
           
              toast.success(<IntlMessages id="addedsuccessfully" />, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
              });
              setState({ ...state, disabled: true })
              setTimeout(function () { location.reload()}, 3000)
            }

         
          }).catch(error => {
    
              
             if (error.response.status === 409) {
                  toast.error("هذا الاسم مسجل من قبل", {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  }); 
              }
             else if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
          });
     
     }
 
     const { match } = props;

     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>مراكز التكلفة</title>
                         <meta name="description" content="إضافة  مركز تكلفة" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.costcenter" />} match={match} />
                     <div className="row">
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.costcenter" />}>


{(state.updateflag == true) ?  


<form onSubmit={handleSubmit(onSubmit)}>
<div className="form-row">
    <div className="form-group col-md-4">
        <label><IntlMessages id="form.costcenterName" /></label>
        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
        borderWidth: "0px 2px 2px 0px",
        lineHeight: "0px",
        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                        }}>
                                                                
        <input type="text" className="form-control" name="businessUnitNameAr" defaultValue={state.businessUnitNameAr}
       //  value={state.selected_user[1]}
            onChange={handleFields}
            ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
    </div>
        <span className="errors">
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'required' &&
                <IntlMessages id="form.requiredError" />}
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'minLength' &&
                <IntlMessages id="form.minLengthError" />}
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'pattern' &&
                <IntlMessages id="form.lettersOnlyError" />}
        </span>
    </div>


    <div className="form-group col-md-4">
        <label><IntlMessages id="form.costcenterNameeng" /></label>
        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
        borderWidth: "0px 2px 2px 0px",
        lineHeight: "0px",
        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                        }}>
                                                                
        <input type="text" className="form-control" name="businessUnitNameEn" defaultValue={state.businessUnitNameEn}
       //  value={state.selected_user[1]}
            onChange={handleFields}
            ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
    </div>
        <span className="errors">
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'required' &&
                <IntlMessages id="form.requiredError" />}
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'minLength' &&
                <IntlMessages id="form.minLengthError" />}
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'pattern' &&
                <IntlMessages id="form.lettersOnlyError" />}
        </span>
    </div>
</div>

{(state.disabled === false) ? 
    <button type="submit" className="btn-margin" style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
        <IntlMessages id="form.add" />
    </button> : 
    <button type="submit" className="btn-margin" disabled={true} style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
        <IntlMessages id="form.add" />
    </button>
}
</form>


:

<form onSubmit={handleSubmit(onSubmitUPDATE)}>
<div className="form-row">
    <div className="form-group col-md-4">
        <label><IntlMessages id="form.costcenterName" /></label>
        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
        borderWidth: "0px 2px 2px 0px",
        lineHeight: "0px",
        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                        }}>
                                                                
        <input type="text" className="form-control" name="businessUnitNameAr" defaultValue={state.businessUnitNameAr}
       //  value={state.selected_user[1]}
            onChange={handleFields}
            ref={register({ required: true, minLength: 3, pattern: /^[ء-ي_ ]+$/i, })} />
    </div>
        <span className="errors">
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'required' &&
                <IntlMessages id="form.requiredError" />}
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'minLength' &&
                <IntlMessages id="form.minLengthError" />}
            {errors.businessUnitNameAr && errors.businessUnitNameAr.type === 'pattern' &&
                <IntlMessages id="form.lettersOnlyError" />}
        </span>
    </div>


    <div className="form-group col-md-4">
        <label><IntlMessages id="form.costcenterNameeng" /></label>
        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
        borderWidth: "0px 2px 2px 0px",
        lineHeight: "0px",
        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                        }}>
                                                                
        <input type="text" className="form-control" name="businessUnitNameEn" defaultValue={state.businessUnitNameEn}
       //  value={state.selected_user[1]}
            onChange={handleFields}
            ref={register({ required: true, minLength: 3, pattern: /^[A-Za-z_ ]+$/i, })} />
    </div>
        <span className="errors">
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'required' &&
                <IntlMessages id="form.requiredError" />}
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'minLength' &&
                <IntlMessages id="form.minLengthError" />}
            {errors.businessUnitNameEn && errors.businessUnitNameEn.type === 'pattern' &&
                <IntlMessages id="form.lettersOnlyError" />}
        </span>
    </div>
</div>

{(state.disabled === false) ? 
                             <button type="submit" className="btn btn-warning">
                             <IntlMessages id="form.update" />
                             </button> : 
                             <button type="submit" className="btn btn-warning" disabled={true} >
                             <IntlMessages id="form.update" />
                             </button>
                             }
 
                         <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                             <IntlMessages id="form.clear" />
                         </button>
</form>

}


 
 
                         </RctCollapsibleCard>
                     </div>
                 
                 


{/* ----------------------------------------Tableeeeeeeeeeeeeeeeeee------------------------------------ */}


 <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                    data={state.costcetners}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "business_unit_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                 label: <IntlMessages id="form.costcenterName" />,
                                                 name:  "business_unit_name",
                                             },
                                             {
                                                label: <IntlMessages id="form.costcenterNameeng" />,
                                                name:  "business_unit_name_en",
                                            },
                                             
                                             {
                                                 label: <IntlMessages id="form.dataTime" />,
                                                 name:  "creation_date"
                                             },
                                          
                                          
                                             {
                                                 label: <IntlMessages id="form.update" />,
                                                 name: "",
                                                 options: {
                                                     filter: true,
                                                     sort: false,
                                                     empty: true,
                                                     print: false,
                                                     customBodyRender: (value, tableMeta, updateValue) => {
                                                     return (
                                                         <React.Fragment>
                                                             <button type="button" className="btn btn-warning" 
                                                             onClick={() => {
                                                                
                                                                    setState({
                                                                     ...state, 
                                                                     updateflag: false,
                                                                     business_unit_id:tableMeta.rowData[0],
                                                                     businessUnitNameAr:tableMeta.rowData[1],
                                                                     businessUnitNameEn:tableMeta.rowData[2],
                                                                     
                                                               
                                                                    });
                                                             }}>
                                                             <IntlMessages id="form.update" />
                                                             </button>
                                                         </React.Fragment>
                                                     );
                                                 }
                                                 }
                         
                                             },
                                            
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>

</div>



: (
 
    history.push("/access-denied")
)
}



         </React.Fragment>
         
     )
 }