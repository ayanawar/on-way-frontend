import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();


export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		extra_fees_percentage: '',
		transfer_fees_id: '',
		disabled: false,
		hidden: true,
		extra_fees: []
	});


	useEffect(() => {
			axios.get('http://localhost:8000/allextrafeeses',USER_TOKEN).then(response2 => {
				if(response2.data == "Token Expired" || response2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else{
				// console.log("jjj ",response2.data);
				// response.data.regions.splice(0, 1);
					setState({
						// regions: response.data.regions, 
						extra_fees: response2.data.result,
						disabled: false,
						hidden: true,
					})
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
	}, []); 


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const onClearClicked = () => {
		setState({...state, extra_fees_percentage: '',transfer_fees_id: '', hidden: true, })
	}
	const onSubmit = e => {
		// if (state.region_from_id === state.region_to_id) {
		// 	toast.error(<IntlMessages id="form.extraFeesError" />, {
		// 		position: "top-center",
		// 		autoClose: 4000,
		// 		hideProgressBar: false,
		// 		closeOnClick: true,
		// 		pauseOnHover: true,
		// 		draggable: true
		// 	});
		// } else {
			axios.post(`http://localhost:8000/updateexrafees`, {extra_fees_percentage: state.extra_fees_percentage,
			transfer_fees_id: state.transfer_fees_id },USER_TOKEN
				).then(res =>{
					if(res.data == "Token Expired") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else if(res.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else {
						toast.success(<IntlMessages id="form.extraFeesSuccess" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({ ...state, disabled: true })
						setTimeout(function () { location.reload()}, 3000)
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}
				});
		// }
	}

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "600px"
	};

	const { match } = props;
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
			<title>رسوم تحويل أرصدة الامتداد</title>
			<meta name="description" content="رسوم تحويل أرصدة الامتداد" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.basic" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.basic" />}>
			{(state.hidden == true) ? <React.Fragment></React.Fragment>
                : 
				<form onSubmit={handleSubmit(onSubmit)}>
					{/* <h3 className="text-center"><IntlMessages id="sidebar.basicDescription" /></h3> */}
					<div className="form-row">
						<div className="form-group col-md-4">
							<label><IntlMessages id="form.fees" /></label>
							<input type="tel" className="form-control" name="extra_fees_percentage"
									onChange={handleFields} value={state.extra_fees_percentage}
									ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
							
							<span className="errors">
								{errors.extra_fees_percentage && errors.extra_fees_percentage.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.extra_fees_percentage && errors.extra_fees_percentage.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
							</span>
						</div>
					</div>
					{/* <div className="form-row">
						<div className="form-group  dropdown col-md-6">
							<label><IntlMessages id="form.from" /> - <IntlMessages id="form.region" /> -</label>
							<select name="region_from_id" className="form-control input-text" 
									onChange={handleFields} ref={register({ required: true })}>
									<option key="0" value="">برجاء الاختيار</option>
							{state.regions.map(region => {
								return (
									<option key={region.region_id} value={region.region_id}>
										{region.region_arabic_name}
									</option>
								)
							})
							}
							</select>
							<span className="errors">
								{errors.region_from_id && errors.region_from_id.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
						</div>
						<div className="form-group  dropdown col-md-6">
							<label><IntlMessages id="form.to" /> - <IntlMessages id="form.region" /> - </label>
							<select name="region_to_id" className="form-control input-text" 
									onChange={handleFields} ref={register({ required: true })}>
									<option key="0" value="">برجاء الاختيار</option>
							{state.regions.map(region => {
								return (
									<option key={region.region_id} value={region.region_id}>
										{region.region_arabic_name}
									</option>
								)
							})
							}
							</select>
							<span className="errors">
								{errors.region_to_id && errors.region_to_id.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
						</div>
					</div> */}

					{(state.disabled === false) ? 
						<button type="submit" className="btn btn-primary">
							<IntlMessages id="form.update" />
						</button> : 
						<button type="submit" className="btn btn-primary" disabled={true}>
							<IntlMessages id="form.update" />
						</button>
					}
					<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
						<IntlMessages id="form.clear" />
					</button>
				</form>
			}
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							// title={<IntlMessages id="sidebar.basic" />}
							data={state.extra_fees}
							columns={[
									{
										label: "id",
										name:  "transfer_fees_id",
										options: {
											display: "none"
										}
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "sender_type" 
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "reciever_type",
									},
									{
										label: <IntlMessages id="form.existsFees" />,
										name:  "same_region_flag_ar" 
									},
									{
										label: <IntlMessages id="form.fees" />,
										name:  "extra_fees_percentage",
									},
									{
										label: <IntlMessages id="form.updateDataTime" />,
										name:  "update_datetime" 
									},
									{
										label: <IntlMessages id="form.update" />,
										name: "",
										options: {
											filter: true,
											sort: false,
											empty: true,
											customBodyRender: (value, tableMeta, updateValue) => {
												return (
													<React.Fragment>
														<button type="button" className="btn btn-primary" 
														onClick={() => {
															// console.log(tableMeta.rowData[0])
															setState({...state , 
																transfer_fees_id:tableMeta.rowData[0], 
																extra_fees_percentage: tableMeta.rowData[4],
																hidden: false,
															})
															}}>
															<IntlMessages id="form.update" />
														</button>
													</React.Fragment>
												);
											}
										}
		
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
	</div>
	: (
			
		history.push("/access-denied")
	   )
	} 
	</React.Fragment>
	)
	}