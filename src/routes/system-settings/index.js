/**
 * Tables Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";
// async components
import {
    AsyncBasicTableComponent,
    AsyncDataTableComponent,
    AsyncResponsiveTableComponent,
    AsyncDisableSystemComponent,
    AsyncResetPasswordComponent,
    AsyncSystemUser,
    AsyncSystemUserUpdate,
    Asynccostcenter,

} from 'Components/AsyncComponent/AsyncComponent';

const Pages = ({ match }) => (
    <div className="content-wrapper">
        <Helmet>
            <title>إعدادات النظام</title>
            <meta name="description" content="إعدادات النظام" />
        </Helmet>
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/extra-transfer-fees`} />
            <Route path={`${match.url}/extra-transfer-fees`} component={AsyncBasicTableComponent} />
            <Route path={`${match.url}/display-regions-and-areas`} component={AsyncDataTableComponent} />
            <Route path={`${match.url}/determine-the-exchange-rate`} component={AsyncResponsiveTableComponent} />
            <Route path={`${match.url}/disable-system`} component={AsyncDisableSystemComponent} />
            <Route path={`${match.url}/reset-password`} component={AsyncResetPasswordComponent} />
            <Route path={`${match.url}/system-user`} component={AsyncSystemUser} />
            <Route path={`${match.url}/system-user-update`} component={AsyncSystemUserUpdate} />
            <Route path={`${match.url}/costcenter`} component={Asynccostcenter} />
        </Switch>
    </div>
);

export default Pages;
