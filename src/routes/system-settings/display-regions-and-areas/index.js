import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
        regions: [],
        areas: [],
        companyName: '',
        sellPriceTable: '',
        categoryId: '',  
        companyId: '',
        sellPrice: '',
        disabled: false,
        hidden: true,
    });
    
    const nf = new Intl.NumberFormat();

	useEffect(() => {
        axios.get(`http://localhost:8000/allregions`,USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else{
                response.data.regions.splice(0, 1);
                setState({
                    ...state,
                    regions: response.data.regions,
                    disabled: false,
                    hidden: true,
                })
            }
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
            MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontSize: "30px",
					backgroundColor: "#599A5F",
					fontFamily: "'Almarai', sans-serif"
				}
            },
            MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
    })
  
    const handleRowClick = (rowData, rowIndex) => {
        axios.post(`http://localhost:8000/aresbyregionid`,{"regionId":rowData[0]},USER_TOKEN).then(res => {
			if(res.data.areas.length == 0){
				toast.error('لا يوجد حاليا مناطق متوفرة فى هذة المحافظة', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ 
					...state,
					areas: res.data.areas,
					companyName: rowData[1]
				})

			}else{
			setState({ 
				...state,
				areas: res.data.areas,
				// companyName: res.data.message[0].company_name_ar
				companyName: rowData[1]
			})
		}
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        }); 
    };

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 20,
		rowsPerPageOptions: [5,10,20,32,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        download: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
    };
    
    const options2 = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 20,
		rowsPerPageOptions: [5,10,20,32,50,100],
		responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: 'none',
        // onRowClick: handleRowClick,
        sort: false,
        viewColumns: false,
        download: false,
        selectableRowsHeader: false,
        fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "630px"
	};

	const { match } = props;
	// console.log(state);
	return (
        <React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>عرض المحافظات والمناطق</title>
 				<meta name="description" content="عرض المحافظات والمناطق" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.dataTable" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.dataTable" />}>
                    <div className="form-row mt-4">
                        <div className="form-group col-md-4 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.regions}
                                        columns={[
                                            {
                                                name:  "region_id",
                                                options: {
                                                display: "none",
                                                filter: false
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="form.region" />,
                                                name:  "region_arabic_name",
                                            },
											{
												label: 'المحافظة باللغة الإنجليزية',
                                                name:  "region_english_name",
											}
                                            ]}
                                        options={options}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                        <div className="form-group col-md-8 text-center">
                            {/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
                            <RctCollapsibleCard fullBlock>
                                <MuiThemeProvider theme={getMuiTheme()}>
                                    <MUIDataTable
                                        data={state.areas}
                                        title={state.companyName}
                                        columns={[
                                            {
                                                name:  "area_id",
                                                options: {
                                                    display: "none",
                                                    filter: false
                                                }
                                            },
                                            {
                                                label: <IntlMessages id="form.area" />,
                                                name:  "area_arabic_name",
                                            },
                                            {
                                                label: 'المنطقة باللغة الإنجليزية',
                                                name:  "area_english_name"
                                            },
                                            ]}
                                        options={options2}
                                    />
                                </MuiThemeProvider>
                            </RctCollapsibleCard>
                        </div>
                    </div>
			    </RctCollapsibleCard>
		    </div>
		</div>
        : (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}
