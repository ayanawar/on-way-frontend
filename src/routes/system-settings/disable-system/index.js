import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-awesome-modal';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();

export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_id: '',
		user_type_id: '',
		systemStatus: '',
		currentSystemStatus: '',
		disabled: false,
		modal: false,
	});

	const nf = new Intl.NumberFormat();
	useEffect(() => {
		axios.get('http://localhost:8000/alaimtidad',USER_TOKEN).then(response => {
				if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
					// console.log(response.data.message[0].system_on);
					setState({
						currentSystemStatus: response.data.message[0].system_on_ar,
						// categories: response.data.message,
						// cards: response2.data.message,
						user_id: localStorage.getItem("userIdInUsers"),
						user_type_id: localStorage.getItem("user_type_id"),
						disabled: false,
						modal: false,
					})
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
	}, []); 


	const handleFields = ({ target }) => {
		// if(state.cardsnumber % 1000 != 0) {
			setState({ ...state, [target.name]: target.value,  });
			// console.log(target);
		// }
		
	};

	const closeModel = () => {
		setState({
			...state,
		   	modal: false
		});
	}


	const openPrintModel = () => {
		setState({
			...state,
		   modal: true
		})
		// console.log(state);
	}

	const resetState = () => {
		setState({
			...state,
			disabled: false,
			modal: false,
		})
	}

	const Token = cookies.get('UserToken');
	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updatesystemstatus', data: {
				"systemStatus": state.systemStatus,
				"userTypeId": state.user_type_id,
				"user_id": state.user_id,
				"user_password": state.user_password
			},headers: { "x-access-token": `${Token}`  }
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				
				toast.success('تم تغيير الحالة بنجاح', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true, modal: false, })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			} else {
				toast.error('كلمة السر غير صحيحة غير مصرح لك بتغيير حالة النظام', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, modal: false, })
			}
		});	
	// }
	}

	const { match } = props;
	// console.log(state.systemStatus);
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
			<title>إيقاف النظام</title>
 			<meta name="description" content="إيقاف النظام" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.disableSystem" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.disableSystem" />}>
				<form onSubmit={handleSubmit(openPrintModel)}>
					<div className="form-row">
						<div className="form-group  dropdown col-md-4">
							<h2>حالة النظام الآن : 
								{(state.currentSystemStatus === "مفعل") ? 
								<span className="btn btn-success m-2 p-2" style={{pointerEvents: "none"}}>
									{state.currentSystemStatus}
								</span> 
								:
								<span className="btn btn-danger m-2 p-2" style={{pointerEvents: "none"}}>
									{state.currentSystemStatus}
								</span>
								}
							</h2>	
						</div>
						<div className="form-group  dropdown col-md-4">
							<label>حالة النظام</label>
							<select name="systemStatus" className="form-control input-text" 
									onChange={handleFields} ref={register({ required: true })}>
									<option key="0" value="">برجاء اختيار حالة النظام</option>
									<option key="1" value="1">مفعل</option>
									<option key="2" value="0">موقوف</option>
							</select>
							<span className="errors">
								{errors.systemStatus && errors.systemStatus.type === 'required' &&
									<IntlMessages id="form.requiredOptionError" /> }
							</span>
						</div>
					</div>
					
					{(state.disabled === false) ? 
						// <button onClick={openPrintModel}> testtttt</button>
						<button type="submit" className="btn btn-primary">
							تغيير
						</button> : 
						<button type="submit" className="btn btn-primary" disabled={true}>
						 تغيير
						</button>
					}
				</form>
				

				{state.modal == true ?
					<Modal visible={state.modal} width="550" effect="fadeInUp" onClickAway={()=>{closeModel(); resetState()}}>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">تغيير حالة النظام</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							{/*  */}
							<form onSubmit={handleSubmit(onSubmit)}>
							<div className="modal-body">
								<p>برجاء إدخال كلمة السر للتأكيد</p>
								<input
                                       // value={user_password}
                                       type="Password"
                                       name="user_password"
                                       className="form-control p-3"
                                       onChange={handleFields}
                                       style={{ borderRadius: "15px", fontSize:"17px" }}
                                       ref={register({ required: true 
                                          , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/ 
                                       })}
                                    />
									<br />
                                    <span className="errors">
                                       {errors.user_password && errors.user_password.type === 'required' &&
                                          <IntlMessages id="form.requiredError" /> }
                                       {errors.user_password && errors.user_password.type === 'pattern' &&
                                          <IntlMessages id="form.passwordMinLengthError" /> }
                                    </span>
							</div>
							<div className="modal-footer">
								{(state.disabled === false) ? 
									<button type="submit" className="btn btn-primary">
										تغيير
									</button> : 
									<button type="submit" className="btn btn-primary" disabled={true}>
									تغيير
									</button>
								}
								<button type="button" className="btn btn-secondary" onClick={()=>{closeModel(); resetState()}}>اغلاق</button>
								<div style={{display: "none"}}>
								{/* <Invoice id="print-invoice" {...state} onSelectCLose={closeModelFromChild} ref={el => (componentRef = el)} /> */}
								</div>
								
							</div>
							</form>
						</div>	
					</Modal>
                        :
                        null
                }
			</RctCollapsibleCard>

		</div>
		{/* <div className="row">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							data={state.cards}
							columns={[
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_bundle_total_value" 
									},
									{
										label: <IntlMessages id="form.createdBy" />,
										name:  "admin_name",
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "virtual_mony_bundle_date_time" 
									},
									{
										label: <IntlMessages id="form.cardsCount" />,
										name:  "virtual_mony_bundle_count" 
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name: "category_value"
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div> */}
	</div>
	: (
			
		history.push("/access-denied")
	   )
	} 
	   </React.Fragment>
	)
	}