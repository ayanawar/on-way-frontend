/**
 * add supplier
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         user_type_id: '',
         first_name: '',
         middle_name: '',
         last_name: '',
         security_level_id:'',
         user_region_id: '',
         user_area_id: '',
         user_email: '',
         user_password: '',
         user_phonenumber: '',
         levels: [],
         currency: [],
         disabled: false,
         types: [],
         regions: [],
         areas: [],
         usertypes:[],
     });
 
 
     useEffect(() => {
        axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
                axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
                    axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
                       axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                        axios.get('https://accbackend.alaimtidad-itland.com/user-types ', USER_TOKEN).then(response5 => {        
                 // console.log("state", state);
                 if (response.data == "Token Expired" || response.data == "Token UnAuthorized" || response8.data == "Token Expired" || response8.data == "Token UnAuthorized") {
                     cookies.remove('UserToken', { path: '/' })
                     window.location.href = "/signin";
                 }
                 else {
                     setState({
                         ...state,
                         regions: response8.data.regions,
                         levels: response.data.levels,
                         types: response3.data.message,
                         currency:response4.data.message,
                         usertypes:response5.data,
                         disabled: false,
                        
                     })
                 }
                }).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                           position: "top-center",
                           autoClose: 4000,
                           hideProgressBar: false,
                           closeOnClick: true,
                           pauseOnHover: true,
                           draggable: true
                        }); 
                    }
                });
                }).catch(error => {
                    if (error.response.status === 429) {
                        toast.error(error.response.data, {
                           position: "top-center",
                           autoClose: 4000,
                           hideProgressBar: false,
                           closeOnClick: true,
                           pauseOnHover: true,
                           draggable: true
                        }); 
                    }
                });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
             }).catch(error => {
                 if (error.response.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     }); 
                 }
             });
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);

 
   
 
 
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
 
     const handleTwoEvents = (e) => {
		handleFields(e);
		let regionId = e.target.value;

		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						user_area_id: '',
						
					})

				}else{
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
					
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}
 

 
      
     const onSubmit = e => {
 let data = {

    user_type_id:state.user_type_id,
    first_name: state.first_name,
    middle_name: state.middle_name,
    last_name:state.last_name,
    security_level_id:state.security_level_id,
    user_region_id: state.user_region_id,
    user_area_id:state.user_area_id,
    user_email:state.user_email,
    user_password: state.user_password,
    user_phonenumber: state.user_phonenumber,
    
    }
    console.log("data",data);
        axios({url:'https://accbackend.alaimtidad-itland.com/register-user',method:'post',data:data,
         
             headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else if(res.data == "User registered successfully"){
                    console.log(res,"response");
				toast.success(<IntlMessages id="form.addAccountSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)

            }
			}).catch(error => {
                console.log(error,"error");
				if (error.response.status === 400) {
					toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				} else if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
     }
     const { match } = props;
     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>إضافة مستخدم جديد للنظام</title>
                         <meta name="description" content="إضافة مستخدم جديد للنظام" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.systemuser" />} match={match} />
                     <div className="row">
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.systemuser" />}>
                             <form onSubmit={handleSubmit(onSubmit)}>
                             <div className="form-row">
                                 <label><IntlMessages id="form.usertype" /></label>
                                 <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <select name="user_type_id" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار نوع المستخدم</option>
                                             {state.usertypes.map(user => {
                                                 return (
                                                     <option key={user.user_type_id} value={user.user_type_id}>
                                                         {user.type_ar}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         </div>
                                         <span className="errors">
                                             {errors.user_type_id && errors.user_type_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                 
                                 </div>
                                 <br/>
                                 <br/>
                                 <br/>
                                 
                                 <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.firstName" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                     <input type="text" className="form-control" name="first_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                     </div>
                                         <span className="errors">
                                             {errors.first_name && errors.first_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.first_name && errors.first_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.first_name && errors.first_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.middleName" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <input type="text" className="form-control" name="middle_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                     </div>
                                         <span className="errors">
                                             {errors.middle_name && errors.middle_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.middle_name && errors.middle_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.middle_name && errors.middle_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.lastName" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <input type="text" className="form-control" name="last_name"
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         </div>
                                         <span className="errors">
                                             {errors.last_name && errors.last_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_last_name && errors.last_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.last_name && errors.last_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                 </div>
                                 <div className="form-row">
                                 <div className="form-group col-md-6">
									  <label><IntlMessages id="form.secretdegree" /></label>
									  <div >
									  <select className="form-control input-text" name="security_level_id"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار درجة السرية</option>
										  {state.levels.map(level => {
											 return (
												 <option key={level.security_level_id} value={level.security_level_id}>
													 {level.security_level_type_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.security_level_id && errors.security_level_id.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.phoneNumber" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <input type="tel" className="form-control" name="user_phonenumber"
                                             onChange={handleFields}
                                             ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
                                     </div>
                                         <span className="errors">
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
                                                 <IntlMessages id="form.numbersOnlyErrorError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
                                                 <IntlMessages id="form.minPhoneLengthError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
                                                 <IntlMessages id="form.minPhoneLengthError" />}
                                         </span>
                                     </div>
                                 </div>
                                 <div className="form-row">
                                     <div className="form-group  dropdown col-md-6">
                                         <label><IntlMessages id="form.region" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <select name="user_region_id" className="form-control input-text"
                                             onChange={handleTwoEvents} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المحافظة</option>
                                             {state.regions.map(region => {
                                                 return (
                                                     <option key={region.region_id} value={region.region_id}>
                                                         {region.region_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         </div>
                                         <span className="errors">
                                             {errors.user_region_id && errors.user_region_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                     <div className="form-group  dropdown col-md-6">
                                         <label><IntlMessages id="form.area" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <select name="user_area_id" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المنطقة</option>
                                             {state.areas.map(area => {
                                                 return (
                                                     <option key={area.area_id} value={area.area_id}>
                                                         {area.area_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         </div>
                                         <span className="errors">
                                             {errors.user_area_id && errors.user_area_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                 </div>
 
 
 
     {/*------------------------------------ New Fields---------------------------------------------------------------- */}
 


                                 <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.email" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields}
                                             ref={register({
                                                 // required: true,
                                                 pattern: /^[^@ ]+@[^@ ]+\.[^@ .]{2,}$/
                                             })} />
                                         <span className="errors">
                                          {errors.user_email && errors.user_email.type === 'required' &&
                                 <IntlMessages id="form.requiredError" /> } 
                                             {errors.user_email && errors.user_email.type === 'pattern' &&
                                                 <IntlMessages id="form.emailError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.password" /></label>
                                         <input type="password" className="form-control" name="user_password"
                                             onChange={handleFields} ref={register({
                                                 required: true
                                                 , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/
                                             })} />
                                         <span className="errors">
                                             {errors.user_password && errors.user_password.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_password && errors.user_password.type === 'pattern' &&
                                                 <IntlMessages id="form.passwordMinLengthError" />}
                                              {errors.user_password && errors.user_password.type === 'minLength' &&
                                 <IntlMessages id="form.passwordMinLengthError" /> } 
                                         </span>
                                     </div>
                                 </div>
                                
                                 {(state.disabled === false) ? 
                                     <button type="submit" className="btn-margin" style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className="btn-margin" disabled={true} style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
                             </form>
 
 
                         </RctCollapsibleCard>
                     </div>
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }
         </React.Fragment>
     )
 }