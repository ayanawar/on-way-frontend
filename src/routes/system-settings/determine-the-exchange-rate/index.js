/**
 * extra fees
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();



export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		dollarValue: 0,
		disabled: false,
		currencies: []
	});

	useEffect(() => {
		axios.get('http://localhost:8000/getcurrency',USER_TOKEN).then(response => {
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				setState({
					currencies: response.data.message,
					disabled: false
				})
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});	
	}, []);


	const handleFields = ({ target }) => {
		// if(state.cardsnumber % 1000 != 0) {
			setState({ ...state, [target.name]: target.value,  });
			// console.log(target);
		// }
		
	};

	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/addcurrency', data: {
				"userTypeId": localStorage.getItem("user_type_id"),
				"dollarValue": state.dollarValue
			},headers: { "x-access-token": `${cookies.get('UserToken')}`}
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else if(res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				toast.success(<IntlMessages id="form.exchangeRateSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});	
	}

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "400px"
	};

	const { match } = props;
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
			<title>تحديد سعر الصرف</title>
			<meta name="description" content="تحديد سعر الصرف" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.responsive" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.responsive" />}>
				<form onSubmit={handleSubmit(onSubmit)}>
					<div className="form-row">
						<div className="form-group col-md-4">
							<label><IntlMessages id="form.exchangeRate" /></label>
							<input type="tel" className="form-control" name="dollarValue"
									onChange={handleFields}
									ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
							
							<span className="errors">
								{errors.dollarValue && errors.dollarValue.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.dollarValue && errors.dollarValue.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
							</span>
						</div>
					</div>
					{/* <div className="form-row"> */}
						
					{/* </div> */}
					{(state.disabled === false) ? 
						<button type="submit" className="btn btn-primary">
							<IntlMessages id="form.add" />
						</button> : 
						<button type="submit" className="btn btn-primary" disabled={true}>
							<IntlMessages id="form.add" />
						</button>
					}
				</form>
			</RctCollapsibleCard>
		</div>
		<div className="row mb-5">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							data={state.currencies}
							columns={[
									{
										label: <IntlMessages id="form.exchangeRate" />,
										name:  "dollar_value",
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "currency_creation_date" 
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div>
	</div>
	: (
			
		history.push("/access-denied")
	   )
	} 
	</React.Fragment>
	)
	}