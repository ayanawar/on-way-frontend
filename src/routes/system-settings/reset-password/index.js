import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import Modal from 'react-awesome-modal';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();

export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_id: '',
		user_type_id: '',
        new_password: '',
        person_user_id: '',
        name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
		commercialName: '',
		disabled: false,
		ModelToPrint: false,
	});

	const nf = new Intl.NumberFormat();
	
	const handleFields = ({ target }) => {
		// if(state.cardsnumber % 1000 != 0) {
			setState({ ...state, [target.name]: target.value,  });
			// console.log(target);
		// }
		
	};

	const closeModel = () => {
		setState({
			...state,
		   	ModelToPrint: false
		});
	}


	const openPrintModel = () => {
		setState({
			...state,
		   ModelToPrint: true
		})
		// console.log(state);
	}

	const resetState = () => {
		setState({
			...state,
			disabled: false,
			ModelToPrint: false,
		})
	}

    const showDetails = e => {
		// if (state.phoneNumberField === localStorage.getItem("phoneNumber")) {
		// 	toast.error(<IntlMessages id="form.extraFeesError" />, {
		// 		position: "top-center",
		// 		autoClose: 4000,
		// 		hideProgressBar: false,
		// 		closeOnClick: true,
		// 		pauseOnHover: true,
		// 		draggable: true
		// 	});
		// 	setState({
		// 		...state,
		// 		name: '',
		// 		region: '',
		// 		area: '',
		// 		phoneNumber: '',
		// 		balance: '',
		// 		rank: '',
		// 		commercialName: '',
		// 		type: ''
		// 	});
		// } else {
			axios.post(`http://localhost:8000/userdata/${state.phoneNumberField}`,
			{ 'userTypeId': 5 },USER_TOKEN).then(res => {
				if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
					cookies.remove('UserToken', {path:'/'})
					window.location.href = "/signin";	
				}
				else {
				// console.log(res.data.user[0]);
					if (res.data.user[0].userTypeId === 5) {
						setState({  
							...state,
                            person_user_id: res.data.user[0].userId,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].administrator_phone_number,
							balance: nf.format(res.data.user[0].administrator_virtual_money_balance),
							active_status: res.data.user[0].active_status,
							rank: '',
							commercialName: '',
							type: "الإدارة"
						});
					}
					else if (res.data.user[0].userTypeId === 3) {
						setState({
							...state,
                            person_user_id: res.data.user[0].userId,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].pos_phone_number,
							balance: nf.format(res.data.user[0].pos_vm_balance),
							rank: res.data.user[0].pos_rank,
							commercialName: res.data.user[0].pos_commercial_name,
							type: "نقطة البيع"
						});
					}
					else if (res.data.user[0].userTypeId === 2) {
						setState({
							...state,
                            person_user_id: res.data.user[0].userId,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].dealer_phone_number,
							balance: nf.format(res.data.user[0].dealer_virtual_money_balance),
							rank: '',
							commercialName: '',
							type: "الوكيل"
						});
					}
					else if (res.data.user[0].userTypeId === 1) {
						setState({
							...state,
                            person_user_id: res.data.user[0].userId,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].representative_phone_number,
							balance: nf.format(res.data.user[0].representative_virtual_mony_balance),
							rank: '',
							commercialName: '',
							type: "المندوب"
						});
					} 
					else if(res.data.user[0].userTypeId === 4) {
						setState({
							...state,
                            person_user_id: res.data.user[0].userId,
							name: res.data.user[0].userName,
							region: res.data.user[0].region_arabic_name,
							area: res.data.user[0].area_arabic_name,
							phoneNumber: res.data.user[0].supplier_phone_number,
							balance: '',
							rank: '',
							commercialName: '',
							type: 'المورد'
						});
					}
				}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					} else {
						toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							name: '',
							region: '',
							area: '',
							phoneNumber: '',
							balance: '',
							rank: '',
							commercialName: '',
							type: ''
						});
					}
					// }
				// console.log('Error fetching and parsing data', error.message);
			});
		// }
	}

	const Token = cookies.get('UserToken');
	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/adminResetPassword', data: {
				"new_password": state.new_password,
                "person_user_id": state.person_user_id,

				"userTypeId": localStorage.getItem("user_type_id"),
				"user_id": localStorage.getItem("userIdInUsers"),
				"user_password": state.user_password,

			},headers: { "x-access-token": `${Token}`  }
		}).then(res =>{
			// console.log(res);
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				
				toast.success('تم إعادة تعيين كلمة السر بنجاح', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true, ModelToPrint: false, })
				setTimeout(function () { location.reload()}, 3000)
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				});
				setState({ ...state, ModelToPrint: false, }) 
			} else {
				toast.error('كلمة السرالخاصة بالإدارة غير صحيحة غير مصرح لك بإعادة تعيين كلمة السر', {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, ModelToPrint: false, })
			}
		// console.log(error.message);
		});	
	// }
	}

	const { match } = props;
	// console.log(state);
	return (
	<React.Fragment>
    { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
	<div className="shop-wrapper mb-4">
		<ToastContainer />
		<Helmet>
			<title>إعادة تعيين كلمة السر</title>
 			<meta name="description" content="إعادة تعيين كلمة السر" />
  		</Helmet>
		<PageTitleBar title={<IntlMessages id="sidebar.resetPassword" />} match={match} />
		<div className="row">
			<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.resetPassword" />}>
				    <form onSubmit={handleSubmit(openPrintModel)}>
                        <div className="form-row">
                            <div className="form-group col-md-8">
                                <div className="row">
                                    <Form inline>
                                        <FormGroup className="mr-10 mb-10">
                                            <Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
                                            <input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
                                                onChange={handleFields} 
                                                ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
                                        </FormGroup>
                                
                                        <Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
                                    </Form>
                                </div>
                                <span className="errors">
                                    {errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
                                        <IntlMessages id="form.numbersOnlyErrorError" /> }
                                    {errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
                                        <IntlMessages id="form.minPhoneLengthError" /> }
                                    {errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
                                        <IntlMessages id="form.minPhoneLengthError" /> }
                                </span>
                            
                                <br />
                                {(state.name === '') ? <div></div>:
                                <React.Fragment>
                                    <h1>{state.type}</h1>
                                    <br /> 
                                    <h2><IntlMessages id="form.name" />: {state.name}</h2>
                                    <h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
                                    <h2><IntlMessages id="form.region" />: {state.region}</h2>
                                    <h2><IntlMessages id="form.area" />: {state.area}</h2>
                                    {(state.type === 'المورد') ? <div></div>:
                                        <h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
                                    }
                                    {(state.rank === '') ? <div></div>: 
                                        <React.Fragment>
                                            <h2><IntlMessages id="form.rank" />: {state.rank}</h2>
                                            <h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
                                        </React.Fragment>
                                    }
                                </React.Fragment>
                                }
                            </div>
                            <div className="form-group  dropdown col-md-4">
                                <label>كلمة السر الجديدة</label>
                                <input type="password" className="form-control" name="new_password"
                                        onChange={handleFields} ref={register({ required: true 
                                            , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/ })} />
                                <span className="errors">
                                    {errors.new_password && errors.new_password.type === 'required' &&
                                        <IntlMessages id="form.requiredError" /> }
                                    {errors.new_password && errors.new_password.type === 'pattern' &&
                                        <IntlMessages id="form.passwordMinLengthError" /> }
                                </span>
                            </div>
					    </div>
					
                        {(state.disabled === false) ? 
                            // <button onClick={openPrintModel}> testtttt</button>
                            <button type="submit" className="btn btn-primary">
                            تغيير
                            </button> : 
                            <button type="submit" className="btn btn-primary" disabled={true}>
                            تغيير
                            </button>
                        }
				    </form>
				

				{state.ModelToPrint == true ?
					<Modal visible={state.ModelToPrint} width="550" effect="fadeInUp" onClickAway={()=>{closeModel(); resetState()}}>
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title">إعادة تعيين كلمة السر</h5>
								<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							{/*  */}
							<form onSubmit={handleSubmit(onSubmit)}>
							<div className="modal-body">
								<p>برجاء إدخال كلمة السر الخاصة بالإدارة للتأكيد</p>
								<input
                                       // value={user_password}
                                       type="Password"
                                       name="user_password"
                                       className="form-control p-3"
                                       onChange={handleFields}
                                       style={{ borderRadius: "15px", fontSize:"17px" }}
                                       ref={register({ required: true 
                                          , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/ 
                                       })}
                                    />
									<br />
                                    <span className="errors">
                                       {errors.user_password && errors.user_password.type === 'required' &&
                                          <IntlMessages id="form.requiredError" /> }
                                       {errors.user_password && errors.user_password.type === 'pattern' &&
                                          <IntlMessages id="form.passwordMinLengthError" /> }
                                    </span>
							</div>
							<div className="modal-footer">
								{(state.disabled === false) ? 
									<button type="submit" className="btn btn-primary">
										تغيير
									</button> : 
									<button type="submit" className="btn btn-primary" disabled={true}>
									تغيير
									</button>
								}
								<button type="button" className="btn btn-secondary" onClick={()=>{closeModel(); resetState()}}>اغلاق</button>
								<div style={{display: "none"}}>
								{/* <Invoice id="print-invoice" {...state} onSelectCLose={closeModelFromChild} ref={el => (componentRef = el)} /> */}
								</div>
								
							</div>
							</form>
						</div>	
					</Modal>
                        :
                        null
                  }
			</RctCollapsibleCard>

		</div>
		{/* <div className="row">
			<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
				<RctCard className="mb-4">
					<RctCardContent noPadding>
						<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable 
							data={state.cards}
							columns={[
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundle_id",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_bundle_total_value" 
									},
									{
										label: <IntlMessages id="form.createdBy" />,
										name:  "admin_name",
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "virtual_mony_bundle_date_time" 
									},
									{
										label: <IntlMessages id="form.cardsCount" />,
										name:  "virtual_mony_bundle_count" 
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name: "category_value"
									},
									]}
							options={options}  
							/>
						</MuiThemeProvider>
					</RctCardContent>
				</RctCard>
			</RctCollapsibleCard>
		</div> */}
	</div>
	: (
			
		history.push("/access-denied")
	   )
	} 
	   </React.Fragment>
	)
	}