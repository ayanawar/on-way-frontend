/**
 * View Cart Page
 */
 import React, { useEffect, useState } from 'react';
 import axios from 'axios';
 import MUIDataTable from "mui-datatables";
 import { RctCard, RctCardContent } from 'Components/RctCard';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import { useHistory } from 'react-router-dom';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
import StateManager from 'react-select';
 
 const cookies = new Cookies(); 
 
 const options = {
     filter: true,
     filterType: 'dropdown',
     rowsPerPage: 10,
     rowsPerPageOptions: [5,10,25,50,100],
     responsive: 'vertical',
     enableNestedDataAccess: '.',
     selectableRows: "none",
     viewColumns: false,
     sort: false,
     fixedHeader: true,
     download: false,
     fixedSelectColumn: false,
     tableBodyHeight: "600px"
  };
 
  export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         users: [],
         disabled: false,
         hiddenStatusForm: true,
         regions: [],
         areas2: [],
         user_id:'',
         first_name:'',
         middle_name:'',
         last_name:'',
         user_phonenumber:'',
         region_arabic_name:'',
         area_arabic_name:'',
         user_type_id:'',
         usertypes:[],
         type_ar:'',
         security_level_type_ar:'',
         regionId:'',
         user_area_id: '',
         user_region_id:'',
         security_level_id:'',
         active_status:'',
         levels: [],
     });
     useEffect(() => {
         axios.get('https://accbackend.alaimtidad-itland.com/users',USER_TOKEN).then(response2 => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => { 
                axios.get('https://accbackend.alaimtidad-itland.com/user-types ', USER_TOKEN).then(response5 => { 
                    axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {       
             if(response5.data == "Token Expired" || response5.data == "Token UnAuthorized" ||response2.data == "Token Expired" || response2.data == "Token UnAuthorized" ||response8.data == "Token Expired" || response8.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', {path:'/'})
                 window.location.href = "/signin";	
             }
             else{
                 setState({
                     ...state,
                     users: response2.data.users,
                     disabled: false,
                     hiddenStatusForm: true,
                     regions: response8.data.regions,
                     usertypes:response5.data,
                     levels: response.data.levels,
                 })
             }
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
         })
   
         .catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response2.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);
 
     const onClearStatusClicked = () => {
         setState({...state,
            hiddenStatusForm: true,
            user_id:'',
            first_name:'',
            middle_name:'',
            last_name:'',
            user_phonenumber:'',
            region_arabic_name:'',
            area_arabic_name:'',
            type_ar:'',
            security_level_type_ar:'',
         
         })
     };
     const handleTwoEvents3 = (e) => {
        handleFields(e);
		let regionId = e.target.value;

		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": e.target.value }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						regionId: regionId,
						areas2: response.data.areas,
						user_area_id: '',
						
					})

				}else{
					setState({
						...state,
						regionId: regionId,
						areas2: response.data.areas,
					
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
     }
     const onSubmit2 = e => {
         let data ={

            first_name: state.first_name,
            middle_name: state.middle_name,
            last_name: state.last_name,
            security_level_id: state.security_level_id,
            user_phonenumber:state.user_phonenumber,
            user_region_id: state.regionId,
            user_area_id: state.user_area_id,
            user_email:'',
            user_password: state.user_password,
            user_type_id: state.user_type_id,
            active_status: state.active_status
         }
          console.log("data",data);
         axios({
             method: 'post', url: `https://accbackend.alaimtidad-itland.com/update-user/${state.user_id}`, data: data,
             headers: { "x-access-token": `${cookies.get('UserToken')}`}
         }).then(res =>{
             console.log("res",res);
             toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
                 position: "top-center",
                 autoClose: 4000,
                 hideProgressBar: false,
                 closeOnClick: true,
                 pauseOnHover: true,
                 draggable: true
             });
             setState({ ...state, disabled: true })
             setTimeout(function () { location.reload()}, 3000)
         }).catch(error => {
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
        }
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
    
     const getMuiTheme = () => createMuiTheme({
         overrides: {
             MUIDataTable: {
                 responsiveScroll: {
                   maxHeight: 'unset',
                   overflowX: 'unset',
                   overflowY: 'unset',
                 },
             },
             MuiTableCell: {
                 head: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif"
                 }, 
                 body: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontSize: "15px",
                     fontFamily: "'Almarai', sans-serif",
                 }       
             },
             MUIDataTableHeadCell: {
                 data: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 },
                 fixedHeader: {
                     position: "sticky !important",
                     zIndex: '100',
                 }
             },
             MUIDataTableSelectCell: {
                 headerCell: {
                     zIndex: 1
                 },
                 fixedLeft: {
                     zIndex: 1
                 }
             },
             MUIDataTableToolbarSelect: {
                 root: {
                     color: "#599A5F",
                     fontWeight: "bold",
                     zIndex: 1,
                 }         
             },
             MuiPaper: {
                 root: {
                     color: "#092346",
                     fontWeight: "bold",
                     fontFamily: "'Almarai', sans-serif"
                 }
             },
             MuiToolbar:{
                 regular: {
                     backgroundColor:"gray"
                 },
                 root: {
                     top: 0,
                     position: 'sticky',
                     background: 'white',
                     zIndex: '100',
                 },
             },
             MUIDataTablePagination: {
                 tableCellContainer: {
                     backgroundColor:"gray"
                 }
             },
             MUIDataTableBody: {
                 emptyTitle: {
                     display: "none",
                 }
             }
         }
     })

 //    render() {
       const { match } = props;
    
       return (
         <React.Fragment>
         { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
          <div className="cart-wrapper">
             <ToastContainer />
             <Helmet>
                 <title>قائمة المستخدمين</title>
                 <meta name="description" content="قائمة المستخدمين" />
               </Helmet>
             <PageTitleBar title={<IntlMessages id="sidebar.systemuserupdate" />} match={match} />
             <div className="row">
                <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={"تعديل بيانات المستخدمين"}>
                     {/*  */}
                     {(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
                     : 
                     <form onSubmit={handleSubmit(onSubmit2)}>	
                     <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.firstName" /></label>
                                         <input type="text" className="form-control" name="first_name" defaultValue={state.first_name}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.first_name && errors.first_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.first_name && errors.first_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.first_name && errors.first_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.middleName" /></label>
                                         <input type="text" className="form-control" name="middle_name" defaultValue={state.middle_name}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.middle_name && errors.middle_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.middle_name && errors.middle_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.middle_name && errors.middle_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.lastName" /></label>
                                         <input type="text" className="form-control" name="last_name" defaultValue={state.last_name}
                                             onChange={handleFields}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                         <span className="errors">
                                             {errors.last_name && errors.last_name.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.last_name && errors.last_name.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.last_name && errors.last_name.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>
                                 </div>
                                 <div className="form-row">
                                 <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.password" /></label>
                                         <input type="password" className="form-control" name="user_password"
                                             onChange={handleFields} ref={register({
                                                 required: true
                                                 , pattern: /^(?=.*\d+)(?=.*[a-zA-Z])[0-9a-zA-Z]{4,100}$/
                                             })} />
                                         <span className="errors">
                                             {errors.user_password && errors.user_password.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_password && errors.user_password.type === 'pattern' &&
                                                 <IntlMessages id="form.passwordMinLengthError" />}
                                              {errors.user_password && errors.user_password.type === 'minLength' &&
                                 <IntlMessages id="form.passwordMinLengthError" /> } 
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.phoneNumber" /></label>
                                         <input type="tel" className="form-control" name="user_phonenumber" defaultValue={state.user_phonenumber}
                                             onChange={handleFields}
                                             ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })} />
                                         <span className="errors">
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
                                                 <IntlMessages id="form.numbersOnlyErrorError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'minLength' &&
                                                 <IntlMessages id="form.minPhoneLengthError" />}
                                             {errors.user_phonenumber && errors.user_phonenumber.type === 'maxLength' &&
                                                 <IntlMessages id="form.minPhoneLengthError" />}
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                     <label><IntlMessages id="form.usertype" /></label>
                                 <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                         <select name="user_type_id" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار نوع المستخدم</option>
                                             {state.usertypes.map(user => {
                                                 return (
                                                     <option key={user.user_type_id} value={user.user_type_id}>
                                                         {user.type_ar}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         </div>
                                         <span className="errors">
                                             {errors.user_type_id && errors.user_type_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                         </div>
                                 </div>
                                 <div className="form-row">
                                     <div className="form-group  dropdown col-md-4">
                                         <label><IntlMessages id="form.region" /></label>
                                         <select name="user_region_id" className="form-control input-text"
                                             onChange={handleTwoEvents3} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار المحافظة</option>
                                             {state.regions.map(region => {
                                                 return (
                                                     <option key={region.region_id} value={region.region_id}>
                                                         {region.region_arabic_name}
                                                     </option>
                                                 )
                                             })
                                             }
                                         </select>
                                         <span className="errors">
                                             {errors.user_region_id && errors.user_region_id.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                     <div className="form-group  dropdown col-md-4">
										<label><IntlMessages id="form.area" /></label>
										<div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
										borderWidth: "0px 2px 2px 0px",
										lineHeight: "0px",
									    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
														}}>
										<select name="user_area_id" className="form-control input-text"
											onChange={handleFields} ref={register({ required: true })}>
											<option key="0" value="">برجاء اختيار المنطقة</option>
											{state.areas2.map(a=>
											{
												return (
													<option key={a.area_id} value={a.area_id}>
														{a.area_arabic_name}
													</option>
												)
											})
									
											}
										</select>
										</div>
										<span className="errors">
											{errors.user_area_id && errors.user_area_id.type === 'required' &&
												<IntlMessages id="form.requiredOptionError" />}
										</span>
									</div>

                                     <div className="form-group col-md-4">
									  <label><IntlMessages id="form.secretdegree" /></label>
									  <div >
									  <select className="form-control input-text" name="security_level_id"
										  onChange={handleFields} ref={register({ required: true })} >
										  <option key="0" value="">برجاء اختيار درجة السرية</option>
										  {state.levels.map(level => {
											 return (
												 <option key={level.security_level_id} value={level.security_level_id}>
													 {level.security_level_type_ar}
												 </option>
											 )
										 })
										 }
					   </select>
					   </div>
					   <span className="errors">
										 {errors.security_level_id && errors.security_level_id.type === 'required' &&
											 <IntlMessages id="form.requiredOptionError" />}
									 </span>
								  </div>
                                 </div>
 
                                 <div className="form-row">
                                     <div className="form-group  dropdown col-md-4">
                                         <label><IntlMessages id="widgets.status" /></label>
                                         <select name="active_status" className="form-control input-text"
                                             onChange={handleFields} ref={register({ required: true })}>
                                             <option key="0" value="">برجاء اختيار الحالة</option>
                                             <option key="1" value="Active">مفعل</option>
                                             <option key="2" value="inactive">غير مفعل</option>
                                         </select>
                                         <span className="errors">
                                             {errors.active_status && errors.active_status.type === 'required' &&
                                                 <IntlMessages id="form.requiredOptionError" />}
                                         </span>
                                     </div>
                                     </div>
                         {(state.disabled === false) ? 
                             <button type="submit" className="btn btn-primary">
                             <IntlMessages id="form.update" />
                             </button> : 
                             <button type="submit" className="btn btn-primary" disabled={true} >
                             <IntlMessages id="form.update" />
                             </button>
                             }
 
                         <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                             <IntlMessages id="form.clear" />
                         </button>
                     </form>
                       }
                </RctCollapsibleCard>
             </div>
             <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                     data={state.users}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "user_id",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                 label: <IntlMessages id="form.firstName" />,
                                                 name:  "first_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.middleName" />,
                                                 name:  "middle_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.lastName" />,
                                                 name:  "last_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.phoneNumber" />,
                                                 name:  "user_phonenumber"
                                             },
                                             {
                                                 label: <IntlMessages id="form.region" />,
                                                 name:  "region_arabic_name",
                                             },
                                             {
                                                 label: <IntlMessages id="form.area" />,
                                                 name:  "area_arabic_name"
                                             },
                                             {
                                                 label: <IntlMessages id="form.usertype" />,
                                                 name:  "type_ar"
                                             },
                                             
                                             {
                                                label: <IntlMessages id="form.secretdegree" />,
                                                name:  "security_level_type_ar"
                                            },
                                            {
                                                label: <IntlMessages id="widgets.status"/>,
                                                name:  "active_status",
                                                options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                }
                                            },
                                            {
                                                label:<IntlMessages id="widgets.status" /> ,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            {(tableMeta.rowData[9] == "inactive" ) ? 
                                                            <span>غير مفعل</span> : 
    
                                                            <span>مفعل</span>
                                                            
                                                            }
                                                          
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                            
                                            {
                                                label: <IntlMessages id="form.update" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                          <button type="button" className="btn btn-warning" 
                                                       onClick={() => {
                                                       
                                                        setState({
                                                         ...state , 

                                                        user_id:tableMeta.rowData[0],
                                                        first_name:tableMeta.rowData[1],
                                                        middle_name:tableMeta.rowData[2],
                                                        last_name:tableMeta.rowData[3],
                                                        user_phonenumber:tableMeta.rowData[4],
                                                        region_arabic_name:tableMeta.rowData[5],
                                                        area_arabic_name:tableMeta.rowData[6],
                                                        type_ar:tableMeta.rowData[7],
                                                        security_level_type_ar:tableMeta.rowData[8],
                                                        hiddenStatusForm: false,
                                                 
                                                        });
                                                 }}
                                                           > 
                                                           <IntlMessages id="form.update" />
                                                            </button> 
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },

                                             {
                                                label: <IntlMessages id="form.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger" 
                                                           
                                                            >
                                                            <IntlMessages id="form.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>
          </div>
          : (
             history.push("/access-denied")
            )
         } 
            </React.Fragment>
       )
 //    }
 }