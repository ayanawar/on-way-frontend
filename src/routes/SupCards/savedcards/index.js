import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

export default function Shop(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,

      userIdInTable:'',
      userTypeIdInTable: '',
		userTypeId: 0,
		userId: 0,
		availableCards: [],
		pos_sim_card_transaction: [],
		availableCards: []
	});

	const nf = new Intl.NumberFormat();

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					zIndex: 1
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				}
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
		}
	})
	
	React.useEffect(() => {
		( 
		axios.get(`http://localhost:8000/getuserbyphone/${localStorage.getItem("phoneNumber")}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
				
			}
			
			else {
				// console.log("phonenumber",localStorage.getItem("phoneNumber"));
				// console.log("ress",res.data);
				axios.post('http://localhost:8000/availablecards', 
				{ 'userTypeId': localStorage.getItem("user_type_id"),
				  'supplierPhoneNumber': localStorage.getItem("phoneNumber"),
				},USER_TOKEN).then(res2 => {
			
					setState({
						...state,
						name: res.data.tableresult[0].userName,
						region: res.data.tableresult[0].region_arabic_name,
						area: res.data.tableresult[0].area_arabic_name,
						phoneNumber: res.data.tableresult[0].supplier_phone_number,
						userTypeIdInTable: res.data.userresult[0].user_type_id,
						userIdInTable: res.data.userresult[0].user_id,
						type: "مورد",
						availableCards: res2.data.availableCards,

					})
					// console.log("state",state);
				}).catch(error => {

				});
			
		}
		}).catch(error => {
			toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
	})
		)
}, []);
	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_USER_TYPE === 4 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
		<div className="shop-wrapper">
			<ToastContainer />
         <PageTitleBar title={<IntlMessages id="sidebar.availableCards" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.availableCards" />}>
					 <div className="form-row">
						<div className="form-group col-md-12">
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									{/* <h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2> */}
								</React.Fragment>
							}
						</div>
					</div> 
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							{/* <h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1> */}
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.availableCards}
								columns={[
									{
										label: <IntlMessages id="form.card_id" />,
										name:  "card_id",
											// options: {
											// 	display: "none",
											// 	filter: false
											// }
                           },
                           {
                              label: <IntlMessages id="form.bundleId" />,
										name:  "bundle_id",
                           },
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "serial",
                           },
                           {
										label: <IntlMessages id="form.companyName" />,
										name:  "company_name_ar"
                           },
                           {
										label: <IntlMessages id="widgets.category" />,
										name:  "category_text"
                           },
									{
										label: <IntlMessages id="form.purchasePrice" />,
										name:  "purchase_price"
									},
									{
										label: <IntlMessages id="form.cardsSellPriceAdmin" />,
										name:  "sell_price",
									},
									{
										label: <IntlMessages id="form.cardsSellPricePOS" />,
										name:  "pos_sell_price"
                           },
                           {
										label: <IntlMessages id="form.currency" />,
										name:  "currency"
									},
                           {
                              label: <IntlMessages id="form.ownerUserName" />,
										name:  "ownerUserName"
                           },
									{
										label: <IntlMessages id="widgets.endDate" />,
										name:  "expire_date"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	);
}




