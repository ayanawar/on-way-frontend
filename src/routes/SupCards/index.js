/**
 * Pages Routes
 */
import React from 'react';
import { Helmet } from "react-helmet";
import { Redirect, Route, Switch } from 'react-router-dom';

// async components
import {
	AsyncUserWidgetComponent,
	AsyncUserChartsComponent,
	AsyncGeneralWidgetsComponent,
    AsyncPromoWidgetsComponent,
	AsyncSupCardsComponent,
	AsyncsavedcardsComponents,
	AsyucpaidcardsComponents,
	AsyucfinishcardsComponents,
	AsyncioanmoneyComponents,
	AsynctotalAssesmentComponents
} from 'Components/AsyncComponent/AsyncComponent';

const Pages = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>الصفحة الرئيسية للمورد</title>
			<meta name="description" content="Reactify Widgets" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/user`} />
			<Route path={`${match.url}/user`} component={AsyncUserWidgetComponent} />
			<Route path={`${match.url}/charts`} component={AsyncUserChartsComponent} />
			<Route path={`${match.url}/general`} component={AsyncGeneralWidgetsComponent} />
            <Route path={`${match.url}/cards`} component={AsyncSupCardsComponent} />
			<Route path={`${match.url}/promo`} component={AsyncPromoWidgetsComponent} />
			<Route path={`${match.url}/savedcards`} component={AsyncsavedcardsComponents} />
			<Route path={`${match.url}/paidcards`} component={AsyucpaidcardsComponents} />
			<Route path={`${match.url}/finishmoney`} component={AsyucfinishcardsComponents} />
			<Route path={`${match.url}/ioanmoney`} component={AsyncioanmoneyComponents} />
			<Route path={`${match.url}/totalAssesment`} component={AsynctotalAssesmentComponents} />
			
			
			
		</Switch>
	</div>
);

export default Pages;
