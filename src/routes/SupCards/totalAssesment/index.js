/**
 * 
 */
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { Helmet } from "react-helmet";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';

export default function Shop(props) {
	const history = useHistory();
	const [state, setState] = useState({
		VM: [],
		RM: [],
		companies: [],
		categoriesByCompany: [],
		companyName: ''  
	});

	useEffect(() => {
    axios.get(`http://localhost:8000/getallcompanies/${localStorage.getItem("user_type_id")}`,USER_TOKEN).then(response => {
      setState({
        ...state,
        companies: response.data.message,
      })
    }).catch(error => {
      	// console.log('Error fetching and parsing data', error);
    });
	}, []);

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					zIndex: 1
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				}
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif",
					fontSize: "20px"
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
		}
  })
  // const changeRowColor = (index) =>{
  //   let row = document.getElementById(`MUIDataTableBodyRow-${index}`);
  //   row.setAttribute('style', 'background: yellow');
  // }
  
  const handleRowClick = (rowData, rowIndex) => {
    axios.post('http://localhost:8000/getcompanyavcards', {
      'userTypeId': localStorage.getItem("user_type_id") ,
      'companyId': rowData[0]
    },USER_TOKEN).then(res => {
      // changeRowColor(rowIndex.rowIndex);
      setState({ 
        ...state,
        categoriesByCompany: res.data.message,
        companyName: res.data.message[0].company_name_ar
        // companyName: res.data.message[0].company_name
      })
    }).catch(error => {
      	// console.log(error)
    }); 
  };


	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 32,
		rowsPerPageOptions: [5,10,32,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: 'none',
		onRowClick: handleRowClick,
		sort: false,
		viewColumns: false,
		selectableRowsHeader: false,
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_USER_TYPE === 4 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
 				<title>كشف حساب</title>
 				<meta name="description" content="كشف حساب" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sup.totalAssesment" />} match={props.match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					>
				
					<br />			
				<div className="row">
					
					<RctCollapsibleCard
						customClasses="p-20 text-center"
						colClasses="col-sm-12 col-lg-12 col-xl-12"
					>
						<h3 className="mb-20"><IntlMessages id="sup.totalAssesment" /></h3>
						<h4 className="reportsCardsNumber"></h4>	
					</RctCollapsibleCard>
				</div>
				<div className="form-row mt-4">
				
          <div className="form-group col-md-12 text-center">
						{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
						<RctCollapsibleCard fullBlock>
						<MuiThemeProvider theme={getMuiTheme()}>
						<MUIDataTable
              				data={state.RM}
							title={<IntlMessages id="sup.totalAssesment" />}
							columns={[
								{
								   label: <IntlMessages id="form.phoneNumber" />,
								   name:  "",
								},
								{
									label: <IntlMessages id="form.companyName" />,
									name:  ""
								},
								{
									label: <IntlMessages id="widgets.cardGroup" />,
									name:  ""
								},
								{
									label: <IntlMessages id="totalpaidCards" />,
									name:  ""
								},
								{
									label: <IntlMessages id="totalAvailablecards" />,
									name:  ""
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  ""
								}, 
								]}
							options={options}
						/>
						</MuiThemeProvider>
						</RctCollapsibleCard>
					</div>
				</div>
			</RctCollapsibleCard>
		</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}

