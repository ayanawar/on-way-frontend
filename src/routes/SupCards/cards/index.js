/**
 * View Cart Page
 */
import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Helmet } from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const options = {
   filter: true,
   filterType: 'dropdown',
   rowsPerPage: 5,
   rowsPerPageOptions: [5,10,25,50,100],
   responsive: 'vertical',
   enableNestedDataAccess: '.',
   selectableRows: "none",
   viewColumns: false,
   sort: false
 };

class Cards extends Component {
	state = {
			areas: [],
	};
   
   componentDidMount() {
		// all areas
		axios.get('http://localhost:8000/allareas',USER_TOKEN).then(response => {
			// console.log(response.data);
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				response.data.areas.splice(0, 1);
				this.setState({
					areas: response.data.areas
				})
			}
		})
		.catch(error => {
		   	// console.log('Error fetching and parsing data', error);
		});
   }
   
   getMuiTheme = () => createMuiTheme({
		overrides: {
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					zIndex: 1
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				}
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
		}
	})

   render() {
      const { match } = this.props;
      return (
          
         <div className="cart-wrapper">

            <Helmet>
 				<title>قائمة الكارتات </title>
 				<meta name="description" content="  قائمةالكارتات  " />
 			</Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.dataTable1" />} match={match} />
            <RctCard>
               <RctCardContent noPadding>
					<MuiThemeProvider theme={this.getMuiTheme()}>
						<MUIDataTable 
						// title={<IntlMessages id="sidebar.dataTable" />}
						data={this.state.areas}
						columns={[
                                    {
                                        label: <IntlMessages id="form.dataTime" />,
                                         name:  "region_arabic_name",
                                    },
									{
										label: <IntlMessages id="widgets.category" />,
										name:  "region_arabic_name",
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "area_arabic_name"
                                    },
                                    {
										label: "PIN",
										name:  "region_arabic_name",
                                    },
                                    {
										label: <IntlMessages id="form.serialNumber"/>,
										name:  "region_arabic_name",
                                    },
                                    {
										label: <IntlMessages id="form.area"/>,
										name:  "region_arabic_name",
                                    },
                                    {
										label: <IntlMessages id="form.sellPrice"/>,
										name:  "region_arabic_name",
                                    },
                                    {
										label: <IntlMessages id="widgets.status"/>,
										name:  "region_arabic_name",
                                    },
                                    
								]}
							options={options}
						/>
					</MuiThemeProvider>
               </RctCardContent>
            </RctCard>
         </div>
      )
   }
}


export default Cards;