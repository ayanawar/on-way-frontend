import React, { useState, useEffect } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Helmet } from "react-helmet";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';
const cookies = new Cookies();


export default function Shop(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
        commercialName: '',
        userIdInTable:'',
		userTypeIdInTable: '',
		bundleNumber: '',
		serial_number: '',
		pin_number: '',
		userTypeId: 0,
        userId: 0,
		companyId: '',
		categoryId: '',
        companies: [],
        categories: [],
		pos_company_cards_transaction: [],
		realmoney_transaction: [],
		vm_transaction: [],
		hiddenExcelButton: true,
    });
    
    useEffect(() => {
		// axios.get('http://localhost:8000/allregions',USER_TOKEN).then(response => {
		// 	axios.get('http://localhost:8000/allareas',USER_TOKEN).then(response2 => {
				axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response3 => {
					if(response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else{
						// response.data.regions.splice(0, 1);
						// response2.data.areas.splice(0, 1);
						response3.data.companies.splice(0, 1);
						setState({
                            ...state,
							// regions: response.data.regions, 
							// areas: response2.data.areas,
							companies: response3.data.companies,
							// imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						});
					}
				});
		// 	}).catch(error => {
		// 		// console.log('Error fetching and parsing data', error);
		// 	});
		// }).catch(error => {
		//    	// console.log('Error fetching and parsing data', error);
		// });
	}, []); 
	//const nf = new Intl.NumberFormat();

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	
	const showDetails = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	'user_type_id': localStorage.getItem("user_type_id"),
			'user_id': localStorage.getItem("userIdInUsers"),
			'serial_number': state.serial_number
		},USER_TOKEN).then(res2 => {
			// console.log(res2.data);
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				if(res2.data.vm_transaction.length == 0) {
					toast.error(<IntlMessages id="components.NoItemFound" /> , {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						vm_transaction: res2.data.vm_transaction,
						hiddenExcelButton: true,
					})
				} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}

	const showDetails2 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'bundleNumber': state.bundleNumber
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}

	const showDetails3 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'pin_number': state.pin_number
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}
	
	const showDetails4 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'companyId': state.companyId,
			'categoryId': state.categoryId,
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
    }
    
    const getCategories = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

    	axios.get(`http://localhost:8000/allcategories/${target.value}`,USER_TOKEN).then(response => {
            setState({
                ...state,
                companyId: target.value, 
                categories: response.data.message,
                
            })
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
    }	

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		let newArr = []
		newArr = state.vm_transaction.map(p => ({
			"Bundle Number": p.bundle_number,
			"Date & time Bundle": p.creation_date,
			"Company Name": p.company_name_ar,
			"Category": p.category_text,
			"Serial": p.company_cards_serial,
			"PIN": p.pin_number,
			"POS Sell Price": parseLocaleNumber(p.cards_pos_sell_price),
			"Admin Sell Price": parseLocaleNumber(p.admin_sell_price),
			"POS Name": p.useerName,
			"POS Phone": p.user_phonenumber,
			"Profit":p.cards_pos_profit,
			"Invoice Number": p.rm_cards_receipt,
			"Date & time": p.creation_date,
			"Extra or Discount fees For POS": parseLocaleNumber(p.pos_discount_amount),
		    "Extra or Discount fees For Region": parseLocaleNumber(p.region_discount_amount)
		}));

		const ws = XLSX.utils.json_to_sheet(newArr);

		const wb = { Sheets: { 'حركة كارتات الشركات': ws }, SheetNames: ['حركة كارتات الشركات'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, `حركة كارتات الشركات` + fileExtension);
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "420px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
            <Helmet>
 				<title>حركة كارتات الشركات</title>
 				<meta name="description" content="حركة كارتات الشركات" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.movementOf" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="form.inquireAboutTheMovementOfCompaniesCards" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<div className="form-group col-md-4">
									<label><IntlMessages id="form.searchBy" /></label>
									<select className="form-control" name="search_by" 
											onChange={handleFields} ref={register({ required: true })}>  
											<option key="0" value="">برجاء اختيار وسيلة البحث</option>
											<option key="1" value="bundleId">رقم الكوتة</option> 
											<option key="2" value="serialNumber">الرقم التسلسلى</option>   
											<option key="3" value="PIN">PIN</option> 
                                            <option key="4" value="companyAndCategory">الشركة والفئة</option>      
									</select>
									<span className="errors">
										{errors.search_by && errors.search_by.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" /> }
									</span>
								</div>
								{(state.search_by != "bundleId") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails2)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.bundleId" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="bundleNumber"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 1 })}/>
									</FormGroup>
									<Button className="mb-10" color="primary" type="submit"><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.bundleNumber && errors.bundleNumber.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.bundleNumber && errors.bundleNumber.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
								{(state.search_by != "serialNumber") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.serialNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="serial_number"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]*$/, minLength: 1,  })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.serial_number && errors.serial_number.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.serial_number && errors.serial_number.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
								{(state.search_by != "PIN") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails3)}>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2">PIN</Label>
										<input type="text" className="form-control mr-2 ml-2" name="pin_number"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]*$/, minLength: 1, })}
											/>
									</FormGroup>
							
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
									<span className="errors m-2">
										{errors.pin_number && errors.pin_number.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.pin_number && errors.pin_number.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span>
								</Form>
								}
                                {(state.search_by != "companyAndCategory") ? <React.Fragment></React.Fragment> :
								<Form inline onSubmit={handleSubmit(showDetails4)}>
									<FormGroup className="mr-10 mb-10">
                                    <div className="form-group col-md-6">
                                        <label><IntlMessages id="form.companyName" /></label>
                                        <select className="form-control" name="companyId" 
                                                onChange={getCategories} ref={register({ required: true })}>  
                                                <option key="0" value="">برجاء اختيار اسم الشركة</option>    
                                        {state.companies.map((company)=>{
                                            return(
                                                <option key={company.company_id} value={company.company_id}>
                                                    {company.company_name_ar}
                                                </option>
                                            )
                                        })
                                        }
                                        </select>
                                        <span className="errors">
                                            {errors.companyId && errors.companyId.type === 'required' &&
                                                <IntlMessages id="form.requiredOptionError" /> }
                                        </span>
                                    </div>
                                    {(state.companyId == '') ? <React.Fragment></React.Fragment> :
                                    <div className="form-group  dropdown col-md-4">
                                        <label><IntlMessages id="widgets.category" /></label>
                                        <select name="categoryId" className="form-control input-text" 
                                                onChange={handleFields} ref={register({ required: true })}>
                                                <option key="0" value="">برجاء اختيار الفئة</option>
                                        {state.categories.map(category => {
                                            return (
                                                <option key={category.cards_category_id} value={category.cards_category_id}>
                                                    {category.category_text}
                                                </option>
                                            )
                                        })
                                        }
                                        </select>
                                        <span className="errors">
                                            {errors.categoryId && errors.categoryId.type === 'required' &&
                                                <IntlMessages id="form.requiredOptionError" /> }
                                        </span>
                                    </div>
                                    }
									</FormGroup>
							
									<Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
									{/* <span className="errors m-2">
										{errors.pin_number && errors.pin_number.type === 'required' &&
											<IntlMessages id="form.requiredError" /> }
										{errors.pin_number && errors.pin_number.type === 'pattern' &&
											<IntlMessages id="form.numbersOnlyErrorError" /> }
									</span> */}
								</Form>
								}
							</div>
							
							<br />
							{/* {(state.name === '') ? <div></div>:
								<React.Fragment>
									<h1>{state.type}</h1>
									<br /> 
									<h2><IntlMessages id="form.name" />: {state.name}</h2>
									<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
									<h2><IntlMessages id="form.region" />: {state.region}</h2>
									<h2><IntlMessages id="form.area" />: {state.area}</h2>
									<h2><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h2>
									{(state.rank === '') ? <div></div>: 
										<React.Fragment>
											<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
											<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
										</React.Fragment>
									}
								</React.Fragment>
							} */}
						</div>
					</div>


					<div className="form-group col-md-6">
								<br />
								<div className="container">
									{(state.hiddenExcelButton === false) ?
										<React.Fragment>
											<button className="btn btn-primary ml-5 mr-5" onClick={(e) => exportToCSV()}>
												<IntlMessages id="form.exportExcel" />
											</button>
										</React.Fragment>
										:
										<React.Fragment></React.Fragment>
									}
								</div>
							</div>

					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.vm_transaction}
								columns={[
								{
									label: <IntlMessages id="form.serialNumber" />,
										name:  "transaction_id",
										options: {
											display: "none",
											filter: false
										}
								},
								{
									label: <IntlMessages id="form.bundleId" />,
									name:  "bundle_number" 
								},
								{
									label: "تاريخ رفع الكوتة",
									name:  "creation_date" 
								},
								{
									label: <IntlMessages id="form.cardType" />,
									name:  "company_name_ar",
								},
								{
									label: <IntlMessages id="widgets.category" />,
									name:  "category_text"
								},
								{
									label: <IntlMessages id="form.serialNumber" />,
									name:  "company_cards_serial",
								},
								{
									label: "PIN",
									name:  "pin_number",
								},
								{
									label: <IntlMessages id="form.cardsSellPricePOS" />,
									name:  "cards_pos_sell_price"
								},
								{
									label: <IntlMessages id="form.cardsSellPriceAdmin" />,
									name:  "admin_sell_price",
								},
								{
									label: "اسم نقطة البيع",
									name:  "useerName"
								},
								{
									label: "رقم نقطة البيع",
									name:  "user_phonenumber"
								},
								{
									label: <IntlMessages id="widgets.netProfit" />,
									name:  "cards_pos_profit"
								},
								{
									label: <IntlMessages id="form.invoiceNumber" />,
									name:  "rm_cards_receipt"
								},
								{
									label: <IntlMessages id="form.dataTime" />,
									name:  "creation_date",
								},
								{
									label: <IntlMessages id="form.discountAmountPOS" />,
									name:  "pos_discount_amount"
								},
								{
									label: <IntlMessages id="form.regionDiscountAmount" />,
									name:  "region_discount_amount"
								},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
					{/* <div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfRM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.realmoney_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "rm_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_serial_number",
									},
									{
										label: "PIN",
										name:  "virtual_mony_pin_number",
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "virtual_mony_bundleNumber"
									},
									{
										label: <IntlMessages id="form.paidAmount" />,
										name:  "paid_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div> */}
					{/* <div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.vm_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "virtual_mony_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_transaction_serial_number",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_transaction_value"
									},
									{
										label: <IntlMessages id="form.transferredBalanceValue" />,
										name:  "virtual_money_transaction_transfer_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div> */}
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
		   )
		} 
		</React.Fragment>
	)
	}
