/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    Asyncdailyinvoice,
    Asyncpayinvoice,
    Asyncaddinvoice,
    Asyncallinvoice
    //  AsyncDataTableComponent,
    //  AsyncResponsiveTableComponent,
    //  AsyncDisableSystemComponent,
    //  AsyncResetPasswordComponent,
    //  AsyncSystemUser,
    //  AsyncSystemUserUpdate,
    //  Asynccostcenter,
    //  AsyncAddEmployeeComponent,
    //  AsyncAllEmployeeComponent,
    //  AsyncemployeetransactionComponent
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>قيود</title>
             <meta name="description" content="قيود" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/addinvoice`} />
             <Route path={`${match.url}/dailyinvoice`} component={Asyncdailyinvoice} />
             <Route path={`${match.url}/payinvoice`} component={Asyncpayinvoice} />
             <Route path={`${match.url}/addinvoice`} component={Asyncaddinvoice} />
             <Route path={`${match.url}/allinvoices`} component={Asyncallinvoice} />
         </Switch>
     </div>
 );
 
 export default Pages;
 