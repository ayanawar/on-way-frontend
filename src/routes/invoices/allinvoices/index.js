/**
 * add supplier
 */
 import React, { useEffect, useState, useRef  } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 import { RctCard, RctCardContent } from 'Components/RctCard';
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Button, Form, FormGroup, Label, } from 'reactstrap';
 import { useReactToPrint } from 'react-to-print';
 import PrintPdf from '../../alaimtidad-balances/print-balances/print-pdf';
 import { Helmet } from "react-helmet";
 import MUIDataTable from "mui-datatables";
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 
 
 
 export default function Shop(props) {
     const history = useHistory();
     const componentRef = useRef();

     const handlePrint = useReactToPrint({
         content: () => componentRef.current,
         onAfterPrint: () => { 
             setState({
                 ...state, 
                 disabled: true
             })
             location.reload()
         },
     });
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
         from_date: '',
         to_date: '',
         transaction_id:'',
         transation_number:'',
         search_by:'',
         account_number:'',
         cardsForPrintPdf: [],
         receipts: [],
         costcetners:[],
         disabled: false,
         audit_transaction_id:'',
         transaction_id:'',
         acc_number:'',
         trans_no: '',
         acc_name:'',
         amount: '',
         currency_ar: '',
         audit_type_ar: '',
         business_unit_name: '',
         business_unit_id:'',
         notes:'',
         entry_date: '',
         transaction_type_ar: '',
         hiddenStatusForm: true,
         hiddenDebitLimitForm: true
     });
 
     const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
     };

     useEffect(() => {
        var date = new Date();
		date = date.getUTCFullYear() + '-' +
			('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
			('00' + date.getUTCDate()).slice(-2);

        axios.get('https://accbackend.alaimtidad-itland.com/all-business-units', USER_TOKEN).then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else {

                setState({
                    ...state,
                    costcetners: response.data.businessUnits,
                    from_date: date,
                    to_date: date,
                })
            }
        })
            .catch(error => {

                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                    
                setState({
                    ...state,
                    from_date: date,
                    to_date: date,
                })
                }
            });
     }, []);
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
     const onSubmitNoupdate = (audit_transaction_id) => {

        let data = {
            transaction_id: audit_transaction_id,

        }
        console.log("data", data);

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions/commit', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res", res.data);
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            else {
                console.log("res", res.data);
                if (res.data.message === "transaction is committed")
                    toast.success("تم الترحيل بنجاح", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }

     const onSubmit = e => {

        let data = {
            transaction_id: state.audit_transaction_id,
            trans_no: state.trans_no,
            amount: state.amount,
            entry_date: state.entry_date,
            notes: state.notes,
            business_unit_id:state.business_unit_id,
        }
        console.log("data", data);

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions/update', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res", res.data);
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            else {
                console.log("res", res.data);
                if (res.data.message === "transaction is updated")
                    toast.success("تم التعديل بنجاح", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }
     const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                  maxHeight: 'unset',
                  overflowX: 'unset',
                  overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }       
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
    const downloadPdf = () => {
		setState({ ...state, cardsForPrintPdf: objArr })
		setTimeout(function () { handlePrint() }, 2000)
    }
    const onClearStatusClicked = () => {
        setState({
               ...state,
               hiddenStatusForm: true,
               audit_transaction_id:'',
               transaction_id:'',
               acc_number:'',
               trans_no: '',
               amount: '',
               currency_ar: '',
               audit_type_ar: '',
               business_unit_name: '',
               business_unit_id:'',
               notes:'',
               entry_date: '',
               transaction_type_ar: '',
        })
    };
    const showDetails = e => {
             let data ={
				fromDate: state.from_date,
				toDate: state.to_date,
				transaction_id: state.transaction_id,
                  }
                axios({
                    method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions/get', data: data,
                    headers: { "x-access-token": `${cookies.get('UserToken')}` }
                }).then(res => {
				if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res.data.message.length === 0) {
						toast.error("لا يوجد قيد بهذا الرقم", {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: [],
						})
					}
				
					else {
						setState({
							...state,
							receipts: res.data.message,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

	const showDetails2 = e => {
        let data ={
            fromDate: state.from_date,
            toDate: state.to_date,
            account_number: state.account_number,
              }
            axios({
                method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions/get', data: data,
                headers: { "x-access-token": `${cookies.get('UserToken')}` }
            }).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.message.length === 0) {
						toast.error("لا يوجد قيد بهذا الرقم", {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: [],
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.message,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}
    const showDetails3 = e => {
        let data ={
            fromDate: state.from_date,
            toDate: state.to_date,
            transation_number: state.transation_number,
              }
            axios({
                method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions/get', data: data,
                headers: { "x-access-token": `${cookies.get('UserToken')}` }
            }).then(res2 => {
				if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
					cookies.remove('UserToken', { path: '/' })
					window.location.href = "/signin";
				}
				else {
					if (res2.data.message.length === 0) {
						toast.error("لا يوجد قيد بهذا الرقم", {
							position: "top-center",
							autoClose: 4000,
							hideProgressBar: false,
							closeOnClick: true,
							pauseOnHover: true,
							draggable: true
						});
						setState({
							...state,
							receipts: [],
						})
					}
					else {
						setState({
							...state,
							receipts: res2.data.message,
						})
					}
				}
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
	}

    let totalBalancesToBePrinted = 0;
	let selectedBalances = [];
	let objForPrintPdf = {};
	let objArr = [];
	const handlemultiSelect = (selectedRows,displayData) => {
		
		selectedBalances = [];
		objArr = [];
		objForPrintPdf = {};

		selectedRows.data.forEach(element => {
			selectedBalances.push(parseLocaleNumber(displayData[element.index].data[4]));

			objForPrintPdf = {
				"pin": displayData[element.index].data[2],
				"serial": displayData[element.index].data[1],
				"balance": displayData[element.index].data[4],
				"date": displayData[element.index].data[8],
			};
			objArr.push(objForPrintPdf)
		});
		
		totalBalancesToBePrinted = 0;
		for (let i = 0; i< selectedBalances.length; i++) {
			totalBalancesToBePrinted += selectedBalances[i];	
		}
		return(
			<React.Fragment>
				<div className="form-group col-md-12 mt-3 text-center">
					<h2 className="mb-3"><IntlMessages id="form.balancesToPrint" /></h2>
					<h2>{nf.format(totalBalancesToBePrinted)}</h2>
					<br />
					<div className="container">
						<div className="row justify-content-center">	
							{(state.disabled === false) ? 
								<button className="btn btn-primary ml-4 mr-3" 
										onClick={(e) => downloadPdf()}>
									طباعة PDF
								</button>
								: 
								<button className="btn btn-primary ml-4 mr-3" 
										disabled={true}>
									طباعة PDF
								</button>
							}
						</div>
					</div>
				</div>
			</React.Fragment>
		)
	}

 
     const { match } = props;
     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>سندات قيد </title>
                         <meta name="description" content="سندات قيد" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.allinvoices" />} match={match} />
                     <div className="row">
                         {(state.hiddenStatusForm == true)?
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.allinvoices" />}>
                           
                            
							<div className="form-row">
								<div className="form-group col-md-12">
									<div className="row">
										<div className="form-group col-md-4">
											<label><IntlMessages id="form.searchBy" /></label>
											<select className="form-control" name="search_by"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار وسيلة البحث</option>
												<option key="1" value="transno">رقم القيد</option>
                                                <option key="2" value="accountno">رقم الحساب</option>
												<option key="3" value="serial">المسلسل</option>
                                              
											</select>
											<span className="errors">
												{errors.search_by && errors.search_by.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
                                        
										{(state.search_by == "transno") ? 
											<Form inline onSubmit={handleSubmit(showDetails)}>
                                             <div className="row">
                                             <div className="form-group col-md-3">
                                                <h3><IntlMessages id="table.restrictionNo" /></h3>
                                                <input type="number" className="form-control" name="transaction_id"
                                                    onChange={handleFields}
                                                    ref={register({ required: true })} />
                                           
                                            <span className="errors m-2">
                                                {errors.transaction_id && errors.transaction_id.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                             
                                            </span>
                                            </div>
								<div className="form-group col-md-3">
									<h3><IntlMessages id="form.dateFrom" /></h3>
									<input name="from_date" placeholder="date placeholder"
										defaultValue={state.from_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.from_date && errors.from_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								<div className="form-group col-md-3">
									<span><IntlMessages id="form.dateTo" /></span>
									<input name="to_date" placeholder="date placeholder"
										defaultValue={state.to_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.to_date && errors.to_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
                                <div className="form-group col-md-3">
                                            <Button className="mb-10" color="primary"><IntlMessages id="form.showDetails" /></Button>
                                            </div>
							</div>
                           
                                        </Form>
										:(state.search_by == "accountno")? 
											<Form inline onSubmit={handleSubmit(showDetails2)}>
											 <div className="row">
                                             <div className="form-group col-md-3">
                                             <h3><IntlMessages id="table.accountNo" /></h3>
													<input type="number" className="form-control" name="account_number"
														onChange={handleFields}
														ref={register({ required: true })} />
											

												<span className="errors m-2">
													{errors.account_number && errors.account_number.type === 'required' &&
														<IntlMessages id="form.requiredError" />}
													
												</span>
                                                </div>
								<div className="form-group col-md-3">
									<h3><IntlMessages id="form.dateFrom" /></h3>
									<input name="from_date" placeholder="date placeholder"
										defaultValue={state.from_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.from_date && errors.from_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								<div className="form-group col-md-3">
									<span><IntlMessages id="form.dateTo" /></span>
									<input name="to_date" placeholder="date placeholder"
										defaultValue={state.to_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.to_date && errors.to_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
						
                                    </div>	
                                <div className="form-group col-md-3">
									<Button className="btn btn-primary" color="primary" ><IntlMessages id="form.showDetails" /></Button>
                            </div>
                            </div>		
                            	</Form>
										:(state.search_by == "serial")? 
											<Form inline onSubmit={handleSubmit(showDetails3)}>
											   <div className="row">
                                               <div className="form-group col-md-3">
													<h3><IntlMessages id="table.serial" /></h3>
													<input type="number" className="form-control " name="transation_number"
														onChange={handleFields}
														ref={register({ required: true })} />
											

												<span className="errors m-2">
													{errors.transation_number && errors.transation_number.type === 'required' &&
														<IntlMessages id="form.requiredError" />}
												
												</span>
                                            	</div> 
								<div className="form-group col-md-3">
									<h3><IntlMessages id="form.dateFrom" /></h3>
									<input name="from_date" placeholder="date placeholder"
										defaultValue={state.from_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.from_date && errors.from_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
								<div className="form-group col-md-3">
									<span><IntlMessages id="form.dateTo" /></span>
									<input name="to_date" placeholder="date placeholder"
										defaultValue={state.to_date}
										type="date" className="form-control" onChange={handleFields}
										ref={register({ required: true })} />
									<span className="errors m-2">
										{errors.to_date && errors.to_date.type === 'required' &&
											<IntlMessages id="form.requiredOptionError" />}
									</span>
								</div>
                                <div className="form-group col-md-3">
									<Button className="btn btn-primary" color="primary" ><IntlMessages id="form.showDetails" /></Button>
                            </div>
							</div>
												
											</Form>
                                            :
                                            <span></span>
										}
									</div>
								</div>
							</div>
                         
                           
                            <br />
							<br />
							<div style={{ display: "none" }}>
            					<PrintPdf ref={componentRef} {...state} />	
        					</div>
							<RctCard>
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											data={state.receipts}
											columns={[
                                                {
                                                    label: "id",
                                                    name: "audit_transaction_id",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    }
                                                },
                                                {
													label: <IntlMessages id="table.restrictionNo" />,
													name: "transaction_id",
												},
                                            
                                                {
                                                    label: <IntlMessages id="table.accountNo" />,
                                                    name: "acc_number"
                                                },
                                                    
                                                {
                                                    label: <IntlMessages id="form.accountname" />,
                                                    name: "acc_name_ar"
                                                },
                                                {
													label: <IntlMessages id="table.serial" />,
													name: "trans_no"
												},

												{
													label: <IntlMessages id="table.discount_amount" />,
													name: "amount"

												},
												{
													label: <IntlMessages id="form.cointype" />,
													name: "currency_ar"
												},
												{
													label: <IntlMessages id="FORM.DEBT" />,
													name: "audit_type_ar"
												},
												{
													label: <IntlMessages id="form.costcenterName" />,
													name: "business_unit_name"

												},
												
												{
													label: <IntlMessages id="form.Statement" />,
													name: "notes",
												},
												{
													label: <IntlMessages id="form.restrictionDate" />,
													name: "entry_date"
												},
												{
													label: <IntlMessages id="form.actiontype" />,
													name: "transaction_type_ar",
												},
                                                {
													label: "id",
													name: "transaction_type_id",
                                                    options: {
                                                    display: "none",
                                                    filter: false,
                                                    print: false,
                                                    }
												},
                                                {
                                                    label:<IntlMessages id="form.update" /> ,
                                                    name: "",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        print: false,
                                                        customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                {(tableMeta.rowData[12] === 1 ) ? 
                                                                  <button type="button" className="btn btn-warning" 
                                                                  onClick={() => {
                                                                    
                                                                        setState({
                                                                         ...state , 
                                                                   
                                                                         audit_transaction_id:tableMeta.rowData[0],
                                                                         transaction_id:tableMeta.rowData[1],
                                                                         acc_number:tableMeta.rowData[2],
                                                                         acc_name:tableMeta.rowData[3],
                                                                         trans_no: tableMeta.rowData[4],
                                                                         amount: tableMeta.rowData[5],
                                                                         currency_ar: tableMeta.rowData[6],
                                                                         audit_type_ar: tableMeta.rowData[7],
                                                                         business_unit_name: tableMeta.rowData[8],
                                                                         notes: tableMeta.rowData[9],
                                                                         entry_date: tableMeta.rowData[10],
                                                                         transaction_type_ar: tableMeta.rowData[11],
                                                                         hiddenStatusForm: false,
                                                                         hiddenDebitLimitForm: true
                                                                        });
                                                                       
                                                          
                                                                 }}>
                                                                  <IntlMessages id="form.update" />
                                                                  </button>
                                                                : 
        
                                                                <span></span>
                                                                
                                                                }
                                                              
                                                            </React.Fragment>
                                                        );
                                                    }
                                                    }
                            
                                                },
                                                {
                                                    label:<IntlMessages id="sidebar.moneycash" /> ,
                                                    name: "",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        print: false,
                                                        customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                {(tableMeta.rowData[12] == 1 ) ? 
                                                                  <button type="button" className="btn btn-primary" 
                                                                  onClick={() => {onSubmitNoupdate(tableMeta.rowData[0]) }}>
                                                                  <IntlMessages id="sidebar.moneycash" />
                                                                  </button>
                                                                : 
        
                                                                <span></span>
                                                                
                                                                }
                                                              
                                                            </React.Fragment>
                                                        );
                                                    }
                                                    }
                            
                                                },
                                                {
                                                    label:<IntlMessages id="button.delete" /> ,
                                                    name: "",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        print: false,
                                                        customBodyRender: (value, tableMeta, updateValue) => {
                                                        return (
                                                            <React.Fragment>
                                                                {(tableMeta.rowData[12] == 1 ) ? 
                                                                  <button type="button" className="btn btn-danger" 
                                                                  onClick={() => {
                                                                    
                                                                        // setState({
                                                                        //  ...state , 
                                                                        //  selected_user:tableMeta.rowData,
                                                                        //  accountNameAr:tableMeta.rowData[5],
                                                                        //  accountDescription:tableMeta.rowData[9],
                                                                        //  accountNameEn:tableMeta.rowData[6],
                                                                        //  currency_ar: tableMeta.rowData[7],
                                                                        //  audit_type_ar: tableMeta.rowData[8],
                                                                        //  accountTypeId: tableMeta.rowData[15],
                                                                        //  accountCurrencyId: tableMeta.rowData[14],
                                                                        //  hiddenStatusForm: false,
                                                                        //  hiddenDebitLimitForm: true
                                                                        // });
                                                                       
                                                          
                                                                 }}>
                                                                  <IntlMessages id="form.delete" />
                                                                  </button>
                                                                : 
        
                                                                <span></span>
                                                                
                                                                }
                                                              
                                                            </React.Fragment>
                                                        );
                                                    }
                                                    }
                            
                                                },
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
 
                         </RctCollapsibleCard>
                         :
                         <RctCollapsibleCard>
                             	<RctCard>
                             	<RctCardContent> 
                <form onSubmit={handleSubmit(onSubmit)}>	
        
                         <div className="form-row">
                             
                      
                             <div className="form-group col-md-4">
                             <label><IntlMessages id="widgets.status" /></label>
                 
                             <select className="form-control" name="business_unit_id" 
                                     onChange={handleFields} ref={register({ required: true })} >  
                                     <option key="0" value="">اختر مركز التكلفة</option>
                                     {state.costcetners.map(cost => {
												return (
													<option key={cost.business_unit_id} value={cost.business_unit_id}>
														{cost.business_unit_name}
													</option>
												)
											})
											}    
                             </select>
                             <span className="errors">
                                 {errors.business_unit_id && errors.business_unit_id.type === 'required' &&
                                     <IntlMessages id="form.requiredOptionError" /> }
                             </span>
                             </div>
                         </div>
                         <div className="form-row">
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.serial" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid", 
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="number" className="form-control" defaultValue={state.trans_no} name="trans_no" 
                                             onChange={handleFields}
                                             ref={register({ required: true})}   />
                                                  </div>
                                         <span className="errors">
                                             {errors.trans_no && errors.trans_no.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                       
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.discount_amount" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <input type="text" className="form-control" defaultValue={state.amount} name="amount"
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                            </div>
                                         <span className="errors">
                                             {errors.amount && errors.amount.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                        
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.cointype" /></label>
                                         <div >
                                           <label className="middletitle">{state.currency_ar}</label>
                          </div>
                      
                                     </div>
                                 </div>
                             
      
 
                                 <div className="form-row">
                                 <div className="form-group col-md-4">
                                         <label><IntlMessages id="FORM.DEBT" /></label>
                                         <div >
                                         <label className="middletitle">{state.audit_type_ar}</label>
                          </div>
                    
                                     </div>

                                

                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.accountNo" /></label>
                                         <div >
                                         <label className="middletitle">{state.acc_number}</label>
                          </div>
                    
                                     </div>

                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountname" /></label>
                                         <div >
                                         <label className="middletitle">{state.acc_name}</label>
                          </div>
                    
                                     </div>

                                     </div>
                                
                                     <div className="form-row">
                                     <div className="form-group col-md-6">
                                        
                                         <label><IntlMessages id="form.restrictionDate" /></label>
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  	<input name="entry_date" placeholder="date placeholder"
                                                    defaultValue={state.entry_date}
                                                    type="date" className="form-control" onChange={handleFields}
                                                    ref={register({ required: true })} />
                                                <span className="errors m-2">
                                                    {errors.entry_date && errors.entry_date.type === 'required' &&
                                                        <IntlMessages id="form.requiredOptionError" />}
                                                </span>
                                            </span>
                                            <span className="errors">
                                                {errors.entry_date && errors.entry_date.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}

                                            </span>
                                     </div>

                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.Statement" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
    borderWidth: "0px 2px 2px 0px",
    lineHeight: "0px",
    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
   }}>
                                         <textarea  className="form-control" name="notes"rows={3} cols={5}
                                             onChange={handleFields} defaultValue={state.notes}
                                             ref={register({ required: true, minLength: 3, pattern: /^[A-Za-zء-ي_ ]+$/i, })}  />
                                          </div>
                                         <span className="errors">
                                             {errors.notes && errors.notes.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                             {errors.notes && errors.notes.type === 'minLength' &&
                                                 <IntlMessages id="form.minLengthError" />}
                                             {errors.notes && errors.notes.type === 'pattern' &&
                                                 <IntlMessages id="form.lettersOnlyError" />}
                                         </span>
                                     </div>

                                     </div>

                         {(state.disabled === false) ? 
                             <button type="submit" className="btn btn-warning">
                             <IntlMessages id="form.update" />
                             </button> : 
                             <button type="submit" className="btn btn-warning" disabled={true} >
                             <IntlMessages id="form.update" />
                             </button>
                             }
 

             <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                             <IntlMessages id="form.clear" />
                         </button>
                     </form>
                     </RctCardContent>
							</RctCard>
                     </RctCollapsibleCard>
                        }
                     </div>
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }











         </React.Fragment>
     )
 }