/**
 * add supplier
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 import { RctCard, RctCardContent } from 'Components/RctCard';
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import MUIDataTable from "mui-datatables";
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 
 
 
 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
          currency: '',
          secretdegree:'',
          Statement:'',
          accountname:'',
          equality:'',
          creditor:'',
          debtor:'',
          Equivalent:'',
         costcetners:[{currency:'دينار عراقي',secretdegree:"عالي",Statement:"",accountname:'',equality:'',creditor:'',debtor:'',Equivalent:''}],
         disabled: false,
         selected_user:[],
         hiddenStatusForm: true,
         hiddenDebitLimitForm: true
     });
 
  
     const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
     };
    let newDate =new  Date();
     useEffect(() => {
     }, []);
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
 
     const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                  maxHeight: 'unset',
                  overflowX: 'unset',
                  overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }       
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
 

     const { match } = props;
     // console.log(state);
     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>اضافة سند قيد</title>
                         <meta name="description" content="اضافة سند قيد" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.addinvoice" />} match={match} />
                     <div className="row">
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.addinvoice" />}>
                                 <div className="form-row"> 
                                      <div className="form-group col-md-4">
                                   <div className="form-row"> 
                              <label><IntlMessages id="form.restrictionDate" /></label>
                             <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {new Date().getSeconds()}:{new Date().getMinutes()}:{new Date().getHours()}&nbsp;&nbsp;&nbsp;{new Date().getFullYear()}/{new Date().getDate()}/{new Date().getMonth()+1}</span>
                              </div>
                              </div>
                              <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.serial" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="currency"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.currency && errors.currency.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                         </div>
                                     
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.currency" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="currency"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.currency && errors.currency.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                    
                              </div> 
                             <form onSubmit={handleSubmit(onSubmit)}>
                                 <div className="form-row">
                                   
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.secretdegree" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="secretdegree"
                                            value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.secretdegree && errors.secretdegree.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}                                    
                                         </span>
                                     </div>


                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.creditor" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="creditor"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.creditor && errors.creditor.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="table.debtor" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="debtor"
                                            value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.debtor && errors.debtor.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}                                    
                                         </span>
                                     </div>
            
                                 </div>
                           


                                 <div className="form-row">

                                 <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.accountname" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="accountname"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.accountname && errors.accountname.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.costcenterName" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="costcenterName"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.costcenterName && errors.costcenterName.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="widgets.note" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="debtor"
                                            value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.debtor && errors.debtor.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}                                    
                                         </span>
                                     </div>
                                  
                                 </div>
                                 <div className="form-row">
                                     <div className="form-group col-md-12">
                                         <label><IntlMessages id="form.Statement" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="Statement"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.Statement && errors.Statement.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                 </div>

                                 {(state.disabled === false) ? 
                                     <button type="submit" className="btn-margin" style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className="btn-margin" disabled={true} style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
<div style={{borderColor:'#000',borderWidth:'100px',border:'ridge',padding: "10px",backgroundColor:'#D2D2D2'}}>
                                   <div className="form-row">
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Creditor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>-</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Debitor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} /> 
                                       
                                     </div>

                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>=</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="widgets.results" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     </div>
                                     </div>



                                 
                             
                             </form>
 
 
                         </RctCollapsibleCard>
                     </div>
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }


{/* ----------------------------------------Tableeeeeeeeeeeeeeeeeee------------------------------------ */}


 <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                    data={state.costcetners}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                 label: <IntlMessages id="form.currency" />,
                                                 name:  "currency",
                                             },
                                             {
                                                 label: <IntlMessages id="form.secretdegree" />,
                                                 name:  "secretdegree"
                                             },
                                             {
                                                 label: <IntlMessages id="form.Statement" />,
                                                 name:  "Statement",
                                             },
                                             {
                                                label: <IntlMessages id="table.creditor" />,
                                                name:  "creditor",
                                            },
                                            {
                                                label: <IntlMessages id="table.debtor" />,
                                                name:  "debtor",
                                            },
                                             {
                                                label: <IntlMessages id="form.costcenterName" />,
                                                name:  "accountname",
                                            },
                                            {
                                                label: <IntlMessages id="widgets.note" />,
                                                name:  "firstcredit",
                                            },
                                            {
                                                label: <IntlMessages id="form.accountname" />,
                                                name:  "form.virtualMoneyBalance",
                                            },
                                          
                                             {
                                                 label: <IntlMessages id="form.update" />,
                                                 name: "",
                                                 options: {
                                                     filter: true,
                                                     sort: false,
                                                     empty: true,
                                                     print: false,
                                                     customBodyRender: (value, tableMeta, updateValue) => {
                                                     return (
                                                         <React.Fragment>
                                                             <button type="button" className="btn btn-primary" 
                                                             onClick={() => {
                                                                 console.log("HIIIII",tableMeta.rowData[4]);
                                                                    setState({
                                                                     ...state , 
                                                                     selected_user:tableMeta.rowData,
                                                                     hiddenStatusForm: false,
                                                                     hiddenDebitLimitForm: true
                                                                    });
                                                             }}>
                                                             <IntlMessages id="form.update" />
                                                             </button>
                                                         </React.Fragment>
                                                     );
                                                 }
                                                 }
                         
                                             },
                                             {
                                                label: <IntlMessages id="form.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger" 
                                                            onClick={() => {
                                                                console.log("HIIIII",tableMeta.rowData[4]);
                                                                   setState({
                                                                    ...state , 
                                                                    selected_user:tableMeta.rowData,
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true
                                                                   });
                                                            }}>
                                                            <IntlMessages id="form.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>









         </React.Fragment>
     )
 }