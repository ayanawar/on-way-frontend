/**
 * add supplier
 */
import React, { useEffect, useState ,useRef} from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RctCard, RctCardContent } from 'Components/RctCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import PrintReceivableReceipt from './print-receivable-receipt';
import { useReactToPrint } from 'react-to-print';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import MUIDataTable from "mui-datatables";
const cookies = new Cookies();
const Token = cookies.get('UserToken');

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const componentRef = useRef();

    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
        onAfterPrint: () => {
        
            location.reload()
        },
    });
    const [nameFocused, setNameFocused] = useState(false)
    const [state, setState] = useState({
        restrictionDate: '',
        todayDate: `${new Date().getSeconds()}:${new Date().getMinutes()}:${new Date().getHours()} ${new Date().getFullYear()}/${new Date().getDate()}/${new Date().getMonth() + 1}`,
        serial: '',
        accountnumber: '',
        amount: '',
        accounts: [],
        costcetners: [],
        tablearray:[],
        costcenter: '',
        tableid:0,
        statment:'',
        costcenterid:'',
        recieptid:'',
        coin_type:'',
        debt:'',
        hiddenStatusForm: true,
        hidden: true,
        hiddenDebitLimitForm: true,
        hiddentable : true,
        tablearray2:[],
    });


    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5, 10, 25, 50, 100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
    };
    let newDate = new Date();
    useEffect(() => {
        
        axios.get('https://accbackend.alaimtidad-itland.com/all-business-units', USER_TOKEN).then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else {

                setState({
                    ...state,
                    costcetners: response.data.businessUnits,

                })
            }
        })
            .catch(error => {

                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });
    }, []);

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                    maxHeight: 'unset',
                    overflowX: 'unset',
                    overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                },
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar: {
                regular: {
                    backgroundColor: "gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor: "gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
    const showDetails = e => {

        let data = {
            accountNumber: state.accountnumber,
        }

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/getaccountdata', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res no account", res.data);
                setState({...state, hidden:true })
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            
            }
            else {
                console.log("res", res.data.message);
                setState({ ...state, disabled: true, accounts: res.data.message, hidden: false })

            }
        }).catch(error => {
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                setState({...state, hidden:true })
            }
        });
    }
    const onSubmit = e => {

        let data = {
      
            dailyTransactions: state.tablearray2
        }
        console.log("data", data);

        axios({
            method: 'post', url: 'https://accbackend.alaimtidad-itland.com/transactions', data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}` }
        }).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            }
            else if (res.data.message.length === 0) {
                console.log("res", res.data);
                toast.error(<IntlMessages id="components.NoItemFound" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                });
            }
            else {
                console.log("res", res.data);
                if (res.data.message === "transaction created successfully")
                    toast.success("تمت العملية بنجاح", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true, recieptid: res.data.transId })
                setTimeout(function () { handlePrint() }, 2000)
            }
        }).catch(error => {
            console.log(error);
            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        });

    }


    const removeRow = (r) => {

        for (var i = 0; i < state.tablearray.length; i++) {
            console.log(state.tablearray[i].tableid == r);
            console.log(state.tablearray[i].tableid);
            console.log(r);
            if (state.tablearray[i].tableid == r) {
                console.log("state.tablearray[i]", state.tablearray[i]);
                state.tablearray.splice(i, 1);
                break;
            }

        }
        console.log("event", state.tablearray);
        setState({ ...state, tablearray: state.tablearray })
        if (state.tablearray.length == 0) {
            setState({ ...state, hiddentable: true })
        }


    }

    const pushinarray = () => {
        console.log("ENTEREDD ");
  console.log("state",state);


        var e = document.getElementById('costcenter')

            let obj = {
                tableid: state.tableid,
                costcenter: e?.options[e?.selectedIndex].text,
                amount: state.amount,
                todayDate: state.todayDate,
                restrictionDate: state.restrictionDate,
                statment: state.statment,
                serial: state.serial,
                accountnumber: state.accountnumber,
                coin_type: state.accounts[0].currency_ar,
                debt: state.accounts[0].audit_type_ar,
                transactionNumber: state.serial,
                auditTypeId:state.accounts[0].audit_type_id,
                currencyId:state.accounts[0].currency_id,
                entryDate:state.restrictionDate,
                accountId: state.accounts[0].acc_id,
                notes:state.statment,
                businessUnitId:state.costcenterid,
                securityLevelId:state.accounts[0].security_level_id,
                transTypeId:1,
            }
            let obj2 = {
                amount: state.amount,
                transactionNumber: state.serial,
                auditTypeId:state.accounts[0].audit_type_id,
                currencyId:state.accounts[0].currency_id,
                entryDate:state.restrictionDate,
                accountId: state.accounts[0].acc_id,
                notes:state.statment,
                businessUnitId:state.costcenterid,
                securityLevelId:state.accounts[0].security_level_id,
                transTypeId:1,
            }
            console.log("obj",obj);
            state.tablearray.push(obj)
            state.tablearray2.push(obj2)
            setState({
                ...state, disabled: false, hiddentextbox: false, hiddentable: false,tablearray2: state.tablearray2, tablearray: state.tablearray, tableid: (state.tableid + 1)
            })

        



    }
    const { match } = props;
    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="shop-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>سند يومية</title>
                        <meta name="description" content="سند يومية" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.dailyinvoice" />} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.dailyinvoice" />}>
                            <form onSubmit={handleSubmit(pushinarray)}>
                                <div className="form-row">
                                    <div className="form-group col-md-4">
                                        <div className="form-row">
                                            <label><IntlMessages id="form.restrictionDate" /></label>
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  	<input name="restrictionDate" placeholder="date placeholder"
                                                    defaultValue={state.from_date}
                                                    type="date" className="form-control" onChange={handleFields}
                                                    ref={register({ required: true })} />
                                                <span className="errors m-2">
                                                    {errors.from_date && errors.from_date.type === 'required' &&
                                                        <IntlMessages id="form.requiredOptionError" />}
                                                </span>
                                            </span>
                                            <span className="errors">
                                                {errors.restrictionDate && errors.restrictionDate.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}

                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-group col-md-4">
                                        <div className="form-row">
                                            <label><IntlMessages id="form.todayDate" /></label>
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {new Date().getSeconds()}:{new Date().getMinutes()}:{new Date().getHours()}&nbsp;&nbsp;&nbsp;{new Date().getFullYear()}/{new Date().getDate()}/{new Date().getMonth() + 1}</span>
                                        </div>
                                    </div>
                                </div>


                                <div className="form-row">
                                    <div className="form-group col-md-4">
                                        <label><IntlMessages id="table.serial" /></label>
                                        <div style={{
                                            position: "relative", margin: "0 30px", borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                        }}>

                                            <input type="text" className="form-control" name="serial"

                                                onChange={handleFields}
                                                ref={register({ required: true })} />
                                        </div>
                                        <span className="errors">
                                            {errors.serial && errors.serial.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}

                                        </span>
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label>
                                            <IntlMessages id="table.accountNo" />
                                        </label>

                                        <div style={{
                                            position: "relative", margin: "0 30px", borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                        }}>
                                            <input type="number" className="form-control" name="accountnumber"
                                                onChange={handleFields}
                                                onBlur={()=>showDetails()}
                                                ref={register({ required: true, pattern: /^[0-9]+$/ })}
                                            />
                                        </div>
                                        <span className="errors">
                                            {errors.accountnumber && errors.accountnumber.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}
                                            {errors.accountnumber && errors.accountnumber.type === 'pattern' &&
                                                <IntlMessages id="form.numbersOnlyErrorError" />}

                                        </span>


                                    </div>
                                    {/* <div className="form-group col-md-4">
                                        {(state.accountnumber === '' && state.accounts.length == 0  ) ? <React.Fragment></React.Fragment>

                                            :
                                            <button onClick={() => { showDetails() }} type='button' className=" btn-margin" style={{
                                                margin: "0 30px", color: '#fff', backgroundColor: "#0063c1",
                                                fontSize: "17px", fontWeight: "bold", height: '50px'
                                            }}>
                                                <IntlMessages id="form.showDetails" />
                                            </button>
                                        }

                                    </div> */}
                                </div>

                                {(state.hidden == true || state.accountnumber === '') ? <React.Fragment></React.Fragment>
                                    :
                                    <div className="form-row">
                                        <div className="form-group col-md-3">
                                            <label><IntlMessages id="form.accountname" /></label>
                                            <label>{state.accounts[0].acc_name_ar}</label>
                                        </div>
                                        <div className="form-group col-md-3">
                                            <label><IntlMessages id="form.cointype" /></label>
                                            <label>{state.accounts[0].currency_ar}</label>
                                        </div>
                                        <div className="form-group col-md-3">
                                            <label><IntlMessages id="FORM.DEBT" /></label>
                                            <label>{state.accounts[0].audit_type_ar}</label>
                                        </div>
                                        <div className="form-group col-md-3">
                                            <label>
                                                <IntlMessages id="form.secretdegree" />
                                            </label>
                                            <label>{state.accounts[0].security_level_type_ar}</label>


                                        </div>

                                    </div>
                                }
                                <br />
                                <div className="form-row">

                                    <div className="form-group col-md-6">
                                        <label><IntlMessages id="table.discount_amount" /></label>
                                        <div style={{
                                            position: "relative", margin: "0 30px", borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                        }}>

                                            <input type="number" className="form-control" name="amount" onChange={handleFields}
                                                ref={register({ required: true })} />
                                        </div>
                                        <span className="errors">
                                            {errors.amount && errors.amount.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}

                                        </span>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label><IntlMessages id="form.costcenterName" /></label>
                                        <select className="form-control" name="costcenterid" id="costcenter"
                                            onChange={handleFields} ref={register({ required: true })}>
                                            <option key="0" value="">اختر مراكز التكلفة</option>
                                            {state.costcetners.map(costcetner => {
                                                return (
                                                    <option key={costcetner.business_unit_id} value={costcetner.business_unit_id}>
                                                        {costcetner.business_unit_name}
                                                    </option>
                                                )
                                            })
                                            }
                                        </select>
                                        <span className="errors">
                                            {errors.costcenterid && errors.costcenterid.type === 'required' &&
                                                <IntlMessages id="form.requiredOptionError" />}
                                        </span>
                                    </div>


                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label><IntlMessages id="form.Statement" /></label>
                                        <div style={{
                                            position: "relative", margin: "0 30px", borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                        }}>

                                            <input type="text" className="form-control" name="statment"

                                                onChange={handleFields}
                                                ref={register({ required: true })} />
                                        </div>
                                        <span className="errors">
                                            {errors.statment && errors.statment.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}

                                        </span>
                                    </div>
                                </div>
                                <button type="submit"
                                                    className=" btn-margin" style={{
                                                        margin: "30px 30px 20px 0", color: '#fff', backgroundColor: "#0063c1",
                                                        fontSize: "17px", fontWeight: "bold", height: '50px'
                                                    }}>
                                                    <IntlMessages id="form.add" />
                                                </button>

                            </form>


                        </RctCollapsibleCard>
                    </div>
                </div>
                : (

                    history.push("/access-denied")
                )
            }


            {/* ----------------------------------------Tableeeeeeeeeeeeeeeeeee------------------------------------ */}
            <div style={{ display: "none" }}>
                        <PrintReceivableReceipt ref={componentRef} {...state} />
                    </div>
                    {(state.hiddentable == true) ? <React.Fragment></React.Fragment>
                        :

            <div className="row mb-5">
                <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                    <RctCard>
                        <RctCardContent noPadding>
                            <MuiThemeProvider theme={getMuiTheme()}>
                                <MUIDataTable
                                      id="myTable"

                                      data={state.tablearray}
                                    columns={[
                                        {
                                            label: "id",
                                            name: "tableid",
                                            options: {
                                                display: "none",
                                                filter: false,
                                                print: false,
                                            }
                                        },
                                        {
                                            label: <IntlMessages id="table.serial" />,
                                            name: "serial",
                                        },
                                        {
                                            label: <IntlMessages id="table.accountNo" />,
                                            name: "accountnumber"
                                        },
                                        {
                                            label: <IntlMessages id="table.discount_amount" />,
                                            name: "amount",
                                        },
                                    
                                        {
                                            label: <IntlMessages id="form.cointype" />,
                                            name: "coin_type",
                                        },
                                        {
                                            label: <IntlMessages id="FORM.DEBT" />,
                                            name: "debt",
                                        },
                                        {
                                            label: <IntlMessages id="form.costcenterName" />,
                                            name: "costcenter",
                                        },
                                        {
                                            label: <IntlMessages id="form.Statement" />,
                                            name: "statment",
                                        },
                                        {
                                            label: <IntlMessages id="button.delete" />,
                                            name: "tableid",
                                            options: {
                                                filter: true,
                                                sort: false,
                                                empty: true,
                                                print: false,
                                                customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger"
                                                                onClick={
                                                                    () => {
                                                                        removeRow(value)


                                                                    }}>
                                                                <IntlMessages id="form.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                            }

                                        },

                                    ]}
                                    options={options}
                                />
                            </MuiThemeProvider>
                        </RctCardContent>
                    </RctCard>
                </RctCollapsibleCard>
            </div>
}



<div style={{ marginTop: "-10px",direction:"ltr" , display: 'flex', alignSelf: 'flex-end', marginBottom: "20px" }}>
                        {(state.hiddentable == true) ? <React.Fragment></React.Fragment>
                            :
                            (state.disabled === false) ?
                                <button onClick={() => { onSubmit() }}
                                    className=" btn btn-primary" style={{
                                        margin: "0px 30px 70px 0", color: '#fff', backgroundColor: "#0063c1",
                                        fontSize: "17px", fontWeight: "bold", height: '50px'
                                    }}>
                                    <IntlMessages id="form.getrerestriction" />
                                </button>
                                :
                                <button className=" btn btn-primary" disabled={true} style={{
                                    margin: "0px 30px 70px 0", color: '#fff', backgroundColor: "#0063c1",
                                    fontSize: "17px", fontWeight: "bold", height: '50px'
                                }}>
                                    <IntlMessages id="form.getrerestriction" />
                                </button>
                        }
                    </div>




        </React.Fragment>
    )
}