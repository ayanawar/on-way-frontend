import moment from 'moment';
import React, { Component } from 'react';
import IntlMessages from 'Util/IntlMessages';
import logo from '../../../assets/img/icon10.jpg';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MUIDataTable from "mui-datatables";
const nf = new Intl.NumberFormat();

class PrintReceivableReceipt extends Component {
    constructor(props) {
        super(props);
    }
    renderTableData() {


        // console.log("three bundles", this.props.threeLastBundles);
        return this.props.tablearray.map((bundle, index) => {
            const { tableid, serial, accountnumber, amount, coin_type, debt, costcenter,statment } = bundle //destructuring
          
            return (
                <tr key={tableid}>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{serial}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{accountnumber}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{amount}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{coin_type}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{debt}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{costcenter}</td>
                    <td style={styles.tableStyle} style={styles.fontStyle}>{statment}</td>
                </tr>
            )
        })
    }
    render() {
        const { classes } = this.props;
        return (
            <div className="container">    
              <div className="row"  >
                <div className="col-md-6 ">
                    <img width={400} height={400} src={logo} className="img-fluid" style={{
                         marginLeft: "-50px",
                        // marginRight: "auto"
                    }} />
                </div>
                </div>
                <br />
                        <br />
                <div className="row"  >
                    <div className="col-md-12 text-center">
                
                        <h1 className="text-danger" style={styles.headerFont}>سند قيد يومية</h1>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div style={styles.receiptIdStyle1}>
                        <br />
                        <br />
                        <h1 style={styles.font} className="text-right mr-3" > رقم سند قيد اليومية: <span className="ml-2 text-danger">{this.props.recieptid}</span></h1>
                       <br/>
                       <br />
                        <h2  style={styles.font} className="text-right mr-3" ><span className="ml-2">{moment().format('YYYY-MM-DD hh:mm:ss A')}</span> :التاريخ</h2>  
                        <br />
                        <br />
                        <h2 className="text-right mr-3" style={styles.font}>اسم المستخدم: <span className="ml-2">{localStorage.getItem("userFirstName")+" "+localStorage.getItem("usermiddleName")+" "+localStorage.getItem("userLastName")}</span></h2>
                        <br />
                        <br />
                      
                        <br />
                        <br />
                        <br />
                        <br />
                        <div style={styles.receiptIdStyle1}>
                        <div style={{ direction: "rtl" }}>
                                        <div className="row p-5" style={{ textAlignLast: "right" }}>
                                            <div className="col-md-12">
                                                <table className="table" style={styles.tableStyle}>
                                                    <thead style={styles.tableStyle}>
                                                        <tr style={styles.tableStyle}>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="table.serial" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="table.accountNo" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="table.discount_amount" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="form.cointype" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="FORM.DEBT" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}><IntlMessages id="form.costcenterName" /></th>
                                                            <th className="border-0 text-uppercase small font-weight-bold" style={styles.fontStyle}> <IntlMessages id="form.Statement" /></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style={styles.tableStyle}>
                                                        {this.renderTableData()}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                     
                                    </div>
                          
                                    </div>  
                  
                        </div>     
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                       
                      
                       
                       
                    </div>
                </div>
            </div>
        );

    }
}

const styles = {
    headerFont: {
        fontSize: "92px",
    },
    headerFont1: {
        fontSize: "33px",
    },
    headerFont2: {
        padding: "20px",
        fontSize: "33px",
        marginLeft: "3px",
    },
    mainFont: {
        fontSize: "28px",
    },
    font: {
        fontSize: "59px",
        marginRight: "100px !important"
    },
    tableStyle: {

        borderCollapse: "collapse",
        padding: "10px",
    },
    font1: {
        fontSize: "large"
    },
    fontStyle: {
        fontSize: "40px"
    },
    receiptIdStyle: {
        color: "darkgreen",
        fontSize: "30px",
        border: "2px solid darkgreen",
        //marginRight: "300px",
        marginLeft: "300px",
        padding: "10px",
        width:"600px"
    },
    receiptIdStyle1: {
      //  fontSize: "30px",
        border: "2px solid black",
        width:"1500px",
       // display:'flex',
        
        //marginRight: "300px",
      
      //  padding: "10px",
      //  width:"600px"
    },
    receiptIdStyle2: {
        //  fontSize: "30px",
          border: "2px solid black",
          //width:"500px",
         // display:'flex',
          
          //marginRight: "300px",
        // marginLeft: "600px",
        //  padding: "10px",
         width:"200px"
      },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }
}


export default PrintReceivableReceipt;
