/**
 * add supplier
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 import { RctCard, RctCardContent } from 'Components/RctCard';
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 import MUIDataTable from "mui-datatables";
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 
 
 
 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
          currency: '',
          secretdegree:'',
          Statement:'',
          accountname:'',
          equality:'',
          creditor:'',
          debtor:'',
          Equivalent:'',
         costcetners:[{currency:'دينار عراقي',secretdegree:"عالي",Statement:"",accountname:'',equality:'',creditor:'',debtor:'',Equivalent:''}],
         disabled: false,
         selected_user:[],
         hiddenStatusForm: true,
         hiddenDebitLimitForm: true
     });
 
  
     const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
     };
    let newDate =new  Date();
     useEffect(() => {
     }, []);
 
     const handleFields = ({ target }) => {
         setState({ ...state, [target.name]: target.value });
     };
 
     const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                  maxHeight: 'unset',
                  overflowX: 'unset',
                  overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }       
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
 
     const onSubmit = e => {
 
        //  let data = new FormData();
        //  data.append('user_type_id', '4'),
        //      data.append('user_first_name', state.user_first_name),
        //      data.append('user_middle_name', state.user_middle_name),
        //      data.append('user_last_name', state.user_last_name),
        //      data.append('supplier_company_id', state.supplier_company_id),
        //      data.append('user_region_id', state.user_region_id),
        //      data.append('user_area_id', state.user_area_id),
        //      data.append('user_phonenumber', state.user_phonenumber),
        //      data.append('user_userlevel', state.user_level),
        //      data.append('tax_registration_number)', state.tax_registration_number),
        //      // data.append('user_email', state.user_email),
        //      // data.append('user_password', state.user_password),
        //      // data.append('user_personal_image', state.user_personal_image),
 
        //      axios({
        //          method: 'post', url: 'http://localhost:8000/createaccount', USER_TOKEN, data: data,
        //          headers: { 'Content-Type': 'multipart/form-data', 'x-access-token': `${Token}` }
        //      }).then(res => {
        //          toast.success(<IntlMessages id="form.addSupplierSuccess" />, {
        //              position: "top-center",
        //              autoClose: 4000,
        //              hideProgressBar: false,
        //              closeOnClick: true,
        //              pauseOnHover: true,
        //              draggable: true
        //          });
        //          setState({ ...state, disabled: true })
        //          setTimeout(function () { location.reload()}, 3000)
        //      }).catch(error => {
        //          // console.log(error.response.data.message);
        //          if (error.response.data.message) {
        //              toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
        //                  position: "top-center",
        //                  autoClose: 4000,
        //                  hideProgressBar: false,
        //                  closeOnClick: true,
        //                  pauseOnHover: true,
        //                  draggable: true
        //              });
        //          } else if (error.response.status === 429) {
        //              toast.error(error.response.data, {
        //                 position: "top-center",
        //                 autoClose: 4000,
        //                 hideProgressBar: false,
        //                 closeOnClick: true,
        //                 pauseOnHover: true,
        //                 draggable: true
        //              }); 
        //          }
        //      });
     }
 
     const { match } = props;
     // console.log(state);
     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>سند دفع</title>
                         <meta name="description" content="سند دفع" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.payinvoice" />} match={match} />
                     <div className="row">
                         <RctCollapsibleCard
                             colClasses="col-sm-12 col-md-12 col-lg-12"
                             heading={<IntlMessages id="sidebar.payinvoice" />}>
                                      <div className="form-group col-md-4">
                                   <div className="form-row"> 
                              <label><IntlMessages id="form.restrictionDate" /></label>
                             <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {new Date().getSeconds()}:{new Date().getMinutes()}:{new Date().getHours()}&nbsp;&nbsp;&nbsp;{new Date().getFullYear()}/{new Date().getDate()}/{new Date().getMonth()+1}</span>
                              </div>
                              </div> 
                             <form onSubmit={handleSubmit(onSubmit)}>
                                 <div className="form-row">
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.currency" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="currency"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.currency && errors.currency.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-6">
                                         <label><IntlMessages id="form.secretdegree" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="secretdegree"
                                            value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.secretdegree && errors.secretdegree.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}                                    
                                         </span>
                                     </div>
                                 </div>
                           


                                 <div className="form-row">

                                 <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.reversebox" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="reversebox"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.reversebox && errors.reversebox.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="form.virtualMoneyBalance" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="virtualMoneyBalance"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.virtualMoneyBalance && errors.virtualMoneyBalance.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="firstcredit" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="debtor"
                                            value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.debtor && errors.debtor.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}                                    
                                         </span>
                                     </div>
                                  
                                 </div>
                                 <div className="form-row">
                                     <div className="form-group col-md-12">
                                         <label><IntlMessages id="form.Statement" /></label>
                                         <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                         borderWidth: "0px 2px 2px 0px",
                                         lineHeight: "0px",
                                         borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                         }}>
                                                                                                 
                                         <input type="text" className="form-control" name="Statement"
                                         value={state.selected_user[1]}
                                             onChange={handleFields}
                                             ref={register({ required: true })} />
                                     </div>
                                         <span className="errors">
                                             {errors.Statement && errors.Statement.type === 'required' &&
                                                 <IntlMessages id="form.requiredError" />}
                                            
                                         </span>
                                     </div>
                                 </div>

                                 {(state.disabled === false) ? 
                                     <button type="submit" className="btn-margin" style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button> : 
                                     <button type="submit" className="btn-margin" disabled={true} style={{backgroundColor: "#0063c1", fontSize:"17px", fontWeight: "bold", height:'50px' ,color:'#fff'}}>
                                         <IntlMessages id="form.add" />
                                     </button>
                                 }
<div style={{borderColor:'#000',borderWidth:'100px',border:'ridge',padding: "10px",backgroundColor:'#D2D2D2'}}>
                                   <div className="form-row">
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Creditor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>-</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-3">
                                         <label><IntlMessages id="form.Debitor" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} /> 
                                       
                                     </div>

                                     <div className="form-group col-md-1">
                                     <div></div>
                                         <label style={{marginRight:'47%',fontSize:'xxx-large'}}>=</label>
                              
                                       
                                     </div>
                                     <div className="form-group col-md-4">
                                         <label><IntlMessages id="widgets.results" /></label>
                                         <input type="email" className="form-control" name="user_email"
                                             onChange={handleFields} />
                                       
                                     </div>
                                     </div>
                                     </div>



                                 
                             
                             </form>
 
 
                         </RctCollapsibleCard>
                     </div>
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }


{/* ----------------------------------------Tableeeeeeeeeeeeeeeeeee------------------------------------ */}


 <div className="row mb-5">
                 <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                     <RctCard>
                         <RctCardContent noPadding>
                             <MuiThemeProvider theme={getMuiTheme()}>
                                 <MUIDataTable 
                                 // title={<IntlMessages id="sidebar.cart" />}
                                    data={state.costcetners}
                                     columns={[
                                             {
                                                 label: "id",
                                                 name:  "",
                                                 options: {
                                                     display: "none",
                                                     filter: false,
                                                     print: false,
                                                 }
                                             },
                                             {
                                                 label: <IntlMessages id="form.currency" />,
                                                 name:  "currency",
                                             },
                                             {
                                                 label: <IntlMessages id="form.secretdegree" />,
                                                 name:  "secretdegree"
                                             },
                                             {
                                                 label: <IntlMessages id="form.Statement" />,
                                                 name:  "Statement",
                                             },
                                             {
                                                label: <IntlMessages id="form.reversebox" />,
                                                name:  "accountname",
                                            },
                                            {
                                                label: <IntlMessages id="firstcredit" />,
                                                name:  "firstcredit",
                                            },
                                            {
                                                label: <IntlMessages id="form.virtualMoneyBalance" />,
                                                name:  "form.virtualMoneyBalance",
                                            },
                                          
                                             {
                                                 label: <IntlMessages id="form.update" />,
                                                 name: "",
                                                 options: {
                                                     filter: true,
                                                     sort: false,
                                                     empty: true,
                                                     print: false,
                                                     customBodyRender: (value, tableMeta, updateValue) => {
                                                     return (
                                                         <React.Fragment>
                                                             <button type="button" className="btn btn-primary" 
                                                             onClick={() => {
                                                                 console.log("HIIIII",tableMeta.rowData[4]);
                                                                    setState({
                                                                     ...state , 
                                                                     selected_user:tableMeta.rowData,
                                                                     hiddenStatusForm: false,
                                                                     hiddenDebitLimitForm: true
                                                                    });
                                                             }}>
                                                             <IntlMessages id="form.update" />
                                                             </button>
                                                         </React.Fragment>
                                                     );
                                                 }
                                                 }
                         
                                             },
                                             {
                                                label: <IntlMessages id="form.delete" />,
                                                name: "",
                                                options: {
                                                    filter: true,
                                                    sort: false,
                                                    empty: true,
                                                    print: false,
                                                    customBodyRender: (value, tableMeta, updateValue) => {
                                                    return (
                                                        <React.Fragment>
                                                            <button type="button" className="btn btn-danger" 
                                                            onClick={() => {
                                                                console.log("HIIIII",tableMeta.rowData[4]);
                                                                   setState({
                                                                    ...state , 
                                                                    selected_user:tableMeta.rowData,
                                                                    hiddenStatusForm: false,
                                                                    hiddenDebitLimitForm: true
                                                                   });
                                                            }}>
                                                            <IntlMessages id="form.delete" />
                                                            </button>
                                                        </React.Fragment>
                                                    );
                                                }
                                                }
                        
                                            },
                                         ]}
                                     options={options}
                                 />
                             </MuiThemeProvider>
                         </RctCardContent>
                     </RctCard>
                 </RctCollapsibleCard>
             </div>









         </React.Fragment>
     )
 }