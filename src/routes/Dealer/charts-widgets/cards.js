import React, { Component } from 'react';
import USER_TOKEN from '../../../constants/Token';
import axios from 'axios';
import Barcode from 'react-barcode';
import compImage1 from '../../../assets/comp-img/2.png'
import logo from '../../../assets/comp-img/1.jpg'
import moment from 'moment';
import './_style.css';

class PrintCard extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {
        console.log("hiiii");
        // this.setState({
        //     img: this.props.companyId
        // })

    }

    render() {
        // console.log("props", this.props);
        // console.log("entered print comp", this.state);

        const { classes } = this.props;
        return (
            <div className="container printCard">
                <div className="row">
                    {this.props.cardsToPrint.map((obj, index) => {
                        return (
                            <div className="col-2 text-center border border-3" style={styles.mainContainer}>
                                <img width={100} height={100} src={require(`../../../assets/comp-img/${this.props.companyId}.png`)} className="img-fluid" style={{
                                    marginLeft: "auto",
                                    marginRight: "auto",
                                    marginTop: "5px"
                                }} />
                                <p style={{
                                    fontSize: "5px",
                                    paddingBottom: "0.5px",
                                }}>AMOUNT {this.props.categoryName}-IQD</p>
                                <p style={{
                                    fontSize: "5px",
                                    paddingBottom: "0.5px",
                                }}>PIN-CODE</p>
                                <p style={{ fontSize: "10px", color: "red", paddingBottom: "0.5px" }}>{obj.pin}</p>
                                {/* <p style={styles.pinStyle}>{obj.pin}</p> */}
                                <br />
                                <div style={{fontSize: "2px"}}>
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th style={{fontSize: "7px", paddingBottom: "0.5px" }} scope="row">Serial: </th>
                                                <td style={{fontSize: "7px", paddingBottom: "0.5px" }}>{obj.serial}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th style={{fontSize: "7px", paddingBottom: "0.5px" }} scope="row">Expiry: </th>
                                                <td style={{fontSize: "7px", paddingBottom: "0.5px" }}>{obj.expire_date}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th style={{fontSize: "7px", paddingBottom: "0.5px" }} scope="row">Merchant Name: </th>
                                                <td style={{fontSize: "7px", paddingBottom: "0.5px" }} >{this.props.userFirstName} {this.props.usermiddleName} {this.props.userLastName}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th style={{fontSize: "7px", paddingBottom: "0.5px" }} scope="row">Merchant Id: </th>
                                                <td style={{fontSize: "7px", paddingBottom: "0.5px" }} >{this.props.userPhoneNumber}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th style={{fontSize: "7px", paddingBottom: "0.5px" }} scope="row">Time: </th>
                                                <td style={{fontSize: "7px", paddingBottom: "0.5px" }} >{moment().format('YYYY-DD-MM hh:mm:ss A')}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <img width={100} height={100} src={logo} className="img-fluid" style={{
                                    marginLeft: "auto",
                                    marginRight: "auto"
                                }} />
                                <br />
                            </div>
                        )

                    })}
                    <br />
                    <br />
                </div>

            </div>
        );
    }
}

const styles = {
    mainContainer: {
        // border: "2px solid black",
        // margin: "20px",
        marginTop: "33px",
        marginBottom: "33px",
        marginLeft: "4px"
    },
    spaceSize: {
        fontSize: "3px",
        paddingBottom: "1px"
    },
    pinStyle: {
        fontSize: "45px",
        border: "2px solid black",
        marginRight: "300px",
        marginLeft: "300px",
        marginTop: "30px"
    },
    balanceStyle: {
        fontSize: "70px",
        color: "red",
        fontWeight: "bold"
    },
    mainDiv: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        fontSize: "3px",
    },
    mainFont: {
        fontSize: "28px",
    },
    // font: {
    //     fontSize: "25px",
    //     marginRight: "100px !important"
    // },
    fontStyle: {
        fontSize: "x-large"
    },
    wordsFont: {
        fontSize: "19px"
    },
    spaceSize: {
        paddingBottom: "11px",
        fontSize: "large"
    }



}


const printStyle = `
@media print {
    @page { size: landscape; }
  }
`


export default PrintCard;
