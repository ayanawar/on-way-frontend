//upload comapny cards

import React, { useState, useRef, useEffect } from 'react';
import USER_TOKEN from '../../../constants/Token';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import Modal from 'react-awesome-modal';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import { ToastContainer, toast } from 'react-toastify';
import * as XLSX from 'xlsx';
import axios from 'axios';
import Cookies from 'universal-cookie';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import logoImage from '../../../assets/img/icon10.jpg';
import moment from 'moment';

import logo from '../../../assets/comp-img/1.jpg'

// printer 
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';
import { useReactToPrint } from 'react-to-print';
import './_style.css';
// thermal printer
// const ThermalPrinter = require("node-thermal-printer").printer;
// const PrinterTypes = require("node-thermal-printer").types;
// const {icpRenderer} = require('electron');
// const path = require("path");

// cards component
import PrintCard from './cards.js';





export default function sellcards(props) {

   const nf = new Intl.NumberFormat();

   const cookies = new Cookies();

   const componentRef = useRef();

   const handlePrint = useReactToPrint({
      content: () => componentRef.current,

      //   onAfterPrint: () => { 
      //       setState({
      //           ...state, 
      // 		disabled: true
      //       })
      //       location.reload()
      //   },
   });


   const [state, setState] = useState({
      cardsNumber: 1,
      total: 0,
      regions: [],
      companies: [],
      categories: [],
      todayDate: '',
      companyId: '',
      categoryId: 0,
      supplierId: 0,
      companyName: '',
      categoryName: '',
      CarrdNumber: 1,
      companyNameError: '',
      categoryNameError: '',
      loadModel: false,
      userId: localStorage.getItem("userIdInUsers"),
      userTypeId: localStorage.getItem("user_type_id"),
      userRegionId: localStorage.getItem("regionId"),
      categoriesAvailable: [],
      adminDiscount: 0,
      lastSellPrice: '',
      dealerSellPrice: '',
      priceChange: false,
      userPhoneNumber: localStorage.getItem("phoneNumber"),
      userFirstName: localStorage.getItem("userFirstName"),
      usermiddleName: localStorage.getItem("usermiddleName"),
      userLastName: localStorage.getItem("userLastName"),
      bundleAvailableCards: 0,
      bundleId: 0,
      cardsRespons: [],
      showCards: false,
      buttonDisable: false,
      cardsToPrint: []
   });

   const validate = () => {
      let companyNameError = '';
      let categoryNameError = '';
      // let supplierNameError = '';
      // let sourceError = '';
      // let regionError = '';
      // let expiryDateError = '';
      // let bundleNumberError = '';
      // let currencyError = '';
      // let uploadFileError = '';
      if (state.companyId == '') {
         companyNameError = 'برجاء اختيار الشركة'
      }
      if (state.categoryId == 0) {
         categoryNameError = 'برجاء اختيار الفئة'
      }




      if (companyNameError || categoryNameError) {
         setState({
            ...state,
            companyNameError, categoryNameError
         })
         return false;
      }


      return true;
   }

   const setTodayDate = () => {
      var date = new Date();
      date = date.getUTCFullYear() + 1 + '-' +
         ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
         ('00' + date.getUTCDate()).slice(-2) + ' ';
      // console.log(date);

      setState({
         ...state,
         expiryDate: date.trim()
      })
      return date.trim();
   }

   useEffect(() => {
      setTodayDate()

      // all companies
      axios.get('http://localhost:8000/allcompanies', USER_TOKEN)
         .then(response => {
         
               response.data.companies.splice(0, 1);
               console.log("response", response.data.companies);
               setState({
                  ...state,
                  companies: response.data.companies
               })
            


         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });


   }, []);

   // constructor(props) {
   //    super(props);

   // // For a full list of possible configurations,
   // // please consult http://www.dropzonejs.com/#configuration
   // this.djsConfig = {
   //    addRemoveLinks: true,
   //    acceptedFiles: "file/xlsx"
   // };

   // this.componentConfig = {
   //    iconFiletypes: ['.xlsx'],
   //    showFiletypeIcon: true,
   //    postUrl: '/'
   // };

   // // If you want to attach multiple callbacks, simply
   // // create an array filled with all your callbacks.
   // this.callbackArray = [() => console.log('Hi!'), () => console.log('Ho!')];

   // // // Simple callbacks work too, of course
   // this.callback = (file) => { };

   // //this.success = file => console.log('uploaded', file);

   // this.removedfile = file => console.log('removing...', file);

   // this.dropzone = null;
   // this.handleChange = this.handleChange.bind(this);
   // }

   const handleTwochangeEvent = e => {
      handleChange(e);
      getAvailableCardsForEachCompany(e);
   }

   const loadModel = () => {
      let total = 0;
      if (state.priceChange == false) {
         total = parseInt(state.cardsNumber) * parseInt(state.adminSellPrice);
      } else {
         total = parseInt(state.cardsNumber) * parseInt(state.dealerSellPrice)
      }
      console.log("total", total);

      setState({
         ...state,
         total: total
      })

      let isValidate = validate()
      if (isValidate == true) {
         //   console.log("Validation is Ok");
         setState({
            ...state,
            total: total,
            loadModel: true
         })
      }

   }


   const getMuiTheme = () => createMuiTheme({
      overrides: {
         MUIDataTable: {
            responsiveScroll: {
               maxHeight: 'unset',
               overflowX: 'unset',
               overflowY: 'unset',
            },
         },
         MuiTableCell: {
            head: {
               color: "#599A5F",
               fontWeight: "bold",
               fontSize: "15px",
               fontFamily: "'Almarai', sans-serif"
            },
            body: {
               color: "#092346",
               fontWeight: "bold",
               fontSize: "15px",
               fontFamily: "'Almarai', sans-serif",
            }
         },
         MUIDataTableHeadCell: {
            data: {
               color: "#599A5F",
               fontWeight: "bold",
               fontFamily: "'Almarai', sans-serif"
            },
            fixedHeader: {
               position: "sticky !important",
               zIndex: '100',
            }
         },
         MUIDataTableSelectCell: {
            headerCell: {
               zIndex: 1
            },
            fixedLeft: {
               zIndex: 1
            }
         },
         MuiToolbar: {
            regular: {
               backgroundColor: "gray"
            },
            root: {
               top: 0,
               position: 'sticky',
               background: 'white',
               zIndex: '100',
            },
         },
         MUIDataTablePagination: {
            tableCellContainer: {
               backgroundColor: "gray"
            }
         },
         MUIDataTableToolbarSelect: {
            root: {
               color: "#599A5F",
               fontWeight: "bold",
               zIndex: 1,
            },
            title: {
               display: "none"
            }
         },
         MuiPaper: {
            root: {
               color: "#092346",
               fontWeight: "bold",
               fontFamily: "'Almarai', sans-serif"
            }
         },
         MUIDataTableBody: {
            emptyTitle: {
               display: "none",
            }
         }
      }
   })

   let selectedRowsToPrint = [];

   const handlemultiSelect = (selectedRows, displayData) => {
      console.log("hii", selectedRows);
      console.log("displayDatalength", displayData.length);
      console.log("displayData", displayData);
      // cardsIds = [];
      // bundleIds = [];
      // bundleNumbers = [];
      // pins = [];
      // serialNumbers = [];
      // selectedBalances = [];
      // selectedBalances2 = [];
      // selectedRowsWithData = [];
      // selectedCount = 0;
      selectedRowsToPrint = [];

      // console.log("selectedRowsArray", selectedRows);
      selectedRows.data.forEach(element => {
         // console.log("element", element);
         // cardsIds.push(displayData[element.index].data[0]);
         // bundleIds.push(displayData[element.index].data[1]);
         // bundleNumbers.push(displayData[element.index].data[2]);
         // pins.push(displayData[element.index].data[3]);
         // serialNumbers.push(displayData[element.index].data[4])
         // selectedBalances2.push(displayData[element.index].data[5]);
         // selectedBalances.push(displayData[element.index].data[6]);
         selectedRowsToPrint.push({
            "serial": displayData[element.index].data[1],
            "pin": displayData[element.index].data[2],
            "expire_date": displayData[element.index].data[3],
            // "Last Name": displayData[element.index].data[4],
            // "POS Phone": displayData[element.index].data[5],
            // "region": displayData[element.index].data[6],
            // "area": displayData[element.index].data[7],
            // "Current Balance": parseLocaleNumber(displayData[element.index].data[8]),
            // "Dealer Name": displayData[element.index].data[10],
            // "Representative Name": displayData[element.index].data[11],
            // "POS Type": displayData[element.index].data[12],
            // "rate": displayData[element.index].data[13],
            // "status": displayData[element.index].data[14],
            // "Dept Limit": displayData[element.index].data[16],
            // "Date And Time": displayData[element.index].data[17]
         })
      });
      // selectedRowsToPrint = displayData
      // setState({
      //    ...state,
      //    cardsRespons: selectedRowsToPrint
      // })
      console.log("rows ro print", selectedRowsToPrint);

      // console.log("selectedBalances: ", selectedBalances);
      // console.log("selectedRowsToExport", selectedRowsToExport);
      // setState({
      //     ...state,
      //     dataToExport: selectedRowsToExport
      // })

      // totalBalancesToBePrinted = 0;
      // for (let i = 0; i < selectedBalances.length; i++) {
      //     totalBalancesToBePrinted += selectedBalances[i];
      //     selectedCount = selectedBalances.length
      // }
      return (
         <React.Fragment>
            <div className="form-group col-md-12 mt-3 text-center" id="cardsToBePrintedDiv">
               {displayData.length == 0 ? <React.Fragment></React.Fragment> :
                  // <div className="form-group col-md-12 justify-content-center">
                  //    <div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                  //       <div class="btn-group mr-2 justify-content-center" role="group" aria-label="Second group">
                  //          <button type="button" class="btn btn-primary" Style="margin: inherit;">اعادة طباعة</button>
                  //          {/* <button type="button" class="btn btn-secondary">اغلاق</button> */}
                  //       </div>
                  //    </div>

                  <div className="row justify-content-center">
                     <button className="btn btn-primary" onClick={() => { rePrintCards(selectedRowsToPrint) }}>
                        اعادة طباعة
                                </button>
                  </div>


                  // </div>
               }
            </div>
         </React.Fragment>
      )
   }

   const rePrintCards = (cardsTOPrint) => {

      console.log("crds to print", cardsTOPrint);

      setState({
         ...state,
         cardsToPrint: cardsTOPrint
      })
      setTimeout(() => { handlePrint() }, 1000);
   }

   const options = {
      filter: true,
      filterType: 'dropdown',
      rowsPerPage: 10,
      rowsPerPageOptions: [5, 10, 25, 50, 100, 250, 500],
      responsive: 'vertical',
      enableNestedDataAccess: '.',
      customToolbarSelect: handlemultiSelect,
      selectableRowsHeader: true,
      download: false,
      print: false,
      viewColumns: false,
      sort: false,
      fixedHeader: true,
      fixedSelectColumn: false,
      tableBodyHeight: "345px"
   };

   const getAvailableCardsForEachCompany = (event) => {
      // get available cards to sell  
      console.log("state afet compId", state);
      let comId = event.target.value;
      let compName = "";
      state.companies.forEach(company => {
         if (company.company_id == event.target.value) {
            compName = company.company_name_ar;
            // setState({
            //    ...state,
            //    companyId: event.target.value,
            //    companyName: company.company_name_ar
            // })
         }
      });
      axios.post('http://localhost:8000/getbundledata/sellprice',
         {
            company_id: event.target.value,
            region_id: state.userRegionId,
            user_id: state.userId
         }
         , USER_TOKEN)
         .then(response => {
            console.log("response", response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {

               if (response.data.message == "No Cards Available For This Category or bundle is expired") {
                  setState({
                     ...state,
                     companyName: compName,
                     companyId: comId,
                     categoriesAvailable: []
                  })
                  toast.error("لا يوجد كروت متوفرة فى هذة الشركة", {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  });
               } else {
                  setState({
                     ...state,
                     companyName: compName,
                     categoriesAvailable: response.data.message,
                     companyId: comId
                  })
               }

            }
         })
         .catch(error => {
            console.log("error", error);
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
   }

   const getcarsForSpeceficCompIdCartId = (event) => {
      console.log("compId", state.companyId);
      let company_id = state.companyId
      let cards_category_id = event.target.value
      let catName = "";
      state.categoriesAvailable.forEach(category => {
         if (category.cards_category_id == cards_category_id) {
            console.log("category", category.category_text);
            catName = category.category_text
            // setState({
            //    ...state,

            //    categoryName: category.category_text
            // })
         }
      });

      console.log("comp Id", company_id);
      axios.post(`http://localhost:8000/getcardsbundledata`, {
         company_id: company_id,
         card_category_id: cards_category_id,
         region_id: state.userRegionId,
         user_id: state.userId
      }, USER_TOKEN)
         .then(response => {
            console.log(response.data);
            // console.log("plaaa", response);
            // console.log("sellprice", response.data.message[0].sell_price);
            // console.log("discount", response.data.discount);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               let lastSellPrice = parseInt(response.data.message[0].sell_price) + (parseInt(response.data.discount));
               setState({
                  ...state,
                  categoryId: cards_category_id,
                  categoryName: catName,
                  adminSellPrice: response.data.message[0].sell_price,
                  adminDiscount: response.data.discount,
                  lastSellPrice: lastSellPrice,
                  dealerSellPrice: response.data.message[0].sell_price,
                  bundleAvailableCards: response.data.message[0].bundle_available_card,
                  bundleId: response.data.message[0].bundle_id
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
            }
         });
      // console.log(this.state.sellPrice);
   }

   const handleChange = ({ target }) => {
      console.log("atret valu", target.value);
      setState({ ...state, [target.name]: target.value });
      // console.log(this.state)
   };

   const handleNewPrices = (event) => {

      setState({
         ...state,
         dealerSellPrice: event.target.value,
         priceChange: true
      })

   }

   const handleTwoEvents = (e) => {
      // console.log("entered for getting prices", e.target);
      handleChange(e);
      getcarsForSpeceficCompIdCartId(e);
   }

   const closeModel = () => {
      setState({
         ...state,
         loadModel: false,
         showCards: false
      });
   }
   const RefreshPage = () => {
      setTimeout(function () { location.reload() }, 700)
   }

   const handleSubmit = (e) => {
      // e.preventDefault();


      let data = {
         company_id: state.companyId,
         card_category_id: state.categoryId,
         region_id: state.userRegionId,
         bundle_id: state.bundleId,
         cardsnumber: state.cardsNumber,
         user_id: state.userId,
         user_type_id: state.userTypeId,
         dealer_sell_price: state.total,
         bundle_available_card: state.bundleAvailableCards,
         userPhoneNumber: state.userPhoneNumber,

      }

      console.log("data", data);

      axios.post(`http://localhost:8000/dealersellcards`, data, USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               // closeModel();
               console.log("cards", response.data.message);
               if (response.data.message == "Cards is finished") {
                  toast.error("لقد انتهت كرتات هذة الوجبة", {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  });
               } else if (response.data.message == "You have no credit to continue") {
                  toast.error("ليس لديك رصيد كافى", {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  });
                  setState({
                     ...state,
                     showCards: false,
                     cardsRespons: [],
                     loadModel: false,
                     buttonDisable: false
                  })
               } else {
                  toast.success('تمت عملية الشراء بنجاح', {
                     position: "top-center",
                     autoClose: 4000,
                     hideProgressBar: false,
                     closeOnClick: true,
                     pauseOnHover: true,
                     draggable: true
                  });

                  let cardsRespons = response.data.message;

                  // var today = new Date();
                  // var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                  // var hours = date.getHours();
                  // var minutes = date.getMinutes();
                  // var ampm = hours >= 12 ? 'pm' : 'am';
                  // hours = hours % 12;
                  // hours = hours ? hours : 12; // the hour '0' should be '12'
                  // minutes = minutes < 10 ? '0' + minutes : minutes;
                  // var strTime = hours + ':' + minutes + ' ' + ampm;
                  // var currentDate = date + ' ' + strTime;
                  // console.log("time", currentDate);
                  console.log("cards sold", response.data.message);

                  setState({
                     ...state,
                     showCards: true,
                     cardsRespons: cardsRespons,
                     cardsToPrint: cardsRespons,
                     loadModel: false,
                     buttonDisable: true
                  })

                  handlePrint();
                  // let printer = new ThermalPrinter({
                  //    type: PrinterTypes.EPSON,
                  //    interface: '/dev/usb/lp1'
                  //  });
                  //   //tcp://xxx.xxx.xxx.xxx
                  //  printer.alignCenter();
                  //  printer.println("Hello world");
                  //  printer.println(this.state.companyName);
                  //  printer.println("Amount");
                  //  printer.println(`${this.state.categoryName}-IQD`);
                  //  printer.println("PIN-CODE");
                  // //  printer.println("${cardsRespons[0].pin}");

                  //  printer.tableCustom([                                       // Prints table with custom settings (text, align, width, cols, bold)
                  //    { text:`${cardsRespons[0].pin}`, align:"CENTER", width:0.50, bold:true },
                  //  ]);

                  //  printer.tableCustom([                                       // Prints table with custom settings (text, align, width, cols, bold)
                  //    { text:"Left", align:"LEFT", width:0.5 },
                  //    { text:"Center", align:"CENTER", width:0.25, bold:true },
                  //    { text:"Right", align:"RIGHT", cols:8 }
                  //  ]);

                  //  printer.table(["One", "Two", "Three"]); 

                  //  printer.printImage(logoImage)

                  //  printer.cut();

                  //  try {
                  //    let execute = printer.execute()
                  //    alert("printed successfully")
                  //  } catch (error) {
                  //    alert("failed with error", error)
                  //  }

                  //  printer.execute(function(err){
                  //     if(err){
                  //        alert("failed with error1", err)
                  //     }else{
                  //        alert("printed successfully1");
                  //     }
                  //  })
                  // const cards = [{
                  //    type: 'image',
                  //    path: 'assets/banner.png',     // file path
                  //    position: 'center',                                  // position of image: 'left' | 'center' | 'right'
                  //    width: '60px',                                           // width of image in px; default: auto
                  //    height: '60px',                                          // width of image in px; default: 50 or '50px'
                  // },
                  // {
                  //    type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image' | 'table
                  //    value: `${this.state.companyName}`,
                  //    style: `text-align:center;`,
                  //    css: { "font-weight": "700", "font-size": "15px" }
                  // },
                  // {
                  //    type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image' | 'table
                  //    value: 'AMOUNT',
                  //    style: `text-align:center;`,
                  //    css: { "font-weight": "700", "font-size": "15px" }
                  // },
                  // {
                  //    type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image' | 'table
                  //    value: `${this.state.categoryName}-IQD`,
                  //    style: `text-align:center;`,
                  //    css: { "font-weight": "700", "font-size": "15px" }
                  // },
                  // {
                  //    type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image' | 'table
                  //    value: 'PIN-CODE',
                  //    style: `text-align:center;`,
                  //    css: { "font-weight": "700", "font-size": "18px" }
                  // },
                  // {
                  //    type: 'text',                                       // 'text' | 'barCode' | 'qrCode' | 'image' | 'table
                  //    value: `${cardsRespons[0].pin}`,
                  //    style: `text-align:center;`,
                  //    css: { "font-weight": "700", "font-size": "18px" }
                  // },
                  // {
                  //    type: 'table',
                  //    // style the table
                  //    style: 'border: 0.2px ',

                  //    // multi dimensional array depicting the rows and columns of the table body
                  //    tableBody: [
                  //       ['Serial', cardsRespons[0].serial],
                  //       ['Expiry', cardsRespons[0].expire_date],
                  //       ['Name', userFirstName + ' ' + usermiddleName + ' ' + userLastName],
                  //       ['ID', this.state.userId],
                  //       ['Time', currentDate],
                  //    ],
                  //    // // list of columns to be rendered in the table footer
                  //    // tableFooter: ['Animal', 'Age'],
                  //    // custom style for the table header
                  //    // tableHeaderStyle: 'background-color: #000; color: white;',
                  //    // custom style for the table body
                  //    // tableBodyStyle: 'border: 0.5px solid #ddd',
                  //    // custom style for the table footer
                  //    // tableFooterStyle: 'background-color: #000; color: white;',
                  // },
                  // ]

                  // PosPrinter.print(data, options)
                  //    .then(() => { })
                  //    .catch((error) => {
                  //       console.error(error);
                  //    });
                  // // icpRenderer.send(`print`, JSON.stringify(cards))

                  // setTimeout(function () { location.reload() }, 3000)
               }

            }
         })
         .catch(error => {
            closeModel();
            // console.log('Error fetching and parsing data ', error);
            toast.error("", {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });


         });



   }

   // console.log(this.state);

   return (
      console.log("state", state),
      <React.Fragment>

            <div className="dropzone-wrapper">
               <ToastContainer />
               <PageTitleBar title={<IntlMessages id="sidebar.Sellcards" />} match={props.match} />
               <div className="row">


                  <RctCollapsibleCard
                     colClasses="col-sm-12 col-md-12 col-lg-12"
                     heading={<IntlMessages id="sidebar.Sellcards" />}
                  >
                     <form>
                        <div className="form-row">
                           <div className="form-group col-md-6">
                              <label><IntlMessages id="form.companyName" /></label>
                              <select id="inputComapny" className="form-control" name="companyId" onChange={handleTwochangeEvent}>
                                 <option key="0" value="" > </option>
                                 {
                                    state.companies.map((company) => {
                                       return (
                                          <option key={company.company_id} value={company.company_id}>{company.company_name}</option>
                                       )
                                    })
                                 }
                              </select>
                              {state.companyNameError ? <div style={{ color: 'red', fontSize: 12 }}>
                                 {state.companyNameError}
                              </div> : null}
                           </div>
                           <div className="form-group col-md-6">
                              <label><IntlMessages id="widgets.category" /></label>
                              <select id="selectCategory" className="form-control" name="categoryId" value={state.categoryId} onChange={handleTwoEvents}>
                                 <option key="0" value="" ></option>
                                 {
                                    state.categoriesAvailable.map((category) => {
                                       return (
                                          <option key={category.card_category_id} value={category.card_category_id} >{category.category_text}</option>
                                       )
                                    })
                                 }
                              </select>
                              <div style={{ color: 'red', fontSize: 12 }}>
                                 {state.categoryNameError}
                              </div>
                           </div>
                        </div>



                        <div className="form-row">
                           <div className="form-group col-md-12">
                              <label><IntlMessages id="form.cardByingPrice" /></label>
                              {/* <input type="text" className="form-control" name="bundleNumber" onChange={this.handleChange} /> */}
                              <span>{state.lastSellPrice}</span>
                           </div>
                        </div>
                        <div className="form-row">
                           <div className="form-group col-md-6">
                              <label><IntlMessages id="form.EditcardsSellPrice" /></label>

                              <input type="number" className="form-control" id="inputSellCards" name="dealerSellPrice" defaultValue={state.dealerSellPrice} onChange={handleNewPrices} />
                              {/* {console.log(this.state)} */}
                              <div style={{ color: 'red', fontSize: 12 }}>
                                 {state.cardsSellPriceError}
                              </div>
                           </div>
                           <div className="form-group col-md-6">
                              <label><IntlMessages id="form.cardsCount" /></label>

                              <input type="number" className="form-control" id="cardsNumber" name="cardsNumber" min="1" max="100000" defaultValue={state.cardsNumber} onChange={handleChange} />
                              {/* {console.log(this.state)} */}

                           </div>



                        </div>





                        <button type="button" className="btn btn-primary btn-margin" onClick={loadModel} disabled={state.buttonDisable}><IntlMessages id="form.BuyOk" /></button>

                        

                     </form>

                     {
                        state.loadModel == true ?
                           <Modal visible={state.loadModel} width="550" height="340" effect="fadeInUp" onClickAway={closeModel}>
                              <div className="modalstyleInvoice" Style="padding-bottom: 233px;">
                                 <div className="modal-header">
                                    <h5 className="modal-title" id="exampleModalCenterTitle"> <IntlMessages id="sidebar.Sellcards" />    </h5>

                                    <a type="button" className="close" aria-label="Close" onClick={closeModel}>
                                       <span aria-hidden="true">&times;</span>
                                    </a>
                                 </div>
                                 <div className="modal-body">
                                    <h3 className="modal-title"><IntlMessages id="sidebar.WillBuy" /> {state.cardsNumber} <IntlMessages id="Cards" /> <IntlMessages id="FromCompany" /> {state.companyName}</h3>
                                    <blockquote className="blockquote">
                                       <span className="mb-0"> <IntlMessages id="Yourbalancewillbededucted" />  {state.total} </span>

                                    </blockquote>
                                 </div>
                                 <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" onClick={closeModel}><IntlMessages id="Colse" /></button>
                                    <button className="btn btn-primary" onClick={(e) => handleSubmit()}><IntlMessages id="form.Ok" /></button>



                                    {/* <ReactToPrint content={() => componentRef}>

                                       <PrintContextConsumer>
                                          {({ handlePrint }) => (
                                             // <div className="container" style={{
                                             //     marginRight: "494px",
                                             //     maxWidth: "max-content", position: "fixed",
                                             //     marginTop: "47px"
                                             // }}>
                                             <div>
                                                <h1 style={{ display: "none" }}>hiii</h1>
                                                <button className="btn btn-danger" onClick={handlePrint}><IntlMessages id="form.print this out!" /></button>

                                             </div>
                                          )}
                                       </PrintContextConsumer>
                                    </ReactToPrint> */}
                                 </div>
                              </div>
                           </Modal>
                           :
                           null
                     }
                     
                     {
                        state.showCards == true ?

                           <div>
                              <RctCard>
                                 <div className="container" Style="padding-right: 37px;">
                                    <div className="form-row">
                                       <div class="form-group row col-md-5">
                                          <h4 className="text-danger mr-3 ml-3" style={{ marginTop: "auto" }}>اسم الشركة: <span className="text-dark ml-3">{state.companyName}</span></h4>

                                       </div>
                                       <div class="form-group row col-md-5">
                                          <h4 className="text-danger mr-3 ml-3" style={{ marginTop: "auto" }}>الفئة: <span className="text-dark ml-3">{state.categoryName}</span></h4>

                                          {/* <label class="col-form-label" Style="margin-top: auto;"> {this.state.categoryName}</label> */}
                                          {/* <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={this.state.categoryName} />
                                             </div> */}
                                       </div>
                                       <div className="form-group row col-md-2">

                                          <button type="button" className="btn btn-secondary btn-margin" onClick={RefreshPage}>اغلاق</button>
                                       </div>
                                    </div>
                                    <div className="form-row">
                                       <div class="form-group row col-md-5">
                                          {/* <label className="col-form-label text-danger mr-3 ml-3" style={{ marginTop: "auto" }}>اسم الشركة: <span className="text-dark ml-3">{this.state.companyName}</span></label> */}
                                          <h4 className="text-danger mr-3 ml-3" style={{ marginTop: "auto" }}>سعر شراء الكارت: <span className="text-dark ml-3">{state.total}</span></h4>

                                          {/* <label class=" col-form-label"> {this.state.total}</label> */}
                                          {/* <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={this.state.total} />
                                             </div> */}
                                       </div>
                                       <div class="form-group row col-md-5">
                                          <h4 className="text-danger mr-3 ml-3" style={{ marginTop: "auto" }}>التاريخ : <span className="text-dark ml-3">{moment().format('YYYY-DD-MM hh:mm:ss A')}</span></h4>

                                          {/* <label class="col-form-label">التاريخ : {moment().format('YYYY-DD-MM hh:mm:ss A')}</label> */}
                                          {/* <div class="col-sm-10">
                                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={this.state.categoryName} />
                                             </div> */}
                                       </div>
                                    </div>

                                 </div>
                                 <div style={{ display: "none" }}>
                                    <PrintCard ref={componentRef} {...state} />
                                 </div>

                                 <RctCollapsibleCard fullBlock>
                                    <MuiThemeProvider theme={getMuiTheme()}>
                                       <MUIDataTable
                                          data={state.cardsRespons}
                                          columns={[
                                             {
                                                label: "id",
                                                name: "card_id",
                                                options: {
                                                   display: "none",
                                                   filter: false,
                                                   print: false,
                                                }
                                             },
                                             {
                                                label: "Serial",
                                                name: "serial",
                                             },
                                             {
                                                label: "pin",
                                                name: "pin",
                                             },
                                             {
                                                label: "تاريخ الانتهاء",
                                                name: "expire_date"
                                             },
                                             // {
                                             //    label: <IntlMessages id="form.image" />,
                                             //    name: "pos_commercial_logo",
                                             //    options: {
                                             //       filter: true,
                                             //       sort: false,
                                             //       empty: true,
                                             //       customBodyRender: (value, tableMeta, updateValue) => {
                                             //          return (
                                             //             <div>
                                             //                <img src={"http://localhost:8000/" + value} alt="لا يوجد صورة" width="90" height="100" />
                                             //             </div>
                                             //          );
                                             //       }
                                             //    }
                                             // },
                                             // {
                                             //    label: <IntlMessages id="form.dealerName" />,
                                             //    name: "dealeruserName"
                                             // },
                                             // {
                                             //    label: <IntlMessages id="form.representativeName" />,
                                             //    name: "repuserName"
                                             // },

                                             // {
                                             //    label: <IntlMessages id="form.type" />,
                                             //    name: "pos_type_ar"
                                             // },
                                             // {
                                             //    label: <IntlMessages id="form.rank" />,
                                             //    name: "pos_rank_ar"
                                             // },
                                             // {
                                             //    label: <IntlMessages id="widgets.status" />,
                                             //    name: "active_status_ar"
                                             // },
                                             // {
                                             //    label: <IntlMessages id="table.allowedDebtLimit" />,
                                             //    name: "dept_limit"
                                             // },
                                             // {
                                             //    label: <IntlMessages id="table.allowedDebtLimit" />,
                                             //    name: "dept_limit_un",
                                             //    options: {
                                             //       display: "none",
                                             //       filter: false,
                                             //       print: false,
                                             //    }
                                             // },
                                             // {
                                             //    label: "تاريخ و وقت التسجيل",
                                             //    name: "creation_date"
                                             // }

                                          ]}
                                          options={options}
                                       />
                                    </MuiThemeProvider>
                                 </RctCollapsibleCard>


                              </RctCard>


                           </div>

                           :
                           null
                     }

                     

                  </RctCollapsibleCard>
               </div>

            </div>
    
      </React.Fragment>

   )

}


const styles = {
   mainContainer: {
      // border: "2px solid black",
      // margin: "20px",
      marginTop: "10px",
      marginBottom: "10px"
   },
   spaceSize: {
      fontSize: "5px",
      paddingBottom: "1px"
   },
   pinStyle: {
      fontSize: "45px",
      border: "2px solid black",
      marginRight: "300px",
      marginLeft: "300px",
      marginTop: "30px"
   },
   balanceStyle: {
      fontSize: "70px",
      color: "red",
      fontWeight: "bold"
   },
   mainDiv: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      fontSize: "5px",
   },
   mainFont: {
      fontSize: "28px",
   },
   // font: {
   //     fontSize: "25px",
   //     marginRight: "100px !important"
   // },
   fontStyle: {
      fontSize: "x-large"
   },
   wordsFont: {
      fontSize: "19px"
   },
   spaceSize: {
      paddingBottom: "11px",
      fontSize: "large"
   }
}
