//upload comapny cards

import React, { Component } from 'react';
import USER_TOKEN from '../../../constants/Token';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import Modal from 'react-awesome-modal';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// import UploadCardModel from './uploadCardsPopUp';

import { ToastContainer, toast } from 'react-toastify';
import * as XLSX from 'xlsx';
import axios from 'axios';
import Cookies from 'universal-cookie';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
const nf = new Intl.NumberFormat();

const cookies = new Cookies();

class Dropzone extends Component {
   

   state = {
      isUploaded: false,
	  cardsNumber: 1,
	  total:0,
	  initialsellPrice:'',
      excelFileName: '',
      sheetName: '',
      fileSize: 0,
      regions: [],
      companies: [],
      suppliers: [],
      categories: [],
      todayDate: '',
      companyId: '',
      categoryId: 0,
      supplierId: 0,
      source: '',
      regionId: 0,
      bundleNumber: '',
      cardsBuyingPrice: 0,
      cardsSellPrice: 0,
      expiryDate: '',
      currency: '',
      notes: '',
      fileData: [],
      bundleCount: 0,
      companyName: '',
      date: 0,
	  sellPrice: '',
	  CarrdNumber:1,
      purchasePrice: '',
      companyNameError: '',
      categoryNameError: '',
      supplierNameError: '',
      sourceError: '',
      regionError: '',
      expiryDateError: '',
      bundleNumberError: '',
      currencyError: '',
      uploadFileError: '',
      categoryCurrency: '',
      oldCardsCount: 0,
      loadModel: false,
      sellPriceChange: false,
      purchasePriceChange: false,
      fileType: '',
      userId: localStorage.getItem("userIdInUsers"),
      userTypeId: localStorage.getItem("user_type_id"),
   };

   validate = () => {
      let companyNameError = '';
      let categoryNameError = '';
      let supplierNameError = '';
      let sourceError = '';
      let regionError = '';
      let expiryDateError = '';
      let bundleNumberError = '';
      let currencyError = '';
      let uploadFileError = '';
      if (this.state.companyId == '') {
         companyNameError = 'برجاء اختيار الشركة'
      }
      if (this.state.categoryId == 0) {
         categoryNameError = 'برجاء اختيار الفئة'
      }
   
     
   
    
      if (companyNameError || categoryNameError ) {
         this.setState({
            companyNameError, categoryNameError
         })
         return false;
      }


      return true;
   }

   setTodayDate = () => {
      var date = new Date();
      date = date.getUTCFullYear() + 1 + '-' +
         ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
         ('00' + date.getUTCDate()).slice(-2) + ' ';
      // console.log(date);

      this.setState({
         expiryDate: date.trim()
      })
      return date.trim();
   }

   componentDidMount() {
      this.setTodayDate()
      // console.log("cookies", cookies);

      // all regions
      axios.get('http://localhost:8000/allregions', USER_TOKEN)
         .then(response => {
            // console.log("response", response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            }
            else {
               response.data.regions.splice(0, 1);
               this.setState({
                  regions: response.data.regions
               })
            }


         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });

      // all companies
      axios.get('http://localhost:8000/allcompanies', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               response.data.companies.splice(0, 1);
               this.setState({
                  companies: response.data.companies
               })
            }


         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });

      // all suppliers
      axios.get('http://localhost:8000/allsuppliers', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  suppliers: response.data.suppliers
               })
            }


         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });

      // next bundle number
      axios.get('http://localhost:8000/bundleid', USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  bundleNumber: response.data.bundleId
               })
            }
         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });
   }
   constructor(props) {
      super(props);

      // // For a full list of possible configurations,
      // // please consult http://www.dropzonejs.com/#configuration
      // this.djsConfig = {
      //    addRemoveLinks: true,
      //    acceptedFiles: "file/xlsx"
      // };

      // this.componentConfig = {
      //    iconFiletypes: ['.xlsx'],
      //    showFiletypeIcon: true,
      //    postUrl: '/'
      // };

      // // If you want to attach multiple callbacks, simply
      // // create an array filled with all your callbacks.
      // this.callbackArray = [() => console.log('Hi!'), () => console.log('Ho!')];

      // // // Simple callbacks work too, of course
      // this.callback = (file) => { };

      // //this.success = file => console.log('uploaded', file);

      // this.removedfile = file => console.log('removing...', file);

      // this.dropzone = null;
      // this.handleChange = this.handleChange.bind(this);
   }
   handleTwochangeEvent = e => {
      this.handleChange(e);
      this.getAllCategoriesByCompanyID(e);
      // console.log("plaaaaaaaaa");
      this.setCompanyNameSelected(e);
      this.getOldCardsCount(e);
   }

   getOldCardsCount = (event) => {
      // console.log("entered for cards number", event.target.value);
      let companyId = event.target.value;
      // console.log("companyId", companyId);
      axios.post(`http://localhost:8000/cardscount`, { "companyId": companyId }, USER_TOKEN)
         .then(response => {
            // console.log(response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  oldCardsCount: response.data.cardsCount
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });
   }

   setCompanyNameSelected = (event) => {
      for (var i = 0; i < this.state.companies.length; i++) {
         // console.log(event.target.value);
         if (this.state.companies[i].company_id == event.target.value) {
            // console.log(this.state.companies[i].company_name);
            this.setState({
               companyName: this.state.companies[i].company_name
            })
         }
      };
   }

   loadModel = () => {
	let total =parseInt(this.state.cardsNumber)*parseInt(this.state.initialsellPrice)
	// console.log("total",total);
   this.setState({total:total}) 
    //   let compId = this.state.companyId;

    //   axios.post(`http://localhost:8000/cardscount`, { "companyId": compId }, USER_TOKEN)
    //      .then(response => {
    //         console.log(response.data.cardsCount);
    //         if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
    //            cookies.remove('UserToken', { path: '/' })
    //            window.location.href = "/signin";
    //         } else {
    //            this.setState({
    //               oldCardsCount: response.data.cardsCount
    //            })
    //         }
    //         console.log(this.state);
    //      })
    //      .catch(error => {
    //         // console.log('Error fetching and parsing data', error);
    //      });

      let isValidate = this.validate()
      if (isValidate == true) {
		//   console.log("Validation is Ok");
         this.setState({
            loadModel: true
         })
      }

   }

   getAllCategoriesByCompanyID = (event) => {
      // console.log("loggggggg");
      axios.get(`http://localhost:8000/allcategories/${event.target.value}`, USER_TOKEN)
         .then(response => {
            // console.log(response);
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
                  categories: response.data.message
               })
            }
            // console.log(this.state.categories);
         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });
   }

   getpurchaseAndSellPriceByCategoryId = (event) => {
      // console.log("get prices");
      let company_id = this.state.companyId
      let cards_category_id = event.target.value

      axios.post(`http://localhost:8000/allcategoriesbyid`, { company_id, cards_category_id }, USER_TOKEN)
         .then(response => {
            // console.log(response.data.message[0]);
            // console.log("plaaa");
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.setState({
				  sellPrice: response.data.message[0].sell_price,
				  initialsellPrice:response.data.message[0].sell_price,
                  purchasePrice: response.data.message[0].purchase_price,
                  Currency: response.data.message[0].category_currency
               })
            }
            // console.log(this.state);
         })
         .catch(error => {
            // console.log('Error fetching and parsing data', error);
         });
      // console.log(this.state.sellPrice);
   }

   readTextFile = (e) => {
      // console.log(e.target.files[0]);
      var file = e.target.files[0]
      // console.log(e.target.files[0].size);
      var size = file.size;
      var name = file.name;
      let files = e.target.files
      let reader = new FileReader();
      reader.readAsText(files[0]);
      reader.onload = (e) => {
         // console.log(e.target.result);
         var cells = e.target.result.split('\n').map(function (el) { return el.split(/\s+/); });
         let serialnumber, pinNumber, date;
         let headings = [serialnumber, pinNumber, date]

         var obj = cells.map(function (el) {
            var obj = {};
            for (var i = 0, l = el.length; i < l; i++) {
               obj[headings[i]] = isNaN(Number(el[i])) ? el[i] : +el[i];
            }
            return obj;
         });
         var count = obj.length - 1;
         var json = obj;

         this.setState({
            isUploaded: true,
            bundleCount: count,
            excelFileName: name,
            fileSize: size,
            fileData: json
         });
      }

   }

   readExcel = (file) => {
      const promise = new Promise((resolve, reject) => {
         const fileReader = new FileReader();
         fileReader.readAsArrayBuffer(file);

         fileReader.onload = (e) => {
            const bufferArray = e.target.result;
            // console.log(file.type);
            const wb = XLSX.read(bufferArray, { type: 'buffer' });
            const wsname = wb.SheetNames[0];
            //console.log(wb.SheetNames);
            //console.log(wsname);
            const ws = wb.Sheets[wsname]
            const data = XLSX.utils.sheet_to_json(ws);
            data.push(wsname)
            resolve(data)


         };

         fileReader.onerror = ((error) => {
            reject(error);
         })
      })

      promise.then((d) => {
         // console.log("bundleCount", d.length)
         var count = d.length
         if(this.state.companyId == "6"){
            count = count - 1;
         }
         // console.log(d)
         let size = file.size / 1000;
         this.setState({
            isUploaded: true,
            bundleCount: count,
            excelFileName: file.name,
            sheetName: d[d.length - 1],
            fileSize: size,
            fileData: d,
            fileType: "xml"
         });
      })
   }

   handleChange = ({ target }) => {
      this.setState({ ...this.state, [target.name]: target.value });
      // console.log(this.state)
   };

   handleNewPrices = (event) => {
      // console.log("plaaaaa", event.target.name);
      this.setState({ ...this.state, [event.target.name]: event.target.value });
      if (event.target.name == 'cardsBuyingPrice') {
         this.setState({
            purchasePriceChange: true
         })
      } else if (event.target.name == 'cardsSellPrice') {
         this.setState({
            sellPriceChange: true
		 })
		
      }

   }

   handleTwoEvents = (e) => {
      // console.log("entered for getting prices", e.target);
      this.handleChange(e);
      this.getpurchaseAndSellPriceByCategoryId(e);
   }

   closeModel = () => {
      this.setState({
         loadModel: false
      });
   }

   handleSubmit = (e) => {
      e.preventDefault();

      // let isValidate = this.validate()

      // send excel file data to server
      // if (isValidate == true) {

      axios.post(`http://localhost:8000/uploadcards`, this.state, USER_TOKEN)
         .then(response => {
            if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
               cookies.remove('UserToken', { path: '/' })
               window.location.href = "/signin";
            } else {
               this.closeModel();
               // console.log(response.data.message); 
               toast.success('تم تحميل كرتات الشركات بنجاح', {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               });
               setTimeout(function () { location.reload() }, 3000)
               
            }
         })
         .catch(error => {
            this.closeModel();
            // console.log('Error fetching and parsing data ', error);
            toast.error("لقد قمت بتحميل هذة الكوته بالقعل", {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });

            
         });



   }
   render() {

      // console.log(this.state);
      //console.log(this.state.todayDate)
      // const config = this.componentConfig;
      // const djsConfig = this.djsConfig;

      // // For a list of all possible events (there are many), see README.md!
      // const eventHandlers = {
      //    init: dz => this.dropzone = dz,
      //    drop: this.callbackArray,
      //    addedfile: this.callback,
      //    success: this.success,
      //    removedfile: this.removedfile
      // }


      return (
      <React.Fragment>
    	{ PURE_TOKEN_USER_TYPE === 2 && PURE_TOKEN_PHONE_NUM === localStorage.getItem("phoneNumber") ?
         <div className="dropzone-wrapper">
            <ToastContainer />
            <PageTitleBar title={<IntlMessages id="sidebar.RequestCreditFromRepresentative" />} match={this.props.match} />
            <div className="row">


               <RctCollapsibleCard
                  colClasses="col-sm-12 col-md-12 col-lg-12"
                  heading={<IntlMessages id="sidebar.RequestCreditFromRepresentative" />}
               >
                  <form>
				
                     <div className="form-row">
                     <div className="form-group col-md-6">
                           <label><IntlMessages id="form.RepMobile" /></label>

                           <input type="text" className="form-control" id="inputSellCards"  name="cardsSellPrice" defaultValue={this.state.sellPrice} onChange={this.handleNewPrices} />
                           {/* {console.log(this.state)} */}
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.cardsSellPriceError}
                           </div>
                        </div>
					 <div className="form-group col-md-6">
                           <label><IntlMessages id="form.amount" /></label>

                           <input type="number" className="form-control" id="inputSellCards" min="1" max="10000000" name="cardsSellPrice" defaultValue={this.state.sellPrice} onChange={this.handleNewPrices} />
                           {/* {console.log(this.state)} */}
                           <div style={{ color: 'red', fontSize: 12 }}>
                              {this.state.cardsSellPriceError}
                           </div>
                        </div>
                     
                     </div>
                     <button type="button" className="btn btn-primary btn-margin" onClick={this.loadModel}><IntlMessages id="SendRequest" /></button>

                  </form>

                  {/* {
                     this.state.loadModel == true ?
                        <Modal visible={this.state.loadModel} width="550" height="340" effect="fadeInUp" onClickAway={this.closeModel}>
                           <div className="modalstyleInvoice" Style="padding-bottom: 233px;">
                              <div class="modal-header">
                                 <h5 class="modal-title" id="exampleModalCenterTitle"> <IntlMessages id="sidebar.Sellcards" />    </h5>

                                 <a type="button" class="close" aria-label="Close" onClick={this.closeModel}>
                                    <span aria-hidden="true">&times;</span>
                                 </a>
                              </div>
                              <div class="modal-body">
                                 <h3 class="modal-title"><IntlMessages id="sidebar.WillBuy"/> {this.state.cardsNumber} <IntlMessages id="Cards"/> <IntlMessages id="FromCompany"/> {this.state.companyName}</h3>
                                 <blockquote class="blockquote">
                                    <span class="mb-0"> <IntlMessages id="Yourbalancewillbededucted"/>  {this.state.total} </span>
                                  
                                 </blockquote>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" class="btn btn-secondary" onClick={this.closeModel}><IntlMessages id="Colse" /></button>
                                 <button type="button" class="btn btn-primary" onClick={this.handleSubmit}><IntlMessages id="form.Ok" /></button>
                              </div>
                           </div>
                        </Modal>
                        :
                        null
                  } */}
               </RctCollapsibleCard>
            </div>

         </div>
         : (
            this.props.history.push("/access-denied")
            )
         } 
         </React.Fragment>

      )
   }
}
export default Dropzone;
