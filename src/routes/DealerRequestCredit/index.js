/**
 * Pages Routes
 */
import React from 'react';
import { Helmet } from "react-helmet";
import { Redirect, Route, Switch } from 'react-router-dom';

// async components
import {
	AsyncRequestfromEmtdadComponent,
    AsyncRequestfromRepresentativeComponent,
} from 'Components/AsyncComponent/AsyncComponent';


const Pages = ({ match }) => (
	<div className="content-wrapper">
		<Helmet>
			<title>معاملات الوكيل</title>
			<meta name="description" content="Reactify Widgets" />
		</Helmet>
		<Switch>
			<Redirect exact from={`${match.url}/`} to={`${match.url}/RequestfromEmtdad`} />
			<Route path={`${match.url}/RequestfromEmtdad`} component={AsyncRequestfromEmtdadComponent} />
			<Route path={`${match.url}/RequestfromRepresentative`} component={AsyncRequestfromRepresentativeComponent} />
			
		</Switch>
	</div>
);

export default Pages;
