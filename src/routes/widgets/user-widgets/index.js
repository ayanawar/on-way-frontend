/**
 * User Widgets Page
 */
import React, { Component } from 'react';

import {
	ActiveUser,
	PersonalSchedule,
	ToDoListWidget,
	NewCustomersWidget,
	LatestPost,
	ActivityBoard,
	CommentsWidget,
	TwitterFeeds,
	OurLocations,
	SocialCompaninesWidget
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct collapsible card
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';


export default class UserWidgets extends Component {
	render() {
		return (
			<div className="user-widgets-wrapper">
				{/* <PageTitleBar title={<IntlMessages id="sidebar.user" />} match={this.props.match} /> */}
			</div>
		);
	}
}
