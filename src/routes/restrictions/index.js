/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    AsyncOpenRestriction,
    //  AsyncDataTableComponent,
    //  AsyncResponsiveTableComponent,
    //  AsyncDisableSystemComponent,
    //  AsyncResetPasswordComponent,
    //  AsyncSystemUser,
    //  AsyncSystemUserUpdate,
    //  Asynccostcenter,
    //  AsyncAddEmployeeComponent,
    //  AsyncAllEmployeeComponent,
    //  AsyncemployeetransactionComponent
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({ match }) => (
     <div className="content-wrapper">
         <Helmet>
             <title>قيود</title>
             <meta name="description" content="قيود" />
         </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/openedrestriction`} />
             <Route path={`${match.url}/openedrestriction`} component={AsyncOpenRestriction} />
           
         </Switch>
     </div>
 );
 
 export default Pages;
 