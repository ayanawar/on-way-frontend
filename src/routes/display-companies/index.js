/**
 * all companies
 */
import React, { Component } from 'react';
import axios from 'axios';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const cookies = new Cookies();

const options = {
	filter: true,
	filterType: 'dropdown',
	rowsPerPage: 20,
	rowsPerPageOptions: [10,20,32,50,100],
	responsive: 'vertical',
	enableNestedDataAccess: '.',
	selectableRows: "none",
	viewColumns: false,
	download: false,
	sort: false,
	fixedHeader: true,
	fixedSelectColumn: false,
	tableBodyHeight: "720px"
};

class Carts extends Component {
	state = {
			companies: [],
	};
   
   	componentDidMount() {
		// all companies
		axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response => {
			if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else{
				this.setState({
					companies: response.data.companies
				})
			}
		})
		.catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
   }
   
   getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

   render() {
      const { match } = this.props;
      return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
        <div className="cart-wrapper">
			<ToastContainer />
			<Helmet>
				<title>عرض الشركات</title>
				<meta name="description" content="عرض الشركات" />
  			</Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.displayCompanies" />} match={match} />
            <div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard>
						<RctCardContent noPadding>
							<MuiThemeProvider theme={this.getMuiTheme()}>
								<MUIDataTable 
								// title={<IntlMessages id="sidebar.bottomNavigations" />}
								data={this.state.companies}
								columns={[
											{
												label: <IntlMessages id="form.companyEnglishName" />,
												name:  "company_name",
											},
											{
												label: <IntlMessages id="form.companyArabicName" />,
												name:  "company_name_ar" 
											},
											{
												label: <IntlMessages id="components.address" />,
												name:  "company_line_address",
											},
											{
												label: <IntlMessages id="form.region" />,
												name:  "region_arabic_name" 
											},
											{
												label: <IntlMessages id="form.area" />,
												name:  "area_arabic_name",
											},
										]}
									options={options}  
								/>
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
        </div>
		 : (
			this.props.history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      )
   }
}


export default Carts;