import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Modal from 'react-awesome-modal';
// intl messages
import IntlMessages from 'Util/IntlMessages';

// map
// import MapContainer from '../points-of-sale-management/display-and-update-points-of-sale/map-location.js';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies();


export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '',
		hidden: true,
		hiddenStatusForm: true,
		hiddenDebitLimitForm: true,
		hiddenRegionForm: true,
		pos_id: '',
		user_id: '',
		POSComercialName: '',
		POSFirstName: '',
		POSMiddleName: '',
		POSLastName: '',
		currentRegion: '',
		currentArea: '',
		region_id: '',
		area_id: '',
		status: '',
		type:'',
		user_phonenumber: '',
		disabled: false,
		allPOS: [],
		representatives: [],
		dealers: [],
		regions: [],
        areas: [],
		showMap: false,
		posLong: 0,
		posLat:0,
		requestedDevices:[],
		modelstatus:false,
		user_phonenumber:'',
		requestesd_device:'',
		device_number:''
	});


	useEffect(() => {
		axios.get(`http://localhost:8000/getAllPOSActivationRequests`, USER_TOKEN).then(response2 => {
			axios.get(`http://localhost:8000/admin/requesteddevices`, USER_TOKEN).then(response3 => {
				axios.get(`http://localhost:8000/allregions`,USER_TOKEN).then(res4 => {
			
					if (response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized"|| res4.data == "Token Expired" || res4.data == "Token UnAuthorized") {
						cookies.remove('UserToken', { path: '/' })
						window.location.href = "/signin";
					}
					else {
						res4.data.regions.splice(0, 1);
						setState({
							...state,
							allPOS: response2.data.message,
							regions: res4.data.regions,
							requestedDevices:response3.data.message,
							userTypeId: localStorage.getItem('user_type_id'),
							hidden: true,
							hiddenStatusForm: true,
							hiddenDebitLimitForm: true,
							hiddenRegionForm: true,
							disabled: false,
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
						});
					}
				});
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				}
			});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});

	}, []);


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
const updateuserdevice  = ()=>{
	let data = {
		user_phonenumber:state.user_phonenumber,
		requestesd_device:state.requestesd_device,
		device_number:state.device_number
	}
	axios({
		method: 'post', url: 'http://localhost:8000/approverequesteddevice', data:data, headers: { "x-access-token": `${cookies.get('UserToken')}` }
	}).then(res => {
		toast.success("تم تأكيد تفعيل الجهاز بنجاح", {
			position: "top-center",
			autoClose: 4000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});
		setState({ ...state, disabled: true, modelstatus: false })
		setTimeout(function () { location.reload() }, 3000)
	}).catch(error => {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		}
	});
}
	const onClearStatusClicked = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenDebitLimitForm: true,
			hiddenRegionForm: true,
			pos_id: '',
			user_id: '',
			type:'',
			currentRegion: '',
			currentArea: '',
			region_id: '',
			area_id: '',
	
			user_phonenumber: '',
			POSComercialName: '',
			POSFirstName: '',
			POSMiddleName: '',
			POSLastName: '',
			userTypeId: '',
			status: ''
		})
	}

	const closeModel = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenDebitLimitForm: true,
			hiddenRegionForm: true,
			pos_id: '',
			user_id: '',
			type:'',
			currentRegion: '',
			currentArea: '',
			region_id: '',
			area_id: '',
	
			user_phonenumber: '',
			POSComercialName: '',
			POSFirstName: '',
			POSMiddleName: '',
			POSLastName: '',
			userTypeId: '',
			status: '',
			requestesd_device: '',
			device_number: '',
			modelstatus: false
		});
	 }
	const onSubmit2 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/update', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}

	const onSubmit4 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updateareaandregion', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}

	const getAreas = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

    	axios.post(`http://localhost:8000/aresbyregionid`,{"regionId": target.value},USER_TOKEN).then(response => {
				if(response.data.areas.length == 0){
					toast.error('لا يوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({ 
						...state,
						areas: response.data.areas,
						region_id: target.value,
					})

				}else{
					setState({ 
						...state,
						areas: response.data.areas,
						region_id: target.value, 
					})
				}
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
    }


	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			},
			MuiTypography: {
				h6: {
					color: "white",
					fontWeight: "bold",
                    fontSize: "20px",
					backgroundColor: "#599A5F",
					textAlign: "center",
					fontFamily: "'Almarai', sans-serif"
				}
            },
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5, 10, 25, 50, 100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		download: false,
		sort: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "600px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper mb-4">
					<ToastContainer />
					<Helmet>
						<title>طلبات تفعيل الحساب</title>
						<meta name="description" content="طلبات تفعيل الحساب" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.accountActivation" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.report" />}>

							{/*  */}
							{(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit2)}>
									<div className="form-row">
										<div className="form-group col-md-4">
											<label>
												<IntlMessages id="form.commercialName" />: {state.POSComercialName}
											</label>
											{/* <br /> */}
											<label>
												<IntlMessages id="form.name" />:  {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}
											</label>
											<label>
												<IntlMessages id="form.currentStatus" />: {state.status}
											</label>
										</div>
										<div className="form-group col-md-4">
											<label><IntlMessages id="widgets.status" /></label>
											<select className="form-control" name="status_name"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار الحالة</option>
												<option key="1" value="Active">مفعل</option>
												<option key="2" value="Inactive">غير مفعل</option>
												<option key="3" value="Suspend">موقوف</option>
											</select>
											<span className="errors">
												{errors.status_name && errors.status_name.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>

									{(state.disabled === false) ?
										<button type="submit" className="btn btn-warning">
											<IntlMessages id="form.updateStatus" />
										</button> :
										<button type="submit" className="btn btn-warning" disabled={true} >
											<IntlMessages id="form.updateStatus" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}

							{(state.hiddenRegionForm == true) ? <React.Fragment></React.Fragment>
							:
							<React.Fragment>
								<React.Fragment>
									<div className="container">
										<div className="row">
											<div className="col-md-3">
												<label><IntlMessages id="form.name" />: {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}</label>
												<br />
											</div>
											<div className="col-md-3">
												<label><IntlMessages id="form.commercialName" />: {state.POSComercialName}</label>
												<br />
											</div>

											<br />

											<div className="col-md-3">
												<label>المحافظة الحالية: {state.currentRegion}</label>
												<br />
											</div>

											<div className="col-md-3">
												<label>المنطقة الحالية: {state.currentArea}</label>
												<br />
											</div>
											<br />
										</div>
									</div>
								</React.Fragment>

									<br />
								<form inline onSubmit={handleSubmit(onSubmit4)}>
									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.region" /></label>
											<select className="form-control" name="region_id" 
													onChange={getAreas} ref={register({ required: true })}>  
													<option key="0" value="">برجاء اختيار المحافظة</option>    
												{state.regions.map((region)=>{
													return(
														<option key={region.region_id} value={region.region_id}>
															{region.region_arabic_name}
														</option>
													)
												})
												}
											</select>
											<span className="errors">
												{errors.region_id && errors.region_id.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" /> }
											</span>
										</div>
										{(state.region_id == '') ? <React.Fragment></React.Fragment> :
											<div className="form-group col-md-6">
												<label><IntlMessages id="form.area" /></label>
												<select name="area_id" className="form-control input-text" 
														onChange={handleFields} ref={register({ required: true })}>
														<option key="0" value="">برجاء اختيار المنطقة</option>
												{state.areas.map(area => {
													return (
														<option key={area.area_id} value={area.area_id}>
															{area.area_arabic_name}
														</option>
													)
												})
												}
												</select>
												<span className="errors">
													{errors.area_id && errors.area_id.type === 'required' &&
														<IntlMessages id="form.requiredOptionError" /> }
												</span>
											</div>
										}
									</div>
									{(state.disabled === false) ?
										<button type="submit" className="btn btn-dark">
											تعديل
										</button> :
										<button type="submit" className="btn btn-dark" disabled={true} >
											تعديل
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							</React.Fragment>
								}
							{/*  */}
						</RctCollapsibleCard>
					</div>
					<div className="row mb-5">
						<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
							<RctCard className="mb-4">
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											title={"تفعيل الحسابات الموقوفة و غير المفعلة"}
											data={state.allPOS}
											columns={[
												{
													label: "id",
													name: "pos_id",
													options: {
														display: "none",
														filter: false,
														print: false,
													}
												},
												{
													label: <IntlMessages id="form.commercialName" />,
													name: "pos_commercial_name",
												},
												{
													label: <IntlMessages id="form.firstName" />,
													name: "pos_first_name",
												},
												{
													label: <IntlMessages id="form.middleName" />,
													name: "pos_middle_name"
												},
												{
													label: <IntlMessages id="form.lastName" />,
													name: "pos_last_name",
												},
												{
													label: <IntlMessages id="form.phoneNumber" />,
													name: "pos_phone_number"
												},
												{
													label: <IntlMessages id="form.region" />,
													name: "region_arabic_name",
												},
												{
													label: <IntlMessages id="form.area" />,
													name: "area_arabic_name"
												},
												{
													label: <IntlMessages id="form.virtualMoneyBalance" />,
													name: "pos_vm_balance"
												},
												{
													label: <IntlMessages id="form.image" />,
													name: "pos_commercial_logo",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<div>
																	<img src={"http://localhost:8000/" + value} alt="لا يوجد صورة" width="90" height="100" />
																</div>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.dealerName" />,
													name: "dealeruserName"
												},
												{
													label: <IntlMessages id="form.representativeName" />,
													name: "repuserName"
												},

												{
													label: <IntlMessages id="form.type" />,
													name: "pos_type_ar"
												},
												{
													label: <IntlMessages id="form.rank" />,
													name: "pos_rank_ar"
												},
												{
													label: <IntlMessages id="widgets.status" />,
													name: "active_status_ar"
												},
												{
													label: <IntlMessages id="table.allowedDebtLimit" />,
													name: "dept_limit"
												},
												{
													label: <IntlMessages id="table.allowedDebtLimit" />,
													name: "dept_limit_un",
													options: {
														display: "none",
														filter: false,
														print: false,
													}
												},
												{
													label: "مكان التسجيل",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-primary"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				setState({
																					...state,
																					showMap: true,
																					hiddenRegionForm: true,
																					posLong: res.data.tableresult[0].pos_longatude,
																					posLat: res.data.tableresult[0].pos_latitude
																				})
																					

																			}).catch(error => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																						position: "top-center",
																						autoClose: 4000,
																						hideProgressBar: false,
																						closeOnClick: true,
																						pauseOnHover: true,
																						draggable: true
																					});
																				}
																				// console.log('Error fetching and parsing data', error);
																			});
																		}}>
																		عرض مكان التسجيل
																	</button>
																</React.Fragment>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.updateStatus" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-warning"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				setState({
																					...state,
																					user_id: res.data.userresult[0].user_id,
																					user_type_id: res.data.userresult[0].user_type_id,
																					hiddenStatusForm: false,
																					hiddenDebitLimitForm: true,
																					hiddenRegionForm: true,
																					hidden: true,
																					POSComercialName: tableMeta.rowData[1],
																					POSFirstName: tableMeta.rowData[2],
																					POSMiddleName: tableMeta.rowData[3],
																					POSLastName: tableMeta.rowData[4],
																					status: tableMeta.rowData[14],
																				})
																			}).catch((error) => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																						position: "top-center",
																						autoClose: 4000,
																						hideProgressBar: false,
																						closeOnClick: true,
																						pauseOnHover: true,
																						draggable: true
																					});
																				}
																			})
																		}}>
																		<IntlMessages id="form.updateStatus" />
																	</button>
																</React.Fragment>
															);
														}
													}
												},
												{
													label: "تعديل المحافظة و المنطقة",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-success"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {	
																				setState({
																					...state,
																					user_id: res.data.userresult[0].user_id,
																					user_type_id: res.data.userresult[0].user_type_id,
																					hiddenStatusForm: true,
																					hiddenRegionForm: false,
																					hiddenDebitLimitForm: true,
																					hidden: true,
																					user_phonenumber: tableMeta.rowData[5],
																					currentRegion: tableMeta.rowData[6], 
																					currentArea: tableMeta.rowData[7], 
																					POSComercialName: tableMeta.rowData[1],
																					POSFirstName: tableMeta.rowData[2],
																					POSMiddleName: tableMeta.rowData[3],
																					POSLastName: tableMeta.rowData[4],
																					status: tableMeta.rowData[14],
																				})
																			}).catch((error) => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																					position: "top-center",
																					autoClose: 4000,
																					hideProgressBar: false,
																					closeOnClick: true,
																					pauseOnHover: true,
																					draggable: true
																					}); 
																				}
																			})
																		}}>
																		تعديل المحافظة و المنطقة
																	</button>
																</React.Fragment>
															);
														}
													}

												},
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>
					</div>
					
					<div className="row mb-5">
						<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
							<RctCard className="mb-4">
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											title={"تفعيل تخصيص الأجهزة الإضافية"}
											data={state.requestedDevices}
											columns={[
												{
													label: "requestesd_device_number",
													name: "requestesd_device_number",
													options: {
														display: "none",
														filter: false,
														print: false,
													}
												},
												{
													label: <IntlMessages id="form.type" />,
													name: "user_type_name",
												},
												{
													label: <IntlMessages id="form.firstName" />,
													name: "first_name",
												},
												{
													label: <IntlMessages id="form.middleName" />,
													name: "middle_name"
												},
												{
													label: <IntlMessages id="form.lastName" />,
													name: "last_name",
												},
												{
													label: <IntlMessages id="form.phoneNumber" />,
													name: "user_phonenumber"
												},
												{
													label: <IntlMessages id="widgets.status" />,
													name: "active_status",
												},
												{
													label: "كود الجهاز",
													name: "requestesd_device"
												},
												{
													label: "تفعيل / عدم تفعيل الجهاز",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-warning"
																		onClick={() => { 
																			setState({
																				...state,
																				type: tableMeta.rowData[1],
																				POSFirstName: tableMeta.rowData[2],
																				POSMiddleName: tableMeta.rowData[3],
																				POSLastName: tableMeta.rowData[4],
																				user_phonenumber:tableMeta.rowData[5],
																				requestesd_device:tableMeta.rowData[7],
																				device_number:tableMeta.rowData[0],
																				modelstatus:true
																			})
																		}}>
																		تفعيل / عدم تفعيل الجهاز
																	</button>
																</React.Fragment>
															);
														}
													}
												},
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>
					</div>
					
							<div>
								<Modal visible={state.modelstatus} effect="fadeInUp" onClickAway={closeModel}>
								    <div className="modalstyleInvoice">
                               <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalCenterTitle">تأكيد تفعيل الجهاز</h5>
                               </div>
                               <div className="modal-body">
                                  <blockquote className="blockquote">
                                     	<span className="mb-0"> هل ترغب فى تأكيد تفعيل هذا الجهاز </span>
										<br />
										<br />
									 	<h3 className="modal-title"> رقم الهاتف: <span>{state.user_phonenumber}</span> </h3>
										<h3 className="modal-title"> النوع: <span>{state.type}</span> </h3>
										<h3 className="modal-title"> مالك الجهاز: <span>{state.POSFirstName} {state.POSMiddleName} {state.POSLastName}</span></h3>
                                  </blockquote>
                               </div>
                               <div className="modal-footer">
                                  <button type="button" className="btn btn-primary" onClick={updateuserdevice}>تأكيد التفعيل</button>
								  <button type="button" className="btn btn-secondary" onClick={closeModel}>اغلاق</button>
                               </div>
                            </div>
                       			</Modal>
							</div>
					{/* {
						state.showMap == true ?
							<div>
								<MapContainer {...state}/>
							</div>
							:
							null
					} */}
				
				</div>
				
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}