/**
 * Tables Routes
 */
 import React from 'react';
 import { Redirect, Route, Switch } from 'react-router-dom';
 import { Helmet } from "react-helmet";
 // async components
 import {
    Asyncemailconfirm,
   
 } from 'Components/AsyncComponent/AsyncComponent';
 
 const Pages = ({match}) => (
    //  <div className="content-wrapper">
    //      <Helmet>
    //          <title>mail confirmation</title>
    //          <meta name="description" content="mail confirmation" />
    //      </Helmet>
         <Switch>
             <Redirect exact from={`${match.url}/`} to={`${match.url}/emailconfirm`} />
             <Route path={`${match.url}/emailconfirm`} component={Asyncemailconfirm} />
         
         </Switch>
    //  </div>
 );
 
 export default Pages;
 