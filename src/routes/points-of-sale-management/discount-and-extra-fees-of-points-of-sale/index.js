/**
* Report Page
*/
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';


import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 

export default function PosDiscount (props) {
   const history = useHistory();
   const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
      categories:[],
		pos_discount_points: [],
      user_phonenumber:'',
      created_by:'',
      pos_id_in_user:'',
      cards_category_id:'',
      discount_amount:'',
      companies:[],
      tableresult:[],
      userresult:[],
      type:'',
      pos_commercial_name: '',
      pos_first_name:'',
      pos_middle_name:'',
      pos_last_name:'',
      dealer_name:'',
      dealer_phone_number:'',
      dealer_region_arabic_name:'',
      dealer_area_arabic_name: '',
      dealer_virtual_money_balance: '',
      region_arabic_name: '',
      area_arabic_name:'',
      disabled: false,
      hiddenUpdateForm: true,
      pos_vm_balance: ''
   });
   
   const [state2,setState2] = useState({
      companyId:'',
   })

   useEffect(() => {
		axios.get('http://localhost:8000/admin/getallposfees',USER_TOKEN).then(response1 => {
			axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response2 => {
            if(response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response1.data == "Token Expired" || response1.data == "Token UnAuthorized") {
               cookies.remove('UserToken', {path:'/'})
               window.location.href = "/signin";	
            }
            else {
               response2.data.companies.splice(0, 1);
               setState({
                  ...state,
                  disabled: false,
                  pos_discount_points: response1.data.message,
                  companies: response2.data.companies,
                  created_by: localStorage.getItem("userIdInUsers"),
                  hiddenUpdateForm: true,
               })
            }
			}).catch(error => {
				if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            }
			});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});	
	}, []); 

   const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
   };
   const handleField2 = ({ target }) => {
		setState2({ ...state2, [target.name]: target.value });
	};
	const onClearClicked = () => {
		setState({
         ...state, 
         discount_amount: '', 
         cards_category_id: '', 
         categories: [],
         hiddenUpdateForm: true,
      })
   }
   
   const onClearClicked2 = () => {
      setState({
         ...state, 
         user_phonenumber: '',
         pos_first_name:'',
         pos_middle_name:'',
         pos_last_name:'',
         region_arabic_name: '',
         area_arabic_name:'',
         pos_vm_balance: '',
         dealer_name:'',
         dealer_phone_number:'',
         dealer_region_arabic_name:'',
         dealer_area_arabic_name: '',
         dealer_virtual_money_balance: '',
         pos_commercial_name: '',
         pos_id_in_user: ''
      })
	}

   const onClearClicked3 = () => {
      setState({
         ...state, 
         user_phonenumber: '',
         pos_first_name:'',
         pos_middle_name:'',
         pos_last_name:'',
         region_arabic_name: '',
         area_arabic_name:'',
         pos_vm_balance: '',
         dealer_name:'',
         dealer_phone_number:'',
         dealer_region_arabic_name:'',
         dealer_area_arabic_name: '',
         dealer_virtual_money_balance: '',
         pos_commercial_name: '',
         pos_id_in_user: '',
         discount_amount: '', 
         cards_category_id: '', 
         categories: [],
         hiddenUpdateForm: true,
         POSComemercialName: '',
         company_name: '',
         category: '',
         discountID: ''
      })
   }

   const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/creatediscount', data:{
               "created_by":state.created_by,
               "pos_id_in_user":state.pos_id_in_user,
               "company_card_category_id":state.cards_category_id,
               "company_id":state2.companyId,
               "discount_amount":state.discount_amount,
               "type":state.type
            },headers: { "x-access-token": `${cookies.get('UserToken')}`}
			}).then(res =>{
					toast.success(<IntlMessages id="widgets.AddDiscount" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({ ...state, disabled: true })
					setTimeout(function () { location.reload()}, 3000)
			}).catch(error => {
            if (error.response.status === 429) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            } else if (error.response.status === 309) {
               toast.error(error.response.data, {
                  position: "top-center",
                  autoClose: 4000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true
               }); 
            }
			});
		// }
   }


   const getUserDataByphone = (e) => {
      // console.log("entered getUserDataByphone");
      axios.get(`http://localhost:8000/getuserbyphone/${state.user_phonenumber}`,USER_TOKEN)
      .then(response => {
         if(response.data.userresult[0].user_type_id != 3){
            toast.error(<IntlMessages id="toast.NotPosNumber" />, {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            });
            setState({ ...state,
               pos_first_name:'',
               pos_middle_name: '',
               pos_last_name: '',
               
            });
         }else{
         setState({
            ...state,
            pos_first_name: response.data.tableresult[0].pos_first_name,
            pos_middle_name: response.data.tableresult[0].pos_middle_name,
            pos_last_name: response.data.tableresult[0].pos_last_name,
            pos_commercial_name: response.data.tableresult[0].pos_commercial_name,
            dealer_name: response.data.tableresult[0].DealeruserName,
            dealer_phone_number: response.data.tableresult[0].dealer_phone_number,
            dealer_region_arabic_name: response.data.tableresult[0].dealer_region_arabic_name,
            dealer_area_arabic_name: response.data.tableresult[0].dealer_area_arabic_name,
            dealer_virtual_money_balance: response.data.tableresult[0].dealer_virtual_money_balance,
            region_arabic_name: response.data.tableresult[0].region_arabic_name,
            area_arabic_name: response.data.tableresult[0].area_arabic_name,
            pos_vm_balance: response.data.tableresult[0].pos_vm_balance,
            pos_id_in_user:response.data.userresult[0].user_id
            
         });
      }
      })
      .catch(error => {
         if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
      });
   }
 
   const getAllCategories = (e)=>{
      axios.get(`http://localhost:8000/allcategories/${e.target.value}`,USER_TOKEN)
      .then(response => {
         setState({ 
            ...state, 
            categories: response.data.message
         });
      })
      .catch(error => {
         if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
      });
   }

   const handleTwochangeEvent = e => {
      handleField2(e);
      getAllCategories(e);
   }

   const getMuiTheme = () => createMuiTheme({
		overrides: {
         MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
         },
         MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
   })
   
   const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
      sort: false,
      download: false,
      fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "235px"
	};

   const onSubmit3 = e => {
		axios({
	   		method: 'post', url: 'http://localhost:8000/updatediscount', data:{
               "posDiscountId":state.discountID,
               "userTypeId": localStorage.getItem("user_type_id"),
               "discountAmount":state.discount_amount,
	   		},headers: { "x-access-token": `${cookies.get('UserToken')}`}
	   	}).then(res =>{
			toast.success("تم التعديل بنجاح", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		  	setState({ ...state, disabled: true })
		  	setTimeout(function () { location.reload()}, 3000)
	   	}).catch(error => {
		if (error.response.status === 429) {

			toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});

		} else if (error.response.status === 303) {
            toast.error("هذه الشركة والفئة موجودين من قبل من فضلك قم بالتعديل", {
               position: "top-center",
               autoClose: 4000,
               hideProgressBar: false,
               closeOnClick: true,
               pauseOnHover: true,
               draggable: true
            }); 
		  	}
	   });
 	}

	const { match } = props;
   // console.log("state", state);

   
   return (
      <React.Fragment>
    	   { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
         <div className="report-wrapper">
            <ToastContainer />
            <Helmet>
               <title>خصومات و زيادات نقاط البيع و الوكلاء</title>
               <meta name="description" content="خصومات و زيادات نقاط البيع و الوكلاء" />
  			   </Helmet>
            <PageTitleBar title={<IntlMessages id="PageTitleBar.discountAndExtraFess" />} match={match} />
            <div className="row">
                  <RctCollapsibleCard
                     colClasses="col-sm-4 col-md-4 col-lg-4"
                     heading={<IntlMessages id="table.PosData" />}>
                        <form>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="form.phoneNumber" /></label>
                                 <input type="text" className="form-control" name="user_phonenumber"
                                       value={state.user_phonenumber}
                                       onChange={handleFields} 
                                       ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                 <span className="errors">
                                    {errors.user_phonenumber && errors.user_phonenumber.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.user_phonenumber && errors.user_phonenumber.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>
                           <div className="form-group col-md-12">
                              <h1><IntlMessages id="table.PosData" /></h1>
                              <br />
                              <h2><IntlMessages id="form.commercialName" />: {state.pos_commercial_name}</h2>
                              <h2><IntlMessages id="form.name" />: {state.pos_first_name+" "+state.pos_middle_name+" "+state.pos_last_name}</h2>
                              {/* <h2><IntlMessages id="form.phoneNumber" />: {state.user_phonenumber}</h2> */}
                              <h2><IntlMessages id="form.region" />: {state.region_arabic_name}</h2>
                              <h2><IntlMessages id="form.area" />: {state.area_arabic_name}</h2>
                              <h2><IntlMessages id="form.virtualMoneyBalance" />: {state.pos_vm_balance}</h2>
					            </div>

                           {(state.hiddenUpdateForm == true) ? 
                              <React.Fragment>
                                 <button type="button" onClick={getUserDataByphone} className="btn btn-primary">
                                    <IntlMessages id="widgets.search" />
                                 </button>
                                 <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked2}>
                                    <IntlMessages id="form.clear" />
                                 </button>
                              </React.Fragment>
                              : 
                              <React.Fragment></React.Fragment>
                           }
                        </form>
                  </RctCollapsibleCard>
                  <RctCollapsibleCard
                     colClasses="col-sm-4 col-md-4 col-lg-4"
                     heading={"بيانات الوكيل"}>
                     <div className="form-group col-md-12">
                        <h3>بيانات الوكيل المسئول عن نقطة البيع</h3>
                        <br />
                        <h2><IntlMessages id="form.name" />: {state.dealer_name}</h2>
                        <br />
                        <h2><IntlMessages id="form.phoneNumber" />: {state.dealer_phone_number}</h2>
                        <br />
                        <h2><IntlMessages id="form.region" />: {state.dealer_region_arabic_name}</h2>
                        <br />
                        <h2><IntlMessages id="form.area" />: {state.dealer_area_arabic_name}</h2>
                        <br />
                        <h2><IntlMessages id="form.virtualMoneyBalance" />: {state.dealer_virtual_money_balance}</h2>
                     </div>
                  </RctCollapsibleCard>
                  {(state.hiddenUpdateForm == true) ? 
                  <RctCollapsibleCard
                     colClasses="col-sm-4 col-md-4 col-lg-4"
                     heading={<IntlMessages id="PageTitleBar.discountAndExtraFess" />}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="widgets.companyName" /></label>
                                 <select name="companyId" className="form-control" 
                                 onChange={handleTwochangeEvent} ref={register({ required: true })}>
                                       <option key="0" value="">برجاء اختيار الشركه</option>
                                       {state.companies.map(company => {
                                          return (
                                             <option key={company.company_id} value={company.company_id}>
                                                {company.company_name_ar}
                                             </option>
                                          )
                                       })
                                 }
                                 </select>
                                 <span className="errors">
									         {errors.companyId && errors.companyId.type === 'required' &&
										      <IntlMessages id="form.requiredOptionError" /> }
								         </span>
						            </div>    
                           </div>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="widgets.category" /></label>
                                 <select name="cards_category_id" className="form-control" 
                                         onChange={handleFields} ref={register({ required: true })}>
                                       <option key="0" value="">برجاء اختيار الفئه</option>
                                       {/* { console.log(state,state2)} */}
                                       {state.categories.map((category) => {
                                          return (
                                             <option key={category.cards_category_id} value={category.cards_category_id}>
                                                {category.category_text}
                                             </option>
                                          )
                                       })
                                       }
                                 </select>
                                 <span className="errors">
									         {errors.cards_category_id && errors.cards_category_id.type === 'required' &&
										      <IntlMessages id="form.requiredOptionError" /> }
								         </span>	
						            </div>
                           </div>
                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="table.discount_amount"/></label>
                                 <input type="number" className="form-control" name="discount_amount" placeholder="د.ع"
                                       onChange={handleFields} 
                                       ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                 <span className="errors">
                                    {errors.discount_amount && errors.discount_amount.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.discount_amount && errors.discount_amount.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>

                           {(state.disabled === false) ? 
                              <button type="submit" className="btn btn-primary">
                                 <IntlMessages id="form.create" />
                              </button> : 
                              <button type="submit" className="btn btn-primary" disabled={true}>
                              <IntlMessages id="form.create" />
                              </button>
					            }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
                           <IntlMessages id="form.clear" />
                        </button>
                     </form>
                  </RctCollapsibleCard>
               :
                  <RctCollapsibleCard
                     colClasses="col-sm-4 col-md-4 col-lg-4"
                     heading={<IntlMessages id="PageTitleBar.discountAndExtraFess" />}>
                        <form onSubmit={handleSubmit(onSubmit3)}>
                           <div className="form-row m-1">
                              <label className="mr-4">ستقوم بتعديل خصومات و زيادات لنقطة البيع: <span className="reportsCardsNumberCenter">{state.POSComemercialName}</span></label>
                              <br />
                              <label className="mr-4">الشركة: <span className="reportsCardsNumberCenter">{state.company_name}</span></label>
                              <br />
                              <label className="mr-4">الفئة: <span className="reportsCardsNumberCenter">{state.category}</span></label>
                              <br />
                              <br />
						         </div>

                           <div className="form-row">
                              <div className="form-group col-md-12">
                                 <label><IntlMessages id="table.discount_amount"/></label>
                                 <input type="number" className="form-control" name="discount_amount" 
                                        placeholder="د.ع"
                                        onChange={handleFields} 
                                        value={state.discount_amount}
                                        ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
                                 <span className="errors">
                                    {errors.discount_amount && errors.discount_amount.type === 'required' &&
                                       <IntlMessages id="form.requiredError" /> }
                                    {errors.discount_amount && errors.discount_amount.type === 'pattern' &&
                                       <IntlMessages id="form.numbersOnlyErrorError" /> }
                                 </span>
                              </div>
                           </div>

                           {(state.disabled === false) ? 
                              <button type="submit" className="btn btn-primary">
                                 <IntlMessages id="form.update" />
                              </button> : 
                              <button type="submit" className="btn btn-primary" disabled={true}>
                              <IntlMessages id="form.update" />
                              </button>
					            }
                        <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked3}>
                           <IntlMessages id="form.clear" />
                        </button>
                     </form>
                  </RctCollapsibleCard>
               }
            </div>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////// */}
            <div className="row mb-5">
               <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                  <RctCard className="mb-4">
                     <RctCardContent noPadding>
                        <MuiThemeProvider theme={getMuiTheme()}>
                           <MUIDataTable 
                           // title={<IntlMessages id="sidebar.basic" />}
                           data={state.pos_discount_points}
                           columns={[
                                 {
                                    label: <IntlMessages id="table.pos_discount_id" />,
                                    name:  "pos_discount_id",
                                 },
                                 {
                                    label: <IntlMessages id="table.PosName" />,
                                    name:  "PosName",
                                 },
                                 {
                                    label: <IntlMessages id="form.phoneNumber" />,
                                    name:  "pos_phone_number",
                                 },
                                 {
                                    label: <IntlMessages id="form.commercialName" />,
                                    name:  "pos_commercial_name" 
                                 },
                                 {
                                    label: <IntlMessages id="form.companyName"/>,
                                    name:  "company_name_ar" 
                                 },
                                 {
                                    label: <IntlMessages id="widgets.category" />,
                                    name:  "category_text",
                                 },
                                 {
                                    label: <IntlMessages id="table.discount_amount"/>,
                                    name:  "discount_amount_fr" 
                                 },
                                 {
                                    label: "discount_amount",
                                    name:  "discount_amount",
                                    options: {
                                       display: "none",
                                       filter: false,
                                       print: false,
                                    } 
                                 },
                                 {
                                    label: <IntlMessages id="table.type"/>,
                                    name:  "type" 
                                 },                           
                                 {
                                    label: <IntlMessages id="form.dataTime"/>,
                                    name:  "creation_date" 
                                 },
                                 {
                                    label: <IntlMessages id="form.update" />,
                                    name: "",
                                    options: {
                                    filter: true,
                                    sort: false,
                                    empty: true,
                                    customBodyRender: (value, tableMeta, updateValue) => {
                                       return (
                                          <React.Fragment>
                                             <button type="button" className="btn btn-primary" 
                                                onClick={() => {
                                                   axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[2]}`,USER_TOKEN).then(response => { 
                                                      setState({
                                                         ...state,
                                                         hiddenUpdateForm: false,
                                                         discountID: tableMeta.rowData[0],
                                                         POSComemercialName: tableMeta.rowData[3],
                                                         company_name: tableMeta.rowData[4],
                                                         category: tableMeta.rowData[5],
                                                         discount_amount: tableMeta.rowData[6],
                                                         user_phonenumber: tableMeta.rowData[2],
                                                         pos_first_name: response.data.tableresult[0].pos_first_name,
                                                         pos_middle_name: response.data.tableresult[0].pos_middle_name,
                                                         pos_last_name: response.data.tableresult[0].pos_last_name,
                                                         pos_commercial_name: response.data.tableresult[0].pos_commercial_name,
                                                         dealer_name: response.data.tableresult[0].DealeruserName,
                                                         dealer_phone_number: response.data.tableresult[0].dealer_phone_number,
                                                         dealer_region_arabic_name: response.data.tableresult[0].dealer_region_arabic_name,
                                                         dealer_area_arabic_name: response.data.tableresult[0].dealer_area_arabic_name,
                                                         dealer_virtual_money_balance: response.data.tableresult[0].dealer_virtual_money_balance,
                                                         region_arabic_name: response.data.tableresult[0].region_arabic_name,
                                                         area_arabic_name: response.data.tableresult[0].area_arabic_name,
                                                         pos_vm_balance: response.data.tableresult[0].pos_vm_balance,
                                                         pos_id_in_user:response.data.userresult[0].user_id
                                                      }); 
                                                   }).catch(error => {
                                                      if (error.response.status === 429) {
                                                         toast.error(error.response.data, {
                                                            position: "top-center",
                                                            autoClose: 4000,
                                                            hideProgressBar: false,
                                                            closeOnClick: true,
                                                            pauseOnHover: true,
                                                            draggable: true
                                                         }); 
                                                      }
                                                }); 
                                                }}>
                                             <IntlMessages id="form.update" />
                                             </button>
                                          </React.Fragment>
                                       );
                                    }
                                    }
                  
                                 },
                                 ]}
                           options={options}  
                           />
                        </MuiThemeProvider>
                     </RctCardContent>
                  </RctCard>
               </RctCollapsibleCard>
			   </div>
         </div>
         : (
			
            history.push("/access-denied")
            )
         } 
         </React.Fragment>
      );
   
}
