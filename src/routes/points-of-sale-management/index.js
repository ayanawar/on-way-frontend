/**
 * Pages Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";

// async components
import {
    // AsyncGalleryComponent,
    // AsyncFeedbackComponent,
    // AsyncReportComponent,
    // AsyncFaqComponent,
    // AsyncPricingComponent,
    // AsyncBlankComponent,
    // AsyncRegionsDiscountAndExtraFeesComponent,
    // AsyncDiplayPointsOfSaleComponent
    // AsyncDiplayPointsOfSaleComponent
} from 'Components/AsyncComponent/AsyncComponent';

const Pages = ({ match }) => (
    <div className="content-wrapper">
        <Helmet>
			<title>إدارة نقاط البيع</title>
			<meta name="description" content="إدارة نقاط البيع" />
		</Helmet>
        <Switch>
            {/* <Redirect exact from={`${match.url}/`} to={`${match.url}/display-and-update-points-of-sale`} />
            <Route path={`${match.url}/display-and-update-points-of-sale`} component={AsyncGalleryComponent} />
            <Route path={`${match.url}/transactions-of-points-of-sale`} component={AsyncPricingComponent} />
            {/* <Route path={`${match.url}/blank`} component={AsyncBlankComponent} /> */}
            {/* <Route path={`${match.url}/feedback`} component={AsyncFeedbackComponent} /> */}
            {/* <Route path={`${match.url}/discount-and-extra-fees-of-points-of-sale`} component={AsyncReportComponent} />
            <Route path={`${match.url}/regions-discount-and-extra-fees`} component={AsyncRegionsDiscountAndExtraFeesComponent} />
            <Route path={`${match.url}/display-points-of-sale`} component={AsyncDiplayPointsOfSaleComponent} />
            <Route path={`${match.url}/faq`} component={AsyncFaqComponent} /> */} 
        </Switch>
    </div>
);

export default Pages;
