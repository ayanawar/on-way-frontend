import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

// map
// import MapContainer from './map-location.js';

const cookies = new Cookies();


export default function BasicTable(props) {
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		user_type_id: '',
		hidden: true,
		hiddenStatusForm: true,
		hiddenRegionForm: true,
		hiddenDebitLimitForm: true,
		transfer_fees_id: '',
		pos_dealear_id: '',
		pos_rep_id: '',
		pos_type: '',
		pos_rank: '',
		pos_id: '',
		user_id: '',
		POSComercialName: '',
		POSFirstName: '',
		POSMiddleName: '',
		POSLastName: '',
		status: '',
		dealerNameTable: '',
		repNameTable: '',
		POStypeTable: '',
		POSRankTable: '',
		debit_limit: '',
		debitLimitTable: '',
		user_phonenumber: '',
		currentRegion: '',
		currentArea: '',
		region_id: '',
		area_id: '',
		disabled: false,
		allPOS: [],
		regions: [],
        areas: [],
		representatives: [],
		dealers: [],
		showMap: false,
		posLong: 0,
		posLat: 0
	});


	useEffect(() => {
		const Token = cookies.get('UserToken');
		axios({ url: `http://localhost:8000/admin/getallpos/${localStorage.getItem("user_type_id")}`, method: "post", headers:{'access-control-allow-origin':'*',"x-access-token": `${Token}` } }).then(response2 => {
			axios.get('http://localhost:8000/alldealers', USER_TOKEN).then((res2) => {
				axios.get('http://localhost:8000/allrepresentatives', USER_TOKEN).then((res3) => {
					axios.get(`http://localhost:8000/allregions`,USER_TOKEN).then(res4 => {
						if (response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || res2.data == "Token Expired" || res2.data == "Token UnAuthorized" || res3.data == "Token Expired" || res3.data == "Token UnAuthorized" || res4.data == "Token Expired" || res4.data == "Token UnAuthorized") {
							cookies.remove('UserToken', { path: '/' })
							window.location.href = "/signin";
						}
						else {
							res4.data.regions.splice(0, 1);
							setState({
								...state,
								allPOS: response2.data.message,
								userTypeId: localStorage.getItem('user_type_id'),
								dealers: res2.data.dealers,
								representatives: res3.data.representatives,
								regions: res4.data.regions,
								hidden: true,
								hiddenStatusForm: true,
								hiddenRegionForm: true,
								hiddenDebitLimitForm: true,
								disabled: false,
							})
						}
					}).catch(error => {
						console.log(error);
						if (error.response.status === 429) {
							toast.error(error.response.data, {
							   position: "top-center",
							   autoClose: 4000,
							   hideProgressBar: false,
							   closeOnClick: true,
							   pauseOnHover: true,
							   draggable: true
							});
						}
					});
				}).catch(error => {
					console.log(error);
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}	
				});
			}).catch((error) => {
				console.log(error);
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}	
			})
		}).catch(error => {
			console.log(error);
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
		});
	}, []);


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const onClearClicked = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenRegionForm: true,
			hiddenDebitLimitForm: true,
			transfer_fees_id: '',
			pos_dealear_id: '',
			pos_rep_id: '',
			pos_type: '',
			pos_rank: '',
			pos_id: '',
			user_id: '',
			dealerNameTable: '',
			currentRegion: '',
			currentArea: '',
			region_id: '',
			area_id: '',
			repNameTable: '',
			POStypeTable: '',
			POSRankTable: '',
			debit_limit: '',
			debitLimitTable: '',
			user_phonenumber: '',
			POSComercialName: '',
			POSFirstName: '',
			POSMiddleName: '',
			POSLastName: '',
			userTypeId: '',
		})
	}

	const onClearStatusClicked = () => {
		setState({
			...state,
			user_type_id: '',
			hidden: true,
			hiddenStatusForm: true,
			hiddenRegionForm: true,
			hiddenDebitLimitForm: true,
			transfer_fees_id: '',
			pos_dealear_id: '',
			currentRegion: '',
			currentArea: '',
			region_id: '',
			area_id: '',
			pos_rep_id: '',
			pos_type: '',
			pos_rank: '',
			pos_id: '',
			user_id: '',
			dealerNameTable: '',
			repNameTable: '',
			POStypeTable: '',
			POSRankTable: '',
			debit_limit: '',
			debitLimitTable: '',
			user_phonenumber: '',
			POSComercialName: '',
			POSFirstName: '',
			POSMiddleName: '',
			POSLastName: '',
			userTypeId: '',
			status: ''
		})
	}

	const onSubmit = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/adminupdatepos', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updatePOSSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}

	const onSubmit2 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/update', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}

	const onSubmit3 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/admin/updateuserdebitlimit', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success("تم تعديل حد الدين المسموح به", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}


	const onSubmit4 = e => {
		axios({
			method: 'post', url: 'http://localhost:8000/updateareaandregion', data: state, headers: { "x-access-token": `${cookies.get('UserToken')}` }
		}).then(res => {
			toast.success(<IntlMessages id="form.updateStatusSuccess" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload() }, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}	
		});
	}

	const loadMap = () => {

		setState({
			...state,
			showMap: true
		})
	}


	const getAreas = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

    	axios.post(`http://localhost:8000/aresbyregionid`,{"regionId": target.value},USER_TOKEN).then(response => {
				if(response.data.areas.length == 0){
					toast.error('لا يوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({ 
						...state,
						areas: response.data.areas,
						region_id: target.value,
					})

				}else{
					setState({ 
						...state,
						areas: response.data.areas,
						region_id: target.value, 
					})
				}
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
    }

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5, 10, 25, 50, 100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		download: false,
		sort: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "600px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
			{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
				<div className="shop-wrapper mb-4">
					<ToastContainer />
					<Helmet>
						<title>عرض و تعديل نقاط البيع</title>
						<meta name="description" content="عرض و تعديل نقاط البيع" />
					</Helmet>
					<PageTitleBar title={<IntlMessages id="sidebar.gallery" />} match={match} />
					<div className="row">
						<RctCollapsibleCard
							colClasses="col-sm-12 col-md-12 col-lg-12"
							heading={<IntlMessages id="sidebar.report" />}>
							{(state.hidden == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit)}>
									<React.Fragment>
										<div className="container">
											<div className="row">
												<div className="col-md-4">
													<label><IntlMessages id="form.name" />: {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}</label>
													<br />
													<label><IntlMessages id="form.commercialName" />: {state.POSComercialName}</label>
													<br />
												</div>

												<br />

												<div className="col-md-5">
													<label>اسم الوكيل الحالى: {state.dealerNameTable}</label>
													<br />
													<label><IntlMessages id="form.currentRepresentativeName" />: {state.repNameTable}</label>
													<br />
												</div>

												<br />

												<div className="col-md-3">
													<label>النوع الحالى: {state.POStypeTable}</label>
													<br />
													<label>التصنيف الحالى: {state.POSRankTable}</label>
													<br />
												</div>

												<br />
											</div>
										</div>
									</React.Fragment>

									<br />

									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.representativeName" /></label>
											<select className="form-control" name="pos_rep_id"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار اسم المندوب</option>
												{state.representatives.map((representative) => {
													return (
														<option key={representative.representative_id} value={representative.representative_id}>
															{representative.representative_first_name} {representative.representative_middle_name} {representative.representative_last_name}
														</option>
													)
												})
												}
											</select>
											<span className="errors">
												{errors.pos_rep_id && errors.pos_rep_id.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.dealerName" /></label>
											<select className="form-control" name="pos_dealear_id"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار اسم الوكيل</option>
												{state.dealers.map((dealer) => {
													return (
														<option key={dealer.dealer_id} value={dealer.dealer_id}>
															{dealer.dealer_first_name} {dealer.dealer_middle_name} {dealer.dealer_last_name}
														</option>
													)
												})
												}
											</select>
											<span className="errors">
												{errors.pos_dealear_id && errors.pos_dealear_id.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>
									{/*  */}
									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.type" /></label>
											<select className="form-control" name="pos_type"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار النوع</option>
												<option key="1" value="retail">جملة</option>
												<option key="2" value="piece">بالقطعة</option>
											</select>
											<span className="errors">
												{errors.pos_type && errors.pos_type.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.rank" /></label>
											<select className="form-control" name="pos_rank"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار التصنيف</option>
												<option key="1" value="Silver">فضى</option>
												<option key="2" value="Gold">ذهبى</option>
											</select>
											<span className="errors">
												{errors.pos_rank && errors.pos_rank.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>

									{(state.disabled === false) ?
										<button type="submit" className="btn btn-primary">
											<IntlMessages id="form.update" />
										</button>
										:
										<button type="submit" className="btn btn-primary" disabled={true} >
											<IntlMessages id="form.update" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}

							{/*  */}
							{(state.hiddenStatusForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit2)}>
									<div className="form-row">
										<div className="form-group col-md-4">
											<label>
												<IntlMessages id="form.commercialName" />: {state.POSComercialName}
											</label>
											{/* <br /> */}
											<label>
												<IntlMessages id="form.name" />:  {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}
											</label>
											<label>
												<IntlMessages id="form.currentStatus" />: {state.status}
											</label>
										</div>
										<div className="form-group col-md-4">
											<label><IntlMessages id="widgets.status" /></label>
											<select className="form-control" name="status_name"
												onChange={handleFields} ref={register({ required: true })}>
												<option key="0" value="">برجاء اختيار الحالة</option>
												<option key="1" value="Active">مفعل</option>
												<option key="2" value="Inactive">غير مفعل</option>
												<option key="3" value="Suspend">موقوف</option>
											</select>
											<span className="errors">
												{errors.status_name && errors.status_name.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" />}
											</span>
										</div>
									</div>

									{(state.disabled === false) ?
										<button type="submit" className="btn btn-warning">
											<IntlMessages id="form.updateStatus" />
										</button> :
										<button type="submit" className="btn btn-warning" disabled={true} >
											<IntlMessages id="form.updateStatus" />
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}
							{/*  */}
							{(state.hiddenRegionForm == true) ? <React.Fragment></React.Fragment>
							:
							<React.Fragment>
								<React.Fragment>
									<div className="container">
										<div className="row">
											<div className="col-md-3">
												<label><IntlMessages id="form.name" />: {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}</label>
												<br />
											</div>
											<div className="col-md-3">
												<label><IntlMessages id="form.commercialName" />: {state.POSComercialName}</label>
												<br />
											</div>

											<br />

											<div className="col-md-3">
												<label>المحافظة الحالية: {state.currentRegion}</label>
												<br />
											</div>

											<div className="col-md-3">
												<label>المنطقة الحالية: {state.currentArea}</label>
												<br />
											</div>
											<br />
										</div>
									</div>
								</React.Fragment>

									<br />
								<form inline onSubmit={handleSubmit(onSubmit4)}>
									<div className="form-row">
										<div className="form-group col-md-6">
											<label><IntlMessages id="form.region" /></label>
											<select className="form-control" name="region_id" 
													onChange={getAreas} ref={register({ required: true })}>  
													<option key="0" value="">برجاء اختيار المحافظة</option>    
												{state.regions.map((region)=>{
													return(
														<option key={region.region_id} value={region.region_id}>
															{region.region_arabic_name}
														</option>
													)
												})
												}
											</select>
											<span className="errors">
												{errors.region_id && errors.region_id.type === 'required' &&
													<IntlMessages id="form.requiredOptionError" /> }
											</span>
										</div>
										{(state.region_id == '') ? <React.Fragment></React.Fragment> :
											<div className="form-group col-md-6">
												<label><IntlMessages id="form.area" /></label>
												<select name="area_id" className="form-control input-text" 
														onChange={handleFields} ref={register({ required: true })}>
														<option key="0" value="">برجاء اختيار المنطقة</option>
												{state.areas.map(area => {
													return (
														<option key={area.area_id} value={area.area_id}>
															{area.area_arabic_name}
														</option>
													)
												})
												}
												</select>
												<span className="errors">
													{errors.area_id && errors.area_id.type === 'required' &&
														<IntlMessages id="form.requiredOptionError" /> }
												</span>
											</div>
										}
									</div>
									{(state.disabled === false) ?
										<button type="submit" className="btn btn-dark">
											تعديل
										</button> :
										<button type="submit" className="btn btn-dark" disabled={true} >
											تعديل
										</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							</React.Fragment>
								}
							{/*  */}
							{(state.hiddenDebitLimitForm == true) ? <React.Fragment></React.Fragment>
								:
								<form onSubmit={handleSubmit(onSubmit3)}>
									<div className="form-row">
										<div className="form-group col-md-4">
											<label>
												<IntlMessages id="form.commercialName" />: {state.POSComercialName}
											</label>
											{/* <br /> */}
											<label>
												<IntlMessages id="form.name" />:  {state.POSFirstName} {state.POSMiddleName} {state.POSLastName}
											</label>
											<label>
												<IntlMessages id="table.allowedDebtLimit" />: {state.debitLimitTable}
											</label>
										</div>
										<div className="form-group col-md-4">
											<label><IntlMessages id="table.allowedDebtLimit" /></label>
											<input type="tel" className="form-control" name="debit_limit"
												onChange={handleFields} value={state.debit_limit}
												ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })} />

											<span className="errors">
												{errors.debit_limit && errors.debit_limit.type === 'required' &&
													<IntlMessages id="form.requiredError" />}
												{errors.debit_limit && errors.debit_limit.type === 'pattern' &&
													<IntlMessages id="form.numbersOnlyErrorError" />}
											</span>
										</div>
									</div>

									{(state.disabled === false) ?
										<button type="submit" className="btn btn-dark">
											<IntlMessages id="table.updateAllowedDebtLimit" />
                        				</button> :
										<button type="submit" className="btn btn-dark" disabled={true} >
											<IntlMessages id="table.updateAllowedDebtLimit" />
                        				</button>
									}
									<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
										<IntlMessages id="form.clear" />
									</button>
								</form>
							}
						</RctCollapsibleCard>
					</div>
					<div className="row mb-5">
						<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
							<RctCard className="mb-4">
								<RctCardContent noPadding>
									<MuiThemeProvider theme={getMuiTheme()}>
										<MUIDataTable
											// title={<IntlMessages id="sidebar.basic" />}
											data={state.allPOS}
											columns={[
												{
													label: "id",
													name: "pos_id",
													options: {
														display: "none",
														filter: false,
														print: false,
													}
												},
												{
													label: <IntlMessages id="form.commercialName" />,
													name: "pos_commercial_name",
												},
												{
													label: <IntlMessages id="form.firstName" />,
													name: "pos_first_name",
												},
												{
													label: <IntlMessages id="form.middleName" />,
													name: "pos_middle_name"
												},
												{
													label: <IntlMessages id="form.lastName" />,
													name: "pos_last_name",
												},
												{
													label: <IntlMessages id="form.phoneNumber" />,
													name: "pos_phone_number"
												},
												{
													label: <IntlMessages id="form.region" />,
													name: "region_arabic_name",
												},
												{
													label: <IntlMessages id="form.area" />,
													name: "area_arabic_name"
												},
												{
													label: <IntlMessages id="form.virtualMoneyBalance" />,
													name: "pos_vm_balance"
												},
												{
													label: <IntlMessages id="form.image" />,
													name: "pos_commercial_logo",
													options: {
														filter: true,
														sort: false,
														empty: true,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<div>
																	<img src={"http://localhost:8000/" + value} alt="لا يوجد صورة" width="90" height="100" />
																</div>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.dealerName" />,
													name: "dealeruserName"
												},
												{
													label: <IntlMessages id="form.representativeName" />,
													name: "repuserName"
												},

												{
													label: <IntlMessages id="form.type" />,
													name: "pos_type_ar"
												},
												{
													label: <IntlMessages id="form.rank" />,
													name: "pos_rank_ar"
												},
												{
													label: <IntlMessages id="widgets.status" />,
													name: "active_status_ar"
												},
												{
													label: <IntlMessages id="table.allowedDebtLimit" />,
													name: "dept_limit"
												},
												{
													label: <IntlMessages id="table.allowedDebtLimit" />,
													name: "dept_limit_un",
													options: {
														display: "none",
														filter: false,
														print: false,
													}
												},
												{
													label: "تاريخ و وقت التسجيل",
													name: "creation_date"
												},
												{
													label: "مكان التسجيل",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-primary"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				// console.log("response", res.data.tableresult[0].pos_longatude);
																				// console.log("response", res.data.tableresult[0].pos_latitude);
																				// if(state.loadMap == true){
																				// 	setState({
																				// 		...state,
																				// 		showMap: false
																				// 	})
																				// }
																				setState({
																					...state,
																					showMap: true,
																					posLong: res.data.tableresult[0].pos_longatude,
																					posLat: res.data.tableresult[0].pos_latitude
																				})
																				

																			}).catch(error => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																					   position: "top-center",
																					   autoClose: 4000,
																					   hideProgressBar: false,
																					   closeOnClick: true,
																					   pauseOnHover: true,
																					   draggable: true
																					}); 
																				}	
																			});
																		}}>
																		عرض مكان التسجيل
														</button>
																</React.Fragment>
															);
														}
													}
												},
												{
													label: <IntlMessages id="form.update" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-primary"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				axios.get('http://localhost:8000/alldealers', USER_TOKEN).then((res2) => {
																					axios.get('http://localhost:8000/allrepresentatives', USER_TOKEN).then((res3) => {
																						// console.log(res.data.userresult[0].user_id);
																						setState({
																							...state,
																							pos_id: tableMeta.rowData[0],
																							dealers: res2.data.dealers,
																							representatives: res3.data.representatives,
																							hidden: false,
																							hiddenStatusForm: true,
																							hiddenRegionForm: true,
																							hiddenDebitLimitForm: true,
																							POSComercialName: tableMeta.rowData[1],
																							POSFirstName: tableMeta.rowData[2],
																							POSMiddleName: tableMeta.rowData[3],
																							POSLastName: tableMeta.rowData[4],
																							dealerNameTable: tableMeta.rowData[10],
																							repNameTable: tableMeta.rowData[11],
																							POStypeTable: tableMeta.rowData[12],
																							POSRankTable: tableMeta.rowData[13],
																						})
																					}).catch((error) => {
																						if (error.response.status === 429) {
																							toast.error(error.response.data, {
																							   position: "top-center",
																							   autoClose: 4000,
																							   hideProgressBar: false,
																							   closeOnClick: true,
																							   pauseOnHover: true,
																							   draggable: true
																							}); 
																						}	
																					})
																				}).catch((error) => {
																					if (error.response.status === 429) {
																						toast.error(error.response.data, {
																						   position: "top-center",
																						   autoClose: 4000,
																						   hideProgressBar: false,
																						   closeOnClick: true,
																						   pauseOnHover: true,
																						   draggable: true
																						}); 
																					}	
																				}).catch(error => {
																					if (error.response.status === 429) {
																						toast.error(error.response.data, {
																						   position: "top-center",
																						   autoClose: 4000,
																						   hideProgressBar: false,
																						   closeOnClick: true,
																						   pauseOnHover: true,
																						   draggable: true
																						}); 
																					}	
																				});

																			})
																		}}>
																		<IntlMessages id="form.update" />
																	</button>
																</React.Fragment>
															);
														}
													}

												},
												{
													label: <IntlMessages id="form.updateStatus" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-warning"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				axios.get('http://localhost:8000/alldealers', USER_TOKEN).then((res2) => {
																					axios.get('http://localhost:8000/allrepresentatives', USER_TOKEN).then((res3) => {
																						// console.log(res.data.userresult[0].user_id);
																						setState({
																							...state,
																							user_id: res.data.userresult[0].user_id,
																							user_type_id: res.data.userresult[0].user_type_id,
																							dealers: res2.data.dealers,
																							representatives: res3.data.representatives,
																							hiddenStatusForm: false,
																							hiddenRegionForm: true,
																							hiddenDebitLimitForm: true,
																							hidden: true,
																							POSComercialName: tableMeta.rowData[1],
																							POSFirstName: tableMeta.rowData[2],
																							POSMiddleName: tableMeta.rowData[3],
																							POSLastName: tableMeta.rowData[4],
																							status: tableMeta.rowData[14],
																						})
																					}).catch((error) => {
																						if (error.response.status === 429) {
																							toast.error(error.response.data, {
																							position: "top-center",
																							autoClose: 4000,
																							hideProgressBar: false,
																							closeOnClick: true,
																							pauseOnHover: true,
																							draggable: true
																							}); 
																						}
																					})
																				}).catch((error) => {
																					if (error.response.status === 429) {
																						toast.error(error.response.data, {
																						position: "top-center",
																						autoClose: 4000,
																						hideProgressBar: false,
																						closeOnClick: true,
																						pauseOnHover: true,
																						draggable: true
																						}); 
																					}
																				}).catch(error => {
																					if (error.response.status === 429) {
																						toast.error(error.response.data, {
																						position: "top-center",
																						autoClose: 4000,
																						hideProgressBar: false,
																						closeOnClick: true,
																						pauseOnHover: true,
																						draggable: true
																						}); 
																					}
																				});

																			})
																		}}>
																		<IntlMessages id="form.updateStatus" />
																	</button>
																</React.Fragment>
															);
														}
													}

												},
												{
													label: "تعديل المحافظة و المنطقة",
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-success"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {	
																				setState({
																					...state,
																					user_id: res.data.userresult[0].user_id,
																					user_type_id: res.data.userresult[0].user_type_id,
																					hiddenStatusForm: true,
																					hiddenRegionForm: false,
																					hiddenDebitLimitForm: true,
																					hidden: true,
																					user_phonenumber: tableMeta.rowData[5],
																					currentRegion: tableMeta.rowData[6], 
																					currentArea: tableMeta.rowData[7], 
																					POSComercialName: tableMeta.rowData[1],
																					POSFirstName: tableMeta.rowData[2],
																					POSMiddleName: tableMeta.rowData[3],
																					POSLastName: tableMeta.rowData[4],
																					status: tableMeta.rowData[14],
																				})
																			}).catch((error) => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																					position: "top-center",
																					autoClose: 4000,
																					hideProgressBar: false,
																					closeOnClick: true,
																					pauseOnHover: true,
																					draggable: true
																					}); 
																				}
																			})
																		}}>
																		تعديل المحافظة و المنطقة
																	</button>
																</React.Fragment>
															);
														}
													}

												},
												{
													label: <IntlMessages id="table.updateAllowedDebtLimit" />,
													name: "",
													options: {
														filter: true,
														sort: false,
														empty: true,
														print: false,
														customBodyRender: (value, tableMeta, updateValue) => {
															return (
																<React.Fragment>
																	<button type="button" className="btn btn-dark"
																		onClick={() => {
																			axios.get(`http://localhost:8000/getuserbyphone/${tableMeta.rowData[5]}`, USER_TOKEN).then((res) => {
																				
																				setState({
																					...state,
																					user_id: res.data.userresult[0].user_id,
																					user_type_id: res.data.userresult[0].user_type_id,
																					hiddenStatusForm: true,
																					hiddenRegionForm: true,
																					hidden: true,
																					hiddenDebitLimitForm: false,
																					user_phonenumber: tableMeta.rowData[5],
																					POSComercialName: tableMeta.rowData[1],
																					POSFirstName: tableMeta.rowData[2],
																					POSMiddleName: tableMeta.rowData[3],
																					POSLastName: tableMeta.rowData[4],
																					debit_limit: tableMeta.rowData[16],
																					debitLimitTable: tableMeta.rowData[15],
																				})
																			}).catch((error) => {
																				if (error.response.status === 429) {
																					toast.error(error.response.data, {
																					position: "top-center",
																					autoClose: 4000,
																					hideProgressBar: false,
																					closeOnClick: true,
																					pauseOnHover: true,
																					draggable: true
																					}); 
																				}
																			})
																		}}>
																		<IntlMessages id="table.updateAllowedDebtLimit" />
														</button>
																</React.Fragment>
															);
														}
													}

												},
											]}
											options={options}
										/>
									</MuiThemeProvider>
								</RctCardContent>
							</RctCard>
						</RctCollapsibleCard>


					</div>
					

				</div>
				: (

					history.push("/access-denied")
				)
			}
		</React.Fragment>
	)
}