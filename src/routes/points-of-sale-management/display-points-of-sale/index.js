import React, { useState } from 'react';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Button, Form, FormGroup, Label, } from 'reactstrap';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const cookies = new Cookies();

const nf = new Intl.NumberFormat();

export default function Shop(props) {

    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

    const fileExtension = '.xlsx';

    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        name: '',
        phoneNumber: '',
        phoneNumberField: '',
        user_type_id: '',
        user_phonenumber: '',
        receipts: [],
        showExportExcel: false,
        from_date: '',
        to_date: '',
        allPOS: [],
        dataToExport: [],
        isAll: true
    });

    React.useEffect(() => {
        var date = new Date();
        date = date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2);
        // console.log(date);

        setState({
            ...state,
            from_date: date,
            to_date: date
        })

        axios.post(`http://localhost:8000/admin/getallpos/${localStorage.getItem("user_type_id")}`,
            {
                'fromDate': date,
                'toDate': date,
                'isAll': true
            }, USER_TOKEN).then(res2 => {
                console.log("res2 All", res2);
                if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else {
                    if (res2.data.message.length == 0) {
                        toast.error(<IntlMessages id="components.NoItemFound" />, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                        setState({
                            ...state,
                            allPOS: res2.data.message,
                            showExportExcel: false,
                            from_date: date,
                            to_date: date
                        })
                    }
                    else {
                        setState({
                            ...state,
                            allPOS: res2.data.message,
                            showExportExcel: true,
                            from_date: date,
                            to_date: date
                        })
                    }
                }
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });


    }, []);

    const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
					maxHeight: 'unset',
					overflowX: 'unset',
					overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				},
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar: {
				regular: {
					backgroundColor: "gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor: "gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})

    // let totalBalancesToBePrinted = 0;
    // let cardsIds = [];
    // let bundleIds = [];
    // let bundleNumbers = [];
    // let pins = [];
    // let serialNumbers = [];
    // let selectedBalances = [];
    // let selectedBalances2 = []
    // let selectedRowsWithData = [];
    // let selectedCount = 0;
    let selectedRowsToExport = [];

    const handlemultiSelect = (selectedRows, displayData) => {
        console.log("hii", selectedRows);
        console.log("displayDatalength", displayData.length);
        console.log("displayData", displayData);
        // cardsIds = [];
        // bundleIds = [];
        // bundleNumbers = [];
        // pins = [];
        // serialNumbers = [];
        // selectedBalances = [];
        // selectedBalances2 = [];
        // selectedRowsWithData = [];
        // selectedCount = 0;
        selectedRowsToExport = [];

        console.log("selectedRowsArray", selectedRows);
        selectedRows.data.forEach(element => {
            console.log("element", element);
            // cardsIds.push(displayData[element.index].data[0]);
            // bundleIds.push(displayData[element.index].data[1]);
            // bundleNumbers.push(displayData[element.index].data[2]);
            // pins.push(displayData[element.index].data[3]);
            // serialNumbers.push(displayData[element.index].data[4])
            // selectedBalances2.push(displayData[element.index].data[5]);
            // selectedBalances.push(displayData[element.index].data[6]);
            selectedRowsToExport.push({
                "Commercial Name": displayData[element.index].data[1],
                "First Name": displayData[element.index].data[2],
                "Second Name": displayData[element.index].data[3],
                "Last Name": displayData[element.index].data[4],
                "POS Phone": displayData[element.index].data[5],
                "region": displayData[element.index].data[6],
                "area": displayData[element.index].data[7],
                "Current Balance": parseLocaleNumber(displayData[element.index].data[8]),
                "Dealer Name": displayData[element.index].data[10],
                "Representative Name": displayData[element.index].data[11],
                "POS Type": displayData[element.index].data[12],
                "rate": displayData[element.index].data[13],
                "status": displayData[element.index].data[14],
                "Dept Limit": displayData[element.index].data[16],
                "Date And Time": displayData[element.index].data[17]
            })
        });
        // console.log("selectedBalances: ", selectedBalances);
        console.log("selectedRowsToExport", selectedRowsToExport);
        // setState({
        //     ...state,
        //     dataToExport: selectedRowsToExport
        // })

        // totalBalancesToBePrinted = 0;
        // for (let i = 0; i < selectedBalances.length; i++) {
        //     totalBalancesToBePrinted += selectedBalances[i];
        //     selectedCount = selectedBalances.length
        // }
        return (
            <React.Fragment>
                <div className="form-group col-md-12 mt-3 text-center" id="cardsToBePrintedDiv">
                    {/* <h2 className="mb-3"><IntlMessages id="form.cardsToPrint" />: <span>12586</span></h2>
                    <h2><span>عدد الكارتات المسحوبة: </span><span>125</span></h2>
                    <div className="row">
                        <div className="col-6">
                            <h2><span>الشركة:</span> <span>asiacell</span></h2>
                        </div>
                        <div className="col-6">
                            <h2><span>الفئة: <span>12</span></span></h2>
                        </div>
                    </div> */}

                    {selectedRowsToExport.length == 0 ? <React.Fragment></React.Fragment> :
                    <div className="form-group col-md-12">
                        {/* <br /> */}
                        {/* <div className="container"> */}
                            {/* {(state.disabled === false) ? */}
                            <div className="row justify-content-center">
                                <button className="btn btn-primary" onClick={(e) => exportToCSV()}>
                                    <IntlMessages id="form.exportExcel" />
                                </button>
                            </div>
                            {/* :
                                    <div className="row justify-content-center">
                                        <button className="btn btn-primary" disabled={true}>
                                            <IntlMessages id="form.exportExcel" />
                                        </button>
                                    </div> */}
                            {/* } */}
                        {/* </div> */}
                    </div>
                    }
                </div>
            </React.Fragment>
        )
    }

    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5, 10, 25, 50, 100, 250, 500],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        customToolbarSelect: handlemultiSelect,
        selectableRowsHeader: true,
        download: false,
        print: false,
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        fixedSelectColumn: false,
        tableBodyHeight: "345px"
    };

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const handleSearch = e => {
        axios.post(`http://localhost:8000/admin/getallpos/${localStorage.getItem("user_type_id")}`,
            {
                'fromDate': state.from_date,
                'toDate': state.to_date,
                'isAll': false
            }, USER_TOKEN).then(res2 => {
                console.log("res2 in search", res2);
                if (res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else {
                    if (res2.data.message.length == 0) {
                        toast.error(<IntlMessages id="components.NoItemFound" />, {
                            position: "top-center",
                            autoClose: 4000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                        setState({
                            ...state,
                            allPOS: res2.data.message,
                            showExportExcel: false
                        })
                    }
                    else {
                        setState({
                            ...state,
                            allPOS: res2.data.message,
                            showExportExcel: true
                        })
                    }
                }
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            });
    }

    const parseLocaleNumber = (stringNumber, locale) => {
        var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
        var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

        return parseFloat(stringNumber
            .replace(new RegExp('\\' + thousandSeparator, 'g'), '')
            .replace(new RegExp('\\' + decimalSeparator), '.')
        );
    }

    const exportToCSV = () => {

        const ws = XLSX.utils.json_to_sheet(selectedRowsToExport);

        const wb = { Sheets: { 'نقاط البيع': ws }, SheetNames: ['نقاط البيع'] };

        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

        const data = new Blob([excelBuffer], { type: fileType });

        FileSaver.saveAs(data, `نقاط البيع - ${state.from_date} - ${state.to_date}` + fileExtension);
    }

    const { match } = props;
    console.log("state", state);
    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="cart-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>عرض نقاط البيع</title>
                        <meta name="description" content="عرض نقاط البيع" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.dispalyPOS" />} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.dispalyPOS" />}>


                            <div className="row">
                                <div className="col-sm-4 col-lg-4 col-xl-4">
                                    <h3><IntlMessages id="form.dateFrom" /></h3>
                                    <input name="from_date" placeholder="date placeholder"
                                        defaultValue={state.from_date}
                                        type="date" className="form-control" onChange={handleFields}
                                        ref={register({ required: true })} />
                                    <span className="errors m-2">
                                        {errors.from_date && errors.from_date.type === 'required' &&
                                            <IntlMessages id="form.requiredOptionError" />}
                                    </span>
                                </div>
                                <div className="col-sm-4 col-lg-4 col-xl-4">
                                    <span><IntlMessages id="form.dateTo" /></span>
                                    <input name="to_date" placeholder="date placeholder"
                                        defaultValue={state.to_date}
                                        type="date" className="form-control"
                                        onChange={handleFields} value={state.to_date}
                                        ref={register({ required: true })} />
                                    <span className="errors m-2">
                                        {errors.to_date && errors.to_date.type === 'required' &&
                                            <IntlMessages id="form.requiredOptionError" />}
                                    </span>
                                </div>
                                <br />
                                <div className="col-sm-4 col-lg-4 col-xl-4">
                                    <div className="container">
                                        <button color="primary" className="btn btn-primary"
                                            onClick={handleSearch} style={{ marginTop: "24px" }}>
                                            ابحث
                                </button>
                                        {(state.hiddenExcelButton === false) ?
                                            <React.Fragment>
                                                <button type="button" className="btn btn-primary mr-4 ml-4" style={{ marginTop: "24px" }} onClick={(e) => exportToCSV()}>
                                                    <IntlMessages id="form.exportExcel" />
                                                </button>
                                            </React.Fragment>
                                            :
                                            <React.Fragment></React.Fragment>
                                        }
                                    </div>
                                </div>
                            </div>


                            <br />
                            <br />
                            <RctCard>
                                <RctCollapsibleCard fullBlock>
                                    <MuiThemeProvider theme={getMuiTheme()}>
                                        <MUIDataTable
                                            data={state.allPOS}
                                            columns={[
                                                {
                                                    label: "id",
                                                    name: "pos_id",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    }
                                                },
                                                {
                                                    label: <IntlMessages id="form.commercialName" />,
                                                    name: "pos_commercial_name",
                                                },
                                                {
                                                    label: <IntlMessages id="form.firstName" />,
                                                    name: "pos_first_name",
                                                },
                                                {
                                                    label: <IntlMessages id="form.middleName" />,
                                                    name: "pos_middle_name"
                                                },
                                                {
                                                    label: <IntlMessages id="form.lastName" />,
                                                    name: "pos_last_name",
                                                },
                                                {
                                                    label: <IntlMessages id="form.phoneNumber" />,
                                                    name: "pos_phone_number"
                                                },
                                                {
                                                    label: <IntlMessages id="form.region" />,
                                                    name: "region_arabic_name",
                                                },
                                                {
                                                    label: <IntlMessages id="form.area" />,
                                                    name: "area_arabic_name"
                                                },
                                                {
                                                    label: <IntlMessages id="form.virtualMoneyBalance" />,
                                                    name: "pos_vm_balance"
                                                },
                                                {
                                                    label: <IntlMessages id="form.image" />,
                                                    name: "pos_commercial_logo",
                                                    options: {
                                                        filter: true,
                                                        sort: false,
                                                        empty: true,
                                                        customBodyRender: (value, tableMeta, updateValue) => {
                                                            return (
                                                                <div>
                                                                    <img src={"http://localhost:8000/" + value} alt="لا يوجد صورة" width="90" height="100" />
                                                                </div>
                                                            );
                                                        }
                                                    }
                                                },
                                                {
                                                    label: <IntlMessages id="form.dealerName" />,
                                                    name: "dealeruserName"
                                                },
                                                {
                                                    label: <IntlMessages id="form.representativeName" />,
                                                    name: "repuserName"
                                                },

                                                {
                                                    label: <IntlMessages id="form.type" />,
                                                    name: "pos_type_ar"
                                                },
                                                {
                                                    label: <IntlMessages id="form.rank" />,
                                                    name: "pos_rank_ar"
                                                },
                                                {
                                                    label: <IntlMessages id="widgets.status" />,
                                                    name: "active_status_ar"
                                                },
                                                {
                                                    label: <IntlMessages id="table.allowedDebtLimit" />,
                                                    name: "dept_limit"
                                                },
                                                {
                                                    label: <IntlMessages id="table.allowedDebtLimit" />,
                                                    name: "dept_limit_un",
                                                    options: {
                                                        display: "none",
                                                        filter: false,
                                                        print: false,
                                                    }
                                                },
                                                {
                                                    label: "تاريخ و وقت التسجيل",
                                                    name: "creation_date"
                                                }

                                            ]}
                                            options={options}
                                        />
                                    </MuiThemeProvider>
                                </RctCollapsibleCard>
                            </RctCard>
                        </RctCollapsibleCard>
                    </div>
                </div>
                : (

                    history.push("/access-denied")
                )
            }
        </React.Fragment>
    )
}


