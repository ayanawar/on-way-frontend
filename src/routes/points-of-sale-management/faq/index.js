/**
 * Faq Page
 */
import React, { Component } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { Helmet } from "react-helmet";
// api
import api from 'Api';

//Components
import SearchIdeas from './components/SearchIdeas';

class Faq extends Component {

	state = {
		faqs: null
	}

	componentDidMount() {
		this.getFaqs();
	}

	// get faqs
	getFaqs() {
		api.get('faqs.js')
			.then((response) => {
				this.setState({ faqs: response.data });
			})
			.catch(error => {
				// error handling
			})
	}

	render() {
		const { faqs } = this.state;
		return (
			<div className="faq-page-wrapper">
			</div>
		)
	}
}

export default Faq;
