/**
 * add supplier
 */
import React, { useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";

const cookies = new Cookies(); 


export default function Shop(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		disabled: false,
		hiddenExcelButton: true,
		rank: '',
        commercialName: '',
        userIdInTable:'',
		userTypeIdInTable: '',
		posSoldCardsCount: '',
		userTypeId: 0,
		userId: 0,
		pos_company_cards_transaction: [],
		pos_sim_card_transaction: [],
		pos_vm_transaction: []
	});


	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const nf = new Intl.NumberFormat();

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	
	const showDetails = e => {
		axios.get(`http://localhost:8000/getuserbyphone/${state.phoneNumberField}`,USER_TOKEN).then(res => {
			if(res.data == "Token Expired" || res.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			if (res.data.userresult[0].user_type_id === 3) {


					setState({
						...state,
						disabled: false,
						hiddenExcelButton: false,
						name: res.data.tableresult[0].userName,
						region: res.data.tableresult[0].region_arabic_name,
						area: res.data.tableresult[0].area_arabic_name,
						phoneNumber: res.data.tableresult[0].pos_phone_number,
						balance: res.data.tableresult[0].pos_vm_balance,
						rank: res.data.tableresult[0].pos_rank,
						commercialName: res.data.tableresult[0].pos_commercial_name,
						userTypeIdInTable: res.data.userresult[0].user_type_id,
						userIdInTable: res.data.userresult[0].user_id,
						type: "نقطة البيع",
					})
				// }).catch(error => {
				// 	if (error.response.status === 429) {
				// 		toast.error(error.response.data, {
				// 		   position: "top-center",
				// 		   autoClose: 4000,
				// 		   hideProgressBar: false,
				// 		   closeOnClick: true,
				// 		   pauseOnHover: true,
				// 		   draggable: true
				// 		}); 
				// 	}	
				// });
			}
			else if (res.data.userresult[0].user_type_id != 3) {
				toast.error(<IntlMessages id="toast.NotPosNumber" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					disabled: false,
					hiddenExcelButton: true,
					name: '',
					region: '',
					area: '',
					phoneNumber: '',
					balance: '',
					userIdInTable: '',
					userTypeIdInTable: '',
					rank: '',
					commercialName: '',
					type: '',
					posSoldCardsCount: '',
					pos_company_cards_transaction: [],
					pos_sim_card_transaction: [],
					pos_vm_transaction: []
				});
			}
		}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			} else {
				toast.error(<IntlMessages id="form.phoneNumberDoesn'tExist" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
			}
			
		// console.log('Error fetching and parsing data', error);
	});	
	}

	
	// let totalBalancesToBePrinted = 0;
	let selectedRowsWithData = [];							
	let bundle_number = [];
	let company_name_ar = [];
	let category_value = [];
	let company_cards_serial = [];
	let pin_number = [];
	let cards_pos_sell_price = [];
	let admin_sell_price = [];
	let cards_pos_profit = [];
	let rm_cards_receipt = [];
	let creation_date = [];
	let pos_discount_amount = [];
	let region_discount_amount = [];


	const handlemultiSelect = (selectedRows,displayData) => {
		
		selectedRowsWithData = [];
		bundle_number = [];
		company_name_ar = [];
		category_value = [];
		company_cards_serial = [];
		pin_number = [];
		cards_pos_sell_price = [];
		admin_sell_price = [];
		cards_pos_profit = [];
		rm_cards_receipt = [];
		creation_date = [];
		pos_discount_amount = [];
		region_discount_amount = [];
		// console.log("selectedRowsArray",selectedRows);
		selectedRows.data.forEach(element => {
			bundle_number.push(displayData[element.index].data[1]);
			company_name_ar.push(displayData[element.index].data[2]);
			category_value.push(displayData[element.index].data[3]);
			company_cards_serial.push(displayData[element.index].data[4]);
			pin_number.push(displayData[element.index].data[5]);
			cards_pos_sell_price.push(displayData[element.index].data[6]);
			admin_sell_price.push(displayData[element.index].data[7]);
			cards_pos_profit.push(displayData[element.index].data[8]);
			rm_cards_receipt.push(displayData[element.index].data[9]);
			creation_date.push(displayData[element.index].data[10]);
			pos_discount_amount.push(displayData[element.index].data[11]);
			region_discount_amount.push(displayData[element.index].data[12]);
			selectedRowsWithData.push({
				"Bundle Id": displayData[element.index].data[1],
				"Card Type": displayData[element.index].data[2],
				"Category": displayData[element.index].data[3],
				"Serials": displayData[element.index].data[4],
				"PINs": displayData[element.index].data[5],
				"Sell Price Specified By Point of sale": parseLocaleNumber(displayData[element.index].data[6]),
				"Sell Price Specified By Admin": parseLocaleNumber(displayData[element.index].data[7]),
				"Net Profit": displayData[element.index].data[8],
				"Invoice Number": displayData[element.index].data[9],
				"Data And Time": displayData[element.index].data[10],
				"Point of sale Discount Or Extra Fees Amount": parseLocaleNumber(displayData[element.index].data[11]),
				"Region Discount Or Extra Fees Amount": parseLocaleNumber(displayData[element.index].data[12]),
			})
		});

		return(
			<React.Fragment>
				<div className="form-group col-md-12">
					<br />
					<div className="container">
						{(state.hiddenExcelButton === false) ? 
						<React.Fragment>
							<div className="row justify-content-center">
								<button className="btn btn-primary ml-5 mr-5" onClick={(e) => exportToCSV()}>
									<IntlMessages id="form.exportExcel" />
								</button>
							</div>
						</React.Fragment>
						: 
						<React.Fragment></React.Fragment>
						} 
					</div>
				</div>
			</React.Fragment>
		)
	}

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');
	
		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		// console.log("hhhhh");
		const ws = XLSX.utils.json_to_sheet(selectedRowsWithData);

		const wb = { Sheets: { 'معاملات كارتات الشركات': ws }, SheetNames: ['معاملات كارتات الشركات'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], {type: fileType});

		FileSaver.saveAs(data, 'معاملات كارتات الشركات' + fileExtension);
	}

	const handleSearch = () => {
		// axios.post('http://localhost:8000/admin/getpostransactions', 
				// { 'user_type_id': res.data.userresult[0].user_type_id,
				//   'user_id': res.data.userresult[0].user_id 
				// },USER_TOKEN).then(res2 => {
				// 	// console.log(res2.data);

				// 	for (let i = 0; i < res2.data.pos_vm_transaction.length; i ++) {
				// 		res2.data.pos_vm_transaction[i].virtual_mony_transaction_value = nf.format(res2.data.pos_vm_transaction[i].virtual_mony_transaction_value);
				// 		res2.data.pos_vm_transaction[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.pos_vm_transaction[i].virtual_money_transaction_transfer_amount);
				// 	}
		axios.post('http://localhost:8000/admin/getpostransactions', 
		{ 	
			'from_date': state.from_date,
			'to_date': state.to_date,
			'user_type_id': state.userTypeIdInTable,
			'user_id': state.userIdInTable 
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				for (let i = 0; i < res2.data.pos_vm_transaction.length; i ++) {
					res2.data.pos_vm_transaction[i].virtual_mony_transaction_value = nf.format(res2.data.pos_vm_transaction[i].virtual_mony_transaction_value);
					res2.data.pos_vm_transaction[i].virtual_money_transaction_transfer_amount = nf.format(res2.data.pos_vm_transaction[i].virtual_money_transaction_transfer_amount);
				}
				if( res2.data.pos_vm_transaction.length == 0 && res2.data.pos_company_cards_transaction.length == 0 ) {
					toast.error(<IntlMessages id="components.NoItemFound" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						posSoldCardsCount: '',
						pos_company_cards_transaction: [],
						pos_sim_card_transaction: [],
						pos_vm_transaction: []
					})
				}
				else {
					setState({
						...state,
						pos_company_cards_transaction: res2.data.pos_company_cards_transaction,
						pos_sim_card_transaction: res2.data.pos_sim_card_transaction,
						pos_vm_transaction: res2.data.pos_vm_transaction,
						posSoldCardsCount: res2.data.posSoldCardsCount
					})
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
		});		
	}
	
	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: true,
		// selectableRows: true,
		customToolbarSelect: handlemultiSelect ,
		sort: false,
		download: false,
		// print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "600px"
	};

	const options2 = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		// print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;
	// console.log("selectedRowsWithData", selectedRowsWithData);
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
			<Helmet>
				<title>معاملات نقاط البيع</title>
				<meta name="description" content="معاملات نقاط البيع" />
  			</Helmet>
			<PageTitleBar title={<IntlMessages id="sidebar.pricing" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.pricing" />}>
					<div className="form-row">
						<div className="form-group col-md-12">
							<div className="row">
								<Form inline>
									<FormGroup className="mr-10 mb-10">
										<Label className="form-control mr-2 ml-2"><IntlMessages id="form.phoneNumber" /></Label>
										<input type="tel" className="form-control mr-2 ml-2" name="phoneNumberField"
											onChange={handleFields} 
											ref={register({ required: true, pattern: /^[0-9]+$/, minLength: 11, maxLength: 11, })}/>
									</FormGroup>
							
									<Button className="mb-10" color="primary" onClick={showDetails}><IntlMessages id="form.showDetails" /></Button>
								</Form>
							</div>
								<span className="errors">
									{errors.phoneNumberField && errors.phoneNumberField.type === 'required' &&
										<IntlMessages id="form.requiredError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'pattern' &&
										<IntlMessages id="form.numbersOnlyErrorError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'minLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
									{errors.phoneNumberField && errors.phoneNumberField.type === 'maxLength' &&
										<IntlMessages id="form.minPhoneLengthError" /> }
								</span>
							
							<br />
							{(state.name === '') ? <div></div>:
								<React.Fragment>
									<div className="container">
										<div className="row">
											<div className="col-md-6 reportsCardsNumberCenter">
												<h1>{state.type}</h1>
											</div>
											<div className="col-md-6 reportsCardsNumberCenter">
												<h1><IntlMessages id="form.virtualMoneyBalance" />: {state.balance}</h1>
											</div>
										</div>
									</div>
									<br /> 
									<div className="container">
										<div className="row">
											<div className="col-md-5">
												<h2><IntlMessages id="form.name" />: {state.name}</h2>
												<h2><IntlMessages id="form.phoneNumber" />: {state.phoneNumber}</h2>
											</div>
											<div className="col-md-3">
												<h2><IntlMessages id="form.region" />: {state.region}</h2>
												<h2><IntlMessages id="form.area" />: {state.area}</h2>	
											</div>
											<div className="col-md-4">
												{(state.rank === '') ? <div></div>: 
													<React.Fragment>
														<h2><IntlMessages id="form.rank" />: {state.rank}</h2>
														<h2><IntlMessages id="form.commercialName" />: {state.commercialName}</h2>
													</React.Fragment>
												}
														</div>
										</div>
									</div> 								
								</React.Fragment>
							}
						</div>
					</div>

					<div>
					<form onSubmit={handleSubmit(handleSearch)}>
						<div className="row">
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<h3><IntlMessages id="form.dateFrom" /></h3>
								<input name="from_date" placeholder="date placeholder" 
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.from_date && errors.from_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<span><IntlMessages id="form.dateTo" /></span>
								<input name="to_date" placeholder="date placeholder" 
									type="date" className="form-control" onChange={handleFields}
									ref={register({ required: true })} />
								<span className="errors m-2">
									{errors.to_date && errors.to_date.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<br />
							<div className="col-sm-4 col-lg-4 col-xl-4">
								<div className="container">
									<button color="primary" className="btn btn-primary" style={{ marginTop: "24px"}}>
										ابحث
									</button>
								</div>
							</div>
						</div>
					</form>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfCompaniesCards" /></h1>
							<h2>عدد الكارتات المباعة: <span style={{ marginRight: "30px" }}>{state.posSoldCardsCount}</span></h2>
							<br />
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								// title={state.posSoldCardsCount}
								data={state.pos_company_cards_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.bundleId" />,
										name:  "bundle_number" 
									},
									{
										label: <IntlMessages id="form.cardType" />,
										name:  "company_name_ar",
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name:  "category_text"
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "company_cards_serial",
									},
									{
										label: "PIN",
										name:  "pin_number",
									},
									{
										label: <IntlMessages id="form.cardsSellPricePOS" />,
										name:  "cards_pos_sell_price"
									},
									{
										label: <IntlMessages id="form.cardsSellPriceAdmin" />,
										name:  "admin_sell_price",
									},
									{
										label: <IntlMessages id="widgets.netProfit" />,
										name:  "cards_pos_profit"
									},
									{
										label: <IntlMessages id="form.invoiceNumber" />,
										name:  "rm_cards_receipt"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									{
										label: <IntlMessages id="form.discountAmountPOS" />,
										name:  "pos_discount_amount"
									},
									{
										label: <IntlMessages id="form.regionDiscountAmount" />,
										name:  "region_discount_amount"
									},
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							<h1 className="mb-3"><IntlMessages id="form.transactionsOfVM" /></h1>
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.pos_vm_transaction}
								columns={[
									{
										label: <IntlMessages id="form.serialNumber" />,
											name:  "virtual_mony_transaction_id",
											options: {
												display: "none",
												filter: false
											}
									},
									{
										label: <IntlMessages id="form.serialNumber" />,
										name:  "virtual_mony_transaction_serial_number",
									},
									{
										label: <IntlMessages id="form.balanceValue" />,
										name:  "virtual_mony_transaction_value"
									},
									{
										label: <IntlMessages id="form.transferredBalanceValue" />,
										name:  "virtual_money_transaction_transfer_amount",
									},
									{
										label: <IntlMessages id="form.transferFrom" />,
										name:  "from_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferTo" />,
										name:  "to_user_type_name"
									},
									{
										label: <IntlMessages id="form.transferFromName" />,
										name:  "from_userName"
									},
									{
										label: <IntlMessages id="form.transferToName" />,
										name:  "to_userName"
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "creation_date",
									},
									// {
									// 	label: <IntlMessages id="form.fees" />,
									// 	name:  "virtual_mony_transaction_extra_fees_id",
									// },
								]}
								options={options2}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
	)
	}