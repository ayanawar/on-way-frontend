/**
 * Faqs Component
 */
import React, { Component } from 'react';
import { Card, CardTitle, CardText, CardColumns, CardBody } from 'reactstrap';

// api
import api from 'Api';

// intl messages
import IntlMessages from 'Util/IntlMessages';

class Faqs extends Component {

    state = {
        faqs: null
    }

    componentDidMount() {
        this.getFaqs();
    }

    // get faqs
    getFaqs() {
        api.get('faqs.js')
            .then((response) => {
                this.setState({ faqs: response.data });
            })
            .catch(error => {
                // error handling
            })
    }

    render() {
        const { faqs } = this.state;
        return (
            <div>
            </div>
        );
    }
}

export default Faqs;
