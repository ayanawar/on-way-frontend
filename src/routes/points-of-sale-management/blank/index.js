/**
 * Blank Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';

export default class Blank extends Component {
	render() {
		
		return (
			<React.Fragment>
    		{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
			<div className="blank-wrapper">
				<Helmet>
					<title>Blank Page</title>
					<meta name="description" content="Reactify Blank Page" />
				</Helmet>
				<PageTitleBar title={<IntlMessages id="sidebar.blank" />} match={this.props.match} />
			</div>
			: (
				this.props.history.push("/access-denied")
			   )
			} 
			</React.Fragment>
		);
	}
}
