/**
* Report Page
*/
import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MUIDataTable from "mui-datatables";
import { RctCard, RctCardContent } from 'Components/RctCard';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import { Helmet } from "react-helmet";


import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import Cookies from 'universal-cookie';

const cookies = new Cookies(); 

export default function PosDiscount (props) {
	const history = useHistory();
   	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		categories:[],
		pos_discount_points: [],
		regions: [],
		user_phonenumber:'',
		created_by:'',
		pos_id_in_user:'',
		cards_category_id:'',
		discount_amount:'',
		companies:[],
		tableresult:[],
		userresult:[],
		type:'',
		pos_first_name:'',
		pos_middle_name:'',
		pos_last_name:'',
		region_arabic_name: '',
		area_arabic_name:'',
		region_id: '',
		disabled: false,
		hiddenUpdateForm: true,
		pos_vm_balance: ''
   });
   
   const [state2,setState2] = useState({
      companyId:'',
   })

   	useEffect(() => {
		axios.get('http://localhost:8000/admin/getallregionfees',USER_TOKEN).then(response1 => {
			axios.get('http://localhost:8000/allcompanies',USER_TOKEN).then(response2 => {
				axios.get('http://localhost:8000/allregions',USER_TOKEN).then(response3 => {
					if(response2.data == "Token Expired" || response2.data == "Token UnAuthorized" || response1.data == "Token Expired" || response1.data == "Token UnAuthorized"  || response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else {
						response2.data.companies.splice(0, 1);
						response3.data.regions.splice(0, 1);
						setState({
						...state,
							pos_discount_points: response1.data.message,
							companies: response2.data.companies,
							created_by: localStorage.getItem("userIdInUsers"),
							regions: response3.data.regions, 
							disabled: false,
							hiddenUpdateForm: true,
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						}); 
					}
				});
			}).catch(error => {
				if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});	
	}, []); 

	const handleFields = ({ target }) => {
			setState({ ...state, [target.name]: target.value });
	};

	const handleField2 = ({ target }) => {
			setState2({ ...state2, [target.name]: target.value });
		};
	const onClearClicked = () => {
		setState({
			...state,
			discount_amount: '', 
			hiddenUpdateForm: true,
			regionDiscountID: '',
			regionName: '',
			company_name: '',
			category: '',
			discount_amount: '',
		})
   	}

   	const onSubmit = e => {
		axios({
				method: 'post', url: 'http://localhost:8000/admin/createregiondiscount', data:{
               "created_by":state.created_by,
            	// "pos_id_in_user":state.pos_id_in_user,
               "company_card_category_id":state.cards_category_id,
               "company_id":state2.companyId,
			   "discount_amount":state.discount_amount,
			   "region_id": state.region_id,
            	// "type":state.type
            }, headers: { "x-access-token": `${cookies.get('UserToken')}`}
		}).then(res =>{
			toast.success(<IntlMessages id="widgets.AddDiscount" />, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			setState({ ...state, disabled: true })
			setTimeout(function () { location.reload()}, 3000)
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				}); 
			}
		});
   	}
   
   	const getAllCategories = (e)=>{
      	axios.get(`http://localhost:8000/allcategories/${e.target.value}`,USER_TOKEN)
      	.then(response => {
         	setState({ ...state, categories: response.data.message,});
      	}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
				}); 
			}
      	});
   	}

   	const handleTwochangeEvent = e => {
      	handleField2(e);
      	getAllCategories(e);
   	}
   	const onSubmit3 = e => {
		axios({
	   		method: 'post', url: 'http://localhost:8000/updatediscount', data:{
			// userTypeId: req.body.userTypeId,
			// posDiscountId: req.body.posDiscountId,
			// discountAmount: req.body.discountAmount
			"posDiscountId":state.regionDiscountID,
			"userTypeId": localStorage.getItem("user_type_id"),
			"discountAmount":state.discount_amount,
	   		},headers: { "x-access-token": `${cookies.get('UserToken')}`}
	   	}).then(res =>{
			toast.success("تم التعديل بنجاح", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
		  	setState({ ...state, disabled: true })
		  	setTimeout(function () { location.reload()}, 3000)
	   	}).catch(error => {
		if (error.response.status === 429) {

			toast.error(error.response.data, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});

		} else if (error.response.status === 303) {

			toast.error("هذه الشركة والفئة موجودين من قبل من فضلك قم بالتعديل", {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			}); 

		  	}
	   });
 	}

   	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				}         
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
   	})
   
   	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 10,
		rowsPerPageOptions: [5,10,25,50,100],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRows: "none",
		viewColumns: false,
		sort: false,
		download: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "345px"
	};

	const { match } = props;

	// console.log(state);

   
    return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
         <div className="report-wrapper">
            <ToastContainer />
			<Helmet>
				<title>خصومات و زيادات المحافظات</title>
				<meta name="description" content="خصومات و زيادات المحافظات" />
  			</Helmet>
            <PageTitleBar title={<IntlMessages id="sidebar.regionsDiscount" />} match={match} />
            <div className="row">
			{(state.hiddenUpdateForm == true) ? 
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.regionsDiscount" />}>
					<form onSubmit={handleSubmit(onSubmit)}>
						{/* <h3 className="text-center"><IntlMessages id="sidebar.basicDescription" /></h3> */}
						<div className="form-row">
							<div className="form-group col-md-4">
								<label><IntlMessages id="table.discount_amount"/></label>
								<br></br>
								<input type="number" className="form-control" name="discount_amount" placeholder="د.ع"
									onChange={handleFields} 
									ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
								<span className="errors">
								{errors.discount_amount && errors.discount_amount.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.discount_amount && errors.discount_amount.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
								</span>
							</div>
						</div>
						<div className="form-row">
							<div className="form-group col-md-4">
								<label><IntlMessages id="form.region" /> </label>
								<select name="region_id" className="form-control" 
										onChange={handleFields} ref={register({ required: true })}>
										<option key="" value="">برجاء الاختيار</option>
								{state.regions.map(region => {
									return (
										<option key={region.region_id} value={region.region_id}>
											{region.region_arabic_name}
										</option>
									)
								})
								}
								</select>
								<span className="errors">
									{errors.region_id && errors.region_id.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="form-group col-md-4">
								<label><IntlMessages id="widgets.companyName" /></label>
								<select name="companyId" className="form-control" 
								onChange={handleTwochangeEvent} ref={register({ required: true })}>
									<option key="0" value="">برجاء اختيار الشركة</option>
									{state.companies.map(company => {
										return (
											<option key={company.company_id} value={company.company_id}>
												{company.company_name_ar}
											</option>
										)
									})
								}
								</select>
								<span className="errors">
									{errors.companyId && errors.companyId.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>
							</div>
							<div className="form-group col-md-4">
								<label><IntlMessages id="widgets.category" /></label>
								<select name="cards_category_id" className="form-control" 
								onChange={handleFields} ref={register({ required: true })}>
									<option key="0" value="">برجاء اختيار الفئة</option>
									{/* { console.log(state,state2)} */}
									{state.categories.map((category) => {
										return (
											<option key={category.cards_category_id} value={category.cards_category_id}>
											{category.category_text}
											</option>
										)
									})
									}
								</select>
								<span className="errors">
									{errors.cards_category_id && errors.cards_category_id.type === 'required' &&
										<IntlMessages id="form.requiredOptionError" /> }
								</span>	
							</div>	
						</div>
						{/* <div className="form-row"> */}
						
						{/* </div> */}
						{(state.disabled === false) ? 
							<button type="submit" className="btn btn-primary">
								<IntlMessages id="form.create" />
							</button> : 
							<button type="submit" className="btn btn-primary" disabled={true}>
							<IntlMessages id="form.create" />
							</button>
						}
					<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
						<IntlMessages id="form.clear" />
					</button>
					</form>
				</RctCollapsibleCard>
				:
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12"
					heading={<IntlMessages id="sidebar.regionsDiscount" />}>
					<form onSubmit={handleSubmit(onSubmit3)}>
						<div className="form-row m-1">
							<label className="mr-4">ستقوم بتعديل خصومات و زيادات محافظة: <span className="reportsCardsNumberCenter">{state.regionName}</span></label>
							<br />
							<label className="mr-4">الشركة: <span className="reportsCardsNumberCenter">{state.company_name}</span></label>
							<label className="mr-4">الفئة: <span className="reportsCardsNumberCenter">{state.category}</span></label>
							<br />
							<br />
						</div>
					
						<div className="form-row">
							<div className="form-group col-md-4">
								<label><IntlMessages id="table.discount_amount"/></label>
								<br></br>
								<input type="number" className="form-control" name="discount_amount" placeholder="د.ع"
									value={state.discount_amount}
									onChange={handleFields} 
									ref={register({ required: true, pattern: /^[+-]?([0-9]*[.])?[0-9]+$/, minLength: 1 })}/>
								<span className="errors">
								{errors.discount_amount && errors.discount_amount.type === 'required' &&
									<IntlMessages id="form.requiredError" /> }
								{errors.discount_amount && errors.discount_amount.type === 'pattern' &&
									<IntlMessages id="form.numbersOnlyErrorError" /> }
								</span>
							</div>
						</div>

						{(state.disabled === false) ? 
							<button type="submit" className="btn btn-primary">
								<IntlMessages id="form.update" />
							</button> : 
							<button type="submit" className="btn btn-primary" disabled={true}>
							<IntlMessages id="form.update" />
							</button>
						}
					<button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearClicked}>
						<IntlMessages id="form.clear" />
					</button>
					</form>
				</RctCollapsibleCard>
			}
            </div>
            {/* /////////////////////////////////////////////////////////////////////////////////////////////// */}
           
			<div className="row mb-5">
				<RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
					<RctCard className="mb-4">
						<RctCardContent noPadding>
							<MuiThemeProvider theme={getMuiTheme()}>
								<MUIDataTable 
									data={state.pos_discount_points}
									columns={[
										{
											label: <IntlMessages id="table.pos_discount_id" />,
											name:  "pos_discount_id",
										},
										{
											label: <IntlMessages id="form.region" />,
											name:  "region_arabic_name",
										},
										{
											label: <IntlMessages id="form.companyArabicName"/>,
											name:  "company_name_ar" 
										},
										{
											label: <IntlMessages id="widgets.category" />,
											name:  "category_text",
										},
										{
											label: <IntlMessages id="table.discount_amount"/>,
											name:  "discount_amount_fr" 
										},
										{
											label: <IntlMessages id="table.type"/>,
											name:  "type" 
										},                           
										{
											label: <IntlMessages id="form.dataTime"/>,
											name:  "creation_date" 
										},
										{
											label: "discount_amount",
											name:  "discount_amount",
											options: {
											display: "none",
											filter: false,
											print: false,
											}
										},
										{
											label: <IntlMessages id="form.update" />,
											name: "",
											options: {
											filter: true,
											sort: false,
											empty: true,
											customBodyRender: (value, tableMeta, updateValue) => {
												return (
													<React.Fragment>
														<button type="button" className="btn btn-primary" 
															onClick={() => {
														// console.log(tableMeta.rowData[0])
																setState({
																	...state,
																	hiddenUpdateForm: false,
																	regionDiscountID: tableMeta.rowData[0],
																	regionName: tableMeta.rowData[1],
																	company_name: tableMeta.rowData[2],
																	category: tableMeta.rowData[3],
																	discount_amount: tableMeta.rowData[7],
																});  
															}}>
														<IntlMessages id="form.update" />
														</button>
													</React.Fragment>
												);
											}
											}
					
										},
									]}
								options={options}  
								/>
							</MuiThemeProvider>
						</RctCardContent>
					</RctCard>
				</RctCollapsibleCard>
			</div>
        </div>
		: (
			
			history.push("/access-denied")
	   	)
		} 
	   	</React.Fragment>
      );
   
}
