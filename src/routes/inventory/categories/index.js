import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RctCard, RctCardContent } from 'Components/RctCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import MUIDataTable from "mui-datatables";
import Modal from 'react-awesome-modal';
const cookies = new Cookies();
const Token = cookies.get('UserToken');

export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        categories:[],
        disabled: false,
        hiddenStatusForm: true,
        showModalForConfirmation: false,
        categoryNameAr:'',
        categoryNameEn:'',
        categoryID:'',
        updateflag:true
    });


    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
    };

    useEffect(() => {
    // all suppliers
    axios.get('https://accbackend.alaimtidad-itland.com/categories',USER_TOKEN).then(response => {
        if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
            cookies.remove('UserToken', {path:'/'})
            window.location.href = "/signin";	
        }
        else{
    
            setState({
                ...state,
                categories: response.data.categories,
                
            })
        }
    })
    .catch(error => {

        if (error.response.status === 429) {
            toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
        }
    });
}, []);

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const getMuiTheme = () => createMuiTheme({
    overrides: {
        MUIDataTable: {
            responsiveScroll: {
                maxHeight: 'unset',
                overflowX: 'unset',
                overflowY: 'unset',
            },
        },
        MuiTableCell: {
            head: {
                color: "#599A5F",
                fontWeight: "bold",
                fontSize: "15px",
                fontFamily: "'Almarai', sans-serif"
            }, 
            body: {
                color: "#092346",
                fontWeight: "bold",
                fontSize: "15px",
                fontFamily: "'Almarai', sans-serif",
            }       
        },
        MUIDataTableHeadCell: {
            data: {
                color: "#599A5F",
                fontWeight: "bold",
                fontFamily: "'Almarai', sans-serif"
            },
            fixedHeader: {
                position: "sticky !important",
                zIndex: '100',
            }
        },
        MUIDataTableSelectCell: {
            headerCell: {
                zIndex: 1
            },
            fixedLeft: {
                zIndex: 1
            }
        },
        MUIDataTableToolbarSelect: {
            root: {
                color: "#599A5F",
                fontWeight: "bold",
                zIndex: 1,
            }         
        },
        MuiPaper: {
            root: {
                color: "#092346",
                fontWeight: "bold",
                fontFamily: "'Almarai', sans-serif"
            }
        },
        MuiToolbar:{
            regular: {
                backgroundColor:"gray"
            },
            root: {
                top: 0,
                position: 'sticky',
                background: 'white',
                zIndex: '100',
            },
        },
        MUIDataTablePagination: {
            tableCellContainer: {
                backgroundColor:"gray"
            }
        },
        MUIDataTableBody: {
            emptyTitle: {
                display: "none",
            }
        }
    }
})
const onSubmitUPDATE = () => {
    axios({
        url:`https://accbackend.alaimtidad-itland.com/categories/${state.categoryID}`,
        method:'put',
        data:{
            "category_name":state.categoryNameAr,
            "category_name_en":state.categoryNameEn, 
        },
        headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            } else{
                toast.success(<IntlMessages id="updatedsuccess" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }      
    }).catch(error => {
        if (error.response.status === 406) {
            toast.error("هذا الاسم مسجل من قبل  من فضلك قم بالتعديل أو أضف اسم جديد", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
        } else if (error.response.status === 429) {
            toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
        } else if (error.response.status === 405) {
            toast.error("عفواَ لا يمكنك إكمال العملية لأنه يوجد بيانات متعلقة بهذا البيان فلا يمكنك حذفه أو التعديل علية الإ بمسح البيانات المتعلقة بيه أولا", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });  
        }
    });
}

const onClearStatusClicked = () => {
    setState({
        ...state,
        updateflag:true,
        disabled: false,
        hiddenStatusForm: true,
        hiddenDebitLimitForm: true,
        categoryNameAr:'',
        categoryNameEn:'',
        categoryID:'',    
    })
};

const handleConfirmation = () => {
    axios({
        url:`https://accbackend.alaimtidad-itland.com/categories/${state.categoryID}`,
        method:'delete',
        headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            } else{
                toast.success("تم الحذف بنجاح", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                setState({ ...state, disabled: true, showModalForConfirmation: false, })
                setTimeout(function () { location.reload()}, 3000)
            }      
    }).catch(error => {
        if (error.response.status === 406) {
            toast.error("هذا الاسم مسجل من قبل من فضلك قم بالتعديل أو أضف اسم جديد", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
            setState({ ...state, showModalForConfirmation: false, })
        } else if (error.response.status === 429) {
            toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
            setState({ ...state, showModalForConfirmation: false, }) 
        } else if (error.response.status === 405) {
            toast.error("عفواَ لا يمكنك إكمال العملية لأنه يوجد بيانات متعلقة بهذا البيان فلا يمكنك حذفه أو التعديل علية الإ بمسح البيانات المتعلقة بيه أولا", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });  
            setState({ ...state, showModalForConfirmation: false, })
        }
    });
}
const onSubmit = e => {
    axios({
        url:'https://accbackend.alaimtidad-itland.com/categories',
        method:'post',
        data:{
            "category_name":state.categoryNameAr,
            "category_name_en":state.categoryNameEn, 
        },
        headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
            if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                cookies.remove('UserToken', { path: '/' })
                window.location.href = "/signin";
            } else{
                toast.success(<IntlMessages id="addedsuccessfully" />, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            setState({ ...state, disabled: true })
            setTimeout(function () { location.reload()}, 3000)
        }   
    }).catch(error => { 
        if (error.response.status === 406) {
            toast.error(" هذا الاسم مسجل من قبل من فضلك قم بالتعديل أو أضف اسم جديد", {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
        } else if (error.response.status === 429) {
            toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            }); 
        } 
    });
    
}

const closeModel3 = () => {
    setState({
        ...state,
        showModalForConfirmation: false,
        categoryNameAr:'',
        categoryNameEn:'',
        categoryID:'',
    });
}

    const { match } = props;

    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="shop-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>الفئات</title>
                        <meta name="description" content="الفئات" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.categories" />} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.categories" />}>

                            {(state.updateflag == true) ?  
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>الفئة</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                            
                                                <input type="text" className="form-control" 
                                                        name="categoryNameAr" 
                                                        defaultValue={state.categoryNameAr}
                                                        onChange={handleFields}
                                                        ref={register({ required: true,
                                                                minLength: 3,
                                                                pattern: /^[ء-ي_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label>الفئة باللغة الإنجليزية</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="categoryNameEn" 
                                                        defaultValue={state.categoryNameEn}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                    minLength: 3, 
                                                                    pattern: /^[A-Za-z_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                    </div>
                                    {(state.disabled === false) ? 
                                        <button type="submit" className="btn mainButtonColor">
                                            <IntlMessages id="form.add" />
                                        </button> : 
                                        <button type="submit" className="btn btn-secondary" disabled={true}>
                                            <IntlMessages id="form.add" />
                                        </button>
                                    }
                                </form>
                                :
                                <form onSubmit={handleSubmit(onSubmitUPDATE)}>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>الفئة</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                            
                                                <input type="text" className="form-control" 
                                                        name="categoryNameAr" 
                                                        defaultValue={state.categoryNameAr}
                                                        onChange={handleFields}
                                                        ref={register({ required: true,
                                                                minLength: 3,
                                                                pattern: /^[ء-ي_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.categoryNameAr && errors.categoryNameAr.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label>الفئة باللغة الإنجليزية</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="categoryNameEn" 
                                                        defaultValue={state.categoryNameEn}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                    minLength: 3, 
                                                                    pattern: /^[A-Za-z_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.categoryNameEn && errors.categoryNameEn.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                    </div>

                                    {(state.disabled === false) ? 
                                        <button type="submit" className="btn btn-warning">
                                            <IntlMessages id="form.update" />
                                        </button> : 
                                        <button type="submit" className="btn btn-warning" disabled={true} >
                                            <IntlMessages id="form.update" />
                                        </button>
                                    }

                                    <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                                        <IntlMessages id="form.clear" />
                                    </button>
                                </form>
                            }
                        </RctCollapsibleCard>
                    </div>

                    {state.showModalForConfirmation == true ?
                        <Modal visible={state.showModalForConfirmation} width="550" height="150" effect="fadeInUp" 
                            onClickAway={closeModel3}>
                            <div className="modal-header">
                                <h3 className="modal-title"> 
                                    <span className="ml-3"> هل أنت متأكد من حذف هذا البيان؟</span>
                                </h3> 
                                {/* <br /> */}
                                
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-primary"
                                        onClick={handleConfirmation}>
                                    موافق
                                </button>                             
                                
                                <button type="button" className="btn btn-secondary" 
                                        onClick={closeModel3}>
                                    إغلاق
                                </button>
                            </div>
                        </Modal>
                    : 
                        null
                    }

                    <div className="row mb-5">
                        <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                            <RctCard>
                                <RctCardContent noPadding>
                                    <MuiThemeProvider theme={getMuiTheme()}>
                                        <MUIDataTable 
                                            // title={<IntlMessages id="sidebar.cart" />}
                                            data={state.categories}
                                            columns={[
                                                    {
                                                        label: "id",
                                                        name:  "category_id",
                                                        options: {
                                                            display: "none",
                                                            filter: false,
                                                            print: false,
                                                        }
                                                    },
                                                    {
                                                        label: "الفئة",
                                                        name:  "category_name",
                                                    },
                                                    {
                                                        label: "الفئة باللغة الإنجليزية",
                                                        name:  "category_name_en",
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.dataTime" />,
                                                        name:  "update_date_time"
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.update" />,
                                                        name: "",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button type="button" className="btn btn-warning" 
                                                                    onClick={() => {
                                                                    
                                                                        setState({
                                                                            ...state, 
                                                                            updateflag: false,
                                                                            categoryID:tableMeta.rowData[0],
                                                                            categoryNameAr:tableMeta.rowData[1],
                                                                            categoryNameEn:tableMeta.rowData[2],
                                                                        });
                                                                    }}>
                                                                    <IntlMessages id="form.update" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        }
                                                        }
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.delete" />,
                                                        name: "",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button type="button" className="btn btn-danger" 
                                                                    onClick={() => {
                                                                        setState({
                                                                            ...state, 
                                                                            updateflag: true,
                                                                            showModalForConfirmation: true,
                                                                            categoryID:tableMeta.rowData[0],
                                                                        });
                                                                    }}>
                                                                    <IntlMessages id="form.delete" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        }
                                                        }
                                                    },
                                                ]}
                                            options={options}
                                        />
                                    </MuiThemeProvider>
                                </RctCardContent>
                            </RctCard>
                        </RctCollapsibleCard>
                    </div>
                </div>
                : (

                history.push("/access-denied")
                )
            }
        </React.Fragment>        
    )
}