import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RctCard, RctCardContent } from 'Components/RctCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
const cookies = new Cookies();
const Token = cookies.get('UserToken');



export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        categories:[],
        disabled: false,
        hiddenStatusForm: true,
        showModalForConfirmation: false,
        productName:'',
        categoryNameEn:'',
        categoryID:'',
        productImage: '',
		imagePreviewUrl: '',
        description: '',
        updateflag:true
    });



    useEffect(() => {
        axios.get('https://accbackend.alaimtidad-itland.com/categories',USER_TOKEN).then(response => {
            if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                cookies.remove('UserToken', {path:'/'})
                window.location.href = "/signin";	
            }
            else{
        
                setState({
                    ...state,
                    categories: response.data.categories,
                    imagePreviewUrl: 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png'
                    
                })
            }
        })
        .catch(error => {

            if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            }
        });
    }, []);

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const maxSelectFile = (e) => {
		let files = e.target.files 
			if (files.length > 1) { 
			  	e.target.value = null 
			//   console.log("You can only upload one image");
				toast.error("You can only upload one image", {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				return false;
		  	}
		return true;
	  
	}

    const checkMimeType= (e) => {
		let files = e.target.files 
		let err = ''
		const types = ['image/png', 'image/jpeg', 'image/gif', 'image/jpg']
		for(var x = 0; x<files.length; x++) {
				if (types.every(type => files[x].type !== type)) {   
					err += files[x].type+' is not a supported format, The allowed extensions are png,jpeg,gif and jpg';
				}
			};
		
		if (err !== '') {  
			e.target.value = null 
			// console.log(err);
			toast.error(err, {
				position: "top-center",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true
			});
			return false; 
		}
		return true;
	}
	  
	const checkFileSize = (e) => {
		let files = e.target.files
		let size = 500000 
		let err = ""; 
		for(var x = 0; x<files.length; x++) {
			if (files[x].size > size) {
				err += files[x].type+' is too large, please pick a smaller file, Files size allowed up to 500 KB';
			}
	  };
	  if (err !== '') {
		e.target.value = null
		// console.log(err);
		toast.error(err, {
			position: "top-center",
			autoClose: 4000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true
		});
		return false
	  }
	  
	  return true;
	  
	}

    const imageHandler = e => {
		let reader = new FileReader();
    	let file = e.target.files[0];
		if(maxSelectFile(e) && checkMimeType(e) && checkFileSize(e)){ 
			reader.onloadend = () => {
			setState({ ...state,
				productImage: file,
				imagePreviewUrl: reader.result
			});
			}
			reader.readAsDataURL(file)
		} else {
			setState({ ...state, productImage: '', imagePreviewUrl: 'https://www.mountaingoatsoftware.com/uploads/blog/2016-09-06-what-is-a-product.png'
			});
		}
	};

    const onSubmit = e => {
        let data = new FormData();
        data.append('category_id', state.categoryID),
		data.append('product_name', state.productName),
		data.append('product_description', state.description),
		data.append('product_purchase_price', state.purchasePrice),
		data.append('product_sell_price', state.sellPrice),	
		data.append('product_image', state.productImage)

        axios({
            url:'https://accbackend.alaimtidad-itland.com/products',
            method:'post',
            data: data,
            headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                } else{
                    toast.success(<IntlMessages id="addedsuccessfully" />, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }   
        }).catch(error => { 
            if (error.response.status === 406) {
                toast.error("هذا الاسم مسجل من قبل", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } 
        });       
    }

    const { match } = props;

    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="shop-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>الفئات</title>
                        <meta name="description" content="الفئات" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.addProducts" />} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.addProducts" />}>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="form-row">
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="widgets.category" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                borderWidth: "0px 2px 2px 0px",
                                                lineHeight: "0px",
                                                borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>

                                            <select className="form-control input-text"
                                                name="categoryID"
                                                onChange={handleFields}
                                                ref={register({required: true,})}>
                                                <option key="0" value="">
                                                        برجاء اختيار الفئة 
                                                </option>
                                                    {state.categories.map(
                                                        (category) => {
                                                            return (
                                                                <option key={category.category_id}
                                                                        value={category.category_id}>
                                                                    {category.category_name}
                                                                </option>
                                                            );
                                                        }
                                                    )}
                                            </select>                            
                                        </div>
                                        <span className="errors">
                                            {errors.categoryID && errors.categoryID.type === 'required' &&
                                                <IntlMessages id="form.requiredOptionError" />}                                    
                                        </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.productName" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                    borderWidth: "0px 2px 2px 0px",
                                                    lineHeight: "0px",
                                                    borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                            <input type="text" className="form-control" 
                                                    name="productName" 
                                                    defaultValue={state.productName}
                                                    onChange={handleFields}
                                                    ref={register({ required: true,
                                                            minLength: 3,
                                                            pattern: /^[A-Za-zء-ي_ ]+$/i, })} />
                                        </div>
                                        <span className="errors">
                                            {errors.productName && errors.productName.type === 'required' &&
                                                <IntlMessages id="form.requiredError" />}
                                            {errors.productName && errors.productName.type === 'minLength' &&
                                                <IntlMessages id="form.minLengthError" />}
                                            {errors.productName && errors.productName.type === 'pattern' &&
                                                <IntlMessages id="form.lettersOnlyError" />}
                                        </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.purchasePrice" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                            <input type="number" className="form-control" name="purchasePrice"
                                                    onChange={handleFields} ref={register({ required: true })}/>                                                        
                                            
                                        </div>
                                            <span className="errors">
                                                {errors.purchasePrice && errors.purchasePrice.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                            </span>
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label><IntlMessages id="form.sellPrice" /></label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                        }}>
                                            <input type="number" className="form-control" name="sellPrice"
                                                    onChange={handleFields} ref={register({ required: true })}/>                                                           
                                        </div>
                                            <span className="errors">
                                                {errors.sellPrice && errors.sellPrice.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                            </span>
                                    </div>  
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label>وصف الصنف</label>
                                        <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                            borderWidth: "0px 2px 2px 0px",
                                            lineHeight: "0px",
                                            borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"
                                                            }}>
                                                                                                    
                                            <input type="text" className="form-control" name="description"
                                                onChange={handleFields} />
                                        </div>
                                        <span className="errors">
                                        
                                        </span>
                                    </div>
                                </div>
                                <div className="md-form md-outline">
                                    <label><IntlMessages id="form.image" /></label>
                                    <br />
                                    <input type="file" name="productImage" onChange={imageHandler} />
                                    <div className="img-holder">
                                        <img src={state.imagePreviewUrl} alt="" id="img" className="img" />
                                    </div>
                                    <br />
                                    
                                </div>
                                {(state.disabled === false) ? 
                                    <button type="submit" className="btn mainButtonColor">
                                        <IntlMessages id="form.add" />
                                    </button> : 
                                    <button type="submit" className="btn btn-secondary" disabled={true}>
                                        <IntlMessages id="form.add" />
                                    </button>
                                }
                            </form>
                        </RctCollapsibleCard>
                    </div>  
                </div>
                : (
                    history.push("/access-denied")
                )
            }
        </React.Fragment>        
    )
}