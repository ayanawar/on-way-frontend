/**
 * add supplier
 */
 import React, { useEffect, useState } from 'react';
 import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
 import axios from 'axios';
 import { useForm } from 'react-hook-form';
 import { ToastContainer, toast } from 'react-toastify';
 import 'react-toastify/dist/ReactToastify.css';
 
 // intl messages
 import IntlMessages from 'Util/IntlMessages';
 
 // page title bar
 import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
 import USER_TOKEN from '../../../constants/Token';
 import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
 import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
 import { useHistory } from 'react-router-dom';
 import Cookies from 'universal-cookie';
 import { Helmet } from "react-helmet";
 
 const cookies = new Cookies();
 const Token = cookies.get('UserToken');
 
 
 
 export default function Shop(props) {
     const history = useHistory();
     const { register, handleSubmit, errors } = useForm();
     const [state, setState] = useState({
        user_first_name: '',
		user_middle_name: '',
		user_last_name: '',
		level1:0,
        level2:0,
        level3:0,
        acc_name_ar: '',
		acc_name_en: '',
        acc_id:'',
        acc_desc: '',
		security_level: '',
		acc_type: '',
		acc_currency_id:'',
        accountStatusId:1,
         job_title: '',
         disabled: false,
         hiddenlvl2:true,
         hiddenlvl3:true,
         accountCurrencyIdupdate:'',
         accountTypeIdupdate: '',
         accountCurrencyIdupdateName:'',
         accountTypeIdupdateName: '',
         levcels2:[],
         levcels3:[],
         types: [],
         levels: [],
         currency: [],
         accounts: [],
         MainAccounts:[],
		user_region_id: '',
        basic_salary:'',
		user_area_id: '',
		user_password: '',
		user_phonenumber: '',
		accountnumber:'',
		accounttype:'',
		disabled: false,
		companies: [],
		regions: [],
		areas: [],
     });
 
 
     useEffect(() => {
        axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-main-accounts', USER_TOKEN).then(response11 => {
                axios.get('https://accbackend.alaimtidad-itland.com/all-security-levels', USER_TOKEN).then(response => {
                    axios.get('https://accbackend.alaimtidad-itland.com/gettype', USER_TOKEN).then(response3 => {
                       axios.get('https://accbackend.alaimtidad-itland.com/getcurrency', USER_TOKEN).then(response4 => {
                           axios.get('https://accbackend.alaimtidad-itland.com/all-accounts', USER_TOKEN).then(response5 => {
       
                 if (response8.data == "Token Expired" || response8.data == "Token UnAuthorized" || response.data == "Token Expired" || response.data == "Token UnAuthorized" || response3.data == "Token Expired" || response3.data == "Token UnAuthorized" || response4.data == "Token Expired" || response4.data == "Token UnAuthorized"|| response5.data == "Token Expired" || response5.data == "Token UnAuthorized" || response11.data == "Token Expired" || response11.data == "Token UnAuthorized" ) {

                     cookies.remove('UserToken', { path: '/' })
                     window.location.href = "/signin";
                 }
                 else {
                   
                     setState({
                         ...state,
                         regions: response8.data.regions,
                         levels: response.data.levels,
                         types: response3.data.message,
                         currency:response4.data.message,
                         accounts:response5.data.accounts,
                         MainAccounts:response11.data.mainAccounts,
                         disabled: false,
                     })
                 }
             }).catch(error => {
                 if (error.response8.status === 429) {
                     toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                     }); 
                 }
             });
            }).catch(error => {
                if (error.response11.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
            }).catch(error => {
                if (error.response3.status === 429) {
                    toast.error(error.response.data, {
                       position: "top-center",
                       autoClose: 4000,
                       hideProgressBar: false,
                       closeOnClick: true,
                       pauseOnHover: true,
                       draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response4.status === 429) {
                toast.error(error.response.data, {
                   position: "top-center",
                   autoClose: 4000,
                   hideProgressBar: false,
                   closeOnClick: true,
                   pauseOnHover: true,
                   draggable: true
                }); 
            }
        });
         }).catch(error => {
             if (error.response5.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }, []);
 
   
 
     const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};
	const handleTwoEvents2 = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
       
         let parentid = target.value;
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
        
                 if(response.data.accounts.length == 0){
                    
                     setState({
                         ...state,
                         level2: parentid,
                         level3:0,
                         levcels3: [],
                     })
 
                 }
                 
                 else if(response.data.accounts.length>0){

                    setState({
                        ...state,
                        level2: parseInt(parentid),
                        accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                       
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: [],
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:true,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                            ...state,
                            level2: parentid,
                            levcels3: response.data.accounts,
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
            
                 }
                 else{
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
         
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
     }
 
     const handleTwoEvents = ({ target }) => {
        for(let i=0; i<=state.MainAccounts.length;i++){
         
             
            if(parseInt(target.value) == parseInt(state.MainAccounts[i].acc_number)){
        
        setState({ ...state, [target.name]: target.value,
            accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
            accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
            accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
            accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
        });
  
         let parentid = target.value;
         
         axios.post('https://accbackend.alaimtidad-itland.com/all-accounts-by-parent-ID', { "accParentID": parentid }, USER_TOKEN).then(response => {
             if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                 cookies.remove('UserToken', { path: '/' })
                 window.location.href = "/signin";
             }
             else {
                 
                 if(response.data.accounts.length == 0){
                     
                
                     setState({
                         ...state,
                         level1: parentid,
                         levcels2:  [],
                         levcels3:  [],
                         level2:0,
                         level3:0,
                         hiddenlvl2:true,
                         accountCurrencyIdupdate:state.MainAccounts[i].currency_id,
                         accountTypeIdupdate:state.MainAccounts[i].audit_type_id,
                         accountCurrencyIdupdateName:state.MainAccounts[i].currency_ar,
                         accountTypeIdupdateName:state.MainAccounts[i].audit_type_ar,
                     })
                 }
                 
                 else if(response.data.accounts.length>0){
                    setState({
                        ...state,
                        level1: parseInt(parentid),
                
                        accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                        accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                        accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                        accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                       
                    })
          
                    if(parseInt(target.value) != parseInt(parentid)){
                        
                        setState({
                            ...state,
                            level1: parentid,
                            levcels2:  [],
                            levcels3:  [],
                            level2:0,
                            level3:0,
                            accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                            accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                            accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                            accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                            hiddenlvl2:false,
                        })
                      
                     }
                   
                    else if(parseInt(target.value) == parseInt(parentid)){
                      
                        setState({
                         ...state,
                         level1: parentid,
                         levcels2: response.data.accounts,
                         levcels3:  [],
                         level3:0,
                         accountCurrencyIdupdate:response.data.accounts[0].currency_id,
                         accountTypeIdupdate:response.data.accounts[0].audit_type_id,
                         accountCurrencyIdupdateName:response.data.accounts[0].currency_ar,
                         accountTypeIdupdateName: response.data.accounts[0].audit_type_ar,
                         hiddenlvl2:false,
                     })
                    }
                    
                 }
                 else{
                   
                     console.log("Errorrrrrrrr");
                 }
             }
         }).catch(error => {
 
             if (error.response.status === 429) {
                 toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                 }); 
             }
         });
         break;
        }
  
        else {
         //    console.log("n0oooo");
        }
    }
     }
 
	const handleTwoEvents3 = (e) => {
		handleFields(e);
		let regionId = e.target.value;

		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
						user_area_id: '',
						
					})

				}else{
					setState({
						...state,
						user_region_id: regionId,
						areas: response.data.areas,
					
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}
 
    
 
     const onSubmit = e => {
 
      	

        axios({url:'https://accbackend.alaimtidad-itland.com/addemployees',method:'post',data:{

			level1:state.level1,
            level2:state.level2,
            level3:state.level3,
            acc_name_ar:state.acc_name_ar,
            acc_name_en:state.acc_name_en,
            acc_desc:state.acc_desc,
            security_level:state.security_level,
            acc_type:parseInt(state.accountTypeIdupdate),
            acc_currency_id:parseInt(state.accountCurrencyIdupdate),
            user_first_name:state.user_first_name,
            user_middle_name:state.user_middle_name,
            user_last_name:state.user_last_name,
            supplier_company_name:state.supplier_company_name,
            user_region_id:parseInt(state.user_region_id),
            user_area_id:parseInt(state.user_area_id),
            employee_phone_number:state.user_phonenumber,
            basic_salary:state.basic_salary,
            job_title:state.job_title,
			
			},
             headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                }
                else{
				toast.success(<IntlMessages id="form.addEmployeeSuccess" />, {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({ ...state, disabled: true })
				setTimeout(function () { location.reload()}, 3000)

            }
			}).catch(error => {
		
				if (error.response.data.message) {
					toast.error(<IntlMessages id="form.phoneNumberIsAlreadyRegistered" />, {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
				} else if (error.response.status === 429) {
					toast.error(error.response.data, {
					   position: "top-center",
					   autoClose: 4000,
					   hideProgressBar: false,
					   closeOnClick: true,
					   pauseOnHover: true,
					   draggable: true
					}); 
				}
			});
     }
 
     const { match } = props;
     return (
         <React.Fragment>
             { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                 <div className="shop-wrapper">
                     <ToastContainer />
                     <Helmet>
                         <title>إضافة موظفين</title>
                         <meta name="description" content="إضافة موظفين" />
                     </Helmet>
                     <PageTitleBar title={<IntlMessages id="sidebar.addemployee" />} match={match} />
                    
                         
                 </div>
                 : (
 
                     history.push("/access-denied")
                 )
             }
         </React.Fragment>
     )
 }