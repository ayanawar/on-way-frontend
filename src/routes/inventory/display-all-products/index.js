import React, { useState, useEffect } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, FormGroup, Label, } from 'reactstrap';
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { Helmet } from "react-helmet";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import * as FileSaver from 'file-saver';

import * as XLSX from 'xlsx';
const cookies = new Cookies();


export default function Shop(props) {
	const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

	const fileExtension = '.xlsx';
	const history = useHistory();
	const { register, handleSubmit, errors } = useForm();
	const [state, setState] = useState({
		name: '',
		region: '',
   		area: '',
		phoneNumber: '',
		active_status: '',
		user_personal_image: '',
		imagePreviewUrl: '',
		phoneNumberField: '',
		balance: 0,
		type: '',
		checked: true,
		rank: '',
        commercialName: '',
        userIdInTable:'',
		userTypeIdInTable: '',
		bundleNumber: '',
		serial_number: '',
		pin_number: '',
		userTypeId: 0,
        userId: 0,
		companyId: '',
		categoryId: '',
        products: [],
        categories: [],
		pos_company_cards_transaction: [],
		realmoney_transaction: [],
		vm_transaction: [],
		hiddenExcelButton: true,
    });
    
    useEffect(() => {
		// 	axios.get('http://localhost:8000/allareas',USER_TOKEN).then(response2 => {
				axios.get('https://accbackend.alaimtidad-itland.com/products',USER_TOKEN).then(response3 => {
					if(response3.data == "Token Expired" || response3.data == "Token UnAuthorized") {
						cookies.remove('UserToken', {path:'/'})
						window.location.href = "/signin";	
					}
					else{
						
						setState({
                            ...state,
							products: response3.data.products,
							// imagePreviewUrl: 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png'
						})
					}
				}).catch(error => {
					if (error.response.status === 429) {
						toast.error(error.response.data, {
						   position: "top-center",
						   autoClose: 4000,
						   hideProgressBar: false,
						   closeOnClick: true,
						   pauseOnHover: true,
						   draggable: true
						});
					}
				});
		// 	}).catch(error => {
		// 		// console.log('Error fetching and parsing data', error);
		// 	});
		// }).catch(error => {
		//    	// console.log('Error fetching and parsing data', error);
		// });
	}, []); 
	//const nf = new Intl.NumberFormat();

	const handleFields = ({ target }) => {
		setState({ ...state, [target.name]: target.value });
	};

	const getMuiTheme = () => createMuiTheme({
		overrides: {
			MUIDataTable: {
				responsiveScroll: {
				  maxHeight: 'unset',
				  overflowX: 'unset',
				  overflowY: 'unset',
				},
			},
			MuiTableCell: {
				head: {
					color: "#599A5F",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif"
				}, 
				body: {
					color: "#092346",
					fontWeight: "bold",
					fontSize: "15px",
					fontFamily: "'Almarai', sans-serif",
				}       
			},
			MUIDataTableHeadCell: {
				data: {
					color: "#599A5F",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				},
				fixedHeader: {
					position: "sticky !important",
					zIndex: '100',
				}
			},
			MUIDataTableSelectCell: {
				headerCell: {
					zIndex: 1
				},
				fixedLeft: {
					zIndex: 1
				}
			},
			MuiToolbar:{
				regular: {
					backgroundColor:"gray"
				},
				root: {
					top: 0,
					position: 'sticky',
					background: 'white',
					zIndex: '100',
				},
			},
			MUIDataTablePagination: {
				tableCellContainer: {
					backgroundColor:"gray"
				}
			},
			MUIDataTableToolbarSelect: {
				root: {
					color: "#599A5F",
					fontWeight: "bold",
					zIndex: 1,
				},
				title: {
					display: "none"
				}        
			},
			MuiPaper: {
				root: {
					color: "#092346",
					fontWeight: "bold",
					fontFamily: "'Almarai', sans-serif"
				}
			},
			MUIDataTableBody: {
				emptyTitle: {
					display: "none",
				}
			}
		}
	})
	
	const showDetails = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	'user_type_id': localStorage.getItem("user_type_id"),
			'user_id': localStorage.getItem("userIdInUsers"),
			'serial_number': state.serial_number
		},USER_TOKEN).then(res2 => {
			// console.log(res2.data);
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
				if(res2.data.vm_transaction.length == 0) {
					toast.error(<IntlMessages id="components.NoItemFound" /> , {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						vm_transaction: res2.data.vm_transaction,
						hiddenExcelButton: true,
					})
				} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
				}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}

	const showDetails2 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'bundleNumber': state.bundleNumber
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}

	const showDetails3 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'pin_number': state.pin_number
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
	}
	
	const showDetails4 = e => {	
		axios.post('http://localhost:8000/admin/getcompanycardstransactions', 
		{ 	
			'companyId': state.companyId,
			'categoryId': state.categoryId,
		},USER_TOKEN).then(res2 => {
			if(res2.data == "Token Expired" || res2.data == "Token UnAuthorized") {
				cookies.remove('UserToken', {path:'/'})
				window.location.href = "/signin";	
			}
			else {
			// console.log(res2.data);
			if(res2.data.vm_transaction.length == 0) {
				toast.error(<IntlMessages id="components.NoItemFound" /> , {
					position: "top-center",
					autoClose: 4000,
					hideProgressBar: false,
					closeOnClick: true,
					pauseOnHover: true,
					draggable: true
				});
				setState({
					...state,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: true,
				})
			} else {
				setState({
					...state,
					// realmoney_transaction: res2.data.realmoney_transaction,
					// vm_transaction: res2.data.vm_transaction,
					vm_transaction: res2.data.vm_transaction,
					hiddenExcelButton: false,
				})
			}
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});		
    }
    
    const getCategories = ({ target }) => {
		setState({ ...state, [target.name]: target.value });

    	axios.get(`http://localhost:8000/allcategories/${target.value}`,USER_TOKEN).then(response => {
            setState({
                ...state,
                companyId: target.value, 
                categories: response.data.message,
                
            })
        }).catch(error => {
            if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
        });
    }	

	const parseLocaleNumber = (stringNumber, locale) => {
		var thousandSeparator = Intl.NumberFormat(locale).format(11111).replace(/\p{Number}/gu, '');
		var decimalSeparator = Intl.NumberFormat(locale).format(1.1).replace(/\p{Number}/gu, '');

		return parseFloat(stringNumber
			.replace(new RegExp('\\' + thousandSeparator, 'g'), '')
			.replace(new RegExp('\\' + decimalSeparator), '.')
		);
	}

	const exportToCSV = () => {
		let newArr = []
		newArr = state.vm_transaction.map(p => ({
			"Bundle Number": p.bundle_number,
			"Date & time Bundle": p.creation_date,
			"Company Name": p.company_name_ar,
			"Category": p.category_text,
			"Serial": p.company_cards_serial,
			"PIN": p.pin_number,
			"POS Sell Price": parseLocaleNumber(p.cards_pos_sell_price),
			"Admin Sell Price": parseLocaleNumber(p.admin_sell_price),
			"POS Name": p.useerName,
			"POS Phone": p.user_phonenumber,
			"Profit":p.cards_pos_profit,
			"Invoice Number": p.rm_cards_receipt,
			"Date & time": p.creation_date,
			"Extra or Discount fees For POS": parseLocaleNumber(p.pos_discount_amount),
		    "Extra or Discount fees For Region": parseLocaleNumber(p.region_discount_amount)
		}));

		const ws = XLSX.utils.json_to_sheet(newArr);

		const wb = { Sheets: { 'حركة كارتات الشركات': ws }, SheetNames: ['حركة كارتات الشركات'] };

		const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });

		const data = new Blob([excelBuffer], { type: fileType });

		FileSaver.saveAs(data, `حركة كارتات الشركات` + fileExtension);
	}

	const options = {
		filter: true,
		filterType: 'dropdown',
		rowsPerPage: 5,
		rowsPerPageOptions: [5,10,25,50,100,250,500],
		responsive: 'vertical',
		enableNestedDataAccess: '.',
		selectableRowsHeader: false,
		selectableRows: "none",
		sort: false,
		download: false,
		print: false,
		viewColumns: false,
		fixedHeader: true,
		fixedSelectColumn: false,
		tableBodyHeight: "420px"
	};

	const { match } = props;
	// console.log(state);
	return (
		<React.Fragment>
    	{ PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
		<div className="shop-wrapper">
			<ToastContainer />
            <Helmet>
 				<title>عرض و تعديل الأصناف</title>
 				<meta name="description" content="عرض و تعديل الأصناف" />
 			</Helmet>
 			<PageTitleBar title={<IntlMessages id="sidebar.displayAll" />} match={match} />
			<div className="row">
				<RctCollapsibleCard
					colClasses="col-sm-12 col-md-12 col-lg-12">
					

					<div className="form-row mt-4">
						<div className="form-group col-md-12 text-center">
							{/* <h1 className="mb-3"><IntlMessages id="form.balances" /></h1> */}
							<RctCollapsibleCard fullBlock>
							<MuiThemeProvider theme={getMuiTheme()}>
							<MUIDataTable
								data={state.products}
								columns={[
									{
										label: "id",
										name:  "product_id",
										options: {
											display: "none",
											filter: false
										}
									},
									{
										label: <IntlMessages id="widgets.category" />,
										name:  "category_name" 
									},
									{
										label: <IntlMessages id="form.productName" />,
										name:  "product_name" 
									},
									{
										label: <IntlMessages id="form.purchasePrice" />,
										name:  "product_purchase_price",
									},
									{
										label: <IntlMessages id="form.sellPrice" />,
										name:  "product_sell_price"
									},
									{
										label: "وصف الصنف",
										name:  "product_desciption",
									},
									{
										label: <IntlMessages id="form.dataTime" />,
										name:  "update_date_time",
									},
									{
										label: <IntlMessages id="form.image" />,
										name: "product_image",
										options: {
											filter: true,
											sort: false,
											empty: true,
											customBodyRender: (value, tableMeta, updateValue) => {
												return (
													<div>
														<img src={"https://accbackend.alaimtidad-itland.com/" + value} alt="لا يوجد صورة" width="90" height="100" />
													</div>
												);
											}
										}
									},
								]}
								options={options}
							/>
							</MuiThemeProvider>
							</RctCollapsibleCard>
						</div>
					</div>
				</RctCollapsibleCard>
			</div>
		</div>
		: (
			
			history.push("/access-denied")
		   )
		} 
		</React.Fragment>
	)
	}
