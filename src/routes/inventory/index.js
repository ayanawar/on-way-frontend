/**
 * Tables Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Helmet } from "react-helmet";

// async components
import {
    AsyncAddInventoryComponent,
    AsyncUpdateInventoryComponent,
    AsyncIrredeemableComponent,
    AsyncReturnsComponent,
    AsyncOutOfInventoryComponent,
    AsyncCategories,
    AsyncWarehouses
} from 'Components/AsyncComponent/AsyncComponent';

const Pages = ({ match }) => (
    <div className="content-wrapper">
        <Helmet>
            <title>إدارة المخازن</title>
            <meta name="description" content="إدارة المخازن" />
        </Helmet>
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/addemployee`} />

            <Route path={`${match.url}/add-products`} component={AsyncAddInventoryComponent} />
            <Route path={`${match.url}/display-all-products`} component={AsyncUpdateInventoryComponent} />
            <Route path={`${match.url}/irredeemable`} component={AsyncIrredeemableComponent} />
            <Route path={`${match.url}/returns`} component={AsyncReturnsComponent} />
            <Route path={`${match.url}/out-of-inventory`} component={AsyncOutOfInventoryComponent} />  
            <Route path={`${match.url}/categories`} component={AsyncCategories} />
            <Route path={`${match.url}/warehouses`} component={AsyncWarehouses} />
            
        </Switch>
    </div>
);

export default Pages;
