import React, { useEffect, useState } from 'react';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { RctCard, RctCardContent } from 'Components/RctCard';
// intl messages
import IntlMessages from 'Util/IntlMessages';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import USER_TOKEN from '../../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../../constants/TokenPhoneNum';
import { useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { Helmet } from "react-helmet";
import MUIDataTable from "mui-datatables";
import Modal from 'react-awesome-modal';
const cookies = new Cookies();
const Token = cookies.get('UserToken');



export default function Shop(props) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm();
    const [state, setState] = useState({
        warehouses:[],
        regions: [],
        areas: [],
        disabled: false,
        hiddenStatusForm: true,
        showModalForConfirmation: false,
        warehouseNameAr:'',
        warehouseNameEn:'',
        warehouseID:'',
        address: '',
        regionID: '',
        areaID: '',
        updateflag:true
    });


    const options = {
        filter: true,
        filterType: 'dropdown',
        rowsPerPage: 10,
        rowsPerPageOptions: [5,10,25,50,100],
        responsive: 'vertical',
        enableNestedDataAccess: '.',
        selectableRows: "none",
        viewColumns: false,
        sort: false,
        fixedHeader: true,
        download: false,
        fixedSelectColumn: false,
        tableBodyHeight: "600px"
    };

    useEffect(() => {
    // all suppliers
        axios.get('https://accbackend.alaimtidad-itland.com/warehouses',USER_TOKEN).then(response => {
            axios.get('https://accbackend.alaimtidad-itland.com/all-regions', USER_TOKEN).then(response8 => {
                if(response.data == "Token Expired" || response.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', {path:'/'})
                    window.location.href = "/signin";	
                }
                else{
                    response8.data.regions.splice(0,1)
                    setState({
                        ...state,
                        warehouses: response.data.warehouses, 
                        regions: response8.data.regions,      
                    })
                }
            }).catch(error => {

                if (error.response.status === 429) {
                    toast.error(error.response.data, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    }); 
                }
            });
        }).catch(error => {
            if (error.response8.status === 429) {
                toast.error(error.response.data, {
                position: "top-center",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
                }); 
            }
        });
    }, []);

    const handleFields = ({ target }) => {
        setState({ ...state, [target.name]: target.value });
    };

    const getMuiTheme = () => createMuiTheme({
        overrides: {
            MUIDataTable: {
                responsiveScroll: {
                    maxHeight: 'unset',
                    overflowX: 'unset',
                    overflowY: 'unset',
                },
            },
            MuiTableCell: {
                head: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif"
                }, 
                body: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontSize: "15px",
                    fontFamily: "'Almarai', sans-serif",
                }       
            },
            MUIDataTableHeadCell: {
                data: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                },
                fixedHeader: {
                    position: "sticky !important",
                    zIndex: '100',
                }
            },
            MUIDataTableSelectCell: {
                headerCell: {
                    zIndex: 1
                },
                fixedLeft: {
                    zIndex: 1
                }
            },
            MUIDataTableToolbarSelect: {
                root: {
                    color: "#599A5F",
                    fontWeight: "bold",
                    zIndex: 1,
                }         
            },
            MuiPaper: {
                root: {
                    color: "#092346",
                    fontWeight: "bold",
                    fontFamily: "'Almarai', sans-serif"
                }
            },
            MuiToolbar:{
                regular: {
                    backgroundColor:"gray"
                },
                root: {
                    top: 0,
                    position: 'sticky',
                    background: 'white',
                    zIndex: '100',
                },
            },
            MUIDataTablePagination: {
                tableCellContainer: {
                    backgroundColor:"gray"
                }
            },
            MUIDataTableBody: {
                emptyTitle: {
                    display: "none",
                }
            }
        }
    })
    const onSubmitUPDATE = () => {
        axios({
            url:`https://accbackend.alaimtidad-itland.com/warehouses/${state.warehouseID}`,
            method:'put',
            data:{
                "warehouse_name":state.warehouseNameAr,
                "warehouse_name_en":state.warehouseNameEn, 
                "warehouse_address": state.address,
                "warehouse_region": state.regionID,
                "warehouse_area": state.areaID,
            },
            headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                } else{
                    // console.log(res);
                    toast.success(<IntlMessages id="updatedsuccess" />, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                    setState({ ...state, disabled: true })
                    setTimeout(function () { location.reload()}, 3000)
                }      
        }).catch(error => {
            if (error.response.status === 406) {
                toast.error("هذا الاسم مسجل من قبل", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 405) {
                toast.error("عفواَ لا يمكنك إكمال العملية لأنه يوجد بيانات متعلقة بهذا البيان فلا يمكنك حذفه أو التعديل علية الإ بمسح البيانات المتعلقة بيه أولا", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });  
            }
        });
    }

    const onClearStatusClicked = () => {
        setState({
            ...state,
            updateflag:true,
            disabled: false,
            hiddenStatusForm: true,
            hiddenDebitLimitForm: true,
            warehouseNameAr:'',
            warehouseNameEn:'',
            warehouseID:'',
            address: '',
            regionID: '',
            areaID: '',   
        })
    };

    const handleConfirmation = () => {
        axios({
            url:`https://accbackend.alaimtidad-itland.com/warehouses/${state.warehouseID}`,
            method:'delete',
            headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                } else{
                    toast.success("تم الحذف بنجاح", {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                    setState({ ...state, disabled: true, showModalForConfirmation: false, })
                    setTimeout(function () { location.reload()}, 3000)
                }      
        }).catch(error => {
            if (error.response.status === 406) {
                toast.error("هذا الاسم مسجل من قبل", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 405) {
                toast.error("عفواَ لا يمكنك إكمال العملية لأنه يوجد بيانات متعلقة بهذا البيان فلا يمكنك حذفه أو التعديل علية الإ بمسح البيانات المتعلقة بيه أولا", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });  
            }
        });
    }
    const onSubmit = e => {
        axios({
            url:'https://accbackend.alaimtidad-itland.com/warehouses',
            method:'post',
            data:{
                "warehouse_name":state.warehouseNameAr,
                "warehouse_name_en":state.warehouseNameEn, 
                "warehouse_address": state.address,
                "warehouse_region": state.regionID,
                "warehouse_area": state.areaID,
            },
            headers: { "x-access-token": `${cookies.get('UserToken')}`}}).then(res => {
                if (res.data == "Token Expired" || res.data == "Token UnAuthorized") {
                    cookies.remove('UserToken', { path: '/' })
                    window.location.href = "/signin";
                } else{
                    toast.success(<IntlMessages id="addedsuccessfully" />, {
                        position: "top-center",
                        autoClose: 4000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                setState({ ...state, disabled: true })
                setTimeout(function () { location.reload()}, 3000)
            }   
        }).catch(error => { 
            if (error.response.status === 406) {
                toast.error("هذا الاسم مسجل من قبل", {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } else if (error.response.status === 429) {
                toast.error(error.response.data, {
                    position: "top-center",
                    autoClose: 4000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                }); 
            } 
        });
        
    }

    const closeModel3 = () => {
        setState({
            ...state,
            showModalForConfirmation: false,
            warehouseNameAr:'',
            warehouseNameEn:'',
            warehouseID:'',
            address: '',
            regionID: '',
            areaID: '',   
        });
    }

    const handleTwoEvents3 = (e) => {
		handleFields(e);
		let regionId = e.target.value;
		axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": regionId }, USER_TOKEN).then(response => {
			if (response.data == "Token Expired" || response.data == "Token UnAuthorized") {
				cookies.remove('UserToken', { path: '/' })
				window.location.href = "/signin";
			}
			else {
		
				if(response.data.areas.length == 0){
					toast.error('لايوجد حاليا مناطق متوفرة فى هذة المحافظة', {
						position: "top-center",
						autoClose: 4000,
						hideProgressBar: false,
						closeOnClick: true,
						pauseOnHover: true,
						draggable: true
					});
					setState({
						...state,
						regionID: regionId,
						areas: response.data.areas,
						areaID: '',
						
					})

				}else{
					setState({
						...state,
						regionID: regionId,
						areas: response.data.areas,
						
					})
				}
				
			}
		}).catch(error => {
			if (error.response.status === 429) {
				toast.error(error.response.data, {
				   position: "top-center",
				   autoClose: 4000,
				   hideProgressBar: false,
				   closeOnClick: true,
				   pauseOnHover: true,
				   draggable: true
				}); 
			}
		});
	}

    const { match } = props;
    // console.log(state);

    return (
        <React.Fragment>
            { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
                <div className="shop-wrapper">
                    <ToastContainer />
                    <Helmet>
                        <title>المستودعات</title>
                        <meta name="description" content="المستودعات" />
                    </Helmet>
                    <PageTitleBar title={<IntlMessages id="sidebar.warehouses" />} match={match} />
                    <div className="row">
                        <RctCollapsibleCard
                            colClasses="col-sm-12 col-md-12 col-lg-12"
                            heading={<IntlMessages id="sidebar.warehouses" />}>

                            {(state.updateflag == true) ?  
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>المستودع</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                            
                                                <input type="text" className="form-control" 
                                                        name="warehouseNameAr" 
                                                        defaultValue={state.warehouseNameAr}
                                                        onChange={handleFields}
                                                        ref={register({ required: true,
                                                                minLength: 3,
                                                                pattern: /^[ء-ي_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label>المستودع باللغة الإنجليزية</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="warehouseNameEn" 
                                                        defaultValue={state.warehouseNameEn}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                    minLength: 3, 
                                                                    pattern: /^[A-Za-z_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>العنوان</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="address" 
                                                        defaultValue={state.address}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                        minLength: 3 })} />
                                            </div>
                                            <span className="errors">
                                                {errors.address && errors.address.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.address && errors.address.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                            </span>
                                        </div>
                                        <div className="form-group  dropdown col-md-4">
                                            <label><IntlMessages id="form.region" /></label>
                                            <select name="regionID" className="form-control input-text"
                                                    onChange={handleTwoEvents3} 
                                                    ref={register({ required: true })}>
                                                <option key="0" value="">برجاء اختيار المحافظة</option>
                                                    {state.regions.map(region => {
                                                        return (
                                                            <option key={region.region_id} value={region.region_id}>
                                                                {region.region_arabic_name}
                                                            </option>
                                                        )
                                                    })
                                                    }
                                            </select>
                                            <span className="errors">
                                                {errors.regionID && errors.regionID.type === 'required' &&
                                                    <IntlMessages id="form.requiredOptionError" />}
                                            </span>
                                        </div>
                                        <div className="form-group  dropdown col-md-4">
                                            <label><IntlMessages id="form.area" /></label>
                                            <select name="areaID" className="form-control input-text"
                                                    onChange={handleFields} ref={register({ required: true })}>
                                                    <option key="0" value="">برجاء اختيار المنطقة</option>
                                                    {state.areas.map(area => {
                                                        return (
                                                            <option key={area.area_id} value={area.area_id}>
                                                                {area.area_arabic_name}
                                                            </option>
                                                        )
                                                    })
                                                    }
                                            </select>
                                            <span className="errors">
                                                {errors.areaID && errors.areaID.type === 'required' &&
                                                    <IntlMessages id="form.requiredOptionError" />}
                                            </span>
                                        </div>                    
                                    </div>
                                    {(state.disabled === false) ? 
                                        <button type="submit" className="btn mainButtonColor">
                                            <IntlMessages id="form.add" />
                                        </button> : 
                                        <button type="submit" className="btn btn-secondary" disabled={true}>
                                            <IntlMessages id="form.add" />
                                        </button>
                                    }
                                </form>
                                :
                                <form onSubmit={handleSubmit(onSubmitUPDATE)}>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>المستودع</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                            
                                                <input type="text" className="form-control" 
                                                        name="warehouseNameAr" 
                                                        defaultValue={state.warehouseNameAr}
                                                        onChange={handleFields}
                                                        ref={register({ required: true,
                                                                minLength: 3,
                                                                pattern: /^[ء-ي_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.warehouseNameAr && errors.warehouseNameAr.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                        <div className="form-group col-md-4">
                                            <label>المستودع باللغة الإنجليزية</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="warehouseNameEn" 
                                                        defaultValue={state.warehouseNameEn}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                    minLength: 3, 
                                                                    pattern: /^[A-Za-z_ ]+$/i, })} />
                                            </div>
                                            <span className="errors">
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                                {errors.warehouseNameEn && errors.warehouseNameEn.type === 'pattern' &&
                                                    <IntlMessages id="form.lettersOnlyError" />}
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="form-group col-md-4">
                                            <label>العنوان</label>
                                            <div style={{ position:"relative", margin:"0 30px" ,  borderStyle: "solid",
                                                        borderWidth: "0px 2px 2px 0px",
                                                        lineHeight: "0px",
                                                        borderColor: "#A8A8A8 #E0E0E0 #E0E0E0 #A8A8A8"}}>
                                                                                        
                                                <input type="text" className="form-control" 
                                                        name="address" 
                                                        defaultValue={state.address}
                                                        onChange={handleFields}
                                                        ref={register({ required: true, 
                                                                        minLength: 3 })} />
                                            </div>
                                            <span className="errors">
                                                {errors.address && errors.address.type === 'required' &&
                                                    <IntlMessages id="form.requiredError" />}
                                                {errors.address && errors.address.type === 'minLength' &&
                                                    <IntlMessages id="form.minLengthError" />}
                                            </span>
                                        </div>
                                        <div className="form-group  dropdown col-md-4">
                                            <label><IntlMessages id="form.region" /></label>
                                            <select name="regionID" className="form-control input-text"
                                            value={state.regionID}
                                                    onChange={handleTwoEvents3}>
                                                <option key="0" value="">برجاء اختيار المحافظة</option>
                                                    {state.regions.map(region => {
                                                        return (
                                                            <option key={region.region_id} value={region.region_id}>
                                                                {region.region_arabic_name}
                                                            </option>
                                                        )
                                                    })
                                                    }
                                            </select>
                                        </div>
                                        <div className="form-group  dropdown col-md-4">
                                            <label><IntlMessages id="form.area" /></label>
                                            <select name="areaID" className="form-control input-text"
                                            value={state.areaID}
                                                    onChange={handleFields}>
                                                    <option key="0" value="">برجاء اختيار المنطقة</option>
                                                    {state.areas.map(area => {
                                                        return (
                                                            <option key={area.area_id} value={area.area_id}>
                                                                {area.area_arabic_name}
                                                            </option>
                                                        )
                                                    })
                                                    }
                                            </select>
                                        </div>                    
                                    </div>

                                    {(state.disabled === false) ? 
                                        <button type="submit" className="btn btn-warning">
                                            <IntlMessages id="form.update" />
                                        </button> : 
                                        <button type="submit" className="btn btn-secondary" disabled={true} >
                                            <IntlMessages id="form.update" />
                                        </button>
                                    }

                                    <button type="reset" className="btn btn-danger ml-5 mr-5" onClick={onClearStatusClicked}>
                                        <IntlMessages id="form.clear" />
                                    </button>
                                </form>
                            }
                        </RctCollapsibleCard>
                    </div>

                    {state.showModalForConfirmation == true ?
                        <Modal visible={state.showModalForConfirmation} width="550" height="150" effect="fadeInUp" 
                            onClickAway={closeModel3}>
                            <div className="modal-header">
                                <h3 className="modal-title"> 
                                    <span className="ml-3"> هل أنت متأكد من حذف هذا البيان؟</span>
                                </h3> 
                                {/* <br /> */}
                                
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-primary"
                                        onClick={handleConfirmation}>
                                    موافق
                                </button>                             
                                
                                <button type="button" className="btn btn-secondary" 
                                        onClick={closeModel3}>
                                    إغلاق
                                </button>
                            </div>
                        </Modal>
                    : 
                        null
                    }

                    <div className="row mb-5">
                        <RctCollapsibleCard colClasses="col-sm-12 col-md-12 col-lg-12">
                            <RctCard>
                                <RctCardContent noPadding>
                                    <MuiThemeProvider theme={getMuiTheme()}>
                                        <MUIDataTable 
                                            // title={<IntlMessages id="sidebar.cart" />}
                                            data={state.warehouses}
                                            columns={[
                                                    {
                                                        label: "id",
                                                        name:  "warehouse_id",
                                                        options: {
                                                            display: "none",
                                                            filter: false,
                                                            print: false,
                                                        }
                                                    },
                                                    {
                                                        label: "المستودع",
                                                        name:  "warehouse_name",
                                                    },
                                                    {
                                                        label: "المستودع باللغة الإنجليزية",
                                                        name:  "warehouse_name_en",
                                                    },
                                                    {
                                                        label: <IntlMessages id="components.address" />,
                                                        name:  "warehouse_address",
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.region" />,
                                                        name:  "region_arabic_name",
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.area" />,
                                                        name:  "area_arabic_name"
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.dataTime" />,
                                                        name:  "update_date_time"
                                                    },
                                                    {
                                                        label: "id",
                                                        name:  "warehouse_region",
                                                        options: {
                                                            display: "none",
                                                            filter: false,
                                                            print: false,
                                                        }
                                                    },
                                                    {
                                                        label: "id",
                                                        name:  "warehouse_area",
                                                        options: {
                                                            display: "none",
                                                            filter: false,
                                                            print: false,
                                                        }
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.update" />,
                                                        name: "",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button type="button" className="btn btn-warning" 
                                                                    onClick={() => {
                                                                        axios.post('https://accbackend.alaimtidad-itland.com/all-areas-by-region-ID', { "regionID": `${tableMeta.rowData[7]}` }, USER_TOKEN).then(response => {
                                                                            setState({
                                                                                ...state, 
                                                                                updateflag: false,
                                                                                warehouseID:tableMeta.rowData[0],
                                                                                warehouseNameAr:tableMeta.rowData[1],
                                                                                warehouseNameEn:tableMeta.rowData[2],
                                                                                address: tableMeta.rowData[3],
                                                                                regionID: tableMeta.rowData[7],
                                                                                areaID: tableMeta.rowData[8],
                                                                                areas: response.data.areas,
                                                                            });
                                                                        
                                                                        }).catch(error => {
                                                                            if (error.response.status === 429) {
                                                                                toast.error(error.response.data, {
                                                                                position: "top-center",
                                                                                autoClose: 4000,
                                                                                hideProgressBar: false,
                                                                                closeOnClick: true,
                                                                                pauseOnHover: true,
                                                                                draggable: true
                                                                                }); 
                                                                            }
                                                                        });
                                                                    }}>
                                                                    <IntlMessages id="form.update" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        }
                                                        }
                                                    },
                                                    {
                                                        label: <IntlMessages id="form.delete" />,
                                                        name: "",
                                                        options: {
                                                            filter: true,
                                                            sort: false,
                                                            empty: true,
                                                            print: false,
                                                            customBodyRender: (value, tableMeta, updateValue) => {
                                                            return (
                                                                <React.Fragment>
                                                                    <button type="button" className="btn btn-danger" 
                                                                    onClick={() => {
                                                                        setState({
                                                                            ...state, 
                                                                            updateflag: true,
                                                                            showModalForConfirmation: true,
                                                                            warehouseID:tableMeta.rowData[0],
                                                                        });
                                                                    }}>
                                                                    <IntlMessages id="form.delete" />
                                                                    </button>
                                                                </React.Fragment>
                                                            );
                                                        }
                                                        }
                                                    },
                                                ]}
                                            options={options}
                                        />
                                    </MuiThemeProvider>
                                </RctCardContent>
                            </RctCard>
                        </RctCollapsibleCard>
                    </div>
                </div>
                : (

                history.push("/access-denied")
                )
            }
        </React.Fragment>        
    )
}