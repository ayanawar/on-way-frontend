/**
 * Charts Widgets Page
 */
import React, { useState, useEffect } from 'react';
import axios from 'axios';

import {
	DailySales,
	DailySalesAsiacell,
	DailySalesKorek,
	DailySalesZain,
	DailySalesPubg,
	DailySalesiTunes,
	DailySalesXbox
} from "Components/Widgets";

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';
// import axios from 'axios';
// rct collapsible card
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import USER_TOKEN from '../../constants/Token';
import PURE_TOKEN_USER_TYPE from '../../constants/TokenUserType';
import PURE_TOKEN_PHONE_NUM from '../../constants/TokenPhoneNum';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Helmet } from "react-helmet";
import { useHistory } from 'react-router-dom';

export default function HomePage(props) {
	const history = useHistory();
	const [state, setState] = useState({
		chartData:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForAsiacell:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForKorek:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForZain:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForPubg:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForiTunes:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
		chartDataForXbox:{
			labels: [],
			chartdata: [],
			datasets:[
				{
				  data:[],
				}
			]
		},
	});
	const [numberofrequests, setnumberofrequest] = useState('');
	const [totalrequests, settotalrequest] = useState('');
	const [totalworkers, settotalworkers] = useState('');
	const [totalservices, settotalservices] = useState('');
	const [totalcompleted, settotalcompleted] = useState('');
	const [totalmoney, settotalmoney] = useState('');
	useEffect(() => {
		axios.post('http://localhost:3000/getbarchardata',{service_provider_location_id:localStorage.getItem("service_provider_location_id")},USER_TOKEN).then(res => {
			axios.post('http://localhost:3000/provider/reports',{service_provider_location_id:localStorage.getItem("service_provider_location_id")},USER_TOKEN).then(res1 => {
			axios.post('http://localhost:3000/gettotalmoney',{service_provider_location_id:localStorage.getItem("service_provider_location_id")},USER_TOKEN).then(res2 => {
    console.log("ssss",res2.data.totalAmount[0]);
			
				setnumberofrequest(res.data.no_allrequests)
				settotalrequest(res1.data.message.total_inprogress)
				settotalworkers(res1.data.message.total_workers)
				settotalservices(res1.data.message.total_services)
				settotalcompleted(res1.data.message.total_completed)
				settotalmoney(res2.data.totalAmount[0].total_money)
								
	}).catch(function (error) {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	})		
	}).catch(function (error) {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	})
	}).catch(function (error) {
		if (error.response.status === 429) {
			toast.error(error.response.data, {
			   position: "top-center",
			   autoClose: 4000,
			   hideProgressBar: false,
			   closeOnClick: true,
			   pauseOnHover: true,
			   draggable: true
			}); 
		}
	})
	},[]);
	return (
        <React.Fragment>
        { PURE_TOKEN_PHONE_NUM === localStorage.getItem("user_email") ?
			<div className="row mb-5">
				<Helmet>
					<title>Home page</title>
					<meta name="description" content="الرئيسية" />
				</Helmet>
			<RctCollapsibleCard
				colClasses="col-sm-12 col-md-12 col-lg-12">
			<div className="charts-widgets-wrapper">
				<PageTitleBar title={<IntlMessages id="sidebar.charts" />} match={props.match} />
				<RctCollapsibleCard
					heading={<span className="reportsCardsNumberCenter">Requests</span>}
					style={{color: "#599A5F !impotant"}}
					closeable
					collapsible
				>
					<DailySales
						label={"Daily Requests"}
						chartdata={state.chartData.datasets[0].data}
						labels={state.chartData.labels}
						/>
				</RctCollapsibleCard>
				<div className="row">
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter">Total Requests</span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						closeable
						collapsible
					
					>
						<label>{numberofrequests}</label>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter">Total Money Earn</span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						collapsible
						closeable
					
					>
						<label>{totalmoney}</label>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter">Completed Tasks </span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						collapsible
						closeable
					>
					<label>{totalcompleted}</label>

					</RctCollapsibleCard>
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter"> In progress Tasks</span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						collapsible
						closeable
					>
						<label>{totalrequests}</label>

					</RctCollapsibleCard>
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter">Total Workers</span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						collapsible
						closeable
					>
							<label>{totalworkers}</label>
					</RctCollapsibleCard>
					<RctCollapsibleCard
						heading={<span className="reportsCardsNumberCenter"> Available services</span>}
						colClasses="col-sm-6 col-md-4 w-xs-full"
						collapsible
						closeable
					>
							<label>{totalservices}</label>
					</RctCollapsibleCard>
					<br />
				</div>
			</div>
			<br />
			</RctCollapsibleCard>
			<br />
			</div>
            : (
				history.push("/access-denied")
               )
        } 
            </React.Fragment>
		);
	// }
}
